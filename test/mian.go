package main

import (
	"fmt"
	"sync"
	"time"
)

/*
@author pengshuo
@date 2021/8/11 19:35
version 1.0.0
desc: go 值传递

*/
type student struct {
	id int
	b  int
	a  int
}

var smap sync.Map

func main() {
	stud := new(student)
	stud.id = 1
	stud.a = 1
	stud.b = 1
	// 存入stud引用
	smap.Store(1, stud)

	go func() {
		for true {
			load, ok := smap.Load(1)
			if ok {
				//取出stu引用值
				stu, match := load.(*student)
				if match {
					stu.a += 1
				}
			}
		}
	}()
	time.Sleep(time.Second * 3)
	go func() {
		i := 0
		for true {
			load, ok := smap.Load(1)
			if ok {
				//取出stu引用值
				stu, match := load.(*student)
				if match {
					stu.b += 1
				}
				i++
				if i == 15 {
					//传递stu引用值的值
					printStu(*stu)
				}
			}
		}
	}()
	time.Sleep(time.Second * 1)
	load, ok := smap.Load(1)
	if ok {
		stu, match := load.(*student)
		if match {
			fmt.Println("-- over student --", stu.id, stu.a, stu.b)
		}
	}

}
func printStu(stu student) {
	go func() {
		for true {
			fmt.Println("-- print student --", stu.id, stu.a, stu.b)
		}
	}()
}
