module hm

go 1.16

require (
	95eh.com/eg v0.0.1
	github.com/Licoy/stail v0.0.4
	github.com/RussellLuo/timingwheel v0.0.0-20201029015908-64de9d088c74
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/gin-contrib/multitemplate v0.0.0-20211002122701-e9e3201b87a0
	github.com/gin-gonic/gin v1.7.4
	github.com/go-redis/redis/v8 v8.11.4
	github.com/gogo/protobuf v1.3.2
	github.com/golang-module/carbon v1.5.4
	github.com/gookit/color v1.5.0
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/consul/api v1.10.1
	github.com/json-iterator/go v1.1.11
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mitchellh/mapstructure v1.4.2
	github.com/olivere/elastic v6.2.37+incompatible
	github.com/orcaman/concurrent-map v0.0.0-20210501183033-44dafcb38ecc
	github.com/panjf2000/ants/v2 v2.4.3
	github.com/tealeg/xlsx v1.0.5
	github.com/vmihailenco/msgpack/v5 v5.3.4
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.16
)

replace 95eh.com/eg => ./pkg/eggo
