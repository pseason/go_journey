package rest

import (
	"95eh.com/eg/codec"
	"encoding/hex"
	"fmt"
	"hm/pkg/game/msg_gm"
	"testing"
)

func TestPb(t *testing.T) {
	p := &msg_gm.ReqSysAuth{
		Key:      "a",
		DeviceId: "b",
	}
	c := codec.NewPbCodec()
	bytes, _ := c.Marshal(p)
	str := hex.EncodeToString(bytes)
	fmt.Println(str)
}