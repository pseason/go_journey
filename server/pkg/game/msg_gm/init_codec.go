package msg_gm

import (
	"95eh.com/eg/intfc"
)

// 绑定与客户端通信的消息协议号和消息结构体的映射
func InitClientCodec(userLogic intfc.IMUserLogic) {
	InitCodecForBackpack(userLogic)
	InitCodecForBackpackShortcutBar(userLogic)
	InitCodecForCharacterAttr(userLogic)
	InitCodecForCharacterInfo(userLogic)
	InitCodecForCity(userLogic)
	InitCodecForCitybuilding(userLogic)
	InitCodecForCityfarm(userLogic)
	InitCodecForCityMakeWine(userLogic)
	InitCodecForCityMarket(userLogic)
	InitCodecForClan(userLogic)
	InitCodecForEventManager(userLogic)
	InitCodecForMail(userLogic)
	InitCodecForMall(userLogic)
	InitCodecForPlot(userLogic)
	InitCodecForSocial(userLogic)
	InitCodecForSpace(userLogic)
	InitCodecForSys(userLogic)
	InitCodecForTask(userLogic)
	InitCodecForTeam(userLogic)
	InitCodecForTesttx(userLogic)
	InitCodecForUnlock(userLogic)
	InitCodecForWeather(userLogic)
}
