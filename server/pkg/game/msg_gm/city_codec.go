package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdCityGetList              MsgCode = 103001001 // Req：；Res：；
	CdCityJoinCity             MsgCode = 103001002 // Req：；Res：；
	CdCityExitCity             MsgCode = 103001003 // Req：；Res：；
	CdCityLoadCity             MsgCode = 103001004 // Req：；Res：；
	CdCityUpNameOrAnnouncement MsgCode = 103001005 // Req：修改城市昵称、公告、宣言；Res：修改城市昵称、公告、宣言；
	CdCityPageCityMember       MsgCode = 103001006 // Req：；Res：；
	CdNoticeWaiting            MsgCode = 103001007 // Notice：；
	CdCityEnterBroadcastSpe    MsgCode = 103001008 // Req：//请求进入场景动作；Res：//响应进入场景动作；
	CdNoticeCityEnter          MsgCode = 103001009 // Notice：//广播进入场景动作通知；
	CdCityEnter                MsgCode = 103001010 // Req：//请求进入场景；Res：；
)

func InitCodecForCity(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdCityGetList,
		func() interface{} {
			return &ReqCityGetList{}
		},
		func() interface{} {
			return &ResCityGetList{}
		})
	userLogic.BindCoderFac(CdCityJoinCity,
		func() interface{} {
			return &ReqCityJoinCity{}
		},
		func() interface{} {
			return &ResCityJoinCity{}
		})
	userLogic.BindCoderFac(CdCityExitCity,
		func() interface{} {
			return &ReqCityExitCity{}
		},
		func() interface{} {
			return &ResCityExitCity{}
		})
	userLogic.BindCoderFac(CdCityLoadCity,
		func() interface{} {
			return &ReqCityLoadCity{}
		},
		func() interface{} {
			return &ResCityLoadCity{}
		})
	userLogic.BindCoderFac(CdCityUpNameOrAnnouncement,
		func() interface{} {
			return &ReqCityUpNameOrAnnouncement{}
		},
		func() interface{} {
			return &ResCityUpNameOrAnnouncement{}
		})
	userLogic.BindCoderFac(CdCityPageCityMember,
		func() interface{} {
			return &ReqCityPageCityMember{}
		},
		func() interface{} {
			return &ResCityPageCityMember{}
		})
	userLogic.BindCoderFac(CdNoticeWaiting,
		nil,
		func() interface{} {
			return &NoticeWaiting{}
		})
	userLogic.BindCoderFac(CdCityEnterBroadcastSpe,
		func() interface{} {
			return &ReqCityEnterBroadcastSpe{}
		},
		func() interface{} {
			return &ResCityEnterBroadcastSpe{}
		})
	userLogic.BindCoderFac(CdNoticeCityEnter,
		nil,
		func() interface{} {
			return &NoticeCityEnter{}
		})
	userLogic.BindCoderFac(CdCityEnter,
		func() interface{} {
			return &ReqCityEnter{}
		},
		func() interface{} {
			return &ResCityEnter{}
		})
}
