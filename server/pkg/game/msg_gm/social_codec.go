package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdPageSocial                MsgCode = 117001001 // Req：分页请求好友申请、好友、黑名单、仇人；Res：分页响应好友申请、好友、黑名单、仇人；
	CdSocialApplyJoinBuddy      MsgCode = 117001002 // Req：请求好友申请；Res：响应好友申请；
	CdNoticeBuddyApply          MsgCode = 117001003 // Notice：好友申请通知；
	CdUnReadBuddyApplyCount     MsgCode = 117001004 // Req：请求未处理好友申请数量；Res：请求未处理好友申请数量；
	CdBuddyApplyHandel          MsgCode = 117001005 // Req：请求处理好友申请；Res：请求处理好友申请；
	CdNoticeJoinBuddy           MsgCode = 117001006 // Notice：成为好友、黑名单通知；
	CdSocialApplyJoinBlacklist  MsgCode = 117001007 // Req：请求添加黑名单；Res：响应添加黑名单；
	CdSocialApplyDelBuddy       MsgCode = 117001008 // Req：请求删除好友；Res：响应删除好友；
	CdSocialApplyDelBlacklist   MsgCode = 117001009 // Req：请求删除黑名单；Res：响应删除黑名单；
	CdSearchCharacter           MsgCode = 117001010 // Req：请求玩家信息；Res：响应玩家信息；
	CdSearchCharacterSocialList MsgCode = 117001011 // Req：请求玩家好友id；Res：响应玩家好友id；
)

func InitCodecForSocial(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdPageSocial,
		func() interface{} {
			return &ReqPageSocial{}
		},
		func() interface{} {
			return &ResPageSocial{}
		})
	userLogic.BindCoderFac(CdSocialApplyJoinBuddy,
		func() interface{} {
			return &ReqSocialApplyJoinBuddy{}
		},
		func() interface{} {
			return &ResSocialApplyJoinBuddy{}
		})
	userLogic.BindCoderFac(CdNoticeBuddyApply,
		nil,
		func() interface{} {
			return &NoticeBuddyApply{}
		})
	userLogic.BindCoderFac(CdUnReadBuddyApplyCount,
		func() interface{} {
			return &ReqUnReadBuddyApplyCount{}
		},
		func() interface{} {
			return &ResUnReadBuddyApplyCount{}
		})
	userLogic.BindCoderFac(CdBuddyApplyHandel,
		func() interface{} {
			return &ReqBuddyApplyHandel{}
		},
		func() interface{} {
			return &ResBuddyApplyHandel{}
		})
	userLogic.BindCoderFac(CdNoticeJoinBuddy,
		nil,
		func() interface{} {
			return &NoticeJoinBuddy{}
		})
	userLogic.BindCoderFac(CdSocialApplyJoinBlacklist,
		func() interface{} {
			return &ReqSocialApplyJoinBlacklist{}
		},
		func() interface{} {
			return &ResSocialApplyJoinBlacklist{}
		})
	userLogic.BindCoderFac(CdSocialApplyDelBuddy,
		func() interface{} {
			return &ReqSocialApplyDelBuddy{}
		},
		func() interface{} {
			return &ResSocialApplyDelBuddy{}
		})
	userLogic.BindCoderFac(CdSocialApplyDelBlacklist,
		func() interface{} {
			return &ReqSocialApplyDelBlacklist{}
		},
		func() interface{} {
			return &ResSocialApplyDelBlacklist{}
		})
	userLogic.BindCoderFac(CdSearchCharacter,
		func() interface{} {
			return &ReqSearchCharacter{}
		},
		func() interface{} {
			return &ResSearchCharacter{}
		})
	userLogic.BindCoderFac(CdSearchCharacterSocialList,
		func() interface{} {
			return &ReqSearchCharacterSocialList{}
		},
		func() interface{} {
			return &ResSearchCharacterSocialList{}
		})
}
