package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdMall                 MsgCode = 110001001 // Req：请求所有商城数据；Res：响应所有商城数据；
	CdMallPurchaseProp     MsgCode = 110001002 // Req：请求购买商品；Res：响应购买商品；
	CdNoticeMallInfoChange MsgCode = 110001003 // Notice：商品变更通知；
)

func InitCodecForMall(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdMall,
		func() interface{} {
			return &ReqMall{}
		},
		func() interface{} {
			return &ResMall{}
		})
	userLogic.BindCoderFac(CdMallPurchaseProp,
		func() interface{} {
			return &ReqMallPurchaseProp{}
		},
		func() interface{} {
			return &ResMallPurchaseProp{}
		})
	userLogic.BindCoderFac(CdNoticeMallInfoChange,
		nil,
		func() interface{} {
			return &NoticeMallInfoChange{}
		})
}
