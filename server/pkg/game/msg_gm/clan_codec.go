package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdClanCreate          MsgCode = 119001001 // Req：请求创建氏族；Res：响应创建氏族；
	CdClanLoad            MsgCode = 119001002 // Req：请求加载氏族；Res：响应加载氏族；
	CdClanUpNameOrSurname MsgCode = 119001003 // Req：请求修改氏族名称和姓氏；Res：响应修改氏族名称和姓氏；
	CdClanUpDeclaration   MsgCode = 119001004 // Req：请求修改氏族宣言；Res：响应修改氏族宣言；
	CdNoticeClanChange    MsgCode = 119001005 // Notice：氏族名称和姓氏、公告修改通知；
	CdClanPageSearch      MsgCode = 119001006 // Req：分页请求氏族列表；Res：响应分页请求氏族列表；
	CdClanSignIn          MsgCode = 119001007 // Req：请求氏族签到；Res：响应氏族签到；
	CdClanUpIntroduction  MsgCode = 119001008 // Req：请求修改个人简介；Res：响应修改个人简介；
	CdClanUpBirthday      MsgCode = 119001009 // Req：请求修改个人生辰；Res：响应修改个人生辰；
)

func InitCodecForClan(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdClanCreate,
		func() interface{} {
			return &ReqClanCreate{}
		},
		func() interface{} {
			return &ResClanCreate{}
		})
	userLogic.BindCoderFac(CdClanLoad,
		func() interface{} {
			return &ReqClanLoad{}
		},
		func() interface{} {
			return &ResClanLoad{}
		})
	userLogic.BindCoderFac(CdClanUpNameOrSurname,
		func() interface{} {
			return &ReqClanUpNameOrSurname{}
		},
		func() interface{} {
			return &ResClanUpNameOrSurname{}
		})
	userLogic.BindCoderFac(CdClanUpDeclaration,
		func() interface{} {
			return &ReqClanUpDeclaration{}
		},
		func() interface{} {
			return &ResClanUpDeclaration{}
		})
	userLogic.BindCoderFac(CdNoticeClanChange,
		nil,
		func() interface{} {
			return &NoticeClanChange{}
		})
	userLogic.BindCoderFac(CdClanPageSearch,
		func() interface{} {
			return &ReqClanPageSearch{}
		},
		func() interface{} {
			return &ResClanPageSearch{}
		})
	userLogic.BindCoderFac(CdClanSignIn,
		func() interface{} {
			return &ReqClanSignIn{}
		},
		func() interface{} {
			return &ResClanSignIn{}
		})
	userLogic.BindCoderFac(CdClanUpIntroduction,
		func() interface{} {
			return &ReqClanUpIntroduction{}
		},
		func() interface{} {
			return &ResClanUpIntroduction{}
		})
	userLogic.BindCoderFac(CdClanUpBirthday,
		func() interface{} {
			return &ReqClanUpBirthday{}
		},
		func() interface{} {
			return &ResClanUpBirthday{}
		})
}
