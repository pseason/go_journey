[TOC]

---

# 服务端

## 项目定制的proto语法
示例：
```protobuf
// service枚举，已弃用，可以不写
// @ServiceEnum=CharacterInfo

// @MessageCode=100010002 角色创建
message ReqCharacterCreate {
  string nickname = 1;
  // 性别 1女 0男
  int32 gender = 2; // 性别
  int32 figure = 3; // 形象
}
```
具体：

用`// @ServiceEnum=xx`或`//@ServiceEnum=xx`或指定服务枚举名(定义在->`game/svc/`)，如
```go
// @ServiceEnum=CharacterInfo
//@ServiceEnum=CharacterInfo
```

用`// @MessageCode=xx`或`//@MessageCode=xx`指定消息编号，如
```go
// @MessageCode=100010002
//@MessageCode=100010002
```

==消息编号规则==
- 前五位：服务编号(服务枚举对应的编号，同样定义在->`game/svc/`)
- 后四位：自定义。一般按一定规律自增



# 客户端