package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdMails           MsgCode = 102001001 // Req：；Res：；
	CdNoticeMailNew   MsgCode = 102001002 // Notice：；
	CdMailOperate     MsgCode = 102001003 // Req：；Res：；
	CdUnReadMailCount MsgCode = 102001004 // Req：；Res：；
)

func InitCodecForMail(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdMails,
		func() interface{} {
			return &ReqMails{}
		},
		func() interface{} {
			return &ResMails{}
		})
	userLogic.BindCoderFac(CdNoticeMailNew,
		nil,
		func() interface{} {
			return &NoticeMailNew{}
		})
	userLogic.BindCoderFac(CdMailOperate,
		func() interface{} {
			return &ReqMailOperate{}
		},
		func() interface{} {
			return &ResMailOperate{}
		})
	userLogic.BindCoderFac(CdUnReadMailCount,
		func() interface{} {
			return &ReqUnReadMailCount{}
		},
		func() interface{} {
			return &ResUnReadMailCount{}
		})
}
