package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdCityEvent         MsgCode = 116001001 // Req：获取城邦事件列表；Res：返回城邦事件列表；
	CdCityRecord        MsgCode = 116001002 // Req：获取城邦记录列表；Res：返回城邦建筑记录列表；
	CdKingdomEvent      MsgCode = 116001003 // Req：获取王国事件列表；Res：返回王国事件列表；
	CdNoticeEventChange MsgCode = 116001004 // Notice：事件通知信息；
)

func InitCodecForEventManager(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdCityEvent,
		func() interface{} {
			return &ReqCityEvent{}
		},
		func() interface{} {
			return &ResCityEvent{}
		})
	userLogic.BindCoderFac(CdCityRecord,
		func() interface{} {
			return &ReqCityRecord{}
		},
		func() interface{} {
			return &ResCityRecord{}
		})
	userLogic.BindCoderFac(CdKingdomEvent,
		func() interface{} {
			return &ReqKingdomEvent{}
		},
		func() interface{} {
			return &ResKingdomEvent{}
		})
	userLogic.BindCoderFac(CdNoticeEventChange,
		nil,
		func() interface{} {
			return &NoticeEventChange{}
		})
}
