package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdInvestResources               MsgCode = 105001001 // Req：投入资源；Res：投入资源；
	CdInvestEnergyNum               MsgCode = 105001002 // Req：投入精力；Res：投入精力；
	CdBuildEnergyInfo               MsgCode = 105001003 // Req：请求当前建筑精力值；Res：响应当前建筑精力值；
	CdBuildResourceInfo             MsgCode = 105001004 // Req：打开建造面板时请求当前建筑资源信息；Res：打开建造面板时请求当前建筑资源信息；
	CdCityAllBuilding               MsgCode = 105001005 // Req：请求玩家城邦所有建筑；Res：请求玩家城邦所有建筑；
	CdNoticeCityBuildingStateChange MsgCode = 105001006 // Notice：建筑状态改变通知；
	CdBuildingInvestEnergy          MsgCode = 105001007 // Req：开始投入精力动作；Res：开始投入精力动作；
	CdNoticeInvestEnergy            MsgCode = 105001008 // Notice：开始投入精力动作通知；
)

func InitCodecForCitybuilding(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdInvestResources,
		func() interface{} {
			return &ReqInvestResources{}
		},
		func() interface{} {
			return &ResInvestResources{}
		})
	userLogic.BindCoderFac(CdInvestEnergyNum,
		func() interface{} {
			return &ReqInvestEnergyNum{}
		},
		func() interface{} {
			return &ResInvestEnergyNum{}
		})
	userLogic.BindCoderFac(CdBuildEnergyInfo,
		func() interface{} {
			return &ReqBuildEnergyInfo{}
		},
		func() interface{} {
			return &ResBuildEnergyInfo{}
		})
	userLogic.BindCoderFac(CdBuildResourceInfo,
		func() interface{} {
			return &ReqBuildResourceInfo{}
		},
		func() interface{} {
			return &ResBuildResourceInfo{}
		})
	userLogic.BindCoderFac(CdCityAllBuilding,
		func() interface{} {
			return &ReqCityAllBuilding{}
		},
		func() interface{} {
			return &ResCityAllBuilding{}
		})
	userLogic.BindCoderFac(CdNoticeCityBuildingStateChange,
		nil,
		func() interface{} {
			return &NoticeCityBuildingStateChange{}
		})
	userLogic.BindCoderFac(CdBuildingInvestEnergy,
		func() interface{} {
			return &ReqBuildingInvestEnergy{}
		},
		func() interface{} {
			return &ResBuildingInvestEnergy{}
		})
	userLogic.BindCoderFac(CdNoticeInvestEnergy,
		nil,
		func() interface{} {
			return &NoticeInvestEnergy{}
		})
}
