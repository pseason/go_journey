package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdPlotList         MsgCode = 113001001 // Req：剧情列表；Res：响应剧情列表；
	CdPlotChangeNotify MsgCode = 113001002 // Res：剧情改变通知；
	CdPlotExecute      MsgCode = 113001003 // Req：剧情执行；Res：响应剧情执行；
)

func InitCodecForPlot(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdPlotList,
		func() interface{} {
			return &ReqPlotList{}
		},
		func() interface{} {
			return &ResPlotList{}
		})
	userLogic.BindCoderFac(CdPlotChangeNotify,
		nil,
		func() interface{} {
			return &ResPlotChangeNotify{}
		})
	userLogic.BindCoderFac(CdPlotExecute,
		func() interface{} {
			return &ReqPlotExecute{}
		},
		func() interface{} {
			return &ResPlotExecute{}
		})
}
