package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdCharacterInfo             MsgCode = 100010000 // Req：根据uid获取角色信息(登陆时使用)；Res：根据uid返回角色信息(如果有角色信息则进行登录，否则客户端根据返回的id=0判断为新用户，再请求创建角色)；
	CdCharacterCreate           MsgCode = 100010001 // Req：角色创建；Res：角色创建-返回角色信息；
	CdNoticeCharacterInfoChange MsgCode = 100010002 // Notice：角色信息改变通知；
	CdCharacterTest             MsgCode = 100019001 // Req：测试用例；Res：测试用例；
)

func InitCodecForCharacterInfo(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdCharacterInfo,
		func() interface{} {
			return &ReqCharacterInfo{}
		},
		func() interface{} {
			return &ResCharacterInfo{}
		})
	userLogic.BindCoderFac(CdCharacterCreate,
		func() interface{} {
			return &ReqCharacterCreate{}
		},
		func() interface{} {
			return &ResCharacterCreate{}
		})
	userLogic.BindCoderFac(CdNoticeCharacterInfoChange,
		nil,
		func() interface{} {
			return &NoticeCharacterInfoChange{}
		})
	userLogic.BindCoderFac(CdCharacterTest,
		func() interface{} {
			return &ReqCharacterTest{}
		},
		func() interface{} {
			return &ResCharacterTest{}
		})
}
