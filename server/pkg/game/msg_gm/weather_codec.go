package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdWeather       MsgCode = 102001211 // Req：请求天气数据；Res：响应天气数据；
	CdNoticeWeather MsgCode = 102001212 // Notice：通知天气数据变更；
)

func InitCodecForWeather(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdWeather,
		func() interface{} {
			return &ReqWeather{}
		},
		func() interface{} {
			return &ResWeather{}
		})
	userLogic.BindCoderFac(CdNoticeWeather,
		nil,
		func() interface{} {
			return &NoticeWeather{}
		})
}
