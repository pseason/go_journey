package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdFarmInfo     MsgCode = 108001001 // Req：请求农场信息；Res：响应农场信息；
	CdFarmPutin    MsgCode = 108001002 // Req：请求种植或畜牧；Res：响应种植或畜牧；
	CdFarmWipe     MsgCode = 108001003 // Req：请求铲除；Res：响应铲除；
	CdCargoHarvest MsgCode = 108001004 // Req：请求仓库收获；Res：响应仓库收获；
	CdFarmInvest   MsgCode = 108001005 // Req：请求灌溉；Res：响应灌溉；
)

func InitCodecForCityfarm(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdFarmInfo,
		func() interface{} {
			return &ReqFarmInfo{}
		},
		func() interface{} {
			return &ResFarmInfo{}
		})
	userLogic.BindCoderFac(CdFarmPutin,
		func() interface{} {
			return &ReqFarmPutin{}
		},
		func() interface{} {
			return &ResFarmPutin{}
		})
	userLogic.BindCoderFac(CdFarmWipe,
		func() interface{} {
			return &ReqFarmWipe{}
		},
		func() interface{} {
			return &ResFarmWipe{}
		})
	userLogic.BindCoderFac(CdCargoHarvest,
		func() interface{} {
			return &ReqCargoHarvest{}
		},
		func() interface{} {
			return &ResCargoHarvest{}
		})
	userLogic.BindCoderFac(CdFarmInvest,
		func() interface{} {
			return &ReqFarmInvest{}
		},
		func() interface{} {
			return &ResFarmInvest{}
		})
}
