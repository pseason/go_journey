package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdNoticeSysError MsgCode = 650001001 // Notice：通知错误；
	CdSysAuth        MsgCode = 650001002 // Req：请求鉴权；Res：响应鉴权；
	CdSysReconnect   MsgCode = 650001003 // Req：请求重连；Res：响应重连；
)

func InitCodecForSys(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdNoticeSysError,
		nil,
		func() interface{} {
			return &NoticeSysError{}
		})
	userLogic.BindCoderFac(CdSysAuth,
		func() interface{} {
			return &ReqSysAuth{}
		},
		func() interface{} {
			return &ResSysAuth{}
		})
	userLogic.BindCoderFac(CdSysReconnect,
		func() interface{} {
			return &ReqSysReconnect{}
		},
		func() interface{} {
			return &ResSysReconnect{}
		})
}
