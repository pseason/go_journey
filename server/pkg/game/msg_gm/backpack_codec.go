package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdBackpack                   MsgCode = 102004001 // Req：背包数据；Res：返回-背包数据；
	CdBackpackDiscard            MsgCode = 102004002 // Req：背包扣除物品；Res：背包丢弃；
	CdBackpackArrange            MsgCode = 102004003 // Req：背包整理；Res：返回-背包整理；
	CdBackpackDivision           MsgCode = 102004004 // Req：背包拆分；Res：返回-背包拆分；
	CdBackpackUseProp            MsgCode = 102004005 // Req：使用物品；Res：返回-使用物品；
	CdBackpackMove               MsgCode = 102004008 // Req：背包物品移动；Res：返回-背包物品移动；
	CdBackpackSells              MsgCode = 102004009 // Req：出售物品列表；Res：返回-出售物品列表；
	CdNoticeBackpackGainNotify   MsgCode = 102004010 // Notice：物品变更主动通知；
	CdBackpackGiftOpen           MsgCode = 102004012 // Req：打开道具礼包；Res：返回-打开道具礼包；
	CdBackpackCompose            MsgCode = 102004013 // Req：材料合成；Res：材料合成；
	CdBackpackGoldExchange       MsgCode = 102004014 // Req：兑换金币；Res：兑换金币；
	CdNoticeBackpackSysAddNotify MsgCode = 102004016 // Notice：非背包功能的物品新增飘字；
	CdBackpackSubstrTry          MsgCode = 102004017 // Req：事务-背包扣减物品；Res：事务-背包扣减物品；
	CdBackpackIncreaseTry        MsgCode = 102004018 // Req：事务-背包增加物品；Res：事务-背包增加物品；
)

func InitCodecForBackpack(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdBackpack,
		func() interface{} {
			return &ReqBackpack{}
		},
		func() interface{} {
			return &ResBackpack{}
		})
	userLogic.BindCoderFac(CdBackpackDiscard,
		func() interface{} {
			return &ReqBackpackDiscard{}
		},
		func() interface{} {
			return &ResBackpackDiscard{}
		})
	userLogic.BindCoderFac(CdBackpackArrange,
		func() interface{} {
			return &ReqBackpackArrange{}
		},
		func() interface{} {
			return &ResBackpackArrange{}
		})
	userLogic.BindCoderFac(CdBackpackDivision,
		func() interface{} {
			return &ReqBackpackDivision{}
		},
		func() interface{} {
			return &ResBackpackDivision{}
		})
	userLogic.BindCoderFac(CdBackpackUseProp,
		func() interface{} {
			return &ReqBackpackUseProp{}
		},
		func() interface{} {
			return &ResBackpackUseProp{}
		})
	userLogic.BindCoderFac(CdBackpackMove,
		func() interface{} {
			return &ReqBackpackMove{}
		},
		func() interface{} {
			return &ResBackpackMove{}
		})
	userLogic.BindCoderFac(CdBackpackSells,
		func() interface{} {
			return &ReqBackpackSells{}
		},
		func() interface{} {
			return &ResBackpackSells{}
		})
	userLogic.BindCoderFac(CdNoticeBackpackGainNotify,
		nil,
		func() interface{} {
			return &NoticeBackpackGainNotify{}
		})
	userLogic.BindCoderFac(CdBackpackGiftOpen,
		func() interface{} {
			return &ReqBackpackGiftOpen{}
		},
		func() interface{} {
			return &ResBackpackGiftOpen{}
		})
	userLogic.BindCoderFac(CdBackpackCompose,
		func() interface{} {
			return &ReqBackpackCompose{}
		},
		func() interface{} {
			return &ResBackpackCompose{}
		})
	userLogic.BindCoderFac(CdBackpackGoldExchange,
		func() interface{} {
			return &ReqBackpackGoldExchange{}
		},
		func() interface{} {
			return &ResBackpackGoldExchange{}
		})
	userLogic.BindCoderFac(CdNoticeBackpackSysAddNotify,
		nil,
		func() interface{} {
			return &NoticeBackpackSysAddNotify{}
		})
	userLogic.BindCoderFac(CdBackpackSubstrTry,
		func() interface{} {
			return &ReqBackpackSubstrTry{}
		},
		func() interface{} {
			return &ResBackpackSubstrTry{}
		})
	userLogic.BindCoderFac(CdBackpackIncreaseTry,
		func() interface{} {
			return &ReqBackpackIncreaseTry{}
		},
		func() interface{} {
			return &ResBackpackIncreaseTry{}
		})
}
