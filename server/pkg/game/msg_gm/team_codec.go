package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdCreateTeam                     MsgCode = 115001001 // Req：创建队伍；Res：创建队伍；
	CdLoadGroup                      MsgCode = 115001002 // Req：加载自己的队伍信息；Res：加载自己的队伍信息；
	CdInviteJoinTeam                 MsgCode = 115001003 // Req：邀请加入；Res：邀请加入；
	CdHandelInvite                   MsgCode = 115001004 // Req：处理队伍邀请；Res：处理队伍邀请；
	CdPleaseLeaveTeam                MsgCode = 115001005 // Req：踢出队伍；Res：踢出队伍；
	CdDisbandTeam                    MsgCode = 115001006 // Req：解散队伍；Res：解散队伍；
	CdTransferTeam                   MsgCode = 115001007 // Req：转移队长；Res：转移队长；
	CdApplyTeam                      MsgCode = 115001008 // Req：申请加入队伍；Res：申请加入队伍；
	CdGroupApplyList                 MsgCode = 115001009 // Req：队长请求申请人列表；Res：队长请求申请人列表；
	CdLeaderApproval                 MsgCode = 115001010 // Req：队长审批加入操作；Res：队长审批加入操作；
	CdLeaveTeam                      MsgCode = 115001011 // Req：离开队伍；Res：离开队伍；
	CdGroupSearch                    MsgCode = 115001012 // Req：搜索队伍；Res：搜索队伍；
	CdSetTeamAims                    MsgCode = 115001013 // Req：队伍目标修改；Res：队伍目标修改；
	CdSetTeamAutoJoin                MsgCode = 115001014 // Req：队伍修改自动同意设置；Res：队伍修改自动同意设置；
	CdOneClickApply                  MsgCode = 115001015 // Req：一键申请；Res：一键申请；
	CdTeamMemberPositionExchange     MsgCode = 115001016 // Req：队伍成员位置交换；Res：队伍成员位置交换；
	CdBuddyTeam                      MsgCode = 115001017 // Req：请求好友组队；Res：请求好友组队；
	CdGroupChange                    MsgCode = 115001031 // Res：队伍成员变更通知；
	CdAddMemberChange                MsgCode = 115001032 // Res：添加新成员通知；
	CdInvateGroupNotify              MsgCode = 115001033 // Res：邀请加入队伍  队员收到通知；
	CdAddGroupNotify                 MsgCode = 115001034 // Res：申请加入队伍  队长收到通知；
	CdNoticeMemberAttrChange         MsgCode = 115001035 // Notice：队员属性改变通知；
	CdTeamAimsChangeNotify           MsgCode = 115001036 // Res：队员目标改变通知；
	CdTeamAutoAgreeChangeNotify      MsgCode = 115001037 // Res：队伍自动同意改变通知；
	CdNoticeTeamMemberPositionChange MsgCode = 115001038 // Notice：队伍成员位置改变通知；
)

func InitCodecForTeam(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdCreateTeam,
		func() interface{} {
			return &ReqCreateTeam{}
		},
		func() interface{} {
			return &ResCreateTeam{}
		})
	userLogic.BindCoderFac(CdLoadGroup,
		func() interface{} {
			return &ReqLoadGroup{}
		},
		func() interface{} {
			return &ResLoadGroup{}
		})
	userLogic.BindCoderFac(CdInviteJoinTeam,
		func() interface{} {
			return &ReqInviteJoinTeam{}
		},
		func() interface{} {
			return &ResInviteJoinTeam{}
		})
	userLogic.BindCoderFac(CdHandelInvite,
		func() interface{} {
			return &ReqHandelInvite{}
		},
		func() interface{} {
			return &ResHandelInvite{}
		})
	userLogic.BindCoderFac(CdPleaseLeaveTeam,
		func() interface{} {
			return &ReqPleaseLeaveTeam{}
		},
		func() interface{} {
			return &ResPleaseLeaveTeam{}
		})
	userLogic.BindCoderFac(CdDisbandTeam,
		func() interface{} {
			return &ReqDisbandTeam{}
		},
		func() interface{} {
			return &ResDisbandTeam{}
		})
	userLogic.BindCoderFac(CdTransferTeam,
		func() interface{} {
			return &ReqTransferTeam{}
		},
		func() interface{} {
			return &ResTransferTeam{}
		})
	userLogic.BindCoderFac(CdApplyTeam,
		func() interface{} {
			return &ReqApplyTeam{}
		},
		func() interface{} {
			return &ResApplyTeam{}
		})
	userLogic.BindCoderFac(CdGroupApplyList,
		func() interface{} {
			return &ReqGroupApplyList{}
		},
		func() interface{} {
			return &ResGroupApplyList{}
		})
	userLogic.BindCoderFac(CdLeaderApproval,
		func() interface{} {
			return &ReqLeaderApproval{}
		},
		func() interface{} {
			return &ResLeaderApproval{}
		})
	userLogic.BindCoderFac(CdLeaveTeam,
		func() interface{} {
			return &ReqLeaveTeam{}
		},
		func() interface{} {
			return &ResLeaveTeam{}
		})
	userLogic.BindCoderFac(CdGroupSearch,
		func() interface{} {
			return &ReqGroupSearch{}
		},
		func() interface{} {
			return &ResGroupSearch{}
		})
	userLogic.BindCoderFac(CdSetTeamAims,
		func() interface{} {
			return &ReqSetTeamAims{}
		},
		func() interface{} {
			return &ResSetTeamAims{}
		})
	userLogic.BindCoderFac(CdSetTeamAutoJoin,
		func() interface{} {
			return &ReqSetTeamAutoJoin{}
		},
		func() interface{} {
			return &ResSetTeamAutoJoin{}
		})
	userLogic.BindCoderFac(CdOneClickApply,
		func() interface{} {
			return &ReqOneClickApply{}
		},
		func() interface{} {
			return &ResOneClickApply{}
		})
	userLogic.BindCoderFac(CdTeamMemberPositionExchange,
		func() interface{} {
			return &ReqTeamMemberPositionExchange{}
		},
		func() interface{} {
			return &ResTeamMemberPositionExchange{}
		})
	userLogic.BindCoderFac(CdBuddyTeam,
		func() interface{} {
			return &ReqBuddyTeam{}
		},
		func() interface{} {
			return &ResBuddyTeam{}
		})
	userLogic.BindCoderFac(CdGroupChange,
		nil,
		func() interface{} {
			return &ResGroupChange{}
		})
	userLogic.BindCoderFac(CdAddMemberChange,
		nil,
		func() interface{} {
			return &ResAddMemberChange{}
		})
	userLogic.BindCoderFac(CdInvateGroupNotify,
		nil,
		func() interface{} {
			return &ResInvateGroupNotify{}
		})
	userLogic.BindCoderFac(CdAddGroupNotify,
		nil,
		func() interface{} {
			return &ResAddGroupNotify{}
		})
	userLogic.BindCoderFac(CdNoticeMemberAttrChange,
		nil,
		func() interface{} {
			return &NoticeMemberAttrChange{}
		})
	userLogic.BindCoderFac(CdTeamAimsChangeNotify,
		nil,
		func() interface{} {
			return &ResTeamAimsChangeNotify{}
		})
	userLogic.BindCoderFac(CdTeamAutoAgreeChangeNotify,
		nil,
		func() interface{} {
			return &ResTeamAutoAgreeChangeNotify{}
		})
	userLogic.BindCoderFac(CdNoticeTeamMemberPositionChange,
		nil,
		func() interface{} {
			return &NoticeTeamMemberPositionChange{}
		})
}
