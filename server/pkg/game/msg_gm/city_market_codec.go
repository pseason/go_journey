package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdMarket               MsgCode = 102004501 // Req：市场商品列表；Res：返回-市场商品列表；
	CdMarketSellingRecords MsgCode = 102004502 // Req：市场我的商品列表；Res：返回-市场我的商品列表；
	CdMarketPutInto        MsgCode = 102004503 // Req：商品上架-客户端只允许单个上架；Res：返回-商品上架；
	CdMarketRemove         MsgCode = 102004504 // Req：商品下架；Res：返回-商品下架；
	CdMarketBuy            MsgCode = 102004505 // Req：商品购买；Res：返回-商品购买；
)

func InitCodecForCityMarket(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdMarket,
		func() interface{} {
			return &ReqMarket{}
		},
		func() interface{} {
			return &ResMarket{}
		})
	userLogic.BindCoderFac(CdMarketSellingRecords,
		func() interface{} {
			return &ReqMarketSellingRecords{}
		},
		func() interface{} {
			return &ResMarketSellingRecords{}
		})
	userLogic.BindCoderFac(CdMarketPutInto,
		func() interface{} {
			return &ReqMarketPutInto{}
		},
		func() interface{} {
			return &ResMarketPutInto{}
		})
	userLogic.BindCoderFac(CdMarketRemove,
		func() interface{} {
			return &ReqMarketRemove{}
		},
		func() interface{} {
			return &ResMarketRemove{}
		})
	userLogic.BindCoderFac(CdMarketBuy,
		func() interface{} {
			return &ReqMarketBuy{}
		},
		func() interface{} {
			return &ResMarketBuy{}
		})
}
