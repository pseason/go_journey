package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	Cd MsgCode = 100020001 //
)

func InitCodecForCharacterAttr(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(Cd,
		nil,
		nil)
}
