package err_gm

import (
	"95eh.com/eg/utils"
	"hm/pkg/game/svc"
)

/*
@Time   : 2021-11-12 17:40
@Author : wushu
@DESC   : 定义错误消息响应码
	errCode在整个节点中是唯一的，以服务号作为前五位，后四位自增(但要注意iota不要超过10000！)

	todo 这里是响应给客户端的错误码，所以不应该过于具体，定义的应当尽量地少
		通过脚本可生成错误码描述文件。错误常量的上一行注释和行尾注释会被识别为错误描述
*/

type ErrCode = utils.TErrCode

// todo 通用的 (应当尽量用通用的，这几个目前来说够用了)
const (
	// 查询失败
	ErrQueryFailed = ErrCode(uint32(svc.Sys)*10000) + iota
	// 创建失败
	ErrCreateFailed
	// 修改失败
	ErrUpdateFailed
	// 删除失败
	ErrDelFailed
	// 鉴权失败
	ErrAuthFailed
)

// characterInfo
const (
	// 昵称被占用
	ErrCharacterNicknameBeingUse = ErrCode(uint32(svc.CharacterInfo)*10000) + iota
)

const (
	ErrSpawnScene = ErrCode(uint32(svc.Space)) + iota
	ErrEnterScene
	ErrExitScene
	ErrMoveStart
	ErrMoveStop
	ErrChangePos
	ErrGather
)

// social
const (
	// 没有此名称的玩家
	ErrNotNameTheCharacter = ErrCode(uint32(svc.Social)*10000) + iota
)

//market
const (
	//物品出售已达建筑上线
	ErrOverLimit = ErrCode(uint32(svc.CityMarket)*10000) + iota
)