package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdUnlockCurrentInfo  MsgCode = 101001001 // Req：获取当前解锁状态信息；Res：响应-获取当前解锁状态信息；
	CdUnlockChangeNotify MsgCode = 101001002 // Res：解锁状态信息改变通知；
)

func InitCodecForUnlock(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdUnlockCurrentInfo,
		func() interface{} {
			return &ReqUnlockCurrentInfo{}
		},
		func() interface{} {
			return &ResUnlockCurrentInfo{}
		})
	userLogic.BindCoderFac(CdUnlockChangeNotify,
		nil,
		func() interface{} {
			return &ResUnlockChangeNotify{}
		})
}
