package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdTaskList         MsgCode = 112001001 // Req：任务列表；Res：响应任务列表；
	CdTaskChangeNotify MsgCode = 112001002 // Res：任务改变通知；
	CdTaskActiveSubmit MsgCode = 112001003 // Req：主动提交；Res：主动提交-完成；
	CdTaskReceiveAward MsgCode = 112001004 // Req：领取任务奖励；Res：领取任务奖励-完成；
	CdTaskToggleHidden MsgCode = 112001005 // Req：改变隐藏状态；Res：改变隐藏状态-完成；
)

func InitCodecForTask(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdTaskList,
		func() interface{} {
			return &ReqTaskList{}
		},
		func() interface{} {
			return &ResTaskList{}
		})
	userLogic.BindCoderFac(CdTaskChangeNotify,
		nil,
		func() interface{} {
			return &ResTaskChangeNotify{}
		})
	userLogic.BindCoderFac(CdTaskActiveSubmit,
		func() interface{} {
			return &ReqTaskActiveSubmit{}
		},
		func() interface{} {
			return &ResTaskActiveSubmit{}
		})
	userLogic.BindCoderFac(CdTaskReceiveAward,
		func() interface{} {
			return &ReqTaskReceiveAward{}
		},
		func() interface{} {
			return &ResTaskReceiveAward{}
		})
	userLogic.BindCoderFac(CdTaskToggleHidden,
		func() interface{} {
			return &ReqTaskToggleHidden{}
		},
		func() interface{} {
			return &ResTaskToggleHidden{}
		})
}
