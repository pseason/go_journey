package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdSpaceMoveStart             MsgCode = 601001001 // Req：移动开始；Res：移动开始；
	CdNoticeSpaceMoveStart       MsgCode = 601001002 // Notice：移动开始事件；
	CdSpaceMoveStop              MsgCode = 601001003 // Req：移动停止；Res：移动停止事件；
	CdNoticeSpaceStopMove        MsgCode = 601001004 // Notice：移动停止；
	CdNoticeSpaceVisible         MsgCode = 601001005 // Notice：可见事件；
	CdNoticeSpaceInvisible       MsgCode = 601001006 // Notice：不可见事件；
	CdSpaceEnterScene            MsgCode = 601001007 // Req：请求进入场景；Res：响应进入场景；
	CdNoticeSpaceEnterScene      MsgCode = 601001008 // Notice：通知进入场景；
	CdSpaceExitCurrentScene      MsgCode = 601001009 // Req：请求退出当前场景；Res：响应退出当前场景；
	CdSpaceChangePos             MsgCode = 601001010 // Req：测试请求更改位置；Res：测试响应更改位置；
	CdNoticeActorInfoChange      MsgCode = 601001011 // Notice：对象信息改变通知；
	CdSpaceGather                MsgCode = 601001012 // Req：采集资源；Res：采集资源；
	CdNoticeSpaceGatherChange    MsgCode = 601001013 // Notice：资源采集改变通知；
	CdSpaceAction                MsgCode = 601001014 // Req：请求开始动作；Res：响应开始动作；
	CdNoticeActorBroadcastAction MsgCode = 601001015 // Notice：对象信息动作广播；
	CdNoticeSpaceResourceState   MsgCode = 601001016 // Notice：静态资源状态；
	CdNoticeSpaceChangePos       MsgCode = 601001017 // Notice：对象改变位置；
)

func InitCodecForSpace(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdSpaceMoveStart,
		func() interface{} {
			return &ReqSpaceMoveStart{}
		},
		func() interface{} {
			return &ResSpaceMoveStart{}
		})
	userLogic.BindCoderFac(CdNoticeSpaceMoveStart,
		nil,
		func() interface{} {
			return &NoticeSpaceMoveStart{}
		})
	userLogic.BindCoderFac(CdSpaceMoveStop,
		func() interface{} {
			return &ReqSpaceMoveStop{}
		},
		func() interface{} {
			return &ResSpaceMoveStop{}
		})
	userLogic.BindCoderFac(CdNoticeSpaceStopMove,
		nil,
		func() interface{} {
			return &NoticeSpaceStopMove{}
		})
	userLogic.BindCoderFac(CdNoticeSpaceVisible,
		nil,
		func() interface{} {
			return &NoticeSpaceVisible{}
		})
	userLogic.BindCoderFac(CdNoticeSpaceInvisible,
		nil,
		func() interface{} {
			return &NoticeSpaceInvisible{}
		})
	userLogic.BindCoderFac(CdSpaceEnterScene,
		func() interface{} {
			return &ReqSpaceEnterScene{}
		},
		func() interface{} {
			return &ResSpaceEnterScene{}
		})
	userLogic.BindCoderFac(CdNoticeSpaceEnterScene,
		nil,
		func() interface{} {
			return &NoticeSpaceEnterScene{}
		})
	userLogic.BindCoderFac(CdSpaceExitCurrentScene,
		func() interface{} {
			return &ReqSpaceExitCurrentScene{}
		},
		func() interface{} {
			return &ResSpaceExitCurrentScene{}
		})
	userLogic.BindCoderFac(CdSpaceChangePos,
		func() interface{} {
			return &ReqSpaceChangePos{}
		},
		func() interface{} {
			return &ResSpaceChangePos{}
		})
	userLogic.BindCoderFac(CdNoticeActorInfoChange,
		nil,
		func() interface{} {
			return &NoticeActorInfoChange{}
		})
	userLogic.BindCoderFac(CdSpaceGather,
		func() interface{} {
			return &ReqSpaceGather{}
		},
		func() interface{} {
			return &ResSpaceGather{}
		})
	userLogic.BindCoderFac(CdNoticeSpaceGatherChange,
		nil,
		func() interface{} {
			return &NoticeSpaceGatherChange{}
		})
	userLogic.BindCoderFac(CdSpaceAction,
		func() interface{} {
			return &ReqSpaceAction{}
		},
		func() interface{} {
			return &ResSpaceAction{}
		})
	userLogic.BindCoderFac(CdNoticeActorBroadcastAction,
		nil,
		func() interface{} {
			return &NoticeActorBroadcastAction{}
		})
	userLogic.BindCoderFac(CdNoticeSpaceResourceState,
		nil,
		func() interface{} {
			return &NoticeSpaceResourceState{}
		})
	userLogic.BindCoderFac(CdNoticeSpaceChangePos,
		nil,
		func() interface{} {
			return &NoticeSpaceChangePos{}
		})
}
