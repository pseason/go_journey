package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdShortcutBar        MsgCode = 102004101 // Req：快捷栏数据；Res：返回-快捷栏数据；
	CdShortcutBarPutInto MsgCode = 102004102 // Req：放入快捷栏；Res：返回-放入快捷栏；
	CdShortcutBarRemove  MsgCode = 102004103 // Req：从快捷栏移除；Res：返回-从快捷栏移除；
)

func InitCodecForBackpackShortcutBar(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdShortcutBar,
		func() interface{} {
			return &ReqShortcutBar{}
		},
		func() interface{} {
			return &ResShortcutBar{}
		})
	userLogic.BindCoderFac(CdShortcutBarPutInto,
		func() interface{} {
			return &ReqShortcutBarPutInto{}
		},
		func() interface{} {
			return &ResShortcutBarPutInto{}
		})
	userLogic.BindCoderFac(CdShortcutBarRemove,
		func() interface{} {
			return &ReqShortcutBarRemove{}
		},
		func() interface{} {
			return &ResShortcutBarRemove{}
		})
}
