// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: testTx.proto

package msg_gm

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

// @MessageCode=651000000 测试事务
type ReqTestTry struct {
	PropId int32 `protobuf:"varint,1,opt,name=PropId,proto3" json:"PropId,omitempty"`
	Count  int32 `protobuf:"varint,2,opt,name=Count,proto3" json:"Count,omitempty"`
}

func (m *ReqTestTry) Reset()         { *m = ReqTestTry{} }
func (m *ReqTestTry) String() string { return proto.CompactTextString(m) }
func (*ReqTestTry) ProtoMessage()    {}
func (*ReqTestTry) Descriptor() ([]byte, []int) {
	return fileDescriptor_c1cd9cddaf22c589, []int{0}
}
func (m *ReqTestTry) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReqTestTry) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReqTestTry.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReqTestTry) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReqTestTry.Merge(m, src)
}
func (m *ReqTestTry) XXX_Size() int {
	return m.Size()
}
func (m *ReqTestTry) XXX_DiscardUnknown() {
	xxx_messageInfo_ReqTestTry.DiscardUnknown(m)
}

var xxx_messageInfo_ReqTestTry proto.InternalMessageInfo

func (m *ReqTestTry) GetPropId() int32 {
	if m != nil {
		return m.PropId
	}
	return 0
}

func (m *ReqTestTry) GetCount() int32 {
	if m != nil {
		return m.Count
	}
	return 0
}

// @MessageCode=651000000 测试事务
type ResTestTry struct {
}

func (m *ResTestTry) Reset()         { *m = ResTestTry{} }
func (m *ResTestTry) String() string { return proto.CompactTextString(m) }
func (*ResTestTry) ProtoMessage()    {}
func (*ResTestTry) Descriptor() ([]byte, []int) {
	return fileDescriptor_c1cd9cddaf22c589, []int{1}
}
func (m *ResTestTry) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ResTestTry) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ResTestTry.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ResTestTry) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ResTestTry.Merge(m, src)
}
func (m *ResTestTry) XXX_Size() int {
	return m.Size()
}
func (m *ResTestTry) XXX_DiscardUnknown() {
	xxx_messageInfo_ResTestTry.DiscardUnknown(m)
}

var xxx_messageInfo_ResTestTry proto.InternalMessageInfo

func init() {
	proto.RegisterType((*ReqTestTry)(nil), "proto.ReqTestTry")
	proto.RegisterType((*ResTestTry)(nil), "proto.ResTestTry")
}

func init() { proto.RegisterFile("testTx.proto", fileDescriptor_c1cd9cddaf22c589) }

var fileDescriptor_c1cd9cddaf22c589 = []byte{
	// 147 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x29, 0x49, 0x2d, 0x2e,
	0x09, 0xa9, 0xd0, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x05, 0x53, 0x4a, 0x56, 0x5c, 0x5c,
	0x41, 0xa9, 0x85, 0x21, 0x20, 0x99, 0xa2, 0x4a, 0x21, 0x31, 0x2e, 0xb6, 0x80, 0xa2, 0xfc, 0x02,
	0xcf, 0x14, 0x09, 0x46, 0x05, 0x46, 0x0d, 0xd6, 0x20, 0x28, 0x4f, 0x48, 0x84, 0x8b, 0xd5, 0x39,
	0xbf, 0x34, 0xaf, 0x44, 0x82, 0x09, 0x2c, 0x0c, 0xe1, 0x28, 0xf1, 0x80, 0xf4, 0x16, 0x43, 0xf5,
	0x3a, 0x19, 0x9e, 0x78, 0x24, 0xc7, 0x78, 0xe1, 0x91, 0x1c, 0xe3, 0x83, 0x47, 0x72, 0x8c, 0x13,
	0x1e, 0xcb, 0x31, 0x5c, 0x78, 0x2c, 0xc7, 0x70, 0xe3, 0xb1, 0x1c, 0x43, 0x14, 0x5b, 0x6e, 0x71,
	0x7a, 0x7c, 0x7a, 0xee, 0x2a, 0x26, 0xbe, 0xe4, 0xfc, 0x5c, 0xbd, 0xf4, 0xc4, 0xdc, 0x54, 0x88,
	0x1b, 0x92, 0xd8, 0xc0, 0x94, 0x31, 0x20, 0x00, 0x00, 0xff, 0xff, 0x23, 0x5f, 0x2b, 0x28, 0x9a,
	0x00, 0x00, 0x00,
}

func (m *ReqTestTry) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReqTestTry) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReqTestTry) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.Count != 0 {
		i = encodeVarintTestTx(dAtA, i, uint64(m.Count))
		i--
		dAtA[i] = 0x10
	}
	if m.PropId != 0 {
		i = encodeVarintTestTx(dAtA, i, uint64(m.PropId))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func (m *ResTestTry) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ResTestTry) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ResTestTry) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	return len(dAtA) - i, nil
}

func encodeVarintTestTx(dAtA []byte, offset int, v uint64) int {
	offset -= sovTestTx(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *ReqTestTry) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.PropId != 0 {
		n += 1 + sovTestTx(uint64(m.PropId))
	}
	if m.Count != 0 {
		n += 1 + sovTestTx(uint64(m.Count))
	}
	return n
}

func (m *ResTestTry) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	return n
}

func sovTestTx(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozTestTx(x uint64) (n int) {
	return sovTestTx(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *ReqTestTry) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowTestTx
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReqTestTry: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReqTestTry: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field PropId", wireType)
			}
			m.PropId = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowTestTx
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.PropId |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Count", wireType)
			}
			m.Count = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowTestTx
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Count |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipTestTx(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthTestTx
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthTestTx
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ResTestTry) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowTestTx
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ResTestTry: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ResTestTry: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		default:
			iNdEx = preIndex
			skippy, err := skipTestTx(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthTestTx
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthTestTx
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipTestTx(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowTestTx
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowTestTx
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowTestTx
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthTestTx
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupTestTx
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthTestTx
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthTestTx        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowTestTx          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupTestTx = fmt.Errorf("proto: unexpected end of group")
)
