package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdTestTry MsgCode = 651000000 // Req：测试事务；Res：测试事务；
)

func InitCodecForTesttx(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdTestTry,
		func() interface{} {
			return &ReqTestTry{}
		},
		func() interface{} {
			return &ResTestTry{}
		})
}
