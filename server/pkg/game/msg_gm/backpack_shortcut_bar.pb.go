// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: backpack_shortcut_bar.proto

package msg_gm

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

//@MessageCode=102004101 快捷栏数据
type ReqShortcutBar struct {
}

func (m *ReqShortcutBar) Reset()         { *m = ReqShortcutBar{} }
func (m *ReqShortcutBar) String() string { return proto.CompactTextString(m) }
func (*ReqShortcutBar) ProtoMessage()    {}
func (*ReqShortcutBar) Descriptor() ([]byte, []int) {
	return fileDescriptor_ac554ef744d8989d, []int{0}
}
func (m *ReqShortcutBar) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReqShortcutBar) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReqShortcutBar.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReqShortcutBar) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReqShortcutBar.Merge(m, src)
}
func (m *ReqShortcutBar) XXX_Size() int {
	return m.Size()
}
func (m *ReqShortcutBar) XXX_DiscardUnknown() {
	xxx_messageInfo_ReqShortcutBar.DiscardUnknown(m)
}

var xxx_messageInfo_ReqShortcutBar proto.InternalMessageInfo

//@MessageCode=102004101 返回-快捷栏数据
type ResShortcutBar struct {
	Ds []int32 `protobuf:"varint,1,rep,packed,name=ds,proto3" json:"ds,omitempty"`
	Cd []int64 `protobuf:"varint,2,rep,packed,name=cd,proto3" json:"cd,omitempty"`
}

func (m *ResShortcutBar) Reset()         { *m = ResShortcutBar{} }
func (m *ResShortcutBar) String() string { return proto.CompactTextString(m) }
func (*ResShortcutBar) ProtoMessage()    {}
func (*ResShortcutBar) Descriptor() ([]byte, []int) {
	return fileDescriptor_ac554ef744d8989d, []int{1}
}
func (m *ResShortcutBar) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ResShortcutBar) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ResShortcutBar.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ResShortcutBar) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ResShortcutBar.Merge(m, src)
}
func (m *ResShortcutBar) XXX_Size() int {
	return m.Size()
}
func (m *ResShortcutBar) XXX_DiscardUnknown() {
	xxx_messageInfo_ResShortcutBar.DiscardUnknown(m)
}

var xxx_messageInfo_ResShortcutBar proto.InternalMessageInfo

func (m *ResShortcutBar) GetDs() []int32 {
	if m != nil {
		return m.Ds
	}
	return nil
}

func (m *ResShortcutBar) GetCd() []int64 {
	if m != nil {
		return m.Cd
	}
	return nil
}

//@MessageCode=102004102 放入快捷栏
type ReqShortcutBarPutInto struct {
	PropId int32 `protobuf:"varint,1,opt,name=propId,proto3" json:"propId,omitempty"`
	Index  int32 `protobuf:"varint,2,opt,name=index,proto3" json:"index,omitempty"`
}

func (m *ReqShortcutBarPutInto) Reset()         { *m = ReqShortcutBarPutInto{} }
func (m *ReqShortcutBarPutInto) String() string { return proto.CompactTextString(m) }
func (*ReqShortcutBarPutInto) ProtoMessage()    {}
func (*ReqShortcutBarPutInto) Descriptor() ([]byte, []int) {
	return fileDescriptor_ac554ef744d8989d, []int{2}
}
func (m *ReqShortcutBarPutInto) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReqShortcutBarPutInto) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReqShortcutBarPutInto.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReqShortcutBarPutInto) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReqShortcutBarPutInto.Merge(m, src)
}
func (m *ReqShortcutBarPutInto) XXX_Size() int {
	return m.Size()
}
func (m *ReqShortcutBarPutInto) XXX_DiscardUnknown() {
	xxx_messageInfo_ReqShortcutBarPutInto.DiscardUnknown(m)
}

var xxx_messageInfo_ReqShortcutBarPutInto proto.InternalMessageInfo

func (m *ReqShortcutBarPutInto) GetPropId() int32 {
	if m != nil {
		return m.PropId
	}
	return 0
}

func (m *ReqShortcutBarPutInto) GetIndex() int32 {
	if m != nil {
		return m.Index
	}
	return 0
}

//@MessageCode=102004102 返回-放入快捷栏
type ResShortcutBarPutInto struct {
	Ds []int32 `protobuf:"varint,1,rep,packed,name=ds,proto3" json:"ds,omitempty"`
	Cd []int64 `protobuf:"varint,2,rep,packed,name=cd,proto3" json:"cd,omitempty"`
}

func (m *ResShortcutBarPutInto) Reset()         { *m = ResShortcutBarPutInto{} }
func (m *ResShortcutBarPutInto) String() string { return proto.CompactTextString(m) }
func (*ResShortcutBarPutInto) ProtoMessage()    {}
func (*ResShortcutBarPutInto) Descriptor() ([]byte, []int) {
	return fileDescriptor_ac554ef744d8989d, []int{3}
}
func (m *ResShortcutBarPutInto) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ResShortcutBarPutInto) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ResShortcutBarPutInto.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ResShortcutBarPutInto) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ResShortcutBarPutInto.Merge(m, src)
}
func (m *ResShortcutBarPutInto) XXX_Size() int {
	return m.Size()
}
func (m *ResShortcutBarPutInto) XXX_DiscardUnknown() {
	xxx_messageInfo_ResShortcutBarPutInto.DiscardUnknown(m)
}

var xxx_messageInfo_ResShortcutBarPutInto proto.InternalMessageInfo

func (m *ResShortcutBarPutInto) GetDs() []int32 {
	if m != nil {
		return m.Ds
	}
	return nil
}

func (m *ResShortcutBarPutInto) GetCd() []int64 {
	if m != nil {
		return m.Cd
	}
	return nil
}

//@MessageCode=102004103 从快捷栏移除
type ReqShortcutBarRemove struct {
	Index int32 `protobuf:"varint,1,opt,name=index,proto3" json:"index,omitempty"`
}

func (m *ReqShortcutBarRemove) Reset()         { *m = ReqShortcutBarRemove{} }
func (m *ReqShortcutBarRemove) String() string { return proto.CompactTextString(m) }
func (*ReqShortcutBarRemove) ProtoMessage()    {}
func (*ReqShortcutBarRemove) Descriptor() ([]byte, []int) {
	return fileDescriptor_ac554ef744d8989d, []int{4}
}
func (m *ReqShortcutBarRemove) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ReqShortcutBarRemove) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ReqShortcutBarRemove.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ReqShortcutBarRemove) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReqShortcutBarRemove.Merge(m, src)
}
func (m *ReqShortcutBarRemove) XXX_Size() int {
	return m.Size()
}
func (m *ReqShortcutBarRemove) XXX_DiscardUnknown() {
	xxx_messageInfo_ReqShortcutBarRemove.DiscardUnknown(m)
}

var xxx_messageInfo_ReqShortcutBarRemove proto.InternalMessageInfo

func (m *ReqShortcutBarRemove) GetIndex() int32 {
	if m != nil {
		return m.Index
	}
	return 0
}

//@MessageCode=102004103 返回-从快捷栏移除
type ResShortcutBarRemove struct {
	Ds []int32 `protobuf:"varint,1,rep,packed,name=ds,proto3" json:"ds,omitempty"`
}

func (m *ResShortcutBarRemove) Reset()         { *m = ResShortcutBarRemove{} }
func (m *ResShortcutBarRemove) String() string { return proto.CompactTextString(m) }
func (*ResShortcutBarRemove) ProtoMessage()    {}
func (*ResShortcutBarRemove) Descriptor() ([]byte, []int) {
	return fileDescriptor_ac554ef744d8989d, []int{5}
}
func (m *ResShortcutBarRemove) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ResShortcutBarRemove) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ResShortcutBarRemove.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ResShortcutBarRemove) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ResShortcutBarRemove.Merge(m, src)
}
func (m *ResShortcutBarRemove) XXX_Size() int {
	return m.Size()
}
func (m *ResShortcutBarRemove) XXX_DiscardUnknown() {
	xxx_messageInfo_ResShortcutBarRemove.DiscardUnknown(m)
}

var xxx_messageInfo_ResShortcutBarRemove proto.InternalMessageInfo

func (m *ResShortcutBarRemove) GetDs() []int32 {
	if m != nil {
		return m.Ds
	}
	return nil
}

func init() {
	proto.RegisterType((*ReqShortcutBar)(nil), "proto.ReqShortcutBar")
	proto.RegisterType((*ResShortcutBar)(nil), "proto.ResShortcutBar")
	proto.RegisterType((*ReqShortcutBarPutInto)(nil), "proto.ReqShortcutBarPutInto")
	proto.RegisterType((*ResShortcutBarPutInto)(nil), "proto.ResShortcutBarPutInto")
	proto.RegisterType((*ReqShortcutBarRemove)(nil), "proto.ReqShortcutBarRemove")
	proto.RegisterType((*ResShortcutBarRemove)(nil), "proto.ResShortcutBarRemove")
}

func init() { proto.RegisterFile("backpack_shortcut_bar.proto", fileDescriptor_ac554ef744d8989d) }

var fileDescriptor_ac554ef744d8989d = []byte{
	// 241 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x4e, 0x4a, 0x4c, 0xce,
	0x2e, 0x48, 0x4c, 0xce, 0x8e, 0x2f, 0xce, 0xc8, 0x2f, 0x2a, 0x49, 0x2e, 0x2d, 0x89, 0x4f, 0x4a,
	0x2c, 0xd2, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x05, 0x53, 0x4a, 0x02, 0x5c, 0x7c, 0x41,
	0xa9, 0x85, 0xc1, 0x50, 0x79, 0xa7, 0xc4, 0x22, 0x25, 0x03, 0x90, 0x48, 0x31, 0x92, 0x88, 0x10,
	0x1f, 0x17, 0x53, 0x4a, 0xb1, 0x04, 0xa3, 0x02, 0xb3, 0x06, 0x6b, 0x10, 0x53, 0x4a, 0x31, 0x88,
	0x9f, 0x9c, 0x22, 0xc1, 0xa4, 0xc0, 0xac, 0xc1, 0x1c, 0xc4, 0x94, 0x9c, 0xa2, 0xe4, 0xca, 0x25,
	0x8a, 0x6a, 0x46, 0x40, 0x69, 0x89, 0x67, 0x5e, 0x49, 0xbe, 0x90, 0x18, 0x17, 0x5b, 0x41, 0x51,
	0x7e, 0x81, 0x67, 0x8a, 0x04, 0xa3, 0x02, 0xa3, 0x06, 0x6b, 0x10, 0x94, 0x27, 0x24, 0xc2, 0xc5,
	0x9a, 0x99, 0x97, 0x92, 0x5a, 0x21, 0xc1, 0x04, 0x16, 0x86, 0x70, 0x94, 0xcc, 0x41, 0xc6, 0x14,
	0x63, 0x31, 0x86, 0x90, 0xfd, 0x3a, 0x5c, 0x22, 0xa8, 0xf6, 0x07, 0xa5, 0xe6, 0xe6, 0x97, 0xa5,
	0x22, 0xac, 0x61, 0x44, 0xb6, 0x46, 0x0d, 0xa4, 0xba, 0x18, 0x53, 0x35, 0x9a, 0x2d, 0x4e, 0x86,
	0x27, 0x1e, 0xc9, 0x31, 0x5e, 0x78, 0x24, 0xc7, 0xf8, 0xe0, 0x91, 0x1c, 0xe3, 0x84, 0xc7, 0x72,
	0x0c, 0x17, 0x1e, 0xcb, 0x31, 0xdc, 0x78, 0x2c, 0xc7, 0x10, 0xc5, 0x96, 0x5b, 0x9c, 0x1e, 0x9f,
	0x9e, 0xbb, 0x8a, 0x89, 0x2f, 0x39, 0x3f, 0x57, 0x2f, 0x3d, 0x31, 0x37, 0x15, 0x12, 0xa6, 0x49,
	0x6c, 0x60, 0xca, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0x52, 0x46, 0xda, 0xa5, 0x79, 0x01, 0x00,
	0x00,
}

func (m *ReqShortcutBar) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReqShortcutBar) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReqShortcutBar) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	return len(dAtA) - i, nil
}

func (m *ResShortcutBar) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ResShortcutBar) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ResShortcutBar) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.Cd) > 0 {
		dAtA2 := make([]byte, len(m.Cd)*10)
		var j1 int
		for _, num1 := range m.Cd {
			num := uint64(num1)
			for num >= 1<<7 {
				dAtA2[j1] = uint8(uint64(num)&0x7f | 0x80)
				num >>= 7
				j1++
			}
			dAtA2[j1] = uint8(num)
			j1++
		}
		i -= j1
		copy(dAtA[i:], dAtA2[:j1])
		i = encodeVarintBackpackShortcutBar(dAtA, i, uint64(j1))
		i--
		dAtA[i] = 0x12
	}
	if len(m.Ds) > 0 {
		dAtA4 := make([]byte, len(m.Ds)*10)
		var j3 int
		for _, num1 := range m.Ds {
			num := uint64(num1)
			for num >= 1<<7 {
				dAtA4[j3] = uint8(uint64(num)&0x7f | 0x80)
				num >>= 7
				j3++
			}
			dAtA4[j3] = uint8(num)
			j3++
		}
		i -= j3
		copy(dAtA[i:], dAtA4[:j3])
		i = encodeVarintBackpackShortcutBar(dAtA, i, uint64(j3))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func (m *ReqShortcutBarPutInto) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReqShortcutBarPutInto) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReqShortcutBarPutInto) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.Index != 0 {
		i = encodeVarintBackpackShortcutBar(dAtA, i, uint64(m.Index))
		i--
		dAtA[i] = 0x10
	}
	if m.PropId != 0 {
		i = encodeVarintBackpackShortcutBar(dAtA, i, uint64(m.PropId))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func (m *ResShortcutBarPutInto) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ResShortcutBarPutInto) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ResShortcutBarPutInto) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.Cd) > 0 {
		dAtA6 := make([]byte, len(m.Cd)*10)
		var j5 int
		for _, num1 := range m.Cd {
			num := uint64(num1)
			for num >= 1<<7 {
				dAtA6[j5] = uint8(uint64(num)&0x7f | 0x80)
				num >>= 7
				j5++
			}
			dAtA6[j5] = uint8(num)
			j5++
		}
		i -= j5
		copy(dAtA[i:], dAtA6[:j5])
		i = encodeVarintBackpackShortcutBar(dAtA, i, uint64(j5))
		i--
		dAtA[i] = 0x12
	}
	if len(m.Ds) > 0 {
		dAtA8 := make([]byte, len(m.Ds)*10)
		var j7 int
		for _, num1 := range m.Ds {
			num := uint64(num1)
			for num >= 1<<7 {
				dAtA8[j7] = uint8(uint64(num)&0x7f | 0x80)
				num >>= 7
				j7++
			}
			dAtA8[j7] = uint8(num)
			j7++
		}
		i -= j7
		copy(dAtA[i:], dAtA8[:j7])
		i = encodeVarintBackpackShortcutBar(dAtA, i, uint64(j7))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func (m *ReqShortcutBarRemove) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ReqShortcutBarRemove) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ReqShortcutBarRemove) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.Index != 0 {
		i = encodeVarintBackpackShortcutBar(dAtA, i, uint64(m.Index))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func (m *ResShortcutBarRemove) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ResShortcutBarRemove) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ResShortcutBarRemove) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.Ds) > 0 {
		dAtA10 := make([]byte, len(m.Ds)*10)
		var j9 int
		for _, num1 := range m.Ds {
			num := uint64(num1)
			for num >= 1<<7 {
				dAtA10[j9] = uint8(uint64(num)&0x7f | 0x80)
				num >>= 7
				j9++
			}
			dAtA10[j9] = uint8(num)
			j9++
		}
		i -= j9
		copy(dAtA[i:], dAtA10[:j9])
		i = encodeVarintBackpackShortcutBar(dAtA, i, uint64(j9))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func encodeVarintBackpackShortcutBar(dAtA []byte, offset int, v uint64) int {
	offset -= sovBackpackShortcutBar(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *ReqShortcutBar) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	return n
}

func (m *ResShortcutBar) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if len(m.Ds) > 0 {
		l = 0
		for _, e := range m.Ds {
			l += sovBackpackShortcutBar(uint64(e))
		}
		n += 1 + sovBackpackShortcutBar(uint64(l)) + l
	}
	if len(m.Cd) > 0 {
		l = 0
		for _, e := range m.Cd {
			l += sovBackpackShortcutBar(uint64(e))
		}
		n += 1 + sovBackpackShortcutBar(uint64(l)) + l
	}
	return n
}

func (m *ReqShortcutBarPutInto) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.PropId != 0 {
		n += 1 + sovBackpackShortcutBar(uint64(m.PropId))
	}
	if m.Index != 0 {
		n += 1 + sovBackpackShortcutBar(uint64(m.Index))
	}
	return n
}

func (m *ResShortcutBarPutInto) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if len(m.Ds) > 0 {
		l = 0
		for _, e := range m.Ds {
			l += sovBackpackShortcutBar(uint64(e))
		}
		n += 1 + sovBackpackShortcutBar(uint64(l)) + l
	}
	if len(m.Cd) > 0 {
		l = 0
		for _, e := range m.Cd {
			l += sovBackpackShortcutBar(uint64(e))
		}
		n += 1 + sovBackpackShortcutBar(uint64(l)) + l
	}
	return n
}

func (m *ReqShortcutBarRemove) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Index != 0 {
		n += 1 + sovBackpackShortcutBar(uint64(m.Index))
	}
	return n
}

func (m *ResShortcutBarRemove) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if len(m.Ds) > 0 {
		l = 0
		for _, e := range m.Ds {
			l += sovBackpackShortcutBar(uint64(e))
		}
		n += 1 + sovBackpackShortcutBar(uint64(l)) + l
	}
	return n
}

func sovBackpackShortcutBar(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozBackpackShortcutBar(x uint64) (n int) {
	return sovBackpackShortcutBar(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *ReqShortcutBar) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowBackpackShortcutBar
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReqShortcutBar: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReqShortcutBar: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		default:
			iNdEx = preIndex
			skippy, err := skipBackpackShortcutBar(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ResShortcutBar) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowBackpackShortcutBar
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ResShortcutBar: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ResShortcutBar: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType == 0 {
				var v int32
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					v |= int32(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				m.Ds = append(m.Ds, v)
			} else if wireType == 2 {
				var packedLen int
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					packedLen |= int(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				if packedLen < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				postIndex := iNdEx + packedLen
				if postIndex < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				if postIndex > l {
					return io.ErrUnexpectedEOF
				}
				var elementCount int
				var count int
				for _, integer := range dAtA[iNdEx:postIndex] {
					if integer < 128 {
						count++
					}
				}
				elementCount = count
				if elementCount != 0 && len(m.Ds) == 0 {
					m.Ds = make([]int32, 0, elementCount)
				}
				for iNdEx < postIndex {
					var v int32
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowBackpackShortcutBar
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						v |= int32(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					m.Ds = append(m.Ds, v)
				}
			} else {
				return fmt.Errorf("proto: wrong wireType = %d for field Ds", wireType)
			}
		case 2:
			if wireType == 0 {
				var v int64
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					v |= int64(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				m.Cd = append(m.Cd, v)
			} else if wireType == 2 {
				var packedLen int
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					packedLen |= int(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				if packedLen < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				postIndex := iNdEx + packedLen
				if postIndex < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				if postIndex > l {
					return io.ErrUnexpectedEOF
				}
				var elementCount int
				var count int
				for _, integer := range dAtA[iNdEx:postIndex] {
					if integer < 128 {
						count++
					}
				}
				elementCount = count
				if elementCount != 0 && len(m.Cd) == 0 {
					m.Cd = make([]int64, 0, elementCount)
				}
				for iNdEx < postIndex {
					var v int64
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowBackpackShortcutBar
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						v |= int64(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					m.Cd = append(m.Cd, v)
				}
			} else {
				return fmt.Errorf("proto: wrong wireType = %d for field Cd", wireType)
			}
		default:
			iNdEx = preIndex
			skippy, err := skipBackpackShortcutBar(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ReqShortcutBarPutInto) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowBackpackShortcutBar
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReqShortcutBarPutInto: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReqShortcutBarPutInto: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field PropId", wireType)
			}
			m.PropId = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowBackpackShortcutBar
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.PropId |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Index", wireType)
			}
			m.Index = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowBackpackShortcutBar
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Index |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipBackpackShortcutBar(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ResShortcutBarPutInto) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowBackpackShortcutBar
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ResShortcutBarPutInto: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ResShortcutBarPutInto: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType == 0 {
				var v int32
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					v |= int32(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				m.Ds = append(m.Ds, v)
			} else if wireType == 2 {
				var packedLen int
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					packedLen |= int(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				if packedLen < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				postIndex := iNdEx + packedLen
				if postIndex < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				if postIndex > l {
					return io.ErrUnexpectedEOF
				}
				var elementCount int
				var count int
				for _, integer := range dAtA[iNdEx:postIndex] {
					if integer < 128 {
						count++
					}
				}
				elementCount = count
				if elementCount != 0 && len(m.Ds) == 0 {
					m.Ds = make([]int32, 0, elementCount)
				}
				for iNdEx < postIndex {
					var v int32
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowBackpackShortcutBar
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						v |= int32(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					m.Ds = append(m.Ds, v)
				}
			} else {
				return fmt.Errorf("proto: wrong wireType = %d for field Ds", wireType)
			}
		case 2:
			if wireType == 0 {
				var v int64
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					v |= int64(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				m.Cd = append(m.Cd, v)
			} else if wireType == 2 {
				var packedLen int
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					packedLen |= int(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				if packedLen < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				postIndex := iNdEx + packedLen
				if postIndex < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				if postIndex > l {
					return io.ErrUnexpectedEOF
				}
				var elementCount int
				var count int
				for _, integer := range dAtA[iNdEx:postIndex] {
					if integer < 128 {
						count++
					}
				}
				elementCount = count
				if elementCount != 0 && len(m.Cd) == 0 {
					m.Cd = make([]int64, 0, elementCount)
				}
				for iNdEx < postIndex {
					var v int64
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowBackpackShortcutBar
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						v |= int64(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					m.Cd = append(m.Cd, v)
				}
			} else {
				return fmt.Errorf("proto: wrong wireType = %d for field Cd", wireType)
			}
		default:
			iNdEx = preIndex
			skippy, err := skipBackpackShortcutBar(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ReqShortcutBarRemove) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowBackpackShortcutBar
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ReqShortcutBarRemove: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ReqShortcutBarRemove: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Index", wireType)
			}
			m.Index = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowBackpackShortcutBar
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Index |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipBackpackShortcutBar(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ResShortcutBarRemove) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowBackpackShortcutBar
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ResShortcutBarRemove: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ResShortcutBarRemove: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType == 0 {
				var v int32
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					v |= int32(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				m.Ds = append(m.Ds, v)
			} else if wireType == 2 {
				var packedLen int
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return ErrIntOverflowBackpackShortcutBar
					}
					if iNdEx >= l {
						return io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					packedLen |= int(b&0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				if packedLen < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				postIndex := iNdEx + packedLen
				if postIndex < 0 {
					return ErrInvalidLengthBackpackShortcutBar
				}
				if postIndex > l {
					return io.ErrUnexpectedEOF
				}
				var elementCount int
				var count int
				for _, integer := range dAtA[iNdEx:postIndex] {
					if integer < 128 {
						count++
					}
				}
				elementCount = count
				if elementCount != 0 && len(m.Ds) == 0 {
					m.Ds = make([]int32, 0, elementCount)
				}
				for iNdEx < postIndex {
					var v int32
					for shift := uint(0); ; shift += 7 {
						if shift >= 64 {
							return ErrIntOverflowBackpackShortcutBar
						}
						if iNdEx >= l {
							return io.ErrUnexpectedEOF
						}
						b := dAtA[iNdEx]
						iNdEx++
						v |= int32(b&0x7F) << shift
						if b < 0x80 {
							break
						}
					}
					m.Ds = append(m.Ds, v)
				}
			} else {
				return fmt.Errorf("proto: wrong wireType = %d for field Ds", wireType)
			}
		default:
			iNdEx = preIndex
			skippy, err := skipBackpackShortcutBar(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthBackpackShortcutBar
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipBackpackShortcutBar(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowBackpackShortcutBar
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowBackpackShortcutBar
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowBackpackShortcutBar
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthBackpackShortcutBar
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupBackpackShortcutBar
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthBackpackShortcutBar
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthBackpackShortcutBar        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowBackpackShortcutBar          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupBackpackShortcutBar = fmt.Errorf("proto: unexpected end of group")
)
