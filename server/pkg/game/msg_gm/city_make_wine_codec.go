package msg_gm

import (
	"95eh.com/eg/intfc"
)

const (
	CdLifeStartWine    MsgCode = 102004201 // Req：开始酿酒；Res：返回-开始酿酒；
	CdLifeCancelWine   MsgCode = 102004202 // Req：取消酿酒；Res：返回-取消酿酒；
	CdLifeGainOverWine MsgCode = 102004203 // Req：收获已完成的酿酒；Res：返回-收获已完成的酿酒；
	CdLifeGetCurWines  MsgCode = 102004204 // Req：当前酿酒的列表；Res：返回-当前酿酒的列表；
	CdLifeForgeItem    MsgCode = 102004205 // Req：返回-装备合成打造；Res：返回-装备合成打造；
	CdLifeDecompose    MsgCode = 102004206 // Req：返回-装备分解；Res：返回-装备分解；
	CdLifeIntake       MsgCode = 102004207 // Req：生活采水；Res：返回-生活采水；
)

func InitCodecForCityMakeWine(userLogic intfc.IMUserLogic) {
	userLogic.BindCoderFac(CdLifeStartWine,
		func() interface{} {
			return &ReqLifeStartWine{}
		},
		func() interface{} {
			return &ResLifeStartWine{}
		})
	userLogic.BindCoderFac(CdLifeCancelWine,
		func() interface{} {
			return &ReqLifeCancelWine{}
		},
		func() interface{} {
			return &ResLifeCancelWine{}
		})
	userLogic.BindCoderFac(CdLifeGainOverWine,
		func() interface{} {
			return &ReqLifeGainOverWine{}
		},
		func() interface{} {
			return &ResLifeGainOverWine{}
		})
	userLogic.BindCoderFac(CdLifeGetCurWines,
		func() interface{} {
			return &ReqLifeGetCurWines{}
		},
		func() interface{} {
			return &ResLifeGetCurWines{}
		})
	userLogic.BindCoderFac(CdLifeForgeItem,
		func() interface{} {
			return &ReqLifeForgeItem{}
		},
		func() interface{} {
			return &ResLifeForgeItem{}
		})
	userLogic.BindCoderFac(CdLifeDecompose,
		func() interface{} {
			return &ReqLifeDecompose{}
		},
		func() interface{} {
			return &ResLifeDecompose{}
		})
	userLogic.BindCoderFac(CdLifeIntake,
		func() interface{} {
			return &ReqLifeIntake{}
		},
		func() interface{} {
			return &ResLifeIntake{}
		})
}
