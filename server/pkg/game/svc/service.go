package svc

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
)

/*
@Time   : 2021-11-08 20:25
@Author : wushu
@DESC   : svc = service
	定义所有由game分发的服务(game接收客户端的请求分发到各个服务去处理，这些服务存在于各个服务节点中)，用于处理通信消息
	定义与客户端的proto时，消息号(共九位数字)开头前五位取此处对应的服务编号，后四位自由定义
*/

type Svc = uint16

// 服务领域划分 ☆☆☆ 若不设置将请求不到服务
var (
	_Services_Data = []Svc{
		Common_Data, CharacterInfo, CharacterAttr,
		Backpack, BackpackShortcutBar, CityMarket, CityMakeWine,
		City, CityBuilding, Event, CityFarm, CityAuth, Mail, Team, Social,
		TestTry, Bill, Clan,Task,Plot,
	}
	_Services_Space = []Svc{
		WorldSpc, CitySpc, ArenaSpc,
	}
)

const scale = 100

// 数据服务10000-29999
const (
	Character           Svc = 100 * scale
	CharacterInfo           = Character + 1
	CharacterAttr           = Character + 2
	Unlock              Svc = 101 * scale
	Mail                Svc = 102 * scale
	City                Svc = 103 * scale
	Backpack            Svc = 104 * scale
	CityBuilding        Svc = 105 * scale
	BackpackShortcutBar Svc = 106 * scale
	CityMarket          Svc = 107 * scale
	CityFarm            Svc = 108 * scale
	CityAuth            Svc = 109 * scale
	Mall                Svc = 110 * scale
	CityMakeWine        Svc = 111 * scale
	Task                Svc = 112 * scale
	Plot                Svc = 113 * scale
	CityFenceTower      Svc = 114 * scale
	Team                Svc = 115 * scale
	Event               Svc = 116 * scale
	Social              Svc = 117 * scale
	Bill                Svc = 118 * scale
	Clan                Svc = 119 * scale
	SpaceSvc            Svc = 120 * scale
	Max
)

// 空间服务 30000-39999 以Spc结尾
const (
	Space        = 300 * scale // 空间通用
	WorldSpc Svc = 311 * scale // 大世界
	CitySpc  Svc = 312 * scale // 城邦
	ArenaSpc Svc = 313 * scale // 竞技场
)

// 其它
const (
	Common_Data Svc = 600 * scale // 各数据服务共用的消息号。用于请求和响应
	Sys         Svc = 650 * scale // 各服务共用的系统消息号。用于响应
	TestTry     Svc = 651 * scale
	Ai          Svc = 652 * scale
)

var (
	// 服务名
	_ServiceToName = map[Svc]string{
		CharacterInfo:       "CharacterInfo",
		CharacterAttr:       "CharacterAttr",
		Unlock:              "TxUnlock",
		Mail:                "Mail",
		City:                "City",
		Backpack:            "Backpack",
		CityBuilding:        "CityBuilding",
		BackpackShortcutBar: "ShortcutBar",
		CityMarket:          "Market",
		CityFarm:            "CityFarm",
		CityAuth:            "CityAuth",
		Mall:                "Mall",
		CityMakeWine:        "MakeWine",
		Task:                "Task",
		Plot:                "Plot",
		CityFenceTower:      "CityFenceTower",
		Team:                "Team",
		Bill:                "Bill",
		Clan:                "Clan",
	}
	_NameToService map[string]Svc
)

func init() {
	_NameToService = make(map[string]Svc, len(_ServiceToName))
	for sn, name := range _ServiceToName {
		_NameToService[name] = sn
	}
}

func NameToService(names []string) []Svc {
	sns := make([]Svc, len(names))
	for i, name := range names {
		g, ok := _NameToService[name]
		if !ok {
			app.Log().Warn("not exist service", utils.M{
				"name": name,
			})
		}
		sns[i] = g
	}
	return sns
}

func ServiceName(sn Svc) string {
	return _ServiceToName[sn]
}

func GetDataServices() []Svc {
	return _Services_Data
}

func GetSpaceServices() []Svc {
	return _Services_Space
}
