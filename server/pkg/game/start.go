package game

import (
	"95eh.com/eg/app"
	"95eh.com/eg/asset"
	"95eh.com/eg/codec"
	"95eh.com/eg/discovery"
	"95eh.com/eg/event"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/scene"
	"95eh.com/eg/timer"
	"95eh.com/eg/tx"
	"95eh.com/eg/user"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"hm/pkg/game/common"
	event_gm "hm/pkg/game/event"
	"hm/pkg/game/logic/backpack"
	"hm/pkg/game/logic/backpack_shortcut_bar"
	"hm/pkg/game/logic/character/info"
	"hm/pkg/game/logic/city"
	"hm/pkg/game/logic/city_building"
	"hm/pkg/game/logic/city_farm"
	"hm/pkg/game/logic/city_make_wine"
	"hm/pkg/game/logic/city_market"
	"hm/pkg/game/logic/clan"
	event_ctrl "hm/pkg/game/logic/event"
	"hm/pkg/game/logic/mail"
	"hm/pkg/game/logic/mall"
	"hm/pkg/game/logic/plot"
	"hm/pkg/game/logic/social"
	space_svc "hm/pkg/game/logic/space"
	"hm/pkg/game/logic/sys"
	"hm/pkg/game/logic/task"
	"hm/pkg/game/logic/team"
	"hm/pkg/game/logic/testTry"
	"hm/pkg/game/logic/unlock"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	"hm/pkg/misc/tools"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/space/msg_spc"
	"math/rand"
	"net/http"
	"time"
)

var _Ctrlers = map[uint16]common.IBaseController{
	svc.CharacterInfo:       info.NewCharacterInfoController(),
	svc.Mail:                mail.NewMailController(),
	svc.Mall:                mall.NewMallController(),
	svc.TestTry:             testTry.NewTestTryCtrl(),
	svc.Backpack:            backpack.NewBackpackController(),
	svc.BackpackShortcutBar: backpack_shortcut_bar.NewShortcutBarController(),
	svc.CityMarket:          city_market.NewMarkController(),
	svc.City:                city.NewCityController(),
	svc.CityBuilding:        city_building.NewCityBuildingController(),
	svc.CityFarm:            city_farm.NewCityFarmController(),
	svc.Sys:                 sys.NewSysController(),
	svc.CityMakeWine:        city_make_wine.NewMakeWineController(),
	svc.Unlock:              unlock.NewUnlockController(),
	svc.Team:                team.NewTeamController(),
	svc.Event:               event_ctrl.NewEventController(),
	svc.Clan:                clan.NewClanController(),
	svc.Task:                task.NewTaskController(),
	svc.Plot:                plot.NewPlotController(),
	svc.SpaceSvc:            space_svc.NewSpaceController(),
	svc.Social:              social.NewSocialController(),
}

func Start() {
	// 设置随机种子
	rand.Seed(time.Now().UnixNano())
	// 加载路径
	utils.ExeDir()

	// 加载配置文件  todo 本地读取基础配置，其他配置从consul读取
	conf, err := common.LoadConf("",
		"common", "common_dev",
		"region", "region_dev",
		"region_game", "region_game_dev",
		"design")
	if err != nil {
		panic(err.Error())
	}

	// 加载tsv
	tsv.LoadNodeTsv(misc.NodeGame)

	// 初始化tools
	if err = tools.InitGameUtils(); err != nil {
		panic(err.Error())
	}

	//启动Consul
	err = utils.StartConsul(conf.Consul)
	if err != nil {
		panic(err.Error())
	}
	//启动Redis
	regionRedis := misc.GetRedisConn(conf.Redis)
	//globalRedis := misc.GetRedisConn(conf.GlobalRedis)

	//与客户端(用户)通信的编解码器使用ProtoBuff
	userCodec := codec.NewPbCodec()

	//serviceCodec := codec.NewJsonCodec()
	// 与服务通信编解码器使用msgpack
	serviceCodec := codec.NewMsgPackCodec()

	//设置响应超时时间
	if conf.Debug {
		dur := (time.Minute * 30).Milliseconds()
		intfc.ResponseTimeoutDur = dur
		intfc.TcpDeadlineDur = dur
	}

	// 加载本节点的gorm和redis
	common.LoadRedis(conf.Redis)

	disConf := conf.Discovery
	app.Start(
		//添加日志记录
		log.NewMLogger(log.Loggers(log.NewConsole(intfc.TLogDebug)), nil),
		//资源模块
		asset.NewMAsset(),
		//区服务事务
		tx.NewMTxProducer(serviceCodec, regionRedis, nil,
			tx.ProducerTxFac(func(pid int64, client *redis.Client, confirm, cancel utils.Action) intfc.ITx {
				return common.NewTx(pid, client, confirm, cancel)
			}),
		),
		//计时模块
		timer.NewMTimer(regionRedis, nil),
		event.NewMEvent(nil),
		//短连接模块
		user.NewMHttp(userCodec,
			func(c *gin.Context, code uint32, any interface{}) {
				c.JSON(http.StatusOK, utils.M{
					"Success": true,
					"Result":  any,
				})
			}, func(c *gin.Context, code uint32, ec utils.TErrCode) {
				c.JSON(http.StatusOK, utils.M{
					"Success": false,
					"ErrCode": ec,
				})
			}, user.HttpOptions(
				//设置Http服务监听的地址
				user.HttpListen(conf.Http.Port),
				// 装饰请求
				user.HttpReceiver(common.ReceiveHttpRequest),
			),
			//模块启动前初始化Http请求相对路径和消息协议号的映射
			intfc.BeforeModuleStart(initHttp)),
		//用户长连接
		user.NewMGate(userCodec,
			user.GateOptions(
				user.GateListen(conf.Game.Gate.Port),
				user.GateCap(conf.Game.Gate.Cap),
				//发送给用户消息的缓存时间
				user.GateCacheDur(conf.Game.Gate.CacheDur),
				//用户连接成功回调
				user.GateConnected(func(str string) {
					app.Log().Debug("user connected", utils.M{
						"addr": str,
					})
					updateGateCount()
				}),
				//用户断开连接回调
				user.GateClosed(func(uid int64, addr string) {
					app.Event().DispatchEvent(event_gm.EvtUserOffline, &event_gm.UserOffLine{
						Uid:  uid,
						Addr: addr,
					})
					app.Log().Debug("user closed", utils.M{
						"addr": addr,
						"uid":  uid,
					})
					updateGateCount()
				}),
				// 装饰请求
				user.GateReceiver(common.ReceiveGateRequest),
			),
			intfc.AfterModuleStart(func() {
				//todo 暂时自己启动监听,后续可改为后台控制
				app.UserGate().StartListen()
			}),
		),
		//用户逻辑处理
		user.NewMLogic(userCodec,
			intfc.BeforeModuleStart(runningCtrler),
			intfc.BeforeModuleStart(func() {
				// 注入协议号和消息结构体的映射
				msg_gm.InitClientCodec(app.UserLogic())
			}),
		),
		//协程模块
		worker.NewMWorker(),
		//服务发现模块
		discovery.NewMDiscovery(
			//节点类型
			discovery.Nodes(misc.NodeGame),
			//节点id
			disConf.NodeId,
			//区服id
			disConf.RegionId,
			//编解码
			serviceCodec,
			regionRedis,
			//模块选项
			intfc.ModuleOptions(
				intfc.BeforeModuleStart(func() {
					// 注入协议号和消息结构体的映射
					msg_dt.InitServiceCodec(app.Discovery())
					msg_spc.InitServiceCodec(app.Discovery())
					// 注入数据服务节点和数据服务的映射
					app.Discovery().BindNodeServices(misc.NodeServices_Data, svc.GetDataServices()...)
				}),
				//模块启动后,监听登录节点
				intfc.AfterModuleStart(func() {
					app.Discovery().WatchNodes(misc.NodeLogin, misc.NodeServices_Data, misc.NodeServices_CitySpc)
				}),
			),
			discovery.Addr(disConf.Ip, disConf.Port),
			discovery.Weight(disConf.Weight),
			//节点连接成功的回调
			discovery.NodeConnected(func(node intfc.TNode, regionId, nodeId uint16) {
				onNodeConnected(node, regionId, nodeId, conf)
			}),
			discovery.NodeClosed(func(node intfc.TNode, regionId, nodeId uint16) {
				onNodeClose(node, regionId, nodeId, conf)
			})),
		scene.NewSceneCache(regionRedis, nil),
	)

	//进程终止前释放game中注入的模块
	utils.BeforeExit("stop game", app.Dispose)
}

func runningCtrler() {
	ul := app.UserLogic()
	// 绑定逻辑处理器
	for _, ctrler := range _Ctrlers {
		// 绑定逻辑处理器
		actions := ctrler.Actions()
		for _, action := range actions {
			act := action
			// id：登录前是uid，之后始终是cid
			ul.BindRequestHandler(act.PermissionCode, act.MsgCode, func(addr string, tid, id int64, reqBody interface{}) {
				act.Action(common.NewActionCtx(addr, tid, id, common.AcType_Req), reqBody)
			})
		}
		// 绑定事件逻辑处理器
		events := ctrler.Events()
		if events != nil {
			for _, action := range events {
				act := action
				// 绑定事件处理器
				app.Discovery().BindEventHandler(action.MsgCode, func(service uint16, tid int64, eveBody interface{}) {
					act.Action(common.NewActionCtx("", tid, 0, common.Acype_Eve), eveBody)
				})
			}
		}
	}
	// 初始化完成后的回调
	for sn, ctrler := range _Ctrlers {
		err := ctrler.AfterInit()
		if err != nil {
			panic(fmt.Sprintf("%s AfterInit() execution error: %v\n", svc.ServiceName(sn), err))
		}
	}
}
