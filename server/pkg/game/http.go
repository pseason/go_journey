package game

import (
	"95eh.com/eg/app"
	"github.com/gin-gonic/gin"
	"hm/pkg/game/msg_gm"
	common_login "hm/pkg/login/common"
	"net/http"
)

func initHttp() {
	userHttp := app.UserHttp()
	userHttp.SetIdParser(func(c *gin.Context) (uid, role int64, err error) {
		tkn := c.GetHeader("token")
		if tkn == "" {
			return 0, 0, nil
		}
		_, claims, err := common_login.ParseJwt(tkn)
		if err != nil {
			return 0, 0, err
		}
		return claims.Uid, claims.Mask, nil
	})

	userHttp.BindProcessor(http.MethodPost, "character/info", msg_gm.CdCharacterInfo)
	userHttp.BindProcessor(http.MethodPost, "character/create", msg_gm.CdCharacterCreate)
	userHttp.BindProcessor(http.MethodPost, "character/test", msg_gm.CdCharacterTest)

	userHttp.BindProcessor(http.MethodPost, "backpackList", msg_gm.CdBackpack)
	userHttp.BindProcessor(http.MethodPost, "arrange", msg_gm.CdBackpackArrange)
	userHttp.BindProcessor(http.MethodPost, "division", msg_gm.CdBackpackDivision)
	userHttp.BindProcessor(http.MethodPost, "sells", msg_gm.CdBackpackSells)
	userHttp.BindProcessor(http.MethodPost, "compose", msg_gm.CdBackpackCompose)
	userHttp.BindProcessor(http.MethodPost, "discard", msg_gm.CdBackpackDiscard)
	userHttp.BindProcessor(http.MethodPost, "move", msg_gm.CdBackpackMove)
	userHttp.BindProcessor(http.MethodPost, "useProp", msg_gm.CdBackpackUseProp)

	userHttp.BindProcessor(http.MethodPost, "shortcutBarList", msg_gm.CdShortcutBar)
	userHttp.BindProcessor(http.MethodPost, "putInto", msg_gm.CdShortcutBarPutInto)
	userHttp.BindProcessor(http.MethodPost, "shortcutBarMove", msg_gm.CdShortcutBarRemove)

	userHttp.BindProcessor(http.MethodPost, "marketSearch", msg_gm.CdMarket)
	userHttp.BindProcessor(http.MethodPost, "marketRecords", msg_gm.CdMarketSellingRecords)
	userHttp.BindProcessor(http.MethodPost, "marketInto", msg_gm.CdMarketPutInto)
	userHttp.BindProcessor(http.MethodPost, "marketRemove", msg_gm.CdMarketRemove)
	userHttp.BindProcessor(http.MethodPost, "marketBuy", msg_gm.CdMarketBuy)

	userHttp.BindProcessor(http.MethodPost, "testTestTry", msg_gm.CdTestTry)
	userHttp.BindProcessor(http.MethodPost, "sys/auth", msg_gm.CdSysAuth)
	userHttp.BindProcessor(http.MethodPost, "sys/reconnect", msg_gm.CdSysReconnect)

	userHttp.BindProcessor(http.MethodPost, "wineList", msg_gm.CdLifeGetCurWines)
	userHttp.BindProcessor(http.MethodPost, "wineStart", msg_gm.CdLifeStartWine)
	userHttp.BindProcessor(http.MethodPost, "wineCancel", msg_gm.CdLifeCancelWine)
	userHttp.BindProcessor(http.MethodPost, "wineRes", msg_gm.CdLifeGainOverWine)
	userHttp.BindProcessor(http.MethodPost, "wine/ForgeItem", msg_gm.CdLifeForgeItem)
	userHttp.BindProcessor(http.MethodPost, "wine/Decompose", msg_gm.CdLifeDecompose)

	userHttp.BindProcessor(http.MethodPost, "space/GetWeather", msg_gm.CdWeather)
	userHttp.BindProcessor(http.MethodPost, "space/Gather", msg_gm.CdSpaceGather)
}
