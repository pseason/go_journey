package plot

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/msg_dt"
)

type IPlotController interface {
	common.IBaseController
}

type plotController struct {
	common.IBaseController
	template     *tsv.PlotTsvManager
	taskTemplate *tsv.TaskTsvManager
}

func NewPlotController() IPlotController {
	return &plotController{
		IBaseController: common.NewBaseController(),
	}
}

func (p *plotController) AfterInit() error {
	p.template = tsv.GetTsvManager(tsv.Plot).(*tsv.PlotTsvManager)
	p.taskTemplate = tsv.GetTsvManager(tsv.Task).(*tsv.TaskTsvManager)
	return nil
}

func (p *plotController) List(ac common.IActionCtx) {
	_, resBody, err := p.RequestSvc(svc.Plot, ac.Tid(), ac.Cid(), msg_dt.CdPlotList, &msg_dt.ReqPlotList{
		Cid: ac.Cid(),
	})
	if err.IsNil() {
		res, ok := resBody.(*msg_dt.ResPlotList)
		var resList []*msg_gm.Plot
		if ok && len(res.Records) > 0 {
			resList = make([]*msg_gm.Plot, 0, len(res.Records))
			for _, record := range res.Records {
				resList = append(resList, &msg_gm.Plot{
					Id:         record.Id,
					TemplateId: record.TemplateId,
					Status:     int32(record.Status),
				})
			}
		}
		ac.ResOk(&msg_gm.ResPlotList{
			List: resList,
		})
	} else {
		ac.ResErr(err)
	}
}
