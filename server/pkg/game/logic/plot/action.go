package plot

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/msg_dt"
	"sort"
)

func (p *plotController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdPlotList, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			p.List(ac)
		}},
		{MsgCode: msg_gm.CdPlotExecute, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			body := reqBody.(*msg_gm.ReqPlotExecute)
			err := p.Execute(ac, body.Id, body.BtnIndex, body.ToIndex, body.Score)
			if err != nil {
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
			ac.ResOk(&msg_gm.ResPlotExecute{})
		}},
	}
}

func (p *plotController) Events() []common.Event {
	return []common.Event{
		{MsgCode: msg_dt.CdEvePlotChange, Action: func(ac common.IActionCtx, eveBody interface{}) {
			req := eveBody.(*msg_dt.EvePlotChange)
			var plots []*msg_gm.Plot
			if req.Records != nil && len(req.Records) > 0 {
				plots = make([]*msg_gm.Plot, 0, len(req.Records))
				for _, record := range req.Records {
					plots = append(plots, &msg_gm.Plot{
						Id:         record.Id,
						TemplateId: record.TemplateId,
						Status:     int32(record.Status),
					})
				}
				sort.SliceStable(plots, func(i, j int) bool {
					return plots[i].Status > plots[j].Status
				})
			}
			p.SendToCharacter(req.Cid, msg_gm.CdUnlockChangeNotify, &msg_gm.ResPlotChangeNotify{
				List: plots,
			})
		}},
	}
}
