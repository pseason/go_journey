package plot

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"errors"
	"hm/pkg/game/common"
	"hm/pkg/game/svc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
)

func (p *plotController) Execute(ac common.IActionCtx, plotId int64, btnIndex, toIndex int32, score []int32) (err error) {
	cid := ac.Cid()
	var curPlot *model.Plot
	logParams := utils.M{
		"cid":      cid,
		"plotId":   plotId,
		"btnIndex": btnIndex,
		"toIndex":  toIndex,
		"score":    score,
	}
	//find current plot
	_, resBody, reqErr := p.RequestSvc(svc.Plot, ac.Tid(), ac.Cid(), msg_dt.CdPlotList, &msg_dt.ReqPlotList{
		Cid:    ac.Cid(),
		PlotId: plotId,
	})
	if reqErr.IsNotNil() {
		app.Log().Error(reqErr.Desc(), logParams)
		err = reqErr.Error()
		return
	}
	res, ok := resBody.(*msg_dt.ResPlotList)
	if !ok || len(res.Records) == 0 {
		err = errors.New("request plot list response data model is not ResPlotList")
		app.Log().Error(err.Error(), logParams)
		return
	}
	curPlot = res.Records[0]
	plotTemplate, has := p.template.TsvMap[curPlot.TemplateId]
	if !has {
		app.Log().Error("plot template not found", logParams)
		return
	}
	if btnIndex == -1 && len(plotTemplate.Btn) == 1 {
		btnIndex = plotTemplate.Btn[0]
	} else if btnIndex == -1 && toIndex != -1 && len(plotTemplate.To_plot) > 0 && plotTemplate.To_plot[0] != 0 {
		btnIndex = int32(model.PlotBtnTypeSkipPlot)
	} else if btnIndex >= 0 && int(btnIndex) < len(plotTemplate.Btn) {
		btnIndex = plotTemplate.Btn[btnIndex]
	}
	btnType := model.PlotBtnType(btnIndex)
	var toPlotTemplate *tsv.PlotTsv
	tasks := make([]int32, 0, 3)
	props := make(map[int32]int32)
	plotAdds := make([]*model.Plot, 0, 3)
	isSkip := false
	switch btnType {
	case model.PlotBtnTypeSkipPlot, model.PlotBtnTypeToPlot:
		{
			if len(plotTemplate.To_plot) > int(toIndex) {
				toPlotTemplateId := plotTemplate.To_plot[toIndex]
				if toPlotTemplateId == 0 {
					err = errors.New("this plot can't skip")
					return
				}
				//目标需要跳至的剧情
				toPlotTemplate, has = p.template.TsvMap[toPlotTemplateId]
				if !has {
					err = errors.New("target plot template not found")
					return
				}
				if btnType == model.PlotBtnTypeSkipPlot {
					if toPlotTemplateId > curPlot.TemplateId {
						for i := curPlot.TemplateId + 1; i <= toPlotTemplateId; i++ {
							toNextPlotTemplate, pHas := p.template.TsvMap[i]
							if pHas && len(toNextPlotTemplate.Btn) == 1 {
								err = p.ExecuteSubOption(ac, curPlot, model.PlotBtnType(toNextPlotTemplate.Btn[0]), toNextPlotTemplate,
									0, &tasks, props, &plotAdds)
								if err != nil {
									return
								}
							}
						}
					} else {
						err = errors.New("this plot can't skip")
						return
					}
					isSkip = true
				} else {
					plotAdds = append(plotAdds, model.GeneratePlot(toPlotTemplate, cid, curPlot.InnerId))
					toPlotTemplate = nil
				}
			}
		}
	case model.PlotBtnTypeAddProp, model.PlotBtnTypeAddTask, model.PlotBtnTypeAddSkill:
		{
			err = p.ExecuteSubOption(ac, curPlot, btnType, plotTemplate, 0, &tasks, props, &plotAdds)
			if err != nil {
				return
			}
		}
	}
	nextPlotFunc := func(nextId int32) (err error) {
		nextPlotCsv, has := p.template.TsvMap[nextId]
		if !has {
			err = errors.New("target plot has next plot, but next plot template not found")
			return
		}
		plotAdds = append(plotAdds, model.GeneratePlot(nextPlotCsv, cid, 0))
		return
	}
	// 是否含有下一个剧情
	if plotTemplate.NextId > 0 && !isSkip {
		err = nextPlotFunc(plotTemplate.NextId)
		if err != nil {
			return
		}
	}
	// 跳过的剧情是否含有下一个剧情
	if isSkip && toPlotTemplate != nil && toPlotTemplate.NextId > 0 {
		err = nextPlotFunc(toPlotTemplate.NextId)
		if err != nil {
			return
		}
		isSkip = false
	}
	var txOk bool
	p.Transaction(ac.Tid(), func(tx common.ITx) (tranResult bool) {
		//change current plot state and add new plots
		var plotUpdates, plotRemoves []*model.Plot
		if curPlot.FightOpt || curPlot.SceneOpt {
			curPlot.Status = model.PlotStatusLink
			plotUpdates = []*model.Plot{curPlot}
		} else {
			curPlot.Status = model.PlotStatusCompleted
			plotRemoves = []*model.Plot{curPlot}
		}
		if _, errCode := tx.Try(svc.Plot, cid, msg_dt.CdPlotBatchExecute, &msg_dt.ReqPlotBatchExecute{
			Cid:     cid,
			Adds:    plotAdds,
			Updates: plotUpdates,
			Removes: plotRemoves,
		}); errCode > 0 {
			return
		}
		//add tasks
		if len(tasks) > 0 {
			//if _, errCode := tx.Try(svc.Task, cid, msg_dt.CdTaskBatchExecute, &msg_dt.ReqTaskBatchExecute{
			//	Cid:              cid,
			//	AddByTemplateIds: tasks,
			//}); errCode > 0 {
			//	return
			//}
		}
		//add props
		if len(props) > 0 {
			if _, errCode := tx.Try(svc.Backpack, cid, msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
				Cid:            cid,
				Props:          props,
				IsFullSendMail: true,
			}); errCode > 0 {
				return
			}
		}
		curPlot.Status = model.PlotStatusCompleted
		txOk = true
		tranResult = true
		return
	})
	if txOk && plotTemplate.NoviceOver == 1 {
		//TODO plot completed -> exit novice scene
		//TODO destroy novice city
		//TODO unlock: exit novice
	}
	return
}

func (p *plotController) ExecuteSubOption(ac common.IActionCtx, plot *model.Plot, btnType model.PlotBtnType, template *tsv.PlotTsv,
	toIndex int32, tasks *[]int32, props map[int32]int32, newPlots *[]*model.Plot) (err error) {
	switch btnType {
	case model.PlotBtnTypeAddProp:
		{
			if len(template.ItemId) > int(toIndex) {
				d := template.ItemId[toIndex]
				for i := 0; i < len(d); i++ {
					_, has := props[d[i]]
					if has {
						props[d[i]] += d[i+1]
					} else {
						props[d[i]] = d[i+1]
					}
					i++
				}
			}
		}
	case model.PlotBtnTypeAddTask:
		{
			*tasks = append(*tasks, template.TaskId...)
			var plots []*model.Plot
			plots = p.BeforeOrAfterEventTask(ac, plot.InnerId, template.TaskId, true)
			if plots != nil {
				*newPlots = append(*newPlots, plots...)
			}
		}
	}
	return
}

func (p *plotController) BeforeOrAfterEventTask(ac common.IActionCtx, innerId int64, taskTemplateId []int32, before bool) (res []*model.Plot) {
	res = make([]*model.Plot, 0, 3)
	if len(taskTemplateId) > 0 {
		//for _, id := range taskTemplateId {
		//	taskTemplate, has := p.taskTemplate.TsvMap[id]
		//	if !has {
		//		continue
		//	}
		//	var template *tsv.PlotTsv
		//	if before && taskTemplate.Plot[0] > 0 {
		//		template, _ = p.template.TsvMap[taskTemplate.Plot[0]]
		//	} else if !before && taskTemplate.Plot[1] > 0 {
		//		template, _ = p.template.TsvMap[taskTemplate.Plot[1]]
		//	}
		//	if template != nil {
		//		res = append(res, model.GeneratePlot(template, ac.Cid(), innerId))
		//	}
		//}
	}
	return
}
