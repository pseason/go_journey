package city_market

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
)

type IMarketController interface {
	common.IBaseController
}

func (mkCtr *markController)  Actions() []common.Action{
	return []common.Action{
		{MsgCode: msg_gm.CdMarket, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqMarket)
			mkCtr.GetTitleSearch(ac,req.BuildId,req.Title,req.Type,req.Page,req.PageSize)
			return
		}},

		{MsgCode: msg_gm.CdMarketSellingRecords, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqMarketSellingRecords)
			mkCtr.GetSellingRecords(ac,req.BuildId,req.Page,req.PageSize)
			return
		}},

		{MsgCode: msg_gm.CdMarketPutInto, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqMarketPutInto)
			mkCtr.PutInto(ac,req.PropId,req.BuildId,req.Num,req.Price,req.Day)
			return
		}},

		{MsgCode: msg_gm.CdMarketRemove, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqMarketRemove)
			mkCtr.Remove(ac,req.Id)
			return
		}},

		{MsgCode: msg_gm.CdMarketBuy, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqMarketBuy)
			mkCtr.Buy(ac,req.Id)
			return
		}},
	}
}