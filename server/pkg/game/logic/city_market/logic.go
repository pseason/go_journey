package city_market

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"fmt"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
	"hm/pkg/services/data/msg_dt"
	"math"
)

type markController struct {
	common.IBaseController
	marketTsv map[int32]*tsv.MarketTsv
	pTsv      map[int32]*tsv.PropTsv
	cronId    int64
}

func NewMarkController() IMarketController {
	return &markController{
		IBaseController: common.NewBaseController(),
	}
}

func (mkCtr *markController) AfterInit() error {
	mkCtr.marketTsv = tsv.GetTsvManager(tsv.Market).(*tsv.MarketTsvManager).TsvMap
	mkCtr.pTsv = tsv.GetTsvManager(tsv.Prop).(*tsv.PropTsvManager).TsvMap
	//if mkCtr.cronId <=0 {
	//	_ = ants.Submit(func() {
	//		mkCtr.cronId,_ = app.Timer().Cron("@every 1s", func() {
	//			mkCtr.CronCancel()
	//		})
	//	})
	//}
	return nil
}

func (mkCtr *markController) GetTitleSearch(ac common.IActionCtx, BuildId int64, Title string, Type map[int32]int32, CurrentPage, TotalPage int64) {

	_, resBody, err := mkCtr.RequestSvc(svc.CityMarket, ac.Tid(), 0, msg_dt.CdMarketList, &msg_dt.ReqMarketTitleList{
		Page:    CurrentPage,
		Size:    TotalPage,
		Title:   Title,
		BuildId: BuildId,
		Type:    Type,
		Cid:     ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Debug("市场数据查询失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res, ok := resBody.(*msg_dt.ResMarketList)
	if !ok {
		app.Log().Debug("市场数据查询为空", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	var MainType int32 = 0
	var SubType int32 = 0
	for m, s := range Type {
		MainType = m
		SubType = s
	}
	ac.ResOk(&msg_gm.ResMarket{
		List:      mkCtr.ProtoMarketList(res.MarketList.List...),
		TotalPage: res.MarketList.TotalPage,
		Page:      res.MarketList.Page,
		Rows:      res.MarketList.Rows,
		MainType:  MainType,
		SubType:   SubType,
	})
}

func (mkCtr *markController) GetSellingRecords(ac common.IActionCtx, BuildId int64, CurrentPage, TotalPage int64) {
	_, resBody, err := mkCtr.RequestSvc(svc.CityMarket, ac.Tid(), 0, msg_dt.CdMarketRecord, &msg_dt.ReqMarketRecord{
		Page:    CurrentPage,
		Size:    TotalPage,
		BuildId: BuildId,
		Cid:     ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Debug("背包数据查询失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res, ok := resBody.(*msg_dt.ResMarketRecord)
	if !ok {
		app.Log().Debug("背包数据查询为空", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResMarketSellingRecords{
		List:      mkCtr.ProtoMarketList(res.MarketList.List...),
		TotalPage: res.MarketList.TotalPage,
		Page:      res.MarketList.Page,
		Rows:      res.MarketList.Rows,
	})
}

func (mkCtr *markController) GetConditionDays(params int32) bool {
	maxConditionDay := []int32{7, 30}
	var resBool = false
	for _, day := range maxConditionDay {
		if day == params {
			resBool = true
			break
		}
	}
	return resBool
}

func (mkCtr *markController) PutInto(ac common.IActionCtx, PropId int32, BuildId int64, Num, Price, Day int32) {
	if !mkCtr.GetConditionDays(Day) || Num <= 0 || Price > 9999999 || Price <= 0 {
		app.Log().Debug("param error", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//todo 通过Bid查询背包PropId
	_, resBody, err := mkCtr.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackGetProps, &msg_dt.ReqBackpackGetProps{
		PropIds: []int32{PropId},
		Cid: ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Debug("query error by backpack", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	backpackInfo := resBody.(*msg_dt.ResBackpackGetProps)
	if len(backpackInfo.Props) <=0 {
		app.Log().Debug("#01 backpack num is not rich", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}else{
		for _,item := range backpackInfo.Props{
			if item.Num <Num{
				app.Log().Debug("#02 backpack num is not rich", utils.M{"errCode": err})
				ac.ResErr(err_gm.ErrQueryFailed)
				return
			}
		}
	}
	if mkCtr.pTsv[PropId].Sell <= 0 {
		app.Log().Debug("the prop not allow sell", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	//目前没有市场等级-暂不从角色信息获取
	_, resBody, err = mkCtr.RequestSvc(svc.CityBuilding, ac.Tid(), 0, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{
		BuildingId: BuildId,
	})
	if err.IsNotNil() {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	buildInfo, ok := resBody.(*msg_dt.ResCityBuildingGetBuilding)
	if !ok {
		app.Log().Debug("build num is empty", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	currentLevel := buildInfo.Building.Level
	if buildInfo.Building.Level <= 1{
		currentLevel = 1
	}

	if currentLevel <= 0 {
		app.Log().TError(ac.Tid(), utils.NewError("buildLv is zero", nil))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	//当前建筑的上架最大数
	buildUpCount := mkCtr.marketTsv[currentLevel].MemberNubmer

	_, markCountRes, err := mkCtr.RequestSvc(svc.CityMarket, ac.Tid(), 0, msg_dt.CdMarketGetUpCount, &msg_dt.ReqMarketGetUpCount{
		BuildId: BuildId,
		Cid:     ac.Cid(),
	})
	markCount := markCountRes.(*msg_dt.ResMarketGetUpCount)
	if markCount.Num >= int64(buildUpCount) {
		app.Log().TError(ac.Tid(), utils.NewError("the building shelf limit has been reached", nil))
		ac.ResErr(err_gm.ErrOverLimit)
		return
	}
	mkCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		//todo 扣减金币
		//todo 扣减背包的物品
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtract, &msg_dt.ReqBackpackSubtract{
			PropType: model.BackpackTypeAll,
			Props: map[int32]int32{
				PropId: Num,
			},
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec > 0 {
			return false
		}
		return true
	})

	var PtParams = model.MarketPutParams{
		Num:    Num,
		Day:    Day,
		Price:  Price,
		PropId: PropId,
	}
	//上架商品
	_, resPutInto, err := mkCtr.RequestSvc(svc.CityMarket, ac.Tid(), 0, msg_dt.CdMarketPutInto, &msg_dt.ReqMarketPutInto{
		PtParams: []*model.MarketPutParams{&PtParams},
		BuildId:  BuildId,
		Cid:      ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Debug("市场上架失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := resPutInto.(*msg_dt.ResMarketPutInto)
	ac.ResOk(&msg_gm.ResMarketPutInto{
		Data: mkCtr.ProtoSingleMarketList(res.MarketList...),
	})
}

func (mkCtr *markController) Remove(ac common.IActionCtx, Bid int64) {
	//todo 获取Bid 对应的信息
	_, resBody, err := mkCtr.RequestSvc(svc.CityMarket, ac.Tid(), 0, msg_dt.CdMarketGetPropById, &msg_dt.ReqMarketGetPropById{
		Bid: Bid,
		Cid: ac.Cid(),
	})
	if err.IsNotNil() {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	marketInfo := resBody.(*msg_dt.ResMarketGetPropById)
	_, _, err = mkCtr.RequestSvc(svc.CityMarket, ac.Tid(), 0, msg_dt.CdMarketRemove, &msg_dt.ReqMarketRemove{
		Bid: Bid,
		Cid: ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Debug("市场下架失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//todo 增加物品
	mkCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
			Props: map[int32]int32{
				marketInfo.MarketList.PropId: marketInfo.MarketList.Number,
			},
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec > 0 {
			return false
		}
		return true
	})
	ac.ResOk(&msg_gm.ResMarketRemove{
		Id: Bid,
	})
}

func (mkCtr *markController) Buy(ac common.IActionCtx, Bid int64) {
	//todo 获取Bid 对应的信息
	_, resBody, err := mkCtr.RequestSvc(svc.CityMarket, ac.Tid(), 0, msg_dt.CdMarketGetPropById, &msg_dt.ReqMarketGetPropById{
		Bid: Bid,
		Cid: ac.Cid(),
	})
	if err.IsNotNil() {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res, ok := resBody.(*msg_dt.ResMarketGetPropById)
	if !ok {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	if res.MarketList == nil || res.MarketList.Price <= 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//todo 获取购买方角色信息
	CharacterInfo, IErr := mkCtr.RequestCharacterInfoQueryByCid(ac.Tid(), nil, ac.Cid())
	if IErr.IsNotNil() {
		app.Log().Error("获取用户角色失败", utils.M{"errCode": IErr.Error()})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//todo 检测金币是否足够
	if CharacterInfo.Gold < res.MarketList.Price {
		app.Log().Error("金币不足", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	var ds = &msg_gm.Market{
		Cid:        res.MarketList.Cid,
		PropId:     res.MarketList.PropId,
		Id:         res.MarketList.Id,
		Number:     res.MarketList.Number,
		BuildId:    res.MarketList.BuildId,
		Price:      res.MarketList.Price,
		Status:     int32(res.MarketList.Status),
		Day:        res.MarketList.Day,
		Expiration: res.MarketList.Expiration,
	}
	mkCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		//todo doStep 1更改上架物品状态信息
		_, ec := tx.Try(svc.CityMarket, ac.Cid(), msg_dt.CdMarketBuy, &msg_dt.ReqMarketBuy{
			Bid:Bid,
			Cid:ac.Cid(),
		})
		if ec.IsNotNil() {
			app.Log().TError(ac.Tid(), utils.NewError(ec.String(), nil))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return false
		}
		//todo doStep 2扣减金币
		_, _, ec = tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, map[model_field_e.Enum]int32{
			character_info_e.Gold: (-1) * ds.Price,
		}, ac.Cid())
		if ec.IsNotNil() {
			app.Log().TError(ac.Tid(), utils.NewError(ec.String(), nil))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return false
		}
		//todo doStep 3物品增加
		_, ec = tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
			Props: map[int32]int32{
				ds.PropId: ds.Number,
			},
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec > 0 {
			return false
		}

		type title struct {
			builtName  string
			propName   string
			propNum    int32
			cname      string
			sGoldPrice int32
			tGoldPrice int32
			GoldPrice  int32
			rGoldPrice int32
		}
		mailTitle := new(title)
		mailTitle.builtName = CharacterInfo.CityName
		mailTitle.cname = CharacterInfo.Nickname
		mailTitle.propName = res.MarketList.Name
		mailTitle.propNum = ds.Number
		mailTitle.rGoldPrice = ds.Price / ds.Number                                            //单价
		mailTitle.tGoldPrice = int32(math.Ceil(float64(mailTitle.rGoldPrice) * float64(0.05))) //税5% 单价
		mailTitle.GoldPrice = mailTitle.rGoldPrice - mailTitle.tGoldPrice                      //最终单价
		mailTitle.sGoldPrice = mailTitle.GoldPrice * ds.Number

		mailContent := fmt.Sprintf("您在[%s]上架的商品[%s]*[%d]被[%s]购买，出售总价为[%d*%d]，税率抽取[%d*%d]，实际收益为[%d*%d]。", mailTitle.builtName,
			mailTitle.propName, mailTitle.propNum, mailTitle.cname, mailTitle.rGoldPrice, mailTitle.propNum,
			mailTitle.tGoldPrice,mailTitle.propNum, mailTitle.GoldPrice,mailTitle.propNum)
		mailProp := map[int64]int32{
			2001: mailTitle.sGoldPrice, //发送金币
		}
		mail, errMail := model.NewSystemMail(ds.Cid, "市场物品购买附件领取", mailContent, mailProp)
		if errMail != nil {
			ac.ResErr(err_gm.ErrQueryFailed)
		}

		//todo doStep 4给上架角色发放邮件
		resBody, err = tx.Try(svc.Mail, ac.Cid(), msg_dt.CdMailSend, &msg_dt.ReqMailSend{
			Mails: []*model.Mail{mail},
		})
		if err.IsNotNil() {
			ac.ResErr(err_gm.ErrQueryFailed)
			return false
		}
		ac.ResOk(&msg_gm.ResMarketBuy{
			Data: ds,
		})

		return true
	})

	return
}

func (mkCtr *markController) CronCancel()  {
	mkCtr.Transaction(0, func(tx common.ITx) bool {
		//Step 1更改上架物品状态信息
		res, ec := tx.Try(svc.CityMarket, 0, msg_dt.CdMarketAutoCancel, &msg_dt.ReqMarketAutoCancel{})
		if ec.IsNotNil() {
			app.Log().TError(0, utils.NewError(ec.String(), nil))
			return false
		}

		markInfos := res.(*msg_dt.ResMarketAutoCancel)

		sendMails := []*model.Mail{}
		for _,item := range markInfos.MarketList{
			title := "你在市场上架的[%s]*[%d]已经到期。"
			mailProp := map[int64]int32{int64(item.PropId):item.Num,}
			mail, errMail := model.NewSystemMail(item.Cid, "市场过期邮件", fmt.Sprintf(title,item.Name,item.Num), mailProp)
			if errMail != nil {
				app.Log().TError(0, utils.NewError(errMail.Error(), nil))
				return false
			}
			sendMails = append(sendMails,mail)
		}

		//step 2 发放邮件
		if len(sendMails) >0 {
			_, err := tx.Try(svc.Mail, 0, msg_dt.CdMailSend, &msg_dt.ReqMailSend{
				Mails: sendMails,
			})
			if err.IsNotNil() {
				app.Log().TError(0, utils.NewError(err.String(), nil))
				return false
			}
		}
		return true
	})
}

func (mkCtr *markController) ProtoSingleMarketList(lists ...*model.CityMarket) (sliceList *msg_gm.Market) {
	for _, items := range lists {
		sliceList = &msg_gm.Market{
			PropId:     items.PropId,
			Id:         items.Id,
			Number:     items.Number,
			BuildId:    items.BuildId,
			Price:      items.Price,
			Status:     int32(items.Status),
			Day:        items.Day,
			Expiration: items.Expiration,
		}
		break
	}
	return
}

func (mkCtr *markController) ProtoMarketList(lists ...*model.CityMarket) (sliceList []*msg_gm.Market) {
	for _, items := range lists {
		sliceList = append(sliceList, &msg_gm.Market{
			PropId:     items.PropId,
			Id:         items.Id,
			Number:     items.Number,
			BuildId:    items.BuildId,
			Price:      items.Price,
			Status:     int32(items.Status),
			Day:        items.Day,
			Expiration: items.Expiration,
		})
	}
	return
}
