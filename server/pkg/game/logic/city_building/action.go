package city_building

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/building_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/space/msg_spc"
)

type ICityBuildingController interface {
	common.IBaseController
}

// 消息处理器
func (m *cityBuildingController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdInvestResources, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqInvestResources)
			m.CityBuildingInvestResources(ac, req.BuildingId, req.Resources)
			return
		}},
		{MsgCode: msg_gm.CdInvestEnergyNum, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqInvestEnergyNum)
			m.CityBuildingInvestEnergyNum(ac, req.BuildingId)
			return
		}},
		{MsgCode: msg_gm.CdBuildEnergyInfo, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBuildEnergyInfo)
			m.CityBuildingEnergyNum(ac, req.BuildingId)
			return
		}},
		{MsgCode: msg_gm.CdBuildResourceInfo, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBuildResourceInfo)
			m.CityBuildingResource(ac, req.BuildingId)
			return
		}},
		{MsgCode: msg_gm.CdCityAllBuilding, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			m.CityBuildingAllByCityId(ac)
			return
		}},
		{MsgCode: msg_gm.CdBuildingInvestEnergy, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBuildingInvestEnergy)

			//查询建筑
			_, resBody, errCode := m.RequestSvc(svc.CityBuilding, ac.Tid(), req.BuildingId, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{BuildingId: req.BuildingId})
			if errCode > 0 {
				app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
				ac.ResErr(err_gm.ErrQueryFailed)
				return
			}
			building := resBody.(*msg_dt.ResCityBuildingGetBuilding).Building
			if building.BuildingState != building_e.InvestEnergy {
				ac.ResErr(err_gm.ErrAuthFailed)
				return
			}
			m.Space().RequestActor(ac.Tid(), ac.Cid(), svc.Space, msg_spc.CdActorBuildingAction, &msg_spc.ReqActorBuildingAction{
				ActionId: req.ActionId,
				TargetId: req.BuildingId,
				ActorId:  ac.Cid(),
			}, func(tid int64, resObj interface{}) {
				ac.ResOk(&msg_gm.ResSpaceAction{})
			}, func(err utils.IError) {
				app.Log().TError(ac.Tid(), err)
				ac.ResErr(err_gm.ErrChangePos)
			})
		},
		},
	}
}

func (c *cityBuildingController) Events() []common.Event {
	return []common.Event{
		{MsgCode: msg_dt.CdEveCityBuildingState, Action: func(ac common.IActionCtx, eveBody interface{}) {
			event := eveBody.(*msg_dt.EveCityBuildingState)
			upMap := make(map[int32]int32)
			switch event.UpdateType {
			case 1:
				upMap[int32(building_e.BuildingInfoState)] = event.BuildingState
			case 2:
				upMap[int32(building_e.BuildingInfoLv)] = event.BuildingLv
			case 3:
				upMap[int32(building_e.BuildingInfoState)] = event.BuildingState
				upMap[int32(building_e.BuildingInfoLv)] = event.BuildingLv
			}
			c.Space().RequestActor(ac.Tid(), event.BuildingId, svc.Space, msg_spc.CdActorBuildingInfoChange, &msg_spc.ReqActorBuildingInfoChange{
				UpMap: upMap,
			}, func(tid int64, resObj interface{}) {
				//ac.ResOk(&msg_gm.ResSpaceMoveStart{})
			}, func(err utils.IError) {
				app.Log().TError(ac.Tid(), err)
				ac.ResErr(err_gm.ErrMoveStart)
			})
		}},
		{
			MsgCode: msg_spc.CdEveActorBuildAction, Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveActorBuildAction)
				c.SendToCharacter(evt.Id, msg_gm.CdNoticeInvestEnergy, &msg_gm.NoticeInvestEnergy{
					Cid:        evt.ActorId,
					BuildingId: evt.TargetId,
					Action:     evt.ActionId,
				})
			},
		},
	}
}
