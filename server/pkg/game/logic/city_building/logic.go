package city_building

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/msg_dt"
)

type cityBuildingController struct {
	common.IBaseController
}

func NewCityBuildingController() ICityBuildingController {
	return &cityBuildingController{
		IBaseController: common.NewBaseController(),
	}
}

func (c *cityBuildingController) AfterInit() error {
	//go func() {
	//	time.Sleep(3 * time.Second)
	//	c.CityBuildingInvestEnergyNum(common.NewActionCtx("",0,1480815391537651716,common.AcType_Req),1480815391537651716)
	//	//c.CityBuildingInvestEnergyNum(common.NewActionCtx("",0,1),1468421334177964036)
	//}()
	return nil
}

//CityBuildingAllByCityId 查询城邦所有建筑 query all building by city id
func (c *cityBuildingController) CityBuildingAllByCityId(ac common.IActionCtx) {
	characterInfo, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query character city id and nick name failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}

	if characterInfo.CityId == 0 {
		app.Log().TError(ac.Tid(), utils.NewError("character not join city", utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	_, body, errCode := c.RequestSvc(svc.CityBuilding, ac.Tid(), characterInfo.CityId, msg_dt.CdCityBuildingList, &msg_dt.ReqCityBuildingList{CityId: characterInfo.CityId})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResCityBuildingList)
	buildings := make([]*msg_gm.Building, 0)
	if len(res.Buildings) > 0 {
		for _, building := range res.Buildings {
			buildings = append(buildings, &msg_gm.Building{
				BuildingId:    building.Id,
				ErectType:     int32(building.BuildingType),
				Level:         building.Level,
				BuildingState: int32(building.BuildingState),
			})
		}
	}
	ac.ResOk(&msg_gm.ResCityAllBuilding{BuildingList: buildings})
}

//投入资源
func (c *cityBuildingController) CityBuildingInvestResources(ac common.IActionCtx, buildingId int64, investResourcesMap map[int32]int32) {
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		body, errCode := tx.Try(svc.CityBuilding, buildingId, msg_dt.CdCityBuildingTryInvestResources, &msg_dt.ReqCityBuildingTryInvestResources{
			BuildingId:      buildingId,
			InvestResources: investResourcesMap,
			Cid:             ac.Cid(),
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		res := body.(*msg_dt.ResCityBuildingTryInvestResources)
		_, errCode = tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtract, &msg_dt.ReqBackpackSubtract{
			PropType:       model.BackpackTypeDefault,
			Props:          res.InvestResources,
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		if len(res.CurrentPutResources) > 0 {
			ac.ResOk(&msg_gm.ResInvestResources{CurrentResources: res.CurrentPutResources})
			return true
		}
		ac.ResOk(&msg_gm.ResInvestResources{})
		return true
	})

}

//投入精力
func (c *cityBuildingController) CityBuildingInvestEnergyNum(ac common.IActionCtx, buildingId int64) {
	characterInfo, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("character query failed", nil))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if characterInfo.CityId == 0 {
		app.Log().TError(ac.Tid(), utils.NewError("character not join city", nil))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//查询建筑
	_, resBody, errCode := c.RequestSvc(svc.CityBuilding, ac.Tid(), buildingId, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{BuildingId: buildingId})
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	building := resBody.(*msg_dt.ResCityBuildingGetBuilding).Building
	if building.CityId != characterInfo.CityId {
		app.Log().TError(ac.Tid(), utils.NewError("character join city and building city not consistent", utils.M{"cityId": building.CityId, "character-cityId": characterInfo.CityId}))
		ac.ResErr(err_gm.ErrQueryFailed)
	}
	//添加建筑建设度
	reqSvmCi := &msg_dt.ReqCityBuildingTryInvestEnergy{
		BuildingId: buildingId,
		Cid:        ac.Cid(),
	}
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		body, code := tx.Try(svc.CityBuilding, buildingId, msg_dt.CdCityBuildingTryInvestEnergy, reqSvmCi)
		if code > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		res := body.(*msg_dt.ResCityBuildingTryInvestEnergy)

		ac.ResOk(&msg_gm.ResInvestEnergyNum{
			Energy: res.CurrentNum,
		})
		return true
	})

}

//建筑当前建设度
func (c *cityBuildingController) CityBuildingEnergyNum(ac common.IActionCtx, buildingId int64) {
	//查询角色精力值，进行扣除
	reqSvmCi := &msg_dt.ReqCityBuildingEnergy{
		BuildingId: buildingId,
	}
	_, body, code := c.RequestSvc(svc.CityBuilding, ac.Tid(), 0, msg_dt.CdCityBuildingEnergy, reqSvmCi)
	if code > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResCityBuildingEnergy)
	ac.ResOk(&msg_gm.ResBuildEnergyInfo{
		BuildingId: buildingId,
		Energy:     res.CurrentEnergyNum,
		NeedBuildingLv: res.NeedBuildingLv,
	})
}

//建筑当前投入材料
func (c *cityBuildingController) CityBuildingResource(ac common.IActionCtx, buildingId int64) {
	//查询建筑当前投入材料
	reqSvmCi := &msg_dt.ReqCityBuildingResource{
		BuildingId: buildingId,
	}
	_, body, code := c.RequestSvc(svc.CityBuilding, ac.Tid(), 0, msg_dt.CdCityBuildingResource, reqSvmCi)
	if code > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResCityBuildingResource)
	ac.ResOk(&msg_gm.ResBuildResourceInfo{
		BuildingId: buildingId,
		Resources:  res.Resources,
		NeedBuildingLv: res.NeedBuildingLv,
	})
}
