package info

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	spc_common "hm/pkg/services/space/common"
)

/*
@Time   : 2021-12-06 16:40
@Author : wushu
@DESC   : 角色游戏声明周期
*/

var (
	// 创建账号后
	AfterCreateHandler []func(cic ICharacterInfoController, ac common.IActionCtx, tx common.ITx, characterInfo *model.CharacterInfo) (iError utils.IError)

	// 进入游戏后
	EnterGameHandler []func(cic ICharacterInfoController, ac common.IActionCtx, characterInfo *model.CharacterInfo) (iError utils.IError)

	// 下线后
	AfterOfflineHandler []func(cic ICharacterInfoController, ac common.IActionCtx) (iError utils.IError)
)

// 注意每个回调数组中的回调函数的顺序，不要随意插入在某个函数的前面
func init() {
	AfterCreateHandler = []func(cic ICharacterInfoController, ac common.IActionCtx, tx common.ITx, characterInfo *model.CharacterInfo) (iError utils.IError){
		// eg：初始化灵魂点数
	}
	EnterGameHandler = []func(cic ICharacterInfoController, ac common.IActionCtx, characterInfo *model.CharacterInfo) (iError utils.IError){
		// 将角色信息添加到缓存
		func(cic ICharacterInfoController, ac common.IActionCtx, characterInfo *model.CharacterInfo) (iError utils.IError) {
			return cic.ToCache(ac, characterInfo)
		},
		// todo 加载战斗属性
		func(cic ICharacterInfoController, ac common.IActionCtx, characterInfo *model.CharacterInfo) (iError utils.IError) {
			return nil
		},
		// 进入空间,将玩家数据写入redis
		func(cic ICharacterInfoController, ac common.IActionCtx, characterInfo *model.CharacterInfo) (iError utils.IError) {
			return app.SceneCache().SaveActor(ac.Cid(), spc_common.ActorPlayer, nil,
				&spc_common.ActorComTransform{
					Position: utils.Vec3{
						X: 31.73,
						Y: 21.29,
						Z: 35,
					},
					Forward: utils.Vec3{
						X: 1,
					},
					MoveSpeed: 4,
				}, &spc_common.ActorComLife{
					NickName:  characterInfo.Nickname,
					Level:     1,
					Gender:    false,
					Figure:    0,
					TeamId:    0,
					FactionId: 0,
				})
		},
		//加载任务
		func(cic ICharacterInfoController, ac common.IActionCtx, characterInfo *model.CharacterInfo) (iError utils.IError) {
			cic.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
				_, errCode := tx.Try(svc.Task, ac.Cid(), msg_dt.CdTaskTryLoginInit, msg_dt.ReqTaskTryLoginInit{
					Cid: []int64{ac.Cid()},
				})
				if errCode > 0 {
					app.Log().TError(ac.Tid(),utils.NewError("character login init task exit",utils.M{"cid": ac.Cid()}))
					return
				}
				return true
			})
			return nil
		},
	}
	AfterOfflineHandler = []func(cic ICharacterInfoController, ac common.IActionCtx) (iError utils.IError){}
}
