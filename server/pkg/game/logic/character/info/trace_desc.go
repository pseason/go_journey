package info

/*
@Time   : 2021-11-05 10:47
@Author : wushu
@DESC   :这里定义链路日志的描述信息
	这些字符串常量用于创建错误、打印日志时作为描述参数传入
	参见开发文档"链路日志和error处理机制"
*/

// 错误描述 (创建自定义错误时使用)
const (
	TQueryErr         = "query err"
	TCreateErr        = "create err"
	TUpdateErr        = "update err"
	TDeleteErr        = "delete err"
	TNicknameBeingUse = "nickname being use" // 昵称被占用
)

// 非错误描述
const ()
