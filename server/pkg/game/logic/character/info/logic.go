package info

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc/cache_key"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

/*
@Time   : 2021-11-12 20:17
@Author : wushu
@DESC   :
*/

type characterInfoController struct {
	common.IBaseController
}

func NewCharacterInfoController() ICharacterInfoController {
	return &characterInfoController{
		IBaseController: common.NewBaseController(),
	}
}

func (cic *characterInfoController) AfterInit() error {
	return nil
}

func (cic *characterInfoController) InfoByUid(ac common.IActionCtx, uid int64) {
	reqSvc := &msg_dt.ReqCharacterInfo{Uid: uid}
	_, resBody, errCode := cic.RequestSvc(svc.CharacterInfo, ac.Tid(), 0, msg_dt.CdCharacterInfo, reqSvc)
	if errCode != 0 {
		app.Log().TError(ac.Tid(), utils.NewError(TQueryErr, nil))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	characterInfo := resBody.(*msg_dt.ResCharacterInfo).CharacterInfo
	ac.ResOk(characterInfo.MarshalPb())
	cid := characterInfo.Id
	if cid == 0 { // 无角色
		return
	}

	// 建立uid-cid映射
	app.UserLogic().SetUidAlias(uid, cid)
	ac.SetCid(cid)

	// 进入游戏回调
	subAc := ac.NewSubActionCtx("into cycle_handler - EnterGameHandler")
	for _, handler := range EnterGameHandler {
		if iError := handler(cic, subAc, characterInfo); iError != nil {
			app.Log().TError(subAc.Tid(), iError)
			return
		}
	}

}

func (cic *characterInfoController) Create(ac common.IActionCtx, uid int64, nickname string, gender uint8, figure model.FigureType) {
	var characterInfo *model.CharacterInfo
	cic.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		// 创建角色基础信息
		reqSvc := &msg_dt.ReqTryCharacterCreate{Uid: uid, Nickname: nickname, Gender: gender, Figure: uint8(figure)}
		resBody, errCode := tx.Try(svc.CharacterInfo, 0, msg_dt.CdTryCharacterCreate, reqSvc)
		if errCode != 0 {
			if errCode == err_dt.ErrCharacterNicknameBeingUse {
				app.Log().TError(ac.Tid(), utils.NewError(TNicknameBeingUse, nil))
				ac.ResErr(err_gm.ErrCharacterNicknameBeingUse)
			} else {
				app.Log().TError(ac.Tid(), utils.NewError(TCreateErr, nil))
				ac.ResErr(err_gm.ErrCreateFailed)
			}
			return
		}

		characterInfo = resBody.(*msg_dt.ResTryCharacterCreate).CharacterInfo
		// 建立uid-cid映射
		cid := characterInfo.Id
		app.UserLogic().SetUidAlias(uid, cid)
		ac.SetCid(cid)

		// 创建角色属性
		resBody, errCode = tx.Try(svc.CharacterAttr, cid, msg_dt.CdTryCharacterAttrCreate, nil)
		if errCode != 0 {
			app.Log().TError(ac.Tid(), utils.NewError(TCreateErr, nil))
			ac.ResErr(err_gm.ErrCreateFailed)
			return
		}

		// 创建角色的回调
		subAc := ac.NewSubActionCtx("into cycle_handler - AfterCreateHandler")
		for _, handler := range AfterCreateHandler {
			if iError := handler(cic, subAc, tx, characterInfo); iError != nil {
				app.Log().TError(subAc.Tid(), iError)
				return
			}
		}
		ac.ResOk(characterInfo.MarshalPb()) // 事务完成，响应
		return true
	})

	// 进入游戏回调
	subAc := ac.NewSubActionCtx("into cycle_handler - EnterGameHandler")
	for _, handler := range EnterGameHandler {
		if iError := handler(cic, subAc, characterInfo); iError != nil {
			app.Log().TError(subAc.Tid(), iError)
			return
		}
	}
}

func (cic *characterInfoController) ToCache(ac common.IActionCtx, info *model.CharacterInfo) (iError utils.IError) {
	redisKey := cache_key.CharacterInfo.Format(info.Id)
	set := cic.Redis().HSet(cic.Context(), redisKey, info.ToRedisMap())
	if set.Err() != nil {
		return utils.NewError(set.Err().Error(), nil)
	}
	return
}
