package info

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"fmt"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
	"hm/pkg/services/data/msg_dt"
)

/*
@Time   : 2021-11-09 14:06
@Author : wushu
@DESC   :
*/

// 列出本控制器的所有接口。仅用来给自己看自己都写了哪些接口，没有其他任何用途
type ICharacterInfoController interface {
	common.IBaseController
	// 通过uid获取角色信息(登录时调用)
	InfoByUid(ac common.IActionCtx, uid int64)
	// 创建角色
	Create(ac common.IActionCtx, uid int64, nickname string, gender uint8, figure model.FigureType)
	// 将角色信息添加到缓存
	ToCache(ac common.IActionCtx, info *model.CharacterInfo) (iError utils.IError)
}

// 消息处理器
func (cic *characterInfoController) Actions() []common.Action {
	return []common.Action{
		// 根据uid获取角色信息
		{MsgCode: msg_gm.CdCharacterInfo, PermissionCode: misc.RoleGuest, Action: func(ac common.IActionCtx, reqBody interface{}) {
			uid := ac.Cid() // 玩家在登录前调用此接口，获取到的是uid
			cic.InfoByUid(ac, uid)
		}},
		// 角色创建
		{MsgCode: msg_gm.CdCharacterCreate, PermissionCode: misc.RoleGuest, Action: func(ac common.IActionCtx, reqBody interface{}) {
			uid := ac.Cid() // 玩家在登录前调用此接口，获取到的是uid
			req := reqBody.(*msg_gm.ReqCharacterCreate)
			cic.Create(ac, uid, req.Nickname, uint8(req.Gender), model.FigureType(req.Figure))
		}},
		// 测试用例
		{MsgCode: msg_gm.CdCharacterTest, PermissionCode: misc.RoleGuest, Action: func(ac common.IActionCtx, reqBody interface{}) {

			var cid1, cid2, cid3 int64 = 1483443498194718720, 1470957893951049728, 1

			// 1-1. 简单查询
			data11, errCode := cic.RequestSvcQueryById(svc.CharacterInfo, ac.Tid(), model_e.CharacterInfo, []character_info_e.Enum{character_info_e.Age, character_info_e.Nickname}, cid1)
			if errCode != 0 {
				app.Log().TError(ac.Tid(), utils.NewError(TQueryErr, nil))
				ac.ResErr(err_gm.ErrQueryFailed)
			}
			characterInfo := data11.(*model.CharacterInfo)
			fmt.Println("1-1", characterInfo)
			ac.ResOk(characterInfo)

			// 1-2.简单查询角色，nil表示查询所有字段
			data12, errCode := cic.RequestCharacterInfoQueryByCid(ac.Tid(), nil, cid1)
			if errCode != 0 {
				app.Log().TError(ac.Tid(), utils.NewError(TQueryErr, nil))
				ac.ResErr(err_gm.ErrQueryFailed)
			}
			fmt.Println("1-2", data12)

			// 2-1. 批量查询
			data21, errCode := cic.RequestSvcQueryByIds(svc.CharacterInfo, ac.Tid(), model_e.CharacterInfo, []character_info_e.Enum{character_info_e.Nickname}, cid1, cid2, cid3)
			if errCode != 0 {
				app.Log().TError(ac.Tid(), utils.NewError(TQueryErr, nil))
				ac.ResErr(err_gm.ErrQueryFailed)
			}
			fmt.Println("2-1", data21[cid1].(*model.CharacterInfo))

			// 2-2. 批量查询角色信息
			data22, errCode := cic.RequestCharacterInfoQueryByCids(ac.Tid(), nil, cid1, cid2, cid3)
			if errCode != 0 {
				app.Log().TError(ac.Tid(), utils.NewError(TQueryErr, nil))
				ac.ResErr(err_gm.ErrQueryFailed)
			}
			fmt.Println("2-2", data22[cid1])

			// 3 事务数据接口
			cic.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
				// 3-1-1. 用于更新的批量查询。查询时会对相关的数据加锁，结果可用于更新。必须在事务中使用
				data311, errCode := tx.TryQueryByIds(svc.CharacterInfo, model_e.CharacterInfo, []character_info_e.Enum{character_info_e.Nickname}, cid1, cid2, cid3)
				if errCode != 0 {
					app.Log().TError(ac.Tid(), utils.NewError(TQueryErr, nil))
					ac.ResErr(err_gm.ErrQueryFailed)
					return
				}
				fmt.Println("3-1-1", data311[cid1].(*model.CharacterInfo))

				// 3-1-2. 用于更新的批量查询角色信息
				data312, errCode := tx.TryCharacterInfoQueryByCids([]character_info_e.Enum{character_info_e.Nickname, character_info_e.Age}, cid1, cid2, cid3)
				if errCode != 0 {
					app.Log().TError(ac.Tid(), utils.NewError(TQueryErr, nil))
					ac.ResErr(err_gm.ErrQueryFailed)
					return
				}
				fmt.Println("3-1-2", data312[cid1])

				// 3-2 更新为指定的值
				data32, errCode := tx.TryUpdateById(svc.CharacterInfo, model_e.CharacterInfo, map[model_field_e.Enum]interface{}{
					//character_info_e.Uid:      1470958636095242239, // 不可修改的字段，将报错
					character_info_e.Nickname: "无数呀",
					character_info_e.Gender:   0,
					character_info_e.TeamId:   0,
					character_info_e.Age:      20001,
					character_info_e.Gold:     999,
					character_info_e.CityId:   1473549737336881171,
				}, cid1)
				if errCode != 0 {
					app.Log().TError(ac.Tid(), utils.NewError(TUpdateErr, nil))
					ac.ResErr(err_gm.ErrUpdateFailed)
					return
				}
				fmt.Println("3-2", data32)

				// 3-3 对数值进行增减
				_, data33, errCode := tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, map[character_info_e.Enum]int32{
					character_info_e.Gold: 200,
					character_info_e.Age:  -1,
					//character_info_e.Nickname: 2, // 不是数值类型，会报错
				}, cid1)
				if errCode != 0 {
					app.Log().TError(ac.Tid(), utils.NewError(TUpdateErr, nil))
					ac.ResErr(err_gm.ErrUpdateFailed)
					return
				}
				fmt.Println("3-3", data33)

				return true
			})

		}},
	}
}

// 事件处理器
func (cic *characterInfoController) Events() []common.Event {
	return []common.Event{
		// 通知角色信息改变 (只通知给属性改变的那个角色；需要广播的会走空间服)
		{MsgCode: msg_dt.CdEveCharacterInfoChange, Action: func(ac common.IActionCtx, eveBody interface{}) {
			notice := &msg_gm.NoticeCharacterInfoChange{}
			eve := eveBody.(*msg_dt.EveCharacterInfoChange)
			notice.Cid = eve.Cid
			notice.IntInfo = make(map[int32]int32)
			for k, v := range eve.IntInfo {
				notice.IntInfo[int32(k)] = v
			}
			notice.LongInfo = make(map[int32]int64)
			for k, v := range eve.LongInfo {
				notice.LongInfo[int32(k)] = v
			}
			notice.StrInfo = make(map[int32]string)
			for k, v := range eve.StrInfo {
				notice.StrInfo[int32(k)] = v
			}
			cic.SendToCharacter(eve.Cid, msg_gm.CdNoticeCharacterInfoChange, notice)
		}},
	}
}
