package space

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/event"
	common2 "hm/pkg/services/space/common"
)

type spaceController struct {
	common.IBaseController
}

func NewSpaceController() *spaceController {
	return &spaceController{
		IBaseController: common.NewBaseController(),
	}
}

func (c *spaceController) AfterInit() error {
	app.Event().BindEvent(event.EvtUserOffline, func(obj interface{}) {
		evt := obj.(*event.UserOffLine)
		cid, ok := app.UserLogic().GetAliasWithUid(evt.Uid)
		if !ok {
			return
		}
		tid := app.Log().TSign(0, utils.M{
			"tracer": "user offline",
		})
		_, _, err := common2.GetRedisActorSceneData(cid)
		if err != nil {
			return
		}
		c.Space().RequestActorExitScene(tid, cid, func(tid int64, res interface{}) {
			app.Log().TInfo(tid, "actor exist scene", nil)
		}, func(e utils.IError) {
			app.Log().TError(tid, e)
		})
	})
	return nil
}
