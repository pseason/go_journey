package space

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/building_e"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/gather_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/space/msg_spc"
)

type ISpaceController interface {
	common.IBaseController
}

func (c *spaceController) Actions() []common.Action {
	return []common.Action{
		{
			MsgCode: msg_gm.CdSpaceEnterScene, PermissionCode: misc.RolePlayer,
			Action: func(ac common.IActionCtx, reqBody interface{}) {
				req := reqBody.(*msg_gm.ReqSpaceEnterScene)
				c.Space().RequestActorEnterScene(ac.Tid(), ac.Cid(), misc.TScene(req.SceneType), req.SceneId, func(tid int64, res interface{}) {
					ac.ResOk(&msg_gm.ResSpaceEnterScene{})
				}, func(err utils.IError) {
					app.Log().TError(ac.Tid(), err)
					ac.ResErr(err_gm.ErrEnterScene)
				})
			},
		},
		{
			MsgCode: msg_gm.CdSpaceExitCurrentScene, PermissionCode: misc.RolePlayer,
			Action: func(ac common.IActionCtx, reqBody interface{}) {
				c.Space().RequestActorExitScene(ac.Tid(), ac.Cid(), func(tid int64, res interface{}) {
					ac.ResOk(&msg_gm.ResSpaceExitCurrentScene{})
				}, func(err utils.IError) {
					app.Log().TError(ac.Tid(), err)
					ac.ResErr(err_gm.ErrExitScene)
				})
			},
		},
		{
			MsgCode: msg_gm.CdSpaceMoveStart, PermissionCode: misc.RolePlayer,
			Action: func(ac common.IActionCtx, reqBody interface{}) {
				req := reqBody.(*msg_gm.ReqSpaceMoveStart)
				c.Space().RequestActor(ac.Tid(), ac.Cid(), svc.Space, msg_spc.CdActorMoveStart, &msg_spc.ReqActorMoveStart{
					ActorId: ac.Cid(),
					PosX:    req.PositionX,
					PosY:    req.PositionY,
					PosZ:    req.PositionZ,
					ForX:    req.ForwardX,
					ForY:    req.ForwardY,
					ForZ:    req.ForwardZ,
				}, func(tid int64, resObj interface{}) {
					ac.ResOk(&msg_gm.ResSpaceMoveStart{})
				}, func(err utils.IError) {
					app.Log().TError(ac.Tid(), err)
					ac.ResErr(err_gm.ErrMoveStart)
				})
			},
		},
		{
			MsgCode: msg_gm.CdSpaceMoveStop, PermissionCode: misc.RolePlayer,
			Action: func(ac common.IActionCtx, reqBody interface{}) {
				req := reqBody.(*msg_gm.ReqSpaceMoveStop)
				c.Space().RequestActor(ac.Tid(), ac.Cid(), svc.Space, msg_spc.CdActorMoveStop, &msg_spc.ReqActorMoveStop{
					PosX: req.PositionX,
					PosY: req.PositionY,
					PosZ: req.PositionZ,
				}, func(tid int64, resObj interface{}) {
					ac.ResOk(&msg_gm.ResSpaceMoveStop{})
				}, func(err utils.IError) {
					app.Log().TError(ac.Tid(), err)
					ac.ResErr(err_gm.ErrMoveStop)
				})
			},
		},
		{
			MsgCode: msg_gm.CdSpaceChangePos, PermissionCode: misc.RolePlayer,
			Action: func(ac common.IActionCtx, reqBody interface{}) {
				req := reqBody.(*msg_gm.ReqSpaceChangePos)
				c.Space().RequestActor(ac.Tid(), ac.Cid(), svc.Space, msg_spc.CdActorPositionChange, &msg_spc.ReqActorPositionChange{
					ActorId: ac.Cid(),
					PosX:    req.PositionX,
					PosY:    req.PositionY,
					PosZ:    req.PositionZ,
				}, func(tid int64, resObj interface{}) {
					ac.ResOk(&msg_gm.ResSpaceChangePos{})
				}, func(err utils.IError) {
					app.Log().TError(ac.Tid(), err)
					ac.ResErr(err_gm.ErrChangePos)
				})
			},
		},
		//客户端调用采集
		{
			MsgCode: msg_gm.CdSpaceGather, PermissionCode: misc.RolePlayer,
			Action: func(ac common.IActionCtx, reqBody interface{}) {
				//查询背包是否已满
				_, resBody, errCode :=c.RequestSvc(svc.Backpack,ac.Tid(),ac.Cid(),msg_dt.CdBackpackIsFullLoad,&msg_dt.ReqBackpackIsFullLoad{
					Cid:ac.Cid(),
					PropType:model.BackpackTypeDefault,
				})
				if errCode > 0 {
					ac.ResErr(err_gm.ErrQueryFailed)
					return
				}
				resBackFull := resBody.(*msg_dt.ResBackpackIsFullLoad)
				if resBackFull.IsFull {
					app.Log().Info("背包已满",nil)
					ac.ResErr(err_gm.ErrQueryFailed)
					return
				}
				//查询角色精力值
				characterInfo, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.Energy}, ac.Cid())
				if errCode > 0 {
					ac.ResErr(err_gm.ErrQueryFailed)
					return
				}
				if characterInfo.Energy <= 0 {
					ac.ResErr(err_gm.ErrQueryFailed)
					return
				}
				req := reqBody.(*msg_gm.ReqSpaceGather)
				c.Space().RequestActor(ac.Tid(), ac.Cid(), svc.Space, msg_spc.CdActorGatherResource, &msg_spc.ReqActorGatherResource{
					ActorId:        ac.Cid(),
					PractisedLevel: 0, 					//todo 查询角色熟练度等级|目前角色信息无此字段
					ResourceId:     req.ResourceId,
					CurrentEnergy:  characterInfo.Energy,
				}, func(tid int64, resObj interface{}) {
					//扣除精力,发放采集资源和触发特殊奖励
					res := resObj.(*msg_spc.ResActorGatherResource)
					if len(res.Rewards) > 0 {
						//扣除精力
						c.Transaction(ac.Tid(), func(tx common.ITx) bool {
							if res.ConsumeEnergy>0{
								_,_, errCode := tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, map[character_info_e.Enum]int32{
									character_info_e.Enum(2006): res.ConsumeEnergy,
								}, ac.Cid())
								if errCode != 0 {
									app.Log().TError(ac.Tid(),utils.NewError("character lack of energy",
										utils.M{"$_type": 2006,"num": res.ConsumeEnergy}))
									ac.ResErr(err_gm.ErrUpdateFailed)
									return false
								}
							}

							_, errCode = tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
								Props:          res.Rewards,
								Cid:            ac.Cid(),
								IsFullSendMail: false,
							})
							if errCode > 0 {
								return false
							}
							return true
						})
					}

					ac.ResOk(&msg_gm.ResSpaceGather{
						Result:res.ResourceId,
					})
				}, func(err utils.IError) {
					app.Log().TError(ac.Tid(), err)
					ac.ResErr(err_gm.ErrGather)
				})
			},
		},
		//客戶端请求天气
		{
			MsgCode: msg_gm.CdWeather, PermissionCode: misc.RolePlayer,
			Action: func(ac common.IActionCtx, reqBody interface{}) {
				req := reqBody.(*msg_gm.ReqWeather)
				c.Space().RequestScene(svc.Space,ac.Tid(), misc.TScene(req.SceneType), req.SceneId, msg_spc.CdSceneWeather,&msg_spc.ReqSceneWeather{
					SceneType:misc.TScene(req.SceneType),
					SceneId:req.SceneId,
				}, func(tid int64, resObj interface{}) {
					resWeatherObj := resObj.(*msg_spc.ResSceneWeather)
					//提取天气数据
					ac.ResOk(&msg_gm.ResWeather{
						SceneType:req.SceneType,
						SceneId:req.SceneId,
						Data:&msg_gm.WeatherData{
							Sun:float32(resWeatherObj.Sun),
							Snow:float32(resWeatherObj.Snow),
							Rain:float32(resWeatherObj.Rain),
							Wind:float32(resWeatherObj.Wind),
							Shade:float32(resWeatherObj.Shade),
						},
					})
				}, func(err utils.IError) {
					app.Log().TError(ac.Tid(), err)
					ac.ResErr(err_gm.ErrQueryFailed)
				})
			},
		},
	}
}

func (c *spaceController) Events() []common.Event {
	return []common.Event{
		{
			MsgCode: msg_spc.CdEveActorMoveStart,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveActorMoveStart)
				app.Log().Debug("move start", utils.M{
					"event": evt,
				})
				c.SendToCharacter(evt.ActorId, msg_gm.CdNoticeSpaceMoveStart, &msg_gm.NoticeSpaceMoveStart{
					ActorId:   evt.TargetId,
					PositionX: evt.PosX,
					PositionY: evt.PosY,
					PositionZ: evt.PosZ,
					ForwardX:  evt.ForX,
					ForwardY:  evt.ForY,
					ForwardZ:  evt.ForZ,
				})
			},
		},
		{
			MsgCode: msg_spc.CdEveActorMoveStop,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveActorMoveStop)
				c.SendToCharacter(evt.ActorId, msg_gm.CdNoticeSpaceStopMove, &msg_gm.NoticeSpaceStopMove{
					ActorId:   evt.TargetId,
					PositionX: evt.PosX,
					PositionY: evt.PosY,
					PositionZ: evt.PosZ,
				})
			},
		},
		{
			MsgCode: msg_spc.CdEveActorVisible,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveActorVisible)
				app.Log().Debug("visible", utils.M{
					"event": evt,
				})
				noticeSpaceVisible := &msg_gm.NoticeSpaceVisible{
					ActorId:   evt.TargetId,
					Type:      int32(evt.TargetType),
					PositionX: evt.PosX,
					PositionY: evt.PosY,
					PositionZ: evt.PosZ,
					ForwardX:  evt.ForX,
					ForwardY:  evt.ForY,
					ForwardZ:  evt.ForZ,
				}

				switch evt.TargetType {
				case 0:
					noticeSpaceVisible.StrInfo = map[int32]string{character_info_e.Nickname: evt.Nickname}
				case 2:
					noticeSpaceVisible.IntInfo = map[int32]int32{
						int32(building_e.BuildingInfoType):  evt.EvtBuilding.BuildingType,
						int32(building_e.BuildingInfoState): evt.EvtBuilding.BuildingState,
						int32(building_e.BuildingInfoLv):    evt.EvtBuilding.Level,
					}
				case 3:
					noticeSpaceVisible.IntInfo = map[int32]int32{
						int32(gather_e.ResourceId):      evt.EvtResource.ResId,
						int32(gather_e.ResourceIsEmpty): evt.EvtResource.Empty,
					}

				}
				c.SendToCharacter(evt.ActorId, msg_gm.CdNoticeSpaceVisible, noticeSpaceVisible)
			},
		},
		{
			MsgCode: msg_spc.CdEveActorInvisible,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveActorInvisible)
				c.SendToCharacter(evt.ActorId, msg_gm.CdNoticeSpaceInvisible, &msg_gm.NoticeSpaceInvisible{
					ActorId: evt.TargetId,
				})
			},
		},
		{
			MsgCode: msg_spc.CdEveBuildingInfoChange,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveBuildingInfoChange)
				c.SendToCharacter(evt.ActorId, msg_gm.CdNoticeActorInfoChange, &msg_gm.NoticeActorInfoChange{
					ActorId: []int64{evt.BuildingId},
					IntInfo: evt.UpMap,
				})
			},
		},
		{
			MsgCode: msg_spc.CdEveActorGatherChange,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveActorGatherChange)
				noticeGatherChange := &msg_gm.NoticeSpaceGatherChange{
					ActorId:   []int64{evt.GatherId},
					ResId:      evt.ResourceId,
					Empty: 		evt.Empty,
				}
				c.SendToCharacter(evt.ActorId, msg_gm.CdNoticeSpaceGatherChange, noticeGatherChange)

			},
		},
		{
			MsgCode: msg_spc.CdEveActorResourceState,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveActorResourceState)
				app.Log().Info("CdEveActorResourceState is",utils.M{"CdEveActorResourceState":evt})
				c.SendToCharacter(evt.ActorId, msg_gm.CdNoticeSpaceResourceState, &msg_gm.NoticeSpaceResourceState{
					Id: evt.ResourceId,
					Empty: evt.Empty,
				})
			},
		},
		{
			MsgCode: msg_spc.CdEveSceneWeatherInfo,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				evt := eveBody.(*msg_spc.EveSceneWeatherInfo)
				c.SendToCharacter(evt.ActorId, msg_gm.CdNoticeWeather, &msg_gm.NoticeWeather{
					SceneId:evt.SceneId,
					SceneType:int32(evt.SceneType),
					Data: &msg_gm.WeatherData{
						Sun:   float32(evt.Sun),
						Snow:  float32(evt.Snow),
						Rain:  float32(evt.Rain),
						Wind:  float32(evt.Wind),
						Shade: float32(evt.Shade),
					},
				})
			},
		},
	}
}
