package sys

import (
	"95eh.com/eg/app"
	"95eh.com/eg/data"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	common2 "hm/pkg/login/common"
)

const (
	UdSid data.TField = iota
	UdDeviceId
)

type sysController struct {
	common.IBaseController
}

func NewSysController() *sysController {
	return &sysController{
		IBaseController: common.NewBaseController(),
	}
}

func (c *sysController) Auth(ac common.IActionCtx, deviceId, jwt string) {
	_, claims, err := common2.ParseJwt(jwt)
	if err != nil {
		ac.ResErr(err_gm.ErrAuthFailed)
		return
	}
	app.UserLogic().AddUserData(claims.Uid, claims.Mask, ac.Addr(), data.Map{
		UdSid:      claims.Sid,
		UdDeviceId: deviceId,
	})
	ac.ResOk(&msg_gm.ResSysAuth{
		Uid: claims.Uid,
	})
}

func (c *sysController) Reconnect(ac common.IActionCtx, uid int64, deviceId string) {
	app.UserLogic().ReadUserData(uid, func(ok bool, data data.IData) {
		if !ok {
			ac.ResErr(err_gm.ErrAuthFailed)
			return
		}
		did, _ := data.String(UdDeviceId)
		if did != deviceId {
			ac.ResErr(err_gm.ErrAuthFailed)
			return
		}
		ac.ResOk(msg_gm.ResSysReconnect{})
	})
}
