package sys

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
)

type ISysController interface {
	common.IBaseController
	// Auth 鉴权
	Auth(ac common.IActionCtx, deviceId, jwt string)
	// Reconnect 断线重连
	Reconnect(ac common.IActionCtx, uid int64, deviceId string)
}

func (c *sysController) Actions() []common.Action {
	return []common.Action{
		{msg_gm.CdSysAuth, misc.RoleGuest, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqSysAuth)
			c.Auth(ac, req.DeviceId, req.Key)
		}},
		{msg_gm.CdSysReconnect, misc.RoleGuest, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqSysReconnect)
			c.Reconnect(ac, req.Uid, req.DeviceId)
		}},
	}
}
