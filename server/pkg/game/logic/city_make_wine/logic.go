package city_make_wine

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
	"math/rand"
	"time"
)

type makeWineController struct {
	common.IBaseController
	forgeTsv     map[int32]*tsv.ForgeTsv
	tavernTsv    *tsv.TavernTsvManager
	decomposeTsv map[int32]*tsv.DecomposeTsv
}

func NewMakeWineController() IMakeWineController {
	return &makeWineController{
		IBaseController: common.NewBaseController(),
	}
}

func (mwCtr *makeWineController) AfterInit() error {
	mwCtr.forgeTsv = tsv.GetTsvManager(tsv.Forge).(*tsv.ForgeTsvManager).TsvMap
	mwCtr.tavernTsv = tsv.GetTsvManager(tsv.Tavern).(*tsv.TavernTsvManager)
	mwCtr.decomposeTsv = tsv.GetTsvManager(tsv.Decompose).(*tsv.DecomposeTsvManager).TsvMap
	return nil
}

func (mwCtr *makeWineController) GetMakeWineList(ac common.IActionCtx, BuildId int64) {
	err, bLevel := mwCtr.GetBuildLevel(ac, BuildId)
	if err.IsNotNil() {
		return
	}
	_, resBody, err := mwCtr.RequestSvc(svc.CityMakeWine, ac.Tid(), 0, msg_dt.CdMakeWineList, &msg_dt.ReqMakeWineList{
		BuildId: BuildId,
		Cid:     ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("酿酒获取数据失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res, ok := resBody.(*msg_dt.ResMakeWineList)
	if !ok {
		app.Log().Error("酿酒获取数据为空", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResLifeGetCurWines{
		List:       mwCtr.FormatMakeWine(bLevel, res.List...),
		BuildLevel: bLevel,
	})
}

//在酿酒服务包装 -获取建筑等级
func (mwCtr *makeWineController) GetBuildLevel(ac common.IActionCtx, BuildId int64) (err utils.TErrCode, currentLevel int32) {
	//获取建筑等级
	_, resBody, err := mwCtr.RequestSvc(svc.CityBuilding, ac.Tid(), 0, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{
		BuildingId: BuildId,
	})
	if err.IsNotNil() {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	buildInfo, ok := resBody.(*msg_dt.ResCityBuildingGetBuilding)
	if !ok {
		app.Log().Debug("建筑数据为空", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if buildInfo.Building.Level <= 1{
		currentLevel = 1
	}else{
		currentLevel = buildInfo.Building.Level
	}
	return
}

func (mwCtr *makeWineController) MakeWineStart(ac common.IActionCtx, BuildId int64, PropId, Slot int32) {
	//
	//todo 检测建筑等级以及卡槽上限
	err, bLevel := mwCtr.GetBuildLevel(ac, BuildId)
	if err.IsNotNil() {
		app.Log().Debug("建筑等级查询失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	currentLevel := mwCtr.tavernTsv.TsvMap[bLevel].Volume
	if Slot > currentLevel {
		app.Log().Debug("该卡槽依赖的建筑等级未达标", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	_, resBody, err := mwCtr.RequestSvc(svc.CityMakeWine, ac.Tid(), 0, msg_dt.CdMakeWineStart, &msg_dt.ReqMakeWineStart{
		BuildId:    BuildId,
		Slot:       Slot,
		PropId:     PropId,
		Cid:        ac.Cid(),
		BuildLevel: bLevel,
	})
	if err.IsNotNil() {
		app.Log().Error("酿酒失败",utils.M{"err":err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	res, ok := resBody.(*msg_dt.ResMakeWineStart)
	if !ok {
		app.Log().Debug("酿酒返回数据解析为空", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//todo 扣除背包物品
	mwCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtract, &msg_dt.ReqBackpackSubtract{
			Props:          map[int32]int32{PropId: 10, 3011025: 10, 3011026: 10},
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec.IsNotNil() {
			app.Log().Debug("扣除物品失败", utils.M{"errCode": ec})
			ac.ResErr(ec)
			return false
		}
		return true
	})

	//todo 更新酿酒数据
	_, resBody, err = mwCtr.RequestSvc(svc.CityMakeWine, ac.Tid(), 0, msg_dt.CdMakeWineListById, &msg_dt.ReqMakeWineListById{
		Bid:        res.List.Id,
		Cid:        ac.Cid(),
		IsUpDelete: true,
	})
	if err.IsNotNil() {
		app.Log().Debug("更新酿酒状态失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	makeWineList, ok := resBody.(*msg_dt.ResMakeWineListById)
	if !ok {
		app.Log().Debug("解析酿酒状态失败", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResLifeStartWine{
		Wine: mwCtr.FormatMakeWineSingle(makeWineList.List),
	})

	return
}

func (mwCtr *makeWineController) MakeWineCancel(ac common.IActionCtx, Bid int64) {
	//todo 获取酿酒数据
	_, resBody, err := mwCtr.RequestSvc(svc.CityMakeWine, ac.Tid(), 0, msg_dt.CdMakeWineListById, &msg_dt.ReqMakeWineListById{
		Bid:        Bid,
		Cid:        ac.Cid(),
		IsUpDelete: false,
	})
	if err.IsNotNil() {
		app.Log().Debug("获取酿酒数据失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	makeWineList, ok := resBody.(*msg_dt.ResMakeWineListById)
	if !ok {
		app.Log().Debug("获取酿酒数据为空", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//清除物品
	_, resBody, err = mwCtr.RequestSvc(svc.CityMakeWine, ac.Tid(), 0, msg_dt.CdMakeWineCancel, &msg_dt.ReqMakeWineCancel{
		Bid: Bid,
		Cid: ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("取消酿酒获取数据失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res, ok := resBody.(*msg_dt.ResMakeWineCancel)
	if !ok || res.Bid <= 0 {
		app.Log().Error("取消酿酒获取数据为空", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//todo 退回酿造物品
	mwCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		//3011025 井水 3011026 谷物
		resIncrease, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
			Cid: ac.Cid(),
			Props: map[int32]int32{
				3011025:                  10,
				3011026:                  10,
				makeWineList.List.PropId: 1,
			},
			IsFullSendMail: true,
		})
		if ec > 0 {
			return false
		}
		resBody := resIncrease.(*msg_dt.ResBackpackIncrease)
		//新增物品溢出发送邮件
		if len(resBody.OverProps) > 0 {
			//todo 邮件发送服务
			//创建mail 对象
			overMail := make([]*model.Mail, 0)
			mailProp := make(map[int64]int32, 0)
			mailProps := make([]map[int64]int32, 0)
			for propId, num := range resBody.OverProps {
				PropId := int64(propId)
				mailProp[PropId] = num
				if len(mailProp) >= 4 {
					mailProps = append(mailProps, mailProp)
					mailProp = make(map[int64]int32, 0)
				}
			}
			if len(mailProp) > 0 {
				mailProps = append(mailProps, mailProp)
			}
			for _, p := range mailProps {
				m, err := model.NewSystemMail(ac.Cid(), "附件", "背包不足，以邮件发送", p)
				if err != nil {
					app.Log().Error("邮件创建失败", utils.M{"errCode": err})
					return false
				}
				overMail = append(overMail, m)
			}
			_, ec = tx.Try(svc.Mail, ac.Cid(), msg_dt.CdMailSend, &msg_dt.ReqMailSend{
				Mails: overMail,
			})
			if ec > 0 {
				return false
			}
		}
		return true
	})
	ac.ResOk(&msg_gm.ResLifeCancelWine{
		WineId: res.Bid,
	})

}

func (mwCtr *makeWineController) MakeWineReap(ac common.IActionCtx, Bid int64) {
	//todo 获取酿酒数据
	_, resBody, err := mwCtr.RequestSvc(svc.CityMakeWine, ac.Tid(), 0, msg_dt.CdMakeWineListById, &msg_dt.ReqMakeWineListById{
		Bid:        Bid,
		Cid:        ac.Cid(),
		IsUpDelete: false,
	})
	if err.IsNotNil() {
		app.Log().Debug("获取酿酒数据失败", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	makeWineList, ok := resBody.(*msg_dt.ResMakeWineListById)
	if !ok {
		app.Log().Debug("获取酿酒数据为空", utils.M{"errCode": nil})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	_, resBody, err = mwCtr.RequestSvc(svc.CityMakeWine, ac.Tid(), 0, msg_dt.CdMakeWineReap, &msg_dt.ReqMakeWineReap{
		Bid: Bid,
		Cid: ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("酿酒收获数据失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res, ok := resBody.(*msg_dt.ResMakeWineReap)
	if !ok || res.Bid <= 0 {
		app.Log().Error("酿酒收获数据为空", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//todo 发放奖励
	mwCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
			Props: map[int32]int32{
				makeWineList.List.PropId: makeWineList.List.Num},
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec > 0 {
			return false
		}
		return true
	})
	ac.ResOk(&msg_gm.ResLifeGainOverWine{
		WineId: res.Bid,
	})
}

//物品打造-炼药-烹饪
func (mwCtr *makeWineController) ForgeItem(ac common.IActionCtx, BuildId int64, forgeId int32) {
	//通过建筑ID查找城邦信息
	_, resBody, err := mwCtr.RequestSvc(svc.CityBuilding, ac.Tid(), 0, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{
		BuildingId: BuildId,
	})
	if err > 0 {
		app.Log().Error("查询城邦信息失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	buildInfo := resBody.(*msg_dt.ResCityBuildingGetBuilding)
	//查询是否是当前的建筑类型
	//if buildInfo.Building.BuildingType != Type {
	//	app.Log().Error("请求建筑类型不一致",utils.M{"errCode":err})
	//	ac.ResErr(err_gm.ErrQueryFailed)
	//	return
	//}
	//获取角色信息
	CharacterInfo,IErr := mwCtr.RequestCharacterInfoQueryByCid(ac.Tid(),nil,ac.Cid())
	if IErr.IsNotNil(){
		app.Log().Error("查询角色失败", utils.M{"errCode": IErr.Error()})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if CharacterInfo.CityId != buildInfo.Building.CityId {
		app.Log().Error("该角色并未加入这个城邦", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	forgeTemplate := mwCtr.forgeTsv[forgeId]
	if forgeTemplate == nil {
		app.Log().Error("查询锻造配置失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if forgeTemplate.BuildValue > buildInfo.Building.Level {
		app.Log().Error("建筑等级未达到", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if forgeTemplate.Gold > CharacterInfo.Gold {
		app.Log().Error("金币不足", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	//扣材料
	needProps := forgeTemplate.Item
	subtractProps := make(map[int32]int32, 0)
	for _, v := range needProps {
		subtractProps[v[0]] = v[1]
	}
	//获取锻炼物品的概率
	rate := forgeTemplate.Forgepr[0]
	rand.Seed(time.Now().UnixNano())
	randNum := rand.Intn(100) + 1
	var hitPropId int32 = 0
	if randNum <= int(rate) {
		hitPropId = forgeId
	}
	mwCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		//todo 扣金币
		if forgeTemplate.Gold >0 {
			_, _, errCode := tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, map[character_info_e.Enum]int32{
				character_info_e.Enum(2001): -forgeTemplate.Gold,
			}, ac.Cid())
			if errCode > 0 {
				app.Log().TError(ac.Tid(), utils.NewError("character lack of money",
					utils.M{"need purchase quantity": forgeTemplate.Gold}))
				ac.ResErr(err_gm.ErrUpdateFailed)
				return false
			}
		}

		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtract, &msg_dt.ReqBackpackSubtract{
			PropType:model.BackpackTypeWarehouseCombine,
			Props:          subtractProps,
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec.IsNotNil() {
			app.Log().Debug("扣除物品失败", utils.M{"errCode": ec})
			ac.ResErr(ec)
			return false
		}
		//开始锻造-加新材料
		if hitPropId > 0 {
			props := map[int32]int32{
				hitPropId: 1,
			}
			_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
				Props:          props,
				Cid:            ac.Cid(),
				IsFullSendMail: true,
			})
			if ec > 0 {
				app.Log().Error("背包物品添加失败", utils.M{"errCode": err})
				return false
			}
		}
		return true
	})
	ac.ResOk(&msg_gm.ResLifeForgeItem{
		PropId: hitPropId,
	})
	return
}

func (mwCtr *makeWineController) DecomposeItem(ac common.IActionCtx, BuildId int64, Bid int64) {
	//通过建筑ID查找城邦信息
	_, resBody, err := mwCtr.RequestSvc(svc.CityBuilding, ac.Tid(), 0, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{
		BuildingId: BuildId,
	})
	if err > 0 {
		app.Log().Error("查询城邦信息失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	buildInfo := resBody.(*msg_dt.ResCityBuildingGetBuilding)
	//查询是否是当前的建筑类型
	//if buildInfo.Building.BuildingType != Type {
	//	app.Log().Error("请求建筑类型不一致",utils.M{"errCode":err})
	//	ac.ResErr(err_gm.ErrQueryFailed)
	//	return
	//}
	//获取角色信息
	uid, ok := app.UserLogic().GetUserUid(ac.Addr())
	if !ok {
		app.Log().Error("查询角色失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	_, CidRes, err := mwCtr.RequestSvc(svc.CharacterInfo, ac.Tid(), 0, msg_dt.CdCharacterInfo, &msg_dt.ReqCharacterInfo{
		Uid: uid,
	})
	if err > 0 {
		app.Log().Error("查询角色信息失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	CidInfo := CidRes.(*msg_dt.ResCharacterInfo)
	if CidInfo.CharacterInfo.CityId != buildInfo.Building.CityId {
		app.Log().Error("该角色并未加入这个城邦", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	//查询背包物品
	_, resBody, err = mwCtr.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackGetPropById, &msg_dt.ReqBackpackGetPropById{
		Bid: Bid,
		Cid:ac.Cid(),
	})
	if err.IsNotNil(){
		app.Log().Error("背包物品查询失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	backpackInfo := resBody.(*msg_dt.ResBackpackGetPropById)

	decomposeTemplate,has := mwCtr.decomposeTsv[backpackInfo.Props.PropId]
	if !has {
		app.Log().Error("该物品不允许分解", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//金币-策划:未给出明确答复,先暂定50
	var Gold int32 = 50
	if Gold > CidInfo.CharacterInfo.Gold {
		app.Log().Error("金币不足", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//概率
	RandomSlice := decomposeTemplate.DecomposeList
	RandomMap := make(map[int32]int32, 0)
	rand.Seed(time.Now().Unix())
	//获取锻炼物品的概率
	RandInt64 := func(min, max int64) int64 {
		if min >= max  {
			return max
		}
		return rand.Int63n(max-min) + min
	}
	for _, v := range RandomSlice {
		rateRand := int32(rand.Intn(101))
		if rateRand <= v[1] {
			hitPropId := v[0]
			randNum := RandInt64(int64(v[2]), int64(v[3])+1)
			RandomMap[hitPropId] = int32(randNum)
		}
	}

	mwCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		//扣金币
		_, _, errCode := tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, map[character_info_e.Enum]int32{
			character_info_e.Enum(2001): -Gold,
		}, ac.Cid())
		if errCode != 0 {
			app.Log().TError(ac.Tid(), utils.NewError("character lack of money",utils.M{ "need purchase quantity": Gold}))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return false
		}
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtractById, &msg_dt.ReqBackpackSubtractById{
			PropType:       model.BackpackTypeDefault,
			Props:          map[int64]int32{Bid: 1},
			Cid:            ac.Cid(),
		})
		if ec.IsNotNil() {
			app.Log().Debug("扣除物品失败", utils.M{"errCode": ec})
			ac.ResErr(ec)
			return false
		}
		//开始锻造-加新材料
		if len(RandomMap) > 0 {
			_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
				Props:          RandomMap,
				Cid:            ac.Cid(),
				IsFullSendMail: true,
			})
			if ec > 0 {
				app.Log().Error("背包物品添加失败", utils.M{"errCode": err})
				return false
			}
		}
		return true
	})
	ac.ResOk(&msg_gm.ResLifeDecompose{
		Props: RandomMap,
	})
	return
}


//采水
func (mwCtr *makeWineController) Intake(ac common.IActionCtx, BuildId int64) {
	//判断背包水资源最新的时间
	addProps := map[int32]int32{3011025: 1}
	_, resBody, err := mwCtr.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackGetPropsByPropId, &msg_dt.ReqBackpackGetPropsByPropId{
		PropId:3011025,
		Cid:ac.Cid(),
	})
	var lastTime int64= 0
	if err.IsNil() {
		props := resBody.(*msg_dt.ResBackpackGetPropsByPropId)
		for _,item :=range props.Props{
			if item.Updated > lastTime{
				lastTime = item.Updated
			}
		}
	}
	if (time.Now().UnixNano() / 1000000) < lastTime + 4000 {
		app.Log().Debug("距离上一次采水时间太近", nil)
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	mwCtr.Transaction(ac.Tid(), func(tx common.ITx) bool {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
			Props: addProps,
			Cid:            ac.Cid(),
			IsFullSendMail: false,
		})
		if ec > 0 {
			return false
		}
		return true
	})
	ac.ResOk(&msg_gm.ResLifeIntake{
		Props: addProps,
	})
}

func (mwCtr *makeWineController) FormatMakeWineSingle(lists ...*model.CityMakeWine) (sliceList *msg_gm.LifeWine) {
	for _, items := range lists {
		sliceList = &msg_gm.LifeWine{
			WineId:  items.Id,
			Slot:    items.Slot,
			EndTime: items.EndTime,
			PropId:  items.PropId,
			Num:     items.Num,
			Status:  2, //酿造中
		}
	}
	return
}

func (mwCtr *makeWineController) FormatMakeWine(bLevel int32, lists ...*model.CityMakeWine) (sliceList []*msg_gm.LifeWine) {
	tavernSlice := mwCtr.tavernTsv.TsvSlice
	maxLevel := tavernSlice[len(tavernSlice)-1].Volume
	currentLevel := mwCtr.tavernTsv.TsvMap[bLevel].Volume
	currentMap := make(map[int32]int32, 0)
	for ; maxLevel > 0; maxLevel-- {
		var currentStatus int32 = 0
		if currentLevel >= maxLevel {
			currentStatus = 1
		}
		currentMap[maxLevel] = currentStatus
		sliceList = append(sliceList, &msg_gm.LifeWine{
			Slot:   maxLevel,
			Status: currentStatus,
		})
	}

	if len(lists) > 0 {
		for _, item := range sliceList {
			for _, lis := range lists {
				if item.Slot == lis.Slot {
					item.WineId = lis.Id
					item.EndTime = lis.EndTime
					item.PropId = lis.PropId
					item.Num = lis.Num

					if lis.EndTime < time.Now().Unix() {
						item.Status = 3
					} else {
						item.Status = 2
					}
				}
			}
		}

	}

	return
}
