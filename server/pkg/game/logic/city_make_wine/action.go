package city_make_wine

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
)

type IMakeWineController interface {
	common.IBaseController
}

func (mwCtr *makeWineController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdLifeGetCurWines, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqLifeGetCurWines)
			mwCtr.GetMakeWineList(ac, req.BuildId)
			return
		}},

		{MsgCode: msg_gm.CdLifeStartWine, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqLifeStartWine)
			mwCtr.MakeWineStart(ac, req.BuildId,req.PropId,req.Slot)
			return
		}},

		{MsgCode: msg_gm.CdLifeGainOverWine, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqLifeGainOverWine)
			mwCtr.MakeWineReap(ac, req.WineId)
			return
		}},

		{MsgCode: msg_gm.CdLifeCancelWine, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqLifeCancelWine)
			mwCtr.MakeWineCancel(ac, req.WineId)
			return
		}},
		//打造
		{MsgCode: msg_gm.CdLifeForgeItem, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqLifeForgeItem)
			mwCtr.ForgeItem(ac,req.BuildId, req.ForgeId)
			return
		}},
		//分解
		{MsgCode: msg_gm.CdLifeDecompose, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqLifeDecompose)
			mwCtr.DecomposeItem(ac,req.BuildId, req.BId)
			return
		}},
		//采水
		{MsgCode: msg_gm.CdLifeIntake, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqLifeIntake)
			mwCtr.Intake(ac,req.BuildId)
			return
		}},
	}
}