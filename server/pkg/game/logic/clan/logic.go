package clan

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/clan_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
)

type clanController struct {
	common.IBaseController
}

func NewClanController() IClanController {
	return &clanController{
		IBaseController: common.NewBaseController(),
	}
}

func (c *clanController) AfterInit() error {
	//go func() {
	//	time.Sleep(3 * time.Second)
	//	c.CityGetCityList(common.NewActionCtx("",0,0))
	//}()

	return nil
}

func (c *clanController) CreateClan(ac common.IActionCtx, clanName, clanSurname string) {
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		resClan, errCode := tx.Try(svc.Clan, ac.Cid(), msg_dt.CdClanTryCreate, &msg_dt.ReqClanTryCreate{
			Cid:         ac.Cid(),
			ClanName:    clanName,
			ClanSurname: clanSurname,
			MemberList:  []int64{ac.Cid()},
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrCreateFailed)
			return false
		}
		clan := resClan.(*msg_dt.ResClanTryCreate).Clan
		_, errCode = tx.Try(svc.CharacterInfo, ac.Cid(), msg_dt.CdTryCommonUpdateById, &msg_dt.ReqTryCommonUpdateById{
			Model: model_e.CharacterInfo,
			UpData: map[character_info_e.Enum]interface{}{
				character_info_e.ClanId:       clan.Id,
				character_info_e.ClanName:     clan.ClanName,
				character_info_e.ClanIdentity: int32(clan_e.Patriarch),
			},
			ModelId: ac.Cid(),
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("update failed", nil))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}

		protoClan := c.ProtoClan(clan)
		memberList, errGmCode := c.ProtoClanMember(ac.Tid(), clan.GetClanMemberList())
		if errGmCode > 0 {
			ac.ResErr(errGmCode)
			return
		}
		ac.ResOk(&msg_gm.ResClanCreate{
			Clan:           protoClan,
			ClanMemberList: memberList,
		})
		return true
	})
}

func (c *clanController) LoadClan(ac common.IActionCtx) {
	character, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.ClanId}, ac.Cid())
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if character.ClanId == 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	_, resClan, errCode := c.RequestSvc(svc.Clan, ac.Tid(), character.ClanId, msg_dt.CdClanLoad, &msg_dt.ReqClanLoad{
		ClanId: character.ClanId,
		Cid:    ac.Cid(),
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	resClanLoad := resClan.(*msg_dt.ResClanLoad)

	protoClan := c.ProtoClan(resClanLoad.Clan)
	protoClan.IsClanSignIn = resClanLoad.IsSignIn
	memberList, errGmCode := c.ProtoClanMember(ac.Tid(), resClanLoad.Clan.GetClanMemberList())
	if errGmCode > 0 {
		ac.ResErr(errGmCode)
		return
	}
	ac.ResOk(&msg_gm.ResClanLoad{
		Clan:           protoClan,
		ClanMemberList: memberList,
	})
}

func (c *clanController) UpClanNameAndClanSurname(ac common.IActionCtx, clanName, clanSurname string) {
	character, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.ClanId}, ac.Cid())
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if character.ClanId == 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode = tx.Try(svc.Clan, character.ClanId, msg_dt.CdClanTryUpNameOrSurname, &msg_dt.ReqClanTryUpNameOrSurname{
			ClanId:      character.ClanId,
			ClanName:    clanName,
			ClanSurname: clanSurname,
		})
		if errCode > 0 {
			return
		}
		ac.ResOk(&msg_gm.ResClanUpNameOrSurname{})
		return true
	})
}

func (c *clanController) UpClanDeclaration(ac common.IActionCtx, declaration string) {
	character, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.ClanId}, ac.Cid())
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if character.ClanId == 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode = tx.Try(svc.Clan, character.ClanId, msg_dt.CdClanTryUpDeclaration, &msg_dt.ReqClanTryUpDeclaration{
			ClanId:          character.ClanId,
			ClanDeclaration: declaration,
		})
		if errCode > 0 {
			return
		}
		ac.ResOk(&msg_gm.ResClanUpDeclaration{})
		return true
	})
}

func (c *clanController) PageSearchClan(ac common.IActionCtx, page, size int32) {
	_, body, errCode := c.RequestSvc(svc.Clan, ac.Tid(), ac.Cid(), msg_dt.CdClanPageSearch, &msg_dt.ReqClanPageSearch{
		Page: page,
		Size: size,
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResClanPageSearch)
	clanList, errCode := c.ProtoClanAll(ac.Tid(), res.ClanList)
	if errCode > 0 {
		ac.ResErr(errCode)
		return
	}
	ac.ResOk(&msg_gm.ResClanPageSearch{
		Total: res.Total,
		Clans: clanList,
	})
}

func (c *clanController) ClanSignIn(ac common.IActionCtx) {
	characterInfo, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.ClanId}, ac.Cid())
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if characterInfo.ClanId == 0 {
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode = tx.Try(svc.Clan, ac.Cid(), msg_dt.CdClanTrySignIn, &msg_dt.ReqClanTrySignIn{
			Cid: ac.Cid(),
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		//添加货币
		_, _, errCode := tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, map[character_info_e.Enum]int32{
			character_info_e.Enum(2001): 1000,
		}, ac.Cid())
		if errCode != 0 {
			app.Log().TError(ac.Tid(), utils.NewError("character lack of money",
				utils.M{"$_type": 2001, "num": 1000}))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return false
		}
		ac.ResOk(&msg_gm.ResClanSignIn{})
		return true
	})
}

func (c *clanController) ClanUpIntroduction(ac common.IActionCtx, introduction string) {
	character, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.Birthday, character_info_e.ClanId}, ac.Cid())
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if character.ClanId == 0 {
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.TryUpdateCharacterInfoByCid(map[character_info_e.Enum]interface{}{character_info_e.Introduction: introduction}, ac.Cid())
		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResClanUpIntroduction{})
		return true
	})
	return
}

func (c *clanController) ClanUpBirthday(ac common.IActionCtx, birthday int32) {
	character, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.Birthday, character_info_e.ClanId}, ac.Cid())
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if character.ClanId == 0 {
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	if character.Birthday > 0 {
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	//var LOC, _ = time.LoadLocation("Asia/Shanghai")
	//st := "2020-03-32 00:00:00"
	//fmt.Printf("%s\n", st)
	//
	//t, _ := time.ParseInLocation("2006-01-02 15:04:05", st, LOC)
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.TryUpdateCharacterInfoByCid(map[character_info_e.Enum]interface{}{character_info_e.Birthday: birthday}, ac.Cid())
		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResClanUpBirthday{})
		return true
	})
}

func (c *clanController) ProtoClan(clan *model.Clan) *msg_gm.Clan {
	return &msg_gm.Clan{
		Id:              clan.Id,
		ClanName:        clan.ClanName,
		ClanSurname:     clan.ClanSurname,
		ClanLevel:       clan.ClanLevel,
		ClanExperience:  clan.ClanExperience,
		ClanDeclaration: clan.ClanDeclaration,
		ClanLeaderId:    clan.ClanLeaderId,
		ClanPeopleNum:   clan.ClanPeopleNum,
	}
}

func (c *clanController) ProtoClanAll(tid int64, clanList []*model.Clan) ([]*msg_gm.Clan, err_gm.ErrCode) {
	res := make([]*msg_gm.Clan, 0)
	cidList := make([]int64, 0)
	if len(clanList) == 0 {
		return res, 0
	}

	for _, clan := range clanList {
		cidList = append(cidList, clan.ClanLeaderId)
	}
	characterInfoQueryByCidMap, dtErrCode := c.RequestCharacterInfoQueryByCids(tid, []character_info_e.Enum{character_info_e.Nickname, character_info_e.Age}, cidList...)
	if dtErrCode > 0 {
		return nil, err_gm.ErrQueryFailed
	}
	for _, clan := range clanList {
		character, ok := characterInfoQueryByCidMap[clan.ClanLeaderId]
		if ok {
			res = append(res, &msg_gm.Clan{
				Id:              clan.Id,
				ClanName:        clan.ClanName,
				ClanSurname:     clan.ClanSurname,
				ClanLevel:       clan.ClanLevel,
				ClanExperience:  clan.ClanExperience,
				ClanDeclaration: clan.ClanDeclaration,
				ClanLeaderId:    clan.ClanLeaderId,
				ClanPeopleNum:   clan.ClanPeopleNum,
				ClanLeaderName:  character.Nickname,
			})
		}

	}

	return res, 0
}

func (c *clanController) ProtoClanMember(tid int64, memberIds []int64) (clanMemberList []*msg_gm.ClanMember, errCode err_gm.ErrCode) {
	clanMemberList = make([]*msg_gm.ClanMember, 0)
	if len(memberIds) > 0 {
		characterInfoQueryByCidMap, dtErrCode := c.RequestCharacterInfoQueryByCids(tid,
			[]character_info_e.Enum{
				character_info_e.Nickname,
				character_info_e.Age,
				character_info_e.Introduction,
				character_info_e.Birthday}, memberIds...)
		if dtErrCode > 0 {
			return nil, err_gm.ErrQueryFailed
		}
		for _, cid := range memberIds {
			characterInfo, ok := characterInfoQueryByCidMap[cid]
			if ok {
				clanMemberList = append(clanMemberList, &msg_gm.ClanMember{
					Cid:          cid,
					Name:         characterInfo.Nickname,
					Age:          characterInfo.Age,
					Introduction: characterInfo.Introduction,
					Birthday:     characterInfo.Birthday,
				})
			}

		}
	}

	return
}
