package clan

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/msg_dt"
)

type IClanController interface {
	common.IBaseController
}

// 消息处理器
func (c *clanController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdClanCreate, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqClanCreate)
			c.CreateClan(ac,req.ClanName,req.ClanSurname)
		}},
		{MsgCode: msg_gm.CdClanLoad, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			c.LoadClan(ac)
		}},
		{MsgCode: msg_gm.CdClanUpNameOrSurname, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqClanUpNameOrSurname)
			c.UpClanNameAndClanSurname(ac,req.UpName,req.UpSurname)
		}},
		{MsgCode: msg_gm.CdClanUpDeclaration, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqClanUpDeclaration)
			c.UpClanDeclaration(ac,req.UpDeclaration)
		}},
		{MsgCode: msg_gm.CdClanPageSearch, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqClanPageSearch)
			c.PageSearchClan(ac,req.Page,req.Size_)
		}},
		{MsgCode: msg_gm.CdClanSignIn, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			c.ClanSignIn(ac)
		}},
		{MsgCode: msg_gm.CdClanUpIntroduction, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqClanUpIntroduction)
			c.ClanUpIntroduction(ac,req.Introduction)
		}},
		{MsgCode: msg_gm.CdClanUpBirthday, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqClanUpBirthday)
			c.ClanUpBirthday(ac,req.BirthdayTime)
		}},
	}
}

func (c *clanController) Events() []common.Event {
	return []common.Event{
		{MsgCode: msg_dt.CdEveClanNoticeChange, Action: func(ac common.IActionCtx, eveBody interface{}) {
			event := eveBody.(*msg_dt.EveClanNoticeChange)
			c.SendToCharacters(event.CidList, msg_gm.CdNoticeClanChange, &msg_gm.NoticeClanChange{
				UpType: event.UpType,
				ClanName: event.ClanName,
				ClanSurname: event.ClanSurname,
				ClanDeclaration: event.ClanDeclaration,
			})
		}},
	}
}
