package team

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
)

type teamController struct {
	common.IBaseController
}

func NewTeamController() ITeamController {
	return &teamController{
		IBaseController:     common.NewBaseController(),
	}
}



func (t *teamController) AfterInit() error {
	return nil
}


func (t *teamController) LoadTeam(ac common.IActionCtx)  {
	characterInfo, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(),[]character_info_e.Enum{character_info_e.TeamId}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query character nick name failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	if characterInfo.TeamId == 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	_, resTeam, errCode := t.RequestSvc(svc.Team, ac.Tid(), 0, msg_dt.CdTeamGetTeamById, &msg_dt.ReqTeamGetTeamById{TeamId: characterInfo.TeamId})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := resTeam.(*msg_dt.ResTeamGetTeamById)
	if res.Team == nil {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	resLoadGroup, errCode := t.PackTeamLoad(ac, res.Team)
	if errCode > 0 {
		ac.ResErr(errCode)
		return
	}
	ac.ResOk(resLoadGroup)

}

func (t *teamController) CreateTeam(ac common.IActionCtx)  {
	characterInfo, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(),
		[]character_info_e.Enum{character_info_e.TeamId,character_info_e.CityId, character_info_e.Nickname},
		ac.Cid())
	if errCode != 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	if characterInfo.TeamId > 0 {
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		resBody, errCode := tx.Try(svc.Team, ac.Cid(), msg_dt.CdTryTeamCreate, &msg_dt.ReqTryTeamCreate{Cid: ac.Cid(),
			CharacterNick: characterInfo.Nickname,CityId: characterInfo.CityId})
		if errCode != 0 {
			app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
			ac.ResErr(err_gm.ErrCreateFailed)
			return
		}
		team := resBody.(*msg_dt.ResTryTeamCreate).Team

		_,errCode = tx.Try(svc.CharacterInfo,ac.Cid(),msg_dt.CdTryCommonUpdateById,&msg_dt.ReqTryCommonUpdateById{
			Model:   model_e.CharacterInfo,
			UpData:  map[character_info_e.Enum]interface{}{character_info_e.TeamId: team.Id},
			ModelId: ac.Cid(),
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("update failed", nil))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResCreateTeam{
			Info: &msg_gm.GroupInfo{
				TeamId: team.Id,
				GroupName: team.TeamName,
				CurrentNumber: team.CurrentNumber,
				GroupType: int32(team.TeamType),
				IsAutoAgree: team.AutoAgree,
				LeaderId: team.LeaderId,
				TeamAims: &msg_gm.TeamAims{
					TeamAimsId: team.TeamAimsTemId,
					MinAgeNum: team.MinAgeClaim,
					MaxAgeNum: team.MaxAgeClaim,
					PowerNum: team.PowerClaim,
				},
				GroupMemberList: []*msg_gm.GroupMember{
					{Cid: ac.Cid(),NickName: characterInfo.Nickname},
				},
			},
		})
		return true
	})
}

func (t *teamController) ApplyJoinTeam(ac common.IActionCtx,teamId int64)  {
	characterInfo, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.TeamId, character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(),utils.NewError("query character cityId and TeamId fail",utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if characterInfo.TeamId > 0 {
		app.Log().TError(ac.Tid(),utils.NewError("character already join team",utils.M{"cid": ac.Cid(),"teamId": characterInfo.TeamId}))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}

	if characterInfo.CityId == 0 {
		app.Log().TError(ac.Tid(),utils.NewError("character not join city",utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	_, resTeam, errCode := t.RequestSvc(svc.Team, ac.Tid(), ac.Cid(), msg_dt.CdTeamGetTeamById, msg_dt.ReqTeamGetTeamById{
		TeamId: teamId,
	})
	if errCode > 0 {
		app.Log().TError(ac.Tid(),utils.NewError("query character cityId and TeamId fail",utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if resTeam == nil {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if resTeam == nil {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	team := resTeam.(*msg_dt.ResTeamGetTeamById).Team
	if team.AutoAgree {
		t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
			_, errCode = tx.Try(svc.Team, teamId, msg_dt.CdTryTeamAutoAgreeJoin, &msg_dt.ReqTryTeamAutoAgreeJoin{
				Cid:  ac.Cid(),
				Team: team,
			})
			if errCode > 0 {
				app.Log().TError(ac.Tid(),utils.NewError("query character cityId and TeamId fail",utils.M{"cid": ac.Cid()}))
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
			_,errCode = tx.TryUpdateCharacterInfoByCid(map[character_info_e.Enum]interface{}{character_info_e.TeamId: team.Id},ac.Cid())
			if errCode > 0 {
				app.Log().TError(ac.Tid(), utils.NewError("update failed", nil))
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
			ac.ResOk(&msg_gm.ResApplyTeam{})
			return true
		})
	}else {
		t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
			tx.Try(svc.Team,ac.Cid(),msg_dt.CdTeamApplyJoinTeam, &msg_dt.ReqTeamApplyJoinTeam{Cid: ac.Cid(),
				CityId: characterInfo.CityId,TeamId: teamId})
			if errCode > 0 {
				ac.ResErr(err_gm.ErrCreateFailed)
				return
			}
			ac.ResOk(&msg_gm.ResApplyTeam{})
			return true
		})
	}

}

func (t *teamController) InviteJoinTeam(ac common.IActionCtx,oid int64)  {
	characterInfo, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.TeamId, character_info_e.CityId}, oid)
	if errCode > 0 {
		app.Log().TError(ac.Tid(),utils.NewError("query character cityId and TeamId fail",utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if characterInfo.TeamId > 0 {
		app.Log().TError(ac.Tid(),utils.NewError("character already join team",utils.M{"cid": ac.Cid(),"teamId": characterInfo.TeamId}))
		ac.ResErr(err_gm.ErrCreateFailed)
	}

	if characterInfo.CityId == 0 {
		app.Log().TError(ac.Tid(),utils.NewError("character not join city",utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrCreateFailed)
	}
	_, _, errCode = t.RequestSvc(svc.Team, ac.Tid(), 0, msg_dt.CdTeamInviteJoinTeam, &msg_dt.ReqTeamInviteJoinTeam{Cid: ac.Cid(),Oid: oid,CityId: characterInfo.CityId})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	ac.ResOk(&msg_gm.ResInviteJoinTeam{})
}

func (t *teamController) AgreeApplyTeam(ac common.IActionCtx,oid int64)  {
	cidList := make([]int64,0)
	var teamId int64
	if oid > 0 {
		characterInfo, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.TeamId, character_info_e.CityId}, oid)
		if errCode > 0 {
			app.Log().TError(ac.Tid(),utils.NewError("query character cityId and TeamId fail",utils.M{"cid": ac.Cid()}))
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		if characterInfo.TeamId > 0 {
			_, _, errCode = t.RequestSvc(svc.Team, ac.Tid(), 0, msg_dt.CdTeamRefuseApplyJoinTeam, &msg_dt.ReqTeamRefuseApplyJoinTeam{Cid: ac.Cid(),Oid: oid})
			if errCode > 0 {
				ac.ResErr(err_gm.ErrUpdateFailed)
				app.Log().TError(ac.Tid(),utils.NewError("del character team apply exit",utils.M{"teamLeader": ac.Cid(),"applyCid": oid}))
				return
			}

			app.Log().TError(ac.Tid(),utils.NewError("character already join team",utils.M{"cid": oid,"teamId": characterInfo.TeamId}))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		_, body, errCode := t.RequestSvc(svc.Team, ac.Tid(), 0, msg_dt.CdTeamAgreeApplyJoinTeam, &msg_dt.ReqTeamAgreeApplyJoinTeam{Cid: ac.Cid(),Oid: oid})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrCreateFailed)
			return
		}
		res := body.(*msg_dt.ResTeamAgreeApplyJoinTeam)
		cidList = append(cidList,res.CidList...)
		teamId = res.TeamId
	}else{
		_, body, errCode := t.RequestSvc(svc.Team, ac.Tid(), ac.Cid(), msg_dt.CdTeamApplyList, &msg_dt.ReqTeamApplyList{
			Cid: ac.Cid(),
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		res := body.(*msg_dt.ResTeamApplyList)
		for _,apply := range res.ApplyList {
			characterInfo, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.TeamId, character_info_e.CityId}, apply.Cid)
			if errCode > 0 {
				app.Log().TError(ac.Tid(),utils.NewError("query character cityId and TeamId fail",utils.M{"cid": ac.Cid()}))
				ac.ResErr(err_gm.ErrQueryFailed)
				return
			}
			if characterInfo.TeamId > 0 {
				_, _, errCode = t.RequestSvc(svc.Team, ac.Tid(), 0, msg_dt.CdTeamRefuseApplyJoinTeam, &msg_dt.ReqTeamRefuseApplyJoinTeam{Cid: ac.Cid(),Oid: apply.Cid})
				if errCode > 0 {
					app.Log().TError(ac.Tid(),utils.NewError("del character team apply exit",utils.M{"teamLeader": ac.Cid(),"applyCid": apply.Cid}))
					continue
				}
				app.Log().TError(ac.Tid(),utils.NewError("character already join team",utils.M{"cid": oid,"teamId": characterInfo.TeamId}))
				continue
			}
			_, body, errCode := t.RequestSvc(svc.Team, ac.Tid(), 0, msg_dt.CdTeamAgreeApplyJoinTeam, &msg_dt.ReqTeamAgreeApplyJoinTeam{Cid: ac.Cid(),Oid: apply.Cid})
			if errCode > 0 {
				ac.ResErr(err_gm.ErrCreateFailed)
				return
			}
			res := body.(*msg_dt.ResTeamAgreeApplyJoinTeam)
			cidList = append(cidList,res.CidList...)
			if teamId == 0 {
				teamId = apply.TeamId
			}
		}
	}

	if len(cidList) > 0 {
		t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
			var errGmCode err_gm.ErrCode
			for _,cid := range cidList {
				_,errCode := tx.TryUpdateCharacterInfoByCid(map[character_info_e.Enum]interface{}{character_info_e.TeamId: teamId},cid)
				if errCode > 0 {
					app.Log().TError(ac.Tid(), utils.NewError("update failed", nil))
					errGmCode = err_gm.ErrUpdateFailed
					break
				}
			}
			if errGmCode > 0 {
				ac.ResErr(errGmCode)
				return
			}
			ac.ResOk(&msg_gm.ResLeaderApproval{Cid: oid})
			return true
		})
		return
	}
	ac.ResOk(&msg_gm.ResLeaderApproval{Cid: oid})

}

func (t *teamController) RefuseApplyJoinTeam(ac common.IActionCtx,oid int64)  {
	_, _, errCode := t.RequestSvc(svc.Team, ac.Tid(), 0, msg_dt.CdTeamRefuseApplyJoinTeam, &msg_dt.ReqTeamRefuseApplyJoinTeam{Cid: ac.Cid(),Oid: oid})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	ac.ResOk(&msg_gm.ResLeaderApproval{Cid: oid})
}

func (t *teamController) OneClickApply(ac common.IActionCtx,aimsTempId int32)  {
	characterInfo, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{
		character_info_e.TeamId, character_info_e.Age,character_info_e.BattlePower, character_info_e.CityId},
		ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(),utils.NewError("query character cityId and TeamId fail",utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if characterInfo.TeamId > 0 {
		app.Log().TError(ac.Tid(),utils.NewError("character already join team",utils.M{"cid": ac.Cid(),"teamId": characterInfo.TeamId}))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	_, body, errCode := t.RequestSvc(svc.Team, ac.Tid(), 0, msg_dt.CdTeamOneClickApply, &msg_dt.ReqTeamOneClickApply{
		Cid: ac.Cid(),
		AimsTempId: aimsTempId,
		Age: characterInfo.Age,
		BattlePower: characterInfo.BattlePower,
	})

	if errCode > 0 {
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	resTeamOneClickApply := body.(*msg_dt.ResTeamOneClickApply)
	if resTeamOneClickApply.TeamId > 0 {
		t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
			_,errCode = tx.TryUpdateCharacterInfoByCid(map[character_info_e.Enum]interface{}{character_info_e.TeamId: resTeamOneClickApply.TeamId},ac.Cid())
			if errCode > 0 {
				app.Log().TError(ac.Tid(), utils.NewError("update failed", nil))
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
			return true
		})
	}
	ac.ResOk(&msg_gm.ResOneClickApply{})
}

func (t *teamController) UpdateTeamAims(ac common.IActionCtx,teamId int64,teamAims *msg_gm.TeamAims)  {
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.Try(svc.Team, teamId, msg_dt.CdTeamUpdateTeamArms, &msg_dt.ReqTeamUpdateTeamArms{
			Cid: ac.Cid(),
			TeamArms: msg_dt.TeamArms{
				TeamAimsId: teamAims.TeamAimsId,
				MinAgeNum: teamAims.MinAgeNum,
				MaxAgeNum: teamAims.MaxAgeNum,
				PowerNum: teamAims.PowerNum,
			},
		})

		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResSetTeamAims{})
		return true
	})
}

func (t *teamController) LeaveTeam(ac common.IActionCtx)  {
	characterInfo, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(),
		[]character_info_e.Enum{character_info_e.TeamId},
		ac.Cid())
	if errCode != 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}

	if characterInfo.TeamId == 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_,errCode = tx.Try(svc.Team,characterInfo.TeamId, msg_dt.CdTryTeamLeaveTeam, &msg_dt.ReqTryTeamLeaveTeam{
			Cid: ac.Cid(),
			TeamId: characterInfo.TeamId,
		})
		if errCode != 0 {
			app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
			ac.ResErr(err_gm.ErrDelFailed)
			return
		}
		_,errCode = tx.Try(svc.CharacterInfo,ac.Cid(),msg_dt.CdTryCommonUpdateById,&msg_dt.ReqTryCommonUpdateById{
			Model:   model_e.CharacterInfo,
			UpData:  map[character_info_e.Enum]interface{}{character_info_e.TeamId: int64(0)},
			ModelId: ac.Cid(),
		})
		if errCode != 0 {
			app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		return true
	})
	ac.ResOk(&msg_gm.ResLeaveTeam{})
}

func (t *teamController) TransferTeam(ac common.IActionCtx,oid int64)  {
	_, _, errCode := t.RequestSvc(svc.Team, ac.Tid(), ac.Cid(), msg_dt.CdTeamTransferTeam, &msg_dt.ReqTeamTransferTeam{
		Cid: ac.Cid(),
		Oid: oid,
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	ac.ResOk(&msg_gm.ResTransferTeam{})
}

func (t *teamController) DisbandTeam(ac common.IActionCtx) {
	_, _, errCode := t.RequestSvc(svc.Team, ac.Tid(), ac.Cid(), msg_dt.CdTeamDisbandTeam, &msg_dt.ReqTeamDisbandTeam{
		Cid: ac.Cid(),
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrDelFailed)
		return
	}
	ac.ResOk(&msg_gm.ResDisbandTeam{})
}

func (t *teamController) PleaseLeaveTeam(ac common.IActionCtx,oid int64) {
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.Try(svc.Team,0,msg_dt.CdTryTeamPleaseLeaveTeam, &msg_dt.ReqTryTeamPleaseLeaveTeam{
			Cid: ac.Cid(),
			Oid: oid,
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		_,errCode = tx.Try(svc.CharacterInfo,oid,msg_dt.CdTryCommonUpdateById,&msg_dt.ReqTryCommonUpdateById{
			Model:   model_e.CharacterInfo,
			UpData:  map[character_info_e.Enum]interface{}{character_info_e.TeamId: int64(0)},
			ModelId: oid,
		})
		if errCode != 0 {
			app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}

		ac.ResOk(&msg_gm.ResPleaseLeaveTeam{})
		return true
	})
}

func (t *teamController) UpdateTeamAutoAgree(ac common.IActionCtx)  {
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.Try(svc.Team,0,msg_dt.CdTryTeamUpdateTeamAutoJoin, &msg_dt.ReqTryTeamUpdateTeamAutoJoin{
			Cid: ac.Cid(),
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResSetTeamAutoJoin{})
		return true
	})
}

func (t *teamController) PageQueryTeam(ac common.IActionCtx,page,size,teamAimsId int32)  {
	character,errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	_,resBody,errCode := t.RequestSvc(svc.Team,ac.Tid(),ac.Cid(),msg_dt.CdTeamPageQueryTeam,&msg_dt.ReqTeamPageQueryTeam{
		CityId: character.CityId,
		Page: page,
		Size: size,
		AimsId: teamAimsId,
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	pageTeam := resBody.(*msg_dt.ResTeamPageQueryTeam)
	teamList := make([]*msg_gm.GroupInfo,0)

	for _,team := range pageTeam.TeamList {
		memberList := model.GetTeamMemberList(team.TeamMember)
		members := make([]*msg_gm.GroupMember,0)
		characters, errCode := t.RequestCharacterInfoQueryByCids(ac.Tid(),
			[]character_info_e.Enum{character_info_e.Id, character_info_e.Nickname, character_info_e.CityId},
			memberList...)
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		for c,characterInfo1 := range characters {
			members = append(members,&msg_gm.GroupMember{
				Cid: c,
				NickName: characterInfo1.Nickname,
				Age: characterInfo1.Age,
				Power: characterInfo1.BattlePower,
			})
		}
		teamList = append(teamList,&msg_gm.GroupInfo{
			TeamId: team.Id,
			GroupName: team.TeamName,
			CurrentNumber: team.CurrentNumber,
			GroupType: int32(team.TeamType),
			IsAutoAgree: team.AutoAgree,
			LeaderId: team.LeaderId,
			GroupMemberList: members,
			TeamAims: &msg_gm.TeamAims{
				TeamAimsId: team.TeamAimsTemId,
				MinAgeNum: team.MinAgeClaim,
				MaxAgeNum: team.MaxAgeClaim,
				PowerNum: team.PowerClaim,
			},
		})
	}

	ac.ResOk(&msg_gm.ResGroupSearch{List: teamList,Total: pageTeam.Total})
	return
}

func (t *teamController) GetTeamApplyList(ac common.IActionCtx)  {
	_, body, errCode := t.RequestSvc(svc.Team, ac.Tid(), ac.Cid(), msg_dt.CdTeamApplyList, &msg_dt.ReqTeamApplyList{
		Cid: ac.Cid(),
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResTeamApplyList)
	list, errCode := t.ProtoTeamApplyList(ac.Tid(), res.ApplyList)
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResGroupApplyList{List: list})
}

func (t *teamController) TeamMemberPositionExchange(ac common.IActionCtx,cid,oid int64)  {

}

func (t *teamController) ProtoTeamApplyList(tid int64,ApplyList []model.TeamRelationship)  ([]*msg_gm.FriendInfo,err_gm.ErrCode) {
	list := make([]*msg_gm.FriendInfo,0)
	if len(ApplyList) > 0 {
		cidList := make([]int64,0)
		for _,apply := range ApplyList {
			cidList = append(cidList,apply.Cid)
		}
		if len(cidList) > 0 {
			characterInfoList, errCode := t.RequestCharacterInfoQueryByCids(tid, []character_info_e.Enum{
				character_info_e.Nickname,
				character_info_e.KingdomName,
				character_info_e.Age,
				character_info_e.Prestige,
				character_info_e.Figure,
			}, cidList...)
			if errCode > 0 {
				return nil,err_gm.ErrQueryFailed
			}
			for cid,characterInfo := range characterInfoList {
				list = append(list,&msg_gm.FriendInfo{
					Cid: cid,
					NickName: characterInfo.Nickname,
					Kingdom: characterInfo.KingdomName,
					Age: characterInfo.Age,
					Prestige: characterInfo.Prestige,
					Avatar: int32(characterInfo.Figure),
				})
			}
		}
	}
	return list,0
}

func (t *teamController) PackTeamLoad(ac common.IActionCtx,team *model.Team) (*msg_gm.ResLoadGroup,err_gm.ErrCode) {
	memberList := model.GetTeamMemberList(team.TeamMember)
	characters, errCode := t.RequestCharacterInfoQueryByCids(ac.Tid(),
		[]character_info_e.Enum{character_info_e.Id, character_info_e.Nickname, character_info_e.CityId},
		memberList...)
	if errCode != 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		return nil,err_gm.ErrQueryFailed
	}
	members := make([]*msg_gm.GroupMember,0)
	for c,characterInfo1 := range characters {
		members = append(members,&msg_gm.GroupMember{
			Cid: c,
			NickName: characterInfo1.Nickname,
			Age: characterInfo1.Age,
			Power: characterInfo1.BattlePower,
		})
	}
	teamAims := &msg_gm.TeamAims{
		TeamAimsId: team.TeamAimsTemId,
		MinAgeNum: team.MinAgeClaim,
		MaxAgeNum: team.MaxAgeClaim,
		PowerNum: team.PowerClaim,
	}
	return &msg_gm.ResLoadGroup{Info: &msg_gm.GroupInfo{
		TeamId: team.Id,
		GroupName: team.TeamName,
		CurrentNumber: team.CurrentNumber,
		GroupMemberList: members,
		GroupType: int32(team.TeamType),
		TeamAims: teamAims,
		IsAutoAgree: team.AutoAgree,
		LeaderId: team.LeaderId,
	}},0
}