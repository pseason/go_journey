package team

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/msg_dt"
)

type ITeamController interface {
	common.IBaseController
}

// 队伍消息处理器
func (t *teamController) Actions() []common.Action {
	return []common.Action{
		{msg_gm.CdLoadGroup, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			t.LoadTeam(ac)
		}},
		{msg_gm.CdCreateTeam, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			t.CreateTeam(ac)
		}},
		{msg_gm.CdApplyTeam, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqApplyTeam)
			t.ApplyJoinTeam(ac, req.TeamId)
		}},
		{msg_gm.CdInviteJoinTeam, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqInviteJoinTeam)
			t.InviteJoinTeam(ac, req.Cid)
		}},
		{msg_gm.CdLeaderApproval, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqLeaderApproval)
			switch req.OperationType {
			case 1:
				t.AgreeApplyTeam(ac, req.Cid)
			case 2:
				t.RefuseApplyJoinTeam(ac, req.Cid)
			case 3:
				t.AgreeApplyTeam(ac, req.Cid)
			case 4:
				t.RefuseApplyJoinTeam(ac, req.Cid)
			}
		}},
		{msg_gm.CdOneClickApply, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqOneClickApply)
			t.OneClickApply(ac, req.AimsTempId)
		}},
		{msg_gm.CdSetTeamAims, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqSetTeamAims)
			t.UpdateTeamAims(ac, req.TeamId, req.TeamAims)
		}},
		{msg_gm.CdLeaveTeam, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			t.LeaveTeam(ac)
		}},
		{msg_gm.CdTransferTeam, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqTransferTeam)
			t.TransferTeam(ac, req.Cid)
		}},
		{msg_gm.CdDisbandTeam, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			t.DisbandTeam(ac)
		}},
		{msg_gm.CdPleaseLeaveTeam, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqPleaseLeaveTeam)
			t.PleaseLeaveTeam(ac, req.Cid)
		}},
		{msg_gm.CdSetTeamAutoJoin, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			t.UpdateTeamAutoAgree(ac)
		}},

		{msg_gm.CdGroupSearch, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqGroupSearch)
			t.PageQueryTeam(ac, req.Page, req.Size_, req.TeamAimsId)
		}},

		{msg_gm.CdGroupApplyList, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			t.GetTeamApplyList(ac)
		}},
		{msg_gm.CdTeamMemberPositionExchange, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqTeamMemberPositionExchange)
			t.TeamMemberPositionExchange(ac, req.Cid, req.Oid)
		}},
	}
}

// 队伍事件处理器
func (t *teamController) Events() []common.Event {
	return []common.Event{
		{msg_dt.CdEveTeamApplyNotify, func(ac common.IActionCtx, eveBody interface{}) {
			eveTeamApplyNotify := eveBody.(*msg_dt.EveTeamApplyNotify)
			t.SendToCharacters([]int64{eveTeamApplyNotify.Cid}, msg_gm.CdAddGroupNotify, &msg_gm.ResAddGroupNotify{})
		}},
		{msg_dt.CdEveTeamInviteNotify, func(ac common.IActionCtx, eveBody interface{}) {
			eveTeamInviteNotify := eveBody.(*msg_dt.EveTeamInviteNotify)
			t.SendToCharacters([]int64{eveTeamInviteNotify.Cid}, msg_gm.CdInvateGroupNotify, &msg_gm.ResInvateGroupNotify{TeamId: eveTeamInviteNotify.TeamId})
		}},
		{msg_dt.CdEveTeamMemberChange, func(ac common.IActionCtx, eveBody interface{}) {
			eveTeamMemberChange := eveBody.(*msg_dt.EveTeamMemberChange)
			t.SendToCharacters(eveTeamMemberChange.MemberList, msg_gm.CdGroupChange,
				&msg_gm.ResGroupChange{Cid: eveTeamMemberChange.Cid, Type: eveTeamMemberChange.Type})
		}},
		{msg_dt.CdEveTeamAddNewMemberNotify, func(ac common.IActionCtx, eveBody interface{}) {
			res := eveBody.(*msg_dt.EveTeamAddNewMemberNotify)
			character, errCode := t.RequestCharacterInfoQueryByCid(ac.Tid(),
				[]character_info_e.Enum{
					character_info_e.Id,
					character_info_e.Nickname,
					character_info_e.CityId,
					character_info_e.Age,
					character_info_e.BattlePower,
				},
				res.Cid)
			if errCode != 0 {
				app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
				ac.ResErr(err_gm.ErrQueryFailed)
				return
			}
			t.SendToCharacters(res.OidList, msg_gm.CdAddMemberChange,
				&msg_gm.ResAddMemberChange{GroupMember: &msg_gm.GroupMember{
					Cid:      res.Cid,
					Avatar:   int32(character.Figure),
					NickName: character.Nickname,
					Age:      character.Age,
					Power:    character.BattlePower,
				}})
		}},
		{msg_dt.CdEveTeamUpdateTeamAimsChangeNotify, func(ac common.IActionCtx, eveBody interface{}) {
			res := eveBody.(*msg_dt.EveTeamUpdateTeamAimsChangeNotify)
			t.SendToCharacters(res.OidList, msg_gm.CdTeamAimsChangeNotify,
				&msg_gm.ResTeamAimsChangeNotify{TeamId: res.TeamId,
					TeamAims: &msg_gm.TeamAims{
						TeamAimsId: res.TeamAims.TeamAimsId,
						MinAgeNum:  res.TeamAims.MinAgeNum,
						MaxAgeNum:  res.TeamAims.MaxAgeNum,
						PowerNum:   res.TeamAims.PowerNum,
					}})
		}},
		{msg_dt.CdEveTeamAutoAgreeChange, func(ac common.IActionCtx, eveBody interface{}) {
			res := eveBody.(*msg_dt.EveTeamAutoAgreeChange)
			t.SendToCharacters(res.CidList, msg_gm.CdTeamAutoAgreeChangeNotify,
				&msg_gm.ResTeamAutoAgreeChangeNotify{TeamId: res.TeamId, IsAutoAgree: res.IsAutoAgree})
		}},
		{msg_dt.CdEveTeamLoadChange, func(ac common.IActionCtx, eveBody interface{}) {
			res := eveBody.(*msg_dt.EveTeamLoadChange)
			resLoadGroup, errCode := t.PackTeamLoad(ac, res.Team)
			if errCode > 0 {
				ac.ResErr(errCode)
				return
			}
			t.SendToCharacter(res.Cid, msg_gm.CdLoadGroup, resLoadGroup)
		}},
	}
}
