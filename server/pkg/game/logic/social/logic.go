package social

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/social_e"
	"hm/pkg/services/data/msg_dt"
	"strconv"
	"unicode"
)

type socialController struct {
	common.IBaseController
}

func NewSocialController() ISocialController {
	return &socialController{
		IBaseController: common.NewBaseController(),
	}
}

func (s *socialController) AfterInit() error {
	return nil
}

func (s *socialController) GetPageMails(ac common.IActionCtx, page, size, socialType int32) {
	reqSvcMail := &msg_dt.ReqSocialGetAllSocial{
		Cid:  ac.Cid(),
		Page: page,
		Size: size,
	}
	switch socialType {
	case 0: //好友申请
		reqSvcMail.SocialType = []social_e.FriendRelationEnum{social_e.ApplyStatus}
	case 1: //好友
		reqSvcMail.SocialType = []social_e.FriendRelationEnum{social_e.BuddyStatus}
	case 2: //黑名单
		reqSvcMail.SocialType = []social_e.FriendRelationEnum{social_e.BlacklistStatus, social_e.BlacklistAndEnemyStatus}
	case 3: //仇人
		reqSvcMail.SocialType = []social_e.FriendRelationEnum{social_e.EnemyStatus, social_e.BlacklistAndEnemyStatus}
	}
	_, body, code := s.RequestSvc(svc.Social, ac.Tid(), ac.Cid(), msg_dt.CdSocialGetAllSocial, reqSvcMail)
	if code > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("查询玩家社交关系失败", utils.M{"cid": ac.Cid(), "socialType": socialType}))
		ac.ResErr(err_gm.ErrQueryFailed)
	}
	res := body.(*msg_dt.ResSocialGetAllSocial)
	proSocialAll, iErr := s.ProSocialAll(ac, res.SocialAll)
	if iErr != nil {
		app.Log().TError(ac.Tid(), iErr)
	}
	ac.ResOk(&msg_gm.ResPageSocial{
		SocialInfo: proSocialAll,
		Total:      res.Count,
		Current:    page,
	})
}

func (s *socialController) ApplyAddBuddy(ac common.IActionCtx, oid int64) {
	if oid == ac.Cid() {
		app.Log().TError(ac.Tid(), utils.NewError("不能添加自己为好友", utils.M{"cid": ac.Cid(), "oid": oid}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	s.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.Try(svc.Social, ac.Cid(), msg_dt.CdSocialTryApplyJoinBuddy, &msg_dt.ReqSocialTryApplyJoinBuddy{
			Cid: ac.Cid(),
			Oid: oid,
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("申请添加好友失败", utils.M{"cid": ac.Cid(), "oid": oid}))
			return
		}
		ac.ResOk(&msg_gm.ResSocialApplyJoinBuddy{})
		return true
	})
}

func (s *socialController) GetUnReadBuddyApplyCount(ac common.IActionCtx) {
	_, body, errCode := s.RequestSvc(svc.Social, ac.Tid(), ac.Cid(), msg_dt.CdSocialUnReadBuddyApplyCount, &msg_dt.ReqSocialUnReadBuddyApplyCount{
		Cid: ac.Cid(),
	})
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("查询好友申请数量失败", utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResSocialUnReadBuddyApplyCount)
	ac.ResOk(&msg_gm.ResUnReadBuddyApplyCount{
		Count: res.Count,
	})
}

func (s *socialController) HandelBuddyApply(ac common.IActionCtx, oid int64, hanDelType int32) {
	s.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		if hanDelType == 0 {
			_, errCode := tx.Try(svc.Social, ac.Cid(), msg_dt.CdSocialTryAgreeFriends, &msg_dt.ReqSocialTryAgreeFriends{
				Cid: ac.Cid(),
				Oid: oid,
			})
			if errCode > 0 {
				app.Log().TError(ac.Tid(), utils.NewError("同意好友申请失败", utils.M{"cid": ac.Cid(), "oid": oid}))
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
		} else if hanDelType == 1 {
			_, errCode := tx.Try(svc.Social, ac.Cid(), msg_dt.CdSocialTryRejectFriends, &msg_dt.ReqSocialTryRejectFriends{
				Cid: ac.Cid(),
				Oid: oid,
			})
			if errCode > 0 {
				app.Log().TError(ac.Tid(), utils.NewError("拒绝好友申请失败", utils.M{"cid": ac.Cid(), "oid": oid}))
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
		}
		ac.ResOk(&msg_gm.ResBuddyApplyHandel{})
		return true
	})
}

func (s *socialController) ApplyAddBlacklist(ac common.IActionCtx, oid int64) {
	if oid == ac.Cid() {
		app.Log().TError(ac.Tid(), utils.NewError("不能添加自己为黑名单", utils.M{"cid": ac.Cid(), "oid": oid}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	s.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.Try(svc.Social, ac.Cid(), msg_dt.CdSocialTryAddBlacklist, &msg_dt.ReqSocialTryAddBlacklist{
			Cid: ac.Cid(),
			Oid: oid,
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("添加黑名单失败", utils.M{"cid": ac.Cid(), "oid": oid}))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResSocialApplyJoinBlacklist{})
		return true
	})
}

func (s *socialController) ApplyDelBuddy(ac common.IActionCtx, oid int64) {
	s.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.Try(svc.Social, ac.Cid(), msg_dt.CdSocialTryDeleteBuddy, &msg_dt.ReqSocialTryDeleteBuddy{
			Cid: ac.Cid(),
			Oid: oid,
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("删除好友失败", utils.M{"cid": ac.Cid(), "oid": oid}))
			ac.ResErr(err_gm.ErrDelFailed)
			return
		}
		ac.ResOk(&msg_gm.ResSocialApplyDelBuddy{})
		return true
	})
}

func (s *socialController) ApplyDelBlacklist(ac common.IActionCtx, oid int64) {
	s.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.Try(svc.Social, ac.Cid(), msg_dt.CdSocialTryDeleteBlacklist, &msg_dt.ReqSocialTryDeleteBlacklist{
			Cid: ac.Cid(),
			Oid: oid,
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("删除黑名单失败", utils.M{"cid": ac.Cid(), "oid": oid}))
			ac.ResErr(err_gm.ErrDelFailed)
			return
		}
		ac.ResOk(&msg_gm.ResSocialApplyDelBlacklist{})
		return true
	})
}

func (s *socialController) SearchCharacterNick(ac common.IActionCtx, nick string) {
	if nick == "" {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	var cid int64
	if len(nick) == 19 {
		b := true
		for _, s := range nick {
			if !unicode.Is(unicode.Han, s) {
				b = false
				break
			}
		}
		if b {
			cid, _ = strconv.ParseInt(nick, 10, 64)
		}
	}
	if cid <= 0 {
		_, body, errCode := s.RequestSvc(svc.CharacterInfo, ac.Tid(), 0, msg_dt.CdCidByNickname, &msg_dt.ReqCidByNickname{
			Nickname: nick,
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("By character nick search character id exit", utils.M{"nick": nick}))
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		res := body.(*msg_dt.ResCidByNickname)
		if res.Cid == 0 {
			ac.ResOk(&msg_gm.ResSearchCharacter{})
			return
		}
		cid = res.Cid
	}
	if cid == ac.Cid() {
		ac.ResOk(&msg_gm.ResSearchCharacter{})
		return
	}
	if cid > 0 {
		characterInfo, errCode := s.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{
			character_info_e.Id,
			character_info_e.Age,
			character_info_e.Figure}, cid)
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("search characterInfo exit", utils.M{"cid": cid}))
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		ac.ResOk(&msg_gm.ResSearchCharacter{
			Cid:      cid,
			Age:      characterInfo.Age,
			FigureId: int32(characterInfo.Figure),
		})
	}

}

func (s *socialController) GetSocialGetBuddyCidList(ac common.IActionCtx) {
	_, buddyBody, errCode := s.RequestSvc(svc.Social, ac.Tid(), ac.Cid(), msg_dt.CdSocialGetBuddyCidList, &msg_dt.ReqSocialGetBuddyCidList{
		Cid:        ac.Cid(),
		SocialType: []social_e.FriendRelationEnum{social_e.BuddyStatus},
	})
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("search character buddy id list exit", utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	_, blacklistBody, errCode := s.RequestSvc(svc.Social, ac.Tid(), ac.Cid(), msg_dt.CdSocialGetBuddyCidList, &msg_dt.ReqSocialGetBuddyCidList{
		Cid:        ac.Cid(),
		SocialType: []social_e.FriendRelationEnum{social_e.BlacklistStatus, social_e.BlacklistAndEnemyStatus},
	})
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("search character buddy id list exit", utils.M{"cid": ac.Cid()}))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	buddyCidList := buddyBody.(*msg_dt.ResSocialGetBuddyCidList).CidList
	blacklistCidList := blacklistBody.(*msg_dt.ResSocialGetBuddyCidList).CidList
	ac.ResOk(&msg_gm.ResSearchCharacterSocialList{BuddyCidList: buddyCidList, BlacklistCidList: blacklistCidList})
}

func (s *socialController) ProSocialAll(ac common.IActionCtx, socials []*model.Social) ([]*msg_gm.SocialInfo, utils.IError) {
	cidList := make([]int64, 0, len(socials))
	socialList := make([]*msg_gm.SocialInfo, 0)
	for _, social := range socials {
		cidList = append(cidList, social.Oid)
		socialList = append(socialList, &msg_gm.SocialInfo{
			Cid: social.Oid,
		})
	}
	if len(cidList) > 0 {
		characterInfoMap, errCode := s.RequestCharacterInfoQueryByCids(ac.Tid(), []character_info_e.Enum{
			character_info_e.Nickname,
			character_info_e.Age,
			character_info_e.Prestige,
			character_info_e.Figure,
			character_info_e.TeamId,
			character_info_e.ClanName,
		}, cidList...)
		if errCode > 0 {
			return nil, utils.NewError("批量查询人物封装信息失败", utils.M{"cidList": cidList})
		}
		for _, social := range socialList {
			info, ok := characterInfoMap[social.Cid]
			if ok {
				social.NickName = info.Nickname
				social.Age = info.Age
				social.Prestige = info.Prestige
				social.ClanName = info.ClanName
				social.Avatar = int32(info.Figure)
				social.IsJoinTeam = info.TeamId > 0
			}
		}
	}

	return socialList, nil
}
