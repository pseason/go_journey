package social

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/msg_dt"
)

type ISocialController interface {
	common.IBaseController
}

// Actions 消息处理器
func (s *socialController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdPageSocial,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqPageSocial)
			s.GetPageMails(ac,req.Page,req.Size_,req.SocialType)
		}},
		{MsgCode: msg_gm.CdSocialApplyJoinBuddy,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqSocialApplyJoinBuddy)
			s.ApplyAddBuddy(ac,req.Oid)
		}},
		{MsgCode: msg_gm.CdUnReadBuddyApplyCount,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			s.GetUnReadBuddyApplyCount(ac)
		}},
		{MsgCode: msg_gm.CdBuddyApplyHandel,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBuddyApplyHandel)
			s.HandelBuddyApply(ac,req.Oid,req.HanDelType)
		}},
		{MsgCode: msg_gm.CdSocialApplyJoinBlacklist,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqSocialApplyJoinBlacklist)
			s.ApplyAddBlacklist(ac,req.Oid)
		}},
		{MsgCode: msg_gm.CdSocialApplyDelBuddy,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqSocialApplyDelBuddy)
			s.ApplyDelBuddy(ac,req.Oid)
		}},
		{MsgCode: msg_gm.CdSocialApplyDelBlacklist,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqSocialApplyDelBlacklist)
			s.ApplyDelBlacklist(ac,req.Oid)
		}},
		{MsgCode: msg_gm.CdSearchCharacter,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqSearchCharacter)
			s.SearchCharacterNick(ac,req.Name)
		}},
		{MsgCode: msg_gm.CdSearchCharacterSocialList,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			s.GetSocialGetBuddyCidList(ac)
		}},
	}
}

// Events 事件处理器
func (s *socialController) Events() []common.Event {
	return []common.Event{
		{MsgCode: msg_dt.CdEveSocialBuddyApplyNotify,Action: func(ac common.IActionCtx, eveBody interface{}) {
			res := eveBody.(*msg_dt.EveSocialBuddyApplyNotify)
			s.SendToCharacter(res.Cid, msg_gm.CdNoticeBuddyApply, &msg_gm.NoticeBuddyApply{})
		}},
		{MsgCode: msg_dt.CdEveSocialJoinBuddyNotify,Action: func(ac common.IActionCtx, eveBody interface{}) {
			res := eveBody.(*msg_dt.EveSocialJoinBuddyNotify)
			character, errCode := s.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.Nickname}, res.Oid)
			if errCode > 0{
				app.Log().TError(ac.Tid(),utils.NewError("search character name exit",utils.M{"cid": res.Oid}))
				return
			}
			s.SendToCharacter(res.Cid, msg_gm.CdNoticeJoinBuddy, &msg_gm.NoticeJoinBuddy{NickName: character.Nickname,Cid: res.Oid,SocialType: res.SocialType})
		}},
	}
}
