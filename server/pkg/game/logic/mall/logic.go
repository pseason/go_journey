package mall

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc/tsv"
	util "hm/pkg/misc/utils"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
	"time"
)

type mallController struct {
	common.IBaseController
	mallTemplates    *tsv.MallTsvManager
	mallCommodityMap map[int64]*msg_gm.Commodity
}

func NewMallController() IMallController {
	return &mallController{
		IBaseController: common.NewBaseController(),
	}
}

func (m *mallController) AfterInit() error {
	m.mallTemplates = tsv.GetTsvManager(tsv.Mall).(*tsv.MallTsvManager)
	m.mallCommodityMap = make(map[int64]*msg_gm.Commodity)
	m.MallInit()
	//go func() {
	//time.Sleep(5 * time.Second)
	//m.MallGetAll(common.NewActionCtx("",0,0,common.AcType_Req))
	//m.MallBuyGoods(common.NewActionCtx("",0,1480822208267313152,common.AcType_Req),1,2)
	//}()
	return nil
}

func (m *mallController) MallInit() {
	if len(m.mallCommodityMap) == 0 {
		for _, v := range m.mallTemplates.TsvSlice {
			commodity := &msg_gm.Commodity{
				Id:            int64(v.Id),
				PropId:        v.PropId,
				ContainNum:    v.ContainNum,
				PurchaseLimit: v.PurchaseLimit,
				Position:      v.Position,
				MallType:      v.MallType,
				CommodityType: v.MallPropType,
				Price:         v.Price,
				DiscountPrice: discountCalculate(v.Price, v.Discount),
				IsDiscount:    v.Discount < 10,
				CurrencyType:  v.CurrencyType,
				PurchaseType:  v.PurchaseType,
			}
			if v.StartTime != "0" {
				startTime := TimeConversion(v.StartTime)
				if v.LimitTime > 0 && startTime > 0 {
					commodity.RemoveTime = int32(startTime + getHour(v.LimitTime))
				}
			}
			if v.DiscountStartTime != "0" {
				commodity.DiscountTime = int32(TimeConversion(v.DiscountStartTime) + getHour(v.Discount_time))
			}
			m.mallCommodityMap[int64(v.Id)] = commodity
		}
	}
}

//获取商城列表
func (m *mallController) MallGetAll(ac common.IActionCtx) {
	resData := make([]*msg_gm.Commodity, 0)
	delList := make([]int64, 0)
	for _, commodity := range m.mallCommodityMap {
		if commodity.RemoveTime > 0 && isExpired(commodity.RemoveTime) {
			delList = append(delList, commodity.Id)
			continue
		}
		if commodity.IsDiscount && isExpired(commodity.DiscountTime) {
			commodity.IsDiscount = false
			commodity.DiscountPrice = commodity.Price
		}
		v := &msg_gm.Commodity{
			Id:            commodity.Id,
			PropId:        commodity.PropId,
			ContainNum:    commodity.ContainNum,
			PurchaseLimit: commodity.PurchaseLimit,
			Position:      commodity.Position,
			MallType:      commodity.MallType,
			CommodityType: commodity.CommodityType,
			Price:         commodity.Price,
			DiscountPrice: commodity.DiscountPrice,
			IsDiscount:    commodity.IsDiscount,
			CurrencyType:  commodity.CurrencyType,
			DiscountTime:  commodity.DiscountTime,
			RemoveTime:    commodity.RemoveTime,
		}
		if commodity.PurchaseLimit > 0 {
			count, errCode := m.IsPurchase(ac, commodity.Id, commodity.PurchaseType)
			if errCode > 0 {
				ac.ResErr(errCode)
				return
			}
			v.AvailableQuantity = commodity.PurchaseLimit - count
		}
		resData = append(resData, v)
	}
	ac.ResOk(&msg_gm.ResMall{AllMallData: resData})
	if len(delList) > 0 {
		for _, deleteId := range delList {
			delete(m.mallCommodityMap, deleteId)
		}
	}
}

//购买商品
func (m *mallController) MallBuyGoods(ac common.IActionCtx, mallId int64, num int32) {
	commodity, ok := m.mallCommodityMap[mallId]
	if !ok {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	count, errCode := m.IsPurchase(ac, mallId, commodity.PurchaseType)
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("character query mall buy fail", utils.M{"mallId": mallId}))
		ac.ResErr(errCode)
		return
	}
	if commodity.PurchaseLimit > 0 && count+num > commodity.PurchaseLimit {
		app.Log().TError(ac.Tid(), utils.NewError("character buy mall exceeded purchase limit",
			utils.M{"purchase limit": commodity.PurchaseLimit, "purchased quantity": count, "need purchase quantity": num}))
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	if commodity.RemoveTime > 0 && isExpired(commodity.RemoveTime) {
		delete(m.mallCommodityMap, mallId)
		app.Log().Info("delete time limit commodity", utils.M{"cid": ac.Cid(), "mallId": mallId, "num": num})
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	m.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		//判断商品是否存在折扣
		var expired bool
		if commodity.DiscountTime > 0 && time.Now().Unix() < int64(commodity.DiscountTime) || commodity.DiscountTime == 0 && commodity.IsDiscount {
			expired = true
		}
		var total int32
		//计算购买总金额
		if expired {
			total = commodity.DiscountPrice * num
		} else {
			total = commodity.Price * num
		}
		var currencyData int32
		if total > 0 {
			//扣除货币
			data, _, errCode := tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, map[character_info_e.Enum]int32{
				character_info_e.Enum(commodity.CurrencyType): -total,
			}, ac.Cid())
			if errCode != 0 {
				app.Log().TError(ac.Tid(), utils.NewError("character lack of money",
					utils.M{"purchase limit": commodity.PurchaseLimit, "purchased quantity": count, "need purchase quantity": num}))
				ac.ResErr(err_gm.ErrUpdateFailed)
				return false
			}
			character := data.(*model.CharacterInfo)
			currencyData = character.GetByEnum(character_info_e.Enum(commodity.CurrencyType)).(int32)
		}

		//物品入背包
		resBackpackBody, errCode := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
			Cid:            ac.Cid(),
			Props:          map[int32]int32{commodity.PropId: num * commodity.ContainNum},
			IsFullSendMail: true,
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("character buy goods Increase backpack fail",
				utils.M{"purchase limit": commodity.PurchaseLimit, "purchased quantity": count, "need purchase quantity": num}))
			ac.ResErr(err_gm.ErrCreateFailed)
			return false
		}
		backpackOver := resBackpackBody.(*msg_dt.ResBackpackIncrease)
		if len(backpackOver.OverProps) > 0 {
			//创建mail 对象
			overMail := make([]*model.Mail, 0)
			mailProp := make(map[int64]int32, 0)
			mailProps := make([]map[int64]int32, 0)
			for propId, num := range backpackOver.OverProps {
				PropId := int64(propId)
				mailProp[PropId] = num
				if len(mailProp) >= 4 {
					mailProps = append(mailProps, mailProp)
					mailProp = make(map[int64]int32, 0)
				}
			}
			if len(mailProp) > 0 {
				mailProps = append(mailProps, mailProp)
			}
			for _, p := range mailProps {
				m, err := model.NewSystemMail(ac.Cid(), "附件", "背包不足，以邮件发送", p)
				if err != nil {
					app.Log().Error("邮件创建失败", utils.M{"errCode": err})
					return false
				}
				overMail = append(overMail, m)
			}
			_, errCode = tx.Try(svc.Mail, ac.Cid(), msg_dt.CdMailSend, &msg_dt.ReqMailSend{
				Mails: overMail,
			})
			if errCode > 0 {
				return false
			}
		}
		//账单记录
		_, errCode = tx.Try(svc.Bill, ac.Cid(), msg_dt.CdBillTryCreate, &msg_dt.ReqBillTryCreate{
			Bill: &model.Bill{
				Cid:             ac.Cid(),
				PropId:          mallId,
				Num:             num,
				CurrencyType:    commodity.CurrencyType,
				UnitPrice:       int64(commodity.Price / num),
				TotalPrice:      int64(total),
				TransactionType: 1,
				TransactionDesc: "mall buy goods",
				SurplusCurrency: currencyData - total,
			},
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("character buy goods bill record fail",
				utils.M{"purchase limit": commodity.PurchaseLimit, "purchased quantity": count, "need purchase quantity": num}))
			ac.ResErr(err_gm.ErrCreateFailed)
		}
		ac.ResOk(&msg_gm.ResMallPurchaseProp{CommodityId: mallId, Num: num})
		return true
	})
}

//效验商品日、周限购
func (m *mallController) IsPurchase(ac common.IActionCtx, mallId int64, purchaseType int32) (count int32, errCode err_gm.ErrCode) {
	if purchaseType == 0 {
		return count, 0
	}
	var timestamp, endTimeNum int64
	if purchaseType == 1 {
		timestamp, endTimeNum = util.GetTimestamp()
	} else if purchaseType == 2 {
		timestamp, endTimeNum = util.GetFirstDateOfWeek(), time.Now().UnixNano()/1e6
	}
	_, body, errCode := m.RequestSvc(svc.Bill, ac.Tid(), ac.Cid(), msg_dt.CdBillGetPurchasePropCount, &msg_dt.ReqBillGetPurchasePropCount{
		Cid:             ac.Cid(),
		PropId:          int32(mallId),
		StartTime:       timestamp,
		EndTime:         endTimeNum,
		TransactionType: 1,
	})
	if errCode > 0 {
		return count, err_gm.ErrQueryFailed
	}
	res := body.(*msg_dt.ResBillGetPurchasePropCount)
	return res.PurchaseCount, 0
}

//限时商品判断处理
func isExpired(removeTime int32) bool {
	if removeTime == 0 {
		return false
	}
	if time.Now().Unix() > int64(removeTime) {
		return true
	}
	return false
}

//折扣计算
func discountCalculate(price, discount int32) int32 {
	return price / 10 * discount
}

/**
 * 小时转毫秒
 * @param hour
 */
func getHour(hour int32) int64 {
	return int64(hour * 1 * 60)
	//return int64(hour * 1 * 60)
}

func TimeConversion(seasonTimeStr string) int64 {
	t, _ := time.Parse("2006-01-02 15:04:05", seasonTimeStr)
	//Unix返回早八点的时间戳，减去8个小时
	return t.UTC().Unix() - 8*60*60
}
