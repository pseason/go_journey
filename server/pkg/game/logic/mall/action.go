package mall

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
)

type IMallController interface {
	common.IBaseController
}

// Actions 消息处理器
func (m *mallController) Actions() []common.Action {
	return []common.Action{
		// 获取商城列表
		{MsgCode: msg_gm.CdMall, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			m.MallGetAll(ac)
			return
		}},
		// 购买商品
		{MsgCode: msg_gm.CdMallPurchaseProp, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqMallPurchaseProp)
			m.MallBuyGoods(ac,req.CommodityId,req.Num)
			return
		}},
	}
}

// Events 事件处理器
func (m *mallController) Events() []common.Event {
	return []common.Event{

	}
}
