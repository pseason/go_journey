package task

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/msg_dt"
)

func (t *taskController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdTaskList, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			t.GetTaskList(ac)
			return
		}},
		{MsgCode: msg_gm.CdTaskActiveSubmit, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			return
		}},
		{MsgCode: msg_gm.CdTaskToggleHidden, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqTaskToggleHidden)
			t.TaskToggleHidden(ac,req.TaskId)
			return
		}},
		{MsgCode: msg_gm.CdTaskReceiveAward, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqTaskReceiveAward)
			t.TaskReceiveAward(ac,req.TaskId)
			return
		}},
	}
}

func (t *taskController) Events() []common.Event {
	return []common.Event{
		{MsgCode: msg_dt.CdEveTaskChange, Action: func(ac common.IActionCtx, eveBody interface{}) {
			req := eveBody.(*msg_dt.EveTaskChange)
			t.SendToCharacter(req.Cid, msg_gm.CdTaskChangeNotify, &msg_gm.ResTaskChangeNotify{
				List: t.ToProtobuf(req.Records),
			})
		}},
		{MsgCode: msg_dt.CdEveTaskExecute, Action: func(ac common.IActionCtx, eveBody interface{}) {
			req := eveBody.(*msg_dt.EveTaskExecute)
			t.ExecuteTask(ac,req.Cid,req.AftType,req.PropId,req.PropNum)
		}},
	}
}
