package task

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/event"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/task_e"
	"hm/pkg/services/data/msg_dt"
)

type ITaskController interface {
	common.IBaseController
}

type taskController struct {
	common.IBaseController
	template     *tsv.TaskTsvManager
	plotTemplate *tsv.PlotTsvManager
}

func NewTaskController() ITaskController {
	return &taskController{
		IBaseController: common.NewBaseController(),
	}
}

func (t *taskController) AfterInit() (err error) {
	t.template = tsv.GetTsvManager(tsv.Task).(*tsv.TaskTsvManager)
	t.plotTemplate = tsv.GetTsvManager(tsv.Plot).(*tsv.PlotTsvManager)
	app.Event().BindEvent(event.EvtUserOffline, func(obj interface{}) {
		evt := obj.(*event.UserOffLine)
		cid, ok := app.UserLogic().GetAliasWithUid(evt.Uid)
		if !ok {
			return
		}
		tid := app.Log().TSign(0, utils.M{
			"tracer": "user offline",
		})
		t.Transaction(tid, func(tx common.ITx) (ok bool) {
			_, errCode := tx.Try(svc.Task, cid, msg_dt.CdTaskTryOffLineInit, msg_dt.ReqTaskTryOffLineInit{
				Cid: cid,
			})
			if errCode > 0 {
				app.Log().TError(tid, utils.NewError("character offline init task exit", utils.M{"cid": cid}))
				return
			}
			return true
		})
	})
	return
}

func (t *taskController) GetTaskList(ac common.IActionCtx) {
	_, body, errCode := t.RequestSvc(svc.Task, ac.Tid(), ac.Cid(), msg_dt.CdTaskList, &msg_dt.ReqTaskList{
		Cid: ac.Cid(),
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		app.Log().TError(ac.Tid(), utils.NewError("character query task list exit", utils.M{"cid": ac.Cid()}))
		return
	}
	taskList := body.(*msg_dt.ResTaskList).Records
	if len(taskList) > 0 {
		ac.ResOk(&msg_gm.ResTaskList{List: t.ToProtobuf(taskList)})
	} else {
		ac.ResOk(&msg_gm.ResTaskList{})
	}
}

func (t *taskController) ExecuteTask(ac common.IActionCtx, cid int64, aftType task_e.TaskAftType, propId, propNum int32) {
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		resExecuteBody, iErr := tx.Try(svc.Task, ac.Cid(), msg_dt.CdTaskTryExecute, &msg_dt.ReqTaskTryExecute{
			Cid:     cid,
			AftType: aftType,
			PropId:  propId,
			PropNum: propNum,
		})
		if iErr > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("task execute exit", utils.M{"cid": ac.Cid()}))
			return
		}
		res := resExecuteBody.(*msg_dt.ResTaskTryExecute)
		//判断新增剧情
		if len(res.AddPlotIdList) > 0 {

		}
		//判断发送奖励
		if len(res.AddPropMap) > 0 {
			//物品入背包
			resBackpackBody, errCode := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
				Cid:            ac.Cid(),
				Props:          res.AddPropMap,
				IsFullSendMail: true,
			})
			if errCode > 0 {
				app.Log().TError(ac.Tid(), utils.NewError("character buy goods Increase backpack fail",
					utils.M{"addPropMap": res.AddPropMap}))
				ac.ResErr(err_gm.ErrCreateFailed)
				return false
			}
			backpackOver := resBackpackBody.(*msg_dt.ResBackpackIncrease)
			if len(backpackOver.OverProps) > 0 {
				//创建mail 对象
				overMail := make([]*model.Mail, 0)
				mailProp := make(map[int64]int32, 0)
				mailProps := make([]map[int64]int32, 0)
				for propId, num := range backpackOver.OverProps {
					PropId := int64(propId)
					mailProp[PropId] = num
					if len(mailProp) >= 4 {
						mailProps = append(mailProps, mailProp)
						mailProp = make(map[int64]int32, 0)
					}
				}
				if len(mailProp) > 0 {
					mailProps = append(mailProps, mailProp)
				}
				for _, p := range mailProps {
					m, err := model.NewSystemMail(ac.Cid(), "附件", "背包不足，以邮件发送", p)
					if err != nil {
						app.Log().Error("邮件创建失败", utils.M{"errCode": err})
						return false
					}
					overMail = append(overMail, m)
				}
				_, errCode = tx.Try(svc.Mail, ac.Cid(), msg_dt.CdMailSend, &msg_dt.ReqMailSend{
					Mails: overMail,
				})
				if errCode > 0 {
					return false
				}
			}
		}
		//判断发送技能
		if len(res.AddSkillMap) > 0 {

		}
		//判断发送货币
		if len(res.AddInfoMap) > 0 {

		}

		return true
	})
}

//TaskToggleHidden 任务隐藏
func (t *taskController) TaskToggleHidden(ac common.IActionCtx, taskId int64) {
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode := tx.Try(svc.Task, taskId, msg_dt.CdTaskTryToggleHidden, &msg_dt.ReqTaskTryToggleHidden{
			Cid:    ac.Cid(),
			TaskId: taskId,
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("任务隐藏失败", utils.M{"taskId": taskId}))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResTaskToggleHidden{})
		return true
	})
}

func (t *taskController) TaskReceiveAward(ac common.IActionCtx, taskId int64) {
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		body, errCode := tx.Try(svc.Task, taskId, msg_dt.CdTaskTryReceiveAward, &msg_dt.ReqTaskTryReceiveAward{
			Cid:    ac.Cid(),
			TaskId: taskId,
		})
		if errCode > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("任务领取奖励失败", utils.M{"taskId": taskId}))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		res := body.(*msg_dt.ResTaskTryReceiveAward)
		//判断发送奖励
		if len(res.AddPropMap) > 0 {
			//物品入背包
			resBackpackBody, errCode := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
				Cid:            ac.Cid(),
				Props:          res.AddPropMap,
				IsFullSendMail: true,
			})
			if errCode > 0 {
				app.Log().TError(ac.Tid(), utils.NewError("character buy goods Increase backpack fail",
					utils.M{"addPropMap": res.AddPropMap}))
				ac.ResErr(err_gm.ErrCreateFailed)
				return
			}
			backpackOver := resBackpackBody.(*msg_dt.ResBackpackIncrease)
			if len(backpackOver.OverProps) > 0 {
				//创建mail 对象
				overMail := make([]*model.Mail, 0)
				mailProp := make(map[int64]int32, 0)
				mailProps := make([]map[int64]int32, 0)
				for prop, num := range backpackOver.OverProps {
					PropId := int64(prop)
					mailProp[PropId] = num
					if len(mailProp) >= 4 {
						mailProps = append(mailProps, mailProp)
						mailProp = make(map[int64]int32, 0)
					}
				}
				if len(mailProp) > 0 {
					mailProps = append(mailProps, mailProp)
				}
				for _, p := range mailProps {
					m, err := model.NewSystemMail(ac.Cid(), "附件", "背包不足，以邮件发送", p)
					if err != nil {
						app.Log().Error("邮件创建失败", utils.M{"errCode": err})
						return
					}
					overMail = append(overMail, m)
				}
				_, errCode = tx.Try(svc.Mail, ac.Cid(), msg_dt.CdMailSend, &msg_dt.ReqMailSend{
					Mails: overMail,
				})
				if errCode > 0 {
					return false
				}
			}
		}
		//判断发送技能
		if len(res.AddSkillMap) > 0 {

		}
		//判断发送货币
		if len(res.AddInfoMap) > 0 {

		}
		ac.ResOk(&msg_gm.ResTaskReceiveAward{})
		return true
	})
}

func (t *taskController) ToProtobuf(record []*model.Task) (res []*msg_gm.Task) {
	if len(record) > 0 {
		res = make([]*msg_gm.Task, 0, len(record))
		for _, v := range record {
			if v.Hidden {
				continue
			}
			//find template
			template, has := t.template.TsvMap[v.TemplateId]
			if !has {
				app.Log().Warn("task template not found in the to protobuf", utils.M{
					"templateId": v.TemplateId,
				})
				continue
			}
			var needs []int32
			if template.Rule == int32(model.TaskRuleOr) || template.Rule == int32(model.TaskRuleRand) {
				var tmpNum int32
				for _, v := range v.Record {
					tmpNum += v
				}
				needs = []int32{tmpNum}
			} else {
				needs = v.Record
			}
			res = append(res, &msg_gm.Task{
				Id:         v.Id,
				Cid:        v.Cid,
				TemplateId: v.TemplateId,
				Needs:      needs,
				Status:     int32(v.Status),
				Hidden:     v.Hidden,
			})
		}
	}
	return
}
