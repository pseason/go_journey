package testTry

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/msg_dt"
)

type testTryCtrl struct {
	common.IBaseController
}

func NewTestTryCtrl() *testTryCtrl {
	return &testTryCtrl{
		IBaseController: common.NewBaseController(),
	}
}

func (t *testTryCtrl) TestTry(ac common.IActionCtx, propId uint32, count uint16) {
	t.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		//尝试购买,物品id和数量,返回支付货币类型和数量,测试物品类型为2时失败
		resOrderAny, ec := tx.Try(svc.TestTry, ac.Cid(), msg_dt.CdTestTryOrder, &msg_dt.ReqTestTryOrder{
			PropId: propId,
			Count:  count,
		})
		if ec > 0 {
			return
		}
		//尝试按支付货币类型冻结资产,测试如果大于500则失败
		resOrder := resOrderAny.(*msg_dt.ResTestTryOrder)
		_, ec = tx.Try(svc.TestTry, ac.Cid(), msg_dt.CdTestTryHoldingAsset, &msg_dt.ReqTestTryHoldingAsset{
			PayType:  resOrder.PayType,
			PayPrice: resOrder.PayPrice,
		})
		if ec > 0 {
			ac.ResErr(ec)
			return
		}
		ac.ResOk(&msg_gm.ResTestTry{})
		return true
	})

}
