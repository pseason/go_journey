package testTry

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
)

type ITestTryCtrl interface {
	common.IBaseController
	TestTry(ac common.IActionCtx, propId uint32, count uint16)
}

func (c *testTryCtrl) Actions() []common.Action {
	return []common.Action{
		{msg_gm.CdTestTry, misc.RolePlayer, func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqTestTry)
			c.TestTry(ac, uint32(req.PropId), uint16(req.Count))
		}},
	}
}
