package unlock

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/msg_dt"
)

type IUnlockController interface {
	common.IBaseController
}

type unlockController struct {
	common.IBaseController
}

func NewUnlockController() IUnlockController {
	return &unlockController{
		IBaseController: common.NewBaseController(),
	}
}

func (u *unlockController) GetUnlockCurrentInfo(ac common.IActionCtx) {
	_, resBody, err := u.RequestSvc(svc.Unlock, ac.Tid(), ac.Cid(), msg_dt.CdUnlockCurrentInfo, &msg_dt.ReqUnlockCurrentInfo{Cid: ac.Cid()})
	if err.IsNotNil() {
		ac.ResErr(err)
	} else {
		info := resBody.(*msg_dt.ResUnlockCurrentInfo)
		ac.ResOk(&msg_gm.ResUnlockCurrentInfo{
			Records: info.Records,
		})
	}
}

func (u *unlockController) Execute(ac common.IActionCtx, req *msg_dt.EveUnlockExecute) {
	_, _, err := u.RequestSvc(svc.Unlock, ac.Tid(), ac.Cid(), msg_dt.CdUnlockExecute, &msg_dt.ReqUnlockExecute{
		Cid:         req.Cid,
		TemplateIds: req.TemplateIds,
		Opt:         req.Opt,
	})
	if err.IsNotNil() {
		app.Log().Error(err.Desc(), utils.M{
			"params": req,
		})
	}
	return
}
