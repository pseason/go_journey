package unlock

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/msg_dt"
)

func (u *unlockController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdUnlockCurrentInfo, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			u.GetUnlockCurrentInfo(ac)
		}},
	}
}

func (u *unlockController) Events() []common.Event {
	return []common.Event{
		{MsgCode: msg_dt.CdEveUnlockChange, Action: func(ac common.IActionCtx, eveBody interface{}) {
			req := eveBody.(*msg_dt.EveUnlockChange)
			u.SendToCharacter(req.Cid, msg_gm.CdUnlockChangeNotify, &msg_gm.ResUnlockChangeNotify{
				Records: req.Records,
			})
		}},
		{MsgCode: msg_dt.CdEveUnlockExecute, Action: func(ac common.IActionCtx, eveBody interface{}) {
			req := eveBody.(*msg_dt.EveUnlockExecute)
			u.Execute(ac, req)
		}},
	}
}
