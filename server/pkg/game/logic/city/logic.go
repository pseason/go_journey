package city

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"fmt"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/city_e"
	"hm/pkg/services/data/model/enum/event_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
	spc_common "hm/pkg/services/space/common"
	"hm/pkg/services/space/msg_spc"
)

type cityController struct {
	common.IBaseController
	enterCitySpaceQueue map[int64][]queuePlayer
	buildingTemplates *tsv.CityBuildingTsvManager
}

type queuePlayer struct {
	cid             int64
	queuePlayerType city_e.QueuePlayerType
}

func NewCityController() ICityController {
	return &cityController{
		IBaseController:     common.NewBaseController(),
		enterCitySpaceQueue: make(map[int64][]queuePlayer),
	}
}

func (c *cityController) AfterInit() error {
	c.buildingTemplates = tsv.GetTsvManager(tsv.CityBuilding).(*tsv.CityBuildingTsvManager)
	//go func() {
	//	time.Sleep(3 * time.Second)
	//	cityBirthPoint := c.Conf().Design.CityBirthPoint
	//	fmt.Println(cityBirthPoint)
	//}()

	return nil
}

//获取城邦列表
func (c *cityController) CityGetCityList(ac common.IActionCtx) {
	_, body, code := c.RequestSvc(svc.City, ac.Tid(), ac.Cid(), msg_dt.CdCityList, &msg_dt.ReqCityList{})
	if code > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResCityList)
	ac.ResOk(&msg_gm.ResCityGetList{CityList: ProBuf(res.CityInfo)})
}

//CityJoin 加入城邦
func (c *cityController) CityJoin(ac common.IActionCtx, cityId int64) {
	data, errCode := c.RequestSvcQueryById(svc.CharacterInfo, ac.Tid(), model_e.CharacterInfo, []character_info_e.Enum{character_info_e.Id, character_info_e.Nickname, character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query character city id and nick name failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	characterInfo := data.(*model.CharacterInfo)

	if characterInfo.CityId > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("join city failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	//Todo start
	_, body, errCode := c.RequestSvc(svc.City, ac.Tid(), ac.Cid(), msg_dt.CdCityLoadCity, msg_dt.ReqCityLoadCity{CityId: 50})
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query character city id and nick name failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	resCity:= body.(*msg_dt.ResCityLoadCity)
	cityId = resCity.City.Id
	//Todo end
	//判断城邦当前容纳人数是否超过限制如果超过，进入排队状态
	reqSvmCi := &msg_dt.ReqCityJoin{
		Cid:    ac.Cid(),
		CityId: cityId,
	}
	var CityTempId int32
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		body, joinCityErr := tx.Try(svc.City, cityId, msg_dt.CdCityJoin, reqSvmCi)
		if joinCityErr > 0 {
			app.Log().Error("加入城邦失败", utils.M{"errCode": errCode})
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		city := body.(*msg_dt.ResCityJoin)
		_, updateCharacterCityId := tx.Try(svc.CharacterInfo, ac.Cid(), msg_dt.CdTryCommonUpdateById, &msg_dt.ReqTryCommonUpdateById{
			Model:   model_e.CharacterInfo,
			UpData:  map[character_info_e.Enum]interface{}{
				character_info_e.CityId: cityId,
				character_info_e.CityName: city.CityName,
				character_info_e.CityPosition: city_e.CityMember},
			ModelId: ac.Cid(),
		})
		if updateCharacterCityId > 0 {
			app.Log().TError(ac.Tid(), utils.NewError("update character city failed", utils.M{"cid": ac.Cid()}))
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}

		reqCreateEvent := &msg_dt.ReqEventCreateEvent{
			Event: []*model.Event{
				model.GenDefaultCityEvent(cityId, event_e.CityMemberEvent, 1, "成员加入",
					fmt.Sprintf("玩家%s加入本城邦", characterInfo.Nickname)),
			},
		}
		_, code := tx.Try(svc.Event, ac.Cid(), msg_dt.CdEventCreateEvent, reqCreateEvent)
		if code > 0 {
			ac.ResErr(err_gm.ErrCreateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResCityJoinCity{})
		CityTempId = city.CityTempId
		return true
	})

	c.EnterBroadcastSpe(ac)

}

func (c *cityController) ExitCIty(ac common.IActionCtx) {
	data, errCode := c.RequestSvcQueryById(svc.CharacterInfo, ac.Tid(), model_e.CharacterInfo, []character_info_e.Enum{character_info_e.Nickname, character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	characterInfo := data.(*model.CharacterInfo)

	if characterInfo.CityId == 0 {
		app.Log().TError(ac.Tid(), utils.NewError("character not join city failed", nil))
		ac.ResErr(err_gm.ErrDelFailed)
	}
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode = tx.Try(svc.City,ac.Cid(),msg_dt.CdCityTryExitCity, msg_dt.ReqCityTryExitCity{Cid: ac.Cid()})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrDelFailed)
			return
		}
		_, errCode = tx.TryUpdateCharacterInfoByCid(map[character_info_e.Enum]interface{}{character_info_e.CityId: 0,character_info_e.CityName: ""},ac.Cid())
		if errCode > 0 {
			ac.ResErr(err_gm.ErrDelFailed)
			return
		}
		_, errCode = tx.Try(svc.Event,ac.Cid(), msg_dt.CdEventCreateEvent, &msg_dt.ReqEventCreateEvent{
			Event: []*model.Event{
				model.GenDefaultCityEvent(characterInfo.CityId, event_e.CityMemberEvent, 2, "成员退出",
					fmt.Sprintf("玩家%s退出了城邦", characterInfo.Nickname)),
			},
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrCreateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResCityExitCity{})
		return true
	})

}

func (c *cityController) CityGetAllCityMember(ac common.IActionCtx,page,size int32)  {
	reqCharacter, errCode := c.RequestSvcQueryById(svc.CharacterInfo, ac.Tid(),
		model_e.CharacterInfo, []character_info_e.Enum{character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	character := reqCharacter.(*model.CharacterInfo)
	if character.CityId == 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	_,resBody,errCode := c.RequestSvc(svc.City,ac.Tid(),character.CityId,msg_dt.CdCityMemberList,
		&msg_dt.ReqCityMemberList{CityId: character.CityId,Page: page,Size: size})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	resCityMemberList := resBody.(*msg_dt.ResCityMemberList)
	cityMembers, errCode := c.ProCityMembers(ac.Tid(), resCityMemberList.CityMemberList)
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResCityPageCityMember{
		MemberInfoList: cityMembers,
		Total: resCityMemberList.Total,
		Current: page,
	})
}

func (c *cityController) EnterBroadcastSpe(ac common.IActionCtx)  {
	c.SendToCharacter(ac.Cid(), msg_gm.CdNoticeCityEnter, nil)
}

//进入城邦
func (c *cityController) EnterCitySpace(ac common.IActionCtx,cityTempId int64) {
	if cityTempId == 0 {
		characterInfo, errCode := c.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.CityId}, ac.Cid())
		if errCode > 0 {
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		if characterInfo.CityId == 0 {
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		_, body, errCode := c.RequestSvc(svc.City, ac.Tid(), ac.Cid(), msg_dt.CdCityLoadCity, &msg_dt.ReqCityLoadCity{
			CityId: characterInfo.CityId,
		})
		if characterInfo.CityId == 0 {
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		city := body.(*msg_dt.ResCityLoadCity).City
		cityTempId = int64(city.CityTemplateId)
	}
	c.SendToCharacter(ac.Cid(), msg_gm.CdCityEnter, &msg_gm.ResCityEnter{})

	cityBirthPoint := c.Conf().Design.CityBirthPoint
	app.Timer().After(2000, func() {
		iErr := c.Space().NoticePlayerEnterSpace(ac.Cid(),misc.SceneCity,
			cityTempId,cityBirthPoint.BirthPointX,cityBirthPoint.BirthPointY,cityBirthPoint.BirthPointZ)

		if iErr > 0{
			app.Log().TError(ac.Tid(),utils.NewError("character enter city scene fail",utils.M{"cid": ac.Cid()}))
		}
	})
	return


	//todo
	//判断当前城邦有人排队，玩家加入排队列表中，
	//如果没有就判断当前城邦可容纳人数到达限制，玩家加入排队列表中
	//玩家进入城邦空间
	//c.SendToCharacter(ac.Cid(), msg_gm.CdNoticeWaiting, &msg_gm.NoticeWaiting{
	//	WaitingNumber: 5,
	//})
	//go func() {
	//	for i := 1; i < 5; i++ {
	//		time.Sleep(3 * time.Second)
	//		c.SendToCharacter(ac.Cid(), msg_gm.CdNoticeWaiting, &msg_gm.NoticeWaiting{
	//			WaitingNumber: 5-int32(i),
	//		})
	//	}
	//	time.Sleep(3 * time.Second)
	//	c.SendToCharacter(ac.Cid(), msg_gm.CdCityEnter, &msg_gm.ResCityEnter{})
	//}()
}

// SetCityMemberPosition 设置城邦成员身份 update city member identity
func (c *cityController) SetCityMemberPosition(ac common.IActionCtx, oid int64, identityId int32) {
	reqSvmCi := &msg_dt.ReqCitySetCityMemberIdentity{
		CityId:   0,
		Cid:      ac.Cid(),
		Oid:      oid,
		Identity: city_e.Identity(identityId),
	}
	_, _, errCode := c.RequestSvc(svc.City, ac.Tid(), 0, msg_dt.CdCitySetCityMemberIdentity, reqSvmCi)
	if errCode > 0 {
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	//_, _ = m.AsyncRequestSvc(svc.CharacterInfo, ac.Tid(), 0,  msg_dt.CdCharacterInfo, reqSvmCi, func(resBody interface{}) {
	//	ac.ResOk(&msg_dt.ResCityJoin{})
	//}, func(resErrCode err_dt.ErrCode) {
	//	ac.ResErr(resErrCode)
	//})
}

func (c *cityController) LoadCity(ac common.IActionCtx, cityId int64) {
	_, resbody, errCode := c.RequestSvc(svc.City, ac.Tid(), 0, msg_dt.CdCityLoadCity, &msg_dt.ReqCityLoadCity{
		CityId: cityId,
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrUpdateFailed)
		return
	}
	res := resbody.(*msg_dt.ResCityLoadCity)
	cityDetails := ProCityDetailsBuf(res.City)
	ac.ResOk(&msg_gm.ResCityLoadCity{
		CityDetails: cityDetails,
	})
}

//UpCityNameOrAnnouncement 修改城邦名称或公告
func (c *cityController) UpCityNameOrAnnouncement(ac common.IActionCtx, upType int32, upStr string) {
	data, errCode := c.RequestSvcQueryById(svc.CharacterInfo, ac.Tid(), model_e.CharacterInfo, []character_info_e.Enum{character_info_e.Nickname, character_info_e.CityId, character_info_e.CityPosition}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	characterInfo := data.(*model.CharacterInfo)
	if characterInfo.CityId == 0 {
		ac.ResErr(err_gm.ErrUpdateFailed)
	}
	if upStr == "" {
		app.Log().TError(ac.Tid(), utils.NewError("update data is nil", utils.M{"upStr": upStr}))
		ac.ResErr(err_gm.ErrUpdateFailed)
	}
	upMap := make(map[city_e.CityInfo]interface{})
	switch upType {
	case 1: //城邦名称
		upMap[city_e.CityNick] = upStr
	case 2: //城邦宣言
		upMap[city_e.Proclamation] = upStr
	}
	//判断权限
	//修改城邦数据
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, errCode = tx.Try(svc.City, characterInfo.CityId, msg_dt.CdCityUpCityData, &msg_dt.ReqCityUpCityData{
			CityId: characterInfo.CityId,
			UpMap:  upMap,
		})
		if errCode > 0 {
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ReqCityUpNameOrAnnouncement{
			UpType: upType,
			UpStr:  upStr,
		})
		return true
	})
}

func ProBuf(cityList []*msg_dt.CityData) (cityInfos []*msg_gm.City) {
	cityInfos = make([]*msg_gm.City, 0)
	for _, city := range cityList {
		cityInfos = append(cityInfos, &msg_gm.City{
			CityId:           city.Id,
			CurrentPlayerCap: city.CurrentPlayerCap,
			MaxPlayerCap:     city.MaxPlayerCap,
			CityName:         city.CityName,
			BannerId:         city.BannerProp,
			CityTemplateId:   city.CityTemplateId,
		})
	}
	return
}

func ProCityDetailsBuf(city *msg_dt.CityDetailsData) *msg_gm.CityDetails {
	return &msg_gm.CityDetails{
		CityId:           city.Id,
		CityName:         city.CityName,
		CurrentPlayerCap: city.CurrentPlayerCap,
		BannerId:         city.BannerProp,
		CityTemplateId:   city.CityTemplateId,
		Lv:               city.Lv,
		ProsperityNum:    city.ProsperityNum,
		OccupationIdList: city.OccupationIdList,
		CityType:         city.CityType,
		Announcement:     city.Announcement,
	}
}

func (c *cityController) ProCityMembers(tid int64,members []model.CityMember) ([]*msg_gm.MemberInfo,err_dt.ErrCode) {
	memberInfos := make([]*msg_gm.MemberInfo,len(members))
	if len(members) == 0 {
		return memberInfos,0
	}
	cidList := make([]int64,len(members))
	for index,m := range members {
		cidList[index] = m.Cid
	}
	characterMap, errCode := c.RequestCharacterInfoQueryByCids(tid, []character_info_e.Enum{
		character_info_e.Nickname,
		character_info_e.Age,
		character_info_e.BattlePower,
		character_info_e.Prestige,
	}, cidList...)
	if errCode > 0{
		return nil,errCode
	}

	for index,member := range members {
		memberInfos[index] = &msg_gm.MemberInfo{
			Cid: member.Cid,
			Name: characterMap[member.Cid].Nickname,
			Age: characterMap[member.Cid].Age,
			FightCapacity: characterMap[member.Cid].BattlePower,
			Prestige: characterMap[member.Cid].Prestige,
			Position: int32(member.Identity),
			WeekCredi: member.WeekContributionNum,
			HistoryCredi: member.HistoryContributionNum,
		}
	}
	return memberInfos,0
}

func LoadCityBuilding(buildingTemplates *tsv.CityBuildingTsvManager,ids ...int64)  {
	var tid int64
	if len(ids) > 0 {
		_, body, errCode := app.Discovery().SyncRequest(svc.City, tid, 0, msg_dt.CdCityIdByTemId, &msg_dt.ReqCityIdByTemId{
			CityTempIds: ids,
		})
		if errCode > 0 {
			app.Log().TError(tid,utils.NewError("query city fail ",utils.M{"cityTempIdList":ids}))
			return
		}
		resCity := body.(*msg_dt.ResCityIdByTemId)
		if len(resCity.CityList) > 0 {
			for _,city := range resCity.CityList {
				_, resBody, errCode := app.Discovery().SyncRequest(svc.CityBuilding, tid, 0, msg_dt.CdLoadCityBuildingList, &msg_dt.ReqLoadCityBuildingList{CityId: city.Id})
				if errCode > 0 {
					app.Log().TError(tid,utils.NewError("query city fail ",utils.M{"cityId":city.Id}))
					return
				}
				buildingList := resBody.(*msg_dt.ResLoadCityBuildingList)
				if len(buildingList.Buildings) > 0 {
					nodeType, nodeId, err := spc_common.GetRedisSceneData(misc.SceneCity, int64(city.CityTempId))
					if err != nil {
						app.Log().TError(tid,utils.NewError(err.Error(),utils.M{"cityId":city.Id}))
						return
					}

					for _,building := range buildingList.Buildings {
						buildingTsv := buildingTemplates.TsvMap[building.BuildingTempId]
						app.SceneCache().SaveActor(building.Id, spc_common.ActorBuilding, nil,
							&spc_common.ActorComTransform{
								Position: utils.Vec3{
									X: buildingTsv.BuildCoordinate[0],
									Y: buildingTsv.BuildCoordinate[1],
									Z: buildingTsv.BuildCoordinate[2],
								},
								Forward: utils.Vec3{
									X: 1,
								},
							}, &spc_common.ActorComBuilding{
								BuildingType: int32(building.BuildingType),
								BuildingState: int32(building.BuildingState),
								Level:     building.Level,
								TeamId:    0,
								FactionId: 0,
							})
						if err != nil {
							app.Log().TError(tid,utils.NewError("加载建筑到redis失败",utils.M{
								"building": building,
							}))
							return
						}
						err = spc_common.SetRedisActorSceneData(building.Id, misc.SceneCity, int64(city.CityTempId))
						if err != nil {
							app.Log().TError(tid,utils.NewError(err.Error(),utils.M{"cityId":city.Id}))
							return
						}
						//todo 如果已经进入另一个节点空间,则退出上一个节点再进入当前节点
						app.Discovery().RequestNode(nodeType, nodeId, svc.Space, tid, building.Id, msg_spc.CdActorEnterScene, &msg_spc.ReqActorEnterScene{
							SceneType: misc.SceneCity,
							SceneId:   int64(city.CityTempId),
						}, func(v int64, obj interface{}) {

						}, func(tid int64, ec utils.TErrCode) {
							app.Log().TError(tid,utils.NewError(err.Error(),utils.M{"cityId":city.Id}))
						})
					}




					//app.Discovery().RequestNode(misc.NodeServices_CitySpc, 21, svc.Space, tid, int64(city.CityTempId), msg_spc.CdActorsEnterSpace, &msg_spc.ReqActorsEnterSpace{
					//	Actors: buildingIds,
					//	},
					//	func(v int64, obj interface{}) {
					//
					//	}, func(tid int64, ec utils.TErrCode) {
					//	app.Log().TError(tid,utils.NewError("进入空间失败", utils.M{
					//		"actorIds":  buildingIds,
					//		"error code": ec,
					//	}))
					//})
				}

			}


		}

	}

}