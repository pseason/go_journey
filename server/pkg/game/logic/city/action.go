package city

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/misc"
)

type ICityController interface {
	common.IBaseController
}

// 消息处理器
func (m *cityController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdCityGetList, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			m.CityGetCityList(ac)
			return
		}},
		{MsgCode: msg_gm.CdCityJoinCity, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqCityJoinCity)
			m.CityJoin(ac, req.CityId)
			return
		}},
		{MsgCode: msg_gm.CdCityExitCity, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			m.ExitCIty(ac)
			return
		}},
		{MsgCode: msg_gm.CdCityLoadCity, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			if reqBody == nil {
				ac.ResErr(err_gm.ErrQueryFailed)
				return
			}
			req := reqBody.(*msg_gm.ReqCityLoadCity)
			m.LoadCity(ac,req.CityId)
			return
		}},
		{MsgCode: msg_gm.CdCityUpNameOrAnnouncement, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqCityUpNameOrAnnouncement)
			m.UpCityNameOrAnnouncement(ac,req.UpType,req.UpStr)
			return
		}},
		{MsgCode: msg_gm.CdCityPageCityMember, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqCityPageCityMember)
			m.CityGetAllCityMember(ac,req.Page,req.Size_)
			return
		}},
		{MsgCode: msg_gm.CdCityEnterBroadcastSpe,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			m.EnterBroadcastSpe(ac)
		}},

		{MsgCode: msg_gm.CdCityEnter,PermissionCode: misc.RolePlayer,Action: func(ac common.IActionCtx, reqBody interface{}) {
			m.EnterCitySpace(ac,0)
		}},

	}
}

func (c *cityController) Events() []common.Event {
	return []common.Event{

	}
}