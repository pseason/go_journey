package backpack_shortcut_bar

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
)

type IShortcutBarController interface {
	common.IBaseController
}

func (stbCtr *shortcutBarController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdShortcutBar, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			//req := reqBody.(*msg_gm.ReqShortcutBar)
			stbCtr.GetAllPropsByCid(ac)
			return
		}},

		{MsgCode: msg_gm.CdShortcutBarPutInto, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqShortcutBarPutInto)
			stbCtr.PutInto(ac,req.PropId,req.Index)
			return
		}},

		{MsgCode: msg_gm.CdShortcutBarRemove, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqShortcutBarRemove)
			stbCtr.Remove(ac,req.Index)
			return
		}},
	}
}

