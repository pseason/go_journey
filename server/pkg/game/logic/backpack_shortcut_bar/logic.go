package backpack_shortcut_bar

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/msg_dt"
)

type shortcutBarController struct {
	common.IBaseController
}

func NewShortcutBarController() IShortcutBarController {
	return &shortcutBarController{
		IBaseController: common.NewBaseController(),
	}
}

func (stbCtr *shortcutBarController) GetAllPropsByCid(ac common.IActionCtx) {
	_,resBody,err:= stbCtr.RequestSvc(svc.BackpackShortcutBar, ac.Tid(), 0, msg_dt.CdShortcutBarList,&msg_dt.ReqShortcutBarList{
		Cid:ac.Cid(),
	})
	if err.IsNotNil(){
		app.Log().Error("快捷栏获取数据失败",utils.M{"errCode":err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res,ok := resBody.(*msg_dt.ResShortcutBarList)
	if !ok{
		app.Log().Error("快捷栏获取数据为空",utils.M{"errCode":err})
		ac.ResErr(err_gm.ErrQueryFailed)
	}
	ac.ResOk(&msg_gm.ResShortcutBar{
		Ds:res.ShortcutBarList.Ds,
		Cd:res.ShortcutBarList.Cd,
	})
}

func (stbCtr *shortcutBarController) PutInto(ac common.IActionCtx,PropId,Index int32) {
	_,resBody,err:= stbCtr.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackGetProps,&msg_dt.ReqBackpackGetProps{
		Cid:ac.Cid(),
		PropIds:[]int32{PropId},
	})
	if err.IsNotNil(){
		app.Log().Error("获取背包数据失败",utils.M{"errCode":err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	_, resBody, err = stbCtr.RequestSvc(svc.BackpackShortcutBar, ac.Tid(), 0, msg_dt.CdShortcutBarPutInto,&msg_dt.ReqShortcutBarPutInto{
		PropId:PropId,
		Cid:ac.Cid(),
		Index:Index,
	})
	if err.IsNotNil(){
		app.Log().Error("快捷栏放入物品失败",utils.M{"errCode":err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res,ok := resBody.(*msg_dt.ResShortcutBarPutInto)
	if !ok {
		app.Log().Error("快捷栏返回数据失败",utils.M{"errCode":err})
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}

	ac.ResOk(&msg_gm.ResShortcutBarPutInto{
		Ds:res.ShortcutBarList.Ds,
		Cd:res.ShortcutBarList.Cd,
	})

}

func (stbCtr *shortcutBarController) Remove(ac common.IActionCtx,Index int32) {
	_, resBody, err:= stbCtr.RequestSvc(svc.BackpackShortcutBar, ac.Tid(), 0, msg_dt.CdShortcutBarRemove,&msg_dt.ReqShortcutBarRemove{
		Cid:ac.Cid(),
		Index:Index,
	})
	if err.IsNotNil(){
		app.Log().Error("快捷栏移动失败",utils.M{"errCode":err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res,ok := resBody.(*msg_dt.ResShortcutBarRemove)
	if !ok{
		app.Log().Error("快捷栏移动数据为空",utils.M{"errCode":err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResShortcutBarRemove{
		Ds:res.ShortcutBarList.Ds,
	})
}
