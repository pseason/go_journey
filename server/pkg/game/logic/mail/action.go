package mail

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/mail_e"
	"hm/pkg/services/data/msg_dt"
)

type IMailController interface {
	common.IBaseController
}

// Actions 消息处理器
func (m *mailController) Actions() []common.Action {
	return []common.Action{
		// 根据uid获取角色信息
		{MsgCode: msg_gm.CdMails, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqMails)
			m.GetPageMails(ac, req.Page, req.Size_)
			return
		}},
		{MsgCode: msg_gm.CdMailOperate, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqMailOperate)
			m.MailOperation(ac, req.MailId, req.MailChangeType)
			return
		}},
		{MsgCode: msg_gm.CdUnReadMailCount, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			m.GetUnreadMailCount(ac)
			return
		}},
	}
}



// Events 事件处理器
func (m *mailController) Events() []common.Event {
	return []common.Event{
		{MsgCode: msg_dt.CdEveMailNew, Action: func(ac common.IActionCtx, eveBody interface{}) {
			req := eveBody.(*msg_dt.EveMailNew)
			if req.MailType == mail_e.Personal && len(req.CidList) > 0 {
				m.SendToCharacters(req.CidList, msg_gm.CdNoticeMailNew, nil)
			} else if req.MailType == mail_e.SystemType {
				m.SendToAllUsers(msg_gm.CdNoticeMailNew, nil)
			}
		}},
	}
}
