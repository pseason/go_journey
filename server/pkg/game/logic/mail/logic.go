package mail

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"encoding/json"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/mail_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
)

type mailController struct {
	common.IBaseController
}

func NewMailController() IMailController {
	return &mailController{
		IBaseController: common.NewBaseController(),
	}
}

func (m *mailController) AfterInit() error {
	return nil
}

func (m *mailController) GetPageMails(ac common.IActionCtx, page, size int32) {
	reqSvcMail := &msg_dt.ReqMailPage{
		Page: page,
		Size: size,
		Cid:  ac.Cid(),
	}
	_, body, code := m.RequestSvc(svc.Mail, ac.Tid(), ac.Cid(), msg_dt.CdMailPage, reqSvcMail)
	if code > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	if body == nil {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResMailPage)
	ac.ResOk(&msg_gm.ResMails{
		MailInfoList: m.ProtobufMailList(res.MailInfoList...),
		Total:        res.Total,
		Current:      res.Current,
	})
}

func (m *mailController) MailOperation(ac common.IActionCtx, mailId int64, mailOperation int32) {
	operationType := mail_e.OperationType(mailOperation)
	mails := make([]int64, 0)
	var errCode err_gm.ErrCode
	mailInfoList := make([]*model.Mail, 0)
	switch operationType {
	case mail_e.DELMAIL:
		fallthrough
	case mail_e.DELMAILALL:
		m.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
			body, code := tx.Try(svc.Mail,ac.Cid(),msg_dt.CdMailDel,&msg_dt.ReqMailDel{MailId: mailId, Cid: ac.Cid()})
			if code > 0 {
				ac.ResErr(err_gm.ErrDelFailed)
				return
			}
			mails = body.(*msg_dt.ResMailDel).MailId
			return true
		})
	case mail_e.RECEIVEMAIL:
		m.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
			body, code := tx.Try(svc.Mail,mailId,msg_dt.CdMailById,&msg_dt.ReqMailById{MailId: mailId})
			if code > 0 {
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
			mailInfoList = append(mailInfoList, body.(*msg_dt.ResMailById).Mail)
			return true
		})
		fallthrough
	case mail_e.RECEIVEMAILALL:
		if len(mailInfoList) == 0 {
			_, body, code := m.RequestSvc(svc.Mail, ac.Tid(), ac.Cid(), msg_dt.CdMails, &msg_dt.ReqMails{})
			if code > 0 {
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
			mailInfoList = body.(*msg_dt.ResMails).MailInfoList
		}

		if len(mailInfoList) > 0 {
			for _, mail := range mailInfoList {
				addProps := make(map[int32]int32)
				if mail.MailEnclosures != "0" && mail.MailEnclosures != "" {
					json.Unmarshal([]byte(mail.MailEnclosures), &addProps)
					if len(addProps) > 0 {
						addBackMap := make(map[int32]int32)
						addCharacterMap := make(map[int32]int32)
						for propId,num := range addProps {
							if character_info_e.IsModifiable(propId) {
								addCharacterMap[propId] = num
							}else{
								addBackMap[propId] = num
							}
						}
						//背包物品新增
						m.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
							if len(addBackMap) > 0 {
								_, errCode = tx.Try(svc.Backpack,  ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
									Cid:            ac.Cid(),
									Props:          addBackMap,
								})
								if errCode > 0 {
									return
								}
							}
							if len(addCharacterMap) > 0{
								_,_, errCode = tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, addCharacterMap, ac.Cid())
								if errCode > 0 {
									app.Log().TError(ac.Tid(),utils.NewError("character add money",
										utils.M{"add character money": addCharacterMap}))
									ac.ResErr(err_gm.ErrUpdateFailed)
									return false
								}
							}
							body, code := tx.Try(svc.Mail,mail.Id,msg_dt.CdMailReceive, &msg_dt.ReqMailReceive{Cid: ac.Cid()})
							if code > 0 {
								return
							}
							mails = append(mails, body.(*msg_dt.ResMailReceive).MailId)

							return true
						})
					}
				} else if !mail.IsMailRead {
					m.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
						body,errCode := tx.Try(svc.Mail, mailId,msg_dt.CdMailRead, &msg_dt.ReqMailRead{MailId: mailId, Cid: ac.Cid()})
						if errCode > 0 {
							return
						}
						mails = append(mails, body.(*msg_dt.ResMailRead).MailId)
						return true
					})

				}
			}

		}
	case mail_e.READMAIL:
		m.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
			body,errCode := tx.Try(svc.Mail, mailId,msg_dt.CdMailRead, &msg_dt.ReqMailRead{MailId: mailId, Cid: ac.Cid()})
			if errCode > 0 {
				return
			}
			mails = append(mails, body.(*msg_dt.ResMailRead).MailId)
			return true
		})
	}
	if errCode > 0 {
		//返回领取失败
		ac.ResOk(&msg_gm.ResMailOperate{
			MailChangeType: 5,
		})
		return
	}
	ac.ResOk(&msg_gm.ResMailOperate{
		MailChangeType: mailOperation,
		MailId:         mails,
	})
}

func (m *mailController) GetUnreadMailCount(ac common.IActionCtx) {
	reqSvcMail := &msg_dt.ReqMailCount{
		Cid: ac.Cid(),
	}
	_, body, code := m.RequestSvc(svc.Mail, ac.Tid(), 0, msg_dt.CdMailCount, reqSvcMail)
	if code > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResMailCount)
	ac.ResOk(&msg_gm.ResUnReadMailCount{
		Count: res.UnReadCount,
	})
}

func (m *mailController) ProtobufMailList(mails ...*model.Mail) (mailInfos []*msg_gm.MailInfo) {
	for _, mail := range mails {
		mailInfos = append(mailInfos, m.protobufMail(mail))
	}
	return
}

func (m *mailController) protobufMail(mail *model.Mail) (res *msg_gm.MailInfo) {
	res = &msg_gm.MailInfo{}
	res.MailId = mail.Id
	res.MailName = mail.MailTitle
	res.MailFrom = mail.MailFrom
	res.MailContent = mail.MailContent
	res.IsMailReceive = mail.IsMailReceive
	res.IsMailRead = mail.IsMailRead
	res.MailFromTime = mail.Created
	res.EnclosuresOverdueTime = mail.MailTime
	if mail.MailEnclosures != "0" {
		propMap := make(map[int32]int32)
		json.Unmarshal([]byte(mail.MailEnclosures), &propMap)
		if len(propMap) > 0 {
			for k, v := range propMap {
				res.MailEnclosures = append(res.GetMailEnclosures(), &msg_gm.Enclosure{
					PropId: k,
					Count:  v,
				})
			}
		}
	}
	return
}
