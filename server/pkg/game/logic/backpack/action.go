package backpack

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/model"
)

type IBackpackController interface {
	common.IBaseController
}

func (b *backpackController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdBackpack, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			//req := reqBody.(*msg_gm.ReqBackpack)
			b.GetAllPropsByCid(ac,model.BackpackTypeAll)
			return
		}},
		//客户端-丢弃物品
		{MsgCode: msg_gm.CdBackpackDiscard, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackDiscard)
			b.Discard(ac, req.Bid)
			return
		}},
		{MsgCode: msg_gm.CdBackpackArrange, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackArrange)
			b.Arrange(ac,int32(req.BackpackType))
			return
		}},
		{MsgCode: msg_gm.CdBackpackDivision, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackDivision)
			b.Division(ac, req.Bid, req.SplitNum)
			return
		}},
		{MsgCode: msg_gm.CdBackpackUseProp, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackUseProp)
			b.UseProp(ac, req.Bid)
			return
		}},
		{MsgCode: msg_gm.CdBackpackMove, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackMove)
			b.Move(ac, req.BackpackId, int32(req.ToType))
			return
		}},
		{MsgCode: msg_gm.CdBackpackSells, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackSells)
			b.Sells(ac, req.Props)
			return
		}},
		{MsgCode: msg_gm.CdBackpackCompose, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackCompose)
			b.Compose(ac, req.Props)
			return
		}},
		{MsgCode: msg_gm.CdBackpackGoldExchange, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackGoldExchange)
			b.GoldExchange(ac, req.AttrId,req.Num)
			return
		}},
		{MsgCode: msg_gm.CdBackpackSubstrTry, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackSubstrTry)
			b.BackpackSubstrTry(ac, int32(req.BackpackType),req.SubstrProps,req.IsFullSendMail)
			return
		}},
		{MsgCode: msg_gm.CdBackpackIncreaseTry, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqBackpackIncreaseTry)
			b.BackpackIncreaseTry(ac,req.IncreaseProps,req.IsFullSendMail)
			return
		}},
	}
}
