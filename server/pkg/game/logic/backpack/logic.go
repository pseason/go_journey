package backpack

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/logic/backpack"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
)

type backpackController struct {
	common.IBaseController
	pTsv     map[int32]*tsv.PropTsv
}

func NewBackpackController() IBackpackController {
	return &backpackController{
		IBaseController: common.NewBaseController(),
	}
}

func (b *backpackController) AfterInit() error {
	b.pTsv = tsv.GetTsvManager(tsv.Prop).(*tsv.PropTsvManager).TsvMap
	return nil
}

func (b *backpackController) Events() []common.Event {
	return []common.Event{
		{MsgCode: msg_dt.CdEveBackpackGainNotify,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				req := eveBody.(*msg_dt.EveBackpackGainNotify)
				b.SendToCharacter(req.Cid, msg_gm.CdNoticeBackpackGainNotify, &msg_gm.NoticeBackpackGainNotify{
					List: b.ProtoBackpackList(req.Data...),
				})
			}},
		{MsgCode: msg_dt.CdEveBackpackSysAddNotify,
			Action: func(ac common.IActionCtx, eveBody interface{}) {
				req := eveBody.(*msg_dt.EveBackpackSysAddNotify)
				b.SendToCharacter(req.Cid, msg_gm.CdNoticeBackpackSysAddNotify, &msg_gm.NoticeBackpackSysAddNotify{
					List: req.Data,
				})
			}},
	}
}

func (b *backpackController) GetAllPropsByCid(ac common.IActionCtx, Type model.BackpackType) {
	//查询仓库等级
	UserInfo,err := b.RequestCharacterInfoQueryByCid(ac.Tid(),[]character_info_e.Enum{character_info_e.WareHouseLv},ac.Cid())
	if err.IsNotNil() {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	_, resBody, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackList, &msg_dt.ReqBackpackList{
		BackpackType: Type,
		Cid:          ac.Cid(),
		WarehouseLevel:UserInfo.WareHouseLv,
	})
	if err.IsNotNil() {
		app.Log().Error("背包获取数据失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res, ok := resBody.(*msg_dt.ResBackpackList)
	if !ok {
		app.Log().Error("获取数据为空", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResBackpack{
		Props:            b.ProtoBackpackList(res.BackpackList...),
		MaxLatticeNumber: res.Num,
		CurrentWarehouseNumber:res.WarehouseNum,
	})
}

func (b *backpackController) Discard(ac common.IActionCtx, Bid int64) {
	_, _, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackDiscard, &msg_dt.ReqBackpackDiscard{
		Bid: Bid,
		Cid: ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("背包丢弃失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResBackpackDiscard{})
}

func (b *backpackController) Arrange(ac common.IActionCtx, backpackType int32) {
	_, _, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackArrange, &msg_dt.ReqBackpackArrange{
		Cid:  ac.Cid(),
		Type: model.BackpackType(backpackType),
	})
	if err.IsNotNil() {
		app.Log().Error("背包物品整理失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResBackpackArrange{})
}

func (b *backpackController) Division(ac common.IActionCtx, Bid int64, SplitNum int32) {
	_, _, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackDivision, &msg_dt.ReqBackpackDivision{
		Bid:      Bid,
		SplitNum: SplitNum,
		Cid:      ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("物品拆分失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResBackpackDivision{})
}

func (b *backpackController) UseProp(ac common.IActionCtx, Bid int64) {
	_, resBody, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackUseProp, &msg_dt.ReqBackpackUseProp{
		Bid: Bid,
		Cid: ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("使用物品失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := resBody.(*msg_dt.ResBackpackUseProp)
	//todo 属性增益
	if res.Type == backpack.RestorePlayerAttrProps {

	} else if res.Type == backpack.Addition {
		//todo 月卡 vip卡使用-时长增长
	}
	ac.ResOk(&msg_gm.ResBackpackUseProp{})
}

func (b *backpackController) Move(ac common.IActionCtx, Bid int64, backpackType int32) {
	//移到仓库需要查询仓库等级
	var warehouseLevel int32 = 0
	if model.BackpackType(backpackType) == model.BackpackTypeWarehouse {
		//获取角色信息
		CharacterInfo,err :=b.RequestCharacterInfoQueryByCid(ac.Tid(),nil,ac.Cid())
		if err.IsNotNil(){
			app.Log().Error("查询角色失败", utils.M{"err": err.Error()})
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		warehouseLevel = CharacterInfo.WareHouseLv
	}
	_, _, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackMove, &msg_dt.ReqBackpackMove{
		Bid:            Bid,
		Cid:            ac.Cid(),
		WarehouseLevel: warehouseLevel,
	})
	if err.IsNotNil() {
		app.Log().Error("移动背包物品失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResBackpackMove{})
}

//回收
func (b *backpackController) Sells(ac common.IActionCtx, Props map[int64]int32) {
	bids := make([]int64,0,len(Props))
	for bid := range Props{
		bids = append(bids,bid)
	}
	_, res, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackGetPropsByIds, &msg_dt.ReqBackpackGetPropsByIds{
		Bids: bids,
		Cid:   ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("Failed to retrieve items", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	//获取回收金币
	var gold int32 = 0
	propsInfo := res.(*msg_dt.ResBackpackGetPropsByIds)
	for _,v:= range propsInfo.Props {
		propTsv, has := b.pTsv[v.PropId]
		if !has {
			app.Log().Error("Item template does not exist", utils.M{"errCode": err})
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		if Props[v.Id] > v.Num {
			app.Log().Error("Insufficient quantity of goods", utils.M{"errCode": err})
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		if propTsv.Sell != 0 {
			gold += propTsv.Sell * Props[v.Id]
		}
	}

	b.Transaction(ac.Tid(), func(tx common.ITx) bool {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtractById, &msg_dt.ReqBackpackSubtractById{
			PropType:       model.BackpackTypeDefault,
			Props:          Props,
			Cid:            ac.Cid(),
		})
		if ec > 0 {
			return false
		}
		if gold >0 {
			_, _, errCode := tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, map[character_info_e.Enum]int32{
				character_info_e.Enum(2001): gold,
			}, ac.Cid())
			if errCode != 0 {
				app.Log().TError(ac.Tid(), utils.NewError("character lack of gold",utils.M{"need purchase gold": gold}))
				ac.ResErr(err_gm.ErrUpdateFailed)
				return false
			}
		}

		return true
	})
	ac.ResOk(&msg_gm.ResBackpackSells{})
}

func (b *backpackController) PropGiftOpen(ac common.IActionCtx, Bid int64, SelectIndex int32) {
	_, _, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackGiftOpen, &msg_dt.ReqBackpackGiftOpen{
		Bid:         Bid,
		SelectIndex: SelectIndex,
		Cid:         ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("道具使用失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResBackpackGiftOpen{})
}

// 物品合成-客户端传的值是Props：propID=>最终合成的数量
func (b *backpackController) Compose(ac common.IActionCtx, Props map[int32]int32) {
	addProps := make(map[int32]int32,0)
	needProps := make(map[int32]int32,0)
	for propId, Num := range Props {
		if Num <= 0 {
			app.Log().Error("数量不能为空", utils.M{"errCode": nil})
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		//获取合成的材料叠套最大值所需的格子数
		p, has := b.pTsv[propId]
		if !has {
			app.Log().Error("物品模板为空", utils.M{"errCode": nil})
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		if len(p.Compose) <= 0 {
			app.Log().Error("该物品不能被合成", utils.M{"errCode": nil})
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		toPropId, toPropNum :=  p.Compose[0], p.Compose[1]
		needProps[propId] = toPropNum * Num
		addProps[toPropId] = Num
	}

	b.Transaction(ac.Tid(), func(tx common.ITx) bool {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtract, &msg_dt.ReqBackpackSubtract{
			PropType:       model.BackpackTypeDefault,
			Props:          needProps,
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec > 0 {
			return false
		}
		_, ec = tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
			Props:          addProps,
			Cid:            ac.Cid(),
			IsFullSendMail: false,
		})
		if ec > 0 {
			return false
		}
		return true
	})
	ac.ResOk(&msg_gm.ResBackpackCompose{})
}

func (b *backpackController) GoldExchange(ac common.IActionCtx, Aid int64, Num int32) {
	_, _, err := b.RequestSvc(svc.Backpack, ac.Tid(), 0, msg_dt.CdBackpackGoldExchange, &msg_dt.ReqBackpackGoldExchange{
		AttrId: Aid,
		Num:    Num,
		Cid:    ac.Cid(),
	})
	if err.IsNotNil() {
		app.Log().Error("金币兑换失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	ac.ResOk(&msg_gm.ResBackpackGoldExchange{})
}

func (b *backpackController) BackpackSubstrTry(ac common.IActionCtx, backpackType int32, substrProps map[int32]int32, isFullSendMail bool) {
	b.Transaction(ac.Tid(), func(tx common.ITx) bool {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtract, &msg_dt.ReqBackpackSubtract{
			PropType:       model.BackpackType(backpackType),
			Props:          substrProps,
			Cid:            ac.Cid(),
			IsFullSendMail: isFullSendMail,
		})
		if ec > 0 {
			return false
		}
		//resBody := resSubstr.(*msg_dt.ResBackpackSubtract)
		ac.ResOk(&msg_gm.ResBackpackSubstrTry{})
		return true
	})
}

func (b *backpackController) BackpackIncreaseTry(ac common.IActionCtx, IncreaseProps map[int32]int32, isFullSendMail bool) {
	b.Transaction(ac.Tid(), func(tx common.ITx) bool {
		resIncrease, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
			Cid:            ac.Cid(),
			Props:          IncreaseProps,
			IsFullSendMail: isFullSendMail,
		})
		if ec > 0 {
			return false
		}
		resBody := resIncrease.(*msg_dt.ResBackpackIncrease)
		//新增物品溢出发送邮件
		if isFullSendMail && len(resBody.OverProps) > 0 {
			//todo 邮件发送服务
			//创建mail 对象
			overMail := make([]*model.Mail,0)
			mailProp := make(map[int64]int32,0)
			mailProps := make([]map[int64]int32,0)
			for propId,num := range resBody.OverProps {
				PropId := int64(propId)
				mailProp[PropId] = num
				if len(mailProp) >= 4{
					mailProps = append(mailProps,mailProp)
					mailProp = make(map[int64]int32,0)
				}
			}
			if len(mailProp) > 0{
				mailProps = append(mailProps,mailProp)
			}
			for _,p := range mailProps {
				m,err:= model.NewSystemMail(ac.Cid(),"附件","背包不足，以邮件发送",p)
				if err != nil{
					app.Log().Error("邮件创建失败", utils.M{"errCode": err})
					return false
				}
				overMail = append(overMail,m)
			}
			_, ec =tx.Try(svc.Mail,ac.Cid(),msg_dt.CdMailSend,&msg_dt.ReqMailSend{
				Mails: overMail,
			})
			if ec > 0 {
				return false
			}
		}

		ac.ResOk(&msg_gm.ResBackpackIncreaseTry{})
		return true
	})
}

func (b *backpackController) ProtoBackpackList(lists ...*model.Backpack) (sliceList []*msg_gm.Props) {
	for _, items := range lists {
		sliceList = append(sliceList, &msg_gm.Props{
			PropId:   items.PropId,
			Id:       items.Id,
			Type:     int32(items.Type),
			Number:   items.Num,
			Position: items.Position,
			Sn:       items.Sn,
		})
	}
	return
}
