package city_farm

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
)

type ICityFarmController interface {
	common.IBaseController
}

// 消息处理器
func (c *cityFarmController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdFarmInfo, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqFarmInfo)
			c.GetFarmInfo(ac, req.BuildId)
			return
		}},
		{MsgCode: msg_gm.CdFarmPutin, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqFarmPutin)
			c.CityBuildingFarmPlanting(ac, req.BuildId, req.ItemId)
			return
		}},
		{MsgCode: msg_gm.CdFarmWipe, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqFarmWipe)
			c.EradicateCrop(ac, req.BuildId)
			return
		}},
		{MsgCode: msg_gm.CdCargoHarvest, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			c.Granary(ac)
			return
		}},
		{MsgCode: msg_gm.CdFarmInvest, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqFarmInvest)
			c.Irrigation(ac, req.BuildId)
			return
		}},
	}
}
