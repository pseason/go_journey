package city_farm

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"github.com/RussellLuo/timingwheel"
	"github.com/panjf2000/ants/v2"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc/tsv"
	util "hm/pkg/misc/utils"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
	"time"
)

type cityFarmController struct {
	common.IBaseController
	tw             *timingwheel.TimingWheel
	delayIdToTimer map[int64]*timingwheel.Timer
	cropCsv        *tsv.CropTsvManager
	farmland       *tsv.FarmlandTsvManager
}

func NewCityFarmController() ICityFarmController {
	return &cityFarmController{
		IBaseController: common.NewBaseController(),
	}
}

func (c *cityFarmController) AfterInit() error {
	c.delayIdToTimer = make(map[int64]*timingwheel.Timer)
	c.tw = timingwheel.NewTimingWheel(time.Millisecond, 20)
	c.tw.Start()
	c.cropCsv = tsv.GetTsvManager(tsv.Crop).(*tsv.CropTsvManager)
	c.farmland = tsv.GetTsvManager(tsv.Farmland).(*tsv.FarmlandTsvManager)
	//go func() {
	//time.Sleep(3 * time.Second)
	//c.CityBuildingFarmPlanting(common.NewActionCtx("", 0, 1), 1469562629411856392, 2050001)
	//c.CityBuildingInvestResources(common.NewActionCtx("",0,1),1468421334177964036,
	//	map[int32]int32{3090001:27,3090004:16,3090011: 4})
	//c.CityBuildingInvestEnergyNum(common.NewActionCtx("",0,1),1468421334177964036)
	//}()
	return nil
}

//获取畜牧场粮仓信息
func (c *cityFarmController) GetFarmInfo(ac common.IActionCtx, buildingId int64) {
	//查询指定建筑
	_, resBody, err := c.RequestSvc(svc.CityFarm, ac.Tid(), 0, msg_dt.CdCityFarmGranaryAndFarmInfo, &msg_dt.ReqCityFarmGranaryAndFarmInfo{
		BuildingId: buildingId,
		Cid:        ac.Cid(),
	})
	if err > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	res := resBody.(*msg_dt.ResCityFarmGranaryAndFarmInfo)
	ac.ResOk(&msg_gm.ResFarmInfo{FarmData: &msg_gm.FarmData{ItemId: res.FarmData.ItemId, Life: res.FarmData.Life, Forage: res.FarmData.Forage},
		CargoData: res.GranaryMap})
}

//畜牧场种植
func (c *cityFarmController) CityBuildingFarmPlanting(ac common.IActionCtx, buildingId int64, propId int32) {
	data, errCode := c.RequestSvcQueryById(svc.CharacterInfo, ac.Tid(), model_e.CharacterInfo, []character_info_e.Enum{character_info_e.Nickname, character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	characterInfo := data.(*model.CharacterInfo)
	//查询指定建筑
	_, resBody, errCode := c.RequestSvc(svc.CityBuilding, ac.Tid(), characterInfo.CityId, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{
		BuildingId: buildingId,
	})
	if errCode > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	building := resBody.(*msg_dt.ResCityBuildingGetBuilding).Building
	cropCsv, ok := c.cropCsv.TsvMap[propId]
	if !ok {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}

	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtract, &msg_dt.ReqBackpackSubtract{
			PropType:       model.BackpackTypeDefault,
			Props:          map[int32]int32{propId: 1},
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec > 0 {
			app.Log().Error("背包物品扣除失败", utils.M{"errCode": errCode})
			ac.ResErr(err_gm.ErrCreateFailed)
			return
		}
		//进行种植
		body, err := tx.Try(svc.CityFarm, building.Id, msg_dt.CdCityFarmPlanting, &msg_dt.ReqCityFarmPlanting{
			Building: building,
			PropId:   propId,
		})
		if err > 0 {
			ac.ResErr(err_gm.ErrQueryFailed)
			return
		}
		resFarm := body.(*msg_dt.ResCityFarmPlanting)
		//开启倒计时
		farmlandCsv := c.farmland.TsvMap[building.Level]
		cdTime := float32(cropCsv.MatureTime) * float32(time.Minute)
		intervalTime := cdTime - cdTime*farmlandCsv.RewardTimeAddition
		ants.Submit(func() {
			c.delayIdToTimer[building.Id] = c.tw.AfterFunc(time.Duration(intervalTime), func() {
				c.GiveOutRewards(ac.Tid(), building.Id, building.CityId)
			})
		})

		res := &msg_gm.ResFarmPutin{FarmData: &msg_gm.FarmData{
			ItemId: resFarm.Farm.CropOmId, Life: int32(resFarm.Farm.CropHp-util.GetCurrentMillisecond()) / 1000, Forage: resFarm.Farm.CropPabulum,
		}}
		ac.ResOk(res)
		app.Log().Info("返回农场数据", utils.M{"res:": res})
		return true
	})
}

//灌溉
func (c *cityFarmController) Irrigation(ac common.IActionCtx, buildingId int64) {
	//查询指定建筑
	_, resBody, err := c.RequestSvc(svc.CityBuilding, ac.Tid(), 0, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{
		BuildingId: buildingId,
	})
	if err > 0 {
		app.Log().Error("查询建筑失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	building := resBody.(*msg_dt.ResCityBuildingGetBuilding).Building
	//查询畜牧场种植物
	_, resBody, err = c.RequestSvc(svc.CityFarm, ac.Tid(), 0, msg_dt.CdCityFarmCrop, &msg_dt.ReqCityFarmCrop{
		BuildingId: buildingId,
	})
	if err > 0 {
		app.Log().Error("查询畜牧场种植物失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	cropId := resBody.(*msg_dt.ResCityFarmCrop).CropId
	cropCsv, ok := c.cropCsv.TsvMap[cropId]
	if !ok {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
		_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackSubtract, &msg_dt.ReqBackpackSubtract{
			PropType:       model.BackpackTypeDefault,
			Props:          map[int32]int32{cropCsv.NeedResource[0]: cropCsv.NeedResource[1]},
			Cid:            ac.Cid(),
			IsFullSendMail: true,
		})
		if ec > 0 {
			ac.ResErr(err_gm.ErrCreateFailed)
			app.Log().Error("背包物品扣除失败", utils.M{"errCode": err})
			return
		}
		_, err = tx.Try(svc.CityFarm, building.Id, msg_dt.CdCityFarmIrrigation, &msg_dt.ReqCityFarmIrrigation{
			BuildingId: building.Id,
			BuildingLv: building.Level,
		})
		if err > 0 {
			app.Log().Error("畜牧场灌溉失败", utils.M{"errCode": err})
			ac.ResErr(err_gm.ErrUpdateFailed)
			return
		}
		ac.ResOk(&msg_gm.ResFarmInvest{BuildId: buildingId})
		return true
	})

}

//铲除农作物
func (c *cityFarmController) EradicateCrop(ac common.IActionCtx, buildingId int64) {
	_, _, err := c.RequestSvc(svc.CityFarm, ac.Tid(), 0, msg_dt.CdCityFarmEradicateCrop, &msg_dt.ReqCityFarmEradicateCrop{
		BuildingId: buildingId,
	})
	if err > 0 {
		app.Log().Error("铲除农作物失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrDelFailed)
		return
	}
	timer, ok := c.delayIdToTimer[buildingId]
	if ok {
		if timer.Stop() {
			delete(c.delayIdToTimer, buildingId)
		}
	}
	ac.ResOk(&msg_gm.ResFarmWipe{BuildId: buildingId})
}

//发放种植物成熟奖励
func (c *cityFarmController) GiveOutRewards(tid, buildingId, cityId int64) {
	_, resBody, errCode := c.RequestSvc(svc.City, tid, 0, msg_dt.CdCityMemberList, &msg_dt.ReqCityMemberList{
		CityId: cityId,
	})
	if errCode > 0 {
		app.Log().Error("查询城邦玩家失败", utils.M{"errCode": errCode})
	}
	memberList := resBody.(*msg_dt.ResCityMemberList).CityMemberList
	//查询指定建筑
	_, resBody, err := c.RequestSvc(svc.CityBuilding, tid, 0, msg_dt.CdCityBuildingGetBuilding, &msg_dt.ReqCityBuildingGetBuilding{
		BuildingId: buildingId,
	})
	if err > 0 {
		app.Log().Error("查询畜牧场失败", utils.M{"errCode": err})
		return
	}
	building := resBody.(*msg_dt.ResCityBuildingGetBuilding).Building
	//查询指定建筑
	_, resBody, err = c.RequestSvc(svc.CityFarm, tid, 0, msg_dt.CdCityFarmGiveOutRewards, &msg_dt.ReqCityFarmGiveOutRewards{
		Building:       building,
		CityMemberList: memberList,
	})
	if err > 0 {
		app.Log().Error("畜牧场奖励发送失败", utils.M{"errCode": err, "building": building})
		return
	}
	farm := resBody.(*msg_dt.ResCityFarmGiveOutRewards).Farm
	c.CheckCoreHp(tid, farm, building.Level)
}

//收获粮仓
func (c *cityFarmController) Granary(ac common.IActionCtx) {
	//查询个人粮仓
	_, resBody, err := c.RequestSvc(svc.CityFarm, ac.Tid(), 0, msg_dt.CdCityFarmGranary, &msg_dt.ReqCityFarmGranary{
		Cid: ac.Cid(),
	})
	if err > 0 {
		app.Log().Error("查询粮仓失败", utils.M{"errCode": err})
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	granaryPropMap := resBody.(*msg_dt.ResCityFarmGranary).GranaryPropMap
	if len(granaryPropMap) > 0 {
		c.Transaction(ac.Tid(), func(tx common.ITx) (ok bool) {
			_, ec := tx.Try(svc.Backpack, ac.Cid(), msg_dt.CdBackpackIncrease, &msg_dt.ReqBackpackIncrease{
				Props:          granaryPropMap,
				Cid:            ac.Cid(),
				IsFullSendMail: false,
			})
			if ec > 0 {
				app.Log().Error("将物品放入背包失败", utils.M{"errCode": err})
				ac.ResErr(err_gm.ErrUpdateFailed)
				return
			}
			//收获粮仓
			_, resBody, err = c.RequestSvc(svc.CityFarm, ac.Tid(), 0, msg_dt.CdCityFarmReceiveFood, &msg_dt.ReqCityFarmReceiveFood{
				Cid: ac.Cid(),
			})
			if err > 0 {
				app.Log().Error("收获粮仓失败", utils.M{"errCode": err})
				ac.ResErr(err_gm.ErrDelFailed)
				return
			}
			ac.ResOk(&msg_gm.ResCargoHarvest{})
			return true
		})

	}

}

//效验种植物生命值
func (c *cityFarmController) CheckCoreHp(tid int64, farmland model.CityFarmland, buildingLv int32) {
	cropCsv, ok := c.cropCsv.TsvMap[farmland.CropOmId]
	if !ok {
		app.Log().Error("获取种子表失败", utils.M{"cropOmId": farmland.CropOmId})
		return
	}
	farmlandCsv := c.farmland.TsvMap[buildingLv]
	//检测种子能否再次产出或者就快销毁了
	cdTime := float32(cropCsv.MatureTime) * float32(time.Minute)
	intervalTime := cdTime - cdTime*farmlandCsv.RewardTimeAddition
	nextTime := time.Now().UnixNano()/1e6 + int64(intervalTime)/1e6
	//判断种植物剩余时间能否发放下一波奖励，如果不能则开启销毁农作物倒计时
	if nextTime < farmland.CropHp {
		ants.Submit(func() {
			c.delayIdToTimer[farmland.BuildId] = c.tw.AfterFunc(time.Duration(intervalTime), func() {
				c.GiveOutRewards(tid, farmland.BuildId, farmland.CityId)
			})
		})
		return
	} else {
		app.Log().Info("开始关闭倒计时", utils.M{"操作粮仓编号为:": farmland.Id})
		needTime := farmland.CropHp*1e6 - time.Now().UnixNano()
		if needTime > 0 {
			c.delayIdToTimer[farmland.BuildId] = c.tw.AfterFunc(time.Duration(needTime), func() {
				//清空种植数据
				_, _, err := c.RequestSvc(svc.CityFarm, tid, 0, msg_dt.CdCityFarmClear, &msg_dt.ReqCityFarmClear{
					FarmId: farmland.Id,
				})
				if err > 0 {
					app.Log().Error("clear farm data fail", utils.M{"farmId": farmland.Id})
				}
			})
		} else {
			//清空种植数据
			_, _, err := c.RequestSvc(svc.CityFarm, tid, 0, msg_dt.CdCityFarmClear, &msg_dt.ReqCityFarmClear{
				FarmId: farmland.Id,
			})
			if err > 0 {
				app.Log().Error("clear farm data fail", utils.M{"farmId": farmland.Id})
			}
			delete(c.delayIdToTimer, farmland.BuildId)
		}

		return
	}
}
