package event

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/msg_gm/err_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/city_e"
	"hm/pkg/services/data/msg_dt"
)

type eventController struct {
	common.IBaseController
	enterCitySpaceQueue map[int64][]queuePlayer
}

type queuePlayer struct {
	cid             int64
	queuePlayerType city_e.QueuePlayerType
}

func NewEventController() IEventController {
	return &eventController{
		IBaseController:     common.NewBaseController(),
		enterCitySpaceQueue: make(map[int64][]queuePlayer),
	}
}

func (e *eventController) AfterInit() error {
	//go func() {
	//	time.Sleep(3 * time.Second)
	//	c.CityGetCityList(common.NewActionCtx("",0,0))
	//}()

	return nil
}

//获取城邦列表
func (e *eventController) EventCityEvent(ac common.IActionCtx, page, size, eventType int32) {
	characterInfo, errCode := e.RequestCharacterInfoQueryByCid(ac.Tid(), []character_info_e.Enum{character_info_e.CityId}, ac.Cid())
	if errCode > 0 {
		app.Log().TError(ac.Tid(), utils.NewError("query failed", nil))
		ac.ResErr(err_gm.ErrCreateFailed)
		return
	}
	if characterInfo.CityId == 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
	}
	_, body, code := e.RequestSvc(svc.Event, ac.Tid(), ac.Cid(), msg_dt.CdEventPageGetCityEvent, &msg_dt.ReqEventPageGetCityEvent{
		CityId:    characterInfo.CityId,
		Page:      page,
		Size:      size,
		EventType: eventType,
	})
	if code > 0 {
		ac.ResErr(err_gm.ErrQueryFailed)
		return
	}
	res := body.(*msg_dt.ResEventPageGetCityEvent)
	eventData := make([]*msg_gm.EventData, 0)
	if len(res.Events) > 0 {
		for _, event := range res.Events {
			eventData = append(eventData, &msg_gm.EventData{
				CreateTime:     event.Created,
				Context:        event.Context,
				BuildType:      event.OperationType,
				DeliveryPerson: event.Name,
			})
		}
	}
	ac.ResOk(&msg_gm.ResCityEvent{
		EventTotal: res.Total,
		EventList:  eventData,
	})
}
