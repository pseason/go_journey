package event

import (
	"hm/pkg/game/common"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
)

type IEventController interface {
	common.IBaseController
}

// 消息处理器
func (e *eventController) Actions() []common.Action {
	return []common.Action{
		{MsgCode: msg_gm.CdCityEvent, PermissionCode: misc.RolePlayer, Action: func(ac common.IActionCtx, reqBody interface{}) {
			req := reqBody.(*msg_gm.ReqCityEvent)
			e.EventCityEvent(ac,req.Page,req.Size_,req.Type)
			return
		}},
	}
}
