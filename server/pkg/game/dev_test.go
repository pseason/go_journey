package game

import (
	"95eh.com/eg/utils"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"testing"
)

func TestJson(t *testing.T) {
	jsonConf := jsoniter.Config{
		EscapeHTML: true,
	}.Froze()

	utils.SetSnowflakeRegionNodeId(1)
	id := utils.GenSnowflakeRegionNodeId()
	str, err := jsonConf.MarshalToString(utils.M{
		"id": id,
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(str)
}
