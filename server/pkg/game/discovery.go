package game

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"context"
	gm_common "hm/pkg/game/common"
	"hm/pkg/game/logic/city"
	"hm/pkg/login/msg_lg"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/model"
)

func onNodeConnected(node intfc.TNode, regionId, nodeId uint16, conf *gm_common.Config) {
	dis := app.Discovery()
	switch node {
	case misc.NodeLogin:
		dis.RequestNode(node, nodeId, 0, 0, 0, msg_lg.RCdAddGameNode, &msg_lg.ReqRAddGameNode{
			RegionId:    dis.RegionId(),
			NodeId:      dis.NodeId(),
			GateTcpAddr: conf.Game.Gate.Ip + conf.Game.Gate.Port,
			Count:       app.UserGate().Len(),
			Cap:         conf.Game.Gate.Cap,
			HttpAddr:    conf.Http.Ip + conf.Http.Port,
		}, func(_ int64, obj interface{}) {
			app.Log().Info("add game success", utils.M{
				"region":    dis.GetRegionName(dis.RegionId()),
				"node id":   dis.NodeId(),
				"tcp addr":  conf.Game.Gate.Ip + conf.Game.Gate.Port,
				"tcp cap":   conf.Game.Gate.Cap,
				"http addr": conf.Http.Ip + conf.Http.Port,
			})
		}, func(_ int64, ec utils.TErrCode) {
			app.Log().Error("add game failed", utils.M{
				"region":     dis.GetRegionName(dis.RegionId()),
				"node id":    dis.NodeId(),
				"tcp addr":   conf.Game.Gate.Ip + conf.Game.Gate.Port,
				"tcp cap":    conf.Game.Gate.Cap,
				"http addr":  conf.Http.Ip + conf.Http.Port,
				"error code": ec.Desc(),
			})
		})
	case misc.NodeServices_CitySpc:

		result, err := app.Discovery().Redis().Get(context.Background(), "start_scene_id").Result()
		if err != nil {
			app.Log().Error(err.Error(), utils.M{})
			return
		}
		cityTempList := model.GetTeamMemberList(result)
		app.Timer().After(3000, func() {
			buildingTemplates := tsv.GetTsvManager(tsv.CityBuilding).(*tsv.CityBuildingTsvManager)
			city.LoadCityBuilding(buildingTemplates, cityTempList...)
		})
		err = app.Discovery().Redis().Del(context.Background(), "start_scene_id").Err()
		if err != nil {
			app.Log().Error(err.Error(), utils.M{"del redis city init building fail": "start_scene_id"})
			return
		}
	}
}

func onNodeClose(node intfc.TNode, regionId, nodeId uint16, conf *gm_common.Config) {

}

func updateGateCount() {
	dis := app.Discovery()
	dis.DispatchEvent(misc.NodeLogin, 0, 0, 0, msg_lg.RCdUpdateGameNodeCount, &msg_lg.EveRUpdateGameNodeCount{
		RegionId: dis.RegionId(),
		NodeId:   dis.NodeId(),
		Count:    app.UserGate().Len(),
	})
}
