package common

/*
@Time   : 2021-12-21 22:33
@Author : wushu
@DESC   :
*/

// 错误描述
const (
	TQueryErr  = "query err"
	TCreateErr = "create err"
	TUpdateErr = "update err"
	TDeleteErr = "delete err"
	TDeCodeErr = "decode err"
)

// 非错误描述
const ()
