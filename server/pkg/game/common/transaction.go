package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/tx"
	"95eh.com/eg/utils"
	"github.com/go-redis/redis/v8"
	"github.com/mitchellh/mapstructure"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
	"math"
)

/*
@Time   : 2022-01-10 16:52
@Author : wushu
@DESC   :
*/

type ITx interface {
	intfc.ITx

	// -----------↓↓↓---数据服务通用接口---↓↓↓--------------

	// 根据cid批量查询。查询时会给对应的数据加锁
	TryQueryByIds(svc svc.Svc, model model_e.Model, fields []model_field_e.Enum, ids ...int64) (data map[int64]interface{}, errCode err_dt.ErrCode)
	// 根据非id条件批量查询

	// 根据非id条件批量查询

	// 根据id更新
	TryUpdateById(svc svc.Svc, model model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (data interface{}, errCode err_dt.ErrCode)
	// 根据id对数值数据进行增减
	// @return data 响应数据，默认为nil。只有请求的服务重写了相关接口，才能设置该值
	TryUpdateNumById(svc svc.Svc, model model_e.Model, incrData map[model_field_e.Enum]int32, id int64) (origData interface{}, data interface{}, errCode err_dt.ErrCode)

	// --↓使用频繁的通用接口，对应模块单独封装。相比通用接口仅直接返回对应的结构体

	// 根据cid批量查询角色信息。查询时会给对应数据加锁
	TryCharacterInfoQueryByCids(fields []character_info_e.Enum, cids ...int64) (data map[int64]*model.CharacterInfo, errCode err_dt.ErrCode)
	// 根据cid更新角色信息
	TryUpdateCharacterInfoByCid(upData map[model_field_e.Enum]interface{}, cid int64) (data interface{}, errCode err_dt.ErrCode)
	// 根据cid更新角色数值数据
	TryUpdateCharacterInfoNumByCid(incrData map[model_field_e.Enum]int32, cid int64) (origData interface{}, data interface{}, errCode err_dt.ErrCode)

	// --↑使用频繁的通用接口

	// -----------↑↑↑---数据服务通用接口---↑↑↑--------------

}

type Tx struct {
	intfc.ITx
}

func NewTx(pid int64, client *redis.Client, confirm, cancel utils.Action) ITx {
	return &Tx{ITx: tx.NewTx(pid, client, confirm, cancel)}
}

func (tx *Tx) TryQueryByIds(svc svc.Svc, model model_e.Model, fields []model_field_e.Enum, ids ...int64) (data map[int64]interface{}, errCode err_dt.ErrCode) {
	body, errCode := tx.Try(svc, 0, msg_dt.CdTryCommonQueryByIds, &msg_dt.ReqTryCommonQueryByIds{
		Model:    model,
		Fields:   fields,
		ModelIds: ids,
	})
	if errCode != 0 {
		return
	}

	res := body.(*msg_dt.ResTryCommonQueryByIds)
	// map[int64]map[string]interface转为map[int64]modelStruct
	data = map[int64]interface{}{}
	for k, v := range res.Data {
		modelStruct := model_e.NewModelPtr(model)
		err := mapstructure.Decode(v, modelStruct)
		if err != nil {
			app.Log().TError(tx.Tid(), utils.NewError(err.Error(), nil))
			return nil, math.MaxUint32
		}
		data[k] = modelStruct
	}
	return
}

func (tx *Tx) TryUpdateById(svc svc.Svc, model model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (data interface{}, errCode err_dt.ErrCode) {
	body, errCode := tx.Try(svc, id, msg_dt.CdTryCommonUpdateById, msg_dt.ReqTryCommonUpdateById{
		Model:   model,
		UpData:  upData,
		ModelId: id,
	})
	if errCode != 0 {
		return
	}

	if body != nil {
		res := body.(*msg_dt.ResTryCommonUpdateById)
		if res != nil {
			data = res.Data
		}
	}
	return
}
func (tx *Tx) TryUpdateCharacterInfoByCid(upData map[model_field_e.Enum]interface{}, cid int64) (data interface{}, errCode err_dt.ErrCode) {
	return tx.TryUpdateById(svc.CharacterInfo, model_e.CharacterInfo, upData, cid)
}

func (tx *Tx) TryUpdateNumById(svc svc.Svc, model model_e.Model, incrData map[model_field_e.Enum]int32, id int64) (origData interface{}, data interface{}, errCode err_dt.ErrCode) {
	body, errCode := tx.Try(svc, id, msg_dt.CdTryCommonUpdateNumById, msg_dt.ReqTryCommonUpdateNumById{
		Model:    model,
		IncrData: incrData,
		ModelId:  id,
	})
	if errCode != 0 {
		return
	}

	if body != nil {
		res := body.(*msg_dt.ResTryCommonUpdateNumById)
		if res != nil {
			origData = model_e.NewModelPtr(model)
			err := mapstructure.Decode(res.OrigData, origData)
			if err != nil {
				app.Log().TError(tx.Tid(), utils.NewError(err.Error(), nil))
				return nil, nil, math.MaxUint32
			}
			data = res.Data
		}
	}
	return
}
func (tx *Tx) TryUpdateCharacterInfoNumByCid(incrData map[model_field_e.Enum]int32, cid int64) (origData interface{}, data interface{}, errCode err_dt.ErrCode) {
	return tx.TryUpdateNumById(svc.CharacterInfo, model_e.CharacterInfo, incrData, cid)
}

func (tx *Tx) TryCharacterInfoQueryByCids(fields []character_info_e.Enum, cids ...int64) (data map[int64]*model.CharacterInfo, errCode err_dt.ErrCode) {
	body, errCode := tx.Try(svc.CharacterInfo, 0, msg_dt.CdTryCommonQueryByIds, &msg_dt.ReqTryCommonQueryByIds{
		Model:    model_e.CharacterInfo,
		Fields:   fields,
		ModelIds: cids,
	})
	if errCode != 0 {
		return
	}

	res := body.(*msg_dt.ResTryCommonQueryByIds)
	data = map[int64]*model.CharacterInfo{}
	for k, v := range res.Data {
		modelStruct := &model.CharacterInfo{}
		err := mapstructure.Decode(v, modelStruct)
		if err != nil {
			app.Log().TError(tx.Tid(), utils.NewError(err.Error(), nil))
			return nil, math.MaxUint32
		}
		data[k] = modelStruct
	}
	return
}
