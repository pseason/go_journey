package common

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"github.com/go-redis/redis/v8"
	consulApi "github.com/hashicorp/consul/api"
	esConfig "github.com/olivere/elastic/config"
	"hm/pkg/misc"
	"hm/pkg/misc/design"
)

var (
	_Conf = &Config{}
)

func LoadConf(root string, paths ...string) (conf *Config, err error) {
	err = utils.StartLocalConf(root, _Conf, paths...)
	return _Conf, err
}

type Config struct {
	Debug       bool
	Discovery   intfc.DiscoveryConf
	Consul      *consulApi.Config
	Game        Game
	Http        misc.Http
	Redis       *redis.Options
	GlobalRedis *redis.Options
	// 策划配置
	Design design.Design
	// AI配置
	Ai AiConfig
	// ElasticSearch配置
	Es *esConfig.Config
}

type Game struct {
	Gate Gate
}

type Gate struct {
	Ip       string
	Port     string
	Cap      int
	CacheDur int64
}

type AiConfig struct {
	Debug    bool
	TickTime int
	ActorCap int
	BtPath   string
	Devtool  AiDevtoolConfig
}

type AiDevtoolConfig struct {
	Port         int
	OpenFileTail bool
	LogDir       string
	OpenEsTail   bool
}
