package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	spc_common "hm/pkg/services/space/common"
	"hm/pkg/services/space/msg_spc"
)

type ISceneProxy interface {
	// SpawnScene 生成场景
	SpawnScene(pTid int64, sceneType misc.TScene, sceneId int64, resOk utils.ActionInt64Any, resErr utils.ActionError)
	// RecycleScene 回收场景
	RecycleScene(tid int64, sceneType misc.TScene, sceneId int64, resOk utils.ActionInt64Any, resErr utils.ActionError)
	// RequestScene 请求场景
	RequestScene(service uint16, tid int64, sceneType misc.TScene, sceneId int64, code uint32, body interface{}, resOk utils.ActionInt64Any, resErr utils.ActionError)
	// NoticePlayerEnterSpace 通知玩家进入场景
	NoticePlayerEnterSpace(cid int64, sceneType misc.TScene, sceneId int64, x, y, z float32) utils.TErrCode
	// RequestActorEnterScene Actor请求进入场景,并更新该类型数量
	RequestActorEnterScene(tid int64, actorId int64, sceneType misc.TScene, sceneId int64, resOk utils.ActionInt64Any, resErr utils.ActionError)
	// RequestActorExitScene 请求Actor退出当前场景,并更新该类型数量
	RequestActorExitScene(tid int64, actorId int64, resOk utils.ActionInt64Any, resErr utils.ActionError)
	// RequestActor 请求Actor
	RequestActor(tid, actorId int64, service uint16, code uint32, body interface{}, resOk utils.ActionInt64Any, resErr utils.ActionError)
}

type sceneProxy struct {
	ctrl *baseController
}

func newSpaceProxy(ctrl *baseController) *sceneProxy {
	return &sceneProxy{ctrl: ctrl}
}

func (s *sceneProxy) SpawnScene(pTid int64, sceneType misc.TScene, sceneId int64,
	resOk utils.ActionInt64Any, resErr utils.ActionError) {
	nodeType := spc_common.GetNodeWithSceneType(sceneType, sceneId)
	app.Discovery().RequestNode(nodeType, 0, svc.Space, pTid, 0, msg_spc.CdSpawnScene, &msg_spc.ReqSpawnScene{
		SceneType: sceneType,
		SceneId:   sceneId,
	}, resOk, func(tid int64, ec utils.TErrCode) {
		resErr(utils.NewError(SceneSpawnFailed, utils.M{
			"scene type": sceneType,
			"scene id":   sceneId,
			"error code": ec,
		}))
	})
}

func (s *sceneProxy) RecycleScene(tid int64, sceneType misc.TScene, sceneId int64, resOk utils.ActionInt64Any, resErr utils.ActionError) {
	nodeType, nodeId, err := spc_common.GetRedisSceneData(sceneType, sceneId)
	if err != nil {
		resErr(err)
		return
	}
	app.Discovery().RequestNode(nodeType, nodeId, svc.Space, tid, sceneId, msg_spc.CdRecycleScene, &msg_spc.ReqRecycleScene{
		SceneType: sceneType,
		SceneId:   sceneId,
	}, resOk, func(tid int64, ec utils.TErrCode) {
		resErr(utils.NewError(SceneRecycleFailed, utils.M{
			"scene type": sceneType,
			"scene id":   sceneId,
			"error code": ec,
		}))
	})
}

func (s *sceneProxy) RequestScene(service uint16, tid int64, sceneType misc.TScene, sceneId int64, code uint32, body interface{}, resOk utils.ActionInt64Any, resErr utils.ActionError) {
	nodeType, nodeId, err := spc_common.GetRedisSceneData(sceneType, sceneId)
	if err != nil {
		resErr(err)
		return
	}
	app.Discovery().RequestNode(nodeType, nodeId, service, tid, sceneId, code, body, resOk, func(tid int64, ec utils.TErrCode) {
		resErr(utils.NewError(SceneRequestFailed, utils.M{
			"scene type": sceneType,
			"scene id":   sceneId,
			"error code": ec,
		}))
	})
}

func (s *sceneProxy) NoticePlayerEnterSpace(cid int64, sceneType misc.TScene, sceneId int64, x, y, z float32) utils.TErrCode {
	return s.ctrl.SendToCharacter(cid, msg_gm.CdNoticeSpaceEnterScene, &msg_gm.NoticeSpaceEnterScene{
		SceneType: int32(sceneType),
		SceneId:   sceneId,
		PositionX: x,
		PositionY: y,
		PositionZ: z,
	})
}

func (s *sceneProxy) RequestActorEnterSpace(tid int64, nodeType misc.TNode, nodeId uint16, actorId int64, resOk utils.ActionInt64Any, resErr utils.ActionError) {
	app.Discovery().RequestNode(nodeType, nodeId, svc.Space, tid, actorId, msg_spc.CdActorEnterSpace, &msg_spc.ReqActorEnterSpace{},
		resOk, func(tid int64, ec utils.TErrCode) {
			resErr(utils.NewError(ActorEnterSpaceFailed, utils.M{
				"node type":  nodeType,
				"node id":    nodeId,
				"actor id":   actorId,
				"error code": ec,
			}))
		})
}

func (s *sceneProxy) RequestActorExitSpace(tid int64, nodeType misc.TNode, nodeId uint16, actorId int64, resOk utils.ActionInt64Any, resErr utils.ActionError) {
	app.Discovery().RequestNode(nodeType, nodeId, svc.Space, tid, actorId, msg_spc.CdActorExitSpace, &msg_spc.ReqActorExitSpace{},
		resOk, func(tid int64, ec utils.TErrCode) {
			resErr(utils.NewError(ActorExitSpaceFailed, utils.M{
				"node type":  nodeType,
				"node id":    nodeId,
				"actor id":   actorId,
				"error code": ec,
			}))
		})
}

func (s *sceneProxy) RequestActorEnterScene(tid int64, actorId int64, sceneType misc.TScene, sceneId int64, resOk utils.ActionInt64Any, resErr utils.ActionError) {
	nodeType, nodeId, err := spc_common.GetRedisSceneData(sceneType, sceneId)
	if err != nil {
		resErr(err)
		return
	}
	err = spc_common.SetRedisActorSceneData(actorId, sceneType, sceneId)
	if err != nil {
		resErr(err)
		return
	}
	//todo 如果已经进入另一个节点空间,则退出上一个节点再进入当前节点
	app.Discovery().RequestNode(nodeType, nodeId, svc.Space, tid, actorId, msg_spc.CdActorEnterScene, &msg_spc.ReqActorEnterScene{
		SceneType: sceneType,
		SceneId:   sceneId,
	}, resOk, func(tid int64, ec utils.TErrCode) {
		resErr(utils.NewError(ActorEnterSceneFailed, utils.M{
			"node type":  nodeType,
			"node id":    nodeId,
			"scene type": sceneType,
			"scene id":   sceneId,
			"actor id":   actorId,
			"error code": ec,
		}))
	})
}

func (s *sceneProxy) RequestActorExitScene(tid int64, actorId int64, resOk utils.ActionInt64Any, resErr utils.ActionError) {
	currSceneType, currSceneId, err := spc_common.GetRedisActorSceneData(actorId)
	if err != nil {
		resErr(err)
		return
	}
	currNodeType, currNodeId, err := spc_common.GetRedisSceneData(currSceneType, currSceneId)
	if err != nil {
		resErr(err)
		return
	}
	app.Discovery().RequestNode(currNodeType, currNodeId, svc.Space, tid, actorId, msg_spc.CdActorExitScene,
		&msg_spc.ReqActorExitScene{}, resOk, func(tid int64, ec utils.TErrCode) {
			resErr(utils.NewError(ActorExitSceneFailed, utils.M{
				"actor id":           actorId,
				"current scene type": currSceneType,
				"current scene id":   currSceneId,
				"error code":         ec,
			}))
		})
}

// RequestActor 请求Actor
func (s *sceneProxy) RequestActor(tid, actorId int64, service uint16, code uint32, body interface{}, resOk utils.ActionInt64Any, resErr utils.ActionError) {
	sceneType, sceneId, err := spc_common.GetRedisActorSceneData(actorId)
	if err != nil {
		app.Log().TError(tid, err)
		return
	}
	nodeType, nodeId, err := spc_common.GetRedisSceneData(sceneType, sceneId)
	if err != nil {
		app.Log().TError(tid, err)
		return
	}
	app.Discovery().RequestNode(nodeType, nodeId, service, tid, actorId, code, body, resOk, func(tid int64, ec utils.TErrCode) {
		resErr(utils.NewError(ActorRequestFailed, utils.M{
			"actor id":   actorId,
			"scene type": sceneType,
			"scene id":   sceneId,
			"service":    service,
			"code":       code,
			"error code": ec,
		}))
	})
}
