package common

import (
	"github.com/go-redis/redis/v8"
	"hm/pkg/misc"
)

/*
@Time   : 2021-11-10 2:48
@Author : wushu
@DESC   :
*/
var (
	_Redis *redis.Client
)

func LoadRedis(config *redis.Options) *redis.Client {
	_Redis = misc.GetRedisConn(config)
	return _Redis
}

func Redis() *redis.Client {
	return _Redis
}
