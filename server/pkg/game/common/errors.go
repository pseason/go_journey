package common

const (
	ActorEnterSpaceFailed = "actor enter space failed"
	ActorExitSpaceFailed  = "actor exit space failed"
	ActorEnterSceneFailed = "actor enter scene failed"
	ActorExitSceneFailed  = "actor exit scene failed"
	ActorRequestFailed    = "actor request failed"
	SceneSpawnFailed      = "scene spawn failed"
	SceneRecycleFailed    = "scene recycle failed"
	SceneRequestFailed    = "scene request failed"
)
