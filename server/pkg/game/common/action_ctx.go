package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/game/msg_gm/err_gm"
)

// 逻辑处理上下文
type IActionCtx interface {
	// 用户请求ip
	Addr() string
	// traceId
	Tid() int64
	// 角色id
	Cid() int64
	// 响应成功消息
	ResOk(resBody interface{})
	// 响应错误
	ResErr(errCode err_gm.ErrCode)
	// 设置Cid
	SetCid(cid int64)
	// 创建子ac
	NewSubActionCtx(note string) IActionCtx
}

type AcType uint8

const (
	AcType_Req AcType = iota // 请求上下文
	Acype_Eve                // 事件上下文
)

// 逻辑处理上下文
type actionCtx struct {
	addr string
	tid  int64
	cid  int64
	typ  AcType // 上下文类型
}

func NewActionCtx(addr string, tid int64, cid int64, typ AcType) IActionCtx {
	return &actionCtx{addr: addr, tid: tid, cid: cid, typ: typ}
}

func (ac *actionCtx) NewSubActionCtx(note string) IActionCtx {
	sTid := app.Log().TSign(ac.Tid(), utils.M{"note": note})
	return &actionCtx{addr: ac.addr, tid: sTid, cid: ac.cid, typ: ac.typ}
}

func (ac *actionCtx) Addr() string {
	return ac.addr
}

func (ac *actionCtx) Tid() int64 {
	return ac.tid
}

func (ac *actionCtx) Cid() int64 {
	return ac.cid
}

func (ac *actionCtx) ResOk(resBody interface{}) {
	app.UserLogic().ResOk(ac.tid, resBody)
}

func (ac *actionCtx) ResErr(errCode err_gm.ErrCode) {
	app.UserLogic().ResErr(ac.tid, errCode)
}

// 设置Cid，todo 仅用于测试
func (ac *actionCtx) SetCid(cid int64) {
	ac.cid = cid
}
