package common

import (
	"95eh.com/eg/app"
	common_eg "95eh.com/eg/common"
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/user"
	"95eh.com/eg/utils"
	"github.com/gin-gonic/gin"
	"hm/pkg/game/msg_gm"
)

/*
@Time   : 2021-11-15 16:39
@Author : wushu
@DESC   :
*/

func ReceiveGateRequest(agent intfc.IAgent, data []byte) utils.TErrCode {
	if len(data) < 4 {
		return common_eg.EcBadPacket
	}
	code, pkt, err := unpackUserPacket(data)
	if err != nil {
		return common_eg.EcUnpackFailed
	}
	addr := agent.Addr()
	uid, ok := app.UserLogic().GetUserUid(addr)
	if ok {
		ok := app.UserLogic().TestUserAuth(uid, code, addr)
		if !ok {
			app.Log().Error("no auth", utils.M{
				"addr": addr,
				"code": code,
			})
			return common_eg.EcNoAuth
		}
	} else {
		ok := app.UserLogic().TestMask(0, code)
		if !ok {
			app.Log().Error("no auth", utils.M{
				"addr": addr,
				"code": code,
			})
			return common_eg.EcNoAuth
		}
	}
	body, err := app.UserGate().Codec().UnmarshalReq(code, pkt)
	if err != nil {
		app.Log().Error("decode request failed", utils.M{
			"code":  code,
			"error": err.Error(),
		})
		return common_eg.EcDecodeFailed
	}
	tid := app.Log().TSign(0, utils.M{
		"tracer": "game request",
		"code":   code,
		"body":   body,
	})
	cid := getCidByUid(uid)
	app.UserGate().ProcessRequest(addr, tid, cid, code, body, func(data []byte) {
		app.UserGate().Send(addr, packUserPacket(code, data))
	}, func(ec utils.TErrCode) {
		app.UserGate().Send(addr, packUserPacket(msg_gm.CdNoticeSysError, packErr(code, ec)))
	})
	return 0
}

func ReceiveHttpRequest(c *gin.Context, tid, uid int64, code uint32, body interface{}) {
	cid := getCidByUid(uid)
	user.ReceiveHttpRequest(c, tid, cid, code, body)
}

func getUidByCid(cid int64) int64 {
	uid, ok := app.UserLogic().GetUidWithAlias(cid)
	if !ok {
		return cid
	}
	return uid
}

// 如果没有cid，则返回uid
func getCidByUid(uid int64) int64 {
	alias, ok := app.UserLogic().GetAliasWithUid(uid)
	if !ok {
		return uid
	}
	return alias
}

func packErr(code uint32, ec utils.TErrCode) []byte {
	bytes, err := app.UserGate().Codec().Marshal(&msg_gm.NoticeSysError{
		MsgCode: code,
		ErrCode: uint32(ec),
	})
	if err != nil {
		app.Log().Error("encode response error failed", utils.M{
			"error": err.Error(),
		})
	}
	return bytes
}

func sendToCharacter(cid int64, code uint32, body interface{}) utils.TErrCode {
	bytes, err := app.UserGate().Codec().Marshal(body)
	if err != nil {
		return common_eg.EcEncodeFailed
	}
	app.UserLogic().SendByAlias(cid, packUserPacket(code, bytes))
	return 0
}

func sendToCharacters(cids []int64, code uint32, body interface{}) utils.TErrCode {
	bytes, err := app.UserGate().Codec().Marshal(body)
	if err != nil {
		return common_eg.EcEncodeFailed
	}
	app.UserLogic().SendByAliases(cids, packUserPacket(code, bytes))
	return 0
}

func sendToUser(uid int64, code uint32, body interface{}) utils.TErrCode {
	bytes, err := app.UserGate().Codec().Marshal(body)
	if err != nil {
		return common_eg.EcEncodeFailed
	}
	app.UserLogic().SendByUid(uid, packUserPacket(code, bytes))
	return 0
}

func sendToUsers(uids []int64, code uint32, body interface{}) utils.TErrCode {
	bytes, err := app.UserGate().Codec().Marshal(body)
	if err != nil {
		return common_eg.EcEncodeFailed
	}
	app.UserLogic().SendByUids(uids, packUserPacket(code, bytes))
	return 0
}

func sendToAllUsers(code uint32, body interface{}) utils.TErrCode {
	bytes, err := app.UserGate().Codec().Marshal(body)
	if err != nil {
		return common_eg.EcEncodeFailed
	}
	app.UserGate().SendAll(packUserPacket(code, bytes))
	return 0
}

func packUserPacket(code uint32, pkt []byte) []byte {
	buffer := data.NewBytesBufferWithLen(uint32(4 + len(pkt)))
	buffer.WUint32(code)
	buffer.Write(pkt)
	return buffer.WAll()
}

func unpackUserPacket(bytes []byte) (code uint32, pkt []byte, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	code, err = buffer.RUint32()
	if err != nil {
		return
	}
	pkt = buffer.RAll()
	return
}
