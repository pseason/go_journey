package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"context"
	"github.com/go-redis/redis/v8"
	"github.com/mitchellh/mapstructure"
	"hm/pkg/game/msg_gm"
	"hm/pkg/game/svc"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
	"math"
)

type Action struct {
	MsgCode        msg_gm.MsgCode                           // 消息码
	PermissionCode int64                                    // 权限码
	Action         func(ac IActionCtx, reqBody interface{}) // 处理函数
}

type Event struct {
	MsgCode msg_dt.MsgCode                           // 消息码
	Action  func(ac IActionCtx, eveBody interface{}) // 处理函数
}

type IBaseController interface {
	// Actions 消息处理器
	Actions() []Action
	// Events 事件处理器
	Events() []Event
	// Redis
	Redis() *redis.Client
	// Conf
	Conf() *Config
	// Context 本controller的上下文 (可以用作操作redis接口时需要的context参数)
	Context() context.Context
	// Space 请求空间服务
	Space() ISceneProxy
	// AfterInit 初始化完成后的回调
	AfterInit() error

	// RequestSvc
	//  @Description: 同步请求服务
	//  @param svc 请求的服务
	//  @param tid 链路id
	//  @param subjectId 请求的服务的主体id，一般是主体表的主键id，没有则传0；例如：请求 svc.CharacterInfo、svc.Backpack，则id为角色id；请求 svc.City、svc.CityMarket，则为城邦id
	//  @param code 请求的消息码
	//  @param reqBody 请求消息体
	//  @return nodeId 处理此请求的节点的id；可以用于后续的 SyncRequestSvcNode#nodeId todo 该返回值应当删除。函数名更换为RequestDataSvc
	//  @return resBody 响应包体，可以转为各个服务中的消息结构体
	//  @return errCode 响应的错误码
	RequestSvc(svc svc.Svc, tid, subjectId int64, code msg_dt.MsgCode, reqBody interface{}) (nodeId uint16, resBody interface{}, errCode err_dt.ErrCode)

	// AsyncRequestSvc
	//  @Description: 异步请求服务
	//  @param svc 请求的服务
	//  @param tid 链路id
	//  @param subjectId 请求的服务的主体id，一般是主体表的主键id，没有则传0；例如：请求 svc.CharacterInfo、svc.Backpack，则id为角色id；请求 svc.City、svc.CityMarket，则为城邦id
	//  @param code 请求的消息码
	//  @param reqBody 请求消息体
	//  @param resOk 响应成功的回调函数。
	// 			@nodeId 等于接口return的nodeId
	//			@sTid 此请求内部产生的subTid，也是请求到达的下个节点所使用的tid
	//			@resBody 响应包体，可以转为各个服务中的消息结构体
	//  @param resErr 响应错误的回调函数
	// 			@nodeId 等于接口return的nodeId
	//			@sTid:此请求内部产生的subTid，也是请求到达的下个节点所使用的tid
	//			@errCode 响应的错误码
	//  @return nodeId 处理此请求的节点的id。可以用于后续的 RequestSvcNode#nodeId todo 该返回值应当删除。函数名更换为AsyncRequestDataSvc
	// Deprecated: 请使用 RequestSvc
	AsyncRequestSvc(svc svc.Svc, tid, subjectId int64, code msg_dt.MsgCode, reqBody interface{}, resOk func(nodeId uint16, sTid int64, resBody interface{}), resErr func(nodeId uint16, sTid int64, errCode err_dt.ErrCode)) (nodeId uint16)

	//// RequestSvcNode 同步请求指定节点id的服务
	//RequestSvcNode(svc svc.Svc, tid, subjectId int64, nodeId uint16, code msg_dt.MsgCode, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode)
	//
	//// AsyncRequestSvcNode 异步请求指定节点id的服务
	//// Deprecated: 请使用 RequestSvcNode
	//AsyncRequestSvcNode(svc svc.Svc, tid, subjectId int64, nodeId uint16, code msg_dt.MsgCode, reqBody interface{}, resOk func(sTid int64, resBody interface{}), resErr func(sTid int64, errCode err_dt.ErrCode))

	// 分布式事务接口
	Transaction(tid int64, action func(tx ITx) (ok bool))

	// SendToCharacter 主动推送
	SendToCharacter(cid int64, code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode)
	SendToCharacters(cids []int64, code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode)
	SendToUser(uid int64, code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode)
	SendToUsers(uids []int64, code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode)
	SendToAllUsers(code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode)

	// GetCidByUid 获取cid或uid
	GetCidByUid(uid int64) int64
	GetUidByCid(cid int64) int64

	// -----------↓↓↓---数据服务通用接口---↓↓↓--------------

	// RequestSvcQueryById
	//  @Description: 根据id查询
	//	@param subjectId 请求的服务的主体id
	//  @param model 需要查询的model枚举
	//  @param fields 需要查询的model字段枚举。包名为`model的蛇形命名_e`，如 character_info_e.Nickname
	//  @return data 查询到的数据model
	RequestSvcQueryById(svc svc.Svc, tid int64, model model_e.Model, fields []model_field_e.Enum, id int64) (data interface{}, errCode err_dt.ErrCode)
	// 根据id批量查询
	RequestSvcQueryByIds(svc svc.Svc, tid int64, model model_e.Model, fields []model_field_e.Enum, ids ...int64) (data map[int64]interface{}, errCode err_dt.ErrCode)

	// --↓使用频繁的通用接口，对应模块单独封装。相比通用接口仅直接返回对应的结构体

	// 根据cid查询角色信息
	RequestCharacterInfoQueryByCid(tid int64, fields []character_info_e.Enum, cid int64) (characterInfo *model.CharacterInfo, errCode err_dt.ErrCode)
	// 根据cid批量查询角色信息
	RequestCharacterInfoQueryByCids(tid int64, fields []character_info_e.Enum, cids ...int64) (data map[int64]*model.CharacterInfo, errCode err_dt.ErrCode)

	// --↑使用频繁的通用接口

	// -----------↑↑↑---数据服务通用接口---↑↑↑--------------

}

func NewBaseController() IBaseController {
	ctrl := &baseController{context: context.Background()}
	ctrl.space = newSpaceProxy(ctrl)
	return ctrl
}

type baseController struct {
	context context.Context
	space   ISceneProxy
}

func (bc *baseController) Events() []Event {
	return nil
}

func (bc *baseController) Redis() *redis.Client {
	return _Redis
}

func (bc *baseController) Conf() *Config {
	return _Conf
}

func (bc *baseController) Context() context.Context {
	return bc.context
}

func (bc *baseController) Space() ISceneProxy {
	return bc.space
}

func (bc *baseController) AfterInit() error {
	return nil
}

// 请求前置处理
func requestPreHandler(service svc.Svc, code msg_dt.MsgCode) (iError utils.IError) {
	sv := uint16(code / 1e4)
	if sv != svc.Common_Data && sv != service {
		return utils.NewError("the request code is inconsistent with the service", utils.M{"code": code, "service": service})
	}
	return
}

func (bc *baseController) AsyncRequestSvc(service svc.Svc, tid, subjectId int64, code msg_dt.MsgCode, reqBody interface{},
	resOk func(nodeId uint16, sTid int64, resBody interface{}), resErr func(nodeId uint16, sTid int64, errCode err_dt.ErrCode)) (nodeId uint16) {
	iError := requestPreHandler(service, code)
	if iError != nil {
		app.Log().TError(tid, iError)
		return
	}
	return app.Discovery().Request(service, tid, subjectId, code, reqBody, resOk, resErr)
}

func (bc *baseController) RequestSvc(service svc.Svc, tid, subjectId int64, code msg_dt.MsgCode, reqBody interface{}) (nodeId uint16, resBody interface{}, errCode err_dt.ErrCode) {
	iError := requestPreHandler(service, code)
	if iError != nil {
		app.Log().TError(tid, iError)
		return
	}
	return app.Discovery().SyncRequest(service, tid, subjectId, code, reqBody)
}

//func (bc *baseController) AsyncRequestSvcNode(service svc.Svc, tid, subjectId int64, nodeId uint16, code msg_dt.MsgCode, reqBody interface{},
//	resOk func(sTid int64, resBody interface{}), resErr func(sTid int64, errCode err_dt.ErrCode)) {
//	iError := requestPreHandler(service, code)
//	if iError != nil {
//		app.Log().TError(tid, iError)
//		return
//	}
//	app.Discovery().RequestNode(node, service, nodeId, tid, subjectId, code, reqBody, resOk, resErr)
//}
//
//func (bc *baseController) RequestSvcNode(service svc.Svc, tid, subjectId int64, nodeId uint16, code msg_dt.MsgCode, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
//	iError := requestPreHandler(service, code)
//	if iError != nil {
//		app.Log().TError(tid, iError)
//		return
//	}
//	return app.Discovery().SyncRequestNode(node, service, nodeId, tid, subjectId, code, reqBody)
//}

func (bc *baseController) Transaction(tid int64, action func(tx ITx) (ok bool)) {
	app.TxProducer().Tx(tid, func(tx intfc.ITx) bool {
		return action(tx.(ITx))
	})
}

func (bc *baseController) SendToCharacter(cid int64, code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode) {
	return sendToCharacter(cid, code, body)
}

func (bc *baseController) SendToCharacters(cids []int64, code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode) {
	return sendToCharacters(cids, code, body)
}

func (bc *baseController) SendToUser(uid int64, code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode) {
	return sendToUser(uid, code, body)
}

func (bc *baseController) SendToUsers(uids []int64, code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode) {
	return sendToUsers(uids, code, body)
}

func (bc *baseController) SendToAllUsers(code msg_gm.MsgCode, body interface{}) (errCode utils.TErrCode) {
	return sendToAllUsers(code, body)
}

func (bc *baseController) GetCidByUid(uid int64) int64 {
	return getCidByUid(uid)
}

func (bc *baseController) GetUidByCid(cid int64) int64 {
	return getUidByCid(cid)
}

func (bc *baseController) Actions() []Action {
	panic("implement me")
}

func (bc *baseController) RequestSvcQueryById(svc svc.Svc, tid int64, model model_e.Model, fields []model_field_e.Enum, id int64) (data interface{}, errCode err_dt.ErrCode) {
	_, body, errCode := bc.RequestSvc(svc, tid, id, msg_dt.CdCommonQueryById, &msg_dt.ReqCommonQueryById{
		Model:   model,
		Fields:  fields,
		ModelId: id,
	})
	if errCode != 0 {
		return
	}
	res := body.(*msg_dt.ResCommonQueryById)
	data = model_e.NewModelPtr(model)
	err := mapstructure.Decode(res.Data, data)
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), nil))
		return nil, math.MaxUint32
	}
	return
}
func (bc *baseController) RequestSvcQueryByIds(svc svc.Svc, tid int64, model model_e.Model, fields []model_field_e.Enum, ids ...int64) (data map[int64]interface{}, errCode err_dt.ErrCode) {
	_, body, errCode := bc.RequestSvc(svc, tid, 0, msg_dt.CdCommonQueryByIds, &msg_dt.ReqCommonQueryByIds{
		Model:    model,
		Fields:   fields,
		ModelIds: ids,
	})
	if errCode != 0 {
		return
	}
	res := body.(*msg_dt.ResCommonQueryByIds)

	// map[int64]map[string]interface转为map[int64]modelStruct
	data = map[int64]interface{}{}
	for k, v := range res.Data {
		modelStruct := model_e.NewModelPtr(model)
		err := mapstructure.Decode(v, modelStruct)
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
			return nil, math.MaxUint32
		}
		data[k] = modelStruct
	}
	return
}

// 角色信息专门的数据接口，和通用接口相比直接返回转换后的结构体
func (bc *baseController) RequestCharacterInfoQueryByCid(tid int64, fields []character_info_e.Enum, cid int64) (characterInfo *model.CharacterInfo, errCode err_dt.ErrCode) {
	_, body, errCode := bc.RequestSvc(svc.CharacterInfo, tid, cid, msg_dt.CdCommonQueryById, &msg_dt.ReqCommonQueryById{
		Model:   model_e.CharacterInfo,
		Fields:  fields,
		ModelId: cid,
	})
	if errCode != 0 {
		return
	}
	res := body.(*msg_dt.ResCommonQueryById)
	characterInfo = &model.CharacterInfo{}
	err := mapstructure.Decode(res.Data, characterInfo)
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), nil))
		return nil, math.MaxUint32
	}
	return
}

func (bc *baseController) RequestCharacterInfoQueryByCids(tid int64, fields []character_info_e.Enum, cids ...int64) (data map[int64]*model.CharacterInfo, errCode err_dt.ErrCode) {
	_, body, errCode := bc.RequestSvc(svc.CharacterInfo, tid, 0, msg_dt.CdCommonQueryByIds, &msg_dt.ReqCommonQueryByIds{
		Model:    model_e.CharacterInfo,
		Fields:   fields,
		ModelIds: cids,
	})
	if errCode != 0 {
		return
	}
	res := body.(*msg_dt.ResCommonQueryByIds)

	data = map[int64]*model.CharacterInfo{}
	for k, v := range res.Data {
		modelStruct := &model.CharacterInfo{}
		err := mapstructure.Decode(v, modelStruct)
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
			return nil, math.MaxUint32
		}
		data[k] = modelStruct
	}
	return
}
