package event

type UserOffLine struct {
	Uid  int64
	Addr string
}
