package misc

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"regexp"
	"strconv"
)

/*
@Time   : 2021-11-10 2:52
@Author : wushu
@DESC   :
*/
var (
	_NeedRedisVersion int64 = 5
)

func GetRedisConn(conf *redis.Options) *redis.Client {
	client := redis.NewClient(conf)
	// 版本检查
	info := client.Info(context.Background(), "server")
	if info.Err() != nil {
		panic(fmt.Sprintf("redis connection\nerror: %v", info.Err()))
	}
	reg := regexp.MustCompile(`version:(\d).*[^\s]`)
	version := reg.FindStringSubmatch(info.String())
	v, _ := strconv.ParseInt(version[1], 10, 64)
	if v < _NeedRedisVersion {
		panic(fmt.Sprintf("当前Redis版本%s；请将版本升至%d以上，以使用Redis Streams\n", version[0], _NeedRedisVersion))
	}
	return client
}
