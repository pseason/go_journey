package cache_key

import "fmt"

/*
@Time   : 2020-12-26 17:14
@Author : wushu
@DESC   : redis key枚举
*/

type CacheKey string

func (cacheKey CacheKey) Format(args ...interface{}) string {
	sprintf := fmt.Sprintf(string(cacheKey), args...)
	return sprintf
}

const (
	// 角色
	CharacterInfo       CacheKey = "character:info:%d"    // 角色基础信息
	CharacterOnlineList CacheKey = "character:onlineList" // 记录角色id和本次上线时间

	// 背包
	BackPackCache     CacheKey = "backpack:%d:%d"     //类别:玩家ID
	ShortcutBarFreeze CacheKey = "shortcut:bar:%d:%d" // 玩家ID:冻结类型

	// 任务
	TaskCache           CacheKey = "task:%d:%d:%d" //cid|type|aftType|status|templateId/taskId
	TaskCacheAllPattern CacheKey = "task:%d:*"
	TaskRefresh         CacheKey = "taskRefresh:%d" //cid
	TaskRefreshAll      CacheKey = "taskRefresh:*"  //*
	TaskEvent           CacheKey = "taskEvent:%d"   //cid
)
