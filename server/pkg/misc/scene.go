package misc

import "95eh.com/eg/intfc"

type TScene = intfc.TScene

// 场景类型
const (
	SceneWorld TScene = iota + 1 //大世界
	SceneNovice //新手村
	SceneBoss //Boss
	SceneCity = 14
)
