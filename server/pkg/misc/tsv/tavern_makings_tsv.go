package tsv

type TavernMakingsTsv struct {
	//材料ID
	Makings int32

	//品质概率
	QualityRandom []int32

	//品质道具ID
	QualityID [][]int32
}

func (tsv *TavernMakingsTsv) SetValues(values []string) {
	i := 0
	tsv.Makings = ToInt(values[i])
	i++
	tsv.QualityRandom = ToIntArray(values[i])
	i++
	tsv.QualityID = ToIntArray2(values[i])
	i++
}

type TavernMakingsTsvManager struct {
	TsvSlice []*TavernMakingsTsv
	TsvMap   map[int32]*TavernMakingsTsv
}

func (manager *TavernMakingsTsvManager) SetValues(data ITsv) {
	tsv := data.(*TavernMakingsTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Makings] = tsv
}
func (manager *TavernMakingsTsvManager) ClearValues() {
	manager.TsvSlice = make([]*TavernMakingsTsv, 0)
	manager.TsvMap = make(map[int32]*TavernMakingsTsv, 0)
}
