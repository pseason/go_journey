package tsv

type WeatherTsv struct {
	//月份
	Id int32

	//天气随机概率第一个为id（WeatherRandom表），第二个为概率,概率值的合为100
	Random [][]int32
}

func (tsv *WeatherTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Random = ToIntArray2(values[i])
	i++
}

type WeatherTsvManager struct {
	TsvSlice []*WeatherTsv
	TsvMap   map[int32]*WeatherTsv
}

func (manager *WeatherTsvManager) SetValues(data ITsv) {
	tsv := data.(*WeatherTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *WeatherTsvManager) ClearValues() {
	manager.TsvSlice = make([]*WeatherTsv, 0)
	manager.TsvMap = make(map[int32]*WeatherTsv, 0)
}
