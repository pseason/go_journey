package tsv

type ForgeTsv struct {
	//可合成列表
	Id int32

	//合成概率
	Forgepr []int32

	//所需id
	Item [][]int32

	//所需建筑类型
	BuildType int32

	//建筑等级
	BuildValue int32

	//消耗精力
	Energy int32

	//消耗金钱
	Gold int32

	//消耗时间
	Time int32
}

func (tsv *ForgeTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Forgepr = ToIntArray(values[i])
	i++
	tsv.Item = ToIntArray2(values[i])
	i++
	tsv.BuildType = ToInt(values[i])
	i++
	tsv.BuildValue = ToInt(values[i])
	i++
	tsv.Energy = ToInt(values[i])
	i++
	tsv.Gold = ToInt(values[i])
	i++
	tsv.Time = ToInt(values[i])
	i++
}

type ForgeTsvManager struct {
	TsvSlice []*ForgeTsv
	TsvMap   map[int32]*ForgeTsv
}

func (manager *ForgeTsvManager) SetValues(data ITsv) {
	tsv := data.(*ForgeTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *ForgeTsvManager) ClearValues() {
	manager.TsvSlice = make([]*ForgeTsv, 0)
	manager.TsvMap = make(map[int32]*ForgeTsv, 0)
}
