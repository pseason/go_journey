package tsv

type MonsterBornPointTsv struct {
	//出生位置Id
	Id int32

	//地图类型
	Type int32

	//野怪类型
	MonsterType int32

	//野怪概率
	MonsterPercent int32

	//属性Id下限
	AttributeIdMin int32

	//属性Id上限
	AttributeIdMax int32

	//出生位置X坐标
	PosX float32

	//出生位置Y坐标
	PosY float32

	//出生位置高度值
	Height float32

	//对应场景
	Scene int32

	//奖励
	Rewards []int32
}

func (tsv *MonsterBornPointTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Type = ToInt(values[i])
	i++
	tsv.MonsterType = ToInt(values[i])
	i++
	tsv.MonsterPercent = ToInt(values[i])
	i++
	tsv.AttributeIdMin = ToInt(values[i])
	i++
	tsv.AttributeIdMax = ToInt(values[i])
	i++
	tsv.PosX = ToFloat(values[i])
	i++
	tsv.PosY = ToFloat(values[i])
	i++
	tsv.Height = ToFloat(values[i])
	i++
	tsv.Scene = ToInt(values[i])
	i++
	tsv.Rewards = ToIntArray(values[i])
	i++
}

type MonsterBornPointTsvManager struct {
	TsvSlice []*MonsterBornPointTsv
	TsvMap   map[int32]*MonsterBornPointTsv
}

func (manager *MonsterBornPointTsvManager) SetValues(data ITsv) {
	tsv := data.(*MonsterBornPointTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *MonsterBornPointTsvManager) ClearValues() {
	manager.TsvSlice = make([]*MonsterBornPointTsv, 0)
	manager.TsvMap = make(map[int32]*MonsterBornPointTsv, 0)
}
