package tsv

type JungleTsv struct {
	//索引
	Id int32

	//名称
	Name string

	//生命值（影响采集时间）
	Hp int32

	//固定产出道具(物品/最小/最大/总量)
	Props [][]int32

	//随机产出道具(物品/最小/最大/概率)
	PropsExtra [][]int32

	//每次采集消耗精力
	ConsumeEnergy int32

	//重生时间间隔(单位秒)
	RebirthDuration int32

	//天气时间随机产出道具(6种天气/日期组合表ID/物品/最小/最大/概率)
	PropsByWeather [][]int32
}

func (tsv *JungleTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Name = values[i]
	i++
	tsv.Hp = ToInt(values[i])
	i++
	tsv.Props = ToIntArray2(values[i])
	i++
	tsv.PropsExtra = ToIntArray2(values[i])
	i++
	tsv.ConsumeEnergy = ToInt(values[i])
	i++
	tsv.RebirthDuration = ToInt(values[i])
	i++
	tsv.PropsByWeather = ToIntArray2(values[i])
	i++
}

type JungleTsvManager struct {
	TsvSlice []*JungleTsv
	TsvMap   map[int32]*JungleTsv
}

func (manager *JungleTsvManager) SetValues(data ITsv) {
	tsv := data.(*JungleTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *JungleTsvManager) ClearValues() {
	manager.TsvSlice = make([]*JungleTsv, 0)
	manager.TsvMap = make(map[int32]*JungleTsv, 0)
}
