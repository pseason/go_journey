package tsv

type MallTsv struct {
	//索引
	Id int32

	//商品模板id
	PropId int32

	//商品包含数量
	ContainNum int32

	//商品类型
	MallPropType int32

	//商品限时时间(小时)
	LimitTime int32

	//商品价格
	Price int32

	//商品折扣
	Discount int32

	//商品折扣限时时间(小时)
	Discount_time int32

	//支付货币种类
	CurrencyType int32

	//商城类别
	MallType int32

	//是否下架
	IsRemove int32

	//限时商品开始时间
	StartTime string

	//折扣开始时间
	DiscountStartTime string

	//限购类型
	PurchaseType int32

	//限购数量
	PurchaseLimit int32

	//职位类型(王国商城对应职位，竞技场商城对应段位)
	Position int32
}

func (tsv *MallTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.PropId = ToInt(values[i])
	i++
	tsv.ContainNum = ToInt(values[i])
	i++
	tsv.MallPropType = ToInt(values[i])
	i++
	tsv.LimitTime = ToInt(values[i])
	i++
	tsv.Price = ToInt(values[i])
	i++
	tsv.Discount = ToInt(values[i])
	i++
	tsv.Discount_time = ToInt(values[i])
	i++
	tsv.CurrencyType = ToInt(values[i])
	i++
	tsv.MallType = ToInt(values[i])
	i++
	tsv.IsRemove = ToInt(values[i])
	i++
	tsv.StartTime = values[i]
	i++
	tsv.DiscountStartTime = values[i]
	i++
	tsv.PurchaseType = ToInt(values[i])
	i++
	tsv.PurchaseLimit = ToInt(values[i])
	i++
	tsv.Position = ToInt(values[i])
	i++
}

type MallTsvManager struct {
	TsvSlice []*MallTsv
	TsvMap   map[int32]*MallTsv
}

func (manager *MallTsvManager) SetValues(data ITsv) {
	tsv := data.(*MallTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *MallTsvManager) ClearValues() {
	manager.TsvSlice = make([]*MallTsv, 0)
	manager.TsvMap = make(map[int32]*MallTsv, 0)
}
