package tsv

import (
	"fmt"
	"hm/pkg/misc"
	"io/ioutil"
	"reflect"
	"strings"

	"95eh.com/eg/utils"
)

/** 使用Tips：
* 加载所有TSV请使用 LoadAllTsv
* 加载对应节点的TSV请使用 LoadNodeTsv
 */

type TsvEnum string

type relation struct {
	tsvStruct        ITsv
	tsvManagerStruct ITsvManager
}

// 定义tsv枚举，枚举值写生成的tsv文件名
const (
	Prop              TsvEnum = "Prop"
	RandomNickname    TsvEnum = "RandomNickname"
	PropGift          TsvEnum = "PropGift"
	Unlock            TsvEnum = "Unlock"
	SystemMail        TsvEnum = "SystemMail"
	CityBuilding      TsvEnum = "CityBuilding"
	CityInit          TsvEnum = "CityInit"
	BuildingAttribute TsvEnum = "BuildingAttribute"
	Monster           TsvEnum = "Monster"
	MonsterBornPoint  TsvEnum = "MonsterBornPoint"
	Tavern            TsvEnum = "Tavern"
	TavernMakings     TsvEnum = "TavernMakings"
	Crop              TsvEnum = "Crop"
	Farmland          TsvEnum = "Farmland"
	Task              TsvEnum = "Task"
	Plot              TsvEnum = "Plot"
	Forge             TsvEnum = "Forge"
	Market            TsvEnum = "Market"
	Decompose         TsvEnum = "Decompose"
	Mall              TsvEnum = "Mall"
	Weather           TsvEnum = "Weather"
	WeatherRandom     TsvEnum = "WeatherRandom"
	Collect            TsvEnum = "Collect"
	CollectProbability TsvEnum = "CollectProbability"
)

// 定义tsv与对应的struct的关系映射
var (
	_Relation = map[TsvEnum]relation{
		RandomNickname:    {tsvStruct: &RandomNicknameTsv{}, tsvManagerStruct: &RandomNicknameTsvManager{}},
		Prop:              {tsvStruct: &PropTsv{}, tsvManagerStruct: &PropTsvManager{}},
		PropGift:          {tsvStruct: &PropGiftTsv{}, tsvManagerStruct: &PropGiftTsvManager{}},
		SystemMail:        {tsvStruct: &SystemMailTsv{}, tsvManagerStruct: &SystemMailTsvManager{}},
		CityBuilding:      {tsvStruct: &CityBuildingTsv{}, tsvManagerStruct: &CityBuildingTsvManager{}},
		CityInit:          {tsvStruct: &CityInitTsv{}, tsvManagerStruct: &CityInitTsvManager{}},
		BuildingAttribute: {tsvStruct: &BuildingAttributeTsv{}, tsvManagerStruct: &BuildingAttributeTsvManager{}},
		Monster:           {tsvStruct: &MonsterTsv{}, tsvManagerStruct: &MonsterTsvManager{}},
		MonsterBornPoint:  {tsvStruct: &MonsterBornPointTsv{}, tsvManagerStruct: &MonsterBornPointTsvManager{}},
		Tavern:            {tsvStruct: &TavernTsv{}, tsvManagerStruct: &TavernTsvManager{}},
		TavernMakings:     {tsvStruct: &TavernMakingsTsv{}, tsvManagerStruct: &TavernMakingsTsvManager{}},
		Crop:              {tsvStruct: &CropTsv{}, tsvManagerStruct: &CropTsvManager{}},
		Farmland:          {tsvStruct: &FarmlandTsv{}, tsvManagerStruct: &FarmlandTsvManager{}},
		Unlock:            {tsvStruct: &UnlockTsv{}, tsvManagerStruct: &UnlockTsvManager{}},
		Task:              {tsvStruct: &TaskTsv{}, tsvManagerStruct: &TaskTsvManager{}},
		Plot:              {tsvStruct: &PlotTsv{}, tsvManagerStruct: &PlotTsvManager{}},
		Forge:             {tsvStruct: &ForgeTsv{}, tsvManagerStruct: &ForgeTsvManager{}},
		Market:            {tsvStruct: &MarketTsv{}, tsvManagerStruct: &MarketTsvManager{}},
		Decompose:         {tsvStruct: &DecomposeTsv{}, tsvManagerStruct: &DecomposeTsvManager{}},
		Mall:              {tsvStruct: &MallTsv{}, tsvManagerStruct: &MallTsvManager{}},
		Weather:           {tsvStruct: &WeatherTsv{}, tsvManagerStruct: &WeatherTsvManager{}},
		WeatherRandom:     {tsvStruct: &WeatherRandomTsv{}, tsvManagerStruct: &WeatherRandomTsvManager{}},
		Collect:            {tsvStruct: &CollectTsv{}, tsvManagerStruct: &CollectTsvManager{}},
		CollectProbability: {tsvStruct: &CollectProbabilityTsv{}, tsvManagerStruct: &CollectProbabilityTsvManager{}},
	}
)

// 定义每个节点需要加载的tsv
var (
	_NodeTsv = map[misc.TNode][]TsvEnum{
		misc.NodeGame: {BuildingAttribute, Crop, Farmland, Forge, Market, Tavern, Decompose, Prop, Mall, Task, Plot,CityBuilding},
		misc.NodeServices_Data: {RandomNickname, Prop, PropGift, SystemMail, CityBuilding, BuildingAttribute, Tavern,
			TavernMakings, Crop, Farmland, Unlock, Task, Plot, CityInit, Mall},
		misc.NodeServices_Space: {Weather,WeatherRandom,Collect,CollectProbability},
		misc.NodeAi:             {Monster, MonsterBornPoint},
	}
)

var (
	_TsvFacs        = make(map[TsvEnum]func() ITsv)
	_TsvManagerFacs = make(map[TsvEnum]func() ITsvManager)
	_TsvManagers    = make(map[TsvEnum]ITsvManager)
)

const (
	RowSplit = "\n"
	ColSplit = "\t"
)

//  LoadNodeTsv 加载指定节点的TSV
func LoadNodeTsv(nodes ...misc.TNode) {
	createFactorys(nodes...)
	if len(nodes) > 0 {
		for _, node := range nodes {
			tsvs := _NodeTsv[node]
			for _, tsv := range tsvs {
				loadData(tsv)
			}
		}
	}
}

// LoadAllTsv 加载所有节点的TSV (重复加载会覆盖)
func LoadAllTsv() {
	createFactorys()
	if len(_TsvFacs) > 0 {
		for enum := range _TsvFacs {
			loadData(enum)
		}
	}
}

//Reload 重新加载数据
func Reload() {
	createFactorys()
	for k := range _TsvManagers {
		loadData(k)
	}
}

//GetTsvManager 获取tsv数据
func GetTsvManager(enum TsvEnum) ITsvManager {
	manager, ok := _TsvManagers[enum]
	if ok {
		return manager
	}
	return nil
}

// createFactorys 创建工厂函数
func createFactorys(nodes ...misc.TNode) {
	if len(nodes) > 0 {
		for _, node := range nodes {
			tsvs := _NodeTsv[node]
			for _, tsv := range tsvs {
				createFactory(tsv, _Relation[tsv].tsvStruct, _Relation[tsv].tsvManagerStruct)
			}
		}
	} else {
		for _, tsvs := range _NodeTsv {
			for _, tsv := range tsvs {
				createFactory(tsv, _Relation[tsv].tsvStruct, _Relation[tsv].tsvManagerStruct)
			}
		}
	}
}

type ITsv interface {
	SetValues(values []string)
}
type ITsvManager interface {
	SetValues(ITsv)
	ClearValues()
}

// createFactory 创建工厂函数
func createFactory(enum TsvEnum, tsv ITsv, tsvManager ITsvManager) {
	tsvElem := reflect.TypeOf(tsv).Elem()
	tsvManagerElem := reflect.TypeOf(tsvManager).Elem()

	_TsvFacs[enum] = func() ITsv {
		return reflect.New(tsvElem).Interface().(ITsv)
	}

	_TsvManagerFacs[enum] = func() ITsvManager {
		tsvManager, ok := _TsvManagers[enum]
		if ok {
			return tsvManager
		} else {
			tsvManager = reflect.New(tsvManagerElem).Interface().(ITsvManager)
			_TsvManagers[enum] = tsvManager
		}
		return tsvManager
	}
}

// loadData 加载Tsv文件数据
func loadData(enum TsvEnum) {
	path := fmt.Sprintf("%s/tsv/%s.tsv", utils.ExeDir(), enum)

	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println("loadData failed：" + path)
		return
	}
	tsvFac, _ := _TsvFacs[enum]
	tsvManagerFac, _ := _TsvManagerFacs[enum]
	tsvManager := tsvManagerFac()
	tsvManager.ClearValues()

	content := strings.ReplaceAll(string(bytes), "\r\n", RowSplit)
	lines := strings.Split(content, RowSplit)

	if len(lines) <= 3 {
		fmt.Println("tsv解析错误：" + path)
		return
	}
	for i, lineStr := range lines {
		if i < 3 {
			continue
		}
		if len(lineStr) == 0 {
			continue
		}
		values := strings.Split(lineStr, ColSplit)
		tsv := tsvFac()
		tsv.SetValues(values)
		tsvManager.SetValues(tsv)
	}
}
