package tsv

type SystemMailTsv struct {
	//索引
	Id int32

	//系统邮箱标题
	SystemMailTitle string

	//邮件内容
	SystemMailContent string

	//附件
	SystemMailEnclosures string

	//系统邮件保存天数
	EnclosuresOverdueTime int32

	//邮件状态
	SystemMailState int32

	//转世之后是否删除
	SystemMailReincarnationDelState int32

	//开始日期
	StartTime string

	//循环发送时间_(分钟
	CD int32

	//最大发送
	MaxAmoust int32
}

func (tsv *SystemMailTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.SystemMailTitle = values[i]
	i++
	tsv.SystemMailContent = values[i]
	i++
	tsv.SystemMailEnclosures = values[i]
	i++
	tsv.EnclosuresOverdueTime = ToInt(values[i])
	i++
	tsv.SystemMailState = ToInt(values[i])
	i++
	tsv.SystemMailReincarnationDelState = ToInt(values[i])
	i++
	tsv.StartTime = values[i]
	i++
	tsv.CD = ToInt(values[i])
	i++
	tsv.MaxAmoust = ToInt(values[i])
	i++
}

type SystemMailTsvManager struct {
	TsvSlice []*SystemMailTsv
	TsvMap   map[int32]*SystemMailTsv
}

func (manager *SystemMailTsvManager) SetValues(data ITsv) {
	tsv := data.(*SystemMailTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *SystemMailTsvManager) ClearValues() {
	manager.TsvSlice = make([]*SystemMailTsv, 0)
	manager.TsvMap = make(map[int32]*SystemMailTsv, 0)
}
