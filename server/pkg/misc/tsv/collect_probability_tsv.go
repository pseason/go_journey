package tsv

type CollectProbabilityTsv struct {
	//索引
	Id int32

	//熟练度1级对应概率（百分比）
	Probability1 int32

	//熟练度2级对应概率（百分比）
	Probability2 int32

	//熟练度3级对应概率（百分比）
	Probability3 int32

	//熟练度4级对应概率（百分比）
	Probability4 int32

	//熟练度5级对应概率（百分比）
	Probability5 int32

	//熟练度6级对应概率（百分比）
	Probability6 int32
}

func (tsv *CollectProbabilityTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Probability1 = ToInt(values[i])
	i++
	tsv.Probability2 = ToInt(values[i])
	i++
	tsv.Probability3 = ToInt(values[i])
	i++
	tsv.Probability4 = ToInt(values[i])
	i++
	tsv.Probability5 = ToInt(values[i])
	i++
	tsv.Probability6 = ToInt(values[i])
	i++
}

type CollectProbabilityTsvManager struct {
	TsvSlice []*CollectProbabilityTsv
	TsvMap   map[int32]*CollectProbabilityTsv
}

func (manager *CollectProbabilityTsvManager) SetValues(data ITsv) {
	tsv := data.(*CollectProbabilityTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *CollectProbabilityTsvManager) ClearValues() {
	manager.TsvSlice = make([]*CollectProbabilityTsv, 0)
	manager.TsvMap = make(map[int32]*CollectProbabilityTsv, 0)
}
