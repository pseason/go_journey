package tsv

type TavernTsv struct {
	//建筑等级
	Level int32

	//所需时间（秒）
	Time int32

	//槽位上限
	Volume int32

	//产出数量
	Produce []int32
}

func (tsv *TavernTsv) SetValues(values []string) {
	i := 0
	tsv.Level = ToInt(values[i])
	i++
	tsv.Time = ToInt(values[i])
	i++
	tsv.Volume = ToInt(values[i])
	i++
	tsv.Produce = ToIntArray(values[i])
	i++
}

type TavernTsvManager struct {
	TsvSlice []*TavernTsv
	TsvMap   map[int32]*TavernTsv
}

func (manager *TavernTsvManager) SetValues(data ITsv) {
	tsv := data.(*TavernTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Level] = tsv
}
func (manager *TavernTsvManager) ClearValues() {
	manager.TsvSlice = make([]*TavernTsv, 0)
	manager.TsvMap = make(map[int32]*TavernTsv, 0)
}
