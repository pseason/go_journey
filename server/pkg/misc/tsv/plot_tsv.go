package tsv

type PlotTsv struct {
	//Id
	Id int32

	//剧情标题
	Name string

	//剧情类型
	Type int32

	//下条剧情
	NextId int32

	//按钮功能
	Btn []int32

	//按钮<br>文字内容
	BtnName []string

	//任务ID
	TaskId []int32

	//获得物品
	ItemId [][]int32

	//跳过剧情
	To_plot []int32

	//写在分支点，第一个位置显示战斗失败剧情，第二个位置显示战斗成功剧情，没有填[0,0]
	Fight_plot []int32

	//第一个位置是场景类型，第一个位置是进入场景剧情ID，第二个位置是退出场景剧情ID：[1,1,2]
	Switch_scene []int32

	//副本关闭（0-不关闭或不是副本的，1是关闭）
	Duplicate_close int32

	//最后一条剧情
	NoviceOver int32
}

func (tsv *PlotTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Name = values[i]
	i++
	tsv.Type = ToInt(values[i])
	i++
	tsv.NextId = ToInt(values[i])
	i++
	tsv.Btn = ToIntArray(values[i])
	i++
	tsv.BtnName = ToStringArray(values[i])
	i++
	tsv.TaskId = ToIntArray(values[i])
	i++
	tsv.ItemId = ToIntArray2(values[i])
	i++
	tsv.To_plot = ToIntArray(values[i])
	i++
	tsv.Fight_plot = ToIntArray(values[i])
	i++
	tsv.Switch_scene = ToIntArray(values[i])
	i++
	tsv.Duplicate_close = ToInt(values[i])
	i++
	tsv.NoviceOver = ToInt(values[i])
	i++
}

type PlotTsvManager struct {
	TsvSlice []*PlotTsv
	TsvMap   map[int32]*PlotTsv
}

func (manager *PlotTsvManager) SetValues(data ITsv) {
	tsv := data.(*PlotTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *PlotTsvManager) ClearValues() {
	manager.TsvSlice = make([]*PlotTsv, 0)
	manager.TsvMap = make(map[int32]*PlotTsv, 0)
}
