package tsv

import (
	"strconv"

	jsoniter "github.com/json-iterator/go"
)

func ToBool(value string) bool {
	return value != "0"
}

func ToInt(value string) int32 {
	result, err := strconv.Atoi(value)
	if err != nil {
		result = 0
	}
	return int32(result)
}
func ToIntArray(value string) []int32 {
	var result []int32
	jsoniter.Unmarshal([]byte(value), &result)
	return result
}
func ToIntArray2(value string) [][]int32 {
	var result [][]int32
	jsoniter.Unmarshal([]byte(value), &result)
	return result
}
func ToFloat(value string) float32 {
	result, _ := strconv.ParseFloat(value, 32)
	return float32(result)
}
func ToFloatArray(value string) []float32 {
	var result []float32
	jsoniter.Unmarshal([]byte(value), &result)
	return result
}
func ToFloatArray2(value string) [][]float32 {
	var result [][]float32
	jsoniter.Unmarshal([]byte(value), &result)
	return result
}
func ToStringArray(value string) []string {
	var result []string
	jsoniter.Unmarshal([]byte(value), &result)
	return result
}
func ToStringArray2(value string) [][]string {
	var result [][]string
	jsoniter.Unmarshal([]byte(value), &result)
	return result
}
