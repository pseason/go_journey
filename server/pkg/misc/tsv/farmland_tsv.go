package tsv

type FarmlandTsv struct {
	//农场等级
	Lv int32

	//可以建造农田的数量
	Buildable int32

	//收获加成百分比数量（每次产量增加百分比）
	RewardAddition float32

	//收获效率加成百分比时间（每次产出间隔减少）
	RewardTimeAddition float32

	//养料上限
	PabulumMax int32
}

func (tsv *FarmlandTsv) SetValues(values []string) {
	i := 0
	tsv.Lv = ToInt(values[i])
	i++
	tsv.Buildable = ToInt(values[i])
	i++
	tsv.RewardAddition = ToFloat(values[i])
	i++
	tsv.RewardTimeAddition = ToFloat(values[i])
	i++
	tsv.PabulumMax = ToInt(values[i])
	i++
}

type FarmlandTsvManager struct {
	TsvSlice []*FarmlandTsv
	TsvMap   map[int32]*FarmlandTsv
}

func (manager *FarmlandTsvManager) SetValues(data ITsv) {
	tsv := data.(*FarmlandTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Lv] = tsv
}
func (manager *FarmlandTsvManager) ClearValues() {
	manager.TsvSlice = make([]*FarmlandTsv, 0)
	manager.TsvMap = make(map[int32]*FarmlandTsv, 0)
}
