package tsv

type CityBuildingTsv struct {
	//id
	Id int32

	//建筑类型
	BuildType int32

	//建筑名称
	BuildName string

	//建筑等级
	BuildLv int32

	//建筑坐标
	BuildCoordinate []float32
}

func (tsv *CityBuildingTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.BuildType = ToInt(values[i])
	i++
	tsv.BuildName = values[i]
	i++
	tsv.BuildLv = ToInt(values[i])
	i++
	tsv.BuildCoordinate = ToFloatArray(values[i])
	i++
}

type CityBuildingTsvManager struct {
	TsvSlice []*CityBuildingTsv
	TsvMap   map[int32]*CityBuildingTsv
}

func (manager *CityBuildingTsvManager) SetValues(data ITsv) {
	tsv := data.(*CityBuildingTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *CityBuildingTsvManager) ClearValues() {
	manager.TsvSlice = make([]*CityBuildingTsv, 0)
	manager.TsvMap = make(map[int32]*CityBuildingTsv, 0)
}
