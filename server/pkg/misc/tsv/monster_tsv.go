package tsv

type MonsterTsv struct {
	//索引
	ID int32

	//AI名称
	Name string

	//3D资源路径
	Prefab string

	//巡逻速度
	PatrolSpeed float32

	//警戒半径
	WarnRadius int32

	//最大仇恨数量
	MaxHaredCount int32

	//重生时间
	RebirthDuration int32

	//资源Id
	JungleId int32

	//场景小类型
	SubSceneType int32

	//身位半径
	BodyRadius float32
}

func (tsv *MonsterTsv) SetValues(values []string) {
	i := 0
	tsv.ID = ToInt(values[i])
	i++
	tsv.Name = values[i]
	i++
	tsv.Prefab = values[i]
	i++
	tsv.PatrolSpeed = ToFloat(values[i])
	i++
	tsv.WarnRadius = ToInt(values[i])
	i++
	tsv.MaxHaredCount = ToInt(values[i])
	i++
	tsv.RebirthDuration = ToInt(values[i])
	i++
	tsv.JungleId = ToInt(values[i])
	i++
	tsv.SubSceneType = ToInt(values[i])
	i++
	tsv.BodyRadius = ToFloat(values[i])
	i++
}

type MonsterTsvManager struct {
	TsvSlice []*MonsterTsv
	TsvMap   map[int32]*MonsterTsv
}

func (manager *MonsterTsvManager) SetValues(data ITsv) {
	tsv := data.(*MonsterTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.ID] = tsv
}
func (manager *MonsterTsvManager) ClearValues() {
	manager.TsvSlice = make([]*MonsterTsv, 0)
	manager.TsvMap = make(map[int32]*MonsterTsv, 0)
}
