package tsv

type WeatherRandomTsv struct {
	//天气id
	Id int32

	//天气类型<br>（1-烈日，2-下雪，3-下雨，4-刮风）
	WeatherType int32

	//强度范围<br>（范围0到1，0是晴天）
	WeatherValue []float32

	//buffId<br>（没有就填[]）
	BuffId []int32

	//允许出现的采集物ID<br>（没有就填[]）
	ObjectId []int32
}

func (tsv *WeatherRandomTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.WeatherType = ToInt(values[i])
	i++
	tsv.WeatherValue = ToFloatArray(values[i])
	i++
	tsv.BuffId = ToIntArray(values[i])
	i++
	tsv.ObjectId = ToIntArray(values[i])
	i++
}

type WeatherRandomTsvManager struct {
	TsvSlice []*WeatherRandomTsv
	TsvMap   map[int32]*WeatherRandomTsv
}

func (manager *WeatherRandomTsvManager) SetValues(data ITsv) {
	tsv := data.(*WeatherRandomTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *WeatherRandomTsvManager) ClearValues() {
	manager.TsvSlice = make([]*WeatherRandomTsv, 0)
	manager.TsvMap = make(map[int32]*WeatherRandomTsv, 0)
}
