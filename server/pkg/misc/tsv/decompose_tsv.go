package tsv

type DecomposeTsv struct {
	//装备ID
	Id int32

	//分解列表
	DecomposeList [][]int32
}

func (tsv *DecomposeTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.DecomposeList = ToIntArray2(values[i])
	i++
}

type DecomposeTsvManager struct {
	TsvSlice []*DecomposeTsv
	TsvMap   map[int32]*DecomposeTsv
}

func (manager *DecomposeTsvManager) SetValues(data ITsv) {
	tsv := data.(*DecomposeTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *DecomposeTsvManager) ClearValues() {
	manager.TsvSlice = make([]*DecomposeTsv, 0)
	manager.TsvMap = make(map[int32]*DecomposeTsv, 0)
}
