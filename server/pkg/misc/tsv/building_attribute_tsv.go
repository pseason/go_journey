package tsv

type BuildingAttributeTsv struct {
	//索引
	Id int32

	//建筑模版id
	ObjectId int32

	//等级
	Level int32

	//属性id
	StatId int32

	//是否无敌
	IsInvincible bool

	//升级材料
	Cost [][]int32

	//所需建设度
	BuildProgress int32

	//需求建造等级
	NeedBuildLevel [][]int32

	//繁荣度
	Flourish int32
}

func (tsv *BuildingAttributeTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.ObjectId = ToInt(values[i])
	i++
	tsv.Level = ToInt(values[i])
	i++
	tsv.StatId = ToInt(values[i])
	i++
	tsv.IsInvincible = ToBool(values[i])
	i++
	tsv.Cost = ToIntArray2(values[i])
	i++
	tsv.BuildProgress = ToInt(values[i])
	i++
	tsv.NeedBuildLevel = ToIntArray2(values[i])
	i++
	tsv.Flourish = ToInt(values[i])
	i++
}

type BuildingAttributeTsvManager struct {
	TsvSlice []*BuildingAttributeTsv
	TsvMap   map[int32]*BuildingAttributeTsv
}

func (manager *BuildingAttributeTsvManager) SetValues(data ITsv) {
	tsv := data.(*BuildingAttributeTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *BuildingAttributeTsvManager) ClearValues() {
	manager.TsvSlice = make([]*BuildingAttributeTsv, 0)
	manager.TsvMap = make(map[int32]*BuildingAttributeTsv, 0)
}
