package tsv

type CollectTsv struct {
	//索引
	Id int32

	//名称
	Name string

	//采集时间(单位秒)
	Hp int32

	//基础产出<br>(物品/最小/最大/总量/概率ID)
	Props [][]int32

	//稀有产出<br>(物品/最小/最大/概率ID)
	PropsExtra [][]int32

	//天气产出道具(物品/最小/最大/天气/概率ID)
	PropsWeather [][]int32

	//每次采集消耗精力
	ConsumeEnergy int32

	//重生时间间隔(单位秒，0表示不重生)
	RebirthDuration int32

	//可出现的昼夜<br>（0-所有，1-白天，2-黑夜）
	Time int32

	//采集物可出现的天气
	Weather []int32
}

func (tsv *CollectTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Name = values[i]
	i++
	tsv.Hp = ToInt(values[i])
	i++
	tsv.Props = ToIntArray2(values[i])
	i++
	tsv.PropsExtra = ToIntArray2(values[i])
	i++
	tsv.PropsWeather = ToIntArray2(values[i])
	i++
	tsv.ConsumeEnergy = ToInt(values[i])
	i++
	tsv.RebirthDuration = ToInt(values[i])
	i++
	tsv.Time = ToInt(values[i])
	i++
	tsv.Weather = ToIntArray(values[i])
	i++
}

type CollectTsvManager struct {
	TsvSlice []*CollectTsv
	TsvMap   map[int32]*CollectTsv
}

func (manager *CollectTsvManager) SetValues(data ITsv) {
	tsv := data.(*CollectTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *CollectTsvManager) ClearValues() {
	manager.TsvSlice = make([]*CollectTsv, 0)
	manager.TsvMap = make(map[int32]*CollectTsv, 0)
}
