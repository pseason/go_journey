package tsv

type RandomNicknameTsv struct {
	//id
	Id int32

	//百家姓
	FamilyName string

	//男双名
	MaleName string

	//女双名
	FemaleName string

	//男单
	MaleSingleName string

	//女单
	FemaleSingleName string
}

func (tsv *RandomNicknameTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.FamilyName = values[i]
	i++
	tsv.MaleName = values[i]
	i++
	tsv.FemaleName = values[i]
	i++
	tsv.MaleSingleName = values[i]
	i++
	tsv.FemaleSingleName = values[i]
	i++
}

type RandomNicknameTsvManager struct {
	TsvSlice []*RandomNicknameTsv
	TsvMap   map[int32]*RandomNicknameTsv
}

func (manager *RandomNicknameTsvManager) SetValues(data ITsv) {
	tsv := data.(*RandomNicknameTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *RandomNicknameTsvManager) ClearValues() {
	manager.TsvSlice = make([]*RandomNicknameTsv, 0)
	manager.TsvMap = make(map[int32]*RandomNicknameTsv, 0)
}
