package tsv

import (
	"95eh.com/eg/utils"
	"fmt"
	"hm/pkg/misc"
	"testing"
)

/*
@Time   : 2021-11-12 11:16
@Author : wushu
@DESC   :
*/
func init() {
	utils.SetExeDir("D:/Develop/WorkSpaceForGoLand/hm-server/output") // wushu
	utils.SetExeDir("~/Develop/ProjectForGoLand/hm-server/output") // wushu-MacOS
	//utils.SetExeDir("D:\\hm\\hm_ms_server\\output") // licoy
	//LoadAllTsv()
	LoadNodeTsv(misc.NodeServices_Data)
}

func TestGetManager(t *testing.T) {
	randomNicknameTsvManager := GetTsvManager(RandomNickname).(*RandomNicknameTsvManager)
	fmt.Println(randomNicknameTsvManager)
}
