package tsv

type CropTsv struct {
	//农作物ID(种子id)填写道具id
	CropId int32

	//产出间隔时间(分钟)
	MatureTime int32

	//产出道具222号道具22%几率产生3个
	Reward [][]int32

	//农作物生命力时间（分钟），无法停止
	CropHp int32

	//描述文件
	Desc string

	//灌溉需要材料:参数1:物品id 参数2:一次性消耗物品数量 参数3:每组物品添加的养料值
	NeedResource []int32

	//关联模型，jungle
	Model int32

	//养料消耗
	Consume int32

	//建筑类型
	BuildType int32

	//解锁等级
	Needlevel int32
}

func (tsv *CropTsv) SetValues(values []string) {
	i := 0
	tsv.CropId = ToInt(values[i])
	i++
	tsv.MatureTime = ToInt(values[i])
	i++
	tsv.Reward = ToIntArray2(values[i])
	i++
	tsv.CropHp = ToInt(values[i])
	i++
	tsv.Desc = values[i]
	i++
	tsv.NeedResource = ToIntArray(values[i])
	i++
	tsv.Model = ToInt(values[i])
	i++
	tsv.Consume = ToInt(values[i])
	i++
	tsv.BuildType = ToInt(values[i])
	i++
	tsv.Needlevel = ToInt(values[i])
	i++
}

type CropTsvManager struct {
	TsvSlice []*CropTsv
	TsvMap   map[int32]*CropTsv
}

func (manager *CropTsvManager) SetValues(data ITsv) {
	tsv := data.(*CropTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.CropId] = tsv
}
func (manager *CropTsvManager) ClearValues() {
	manager.TsvSlice = make([]*CropTsv, 0)
	manager.TsvMap = make(map[int32]*CropTsv, 0)
}
