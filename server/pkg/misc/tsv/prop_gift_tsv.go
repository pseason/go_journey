package tsv

type PropGiftTsv struct {
	//物品ID
	Id int32

	//类型
	Type []int32

	//技能列表（[[技能ID,数量,机率]...]）
	Skills [][]int32

	//（1-金币，2-虚幻币）货币（[[货币ID,数量,机率]...]）
	Golds [][]int32

	//物品（[[物品ID,数量,机率]...]）
	Props [][]int32

	//随机一个技能列表（[[技能ID,数量,权重]...]）
	SkillsOne [][]int32

	//随机一个货币（[[货币ID,数量,权重]...]）
	GoldsOne [][]int32

	//随机一个物品（[[物品ID,数量,权重]...]）
	PropsOne [][]int32

	//多选一，玩家选择（[[技能ID,数量]...]）
	SkillManyOne [][]int32
}

func (tsv *PropGiftTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Type = ToIntArray(values[i])
	i++
	tsv.Skills = ToIntArray2(values[i])
	i++
	tsv.Golds = ToIntArray2(values[i])
	i++
	tsv.Props = ToIntArray2(values[i])
	i++
	tsv.SkillsOne = ToIntArray2(values[i])
	i++
	tsv.GoldsOne = ToIntArray2(values[i])
	i++
	tsv.PropsOne = ToIntArray2(values[i])
	i++
	tsv.SkillManyOne = ToIntArray2(values[i])
	i++
}

type PropGiftTsvManager struct {
	TsvSlice []*PropGiftTsv
	TsvMap   map[int32]*PropGiftTsv
}

func (manager *PropGiftTsvManager) SetValues(data ITsv) {
	tsv := data.(*PropGiftTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *PropGiftTsvManager) ClearValues() {
	manager.TsvSlice = make([]*PropGiftTsv, 0)
	manager.TsvMap = make(map[int32]*PropGiftTsv, 0)
}
