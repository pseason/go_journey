package tsv

type TaskTsv struct {
	//ID
	Id int32

	//任务分类<br> 1-指引任务<br> 2-主线任务<br> 3-支线任务<br> 4-奇遇任务<br> 5-建造任务<br> 6-仙系任务<br> 7-魔系任务<br> 8-日常任务（每日固定时间刷新）<br> 9-城邦活动任务（玩家在城邦任务池中领取，一定时间刷新）<br> 10-成就任务<br> 11-事件任务<br> 12-氏族任务（每周刷新）
	Type int32

	//类型 <br>1 采集PICK [物品ID，数量]<br>2 打怪HUNT [怪物ID,数量]<br>3 装备打造BUILD [打造ID,数量]<br>4 使用技能USE_SKILLS [技能ID,数量]<br>5 场景切换SCENE_SWITCH [地图,1] 0-大世界 1-城邦 2-竞技场 3-奇袭 4-新手村 5-BOSS 6-领主 7单挑<br>6 建筑-投入资源ERECT_IN_RE [物品ID,数量]<br>7 建筑-建造ERECT_BUILD [建筑ID,等级] citybuild表查ID，当前所在的城邦有大于或等于该等级也算完成<br>8 使用物品USE_PROP [物品ID,数量]<br>9 装备技能EQUIPMENT_SKILLS [技能ID,1]<br>10 灵魂转世SOUL_REINCARNATION [1,转世次数]<br>11 登录LOGIN [0,1] 代表游戏在线[[18,0],[23,59]] 时、分<br>12 副本DUPLICATE [副本ID,次数]<br>13 战场BATTLEFIELD [战场ID,次数]<br>14 竞技场次数ARENA [竞技场玩法ID,次数,输赢] 1-3V3 2-1V1 3-15V15夺城战 4-AI<br>15 MOBA<br>16 击杀玩家KILL_PLAYER [地图,击杀次数] 0-大世界 1-城邦 2-竞技场 3-奇袭 4-新手村 5-BOSS 6-领主 7单挑<br>17 发言SPEAK [频道ID,次数]<br>18 赠送GIVE <br>19 抽卡DRAW_CARD [奖池ID,次数]<br>20 签到 <br>21 采集主体PICK_MAIN [主体ID,次数]<br>22 模拟攻城NEW_HAND_GF_SH [类型,次数]<br>23 穿戴装备 [物品ID,1]<br>24 升级技能 [技能ID,提升级数]<br>25 加入城邦 [0,1]<br>26 奇袭 [输赢,次数]<br>27 活跃度 [0,活跃度数量]<br>28 加入王国 [0,1]<br>29 年龄 <br>30 王国宣战 [1,次数]<br>31 攻城战 [输赢,次数]<br>32 匹配竞技场 [竞技场玩法ID,次数]<br>33 客户端完成任务 []无条件 [X,Z]寻路坐标<br>34 挑战BOSS失败 [怪物ID,挑战次数]<br>35 技能升星 [技能ID,提升星级]<br>36 竞技场段位 [竞技场玩法ID,段位]<br>37 竞技场击杀 [竞技场玩法ID,次数]<br>38 发展据点 [等级编号,1]<br>39 发展王国 [等级编号,1]<br>40 据点等级 [等级编号,数量]<br>41 增减属性 [属性ID，数量] 数量填负数就是消耗<br>42 当前属性 [属性ID，下限（含）]<br>43 次日登陆 []<br>44 加入军团 [0,1]<br>45 特殊次日登录 从建号开始算时间 3点刷新 []
	Afttype int32

	//下个任务ID
	NextId int32

	//奖励活跃度
	GetActivity int32

	//要求物品/属性
	Needprop [][]int32

	//寻路坐标 第三位数字 0-大世界 1-城邦 2-竞技场 3-奇袭 4-新手村 5-BOSS 6-领主 7单挑
	Coordinate []int32

	//是否扣除要求物品
	Subtract int32

	//奖励物品/属性
	Getprop [][]int32

	//奖励技能ID/等级/数量
	Skill [][]int32

	//奖励属性<br><br>1001 金币<br>1002 虚幻币<br>1003 城邦贡献值<br>1004 王国贡献值<br>1009 威望<br>1019 灵魂能量<br><br>41/42号任务，不能给属性奖励，否则会进入无限循环判断！！！
	GetMix [][]int32

	//完成任务时自动穿装备
	Auto_equip [][]int32

	//完成任务时自动装技能
	Auto_skill [][]int32

	//完成任务时自动添加物品到快捷栏
	Auto_shortcut_bar [][]int32

	//AND(1, "所有都必须达标"),<br>OR(2, "其中一个达标即可"),<br>RAND(3, "随意，只看所有的总数"),
	Rule int32

	//触发剧情（完成任务后触发的剧情ID，没有填0）
	Plot int32

	//手动领取奖励的控制
	ReceiveRewards int32

	//是否需要弱指引 1-是 0-否
	IsGuide int32

	//隐藏任务<br>1-隐藏 0-有界面显示
	Hidden int32
}

func (tsv *TaskTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Type = ToInt(values[i])
	i++
	tsv.Afttype = ToInt(values[i])
	i++
	tsv.NextId = ToInt(values[i])
	i++
	tsv.GetActivity = ToInt(values[i])
	i++
	tsv.Needprop = ToIntArray2(values[i])
	i++
	tsv.Coordinate = ToIntArray(values[i])
	i++
	tsv.Subtract = ToInt(values[i])
	i++
	tsv.Getprop = ToIntArray2(values[i])
	i++
	tsv.Skill = ToIntArray2(values[i])
	i++
	tsv.GetMix = ToIntArray2(values[i])
	i++
	tsv.Auto_equip = ToIntArray2(values[i])
	i++
	tsv.Auto_skill = ToIntArray2(values[i])
	i++
	tsv.Auto_shortcut_bar = ToIntArray2(values[i])
	i++
	tsv.Rule = ToInt(values[i])
	i++
	tsv.Plot = ToInt(values[i])
	i++
	tsv.ReceiveRewards = ToInt(values[i])
	i++
	tsv.IsGuide = ToInt(values[i])
	i++
	tsv.Hidden = ToInt(values[i])
	i++
}

type TaskTsvManager struct {
	TsvSlice []*TaskTsv
	TsvMap   map[int32]*TaskTsv
}

func (manager *TaskTsvManager) SetValues(data ITsv) {
	tsv := data.(*TaskTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *TaskTsvManager) ClearValues() {
	manager.TsvSlice = make([]*TaskTsv, 0)
	manager.TsvMap = make(map[int32]*TaskTsv, 0)
}
