package tsv

type UnlockTsv struct {
	//索引
	Id int32

	//条件id
	ConditionId int32

	//条件类型<br>1-获得任务，2-完成任务，3-获得剧情，4-完成剧情<br>101-转世一次，102-转世两次
	ConditionType int32
}

func (tsv *UnlockTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.ConditionId = ToInt(values[i])
	i++
	tsv.ConditionType = ToInt(values[i])
	i++
}

type UnlockTsvManager struct {
	TsvSlice []*UnlockTsv
	TsvMap   map[int32]*UnlockTsv
}

func (manager *UnlockTsvManager) SetValues(data ITsv) {
	tsv := data.(*UnlockTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *UnlockTsvManager) ClearValues() {
	manager.TsvSlice = make([]*UnlockTsv, 0)
	manager.TsvMap = make(map[int32]*UnlockTsv, 0)
}
