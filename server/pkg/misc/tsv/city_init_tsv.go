package tsv

type CityInitTsv struct {
	//id
	Id int32

	//城市名称
	CityName string

	//所在领地ID
	LandID int32
}

func (tsv *CityInitTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.CityName = values[i]
	i++
	tsv.LandID = ToInt(values[i])
	i++
}

type CityInitTsvManager struct {
	TsvSlice []*CityInitTsv
	TsvMap   map[int32]*CityInitTsv
}

func (manager *CityInitTsvManager) SetValues(data ITsv) {
	tsv := data.(*CityInitTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *CityInitTsvManager) ClearValues() {
	manager.TsvSlice = make([]*CityInitTsv, 0)
	manager.TsvMap = make(map[int32]*CityInitTsv, 0)
}
