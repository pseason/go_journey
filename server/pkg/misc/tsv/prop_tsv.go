package tsv

type PropTsv struct {
	//ID的组成：kind+type+grade+序号
	Id int32

	//名字
	Name string

	//添加buff的变量
	BufferVar [][]float32

	//类型
	Type int32

	//品质
	Grade int32

	//出售价格
	Sell int32

	//可否堆叠
	Stack int32

	//出售ID
	Sellid int32

	//BUFFID（XML文件）
	Buffid []int32

	//市场大类
	MarketMainType int32

	//市场小类
	MarketType int32

	//类型排序
	Type_sort int32

	//排序
	Sort int32

	//冷却时间类型
	EdibleType int32

	//冷却时间
	EdibleFreezingTime int32

	//获取途径
	GetWays []int32

	//合成
	Compose []int32

	//分解
	Decompose [][]int32
}

func (tsv *PropTsv) SetValues(values []string) {
	i := 0
	tsv.Id = ToInt(values[i])
	i++
	tsv.Name = values[i]
	i++
	tsv.BufferVar = ToFloatArray2(values[i])
	i++
	tsv.Type = ToInt(values[i])
	i++
	tsv.Grade = ToInt(values[i])
	i++
	tsv.Sell = ToInt(values[i])
	i++
	tsv.Stack = ToInt(values[i])
	i++
	tsv.Sellid = ToInt(values[i])
	i++
	tsv.Buffid = ToIntArray(values[i])
	i++
	tsv.MarketMainType = ToInt(values[i])
	i++
	tsv.MarketType = ToInt(values[i])
	i++
	tsv.Type_sort = ToInt(values[i])
	i++
	tsv.Sort = ToInt(values[i])
	i++
	tsv.EdibleType = ToInt(values[i])
	i++
	tsv.EdibleFreezingTime = ToInt(values[i])
	i++
	tsv.GetWays = ToIntArray(values[i])
	i++
	tsv.Compose = ToIntArray(values[i])
	i++
	tsv.Decompose = ToIntArray2(values[i])
	i++
}

type PropTsvManager struct {
	TsvSlice []*PropTsv
	TsvMap   map[int32]*PropTsv
}

func (manager *PropTsvManager) SetValues(data ITsv) {
	tsv := data.(*PropTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Id] = tsv
}
func (manager *PropTsvManager) ClearValues() {
	manager.TsvSlice = make([]*PropTsv, 0)
	manager.TsvMap = make(map[int32]*PropTsv, 0)
}
