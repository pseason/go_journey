package tsv

type MarketTsv struct {
	//市场等级
	Level int32

	//居民拍卖数量
	MemberNubmer int32
}

func (tsv *MarketTsv) SetValues(values []string) {
	i := 0
	tsv.Level = ToInt(values[i])
	i++
	tsv.MemberNubmer = ToInt(values[i])
	i++
}

type MarketTsvManager struct {
	TsvSlice []*MarketTsv
	TsvMap   map[int32]*MarketTsv
}

func (manager *MarketTsvManager) SetValues(data ITsv) {
	tsv := data.(*MarketTsv)
	manager.TsvSlice = append(manager.TsvSlice, tsv)
	manager.TsvMap[tsv.Level] = tsv
}
func (manager *MarketTsvManager) ClearValues() {
	manager.TsvSlice = make([]*MarketTsv, 0)
	manager.TsvMap = make(map[int32]*MarketTsv, 0)
}
