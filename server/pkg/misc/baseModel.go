package misc

import (
	"95eh.com/eg/utils"
	"gorm.io/gorm"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
)

/*
@Time   : 2021-11-10 14:26
@Author : wushu
@DESC   :
*/

const ModelIdFiledName = "id" // id字段名

type IModel interface {
	GetId() int64
	SetId(id int64)
	GetByEnum(code model_field_e.Enum) interface{}
}

type BaseModel struct {
	Id      int64          `gorm:"type:bigint; not null; comment:主键"`
	Created int64          `gorm:"autoCreateTime:milli; type:bigint; not null; comment:创建时间"`
	Updated int64          `gorm:"autoUpdateTime:milli; type:bigint; not null; comment:更新时间"`
	Deleted gorm.DeletedAt `gorm:"comment:删除时间"`
}

func (m *BaseModel) BeforeCreate(tx *gorm.DB) (err error) {
	m.genId()
	return nil
}

func (m *BaseModel) genId() {
	if m.Id != 0 {
		return
	}
	m.Id = utils.GenSnowflakeGlobalNodeId()
}

func (m *BaseModel) GetId() int64 {
	return m.Id
}

func (m *BaseModel) SetId(id int64) {
	m.Id = id
}

type IModelWithRedis interface {
	IModel
	ToRedisMap() (data map[string]interface{})
	FetchRedisMapData(info map[string]string)
}
