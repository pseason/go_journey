package tools

/*
@Time   : 2021-11-24 19:16
@Author : wushu
@DESC   :
*/

// 初始化工具
func InitGameUtils() (err error) {
	// 敏感词
	if err := loadSensitiveWords(); err != nil {
		return err
	}
	return
}

// 初始化工具
func InitServicesUtils() (err error) {
	// 敏感词
	if err := loadSensitiveWords(); err != nil {
		return err
	}
	// 随机昵称
	loadRandomNicknameData()
	return
}

// 初始化工具
func InitLoginUtils() (err error) {
	// 敏感词
	if err := loadSensitiveWords(); err != nil {
		return err
	}
	return
}
