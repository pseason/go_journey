package tools

import (
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

var sensitiveWords []string
// 加载敏感词
func loadSensitiveWords() error {
	path := fmt.Sprintf("%s/sensitive_words/sensitive_words.txt", utils.ExeDir())
	f, err := os.Open(path)
	defer f.Close()
	if err != nil {
		log.Error("加载敏感词失败", nil)
		return err
	}
	sensitiveWords = make([]string,0)
	buf := bufio.NewReader(f)
	for {
		line, _, err := buf.ReadLine()
		if err != nil {
			if err != io.EOF {
				panic(err)
			}
			break
		}
		sensitiveWords = append(sensitiveWords,string(line))
	}
	return nil
}

// 是否有敏感词
func IsSensitiveWords(word string) (isSensitive bool) {
	for _,sensitive := range sensitiveWords {
		if sensitive == "" {
			continue
		}
		split := strings.Split(sensitive, "")
		split1 := strings.Split(word, "")
		isFull := 0
		for _, k := range split {
			for _, text := range split1 {
				if k == text {
					isFull++
					break
				}
			}
		}
		if isFull == len(split) {
			isSensitive = true
			break
		}
	}
	return
}