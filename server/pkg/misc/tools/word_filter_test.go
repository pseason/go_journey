package tools

import (
	"95eh.com/eg/utils"
	"fmt"
	"testing"
)

/*
@Time   : 2021-12-01 17:16
@Author : wushu
@DESC   :
*/

func init() {
	utils.SetExeDir("D:/Develop/WorkSpaceForGoLand/hm-server/output") // wushu
	utils.SetExeDir("~/Develop/ProjectForGoLand/hm-server/output") // wushu-MacOS
	loadSensitiveWords()
}

func TestIsSensitiveWords(t *testing.T) {
	if IsSensitiveWords("习近平啊") {
		fmt.Println("是敏感词")
	} else {
		fmt.Println("不是敏感词")
	}
}
