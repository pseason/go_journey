package tools

import (
	"hm/pkg/misc/tsv"
	"math/rand"
)

/*
@Time   : 2021-11-12 16:54
@Author : wushu
@DESC   :
*/
var (
	_FamilyName []string
	_MaleName   []string
	_FemaleName []string
)

func loadRandomNicknameData() {
	manager := tsv.GetTsvManager(tsv.RandomNickname)
	data := manager.(*tsv.RandomNicknameTsvManager)
	for _, nickname := range data.TsvSlice {
		if len(nickname.FamilyName) != 0 {
			_FamilyName = append(_FamilyName, nickname.FamilyName)
		}
		if len(nickname.MaleName) != 0 {
			_MaleName = append(_MaleName, nickname.MaleName)
		}
		if len(nickname.FemaleName) != 0 {
			_FemaleName = append(_FemaleName, nickname.FemaleName)
		}
		if len(nickname.MaleSingleName) != 0 {
			_MaleName = append(_MaleName, nickname.MaleSingleName)
		}
		if len(nickname.FemaleSingleName) != 0 {
			_FemaleName = append(_FemaleName, nickname.FemaleSingleName)
		}
	}
}

func GenerateRandomNickNames(gender uint8, num int8) (nickname []string) {
	if num > 10 {
		num = 10
	}
	nickname = make([]string, num)

	for i := 0; i < int(num); i++ {
		family := _FamilyName[rand.Intn(len(_FamilyName))]
		nick := ""
		if gender == 1 { // 男
			nick = _MaleName[rand.Intn(len(_MaleName))]
		} else { // 女
			nick = _FemaleName[rand.Intn(len(_FemaleName))]
		}
		nickname[i] = family + nick
	}
	return
}
