package misc

/**
公共配置结构体
节点特有的配置在节点/common/conf.go中定义
*/

type MySQL struct {
	UserName string
	Password string
	Host     string
	Port     int
	DbName   string
}

type Http struct {
	Ip   string
	Port string
	Cert string
	Key    string
}
