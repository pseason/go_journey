package misc

// get elastic search client

import (
	"fmt"
	"github.com/olivere/elastic"
	"github.com/olivere/elastic/config"
)

func GetElasticSearchConn(config *config.Config) *elastic.Client {
	client, err := elastic.NewClientFromConfig(config)
	if err != nil {
		panic(fmt.Sprintf("failed the get elastic search connection: %s", err.Error()))
	}
	return client
}
