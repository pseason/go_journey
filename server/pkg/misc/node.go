package misc

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"hm/pkg/game/svc"
	"strings"
)

type TNode = intfc.TNode

// 服务端的架构节点-应用层
const (
	NodeLogin  TNode = iota
	NodeGlobal       // 暂时没用
	NodeGame
	NodeAi
)

// 服务端的架构节点-服务层：按照服务领域划分的服务节点
const (
	// 大粒度
	NodeServices_Data  TNode = 100 + iota // 数据服务
	NodeServices_Space                    // 空间服务
	// 中粒度-数据
	NodeServices_Character // 角色
	NodeServices_Social    // 社交(包含city、kingdom、friend等模块)
	NodeServices_Data1     // 数据服务集合1(包含邮件、解锁等)
	// 中粒度-空间
	NodeServices_Space1 // 空间服务集合1(包含大世界、新手村等)
	NodeServices_Space2 // 空间服务集合2(包含新手村等副本)
	// 小粒度-数据 (根据svc包中的，一个服务=一个服务节点)
	NodeServices_CharacterInfo // 角色信息
	NodeServices_CharacterAttr // 角色属性
	NodeServices_Mail          // 邮件
	NodeServices_City          // 城邦
	// 小粒度-空间 (根据svc包中的，一个服务=一个服务节点)
	NodeServices_World   // 大世界
	NodeServices_Novice  // 新手村
	NodeServices_CitySpc // 城邦场景
)

var _NodeInfo = map[TNode]NodeInfo{
	NodeLogin:  {"login", false, nil},
	NodeGlobal: {"global", false, nil},
	NodeGame:   {"game", true, nil},
	NodeAi:     {"ai", true, nil},
	// 服务节点：todo 目前只有services_data、services_space节点划分好了。其它的没有配好，目前也用不到，待运行维护时再配置
	// 大粒度
	NodeServices_Data:  {"services_data", true, svc.GetDataServices()},
	NodeServices_Space: {"services_space", true, svc.GetSpaceServices()},
	// 中粒度-数据
	NodeServices_Character: {"services_character", true, []svc.Svc{
		svc.CharacterInfo, svc.CharacterAttr,
	}},
	NodeServices_Social: {"services_social", true, []svc.Svc{
		//svc.Friend,svc.City,svc.Kingdom,
	}},
	NodeServices_Data1: {"services_data1", true, []svc.Svc{
		//...
	}},
	// 中粒度-空间
	NodeServices_Space1: {"services_space1", true, []svc.Svc{
		svc.WorldSpc,
	}},
	NodeServices_Space2: {"services_space2", true, []svc.Svc{
		svc.CitySpc, svc.ArenaSpc,
	}},
	// 小粒度-数据
	NodeServices_CharacterInfo: {"services_characterInfo", true, []svc.Svc{
		svc.CharacterInfo,
	}},
	NodeServices_CharacterAttr: {"services_characterAttr", true, []svc.Svc{
		svc.CharacterAttr,
	}},
	NodeServices_Mail: {"services_mail", true, []svc.Svc{
		svc.Mail,
	}},
	// 小粒度-空间
	NodeServices_World: {"services_world", true, []svc.Svc{
		svc.WorldSpc,
	}},
	NodeServices_CitySpc: {"services_citySpc", true, []svc.Svc{
		svc.CitySpc,
	}},
}

// 节点信息
type NodeInfo struct {
	Name             string
	IsBelongToRegion bool
	Services         []svc.Svc // 服务节点包含的服务
}

var _SvcToNode = map[svc.Svc]TNode{}

func init() {
	for node, info := range _NodeInfo {
		intfc.BindNodeName(node, info.Name, info.IsBelongToRegion)

		//_SvcToNode[]
		//
	}
}

func NameToTNode(name string) (tNode TNode, iError utils.IError) {
	for node, info := range _NodeInfo {
		if strings.EqualFold(info.Name, name) {
			return node, nil
		}
	}
	return 0, utils.NewError("tNode not found", utils.M{"name": name})
}

func GetNodeBySvc(svc svc.Svc) (tNode TNode, iError utils.IError) {
	// 目前只划分了data、space，先写死。todo 应当获取service和开启的service-node的映射，但目前还没有这个映射关系
	if svc >= 10000 && svc <= 29999 {
		return NodeServices_Data, nil
	}
	if svc >= 10000 && svc <= 29999 {
		return NodeServices_Space, nil
	}
	return 0, utils.NewError("the service is unreachable", utils.M{"svc": svc})
}
