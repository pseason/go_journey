package design

// 策划配置
type Design struct {
	Character      Character
	City           City
	Plot           Plot
	CityBuilding   CityBuilding
	Task           Task
	CityBirthPoint CityBirthPoint
	Social         Social
}

// 角色
type Character struct {
	// 年龄上限
	LimitAge int32
}

type City struct {
	Total         int32
	InitPlayerCap int32
}

type CityBirthPoint struct {
	BirthPointX float32
	BirthPointY float32
	BirthPointZ float32
}

type CityBuilding struct {
	CharacterPutEnergy            int32
	EnergyConversionBuildingTotal int32
}

type Plot struct {
	InitId int32
}

type Task struct {
	DailyTaskCap       int32
	DailyTaskResetTime int32
	DailyInitTaskTypes []int32
	WeekInitTaskTypes  []int32
}

type Team struct {
	TeamMemberLimit int32
}

type Social struct {
	BuddyLimit int32
}
