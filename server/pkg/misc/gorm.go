package misc

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"strings"
)

// 获取db连接，如果在过程中出现无效连接，使用 SHOW VARIABLES LIKE '%timeout%' 查看wait_timeout时间是否过短
func GetGormDbConn(conf MySQL) *gorm.DB {
	var dsn string
	if strings.TrimSpace(conf.Password) == "" {
		dsn = fmt.Sprintf("%s@tcp(%s:%d)/%s?charset=utf8mb4", conf.UserName,
			conf.Host, conf.Port, conf.DbName)
	} else {
		dsn = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=true", conf.UserName, conf.Password,
			conf.Host, conf.Port, conf.DbName)
	}
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
		Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		panic(fmt.Sprintf("db connection fail, error : %s", err.Error()))
	}

	//sqlDb, err := db.DB()
	//if err != nil {
	//	panic(fmt.Sprintf("db connection fail, error : %s", err.Error()))
	//}
	//sqlDb.SetMaxIdleConns(20)
	//sqlDb.SetMaxOpenConns(0)
	//sqlDb.SetConnMaxIdleTime(time.Hour)
	//sqlDb.SetConnMaxLifetime(time.Hour)

	return db
}
