package utils

import "sort"

type SortInt32Slice []int32

func (s SortInt32Slice) Len() int           { return len(s) }
func (s SortInt32Slice) Less(i, j int) bool { return s[i] < s[j] }
func (s SortInt32Slice) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s SortInt32Slice) Sort()              { sort.Sort(s) }
