package utils

import (
	"github.com/golang-module/carbon"
	"time"
)

// Author: L
// Created: 2021/11/3
// Describe: common time tools

// GetCurrentMillisecond 获取当前毫秒时间戳
func GetCurrentMillisecond() int64 {
	return carbon.Now().ToTimestampWithMillisecond()
}

// GetCurrentSecond 获取当前秒时间戳
func GetCurrentSecond() int64 {
	return carbon.Now().ToTimestampWithSecond()
}

func GetCurrentTimeDefaultFormat() string {
	return carbon.Now().ToFormatString("Y/m/d H:i:s")
}

func GetCurrentTimeDefaultFormatAndMs() string {
	return carbon.Now().ToFormatString("Y/m/d H:i:s:u")
}

func GetTimestampToDefaultFormat(t int64) string {
	return carbon.CreateFromTimestamp(t).ToFormatString("Y/m/d H:i:s")
}

//获取当天0点和24点时间戳
//beginTimeNum  0点
//endTimeNum  23点.59.59.99
func GetTimestamp() (beginTimeNum, endTimeNum int64) {
	timeStr := time.Now().Format("2006-01-02")
	t, _ := time.ParseInLocation("2006-01-02", timeStr, time.Local)
	beginTimeNum = t.Unix()
	endTimeNum = beginTimeNum + 86399
	return beginTimeNum * 1000, endTimeNum * 1000
}

//获取本周一起止时间
func GetFirstDateOfWeek() (weekMonday int64) {
	now := time.Now()

	offset := int(time.Monday - now.Weekday())
	if offset > 0 {
		offset = -6
	}

	weekStartDate := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local).AddDate(0, 0, offset)
	weekMonday = weekStartDate.UnixNano() / 1e6
	return
}

//GetNextDayTimestamp 获取当日0点时间戳(秒)
func GetCurrentDayTimestamp() int64 {
	t, _ := time.ParseInLocation("2006-01-02", time.Now().Format("2006-01-02"), time.Local)
	return t.Unix()
}