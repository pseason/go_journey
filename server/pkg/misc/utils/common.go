package utils

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"hm/pkg/misc/utils/weight_random"
	"math/rand"
	"runtime"
	"strconv"
	"strings"
)

// Author: L
// Created: 2020/12/1
// Describe: 共用函数

// IsOrmNotFound 是否是ORM的未找到错误
func IsOrmNotFound(err error) bool {
	return errors.Is(err, gorm.ErrRecordNotFound)
}

// IsOrmNotFoundFunc 判断ORM未找到错误并执行回调
func IsOrmNotFoundFunc(err error, hasErrFuc func(), NoHasErrFunc func()) {
	if err != nil {
		if IsOrmNotFound(err) {
			if NoHasErrFunc != nil {
				NoHasErrFunc()
			}
		} else {
			if hasErrFuc != nil {
				hasErrFuc()
			}
		}
	}
}

// SliceContain 切片包含：String类型
func SliceContain(s []string, find string) bool {
	for _, v := range s {
		if v == find {
			return true
		}
	}
	return false
}

// SliceContainInt32 切片包含：Int32类型
func SliceContainInt32(s []int32, find int32) bool {
	for _, v := range s {
		if v == find {
			return true
		}
	}
	return false
}

// PropSlice2ToMap Prop切片转Map
func PropSlice2ToMap(d [][]int32) map[int32]int32 {
	res := make(map[int32]int32)
	for _, v := range d {
		res[v[0]] = v[1]
	}
	return res
}

// StringSliceToInt32 字符串切片转Int32
func StringSliceToInt32(target []string) []int32 {
	res := make([]int32, 0, len(target))
	for _, v := range target {
		if v == "" {
			continue
		}
		n, err := strconv.Atoi(v)
		if err == nil {
			res = append(res, int32(n))
		}
	}
	return res
}

// RemoveSliceEmptyString 删除字符串切片中的空字符
func RemoveSliceEmptyString(target []string) []string {
	r := make([]string, 0, len(target))
	for _, v := range target {
		if v != "" {
			r = append(r, v)
		}
	}
	return r
}

// FuncNotNilCall 用于异步Func批量调用
func FuncNotNilCall(fs ...func()) {
	if len(fs) > 0 {
		for _, f := range fs {
			if f != nil {
				f()
			}
		}
	}
}

// SliceProbabilityGenMany 切片概率生成多个 data格式：[[ID,数量,机率]...]
func SliceProbabilityGenMany(data [][]int32) (res map[int32]int32) {
	if len(data) <= 0 {
		return
	}
	res = make(map[int32]int32)
	for _, d := range data {
		if len(d) != 3 {
			continue
		}
		r := GetRandomInt32(100)
		if r <= d[2] {
			res[d[0]] = d[1]
		}
	}
	return
}

// SliceProbabilityGenWeightOne 物品权重概率生成单个 data格式：[[ID,数量,权重比]...]
//返回：索引
func SliceProbabilityGenWeightOne(data [][]int32) (res int) {
	if len(data) <= 0 {
		return
	}
	mu := make([]*weight_random.WeightUnit, 0, len(data))
	for i, d := range data {
		w := float64(d[2]) / float64(100)
		mu = append(mu, &weight_random.WeightUnit{Id: i, Weight: w})
	}
	return weight_random.GetWeightRandom(mu)
}

// GetRandomInt32 返回[0,n]的一个随机数
func GetRandomInt32(n int32) int32 {
	return rand.Int31n(n + 1)
}

// GetRandomInt 返回[0,n]的一个随机数
func GetRandomInt(n int) int {
	return rand.Intn(n + 1)
}

// Int64ToBytes 整形转换成字节
func Int64ToBytes(n int64) []byte {
	bytesBuffer := bytes.NewBuffer([]byte{})
	binary.Write(bytesBuffer, binary.BigEndian, n)
	return bytesBuffer.Bytes()
}

// BytesToInt64 字节转换成整形
func BytesToInt64(b []byte) (res int64) {
	bytesBuffer := bytes.NewBuffer(b)
	binary.Read(bytesBuffer, binary.BigEndian, &res)
	return
}

func GetGoroutineId() int64 {
	var (
		buf [64]byte
		n   = runtime.Stack(buf[:], false)
		stk = strings.TrimPrefix(string(buf[:n]), "goroutine ")
	)

	idField := strings.Fields(stk)[0]
	id, err := strconv.Atoi(idField)
	if err != nil {
		panic(fmt.Errorf("can not get goroutine id: %v", err))
	}

	return int64(id)
}

func Md5Encode(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

// Slice2ToMap 二维切片转换为map，第0个元素为主键，第1个元素为数值
func Slice2ToMap(data [][]int32) map[int32]int32 {
	res := make(map[int32]int32)
	for _, d := range data {
		if len(d) != 2 {
			continue
		}
		res[d[0]] += d[1]
	}
	return res
}

// NumberListConvert 数位切片转换位运算数字
func NumberListConvert(numberList ...int32) (number int64) {
	if len(numberList) > 0 {
		if numberList[0] == -1 {
			number = -1
		}else if numberList[0] == 0 {
			number = 0
		}else{
			for _,index := range numberList {
				number |= 1 << index
			}
		}
	}
	return number
}