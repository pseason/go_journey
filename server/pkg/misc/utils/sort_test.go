package utils

import (
	"fmt"
	"testing"
)

func TestSortInt32(t *testing.T) {
	slice := SortInt32Slice([]int32{2, 3, 4, 5, 110, 23, 0})
	slice.Sort()
	fmt.Println(slice)
}
