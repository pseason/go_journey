package utils

import (
	"errors"
	"math"
)

/*
@Time   : 2022-01-12 15:24
@Author : wushu
@DESC   :
*/

// 把能够转为int32的interface转为int32
func ToInt32(i interface{}) (int32, error) {
	switch v := i.(type) {
	case int8:
		return int32(v), nil
	case uint8:
		return int32(v), nil
	case int16:
		return int32(v), nil
	case uint16:
		return int32(v), nil
	case int32:
		return v, nil
	case uint32:
		if v <= math.MaxInt32 {
			return int32(v), nil
		}
	case int:
		if v <= math.MaxInt32 {
			return int32(v), nil
		}
	case uint:
		if v <= math.MaxInt32 {
			return int32(v), nil
		}
	}
	return 0, errors.New("unsupported types")
}

// 把能够转为int64的interface转为int64
func ToInt64(i interface{}) (int64, error) {
	switch v := i.(type) {
	case int8:
		return int64(v), nil
	case uint8:
		return int64(v), nil
	case int16:
		return int64(v), nil
	case uint16:
		return int64(v), nil
	case int32:
		return int64(v), nil
	case uint32:
		if v <= math.MaxInt32 {
			return int64(v), nil
		}
	case int64:
		return v, nil
	case uint64:
		if v <= math.MaxInt64 {
			return int64(v), nil
		}
	case int:
		return int64(v), nil
	case uint:
		if v <= math.MaxInt64 {
			return int64(v), nil
		}
	}
	return 0, errors.New("unsupported types")
}
