package utils

import "math/rand"

/*
@Time   : 2021-12-09 20:38
@Author : wushu
@DESC   : 随机数
*/

//  RandInt32
//  返回[0,n]的一个随机数
func RandInt32(n int32) int32 {
	return rand.Int31n(n + 1)
}

//  RandRangeInt32
//  返回[m,n]的一个随机数
func RandRangeInt32(m, n int32) int32 {
	return m + RandInt32(n-m)
}
