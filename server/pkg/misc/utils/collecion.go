package utils

import (
	"fmt"
	"reflect"
	"strconv"
)

/*
@Time   : 2021-12-03 13:57
@Author : wushu
@DESC   : 集合操作
*/

//  SliceRemove
//  删除切片中符合条件的元素
//  @slice：切片的地址
//  @remove: 不可通过判断index来删除元素(因为每次for循环i不一定都会改变)，应当根据slice[i]来进行判断和删除元素
//  此方法方便但效率慢
func SliceRemove(slice interface{}, remove func(i int) bool) {
	elem := reflect.ValueOf(slice).Elem()
	if !elem.CanSet() {
		panic("unsupported type, slice should be like：&[]int{1,2,3...}")
	}
	for i := 0; i < elem.Len(); {
		if remove(i) {
			elem.Set(reflect.AppendSlice(elem.Slice(0, i), elem.Slice(i+1, elem.Len())))
		} else {
			i++
		}
	}
}

//  MergeMapSlice
//  合并两个map切片的数值
//  需要：两个map相同k的切片长度一致
//  场景：返回的数据只读
func MergeMapSlice(ms1, ms2 map[int32][]float32) (res map[int32][]float32) {
	if len(ms2) == 0 {
		return ms1
	}
	res = make(map[int32][]float32)
	for ms1k, ms1v := range ms1 {
		res[ms1k] = make([]float32, len(ms1v))
		copy(res[ms1k], ms1v)
	}

	for ms2k, ms2v := range ms2 {
		if res[ms2k] == nil {
			res[ms2k] = ms2v // 返回后只读，这里不需要深拷贝
		} else {
			for i, v := range ms2v {
				res[ms2k][i] += v
			}
		}
	}
	return
}

//  MergeMapSliceDeep
//  深拷贝合并两个map切片的数值
//  需要：两个map相同k的切片长度一致
//  场景：返回的数据可读可写
func MergeMapSliceDeep(ms1, ms2 map[int32][]float32) (res map[int32][]float32) {
	res = make(map[int32][]float32)
	for ms1k, ms1v := range ms1 {
		res[ms1k] = make([]float32, len(ms1v))
		copy(res[ms1k], ms1v)
	}
	for ms2k, ms2v := range ms2 {
		if res[ms2k] == nil {
			res[ms2k] = make([]float32, len(ms2v))
			copy(res[ms2k], ms2v)
		} else {
			for i, v := range ms2v {
				//res[ms2k][i] += v
				val, err := strconv.ParseFloat(fmt.Sprintf("%.2f", res[ms2k][i]+v), 64) // 浮点误差处理
				if err != nil {
					panic(err)
				}
				res[ms2k][i] = float32(val)
			}
		}
	}
	return
}
