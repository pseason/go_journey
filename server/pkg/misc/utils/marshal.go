package utils

import (
	"bytes"
	"encoding/gob"
	jsoniter "github.com/json-iterator/go"
)

// Author: L
// Created: 2020/12/1
// Describe: none

func GobMarshal(t interface{}) (data []byte, err error) {
	buffer := &bytes.Buffer{}
	e := gob.NewEncoder(buffer)
	err = e.Encode(t)
	return buffer.Bytes(), err
}

func GobUnmarshal(data []byte, t interface{}) (err error) {
	buffer := &bytes.Buffer{}
	buffer.Write(data)
	e := gob.NewDecoder(buffer)
	return e.Decode(t)
}

func EntityMarshal(t interface{}) (data []byte, err error) {
	return jsoniter.Marshal(t)
}

func EntityUnmarshal(data []byte, t interface{}) (err error) {
	return jsoniter.Unmarshal(data, t)
}
