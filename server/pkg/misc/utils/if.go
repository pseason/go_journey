package utils

import (
	"database/sql"
	"gorm.io/gorm"
)

/*
@Time   : 2022-01-12 15:32
@Author : wushu
@DESC   :
*/

// 是否是事务会话的连接
func IsTxConn(conn *gorm.DB) bool {
	if conn == nil {
		return false
	}
	if _, ok := conn.Statement.ConnPool.(*sql.Tx); !ok {
		return false
	} else {
		return true
	}
}
