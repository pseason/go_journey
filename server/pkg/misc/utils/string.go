package utils

import (
	"strings"
	"unicode"
)

/*
@Time   : 2021-12-09 20:37
@Author : wushu
@DESC   : 字符串操作
*/

//  Capitalize
//  返回传入字符串的首字母大写
func Capitalize(str string) string {
	strRune := []rune(str)
	if strRune[0] >= 97 && strRune[0] <= 122 {
		strRune[0] -= 32
	}
	return string(strRune)
}

// SnakeCase 转蛇形命名
func SnakeCase(str string) string {
	var snake string
	runes := []rune(str)
	for i, v := range runes {
		if i != 0 {
			if unicode.IsUpper(v) && unicode.IsLower(runes[i-1]) { // Lower_Upper
				snake = snake + "_"
				goto Join
			}
			if unicode.IsUpper(v) && (i == len(runes)-1 || unicode.IsLower(runes[i+1])) { // _UpperLower
				snake = snake + "_"
				goto Join
			}
		}
	Join:
		snake = snake + string(v)
	}
	return strings.ToLower(snake)
}

// BigHump 转小驼峰命名
func SmallHump(str string) string {
	runes := []rune(BigHump(str))
	runes[0] = unicode.ToLower(runes[0])
	return string(runes)
}

// BigHump 转大驼峰命名
func BigHump(str string) string {
	var bigHump string
	runes := []rune(str)
	// 全部转小写、下划线后的一个字符转大写
	for i := 0; i < len(runes); i++ {
		v := runes[i]
		s := string(runes[i])
		if i == 0 && unicode.IsLetter(v) {
			bigHump = bigHump + strings.ToUpper(s)
			continue
		}
		if s == "_" {
			i++
			s = string(runes[i])
			bigHump = bigHump + strings.ToUpper(s)
			continue
		}
		bigHump = bigHump + strings.ToLower(s)
	}
	return bigHump
}
