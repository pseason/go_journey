package utils

import "os"

/*
@Time   : 2021-12-09 20:37
@Author : wushu
@DESC   : 文件操作
*/

// MkDirAll 创建目录
func MkDirAll(dir string) (err error) {
	if _, err = os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, os.ModePerm)
	}
	return
}
