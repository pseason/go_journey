package utils

import "strconv"

/*
@Time   : 2021-12-13 14:14
@Author : wushu
@DESC   :
*/

func ParseInt32(str string) (i int32, err error) {
	parseInt, err := strconv.ParseInt(str, 10, 32)
	return int32(parseInt), err
}

func ParseInt64(str string) (i int64, err error) {
	return strconv.ParseInt(str, 10, 64)
}

func ParseUint16(str string) (i uint16, err error) {
	parseInt, err := strconv.ParseUint(str, 10, 16)
	return uint16(parseInt), err
}
