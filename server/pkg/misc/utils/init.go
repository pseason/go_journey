package utils

import (
	"math/rand"
	"time"
)

/*
@Time   : 2021-12-09 20:38
@Author : wushu
@DESC   :
*/

func init() {
	rand.Seed(time.Now().UnixNano())
}
