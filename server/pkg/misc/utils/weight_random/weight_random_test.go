package weight_random

import (
	"fmt"
	"testing"
)

// Author: L
// Created: 2021/1/13
// Describe: none

func TestGetWeightRandom(t *testing.T) {
	mu := []*WeightUnit{
		{Id: 1, Weight: 10},
		{Id: 2, Weight: 10},
		{Id: 3, Weight: 20},
		{Id: 4, Weight: 20},
		{Id: 5, Weight: 20},
		{Id: 6, Weight: 20},
	}
	r := map[int]int{
		mu[0].Id: 0,
		mu[1].Id: 0,
		mu[2].Id: 0,
		mu[3].Id: 0,
		mu[4].Id: 0,
		mu[5].Id: 0,
	}
	for i := 0; i < 100000; i++ {
		randomId := GetWeightRandom(mu)
		r[mu[randomId].Id]++
	}

	fmt.Println(r)

}
