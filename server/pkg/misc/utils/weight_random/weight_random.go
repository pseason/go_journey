package weight_random

import "math/rand"

// Author: L
// Created: 2021/1/13
// Describe: 权重随机

type WeightUnit struct {
	Id     int
	Weight float64
}

// 获取权重随机，返回索引
func GetWeightRandom(wu []*WeightUnit) (randomIndex int) {
	sumWeight := float64(0)
	for _, w := range wu {
		sumWeight += w.Weight
	}
	randomNumber := rand.Float64()
	var d1, d2 float64
	for i := 0; i < len(wu); i++ {
		d2 += wu[i].Weight / sumWeight
		if i != 0 {
			d1 += wu[i-1].Weight / sumWeight
		}
		if randomNumber >= d1 && randomNumber <= d2 {
			randomIndex = i
			break
		}
	}
	return
}

func GetWeightRandomId(wu []*WeightUnit) (randomId int) {
	return wu[GetWeightRandom(wu)].Id
}
