package utils

// If3 模拟的三目运算
func If3(condition bool, okVal, falseVal interface{}) interface{} {
	if condition {
		return okVal
	}
	return falseVal
}
