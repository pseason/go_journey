package misc

/**
用户角色
每个连接都包含了`访客`角色
运维人员的角色包含 `玩家`和`运维`
*/

const (
	RoleGuest int64 = 0 //访客 0
)
const (
	RolePlayer int64 = 1 << iota //玩家
	RoleOps                      //运维
)
