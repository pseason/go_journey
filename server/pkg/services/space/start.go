package space

import (
	"95eh.com/eg/app"
	"95eh.com/eg/asset"
	"95eh.com/eg/codec"
	"95eh.com/eg/discovery"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/scene"
	"95eh.com/eg/timer"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
	"errors"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/space/common"
	"hm/pkg/services/space/msg_spc"
	"math/rand"
	"time"
)

func Start() {
	// 设置随机种子
	rand.Seed(time.Now().UnixNano())
	// 加载路径
	utils.ExeDir()

	//加载配置文件
	conf, err := common.LoadConf("",
		"common", "common_dev",
		"region", "region_dev",
		"region_services_space", "region_services_space_dev",
		"design")
	if err != nil {
		panic(err.Error())
	}
	err = utils.StartConsul(conf.Consul)
	if err != nil {
		panic(err.Error())
	}

	//serviceCodec := codec.NewJsonCodec()
	// 与服务通信编解码器使用msgpack
	serviceCodec := codec.NewMsgPackCodec()

	if conf.Debug {
		dur := (time.Minute * 30).Milliseconds()
		intfc.ResponseTimeoutDur = dur
		intfc.TcpDeadlineDur = dur
	}

	//启动Redis
	regionRedis := misc.GetRedisConn(conf.Redis)
	common.LoadRedis(conf.Redis)

	// 加载服务的场景
	var _NodeServices []misc.TNode
	for _, nodeStr := range conf.SpaceServices {
		nodeSvc, err := misc.NameToTNode(nodeStr)
		if err != nil {
			panic(err)
		}
		_NodeServices = append(_NodeServices, nodeSvc)
	}
	if len(_NodeServices) == 0 {
		panic(errors.New("invalid services node"))
	}

	sm := make(map[misc.TNode]struct{})
	for _, service := range _NodeServices {
		sm[service] = struct{}{}
	}

	disConf := conf.Discovery
	app.Start(
		log.NewMLogger(log.Loggers(log.NewConsole(intfc.TLogDebug)), nil),
		asset.NewMAsset(),
		timer.NewMTimer(regionRedis, nil),
		worker.NewMWorker(),
		scene.NewMScene(intfc.ModuleOptions(
			intfc.BeforeModuleStart(func() {
				common.InitActorEvent()
				common.InitActorState()
				common.InitSceneState()
			}),
		)),
		discovery.NewMDiscovery(discovery.Nodes(_NodeServices...), disConf.NodeId, disConf.RegionId, serviceCodec, regionRedis,
			intfc.ModuleOptions(
				intfc.BeforeModuleStart(func() {
					tsv.LoadNodeTsv(misc.NodeServices_Space)
					msg_spc.InitServiceCodec(app.Discovery())
					common.InitSpaceService()
					common.InitSpaceEventProcessor()
					_, ok := sm[misc.NodeServices_CitySpc]
					if ok {
						common.InitCityService()
						common.InitCity()
					}
				}),
				intfc.AfterModuleStart(func() {
					app.Discovery().WatchNodes(misc.NodeGame, misc.NodeAi)
				}),
				intfc.BeforeModuleDispose(func() {
					_, ok := sm[misc.NodeServices_CitySpc]
					if ok {
						common.DisposeCity()
					}
				}),
			),
			discovery.Addr(disConf.Ip, disConf.Port),
			discovery.Weight(disConf.Weight)),
		scene.NewSceneCache(regionRedis, nil),
	)
	utils.BeforeExit("stop space", app.Dispose)
}
