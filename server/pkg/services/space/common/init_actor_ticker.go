package common

import (
	"95eh.com/eg/intfc"
)

func onActorTickMoving(ms int64, actor intfc.IActor) {
	com, _ := actor.GetComponent(Com_Transform)
	tnf := com.(*ActorComTransform)
	if !tnf.Moving() {
		return
	}
	tnf.TickMove()
}
