package common

type EveResourceStaticStatus struct {
	ResId int64
	Empty int32
}

func NewEvtResourceStaticStatus(ResId int64,Empty int32) *EveResourceStaticStatus {
	return &EveResourceStaticStatus{
		ResId: ResId,
		Empty: Empty,
	}
}