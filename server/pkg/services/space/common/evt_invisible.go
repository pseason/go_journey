package common

import "95eh.com/eg/intfc"

func NewEvtInvisible(actor intfc.IActor) *EvtInvisible {
	return &EvtInvisible{
		Id: actor.Id(),
	}
}

type EvtInvisible struct {
	Id int64
}
