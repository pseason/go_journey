package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
)

type TBounding = int16

type ActorComBounding struct {
	scene.ActorComponent
	// 包围盒类型
	typ TBounding
	// 包围盒数据
	data interface{}
}

func (b *ActorComBounding) BoundingType() TBounding {
	return b.typ
}

func (b *ActorComBounding) SetBoundingType(typ TBounding) {
	b.typ = typ
}

func (b *ActorComBounding) Data() interface{} {
	return b.data
}

func (b *ActorComBounding) SetData(data interface{}) {
	b.data = data
}

func (b *ActorComBounding) Type() intfc.TActorComponent {
	return Com_Bounding
}

func (b *ActorComBounding) Start() utils.IError {
	return app.SceneCache().LoadActorComponent(b.Actor().Id(), b)
}

func (b *ActorComBounding) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(b.Actor().Id(), b)
}