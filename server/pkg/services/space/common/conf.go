package common

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"github.com/go-redis/redis/v8"
	consulApi "github.com/hashicorp/consul/api"
	"hm/pkg/misc"
	"hm/pkg/misc/design"
	"hm/pkg/services/space/common/unityai"
)

var (
	_Conf = &Config{}
)

func LoadConf(root string, paths ...string) (conf *Config, err error) {
	err = utils.StartLocalConf(root, _Conf, paths...)
	return _Conf, err
}

type Config struct {
	Debug         bool
	Discovery     intfc.DiscoveryConf
	Consul        *consulApi.Config
	Redis         *redis.Options
	Design        design.Design // 策划配置
	SpaceServices []string
	Space         Space
}

type Space struct {
	City City
}

type Scene struct {
	Width      int32 //地图宽
	Length     int32 //地图长
	TileSize  int32 //瓦片宽
	EventDis   int32 //事件圈数
	VisionDis  int32 //视野圈数
	NavMesh    *unityai.NavMeshManager
}

type City struct {
	Scene  Scene
	Id     CityId
}

type CityId struct {
	Start int64
	End   int64
}

var (
	AllSceneTypes = []misc.TScene{
		misc.SceneCity,
	}
)
