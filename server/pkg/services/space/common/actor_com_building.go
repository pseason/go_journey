package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/model/enum/building_e"
)

type ActorComBuilding struct {
	scene.ActorComponent
	BuildingType  int32 //建筑类型
	Level         int32 //等级
	BuildingState int32 //建筑状态
	TeamId        int64 //队伍id
	FactionId     int64 //阵营
}

func (b *ActorComBuilding) Type() intfc.TActorComponent {
	return Com_Building
}

func (b *ActorComBuilding) OnEnterScene() {
}

func (b *ActorComBuilding) OnExitScene() {
}

func (b *ActorComBuilding) BuildingInfoChange(upMap map[int32]int32) {
	for buildingInfo, v := range upMap {
		switch building_e.BuildingInfo(buildingInfo) {
		case building_e.BuildingInfoState:
			b.BuildingState = v
		case building_e.BuildingInfoLv:
			b.Level = v
		}
	}
	c, _ := b.Actor().GetComponent(Com_Transform)
	transform := c.(*ActorComTransform)
	tx, ty := transform.tileX, transform.tileY
	sceneType := b.Actor().Scene().Type()
	evt := NewEvtBuildingInfo(b.Actor().Id(), upMap)
	tags, _, e := GetSceneVisionTileAroundTags(sceneType, tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}
	b.Actor().Scene().DispatchActorEventByTags(Evt_BuildingInfoChange, evt, tags...)

}

func (b *ActorComBuilding) Start() utils.IError {
	return app.SceneCache().LoadActorComponent(b.Actor().Id(), b)
}

func (b *ActorComBuilding) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(b.Actor().Id(), b)
}
