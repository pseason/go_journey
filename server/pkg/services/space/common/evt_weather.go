package common

type EveSceneWeatherInfo struct {
	UpMap
}
func NewEveSceneWeatherInfo(Sun,Snow,Rain,Wind,Shade float64) *EveSceneWeatherInfo {
	return &EveSceneWeatherInfo{
		UpMap:UpMap{
			Sun:Sun,
			Snow:Snow,
			Rain:Rain,
			Wind:Wind,
			Shade:Shade,
		},
	}
}
