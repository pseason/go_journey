package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"hm/pkg/game/msg_gm"
	"io/ioutil"
)


// ActorComResourceStatic 静态资源组件
type ActorComResourceStatic struct {
	ActorComResource
}

func LoadStaticResourceList(appPath, name string) (list *msg_gm.ServerMapRes, err error) {
	list = &msg_gm.ServerMapRes{}
	p := appPath + "/resource/" + name + ".bytes"
	var data []byte
	data, err = ioutil.ReadFile(p)
	if err != nil {
		return
	}
	err = list.Unmarshal(data)
	return
}

func (s *ActorComResourceStatic) TestGather(actorId, cityId, kingdomId int64) bool {
	return false
}

func (s *ActorComResourceStatic) Rebirth() {
	//rebirthDur := int64(180000) // int64(s.csv.RebirthDuration)
	rebirthDur := int64(30000) // int64(s.csv.RebirthDuration)
	app.Timer().After(rebirthDur,func() {
		_ = app.Scene().DoAction(func() {
			scene, err := app.Scene().GetScene(s.Actor().Scene().Type(), s.Actor().Scene().Id())
			if err != nil {
				app.Log().Error(err.Error(), err.Params())
				return
			}
			_ = scene.DoAction(func() {
				s.resetRemain()
				s.dispatchStatusChange()
			})
		})
	})
}

func (s *ActorComResourceStatic) dispatchStatusChange() {
	var isFalse int32 = 0
	if s.TestEmpty() {
		isFalse = 1
	}
	log.Debug("dispatch resource static status change", utils.M{"empty":isFalse})
	com, _ := s.Actor().GetComponent(Com_Transform)
	tnf := com.(*ActorComTransform)
	sceneType := s.Actor().Scene().Type()
	//广播静态资源状态
	event := NewEvtResourceStaticStatus(s.Actor().Id(),isFalse)

	tags, _, e := GetSceneEventTileAroundTags(sceneType, tnf.tileX, tnf.tileY)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}
	s.Actor().Scene().DispatchActorEventByTags(Evt_ResourceStaticStatus, event, tags...)
}


func (s *ActorComResourceStatic) Start() utils.IError {
	err := app.SceneCache().LoadActorComponent(s.Actor().Id(), s)
	if !s.GetCsv() {
		return utils.NewError("resource load tsv failed",utils.M{"r":s.TplId})
	}
	return err
}

func (s *ActorComResourceStatic) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(s.Actor().Id(), s)
}
