package common

import (
	"95eh.com/eg/intfc"
)

func NewEvtMoveStart(actor intfc.IActor) *EvtMoveStart {
	com, _ := actor.GetComponent(Com_Transform)
	tnf := com.(*ActorComTransform)
	return &EvtMoveStart{
		Id:        actor.Id(),
		Type:      actor.Type(),
		PositionX: tnf.Position.X,
		PositionY: tnf.Position.Y,
		PositionZ: tnf.Position.Z,
		ForwardX:  tnf.Forward.X,
		ForwardY:  tnf.Forward.Y,
		ForwardZ:  tnf.Forward.Z,
	}
}

type EvtMoveStart struct {
	Id        int64
	Type      intfc.TActor
	PositionX float32
	PositionY float32
	PositionZ float32
	ForwardX  float32
	ForwardY  float32
	ForwardZ  float32
}

type EvtMoveStop struct {
	Id        int64
	Type      intfc.TActor
	PositionX float32
	PositionY float32
	PositionZ float32
}

type EvtMove struct {
	Id        int64
	Type      intfc.TActor
	PositionX float32
	PositionY float32
	PositionZ float32
	ForwardX  float32
	ForwardY  float32
	ForwardZ  float32
}

type EvtChangePos struct {
	Id        int64
	PositionX float32
	PositionY float32
	PositionZ float32
	ForwardX  float32
	ForwardY  float32
	ForwardZ  float32
}
