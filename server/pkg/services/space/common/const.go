package common

import (
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
)

type (
	TComponent           = intfc.TActorComponent
	TFiled               = data.TField
	TActor               = intfc.TActor
	TActorEvent = intfc.TActorEvent
)

const (
	ActorPlayer intfc.TActor = iota
	ActorMonster
	ActorBuilding
	ActorResource
)

const (
	ComNil TComponent = iota
	Com_Transform
	Com_Life
	Com_Bounding
	Com_Equip
	Com_Building
	Com_Resource
	Com_Resource_Static
	Com_Gather
)

const (
	Evt_Visible TActorEvent = iota
	Evt_Invisible
	Evt_MoveStart
	Evt_Move
	Evt_MoveStop
	Evt_ChangePos
	Evt_ForwardChange
	Evt_SkillCast
	Evt_BuildingInfoChange
	Evt_GatherChange
	Evt_ResourceStaticStatus
	Evt_Build_Action
	Evt_Weather
	Evt_Actor_Death
)
