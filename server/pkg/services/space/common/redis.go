package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"context"
	"github.com/go-redis/redis/v8"
	"hm/pkg/misc"
	"hm/pkg/services/data/model"
	"strconv"
)

var (
	_Redis *redis.Client
)

func LoadRedis(config *redis.Options) *redis.Client {
	_Redis = misc.GetRedisConn(config)
	return _Redis
}

var (
	_Ctx = context.Background()
)

func GetNodeWithSceneType(sceneType misc.TScene, sceneId int64) (nodeType misc.TNode) {
	switch sceneType {
	case misc.SceneCity:
		nodeType = misc.NodeServices_CitySpc
		return
	default:
		panic("not support scene type")
	}
}

func GetRedisActorDataKey(actorId int64) string {
	return "actor:" + strconv.FormatInt(actorId, 10)
}

func GetRedisSceneDataKey(sceneType misc.TScene, sceneId int64) string {
	return "scene:" + strconv.FormatInt(int64(sceneType), 10) + ":" + strconv.FormatInt(sceneId, 10)
}

func GetRedisSceneNodeTypeKey(nodeType misc.TNode) string {
	return "scene:nodeType:" + strconv.FormatInt(int64(nodeType), 10)
}

// SetRedisActorSceneData 设置Actor的场景类型和Id
func SetRedisActorSceneData(actorId int64, sceneType misc.TScene, sceneId int64) utils.IError {
	err := app.Discovery().Redis().HSet(_Ctx, GetRedisActorDataKey(actorId), "SceneType", int(sceneType), "SceneId", sceneId).Err()
	if err != nil {
		return utils.NewError(err.Error(), utils.M{
			"actor id":   actorId,
			"scene type": sceneType,
			"scene id":   sceneId,
		})
	}
	return nil
}

// GetRedisActorSceneData 通过ActorId获取场景类型和Id
func GetRedisActorSceneData(actorId int64) (sceneType misc.TScene, sceneId int64, e utils.IError) {
	result, err := app.Discovery().Redis().HMGet(_Ctx, GetRedisActorDataKey(actorId), "SceneType", "SceneId").Result()
	if err != nil {
		e = utils.NewError(err.Error(), utils.M{
			"actor id": actorId,
		})
		return
	}
	if result[0] == nil || result[1] == nil {
		e = utils.NewError("empty result", utils.M{
			"actor id": actorId,
			"result":   result,
		})
		return
	}
	st, err := strconv.ParseInt(result[0].(string), 10, 64)
	if err != nil {
		e = utils.NewError(err.Error(), utils.M{
			"actor id": actorId,
		})
		return
	}
	sceneType = misc.TScene(st)
	sceneId, err = strconv.ParseInt(result[1].(string), 10, 64)
	if err != nil {
		e = utils.NewError(err.Error(), utils.M{
			"actor id": actorId,
		})
		return
	}
	return
}

// IncrRedisSceneActorCount 增加场景的Actor计数,空间服调用
func IncrRedisSceneActorCount(sceneType misc.TScene, sceneId int64, actorType intfc.TActor, count int64) utils.IError {
	err := app.Discovery().Redis().HIncrBy(_Ctx, GetRedisSceneDataKey(sceneType, sceneId), actorType.String(), count).Err()
	if err != nil {
		return utils.NewError(err.Error(), utils.M{
			"actor type": actorType,
			"scene type": sceneType,
			"scene id":   sceneId,
			"count":      count,
		})
	}
	return nil
}

// GetRedisSceneActorCount 获取场景Actor计数
func GetRedisSceneActorCount(sceneType misc.TScene, sceneId int64, actorType intfc.TActor) (count int64, e utils.IError) {
	r, err := app.Discovery().Redis().HGet(_Ctx, GetRedisSceneDataKey(sceneType, sceneId), actorType.String()).Result()
	if err != nil {
		e = utils.NewError(err.Error(), utils.M{
			"scene type": sceneType,
			"scene id":   sceneId,
			"actor type": actorType,
		})
		return
	}
	count, err = strconv.ParseInt(r, 10, 64)
	if err != nil {
		e = utils.NewError(err.Error(), utils.M{
			"scene type": sceneType,
			"scene id":   sceneId,
			"actor type": actorType,
		})
	}
	return
}

// SetRedisSceneData 写入场景基础数据到redis
func SetRedisSceneData(sceneType misc.TScene, sceneId int64, nodeType misc.TNode, nodeId uint16) utils.IError {
	err := app.Discovery().Redis().HSet(_Ctx, GetRedisSceneDataKey(sceneType, sceneId), map[string]interface{}{
		"NodeType": uint16(nodeType),
		"NodeId":   nodeId,
	}).Err()
	if err != nil {
		return utils.NewError(err.Error(), utils.M{
			"node type":  nodeType,
			"scene type": sceneType,
			"scene id":   sceneId,
		})
	}
	return nil
}

// DelRedisSceneData 删除场景数据
func DelRedisSceneData(sceneType misc.TScene, sceneId int64) utils.IError {
	e := app.Discovery().Redis().Del(_Ctx, GetRedisSceneDataKey(sceneType, sceneId)).Err()
	if e != nil {
		return utils.NewError(e.Error(), nil)
	}
	return nil
}

// GetRedisSceneData 从redis读取场景数据
func GetRedisSceneData(sceneType misc.TScene, sceneId int64) (nodeType misc.TNode, nodeId uint16, e utils.IError) {
	result, err := app.Discovery().Redis().HGetAll(_Ctx, GetRedisSceneDataKey(sceneType, sceneId)).Result()
	if err != nil {
		e = utils.NewError(err.Error(), utils.M{
			"scene type": sceneType,
			"scene id":   sceneId,
		})
		return
	}

	i, err := strconv.ParseInt(result["NodeType"], 10, 64)
	if err != nil {
		e = utils.NewError(err.Error(), utils.M{
			"scene type": sceneType,
			"scene id":   sceneId,
		})
		return
	}
	nodeType = misc.TNode(i)

	i, err = strconv.ParseInt(result["NodeId"], 10, 64)
	if err != nil {
		e = utils.NewError(err.Error(), utils.M{
			"scene type": sceneType,
			"scene id":   sceneId,
		})
		return
	}
	nodeId = uint16(i)
	return
}

func SetRedisSceneId(sceneId []int64) utils.IError {
	err := app.Discovery().Redis().Set(_Ctx, "start_scene_id", model.GetTeamMemberString(sceneId...), 0).Err()
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"sceneId": sceneId})
	}
	return nil
}
