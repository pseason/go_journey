package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	"hm/pkg/services/space/msg_spc"
)

func InitSpaceEventProcessor() {
	scn := app.Scene()
	scn.BindActorEventProcessor(Evt_MoveStart, onEvtActorMoveStart, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_MoveStop, onEvtActorMoveStop, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_Move, onEvtActorMove, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_ChangePos, onEvtChangePos, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_Visible, onEvtActorVisible, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_Invisible, onEvtActorInvisible, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_BuildingInfoChange, onEvtActorBuildingInfoChange, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_GatherChange, onEvtActorGatherChange, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_Build_Action, onEvtActorAction, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_ResourceStaticStatus, onEvtResourceStaticStatus, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_Weather, onEvtWeatherState, AllSceneTypes...)
	scn.BindActorEventProcessor(Evt_Actor_Death, onEvtActorDeath, AllSceneTypes...)
}

func onEvtActorInvisible(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	evt := event.(*EvtInvisible)
	switch actor.Type() {
	case ActorPlayer, ActorMonster:
		var node = misc.NodeGame
		var nodeId = uint16(1)
		if actor.Type() == ActorMonster {
			node = misc.NodeAi
			nodeId = 31
		}
		app.Discovery().SendEventToNode(node, svc.SpaceSvc, nodeId, 0, actor.Id(),
			msg_spc.CdEveActorInvisible, &msg_spc.EveActorInvisible{
				ActorId:  actor.Id(),
				TargetId: evt.Id,
			})
	}
	return nil
}

func onEvtActorVisible(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	evt := event.(*EvtVisible)
	actorId := actor.Id()
	switch actor.Type() {
	case ActorPlayer, ActorMonster:
		eveActorVisible := &msg_spc.EveActorVisible{
			ActorId:    actorId,
			TargetId:   evt.Id,
			TargetType: evt.Type,
			PosX:       evt.Transform.PosX,
			PosY:       evt.Transform.PosY,
			PosZ:       evt.Transform.PosZ,
			ForX:       evt.Transform.ForX,
			ForY:       evt.Transform.ForY,
			ForZ:       evt.Transform.ForZ,
		}
		if evt.ComBuilding != nil {
			eveActorVisible.EvtBuilding = &msg_spc.EvtBuilding{
				BuildingType:  evt.ComBuilding.BuildingType,
				BuildingState: evt.ComBuilding.BuildingState,
				Level:         evt.ComBuilding.Level,
			}
		}else {
			if evt.Life != nil {
				eveActorVisible.Nickname = evt.Life.NickName
			}
			if evt.Resource != nil{
				eveActorVisible.EvtResource = &msg_spc.EvtResource{
					ResId: int32(evt.Resource.ResId),
					Empty: evt.Resource.Empty,
				}
			}
		}
		app.Log().Debug("visible", utils.M{
			"event": evt,
		})
		if actor.Type() == ActorPlayer {
			app.Discovery().SendEventToNode(misc.NodeGame, svc.SpaceSvc, 1, 0, actor.Id(),
				msg_spc.CdEveActorVisible, eveActorVisible)
		} else {
			app.Discovery().SendEventToNode(misc.NodeAi, svc.SpaceSvc, 31, 0, actor.Id(),
				msg_spc.CdEveActorVisible, eveActorVisible)
		}
	}
	return nil
}

func onEvtActorMove(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	switch actor.Type() {
	case ActorMonster:
		onPlayerMoveAiEvent(actor, t, event)
	}
	return nil
}

func onEvtActorMoveStop(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	evt := event.(*EvtMoveStop)
	switch actor.Type() {
	case ActorPlayer, ActorMonster:
		//todo nodeId改为获取
		var node = misc.NodeGame
		var nodeId = uint16(1)
		if actor.Type() == ActorMonster {
			node = misc.NodeAi
			nodeId = 31
		}
		app.Discovery().SendEventToNode(node, svc.SpaceSvc, nodeId, 0, actor.Id(),
			msg_spc.CdEveActorMoveStop, &msg_spc.EveActorMoveStop{
				ActorId:  actor.Id(),
				TargetId: evt.Id,
				Type:     evt.Type,
				PosX:     evt.PositionX,
				PosY:     evt.PositionY,
				PosZ:     evt.PositionZ,
			})
	}
	return nil
}

func onEvtChangePos(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	evt := event.(*EvtChangePos)
	switch actor.Type() {
	case ActorPlayer:
		app.Discovery().SendEventToNode(misc.NodeGame, svc.SpaceSvc, 1, 0, actor.Id(),
			msg_spc.CdEveActorPositionChange, &msg_spc.EveActorPositionChange{
				ActorId:  actor.Id(),
				TargetId: evt.Id,
				PosX:     evt.PositionX,
				PosY:     evt.PositionY,
				PosZ:     evt.PositionZ,
			})
	}
	return nil
}

func onEvtActorMoveStart(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	evt := event.(*EvtMoveStart)
	switch actor.Type() {
	case ActorPlayer, ActorMonster:
		//todo nodeId改为获取
		var node = misc.NodeGame
		var nodeId = uint16(1)
		if actor.Type() == ActorMonster {
			node = misc.NodeAi
			nodeId = 31
		}
		app.Log().Debug("move start", utils.M{
			"event": evt,
		})
		app.Discovery().SendEventToNode(node, svc.SpaceSvc, nodeId, 0, actor.Id(),
			msg_spc.CdEveActorMoveStart, &msg_spc.EveActorMoveStart{
				ActorId:  actor.Id(),
				TargetId: evt.Id,
				Type:     evt.Type,
				PosX:     evt.PositionX,
				PosY:     evt.PositionY,
				PosZ:     evt.PositionZ,
				ForX:     evt.ForwardX,
				ForY:     evt.ForwardY,
				ForZ:     evt.ForwardZ,
			})
	}
	return nil
}

func onEvtActorBuildingInfoChange(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	evt := event.(*EvtBuildingInfoChange)
	switch actor.Type() {
	case ActorPlayer:
		app.Discovery().SendEventToNode(misc.NodeGame, svc.SpaceSvc, 1, 0, actor.Id(),
			msg_spc.CdEveBuildingInfoChange, &msg_spc.EveBuildingInfoChange{
				ActorId:    actor.Id(),
				BuildingId: evt.Id,
				UpMap:      evt.UpMap,
			})
	}
	return nil
}

func onPlayerMoveAiEvent(actor intfc.IActor, t intfc.TActorEvent, event interface{}) {
	evt := event.(*EvtMove)
	if evt.Type == ActorPlayer {
		component, ok := actor.GetComponent(Com_Transform)
		if !ok {
			return
		}
		ct := component.(*ActorComTransform)
		tx, ty, _, _, err := getSceneTile(actor.Scene().Type(), ct.Position.X, ct.Position.Z)
		if err != nil {
			return
		}
		tags, _, e := GetSceneVisionTileAroundTags(actor.Scene().Type(), tx, ty)
		if e != nil {
			app.Log().Error(e.Error(), e.Params())
			return
		}
		actor.Scene().ActionActorByTags(func(monster intfc.IActor) {
			if monster.Type() == ActorMonster {
				app.Discovery().SendEventToNode(misc.NodeAi, svc.SpaceSvc, 31, 0, monster.Id(), msg_spc.CdEveMove, &msg_spc.EveMove{
					ActorId:   monster.Id(),
					TargetId:  evt.Id,
					Type:      evt.Type,
					PositionX: evt.PositionX,
					PositionY: evt.PositionY,
					PositionZ: evt.PositionZ,
					ForwardX:  evt.ForwardX,
					ForwardY:  evt.ForwardY,
					ForwardZ:  evt.ForwardZ,
				})
			}
		}, tags...)
	} else {
		app.Discovery().SendEventToNode(misc.NodeAi, svc.SpaceSvc, 31, 0, evt.Id, msg_spc.CdEveMove, &msg_spc.EveMove{
			ActorId:   evt.Id,
			TargetId:  evt.Id,
			Type:      evt.Type,
			PositionX: evt.PositionX,
			PositionY: evt.PositionY,
			PositionZ: evt.PositionZ,
			ForwardX:  evt.ForwardX,
			ForwardY:  evt.ForwardY,
			ForwardZ:  evt.ForwardZ,
		})
	}
}

func onEvtActorGatherChange(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	evt := event.(*EvtGatherChange)
	if actor.Type() == ActorPlayer {
		app.Discovery().SendEventToNode(misc.NodeGame, svc.SpaceSvc, 1, 0, actor.Id(),
			msg_spc.CdEveActorGatherChange, &msg_spc.EveActorGatherChange{
				ActorId:    actor.Id(),
				GatherId:   evt.ActorId,
				ResourceId: evt.ResId,
				Empty:      evt.Empty,
			})
	}
	return nil
}

func onEvtActorAction(actor intfc.IActor, t intfc.TActorEvent, event interface{}) utils.IError {
	evt := event.(*EvtBuildAction)
	switch actor.Type() {
	case ActorPlayer:
		app.Discovery().SendEventToNode(misc.NodeGame, svc.SpaceSvc, 1, 0, actor.Id(),
			msg_spc.CdEveActorBuildAction, &msg_spc.EveActorBuildAction{
				Id: actor.Id(),
				ActorId:  evt.Id,
				TargetId: evt.TargetId,
				ActionId: evt.ActionId,
			})
	}
	return nil
}

func onEvtResourceStaticStatus(actor intfc.IActor,t intfc.TActorEvent,event interface{}) utils.IError  {
	evt := event.(*EveResourceStaticStatus)
	switch actor.Type() {
	case ActorPlayer:
		app.Discovery().SendEventToNode(misc.NodeGame, svc.SpaceSvc, 1, 0, actor.Id(),
			msg_spc.CdEveActorResourceState, &msg_spc.EveActorResourceState{
				ActorId:    actor.Id(),
				ResourceId: evt.ResId,
				Empty:      evt.Empty,
			})
	}
	return nil
}

//onEvtWeatherState
func onEvtWeatherState(actor intfc.IActor,t intfc.TActorEvent,event interface{}) utils.IError  {
	evt := event.(*EveSceneWeatherInfo)
	switch actor.Type() {
	case ActorPlayer:
		app.Discovery().SendEventToNode(misc.NodeGame, svc.SpaceSvc, 1, 0, actor.Id(),
			msg_spc.CdEveSceneWeatherInfo, &msg_spc.EveSceneWeatherInfo{
				ActorId:    actor.Id(),
				SceneType: actor.Scene().Type(),
				SceneId:   actor.Scene().Id(),
				Sun:       evt.Sun,
				Snow:      evt.Snow,
				Rain:      evt.Rain,
				Wind:      evt.Wind,
				Shade:     evt.Shade,
			})
	}
	return nil
}

func onEvtActorDeath(actor intfc.IActor,t intfc.TActorEvent,event interface{}) utils.IError  {
	evt := event.(*EveDeath)
	switch actor.Type() {
	case ActorMonster:
		app.Discovery().SendEventToNode(misc.NodeAi, svc.SpaceSvc, 31, 0, actor.Id(),
			msg_spc.CdEveActorDeath, &msg_spc.EveActorDeath{
				ActorId:    actor.Id(),
				IsDeath:    evt.IsDeath,
			})
	}
	return nil
}