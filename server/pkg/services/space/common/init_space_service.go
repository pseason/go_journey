package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/common"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"context"
	"fmt"
	"github.com/panjf2000/ants/v2"
	"hm/pkg/misc"
	"hm/pkg/misc/cache_key"
	"hm/pkg/services/data/model/enum/task_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/space/msg_spc"
)

func InitSpaceService() {
	mDiscovery := app.Discovery()
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdActorEnterScene, onActorEnterScene)
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdActorExitScene, onActorExitScene)
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdActorMoveStart, onActorMoveStart)
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdActorMoveStop, onActorMoveStop)
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdActorPositionChange, onPositionChange)
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdActorBuildingInfoChange, onActorBuildInfoChange)
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdActorGatherResource, onActorGatherResource)
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdSceneWeather, onSceneWeather)
	mDiscovery.BindRequestAsyncHandler(msg_spc.CdActorBuildingAction, onActorBuildingAction)
}
func onActorEnterScene(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	req := body.(*msg_spc.ReqActorEnterScene)
	err := app.Scene().DoAction(func() {
		actorType, tags, err := app.SceneCache().LoadActor(aid)
		if err != nil {
			resErr(common.EcEnterSceneFailed)
			return
		}
		scene, err := app.Scene().GetScene(req.SceneType, req.SceneId)
		if err != nil {
			resErr(common.EcEnterSceneFailed)
			return
		}
		err = scene.DoAction(func() {
			_, err := scene.AddActor(actorType, aid)
			if err != nil {
				app.Log().TError(tid, err)
				resErr(common.EcEnterSceneFailed)
				return
			}
			err = scene.AddActorTags(aid, tags...)
			if err != nil {
				app.Log().TError(tid, err)
				resErr(common.EcEnterSceneFailed)
				return
			}
			resObj(&msg_spc.ResActorEnterScene{})
		})
		if err != nil {
			app.Log().TError(tid, err)
			resErr(common.EcEnterSceneFailed)
		}
	})
	if err != nil {
		app.Log().TError(tid, err)
		resErr(common.EcExitSceneFailed)
	}
}

func onActorExitScene(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	//req := body.(*msg_spc.ReqActorExitScene)
	sceneType, sceneId, err := GetRedisActorSceneData(aid)
	if err != nil {
		app.Log().TError(tid, err)
		resErr(common.EcExitSceneFailed)
		return
	}
	err = app.Scene().DoAction(func() {
		scene, err := app.Scene().GetScene(sceneType, sceneId)
		if err != nil {
			app.Log().TError(tid, err)
			resErr(common.EcExitSceneFailed)
			fmt.Sprintf("%d",0)
			return
		}
		err = scene.DoAction(func() {
			scene.ClearActorTags(aid)
			actor, err := scene.GetActor(aid)
			if err != nil {
				resObj(&msg_spc.ResActorExitSpace{})
				return
			}
			com, _ := actor.GetComponent(Com_Transform)
			tnf := com.(*ActorComTransform)
			tags, _, _ := GetSceneVisionTileAroundTags(sceneType, tnf.tileX, tnf.tileY)
			scene.DispatchActorEventByTags(Evt_Invisible, NewEvtInvisible(actor), tags...)
			scene.RemoveActor(aid)
		})
		if err != nil {
			app.Log().TError(tid, err)
			resErr(common.EcExitSceneFailed)
			return
		}
		resObj(&msg_spc.ResActorExitSpace{})
	})
	if err != nil {
		app.Log().TError(tid, err)
		resErr(common.EcExitSceneFailed)
	}
}

func getActorScene(actorId int64) (intfc.IScene, utils.IError) {
	sceneType, sceneId, err := GetRedisActorSceneData(actorId)
	if err != nil {
		return nil, err
	}
	scene, err := app.Scene().GetScene(sceneType, sceneId)
	if err != nil {
		return nil, err
	}
	return scene, nil
}

func checkErr(err utils.IError, complete utils.Action, resErr utils.ActionErr) {
	if err != nil {
		app.Log().Error(err.Error(), err.Params())
		resErr(err)
		return
	}
	complete()
}

func doActorAction(actorId int64, action intfc.ActionActorToErr, complete utils.Action, resErr utils.ActionErr) {
	err := app.Scene().DoAction(func() {
		scene, err := getActorScene(actorId)
		if err != nil {
			resErr(err)
			return
		}
		err = scene.DoAction(func() {
			checkErr(scene.ActionActor(actorId, action), complete, resErr)
		})
		if err != nil {
			resErr(err)
			return
		}
	})
	if err != nil {
		resErr(err)
		return
	}
}

func onActorMoveStart(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	req := body.(*msg_spc.ReqActorMoveStart)
	doActorAction(aid, func(actor intfc.IActor) utils.IError {
		com, _ := actor.GetComponent(Com_Transform)
		com.(*ActorComTransform).StartMove(req.PosX, req.PosY, req.PosZ, req.ForX, req.ForY, req.ForZ)
		return nil
	}, func() {
		resObj(&msg_spc.ResActorMoveStart{})
	}, func(err error) {

	})
}

func onActorMoveStop(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	req := body.(*msg_spc.ReqActorMoveStop)
	doActorAction(aid, func(actor intfc.IActor) utils.IError {
		com, _ := actor.GetComponent(Com_Transform)
		com.(*ActorComTransform).StopMove(req.PosX, req.PosY, req.PosZ)
		return nil
	}, func() {
		resObj(&msg_spc.ResActorMoveStop{})
	}, func(err error) {
	})
}

func onPositionChange(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	//todo, 不应该客户端给服务端位置下版本沟通
	resObj(&msg_spc.ResActorPositionChange{})
	return

	req := body.(*msg_spc.ReqActorPositionChange)
	doActorAction(aid, func(actor intfc.IActor) utils.IError {
		com, ok := actor.GetComponent(Com_Transform)
		if !ok {
			err := utils.NewError(ErrNotExitActorComponent, utils.M{
				"actor component type": Com_Transform,
			})
			return err
		}
		tnf := com.(*ActorComTransform)
		ox, oy, oz := req.PosX-tnf.Position.X, req.PosY-tnf.Position.Y, req.PosZ-tnf.Position.Z
		tnf.Move(ox, oy, oz, tnf.Forward.X, tnf.Forward.Y, tnf.Forward.Z)
		return nil
	}, func() {
		resObj(&msg_spc.ResActorPositionChange{})
	}, func(err error) {
		resErr(common.EcSceneChangePosFailed)
	})
}

func onActorBuildInfoChange(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	req := body.(*msg_spc.ReqActorBuildingInfoChange)
	doActorAction(aid, func(actor intfc.IActor) utils.IError {
		com, _ := actor.GetComponent(Com_Building)
		com.(*ActorComBuilding).BuildingInfoChange(req.UpMap)
		return nil
	}, func() {
		resObj(&msg_spc.ResActorBuildingInfoChange{})
	}, func(err error) {

	})
}

func onActorBuildingAction(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	req := body.(*msg_spc.ReqActorBuildingAction)
	doActorAction(aid, func(actor intfc.IActor) utils.IError {
		NewEvtAction(aid,req.TargetId,req.ActionId).SyncActorAction(actor)
		return nil
	}, func() {
		resObj(&msg_spc.ResActorBuildingAction{})
	}, func(err error) {

	})
}

//采集资源
func onActorGatherResource(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	req := body.(*msg_spc.ReqActorGatherResource)
	_ = app.Scene().DoAction(func() {
		scene, err := getActorScene(aid)
		if err != nil {
			resErr(common.EcEnterSceneFailed)
			return
		}
		err = scene.ActionActor(aid, func(actor intfc.IActor) utils.IError {
			com, _ := actor.GetComponent(Com_Gather)
			resData := com.(*ActorComGather).TestGather(req.ResourceId, req.CurrentEnergy, req.PractisedLevel)
			if resData == nil {
				resObj(&msg_spc.ResActorGatherResource{})
				return nil
			}

			sceneType := actor.Scene().Type()
			if resData.Event != nil{
				//广播角色推送
				tags, _, e := GetSceneEventTileAroundTags(sceneType, resData.EventPosX, resData.EventPosY)
				if e != nil {
					app.Log().Error(e.Error(), e.Params())
					return e
				}
				app.Log().Info("evt is",utils.M{"evt":resData.Event})
				actor.Scene().DispatchActorEventByTags(Evt_GatherChange, resData.Event, tags...)
			}

			//task 采集调用
			_ = ants.Submit(func() {
				for key,value := range resData.Rewards {
					_,err := app.Discovery().Redis().Get(context.Background(),cache_key.TaskCache.Format(aid,task_e.TaskAftPick,key)).Result()
					if err == nil{
						//todo 触发任务
						app.Discovery().DispatchEvent(misc.NodeGame,0,tid,aid,msg_dt.CdEveTaskExecute,&msg_dt.EveTaskExecute{
							Cid: aid,
							PropId: key,
							PropNum: value,
							AftType: task_e.TaskAftPick,
						})
					}
				}

			})

			resObj(&msg_spc.ResActorGatherResource{
				TplId:         resData.TplId,
				Rewards:       resData.Rewards,
				ResourceId:    int32(resData.Result),
				ConsumeEnergy: resData.ConsumeEnergy,
			})
			return nil
		})
		if err != nil {
			resErr(common.EcEnterSpaceFailed)
			return
		}
	})
}

//获取场景的天气
func onSceneWeather(service uint16, tid int64, aid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
	req := body.(*msg_spc.ReqSceneWeather)
	_ = app.Scene().DoAction(func() {
		scene, err := app.Scene().GetScene(req.SceneType, req.SceneId)
		if err != nil {
			resErr(common.EcEnterSceneFailed)
			return
		}
		weatherData, ok := scene.Get(TFiled(req.SceneType))
		if !ok || weatherData == nil {
			resObj(&msg_spc.ResSceneWeather{})
			return
		}
		weather := weatherData.(*UpMap)
		resObj(&msg_spc.ResSceneWeather{
			Sun:   weather.Sun,
			Snow:  weather.Snow,
			Rain:  weather.Rain,
			Wind:  weather.Wind,
			Shade: weather.Shade,
		})
	})
}
