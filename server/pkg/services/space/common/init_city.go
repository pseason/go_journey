package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"math/rand"
	"sort"
	"time"
)

var (
	_ActorToEnterCity = map[TActor]intfc.ActionSceneActor{
		ActorPlayer:   onLifeEnterCity,
		ActorBuilding: onBuildingEnterCity,
		ActorMonster:  onMonsterEnterCity,
		ActorResource: onStaticResource,
	}
)

func InitCity() {
	SceneGlobal().AddSceneType(misc.SceneCity, &_Conf.Space.City.Scene)

	mSpace := app.Scene()
	mSpace.BindSceneFac(misc.SceneCity, cityFac)
	mSpace.BindActorEnterScene(misc.SceneCity, onActorEnterCity, onCityAddActor)
	mSpace.BindActorExitScene(misc.SceneCity, onActorExitCity, onCityRemoveActor)

	start := _Conf.Space.City.Id.Start
	end := _Conf.Space.City.Id.End
	ids := make([]int64, 0, end-start+1)
	nodeId := app.Discovery().NodeId()

	//[天气初始化定时器][开始]
	var maxMonth int32 = 12
	CronIds = make(map[intfc.TScene]map[int64]int64, 0)
	CronId := make(map[int64]int64, 0)
	s := &InitWeather{
		weatherTsv:       tsv.GetTsvManager(tsv.Weather).(*tsv.WeatherTsvManager),
		weatherRandomTsv: tsv.GetTsvManager(tsv.WeatherRandom).(*tsv.WeatherRandomTsvManager),
		monthId:          1,
		sceneWeather:     make(map[intfc.TScene]map[int64]UpMap, 0),
	}

	//[加载静态资源]
	list, err := LoadStaticResourceList(utils.ExeDir(), "judian_server")
	if err != nil {
		log.Info("static load failed", utils.M{"err": err})
		return
	}

	for i := start; i <= end; i++ {
		ids = append(ids, i)
		sceneWeatherId := i
		//_ = mSpace.CreateScene(sceneWeatherId, misc.SceneCity, nil)

		_ = mSpace.CreateScene(misc.SceneCity, sceneWeatherId, data.Map{misc.SceneCity: s.sceneWeather[misc.SceneCity][sceneWeatherId]})
		WeatherCronId, _ := app.Timer().Cron("* * * 30 1 *", func() {
			_ = app.Scene().DoAction(func() {
				if s.monthId > maxMonth {
					s.monthId = 1
				}
				InitUpMap := InitWeatherRandomInfo(s, s.monthId, misc.SceneCity, sceneWeatherId)
				//app.Log().Info("info",utils.M{"s":s.sceneWeather})
				s.monthId += 1
				//广播所有人
				scene, err := app.Scene().GetScene(misc.SceneCity, sceneWeatherId)
				if err != nil {
					app.Log().Debug("scene nil", utils.M{"sceneId": i})
					return
				}
				//设置map入场景Data
				scene.Set(misc.SceneCity, InitUpMap)
				event := NewEveSceneWeatherInfo(InitUpMap.Sun, InitUpMap.Snow, InitUpMap.Rain, InitUpMap.Wind, InitUpMap.Shade)
				//进入场景协程
				_ = scene.DoAction(func() {
					scene.DispatchAllActorEvent(Evt_Weather, event)
				})

			})
		})
		CronId[i] = WeatherCronId
		CronIds[misc.SceneCity] = CronId
		//[天气初始化定时器][结束]
		SetRedisSceneData(misc.SceneCity, i, misc.NodeServices_CitySpc, nodeId)
		EnterCacheStaticResource(sceneWeatherId, list)
	}
	SetRedisSceneId(ids)
	//[加载静态资源]
	app.Timer().After(8000, func() {
		InitStaticResource(ids, list)
	})
}

//todo 将redis资源生成Actor
func InitStaticResource(sceneId []int64, list *msg_gm.ServerMapRes) {
	for _, id := range sceneId {
		for _, resource := range list.Objects {
			resId := int64(resource.Id)
			actorType, tags, err := app.SceneCache().LoadActor(resId)
			scene, err := app.Scene().GetScene(misc.SceneCity, id)
			if err != nil {
				app.Log().TError(id, err)
				return
			}
			err = scene.DoAction(func() {
				_, err := scene.AddActor(actorType, resId)
				if err != nil {
					app.Log().TError(id, err)
					return
				}
				err = scene.AddActorTags(resId, tags...)
				if err != nil {
					app.Log().TError(id, err)
					return
				}
			})
			if err != nil {
				app.Log().TError(id, err)
			}
		}

	}

}

func EnterCacheStaticResource(sceneId int64, list *msg_gm.ServerMapRes) {
	total := len(list.Objects)
	ResIds := make([]int64, 0, total)
	srcToCountMap := make(map[int32]int32, 0)
	for _, src := range list.Objects {
		srcId := int32(src.JungleId)
		if _, ok := srcToCountMap[srcId]; ok {
			srcToCountMap[srcId] += 1
		} else {
			srcToCountMap[srcId] = 0
		}
		ResIds = append(ResIds, int64(src.Id))
	}

	tsvConf := tsv.GetTsvManager(tsv.Collect).(*tsv.CollectTsvManager).TsvMap

	for _, item := range list.Objects {
		aid := int64(item.Id)
		itemId := int32(item.JungleId)
		collectProp, has := tsvConf[itemId]
		if !has {
			app.Log().Error("get templet failed", utils.M{"itemId": itemId})
			continue
		}
		RemainSrcToCount := make(map[int32]int32, len(collectProp.Props))
		for _, props := range collectProp.Props {
			RemainSrcToCount[props[0]] = props[3]
		}

		err := app.SceneCache().SaveActor(aid, ActorResource, nil,
			&ActorComTransform{
				Position: utils.Vec3{
					X: item.Trs[0],
					Y: item.Trs[1],
					Z: item.Trs[2],
				},
				Forward: utils.Vec3{
					X: 1,
				},
			}, &ActorComResource{
				TplId:            int32(item.JungleId),
				RemainSrcToCount: RemainSrcToCount,
			})
		if err != nil {
			app.Log().TError(sceneId, utils.NewError("加载资源到redis失败", utils.M{
				"aid": aid,
			}))
			return
		}
	}
	log.Info("static resource", utils.M{"total": total})
}

func InitWeatherRandomInfo(s *InitWeather, initMonth int32, sceneType intfc.TScene, sceneId int64) *UpMap {
	outWeather := &UpMap{
		Sun:       0,
		Snow:      0,
		Rain:      0,
		Wind:      0,
		Shade:     0,
		WeatherID: 0,
	}
	RandomSlice := s.weatherTsv.TsvMap[s.monthId].Random

	sort.Slice(RandomSlice, func(i, j int) bool {
		return RandomSlice[i][1] > RandomSlice[j][1]
	})
	RandomMap := make(map[int]int32, 0)
	RandomSliceSum := 0
	for index, v := range RandomSlice {
		RandomMap[index] = v[1]
		RandomSliceSum += int(v[1])
	}
	rand.Seed(time.Now().Unix())
	randIndex := 0
	for i, proSur := range RandomMap {
		randNum := rand.Intn(RandomSliceSum)
		if randNum <= int(proSur) {
			randIndex = i
			break
		} else {
			RandomSliceSum -= int(proSur)
		}
	}
	weatherId := RandomSlice[randIndex][0]
	weatherType := s.weatherRandomTsv.TsvMap[weatherId].WeatherType
	strengthFloat := s.weatherRandomTsv.TsvMap[weatherId].WeatherValue
	RandInt64 := func(min, max int64) int64 {
		if min >= max {
			return max
		}
		return rand.Int63n(max-min) + min
	}
	withStrength := RandInt64(int64(strengthFloat[0]*10), int64(strengthFloat[1]*10)+1)
	shadowStrength := RandInt64(0, 11)

	switch weatherType {
	case 1:
		outWeather.Sun = float64(withStrength) / 10
	case 2:
		outWeather.Snow = float64(withStrength) / 10
	case 3:
		outWeather.Rain = float64(withStrength) / 10
	case 4:
		outWeather.Wind = float64(withStrength) / 10
	}
	outWeather.Shade = float64(shadowStrength) / 10
	outWeather.WeatherID = weatherId
	//写入持有变量
	SceneMap := make(map[int64]UpMap, 0)
	SceneMap[sceneId] = *outWeather
	s.sceneWeather[sceneType] = SceneMap
	return outWeather
}

var (
	CronIds map[intfc.TScene]map[int64]int64
)

type InitWeather struct {
	weatherTsv       *tsv.WeatherTsvManager
	weatherRandomTsv *tsv.WeatherRandomTsvManager
	monthId          int32
	sceneWeather     map[intfc.TScene]map[int64]UpMap
}

type UpMap struct {
	Sun       float64
	Snow      float64
	Rain      float64
	Wind      float64
	Shade     float64
	WeatherID int32
}

func DisposeCity() {
	start := _Conf.Space.City.Id.Start
	end := _Conf.Space.City.Id.End
	ids := make([]int64, 0, end-start+1)
	for i := start; i <= end; i++ {
		ids = append(ids, i)
		DelRedisSceneData(misc.SceneCity, i)
		//清除定时器
		app.Timer().Remove(CronIds[misc.SceneCity][i])
	}
}

func onActorEnterCity(scene intfc.IScene, actor intfc.IActor) {
	action, ok := _ActorToEnterCity[actor.Type()]
	if !ok {
		app.Log().Error("not support actor type", utils.M{
			"actor type": actor.Type(),
		})
		return
	}
	action(scene, actor)
}

func onLifeEnterCity(scene intfc.IScene, actor intfc.IActor) {
	// 添加组件
	tnf := &ActorComTransform{}
	_ = actor.AddComponent(tnf)
	tx, ty, nx, nz, err := getSceneTile(scene.Type(), tnf.Position.X, tnf.Position.Z)
	if err != nil {
		app.Log().Error(err.Error(), err.Params())
		return
	}
	tnf.Position.X, tnf.Position.Z = nx, nz
	tnf.SetTile(tx, ty)
	_ = actor.AddComponent(&ActorComLife{})
	_ = actor.AddComponent(&ActorComGather{})

	//绑定事件
	actor.BindEventProcessor(Evt_Visible, Evt_Invisible, Evt_MoveStart, Evt_MoveStop, Evt_ChangePos,
		Evt_BuildingInfoChange, Evt_Move, Evt_Build_Action, Evt_GatherChange, Evt_ResourceStaticStatus, Evt_Weather)

	// 广播事件
	actorId := actor.Id()
	//获取宫格的标签
	tags, tag, e := GetSceneVisionTileAroundTags(scene.Type(), tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}

	evt := NewEvtVisible(actor)
	actor.Scene().ActionActorByTags(func(actor2 intfc.IActor) {
		//派发新加入的事件给周围的Actor
		actor2.ProcessEvent(Evt_Visible, evt)

		//派发周围的事件给新加入的
		actor.ProcessEvent(Evt_Visible, NewEvtVisible(actor2))
		com, _ := actor2.GetComponent(Com_Transform)
		tnf := com.(*ActorComTransform)
		if tnf.moving {
			actor.ProcessEvent(Evt_MoveStart, NewEvtMoveStart(actor2))
		}
	}, tags...)
	//添加新加入的宫格标签
	actor.Scene().AddActorTags(actorId, tag)
	//增加空间Actor数量
	e = IncrRedisSceneActorCount(misc.SceneCity, scene.Id(), actor.Type(), 1)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
	}
}

func onMonsterEnterCity(scene intfc.IScene, actor intfc.IActor) {
	// 添加组件
	_ = actor.AddComponent(&ActorComTransform{})
	_ = actor.AddComponent(&ActorComLife{})
	componentTransform, ok := actor.GetComponent(Com_Transform)
	if !ok {
		app.Log().Error("load Transform component failed", nil)
		return
	}
	comTransform := componentTransform.(*ActorComTransform)
	tx, ty, _, _, err := getSceneTile(scene.Type(), comTransform.Position.X, comTransform.Position.Z)
	if err != nil {
		app.Log().Error(err.Error(), err.Params())
		return
	}
	//绑定事件
	actor.BindEventProcessor(Evt_Visible, Evt_Invisible, Evt_MoveStart, Evt_MoveStop, Evt_Move)

	// 广播事件
	evt := NewEvtVisible(actor)
	actorId := actor.Id()
	//获取宫格的标签
	tags, tag, e := GetSceneVisionTileAroundTags(scene.Type(), tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}

	scene.ActionActorByTags(func(actor2 intfc.IActor) {
		//派发周围的事件给新加入的
		actor.ProcessEvent(Evt_Visible, NewEvtVisible(actor2))
		//派发新加入的事件给周围的Actor
		actor2.ProcessEvent(Evt_Visible, evt)
	}, tags...)
	//添加新加入的宫格标签
	scene.AddActorTags(actorId, tag)
	//增加空间Actor数量
	e = IncrRedisSceneActorCount(misc.SceneCity, scene.Id(), actor.Type(), 1)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
	}
}

func onBuildingEnterCity(scene intfc.IScene, actor intfc.IActor) {
	// 添加组件
	tnf := &ActorComTransform{}
	actor.AddComponent(tnf)
	tx, ty, nx, nz, err := getSceneTile(scene.Type(), tnf.Position.X, tnf.Position.Z)
	if err != nil {
		app.Log().Error(err.Error(), err.Params())
		return
	}
	tnf.Position.X, tnf.Position.Z = nx, nz
	tnf.SetTile(tx, ty)
	buildingCpt := &ActorComBuilding{}
	actor.AddComponent(buildingCpt)

	// 广播事件
	evt := NewEvtVisible(actor)
	actorId := actor.Id()
	//获取宫格的标签
	tags, tag, e := GetSceneVisionTileAroundTags(scene.Type(), tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}

	//派发新加入的事件给周围的Actor
	scene.DispatchActorEventByTags(Evt_Visible, evt, tags...)
	app.Log().Info("building tags", utils.M{"tags": tag})
	//添加新加入的宫格标签
	scene.AddActorTags(actorId, tag)
}

func onCityAddActor(scene intfc.IScene, actor intfc.IActor) {
}

func onActorExitCity(scene intfc.IScene, actor intfc.IActor) {
	com, _ := actor.GetComponent(Com_Transform)
	tnf := com.(*ActorComTransform)
	tx, ty := tnf.Tile()
	evt := NewEvtInvisible(actor)
	actorId := actor.Id()
	sceneType := scene.Type()
	//获取宫格的标签
	tags, tag, e := GetSceneVisionTileAroundTags(sceneType, tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}
	// 移除宫格标签
	_ = scene.RemoveActorTags(actorId, tag)
	//派发周围不可见事件给推出的Actor
	scene.ActionActorByTags(func(actor2 intfc.IActor) {
		actor.ProcessEvent(Evt_Invisible, NewEvtInvisible(actor2))
		//派发不可见事件给周围Actor
		actor2.ProcessEvent(Evt_Invisible, evt)
	}, tags...)
	//减少空间Actor数量
	e = IncrRedisSceneActorCount(misc.SceneCity, scene.Id(), actor.Type(), -1)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
	}
}

func onCityRemoveActor(scene intfc.IScene, actor intfc.IActor) {

}

func cityFac(scene intfc.IScene, data interface{}) utils.IError {
	app.Log().Debug("create city", utils.M{
		"scene id":   scene.Id(),
		"scene type": scene.Type(),
	})
	//initSceneTileTags(scene.Type(), scene.Id(), &_Conf.Space.City.Scene)
	return nil
}

func onStaticResource(scene intfc.IScene, actor intfc.IActor) {
	// 添加组件
	tnf := &ActorComTransform{}
	actor.AddComponent(tnf)
	tx, ty, nx, nz, err := getSceneTile(scene.Type(), tnf.Position.X, tnf.Position.Z)
	if err != nil {
		app.Log().Error(err.Error(), err.Params())
		return
	}
	tnf.Position.X, tnf.Position.Z = nx, nz
	tnf.SetTile(tx, ty)
	actor.AddComponent(&ActorComResourceStatic{})

	// 广播事件
	evt := NewEvtVisible(actor)
	actorId := actor.Id()
	//获取宫格的标签
	tags, tag, e := GetSceneVisionTileAroundTags(scene.Type(), tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}

	//派发新加入的事件给周围的Actor
	scene.DispatchActorEventByTags(Evt_Visible, evt, tags...)
	app.Log().Info("static resource tags", utils.M{"tags": tag})
	//添加新加入的宫格标签
	scene.AddActorTags(actorId, tag)
}
