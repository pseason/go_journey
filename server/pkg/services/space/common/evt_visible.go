package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
)

func NewEvtVisible(actor intfc.IActor) *EvtVisible {
	return &EvtVisible{
		Id:          actor.Id(),
		Type:        actor.Type(),
		Transform:   newTransform(actor),
		Life:        newLife(actor),
		ComBuilding: newBuilding(actor),
		Resource:    newResource(actor),
	}
}

type EvtVisible struct {
	Id          int64
	Type        intfc.TActor
	Transform   *Transform
	Life        *Life
	ComBuilding *ActorComBuilding
	Resource    *Resource
}

func newTransform(actor intfc.IActor) *Transform {
	c, ok := actor.GetComponent(Com_Transform)
	if !ok {
		return nil
	}
	transform := c.(*ActorComTransform)
	return &Transform{
		PosX: transform.Position.X,
		PosY: transform.Position.Y,
		PosZ: transform.Position.Z,
		ForX: transform.Forward.X,
		ForY: transform.Forward.Y,
		ForZ: transform.Forward.Z,
	}
}

type Transform struct {
	PosX float32
	PosY float32
	PosZ float32
	ForX float32
	ForY float32
	ForZ float32
}

func newLife(actor intfc.IActor) *Life {
	c, ok := actor.GetComponent(Com_Life)
	if !ok {
		return nil
	}
	life := c.(*ActorComLife)
	return &Life{
		NickName:  life.NickName,
		Level:     life.Level,
		Gender:    life.Gender,
		Figure:    life.Figure,
		TeamId:    life.TeamId,
		FactionId: life.FactionId,
	}
}

func newBuilding(actor intfc.IActor) *ActorComBuilding {
	c, ok := actor.GetComponent(Com_Building)
	if !ok {
		return nil
	}
	comBuilding := c.(*ActorComBuilding)
	return &ActorComBuilding{
		BuildingType:  comBuilding.BuildingType,
		BuildingState: comBuilding.BuildingState,
		Level:         comBuilding.Level,
		TeamId:        comBuilding.TeamId,
		FactionId:     comBuilding.FactionId,
	}
}

type Resource struct {
	ResId int64
	Empty int32
}

func newResource(actor intfc.IActor) *Resource {
	c, ok := actor.GetComponent(Com_Resource)
	if !ok {
		return nil
	}

	comResource := c.(IComResource)

	var resEmpty int32 =0
	if comResource.TestEmpty() {
		resEmpty =1
		app.Log().Info("evt_visble resources is",utils.M{"Resource":actor.Id(),"empty":resEmpty})
	}
	return  &Resource{
			ResId:actor.Id(),
			Empty:resEmpty,
	}
}

type Life struct {
	NickName  string //昵称
	Level     int32  //等级
	Gender    bool   //性别
	Figure    int32  //头像
	TeamId    int64  //队伍id
	FactionId int64  //阵营
}
