package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
	"time"
)

const (
	MinGatherIntervalMs = 600 //采集的最大时间间隔
)

type GatherResult uint8

const (
	GatherTooDistant       GatherResult = iota //距离太远
	GatherLackOfEnergy                         //精力不足
	GatherEmpty                                //资源已经被采集完
	GatherSuccess                              //采集成功
	GatherComplete                             //采集完成
	GatherTooFast                              //采集速度太快
	GatherNotExitResource                      //不存在的资源
	GatherMoving                               //移动中不能采集
	GatherCompleteAndEmpty                     //采集完成资源也空了
	GatherNotExitWeather                       //采集资源不在当前天气中
)

type ActorComGather struct {
	scene.ActorComponent
	currentResourceId int64
	currentCount      int32
	lastGatherTime    int64
	lastMoveId        int64
}

//当前采集资源id
func (g *ActorComGather) CurrentResourceId() int64 {
	return g.currentResourceId
}

type TestGatherResult struct {
	TplId         int32
	Rewards       map[int32]int32
	ConsumeEnergy int32
	Result        GatherResult
	EventPosX     int32
	EventPosY     int32
	Event         *EvtGatherChange
}

//尝试采集资源
func (g *ActorComGather) TestGather(resourceId int64, energy,practisedLevel int32) (gatherRes *TestGatherResult) {
	now := time.Now().UnixNano() / 1e6
	if now-g.lastGatherTime < MinGatherIntervalMs {
		gatherRes = &TestGatherResult{
			Result: GatherTooFast,
		}
		return
	}

	com, _ := g.Actor().GetComponent(Com_Transform)
	playerTnf := com.(*ActorComTransform)
	if playerTnf.Moving() {
		gatherRes = &TestGatherResult{
			Result :GatherMoving,
		}
		return
	}
	if playerTnf.moveTickId != g.lastMoveId {
		app.Log().Debug("reset gather because moved", nil)
		g.ClearGather()
		g.lastMoveId = playerTnf.moveTickId
	}
	actor := g.Actor()
	gatherActorId := actor.Id()

	resWeather := &UpMap{WeatherID:1}
	weatherData, ok := actor.Scene().Get(TFiled(actor.Scene().Type()))
	if ok && weatherData != nil{
		resWeather = weatherData.(*UpMap)
	}

	srcActor, err := actor.Scene().GetActor(resourceId)
	if err != nil {
		app.Log().Info(err.Error(), err.Params())
		gatherRes = &TestGatherResult{
			Result: GatherNotExitResource,
		}
		return
	}
	com, ok = srcActor.GetComponent(Com_Resource)
	if !ok {
		app.Log().Info("resource load failed",nil)
		gatherRes = &TestGatherResult{
			Result: GatherNotExitResource,
		}
		return
	}

	res := com.(IComResource)
	if res.TestGather(gatherActorId, 0, 0) {
		gatherRes = &TestGatherResult{
			Result: GatherEmpty,
		}
		return
	}

	//判断资源是否在当前天气中
	if !res.TestWeather(resWeather.WeatherID){
		gatherRes = &TestGatherResult{
			Result: GatherNotExitWeather,
		}
		return nil
	}

	com, _ = srcActor.GetComponent(Com_Transform)
	srcTnf := com.(*ActorComTransform)
	if !ok{
		app.Log().Info("resource get failed",nil)
		gatherRes = &TestGatherResult{
			Result: GatherTooFast,
		}
		return
	}
	//获取相对距离
	dis := utils.Vec3Distance(srcTnf.Position, playerTnf.Position)
	if res.TestDistant(dis) {
		gatherRes = &TestGatherResult{
			Result: GatherTooDistant,
		}
		return
	}
	ce := res.ConsumeEnergy()
	if energy < ce {
		gatherRes = &TestGatherResult{
			Result: GatherLackOfEnergy,
		}
		return
	}
	if res.TestEmpty() {
		gatherRes = &TestGatherResult{
			Result: GatherEmpty,
		}
		return
	}
	event := NewEvtGatherChange(gatherActorId, resourceId, 0)
	if g.currentResourceId != resourceId {
		g.currentResourceId = resourceId
		g.currentCount = 1
		app.Log().Debug("gather success",nil)
		gatherRes = &TestGatherResult{
			Result:   GatherSuccess,
			TplId:    res.GetTplId(),
			Event:         event,
			EventPosX:      srcTnf.tileX,
			EventPosY:      srcTnf.tileY,
		}
		return
	}
	g.currentCount++
	props, complete := res.TestReward(practisedLevel,resWeather.WeatherID,g.currentCount)
	if !complete {
		app.Log().Debug("gather success",nil)
		gatherRes = &TestGatherResult{
			Result:   GatherSuccess,
			TplId:    res.GetTplId(),
			Event:         event,
			EventPosX:     srcTnf.tileX,
			EventPosY:     srcTnf.tileY,
		}
		return
	}
	g.currentCount = 0
	app.Log().Debug("gather complete",nil)
	gatherRes = &TestGatherResult{
		Rewards:       props,
		ConsumeEnergy: ce,
		TplId:         res.GetTplId(),
		Event:         event,
		EventPosX:      srcTnf.tileX,
		EventPosY:      srcTnf.tileY,
	}
	if res.TestEmpty() {
		event.Empty = 1
		res.Rebirth()
		gatherRes.Result = GatherCompleteAndEmpty
	} else {
		gatherRes.Result = GatherComplete
	}
	g.lastGatherTime = now
	return
}

func (g *ActorComGather) ClearGather() {
	if g.currentResourceId == 0 {
		return
	}
	g.currentResourceId = 0
	g.currentCount = 0
}

func (g *ActorComGather) Type() intfc.TActorComponent {
	return Com_Gather
}

func (g *ActorComGather) Init() {
	g.currentResourceId = 0
	g.currentCount = 0
}

func (g *ActorComGather) Start() utils.IError {
	return app.SceneCache().LoadActorComponent(g.Actor().Id(), g)
}

func (g *ActorComGather) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(g.Actor().Id(), g)
}

