package common

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
	"hm/pkg/misc"
	"math"
	"strconv"
)

var (
	_SceneGlobalData = NewSceneGlobalData()
)

func SceneGlobal() *SceneGlobalData {
	return _SceneGlobalData
}

func NewSceneData(conf *Scene) *SceneData {
	return &SceneData{
		conf:             conf,
		visionAroundTags: make(map[string][]string),
		eventAroundTags:  make(map[string][]string),
	}
}

type SceneData struct {
	conf             *Scene
	visionAroundTags map[string][]string
	eventAroundTags  map[string][]string
}

func NewSceneGlobalData() *SceneGlobalData {
	return &SceneGlobalData{
		typeToConf: make(map[intfc.TScene]*SceneData),
	}
}

type SceneGlobalData struct {
	typeToConf map[intfc.TScene]*SceneData
}

func (s *SceneGlobalData) GetSceneData(sceneType intfc.TScene) (sceneData *SceneData, ok bool) {
	sceneData, ok = s.typeToConf[sceneType]
	return
}

func (s *SceneGlobalData) AddSceneType(sceneType intfc.TScene, conf *Scene) utils.IError {
	_, ok := s.typeToConf[sceneType]
	if ok {
		return utils.NewError(scene.ExistSceneType, utils.M{
			"scene type": sceneType,
		})
	}
	var sceneConf = NewSceneData(conf)
	tw := int32(math.Ceil(float64(conf.Width) / float64(conf.TileSize)))
	tl := int32(math.Ceil(float64(conf.Length) / float64(conf.TileSize)))
	visionSize := int32(math.Ceil(float64(conf.VisionDis) / float64(conf.TileSize)))
	eventSize := int32(math.Ceil(float64(conf.EventDis) / float64(conf.TileSize)))
	for x := int32(0); x < tw; x++ {
		for y := int32(0); y < tl; y++ {
			tileTag := mergeSceneTileKey(x, y)
			minX, maxX, minY, maxY := getTileAroundSize(tw, tl, x, y, visionSize)
			sceneConf.visionAroundTags[tileTag] = getTileAroundTiles(minX, maxX, minY, maxY)
			minX, maxX, minY, maxY = getTileAroundSize(tw, tl, x, y, eventSize)
			sceneConf.eventAroundTags[tileTag] = getTileAroundTiles(minX, maxX, minY, maxY)
		}
	}
	s.typeToConf[sceneType] = sceneConf
	return nil
}

func getTileAroundSize(tw, tl, x, y, size int32) (minX, maxX, minY, maxY int32) {
	minX, _ = utils.FloorInt32(0, x-size)
	maxX, _ = utils.CeilInt32(tw, x+size+1)
	minY, _ = utils.FloorInt32(0, y-size)
	maxY, _ = utils.CeilInt32(tl, y+size+1)
	return
}

func mergeSceneTileKey(x, y int32) string {
	return strconv.FormatInt(int64(x), 10) + "_" + strconv.FormatInt(int64(y), 10)
}

func getTileAroundTiles(minX, maxX, minY, maxY int32) []string {
	tiles := make([]string, 0, (maxX-minX)*(maxY-minY))
	for i := minX; i < maxX; i++ {
		for j := minY; j < maxY; j++ {
			tiles = append(tiles, mergeSceneTileKey(i, j))
		}
	}
	return tiles
}

func getSceneTile(sceneType misc.TScene, x, z float32) (tx, ty int32, nx, nz float32, err utils.IError) {
	sceneData, ok := SceneGlobal().GetSceneData(sceneType)
	if !ok {
		err = utils.NewError("not exist scene type", utils.M{
			"scene type": sceneType,
		})
		return
	}
	conf := sceneData.conf
	nx = utils.ClampFloat(1, float32(conf.Width)-1, x)
	nz = utils.ClampFloat(1, float32(conf.Length)-1, z)
	tx = int32(math.Floor(float64(nx) / float64(conf.TileSize)))
	ty = int32(math.Floor(float64(nz) / float64(conf.TileSize)))
	return
}

func GetSceneTileTagByPosition(sceneType intfc.TScene, x, y float32) (string, utils.IError) {
	tx, ty, _, _, err := getSceneTile(sceneType, x, y)
	if err != nil {
		return "", err
	}
	return mergeSceneTileKey(tx, ty), nil
}

// GetSceneVisionTileAroundTags 获取场景视野的九宫格标签
func GetSceneVisionTileAroundTags(sceneType misc.TScene, tx, ty int32) (aroundTags []string, tag string, err utils.IError) {
	sceneData, ok := SceneGlobal().GetSceneData(sceneType)
	if !ok {
		err = utils.NewError("not exist scene type", utils.M{
			"scene type": sceneType,
		})
		return
	}
	aroundTags, ok = sceneData.visionAroundTags[mergeSceneTileKey(tx, ty)]
	if !ok {
		err = utils.NewError("not exist scene tile", utils.M{
			"tile x": tx,
			"tile y": ty,
		})
		return
	}
	tag = mergeSceneTileKey(tx, ty)
	return
}

// GetSceneEventTileAroundTags 获取场景事件的九宫格标签
func GetSceneEventTileAroundTags(sceneType misc.TScene, tx, ty int32) (aroundTags []string, tag string, err utils.IError) {
	sceneData, ok := SceneGlobal().GetSceneData(sceneType)
	if !ok {
		err = utils.NewError("not exist scene type", utils.M{
			"scene type": sceneType,
		})
		return
	}
	tag = mergeSceneTileKey(tx, ty)
	aroundTags, ok = sceneData.eventAroundTags[tag]
	if !ok {
		err = utils.NewError("not exist scene tile", utils.M{
			"tile x": tx,
			"tile y": ty,
		})
		return
	}
	return
}


func GetSceneTileChanged(sceneType misc.TScene, otx, oty, itx, ity int32) (out, in, un []string) {
	o, _, _ := GetSceneVisionTileAroundTags(sceneType, otx, oty)
	i, _, _ := GetSceneVisionTileAroundTags(sceneType, itx, ity)
	om := make(map[string]struct{}, len(out))
	for _, tag := range o {
		om[tag] = struct{}{}
	}
	in = make([]string, 0, len(in))
	un = make([]string, 0, len(in))
	for _, tag := range i {
		if _, ok := om[tag]; ok {
			delete(om, tag)
			un = append(un, tag)
			continue
		}
		in = append(in, tag)
	}
	out = make([]string, 0, len(om))
	for tag := range om {
		out = append(out, tag)
	}
	return
}