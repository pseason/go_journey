package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
	"hm/pkg/misc/tsv"
	"math/rand"
	"time"
)

const (
	_MaxGatherDistant = 5
)

type IComResource interface {
	intfc.IActorComponent
	GetTplId() int32
	SetTplId(tplId int32)
	GetCsv() bool
	ConsumeEnergy() int32
	TestGather(actorId, cityId, kingdomId int64) bool
	TestDistant(dis float32) bool
	TestWeather(currentWeatherId int32) bool
	TestEmpty() bool
	TestReward(practisedLevel,currentWeatherId,count int32) (rewards map[int32]int32, complete bool)
	Rebirth()
}

type ActorComResource struct {
	scene.ActorComponent
	TplId            int32
	RemainSrcToCount map[int32]int32 //剩余资源模版id对应数量
	csv              tsv.CollectTsv
}

func (r *ActorComResource) TestGather(actorId, cityId, kingdomId int64) bool {
	panic("implement me")
}

func (r *ActorComResource) Type() intfc.TActorComponent {
	return Com_Resource
}


// 重置资源余量
func (r *ActorComResource) resetRemain() {
	if r.csv.Id == 0 {
		return
	}
	r.RemainSrcToCount = make(map[int32]int32)
	for _, prop := range r.csv.Props {
		if len(prop) != 5 {
			app.Log().Error("get resource props data failed",utils.M{"name": r.csv.Name,"error":"length error"})
			continue
		}
		r.RemainSrcToCount[prop[0]] = prop[3]
	}
}


func (r *ActorComResource) GetTplId() int32 {
	return r.TplId
}

func (r *ActorComResource) SetTplId(tplId int32) {
	r.TplId = tplId
	r.GetCsv()
	if r.csv.Id == 0 {
		app.Log().Error("get csv failed", utils.M{"error":"not exist template id","tplId": r.TplId})
		return
	}
	r.resetRemain()
}

//获取配置
func (r *ActorComResource) GetCsv() bool {
	if r.csv.Id <= 0 {
		collectCsvManager := tsv.GetTsvManager(tsv.Collect).(*tsv.CollectTsvManager).TsvMap
		r.csv = *collectCsvManager[r.TplId]
		if r.csv.Id <= 0 {
			return false
		}
	}
	return true
}

//获取熟练度概率配置
func (r *ActorComResource) getRandomCsv(PractisedId int32) *tsv.CollectProbabilityTsv {
	randomConf := tsv.GetTsvManager(tsv.CollectProbability).(*tsv.CollectProbabilityTsvManager).TsvMap
	return randomConf[PractisedId]
}

//精力是否足够
func (r *ActorComResource) ConsumeEnergy() int32 {
	return r.csv.ConsumeEnergy
}

func (r *ActorComResource) TestDistant(dis float32) bool {
	return dis > _MaxGatherDistant
}

//是否空了
func (r *ActorComResource) TestEmpty() bool {
	return len(r.RemainSrcToCount) == 0
}

//当前天气资源是否出现
func (r *ActorComResource) TestWeather(currentWeatherId int32) (satisfy bool){
	if len(r.csv.Weather)<=0 {
		satisfy = true
	}else{
		for w:= range r.csv.Weather {
			if w == int(currentWeatherId) {
				satisfy = true
			}
		}
	}
	return
}

//次数是否足够
func (r *ActorComResource) TestReward(practisedLevel,currentWeatherId ,count int32) (rewards map[int32]int32, complete bool) {
	if count < r.csv.Hp {
		return nil, false
	}
	rewards = r.getReward(practisedLevel,currentWeatherId)
	complete = true
	return
}
func (r *ActorComResource) Rebirth() {
	//todo 加载资源配置，重生资源loadActor
}

func (r *ActorComResource) PractisedRandom(practisedId ,practisedLevel int32) bool {
	randomConf := tsv.GetTsvManager(tsv.CollectProbability).(*tsv.CollectProbabilityTsvManager).TsvMap
	var PractisedNum int32 = 0
	practiseConf,ok := randomConf[practisedId]
	if !ok {
		app.Log().Error("get conf failed",utils.M{"randomConf":randomConf,"practisedId":practisedId})
		return false
	}
	switch practisedLevel {
	case 0,1:
		PractisedNum = practiseConf.Probability1
	case 2:
		PractisedNum = practiseConf.Probability2
	case 3:
		PractisedNum = practiseConf.Probability3
	case 4:
		PractisedNum = practiseConf.Probability4
	case 5:
		PractisedNum = practiseConf.Probability5
	case 6:
		PractisedNum = practiseConf.Probability6
	}
	rand.Seed(time.Now().Unix())
	randNum := rand.Int63n(100) + 1
	if randNum <= int64(PractisedNum) {
		return true
	}
	return false
}

//采集资源
func (r *ActorComResource) getReward(practisedLevel ,currentWeatherId int32) (rewards map[int32]int32) {
	rewards = make(map[int32]int32, len(r.csv.Props)+len(r.csv.PropsExtra))
	//普通奖励
	r.getCommonProp(practisedLevel,rewards)
	//额外奖励1
	r.getExtraProp1(practisedLevel,rewards)
	//额外奖励2
	r.getExtraProp2(practisedLevel ,currentWeatherId,rewards)
	app.Log().Debug("gather", utils.M{"rewards":rewards})
	return
}

//获取资源的最大/最小值
func (r *ActorComResource) getCsvPropRange(csv tsv.CollectTsv, propId int32) (min, max,PractisedId int32) {
	for _, prop := range csv.Props {
		if prop[0] == propId {
			return prop[1], prop[2],prop[len(prop)-1]
		}
	}
	return
}

//获取常规奖励
func (r *ActorComResource) getCommonProp(practisedLevel int32,rewards map[int32]int32) {
	for rewardId, remainCount := range r.RemainSrcToCount {
		min, max,practisedId := r.getCsvPropRange(r.csv, rewardId)
		ok := r.PractisedRandom(practisedId,practisedLevel)
		if !ok {
			continue
		}

		if remainCount <= min {
			rewards[rewardId] = remainCount
			delete(r.RemainSrcToCount, rewardId)
			continue
		}

		rewardCount := min + rand.Int31n(utils.MinInt32(max+1, remainCount)-min)

		rewards[rewardId] = rewardCount
		if rewardCount == remainCount {
			delete(r.RemainSrcToCount, rewardId)
			continue
		}
		r.RemainSrcToCount[rewardId] = remainCount - rewardCount
	}
}

//获取额外奖励
func (r *ActorComResource) getExtraProp1(practisedLevel int32,rewards map[int32]int32) {
	for _, data := range r.csv.PropsExtra {
		practisedId :=data[len(data)-1]
		ok := r.PractisedRandom(practisedId,practisedLevel)
		if !ok {
			continue
		}
		//概率
		probability := data[3]
		p := rand.Int31n(100)
		if p > probability {
			continue
		}
		id := data[0]
		min := data[1]
		max := data[2]
		rewards[id] = min + rand.Int31n(max-min+1)
	}
}

//受天气系统-获取特殊奖励[[2032001,1,3,2,3,4,5,0]]
func (r *ActorComResource) getExtraProp2(practisedLevel ,currentWeatherId int32,rewards map[int32]int32) {
	if len(r.csv.PropsWeather)>0{
		for _, data := range r.csv.PropsWeather {
			cIndex := len(data)-1
			practisedId :=data[cIndex]
			ok := r.PractisedRandom(practisedId,practisedLevel)
			if !ok {
				continue
			}
			//是否在天气里出现
			weatherPrize := 0
			weatherSlice := data[3:cIndex-1]
			for w :=range weatherSlice{
				if int(currentWeatherId) == w{
					weatherPrize = 1
				}
			}
			if weatherPrize == 0{
				continue
			}
			//概率
			id := data[0]
			min := data[1]
			max := data[2]
			rewards[id] = min + rand.Int31n(max-min+1)
		}
	}

}

func (r *ActorComResource) Start() utils.IError {
	err := app.SceneCache().LoadActorComponent(r.Actor().Id(), r)
	return err
}

func (r *ActorComResource) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(r.Actor().Id(), r)
}