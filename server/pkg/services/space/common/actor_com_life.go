package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
)

type ActorComLife struct {
	scene.ActorComponent
	NickName  string //昵称
	Level     int32  //等级
	Gender    bool   //性别
	Figure    int32  //头像
	TeamId    int64  //队伍id
	FactionId int64  //阵营
}

func (l *ActorComLife) Type() intfc.TActorComponent {
	return Com_Life
}

func (l *ActorComLife) OnEnterScene() {
}

func (l *ActorComLife) OnExitScene() {
}

func (l *ActorComLife) Start() utils.IError {
	return app.SceneCache().LoadActorComponent(l.Actor().Id(), l)
}

func (l *ActorComLife) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(l.Actor().Id(), l)
}
