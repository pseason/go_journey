package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
)

type ActorComEquip struct {
	scene.ActorComponent
	items map[int32]int32
}

func (e *ActorComEquip) Type() intfc.TActorComponent {
	return Com_Equip
}

func (e *ActorComEquip) SetItem(pos int32, equipId int32) {
	if equipId == 0 {
		delete(e.items, pos)
		return
	}
	e.items[pos] = equipId
}

func (e *ActorComEquip) Start() utils.IError {
	return app.SceneCache().LoadActorComponent(e.Actor().Id(), e)
}

func (e *ActorComEquip) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(e.Actor().Id(), e)
}
