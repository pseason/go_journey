package common

type EveDeath struct {
	ActorId int64
	IsDeath int32
}
func NewEveDeath(actorId int64,isDeath int32) *EveDeath {
	return &EveDeath{
		ActorId:actorId,
		IsDeath:isDeath,
	}
}
