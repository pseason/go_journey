package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
	"hm/pkg/misc"
)

type ActorComNode struct {
	scene.ActorComponent
	nodeType misc.TNode
	nodeId int64
}

func (a *ActorComNode) NodeType() misc.TNode {
	return a.nodeType
}

func (a *ActorComNode) NodeId() int64 {
	return a.nodeId
}

func (a *ActorComNode) Start() utils.IError {
	return app.SceneCache().LoadActorComponent(a.Actor().Id(), a)
}

func (a *ActorComNode) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(a.Actor().Id(), a)
}