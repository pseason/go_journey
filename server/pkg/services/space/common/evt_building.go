package common

type EvtBuildingInfoChange struct {
	Id        int64
	UpMap map[int32]int32
}

func NewEvtBuildingInfo(buildingId int64,upMap map[int32]int32) *EvtBuildingInfoChange {
	return &EvtBuildingInfoChange{
		Id:        buildingId,
		UpMap: upMap,
	}
}