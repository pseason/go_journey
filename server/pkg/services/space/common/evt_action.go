package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
)

func NewEvtAction(actorId,targetId int64, actionId int32) *EvtBuildAction {
	return &EvtBuildAction{
		Id:       actorId,
		ActionId: actionId,
		TargetId: targetId,
	}
}

type EvtBuildAction struct {
	Id       int64
	ActionId int32
	TargetId int64
}

func (b *EvtBuildAction) SyncActorAction(actor intfc.IActor) {
	c, _ := actor.GetComponent(Com_Transform)
	transform := c.(*ActorComTransform)
	tx, ty := transform.tileX, transform.tileY
	sceneType := actor.Scene().Type()

	tags, _, e := GetSceneVisionTileAroundTags(sceneType, tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}
	actor.Scene().DispatchActorEventByTags(Evt_Build_Action, b, tags...)

}
