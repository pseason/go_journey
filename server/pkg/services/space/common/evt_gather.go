package common

type EvtGatherChange struct {
	ActorId int64
	ResId   int64
	Empty   int32
}

func NewEvtGatherChange(ActorId,ResId int64,Empty int32) *EvtGatherChange {
	return &EvtGatherChange{
		ActorId:ActorId,
		ResId:ResId,
		Empty:Empty,
	}
}
