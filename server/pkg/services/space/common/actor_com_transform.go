package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/scene"
	"95eh.com/eg/utils"
	"time"
)

type ActorComTransform struct {
	scene.ActorComponent
	Position           utils.Vec3
	Forward            utils.Vec3
	MoveSpeed          float32
	tileX              int32
	tileY              int32
	moving             bool
	moveTickId         int64
	moveLastTime       time.Time
	moveTargetPosition utils.Vec3
}

func (t *ActorComTransform) Tile() (int32, int32) {
	return t.tileX, t.tileY
}

func (t *ActorComTransform) SetTile(tx, ty int32) {
	t.tileX, t.tileY = tx, ty
}

func (t *ActorComTransform) Moving() bool {
	return t.moving
}

func (t *ActorComTransform) SetMoving(moving bool) {
	t.moving = moving
}

func (t *ActorComTransform) MoveTickId() int64 {
	return t.moveTickId
}

func (t *ActorComTransform) SetMoveTickId(moveTickId int64) {
	t.moveTickId = moveTickId
}

func (t *ActorComTransform) MoveTargetPosition() utils.Vec3 {
	return t.moveTargetPosition
}

func (t *ActorComTransform) SetMoveTargetPosition(moveTargetPosition utils.Vec3) {
	t.moveTargetPosition = moveTargetPosition
}

func (t *ActorComTransform) Type() intfc.TActorComponent {
	return Com_Transform
}

// TickMove 先判断是否在移动,在调用该接口
func (t *ActorComTransform) TickMove() {
	now := time.Now()
	dur := float32(now.Sub(t.moveLastTime).Milliseconds()) / 1000
	speed := dur * t.MoveSpeed
	ox := t.Forward.X * speed
	oy := t.Forward.Y * speed
	oz := t.Forward.Z * speed
	t.moveLastTime = now
	t.Move(ox, oy, oz, t.Forward.X, t.Forward.Y, t.Forward.Z)
}

func (t *ActorComTransform) StartMove(x, y, z, fx, fy, fz float32) {
	if t.moving {
		t.TickMove()
	} else {
		t.moving = true
		t.moveLastTime = time.Now()
		t.Actor().AddTicker(onActorTickMoving)
	}
	app.Log().Debug("start move:", utils.M{"x": x, "y": y, "z": z, "fx": fx, "fy": fy, "fz": fz})
	t.Position = utils.Vec3{
		X: x,
		Y: y,
		Z: z,
	}
	t.Forward = utils.Vec3{
		X: fx,
		Y: fy,
		Z: fz,
	}
	evt := &EvtMoveStart{
		Id:        t.Actor().Id(),
		PositionX: t.Position.X,
		PositionY: t.Position.Y,
		PositionZ: t.Position.Z,
		ForwardX:  t.Forward.X,
		ForwardY:  t.Forward.Y,
		ForwardZ:  t.Forward.Z,
	}
	tx, ty := t.tileX, t.tileY
	tags, _, e := GetSceneEventTileAroundTags(t.Actor().Scene().Type(), tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}
	t.Actor().Scene().DispatchActorEventByTags(Evt_MoveStart, evt, tags...)
}

func (t *ActorComTransform) StopMove(x, y, z float32) {
	if !t.moving {
		return
	}
	t.TickMove()
	t.moving = false
	t.Actor().RemoveTicker(onActorTickMoving)

	//if math.Abs(float64(t.Position.X - x)) < 0.1 && math.Abs(float64(t.Position.Z - z)) < 0.1 {
	t.Position.X = x
	t.Position.Z = z
	//}

	tx, ty := t.tileX, t.tileY
	evt := &EvtMoveStop{
		Id:        t.Actor().Id(),
		Type:      t.Actor().Type(),
		PositionX: t.Position.X,
		PositionY: t.Position.Y,
		PositionZ: t.Position.Z,
	}

	tags, _, e := GetSceneEventTileAroundTags(t.Actor().Scene().Type(), tx, ty)
	if e != nil {
		app.Log().Error(e.Error(), e.Params())
		return
	}
	t.Actor().Scene().DispatchActorEventByTags(Evt_MoveStop, evt, tags...)
}

func (t *ActorComTransform) Move(ox, oy, oz, fx, fy, fz float32) {
	actor := t.Actor()
	actorType := t.Actor().Type()
	actorId := actor.Id()
	scene := actor.Scene()
	sceneType := scene.Type()
	x, y, z := t.Position.X+ox, t.Position.Y+oy, t.Position.Z+oz
	tx, ty, x, z, _ := getSceneTile(sceneType, x, z)
	t.Position.X, t.Position.Y, t.Position.Z = x, y, z
	t.Forward.X, t.Forward.Y, t.Forward.Z = fx, fy, fz
	moveEvt := &EvtMove{
		Id:        actorId,
		Type:      actorType,
		PositionX: x,
		PositionY: y,
		PositionZ: z,
		ForwardX:  fx,
		ForwardY:  fy,
		ForwardZ:  fz,
	}
	tags, tag, _ := GetSceneVisionTileAroundTags(sceneType, tx, ty)
	//所在格子没变化
	if tx == t.tileX && ty == t.tileY {
		t.Actor().Scene().DispatchActorEventByTags(Evt_Move, moveEvt, tags...)
		return
	}
	o_tag := mergeSceneTileKey(t.tileX, t.tileY)
	out, in, un := GetSceneTileChanged(sceneType, t.tileX, t.tileY, tx, ty)
	t.tileX, t.tileY = tx, ty
	scene.RemoveActorTags(actorId, o_tag)
	eveInvisible := NewEvtInvisible(actor)
	scene.ActionActorByTags(func(actor2 intfc.IActor) {
		actor.ProcessEvent(Evt_Invisible, NewEvtInvisible(actor2))
		actor2.ProcessEvent(Evt_Invisible, eveInvisible)
	}, out...)
	var action intfc.ActionActor
	if t.moving {
		action = func(actor2 intfc.IActor) {
			actor2.ProcessEvent(Evt_MoveStart, &EvtMoveStart{
				Id:        actorId,
				Type:      actorType,
				PositionX: t.Position.X,
				PositionY: t.Position.Y,
				PositionZ: t.Position.Z,
				ForwardX:  t.Forward.X,
				ForwardY:  t.Forward.Y,
				ForwardZ:  t.Forward.Z,
			})
		}
	} else {
		action = func(actor2 intfc.IActor) {
			actor2.ProcessEvent(Evt_ChangePos, &EvtChangePos{
				Id:        actorId,
				PositionX: t.Position.X,
				PositionY: t.Position.Y,
				PositionZ: t.Position.Z,
				ForwardX:  t.Forward.X,
				ForwardY:  t.Forward.Y,
				ForwardZ:  t.Forward.Z,
			})
		}
	}
	scene.ActionActorByTags(func(actor2 intfc.IActor) {
		actor2.ProcessEvent(Evt_Visible, NewEvtVisible(actor))
		action(actor2)
		actor.ProcessEvent(Evt_Visible, NewEvtVisible(actor2))
		//todo 视野带状态后不需要一下代码
		com, _ := actor2.GetComponent(Com_Transform)
		if com.(*ActorComTransform).Moving() {
			actor.ProcessEvent(Evt_MoveStart, NewEvtMoveStart(actor2))
		}
	}, in...)
	scene.DispatchActorEventByTags(Evt_Move, moveEvt, un...)
	scene.AddActorTags(actorId, tag)
}

func (t *ActorComTransform) Start() utils.IError {
	return app.SceneCache().LoadActorComponent(t.Actor().Id(), t)
}

func (t *ActorComTransform) Dispose() utils.IError {
	return app.SceneCache().SaveActorComponent(t.Actor().Id(), t)
}
