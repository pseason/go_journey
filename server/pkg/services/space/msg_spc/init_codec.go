package msg_spc

import (
	"95eh.com/eg/intfc"
)

// 绑定与服务通信的消息协议号和消息结构体的映射
func InitServiceCodec(discovery intfc.IMDiscovery) {
	InitCodecForAi(discovery)
	InitCodecForSpace(discovery)
}
