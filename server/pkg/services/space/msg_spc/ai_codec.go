package msg_spc

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdAiMonsterCreate        MsgCode = uint32(svc.Ai)*10000 + 1 // Req：创建野怪；Res：响应创建野怪；
	CdAiMonsterRemove        MsgCode = uint32(svc.Ai)*10000 + 2 // Req：野怪移除；Res：响应野怪移除；
	CdAiSceneMonsterClearAll MsgCode = uint32(svc.Ai)*10000 + 3 // Req：清空场景内的所有野怪；Res：响应清空场景内的所有野怪；
	CdAiSceneMonsterInit     MsgCode = uint32(svc.Ai)*10000 + 4 // Req：初始化场景内的所有野怪；Res：响应初始化场景内的所有野怪；
)

func InitCodecForAi(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdAiMonsterCreate,
		func() interface{} {
			return &ReqAiMonsterCreate{}
		},
		func() interface{} {
			return &ResAiMonsterCreate{}
		})
	discovery.BindCoderFac(CdAiMonsterRemove,
		func() interface{} {
			return &ReqAiMonsterRemove{}
		},
		func() interface{} {
			return &ResAiMonsterRemove{}
		})
	discovery.BindCoderFac(CdAiSceneMonsterClearAll,
		func() interface{} {
			return &ReqAiSceneMonsterClearAll{}
		},
		func() interface{} {
			return &ResAiSceneMonsterClearAll{}
		})
	discovery.BindCoderFac(CdAiSceneMonsterInit,
		func() interface{} {
			return &ReqAiSceneMonsterInit{}
		},
		func() interface{} {
			return &ResAiSceneMonsterInit{}
		})
}
