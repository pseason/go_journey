package msg_spc

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdActorEnterSpace         MsgCode = uint32(svc.Space)*10000 + 1  // Req：；Res：；
	CdActorExitSpace          MsgCode = uint32(svc.Space)*10000 + 2  // Req：；Res：；
	CdSpawnScene              MsgCode = uint32(svc.Space)*10000 + 3  // Req：；Res：；
	CdRecycleScene            MsgCode = uint32(svc.Space)*10000 + 4  // Req：；Res：；
	CdActorEnterScene         MsgCode = uint32(svc.Space)*10000 + 5  // Req：；Res：；
	CdActorExitScene          MsgCode = uint32(svc.Space)*10000 + 6  // Req：；Res：；
	CdActorMoveStart          MsgCode = uint32(svc.Space)*10000 + 7  // Req：；Res：；
	CdEveActorMoveStart       MsgCode = uint32(svc.Space)*10000 + 8  // Eve：；
	CdActorMoveStop           MsgCode = uint32(svc.Space)*10000 + 9  // Req：；Res：；
	CdEveActorMoveStop        MsgCode = uint32(svc.Space)*10000 + 10 // Eve：；
	CdActorForwardChange      MsgCode = uint32(svc.Space)*10000 + 11 // Req：；Res：；
	CdEveActorForwardChange   MsgCode = uint32(svc.Space)*10000 + 12 // Eve：；
	CdActorPositionChange     MsgCode = uint32(svc.Space)*10000 + 13 // Req：；Res：；
	CdEveActorVisible         MsgCode = uint32(svc.Space)*10000 + 14 // Eve：；
	CdEveActorInvisible       MsgCode = uint32(svc.Space)*10000 + 15 // Eve：；
	CdActorsEnterSpace        MsgCode = uint32(svc.Space)*10000 + 16 // Req：；Res：；
	CdActorBuildingInfoChange MsgCode = uint32(svc.Space)*10000 + 17 // Req：；Res：；
	CdEveBuildingInfoChange   MsgCode = uint32(svc.Space)*10000 + 18 // Eve：；
	CdEveMove                 MsgCode = uint32(svc.Space)*10000 + 19 // Eve：；
	CdActorGatherResource     MsgCode = uint32(svc.Space)*10000 + 20 // Req：；Res：；
	CdEveActorGatherChange    MsgCode = uint32(svc.Space)*10000 + 21 // Eve：；
	CdActorBuildingAction     MsgCode = uint32(svc.Space)*10000 + 22 // Req：；Res：；
	CdEveActorBuildAction     MsgCode = uint32(svc.Space)*10000 + 23 // Eve：；
	CdEveActorResourceState   MsgCode = uint32(svc.Space)*10000 + 24 // Eve：；
	CdSceneWeather            MsgCode = uint32(svc.Space)*10000 + 25 // Req：；Res：；
	CdEveSceneWeatherInfo     MsgCode = uint32(svc.Space)*10000 + 26 // Eve：；
	CdEveActorPositionChange  MsgCode = uint32(svc.Space)*10000 + 27 // Eve：；
	CdEveActorDeath           MsgCode = uint32(svc.Space)*10000 + 28 // Eve：；
)

func InitCodecForSpace(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdActorEnterSpace,
		func() interface{} {
			return &ReqActorEnterSpace{}
		},
		func() interface{} {
			return &ResActorEnterSpace{}
		})
	discovery.BindCoderFac(CdActorExitSpace,
		func() interface{} {
			return &ReqActorExitSpace{}
		},
		func() interface{} {
			return &ResActorExitSpace{}
		})
	discovery.BindCoderFac(CdSpawnScene,
		func() interface{} {
			return &ReqSpawnScene{}
		},
		func() interface{} {
			return &ResSpawnScene{}
		})
	discovery.BindCoderFac(CdRecycleScene,
		func() interface{} {
			return &ReqRecycleScene{}
		},
		func() interface{} {
			return &ResRecycleScene{}
		})
	discovery.BindCoderFac(CdActorEnterScene,
		func() interface{} {
			return &ReqActorEnterScene{}
		},
		func() interface{} {
			return &ResActorEnterScene{}
		})
	discovery.BindCoderFac(CdActorExitScene,
		func() interface{} {
			return &ReqActorExitScene{}
		},
		func() interface{} {
			return &ResActorExitScene{}
		})
	discovery.BindCoderFac(CdActorMoveStart,
		func() interface{} {
			return &ReqActorMoveStart{}
		},
		func() interface{} {
			return &ResActorMoveStart{}
		})
	discovery.BindCoderFac(CdEveActorMoveStart,
		nil,
		func() interface{} {
			return &EveActorMoveStart{}
		})
	discovery.BindCoderFac(CdActorMoveStop,
		func() interface{} {
			return &ReqActorMoveStop{}
		},
		func() interface{} {
			return &ResActorMoveStop{}
		})
	discovery.BindCoderFac(CdEveActorMoveStop,
		nil,
		func() interface{} {
			return &EveActorMoveStop{}
		})
	discovery.BindCoderFac(CdActorForwardChange,
		func() interface{} {
			return &ReqActorForwardChange{}
		},
		func() interface{} {
			return &ResActorForwardChange{}
		})
	discovery.BindCoderFac(CdEveActorForwardChange,
		nil,
		func() interface{} {
			return &EveActorForwardChange{}
		})
	discovery.BindCoderFac(CdActorPositionChange,
		func() interface{} {
			return &ReqActorPositionChange{}
		},
		func() interface{} {
			return &ResActorPositionChange{}
		})
	discovery.BindCoderFac(CdEveActorVisible,
		nil,
		func() interface{} {
			return &EveActorVisible{}
		})
	discovery.BindCoderFac(CdEveActorInvisible,
		nil,
		func() interface{} {
			return &EveActorInvisible{}
		})
	discovery.BindCoderFac(CdActorsEnterSpace,
		func() interface{} {
			return &ReqActorsEnterSpace{}
		},
		func() interface{} {
			return &ResActorsEnterSpace{}
		})
	discovery.BindCoderFac(CdActorBuildingInfoChange,
		func() interface{} {
			return &ReqActorBuildingInfoChange{}
		},
		func() interface{} {
			return &ResActorBuildingInfoChange{}
		})
	discovery.BindCoderFac(CdEveBuildingInfoChange,
		nil,
		func() interface{} {
			return &EveBuildingInfoChange{}
		})
	discovery.BindCoderFac(CdEveMove,
		nil,
		func() interface{} {
			return &EveMove{}
		})
	discovery.BindCoderFac(CdActorGatherResource,
		func() interface{} {
			return &ReqActorGatherResource{}
		},
		func() interface{} {
			return &ResActorGatherResource{}
		})
	discovery.BindCoderFac(CdEveActorGatherChange,
		nil,
		func() interface{} {
			return &EveActorGatherChange{}
		})
	discovery.BindCoderFac(CdActorBuildingAction,
		func() interface{} {
			return &ReqActorBuildingAction{}
		},
		func() interface{} {
			return &ResActorBuildingAction{}
		})
	discovery.BindCoderFac(CdEveActorBuildAction,
		nil,
		func() interface{} {
			return &EveActorBuildAction{}
		})
	discovery.BindCoderFac(CdEveActorResourceState,
		nil,
		func() interface{} {
			return &EveActorResourceState{}
		})
	discovery.BindCoderFac(CdSceneWeather,
		func() interface{} {
			return &ReqSceneWeather{}
		},
		func() interface{} {
			return &ResSceneWeather{}
		})
	discovery.BindCoderFac(CdEveSceneWeatherInfo,
		nil,
		func() interface{} {
			return &EveSceneWeatherInfo{}
		})
	discovery.BindCoderFac(CdEveActorPositionChange,
		nil,
		func() interface{} {
			return &EveActorPositionChange{}
		})
	discovery.BindCoderFac(CdEveActorDeath,
		nil,
		func() interface{} {
			return &EveActorDeath{}
		})
}
