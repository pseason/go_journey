package msg_spc

import "95eh.com/eg/utils"

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Ai

// @MessageCode=1 创建野怪
type ReqAiMonsterCreate struct {
	TemplateId int32
	Position   utils.Vec3
}

// @MessageCode=1 响应创建野怪
type ResAiMonsterCreate struct {
	MonsterId int64
}

// @MessageCode=2 野怪移除
type ReqAiMonsterRemove struct {
	MonsterId int64
}

// @MessageCode=2 响应野怪移除
type ResAiMonsterRemove struct {
}

// @MessageCode=3 清空场景内的所有野怪
type ReqAiSceneMonsterClearAll struct {
}

// @MessageCode=3 响应清空场景内的所有野怪
type ResAiSceneMonsterClearAll struct {
}

// @MessageCode=4 初始化场景内的所有野怪
type ReqAiSceneMonsterInit struct {
}

// @MessageCode=4 响应初始化场景内的所有野怪
type ResAiSceneMonsterInit struct {
	ActorIds []int64
}
