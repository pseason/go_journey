package msg_spc

import (
	"95eh.com/eg/intfc"
	"hm/pkg/misc"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Space

type EvtBuilding struct {
	BuildingType  int32
	BuildingState int32
	Level         int32
}

type EvtResource struct {
	ResId int32
	Empty int32
}

// ReqActorEnterSpace 请求对象进入空间
// @MessageCode=1
type ReqActorEnterSpace struct {
}

// ResActorEnterSpace 响应对象进入空间
// @MessageCode=1
type ResActorEnterSpace struct {
}

// ReqActorExitSpace 请求对象进入空间
// @MessageCode=2
type ReqActorExitSpace struct {
}

// ResActorExitSpace 响应对象进入空间
// @MessageCode=2
type ResActorExitSpace struct {
}

// ReqSpawnScene 请求对象进入空间
// @MessageCode=3
type ReqSpawnScene struct {
	SceneType misc.TScene
	SceneId   int64
}

// ResSpawnScene 响应对象进入空间
// @MessageCode=3
type ResSpawnScene struct {
}

// ReqRecycleScene 请求对象进入空间
// @MessageCode=4
type ReqRecycleScene struct {
	SceneType misc.TScene
	SceneId   int64
}

// ResRecycleScene 响应对象进入空间
// @MessageCode=4
type ResRecycleScene struct {
}

// ReqActorEnterScene 请求对象进入空间
// @MessageCode=5
type ReqActorEnterScene struct {
	SceneType misc.TScene
	SceneId   int64
	PositionX float32
	PositionY float32
	PositionZ float32
}

// ResActorEnterScene 响应对象进入空间
// @MessageCode=5
type ResActorEnterScene struct {
}

// ReqActorExitScene 请求对象离开空间
// @MessageCode=6
type ReqActorExitScene struct {
}

// ResActorExitScene 响应对象离开空间
// @MessageCode=6
type ResActorExitScene struct {
}

// ReqActorMoveStart 请求开始移动
// @MessageCode=7
type ReqActorMoveStart struct {
	ActorId int64
	PosX    float32
	PosY    float32
	PosZ    float32
	ForX    float32
	ForY    float32
	ForZ    float32
}

// ResActorMoveStart 响应开始移动
// @MessageCode=7
type ResActorMoveStart struct {
	PosX float32
	PosY float32
	PosZ float32
	ForX float32
	ForY float32
	ForZ float32
}

// EveActorMoveStart 事件开始移动
// @MessageCode=8
type EveActorMoveStart struct {
	ActorId  int64
	TargetId int64
	Type     intfc.TActor
	PosX     float32
	PosY     float32
	PosZ     float32
	ForX     float32
	ForY     float32
	ForZ     float32
}

// ReqActorMoveStop 请求停止移动
// @MessageCode=9
type ReqActorMoveStop struct {
	PosX float32
	PosY float32
	PosZ float32
}

// ResActorMoveStop 响应停止移动
// @MessageCode=9
type ResActorMoveStop struct {
	PosX float32
	PosY float32
	PosZ float32
}

// EveActorMoveStop 事件停止移动
// @MessageCode=10
type EveActorMoveStop struct {
	ActorId  int64
	TargetId int64
	Type     intfc.TActor
	PosX     float32
	PosY     float32
	PosZ     float32
}

// ReqActorForwardChange 请求朝向变更
// @MessageCode=11
type ReqActorForwardChange struct {
	ActorId int64
	ForX    float32
	ForY    float32
	ForZ    float32
}

// ResActorForwardChange 响应朝向变更
// @MessageCode=11
type ResActorForwardChange struct {
	Success bool
}

// EveActorForwardChange 事件朝向变更
// @MessageCode=12
type EveActorForwardChange struct {
	ActorId int64
	ForX    int32
	ForY    int32
	ForZ    int32
}

// ReqActorPositionChange 请求位置变更
// @MessageCode=13
type ReqActorPositionChange struct {
	ActorId int64
	PosX    float32
	PosY    float32
	PosZ    float32
}

// ResActorPositionChange 响应位置变更
// @MessageCode=13
type ResActorPositionChange struct {
}

// EveActorVisible 响应位置变更
// @MessageCode=14
type EveActorVisible struct {
	ActorId     int64
	TargetId    int64
	TargetType  intfc.TActor
	PosX        float32
	PosY        float32
	PosZ        float32
	ForX        float32
	ForY        float32
	ForZ        float32
	Nickname    string
	EvtBuilding *EvtBuilding
	EvtResource *EvtResource
}

// EveActorInvisible 响应位置变更
// @MessageCode=15
type EveActorInvisible struct {
	ActorId  int64
	TargetId int64
}

// ReqActorsEnterSpace 请求对象批量进入空间
// @MessageCode=16
type ReqActorsEnterSpace struct {
	Actors []int64
}

// ResActorsEnterSpace 响应对象批量进入空间
// @MessageCode=16
type ResActorsEnterSpace struct {
}

// ReqActorBuildingInfoChange 请求建筑基本属性变更
// @MessageCode=17
type ReqActorBuildingInfoChange struct {
	UpMap map[int32]int32
}

// ResActorBuildingInfoChange 响应建筑基本属性变更
// @MessageCode=17
type ResActorBuildingInfoChange struct {
}

// EveBuildingInfoChange 响应建筑基础数据变更
// @MessageCode=18
type EveBuildingInfoChange struct {
	ActorId    int64
	BuildingId int64
	UpMap      map[int32]int32
}

// EveMove 移动数据
// @MessageCode=19
type EveMove struct {
	ActorId   int64
	TargetId  int64
	Type      intfc.TActor
	PositionX float32
	PositionY float32
	PositionZ float32
	ForwardX  float32
	ForwardY  float32
	ForwardZ  float32
}

// ReqActorGatherResource 资源采集
// @MessageCode=20
type ReqActorGatherResource struct {
	ActorId        int64
	PractisedLevel int32 //熟练度等级
	ResourceId     int64 //资源id
	CurrentEnergy  int32 //当前精力值
}

// ResActorGatherResource 资源采集
// @MessageCode=20
type ResActorGatherResource struct {
	TplId         int32 //模版id
	Rewards       map[int32]int32
	ResourceId    int32
	ConsumeEnergy int32
}

// EveActorGatherChange 响应对象采集通知
// @MessageCode=21
type EveActorGatherChange struct {
	ActorId    int64
	GatherId   int64
	ResourceId int64 //资源id
	Empty      int32 //是否为空
}

// ReqActorBuildingAction 请求投入资源动作
// @MessageCode=22
type ReqActorBuildingAction struct {
	ActorId  int64
	TargetId int64
	ActionId int32
}

// ResActorBuildingAction 响应投入资源动作
// @MessageCode=22
type ResActorBuildingAction struct {
}

// EveActorBuildAction 响应对象建筑动作
// @MessageCode=23
type EveActorBuildAction struct {
	Id       int64
	ActorId  int64
	TargetId int64
	ActionId int32
}

// EveActorResourceState 响应静态资源状态变更通知
// @MessageCode=24
type EveActorResourceState struct {
	ActorId    int64
	ResourceId int64 //资源id
	Empty      int32 //是否为空
}

// ReqSceneWeather 请求天气信息
// @MessageCode=25
type ReqSceneWeather struct {
	SceneType misc.TScene
	SceneId   int64
}

// ResSceneWeather 响应天气信息
// @MessageCode=25
type ResSceneWeather struct {
	Sun   float64
	Snow  float64
	Rain  float64
	Wind  float64
	Shade float64
}

// EveSceneWeatherInfo 响应天气变更通知
// @MessageCode=26
type EveSceneWeatherInfo struct {
	ActorId   int64
	SceneType misc.TScene
	SceneId   int64
	Sun       float64
	Snow      float64
	Rain      float64
	Wind      float64
	Shade     float64
}

// EveActorPositionChange 坐标变更
// @MessageCode=27
type EveActorPositionChange struct {
	ActorId    int64
	TargetId   int64
	TargetType intfc.TActor
	PosX       float32
	PosY       float32
	PosZ       float32
	ForX       float32
	ForY       float32
	ForZ       float32
}

// EveActorDeath 死亡推送
// @MessageCode=28
type EveActorDeath struct {
	ActorId int64
	IsDeath int32
}