package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdMailPage          MsgCode = uint32(svc.Mail)*10000 + 1  // Req：分页请求邮件；Res：响应分页邮件；
	CdMailDel           MsgCode = uint32(svc.Mail)*10000 + 2  // Req：请求删除邮件；Res：响应删除邮件；
	CdMailReceive       MsgCode = uint32(svc.Mail)*10000 + 3  // Req：请求领取邮件；Res：响应领取邮件；
	CdMailRead          MsgCode = uint32(svc.Mail)*10000 + 4  // Req：请求读取邮件；Res：响应读取邮件；
	CdMailCount         MsgCode = uint32(svc.Mail)*10000 + 5  // Req：请求邮件数量；Res：响应邮件数量；
	CdMailSend          MsgCode = uint32(svc.Mail)*10000 + 6  // Req：请求邮件发送；Res：响应邮件发送；
	CdEveMailNew        MsgCode = uint32(svc.Mail)*10000 + 7  // Eve：邮件通知；
	CdMailGet           MsgCode = uint32(svc.Mail)*10000 + 8  // Req：请求查询邮件；Res：响应邮件查询；
	CdMailAutoOperation MsgCode = uint32(svc.Mail)*10000 + 9  // Req：请求邮件数量；Res：响应邮件数量；
	CdMails             MsgCode = uint32(svc.Mail)*10000 + 10 // Req：请求玩家所有邮件；Res：响应玩家所有邮件；
	CdMailById          MsgCode = uint32(svc.Mail)*10000 + 12 // Req：请求指定邮件；Res：响应指定邮件；
)

func InitCodecForMail(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdMailPage,
		func() interface{} {
			return &ReqMailPage{}
		},
		func() interface{} {
			return &ResMailPage{}
		})
	discovery.BindCoderFac(CdMailDel,
		func() interface{} {
			return &ReqMailDel{}
		},
		func() interface{} {
			return &ResMailDel{}
		})
	discovery.BindCoderFac(CdMailReceive,
		func() interface{} {
			return &ReqMailReceive{}
		},
		func() interface{} {
			return &ResMailReceive{}
		})
	discovery.BindCoderFac(CdMailRead,
		func() interface{} {
			return &ReqMailRead{}
		},
		func() interface{} {
			return &ResMailRead{}
		})
	discovery.BindCoderFac(CdMailCount,
		func() interface{} {
			return &ReqMailCount{}
		},
		func() interface{} {
			return &ResMailCount{}
		})
	discovery.BindCoderFac(CdMailSend,
		func() interface{} {
			return &ReqMailSend{}
		},
		func() interface{} {
			return &ResMailSend{}
		})
	discovery.BindCoderFac(CdEveMailNew,
		nil,
		func() interface{} {
			return &EveMailNew{}
		})
	discovery.BindCoderFac(CdMailGet,
		func() interface{} {
			return &ReqMailGet{}
		},
		func() interface{} {
			return &ResMailGet{}
		})
	discovery.BindCoderFac(CdMailAutoOperation,
		func() interface{} {
			return &ReqMailAutoOperation{}
		},
		func() interface{} {
			return &ResMailAutoOperation{}
		})
	discovery.BindCoderFac(CdMails,
		func() interface{} {
			return &ReqMails{}
		},
		func() interface{} {
			return &ResMails{}
		})
	discovery.BindCoderFac(CdMailById,
		func() interface{} {
			return &ReqMailById{}
		},
		func() interface{} {
			return &ResMailById{}
		})
}
