package msg_dt

import (
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/social_e"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Social

// @MessageCode=1 请求添加好友申请
type ReqSocialTryApplyJoinBuddy struct {
	Cid int64
	Oid int64
}

// @MessageCode=1 响应添加好友申请
type ResSocialTryApplyJoinBuddy struct {
	AddSocial *model.Social
}

// @MessageCode=2 好友申请通知
type EveSocialBuddyApplyNotify struct {
	Cid int64
}

// @MessageCode=3 请求同意好友申请
type ReqSocialTryAgreeFriends struct {
	Cid int64
	Oid int64
}

// @MessageCode=3 响应同意好友申请
type ResSocialTryAgreeFriends struct {
	UpDataId []int64
	UpData map[string]interface{}
	AddSocial *model.Social
}

// @MessageCode=4 请求拒绝好友申请
type ReqSocialTryRejectFriends struct {
	Cid int64
	Oid int64
}

// @MessageCode=4 响应拒绝好友申请
type ResSocialTryRejectFriends struct {
	SocialId int64
}

// @MessageCode=5 好友添加成功通知
type EveSocialJoinBuddyNotify struct {
	Cid int64
	Oid int64
	SocialType int32
}

// @MessageCode=6 请求添加黑名单
type ReqSocialTryAddBlacklist struct {
	Cid int64
	Oid int64
}

// @MessageCode=6 响应添加黑名单
type ResSocialTryAddBlacklist struct {
}

// @MessageCode=7 请求添加仇人
type ReqSocialTryAddEnemy struct {
	Cid int64
	Oid int64
}

// @MessageCode=7 响应添加仇人
type ResSocialTryAddEnemy struct {
}

// @MessageCode=8 请求删除好友
type ReqSocialTryDeleteBuddy struct {
	Cid int64
	Oid int64
}

// @MessageCode=8 响应删除好友
type ResSocialTryDeleteBuddy struct {
	SocialIdList []int64
}

// @MessageCode=9 请求删除黑名单
type ReqSocialTryDeleteBlacklist struct {
	Cid int64
	Oid int64
}

// @MessageCode=9 响应删除黑名单
type ResSocialTryDeleteBlacklist struct {
	Social *model.Social
}

// @MessageCode=10 请求删除仇人
type ReqSocialTryDeleteEnemy struct {
	Cid int64
	Oid int64
}

// @MessageCode=10 响应删除仇人
type ResSocialTryDeleteEnemy struct {
	Social *model.Social
}

// @MessageCode=11 请求玩家所有社交关系
type ReqSocialGetAllSocial struct {
	Cid int64
	SocialType []social_e.FriendRelationEnum
	Page int32
	Size int32
}

// @MessageCode=11 响应玩家所有社交关系
type ResSocialGetAllSocial struct {
	SocialAll []*model.Social
	Count int32
}

// @MessageCode=12 请求玩家好友申请数量
type ReqSocialUnReadBuddyApplyCount struct {
	Cid int64
}

// @MessageCode=12 响应玩家好友申请数量
type ResSocialUnReadBuddyApplyCount struct {
	Count int32
}

// @MessageCode=13 请求玩家好友申请数量
type ReqSocialGetBuddyCidList struct {
	Cid int64
	SocialType []social_e.FriendRelationEnum
}

// @MessageCode=13 响应玩家好友申请数量
type ResSocialGetBuddyCidList struct {
	CidList []int64
}