package msg_dt

import (
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/city_e"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = City

type CityData struct {
	Id               int64
	CityName         string
	BannerProp       int32
	CityType         int32
	SantoName        string
	CurrentPlayerCap int32
	MaxPlayerCap     int32
	CityTemplateId   int32
}

type CityDetailsData struct {
	Id               int64
	Cid              int64
	CityName         string
	Lv               int32
	CurrentPlayerCap int32
	BannerProp       int32
	CityTemplateId   int32
	CityType         int32
	ProsperityNum    int32
	OccupationIdList int32
	Announcement     string
}

// @MessageCode=1 请求城邦列表
type ReqCityList struct {
}

// @MessageCode=1 响应城邦列表
type ResCityList struct {
	CityInfo []*CityData
}

// @MessageCode=2 请求城邦数量
type ReqCityGetCount struct {
}

// @MessageCode=2 响应城邦数量
type ResCityGetCount struct {
	Count int64
}

// @MessageCode=3 请求加入城邦
type ReqCityJoin struct {
	Cid    int64
	CityId int64
}

// @MessageCode=3 响应加入城邦
type ResCityJoin struct {
	CityName string
	CityTempId int32
	AddMember *model.CityMember
}

// @MessageCode=4 请求城邦所有成员
type ReqCityMemberList struct {
	CityId int64
	Page int32
	Size int32
}

// @MessageCode=4 响应城邦所有成员
type ResCityMemberList struct {
	CityMemberList []model.CityMember
	Total int32
}

// @MessageCode=5 请求设置城邦成员身份
type ReqCitySetCityMemberIdentity struct {
	CityId   int64
	Cid      int64
	Oid      int64
	Identity city_e.Identity
}

// @MessageCode=5 响应设置城邦成员身份
type ResCitySetCityMemberIdentity struct {
}

// @MessageCode=6 请求流放城邦成员
type ReqCityExileCityMember struct {
	Cid int64
	Oid int64
}

// @MessageCode=6 响应流放城邦成员
type ResCityExileCityMember struct {
}

// @MessageCode=7 请求退出城邦
type ReqCityTryExitCity struct {
	Cid int64
}

// @MessageCode=7 响应流放城邦成员
type ResCityTryExitCity struct {
	CityMemberId int64
}

// @MessageCode=8 请求加载指定城邦
type ReqCityLoadCity struct {
	CityId int64
}

// @MessageCode=8 响应加载指定城邦
type ResCityLoadCity struct {
	City *CityDetailsData
}

// @MessageCode=9 请求修改城邦数据
type ReqCityUpCityData struct {
	CityId int64
	UpMap  map[city_e.CityInfo]interface{}
}

// @MessageCode=9 响应修改城邦数据
type ResCityUpCityData struct {
}

// @MessageCode=10 请求所有对应城邦模板id的城邦id
type ReqCityIdByTemId struct {
	CityTempIds []int64
}

// @MessageCode=10 响应所有对应城邦模板id的城邦id
type ResCityIdByTemId struct {
	CityList []*model.City
}

// @MessageCode=11 城邦建筑加载事件
type EveCityBuildingLoad struct {
	CityList    []int64
}

// @MessageCode=12 请求城邦id查询模板id
type ReqCityTempIdByCityId struct {
	CityId int64
}

// @MessageCode=12 响应城邦id查询模板id
type ResCityTempIdByCityId struct {
	CityTempId int64
}