package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdTryCharacterAttrCreate MsgCode = uint32(svc.CharacterAttr)*10000 + 1 // Req：创建角色战斗属性数据；Res：创建角色战斗属性数据；
)

func InitCodecForCharacterAttr(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdTryCharacterAttrCreate,
		func() interface{} {
			return &ReqTryCharacterAttrCreate{}
		},
		func() interface{} {
			return &ResTryCharacterAttrCreate{}
		})
}
