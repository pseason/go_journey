package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdTaskList            MsgCode = uint32(svc.Task)*10000 + 1 // Req：请求当前任务列表；Res：响应请求当前任务列表；
	CdEveTaskChange       MsgCode = uint32(svc.Task)*10000 + 2 // Eve：任务改变通知；
	CdTaskTryReceiveAward MsgCode = uint32(svc.Task)*10000 + 3 // Req：任务领取奖励；Res：响应任务领取奖励；
	CdTaskTryToggleHidden MsgCode = uint32(svc.Task)*10000 + 4 // Req：任务显示隐藏；Res：响应任务显示隐藏；
	CdTaskTryLoginInit    MsgCode = uint32(svc.Task)*10000 + 5 // Req：玩家登录初始化任务；Res：玩家登录初始化任务；
	CdTaskTryOffLineInit  MsgCode = uint32(svc.Task)*10000 + 6 // Req：玩家下线初始化任务；Res：玩家下线初始化任务；
	CdTaskTryExecute      MsgCode = uint32(svc.Task)*10000 + 7 // Req：玩家任务执行；Res：玩家任务执行；
	CdEveTaskExecute      MsgCode = uint32(svc.Task)*10000 + 8 // Eve：任务执行；
)

func InitCodecForTask(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdTaskList,
		func() interface{} {
			return &ReqTaskList{}
		},
		func() interface{} {
			return &ResTaskList{}
		})
	discovery.BindCoderFac(CdEveTaskChange,
		nil,
		func() interface{} {
			return &EveTaskChange{}
		})
	discovery.BindCoderFac(CdTaskTryReceiveAward,
		func() interface{} {
			return &ReqTaskTryReceiveAward{}
		},
		func() interface{} {
			return &ResTaskTryReceiveAward{}
		})
	discovery.BindCoderFac(CdTaskTryToggleHidden,
		func() interface{} {
			return &ReqTaskTryToggleHidden{}
		},
		func() interface{} {
			return &ResTaskTryToggleHidden{}
		})
	discovery.BindCoderFac(CdTaskTryLoginInit,
		func() interface{} {
			return &ReqTaskTryLoginInit{}
		},
		func() interface{} {
			return &ResTaskTryLoginInit{}
		})
	discovery.BindCoderFac(CdTaskTryOffLineInit,
		func() interface{} {
			return &ReqTaskTryOffLineInit{}
		},
		func() interface{} {
			return &ResTaskTryOffLineInit{}
		})
	discovery.BindCoderFac(CdTaskTryExecute,
		func() interface{} {
			return &ReqTaskTryExecute{}
		},
		func() interface{} {
			return &ResTaskTryExecute{}
		})
	discovery.BindCoderFac(CdEveTaskExecute,
		nil,
		func() interface{} {
			return &EveTaskExecute{}
		})
}
