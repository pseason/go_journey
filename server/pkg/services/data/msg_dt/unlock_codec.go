package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdUnlockCurrentInfo MsgCode = uint32(svc.Unlock)*10000 + 1 // Req：请求当前解锁的信息；Res：响应请求当前解锁的信息；
	CdEveUnlockChange   MsgCode = uint32(svc.Unlock)*10000 + 2 // Eve：解锁改变通知；
	CdUnlockExecute     MsgCode = uint32(svc.Unlock)*10000 + 3 // Req：解锁操作执行；Res：解锁操作执行；
	CdEveUnlockExecute  MsgCode = uint32(svc.Unlock)*10000 + 4 // Eve：解锁执行；
)

func InitCodecForUnlock(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdUnlockCurrentInfo,
		func() interface{} {
			return &ReqUnlockCurrentInfo{}
		},
		func() interface{} {
			return &ResUnlockCurrentInfo{}
		})
	discovery.BindCoderFac(CdEveUnlockChange,
		nil,
		func() interface{} {
			return &EveUnlockChange{}
		})
	discovery.BindCoderFac(CdUnlockExecute,
		func() interface{} {
			return &ReqUnlockExecute{}
		},
		func() interface{} {
			return &ResUnlockExecute{}
		})
	discovery.BindCoderFac(CdEveUnlockExecute,
		nil,
		func() interface{} {
			return &EveUnlockExecute{}
		})
}
