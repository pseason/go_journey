package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdCityBuildingList               MsgCode = uint32(svc.CityBuilding)*10000 + 1 // Req：请求城邦建筑列表；Res：响应城邦建筑列表；
	CdCityBuildingTryInvestResources MsgCode = uint32(svc.CityBuilding)*10000 + 2 // Req：请求投入资源；Res：响应投入资源；
	CdCityBuildingTryInvestEnergy    MsgCode = uint32(svc.CityBuilding)*10000 + 3 // Req：请求投入精力；Res：响应投入精力；
	CdEveCityBuildingState           MsgCode = uint32(svc.CityBuilding)*10000 + 4 // Eve：城邦建筑状态改变通知；
	CdCityBuildingEnergy             MsgCode = uint32(svc.CityBuilding)*10000 + 5 // Req：请求建筑建设度；Res：响应建筑建设度；
	CdCityBuildingResource           MsgCode = uint32(svc.CityBuilding)*10000 + 6 // Req：请求建筑投入材料；Res：响应建筑投入材料；
	CdCityBuildingGetBuilding        MsgCode = uint32(svc.CityBuilding)*10000 + 7 // Req：请求指定建筑；Res：响应指定建筑；
	CdLoadCityBuildingList           MsgCode = uint32(svc.CityBuilding)*10000 + 8 // Req：请求加载指定城邦建筑列表；Res：响应指定城邦建筑列表；
)

func InitCodecForCityBuilding(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdCityBuildingList,
		func() interface{} {
			return &ReqCityBuildingList{}
		},
		func() interface{} {
			return &ResCityBuildingList{}
		})
	discovery.BindCoderFac(CdCityBuildingTryInvestResources,
		func() interface{} {
			return &ReqCityBuildingTryInvestResources{}
		},
		func() interface{} {
			return &ResCityBuildingTryInvestResources{}
		})
	discovery.BindCoderFac(CdCityBuildingTryInvestEnergy,
		func() interface{} {
			return &ReqCityBuildingTryInvestEnergy{}
		},
		func() interface{} {
			return &ResCityBuildingTryInvestEnergy{}
		})
	discovery.BindCoderFac(CdEveCityBuildingState,
		nil,
		func() interface{} {
			return &EveCityBuildingState{}
		})
	discovery.BindCoderFac(CdCityBuildingEnergy,
		func() interface{} {
			return &ReqCityBuildingEnergy{}
		},
		func() interface{} {
			return &ResCityBuildingEnergy{}
		})
	discovery.BindCoderFac(CdCityBuildingResource,
		func() interface{} {
			return &ReqCityBuildingResource{}
		},
		func() interface{} {
			return &ResCityBuildingResource{}
		})
	discovery.BindCoderFac(CdCityBuildingGetBuilding,
		func() interface{} {
			return &ReqCityBuildingGetBuilding{}
		},
		func() interface{} {
			return &ResCityBuildingGetBuilding{}
		})
	discovery.BindCoderFac(CdLoadCityBuildingList,
		func() interface{} {
			return &ReqLoadCityBuildingList{}
		},
		func() interface{} {
			return &ResLoadCityBuildingList{}
		})
}
