package msg_dt

import "hm/pkg/services/data/model"

type PlotBatchExecuteResBody struct {
	Adds, BeforeUpdates, Updates, Removes []*model.Plot
}

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Plot

// @MessageCode=1 请求当前剧情列表
type ReqPlotList struct {
	Cid    int64
	PlotId int64 //plot的唯一ID，可选
}

// @MessageCode=1 响应请求当前剧情列表
type ResPlotList struct {
	Records []*model.Plot
}

// @MessageCode=2 剧情改变通知
type EvePlotChange struct {
	Cid     int64
	Records []*model.Plot
}

// @MessageCode=3 剧情批量操作
type ReqPlotBatchExecute struct {
	Cid                    int64
	Adds, Updates, Removes []*model.Plot
}

// @MessageCode=3 响应剧情批量操作
type ResPlotBatchExecute struct {
}
