package msg_dt

import (
	"95eh.com/eg/intfc"
)

// 绑定与服务通信的消息协议号和消息结构体的映射
func InitServiceCodec(discovery intfc.IMDiscovery) {
	InitCodecForBackpack(discovery)
	InitCodecForBackpackShortcutBar(discovery)
	InitCodecForBill(discovery)
	InitCodecForCharacterAttr(discovery)
	InitCodecForCharacterInfo(discovery)
	InitCodecForCity(discovery)
	InitCodecForCityBuilding(discovery)
	InitCodecForCityFarm(discovery)
	InitCodecForCityMakeWine(discovery)
	InitCodecForCityMarket(discovery)
	InitCodecForClan(discovery)
	InitCodecForCommon(discovery)
	InitCodecForEvent(discovery)
	InitCodecForMail(discovery)
	InitCodecForPlot(discovery)
	InitCodecForSocial(discovery)
	InitCodecForTask(discovery)
	InitCodecForTeam(discovery)
	InitCodecForTestTry(discovery)
	InitCodecForUnlock(discovery)
}
