package msg_dt

import "hm/pkg/services/data/model"

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = CityMarket

// @MessageCode=1 根据关键字或类型分页查询物品
type ReqMarketTitleList struct {
	Page    int64
	Size    int64
	Title   string
	BuildId int64
	Type    map[int32]int32
	Cid     int64
}

// @MessageCode=1 根据关键字分页查询物品
type ResMarketList struct {
	MarketList model.ResPageList
}

// @MessageCode=2 分页查询状态物品|默认上架
type ReqMarketRecord struct {
	Page    int64
	Size    int64
	BuildId int64
	Cid     int64
}

// @MessageCode=2 分页查询状态物品|默认上架
type ResMarketRecord struct {
	MarketList model.ResPageList
}

// @MessageCode=3 购买-更新物品状态信息
type ReqMarketBuy struct {
	Bid int64
	Cid int64
}

// @MessageCode=3 购买-更新物品状态信息
type ResMarketBuy struct {
	Bid int64
	MarketList *model.CityMarket
}

// @MessageCode=4上架
type ReqMarketPutInto struct {
	PtParams  []*model.MarketPutParams
	BuildId int64
	Cid int64
}

// @MessageCode=4 返回上架后的新数据
type ResMarketPutInto struct {
	MarketList []*model.CityMarket
}

// @MessageCode=5 手动下架
type ReqMarketRemove struct {
	Bid int64
	Cid int64
}

// @MessageCode=5 手动下架
type ResMarketRemove struct {
	MarketList *model.CityMarket
}

// @MessageCode=6 请求获取上架的数量-编译game取建筑上限判断
type ReqMarketGetUpCount struct {
	BuildId int64
	Cid     int64
}

// @MessageCode=6 获取上架的数量
type ResMarketGetUpCount struct {
	Num int64
}

// @MessageCode=7 获取单个物品信息
type ReqMarketGetPropById struct {
	Bid int64
	Cid int64
}

// @MessageCode=7 获取单个物品信息
type ResMarketGetPropById struct {
	MarketList *model.CityMarket
}

// @MessageCode=8 自动更新物品状态信息
type ReqMarketAutoCancel struct {
}

// @MessageCode=8 自动更新物品状态信息
type ResMarketAutoCancel struct {
	UpIds []int64
	MarketList []*model.MarketToMail
}
