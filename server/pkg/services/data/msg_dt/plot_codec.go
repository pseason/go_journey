package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdPlotList         MsgCode = uint32(svc.Plot)*10000 + 1 // Req：请求当前剧情列表；Res：响应请求当前剧情列表；
	CdEvePlotChange    MsgCode = uint32(svc.Plot)*10000 + 2 // Eve：剧情改变通知；
	CdPlotBatchExecute MsgCode = uint32(svc.Plot)*10000 + 3 // Req：剧情批量操作；Res：响应剧情批量操作；
)

func InitCodecForPlot(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdPlotList,
		func() interface{} {
			return &ReqPlotList{}
		},
		func() interface{} {
			return &ResPlotList{}
		})
	discovery.BindCoderFac(CdEvePlotChange,
		nil,
		func() interface{} {
			return &EvePlotChange{}
		})
	discovery.BindCoderFac(CdPlotBatchExecute,
		func() interface{} {
			return &ReqPlotBatchExecute{}
		},
		func() interface{} {
			return &ResPlotBatchExecute{}
		})
}
