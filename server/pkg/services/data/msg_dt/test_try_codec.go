package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdTestTryOrder        MsgCode = uint32(svc.TestTry)*10000 + 1 // Req：请求订单；Res：返回订单花费；
	CdTestTryHoldingAsset MsgCode = uint32(svc.TestTry)*10000 + 2 // Req：请求冻结资产；Res：返回冻结资产；
)

func InitCodecForTestTry(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdTestTryOrder,
		func() interface{} {
			return &ReqTestTryOrder{}
		},
		func() interface{} {
			return &ResTestTryOrder{}
		})
	discovery.BindCoderFac(CdTestTryHoldingAsset,
		func() interface{} {
			return &ReqTestTryHoldingAsset{}
		},
		func() interface{} {
			return &ResTestTryHoldingAsset{}
		})
}
