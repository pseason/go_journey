package msg_dt

import (
	"hm/pkg/services/data/model"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = CityBuilding

// @MessageCode=1 请求城邦建筑列表
type ReqCityBuildingList struct {
	CityId int64
}

// @MessageCode=1 响应城邦建筑列表
type ResCityBuildingList struct {
	Buildings []*model.CityBuilding
}

// @MessageCode=2 请求投入资源
type ReqCityBuildingTryInvestResources struct {
	Cid             int64
	BuildingId      int64
	InvestResources map[int32]int32
}

// @MessageCode=2 响应投入资源
type ResCityBuildingTryInvestResources struct {
	Cid                 int64
	CurrentPutResources map[int32]int32
	InvestResources     map[int32]int32
	UpMap               map[string]interface{}
	BuildingId          int64
	CityId              int64
}

// @MessageCode=3 请求投入精力
type ReqCityBuildingTryInvestEnergy struct {
	Cid        int64
	BuildingId int64
}

// @MessageCode=3 响应投入精力
type ResCityBuildingTryInvestEnergy struct {
	CurrentNum int32
	UpMap      map[string]interface{}
	CityId     int64
}

// @MessageCode=4 城邦建筑状态改变通知
type EveCityBuildingState struct {
	BuildingId    int64
	UpdateType    int32
	BuildingState int32
	BuildingLv    int32
	Cid           int64
}

// @MessageCode=5 请求建筑建设度
type ReqCityBuildingEnergy struct {
	BuildingId int64
}

// @MessageCode=5 响应建筑建设度
type ResCityBuildingEnergy struct {
	CurrentEnergyNum int32
	NeedBuildingLv   map[int32]int32
}

// @MessageCode=6 请求建筑投入材料
type ReqCityBuildingResource struct {
	BuildingId int64
}

// @MessageCode=6 响应建筑投入材料
type ResCityBuildingResource struct {
	Resources      map[int32]int32
	NeedBuildingLv map[int32]int32
}

// @MessageCode=7 请求指定建筑
type ReqCityBuildingGetBuilding struct {
	BuildingId int64
}

// @MessageCode=7 响应指定建筑
type ResCityBuildingGetBuilding struct {
	Building model.CityBuilding
}

// @MessageCode=8 请求加载指定城邦建筑列表
type ReqLoadCityBuildingList struct {
	CityId int64
}

// @MessageCode=8 响应指定城邦建筑列表
type ResLoadCityBuildingList struct {
	Buildings []*model.CityBuilding
}
