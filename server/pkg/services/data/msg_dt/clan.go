package msg_dt

import "hm/pkg/services/data/model"

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Clan

// @MessageCode=1 请求创建氏族
type ReqClanTryCreate struct {
	Cid         int64
	ClanName    string
	ClanSurname string
	MemberList  []int64
}

// @MessageCode=1 响应创建氏族
type ResClanTryCreate struct {
	Clan *model.Clan
	IsSignIn bool
}

// @MessageCode=2 请求加载氏族
type ReqClanLoad struct {
	ClanId int64
	Cid int64
}

// @MessageCode=2 响应加载氏族
type ResClanLoad struct {
	Clan *model.Clan
	IsSignIn bool
}

// @MessageCode=3 请求修改氏族名称和姓氏
type ReqClanTryUpNameOrSurname struct {
	ClanId      int64
	ClanName    string // 氏族名称
	ClanSurname string // 氏族姓氏
}

// @MessageCode=3 响应修改氏族名称和姓氏
type ResClanTryUpNameOrSurname struct {
	ClanId int64
	UpType int32
	UpMap  map[string]interface{}
}

// @MessageCode=4 请求修改氏族宣言
type ReqClanTryUpDeclaration struct {
	ClanId          int64
	ClanDeclaration string // 氏族宣言
}

// @MessageCode=4 响应修改氏族宣言
type ResClanTryUpDeclaration struct {
	ClanId int64
	UpType int32
	UpMap  map[string]interface{}
}

// @MessageCode=5 响应修改氏族宣言
type EveClanNoticeChange struct {
	CidList 		[]int64
	ClanId          int64
	UpType          int32  //修改类型 1.修改氏族名称 2.修改姓氏 3. 修改氏族名称和姓氏 4.修改公告
	ClanName        string //氏族名称
	ClanSurname     string //氏族姓氏
	ClanDeclaration string //氏族公告
}

// @MessageCode=6 分页请求氏族
type ReqClanPageSearch struct {
	Page int32
	Size int32
}

// @MessageCode=6 响应分页请求氏族
type ResClanPageSearch struct {
	Total int32
	ClanList []*model.Clan
}

// @MessageCode=7 请求氏族签到
type ReqClanTrySignIn struct {
	Cid int64
}

// @MessageCode=7 响应氏族签到
type ResClanTrySignIn struct {
	UpMap map[string]interface{}
	ClanMemberId int64
}