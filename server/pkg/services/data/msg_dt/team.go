package msg_dt

import (
	"hm/pkg/services/data/model"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Team

type TeamArms struct {
	TeamAimsId int32 //模板id
	MinAgeNum  int32 //最小年龄
	MaxAgeNum  int32 //最大年龄
	PowerNum   int32 //战力要求
}

// @MessageCode=1 请求创建队伍
type ReqTryTeamCreate struct {
	Cid           int64
	CityId        int64
	CharacterNick string
}

// @MessageCode=1 响应创建队伍
type ResTryTeamCreate struct {
	Team *model.Team
}

// @MessageCode=2 请求查询队伍
type ReqTeamGetTeamById struct {
	TeamId int64
}

// @MessageCode=2 响应查询队伍
type ResTeamGetTeamById struct {
	Team *model.Team
}

// @MessageCode=3 请求申请加入队伍
type ReqTeamApplyJoinTeam struct {
	Cid    int64
	CityId int64
	TeamId int64
}

// @MessageCode=3 响应申请加入队伍
type ResTeamApplyJoinTeam struct {
	TeamId       int64
	Cid          int64
	TeamLeaderId int64
}

// @MessageCode=4 请求邀请加入队伍
type ReqTeamInviteJoinTeam struct {
	Cid    int64
	Oid    int64
	CityId int64
}

// @MessageCode=4 响应邀请加入队伍
type ResTeamInviteJoinTeam struct {
}

// @MessageCode=5 请求同意申请加入
type ReqTeamAgreeApplyJoinTeam struct {
	Cid int64
	Oid int64
}

// @MessageCode=5 响应同意申请加入
type ResTeamAgreeApplyJoinTeam struct {
	TeamId int64
	CidList []int64
}

// @MessageCode=6 请求同意邀请加入
type ReqTeamAgreeInviteJoinTeam struct {
	Cid    int64
	TeamId int64
}

// @MessageCode=6 响应同意邀请加入
type ResTeamAgreeInviteJoinTeam struct {
}

// @MessageCode=7 队伍申请通知
type EveTeamApplyNotify struct {
	Cid int64
}

// @MessageCode=8 队伍邀请通知
type EveTeamInviteNotify struct {
	Cid    int64
	TeamId int64
}

// @MessageCode=9 队伍成员更新
type EveTeamMemberChange struct {
	MemberList []int64
	Cid        int64
	TeamId     int64
	Type       int32
}

// @MessageCode=10 请求拒绝申请加入
type ReqTeamRefuseApplyJoinTeam struct {
	Cid int64
	Oid int64
}

// @MessageCode=10 响应拒绝申请加入
type ResTeamRefuseApplyJoinTeam struct {
}

// @MessageCode=11 请求设置自动同意
type ReqTryTeamUpdateTeamAutoJoin struct {
	Cid int64
}

// @MessageCode=11 响应设置自动同意
type ResTryTeamUpdateTeamAutoJoin struct {
	Team *model.Team
}

// @MessageCode=12 队伍自动同意变更
type EveTeamAutoAgreeChange struct {
	TeamId      int64
	IsAutoAgree bool
	CidList     []int64
}

// @MessageCode=13 队伍添加新成员通知
type EveTeamAddNewMemberNotify struct {
	Cid     int64
	TeamId  int64
	OidList []int64
}

// @MessageCode=14 请求一键申请
type ReqTeamOneClickApply struct {
	Cid         int64
	AimsTempId  int32
	Age         int32
	BattlePower int32
}

// @MessageCode=14 响应一键申请
type ResTeamOneClickApply struct {
	TeamId int64
}

// @MessageCode=15 请求设置队伍目标限制
type ReqTeamUpdateTeamArms struct {
	Cid      int64
	TeamArms TeamArms
}

// @MessageCode=15 响应设置队伍目标限制
type ResTeamUpdateTeamArms struct {
	UpMap map[string]interface{}
	Team *model.Team
}

// @MessageCode=16 队伍修改目标变更通知
type EveTeamUpdateTeamAimsChangeNotify struct {
	TeamId   int64    //队伍id
	TeamAims TeamArms //修改的队伍目标
	OidList  []int64
}

// @MessageCode=17 请求离开队伍
type ReqTryTeamLeaveTeam struct {
	Cid    int64
	TeamId int64
}

// @MessageCode=17 响应离开队伍
type ResTryTeamLeaveTeam struct {
}

// @MessageCode=18 请求转移队长
type ReqTeamTransferTeam struct {
	Cid int64
	Oid int64
}

// @MessageCode=18 响应转移队长
type ResTeamTransferTeam struct {
}

// @MessageCode=19 请求解散队伍
type ReqTeamDisbandTeam struct {
	Cid int64
}

// @MessageCode=19 响应解散队伍
type ResTeamDisbandTeam struct {
}

// @MessageCode=20 请求踢出人员
type ReqTryTeamPleaseLeaveTeam struct {
	Cid int64
	Oid int64
}

// @MessageCode=20 响应踢出人员
type ResTryTeamPleaseLeaveTeam struct {
	UpMap map[string]interface{}
	Team *model.Team
}

// @MessageCode=21 请求分页查询队伍
type ReqTeamPageQueryTeam struct {
	CityId int64
	Page   int32
	Size   int32
	AimsId int32
}

// @MessageCode=21 响应分页查询队伍
type ResTeamPageQueryTeam struct {
	TeamList []*model.Team
	Total    int32
}
// @MessageCode=22 请求队伍申请列表
type ReqTeamApplyList struct {
	Cid int64
}

// @MessageCode=22 响应队伍申请列表
type ResTeamApplyList struct {
	ApplyList []model.TeamRelationship
}

// @MessageCode=23 队伍成员更新
type EveTeamLoadChange struct {
	Cid int64
	Team *model.Team
}

// @MessageCode=24 请求自动同意加入队伍
type ReqTryTeamAutoAgreeJoin struct {
	Cid int64
	Team *model.Team
}

// @MessageCode=24 响应自动同意加入队伍
type ResTryTeamAutoAgreeJoin struct {
	Team *model.Team
	UpData map[string]interface{}
	MemberList []int64
	Oid int64
}
