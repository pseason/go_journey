package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdSocialTryApplyJoinBuddy     MsgCode = uint32(svc.Social)*10000 + 1  // Req：请求添加好友申请；Res：响应添加好友申请；
	CdEveSocialBuddyApplyNotify   MsgCode = uint32(svc.Social)*10000 + 2  // Eve：好友申请通知；
	CdSocialTryAgreeFriends       MsgCode = uint32(svc.Social)*10000 + 3  // Req：请求同意好友申请；Res：响应同意好友申请；
	CdSocialTryRejectFriends      MsgCode = uint32(svc.Social)*10000 + 4  // Req：请求拒绝好友申请；Res：响应拒绝好友申请；
	CdEveSocialJoinBuddyNotify    MsgCode = uint32(svc.Social)*10000 + 5  // Eve：好友添加成功通知；
	CdSocialTryAddBlacklist       MsgCode = uint32(svc.Social)*10000 + 6  // Req：请求添加黑名单；Res：响应添加黑名单；
	CdSocialTryAddEnemy           MsgCode = uint32(svc.Social)*10000 + 7  // Req：请求添加仇人；Res：响应添加仇人；
	CdSocialTryDeleteBuddy        MsgCode = uint32(svc.Social)*10000 + 8  // Req：请求删除好友；Res：响应删除好友；
	CdSocialTryDeleteBlacklist    MsgCode = uint32(svc.Social)*10000 + 9  // Req：请求删除黑名单；Res：响应删除黑名单；
	CdSocialTryDeleteEnemy        MsgCode = uint32(svc.Social)*10000 + 10 // Req：请求删除仇人；Res：响应删除仇人；
	CdSocialGetAllSocial          MsgCode = uint32(svc.Social)*10000 + 11 // Req：请求玩家所有社交关系；Res：响应玩家所有社交关系；
	CdSocialUnReadBuddyApplyCount MsgCode = uint32(svc.Social)*10000 + 12 // Req：请求玩家好友申请数量；Res：响应玩家好友申请数量；
	CdSocialGetBuddyCidList       MsgCode = uint32(svc.Social)*10000 + 13 // Req：请求玩家好友申请数量；Res：响应玩家好友申请数量；
)

func InitCodecForSocial(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdSocialTryApplyJoinBuddy,
		func() interface{} {
			return &ReqSocialTryApplyJoinBuddy{}
		},
		func() interface{} {
			return &ResSocialTryApplyJoinBuddy{}
		})
	discovery.BindCoderFac(CdEveSocialBuddyApplyNotify,
		nil,
		func() interface{} {
			return &EveSocialBuddyApplyNotify{}
		})
	discovery.BindCoderFac(CdSocialTryAgreeFriends,
		func() interface{} {
			return &ReqSocialTryAgreeFriends{}
		},
		func() interface{} {
			return &ResSocialTryAgreeFriends{}
		})
	discovery.BindCoderFac(CdSocialTryRejectFriends,
		func() interface{} {
			return &ReqSocialTryRejectFriends{}
		},
		func() interface{} {
			return &ResSocialTryRejectFriends{}
		})
	discovery.BindCoderFac(CdEveSocialJoinBuddyNotify,
		nil,
		func() interface{} {
			return &EveSocialJoinBuddyNotify{}
		})
	discovery.BindCoderFac(CdSocialTryAddBlacklist,
		func() interface{} {
			return &ReqSocialTryAddBlacklist{}
		},
		func() interface{} {
			return &ResSocialTryAddBlacklist{}
		})
	discovery.BindCoderFac(CdSocialTryAddEnemy,
		func() interface{} {
			return &ReqSocialTryAddEnemy{}
		},
		func() interface{} {
			return &ResSocialTryAddEnemy{}
		})
	discovery.BindCoderFac(CdSocialTryDeleteBuddy,
		func() interface{} {
			return &ReqSocialTryDeleteBuddy{}
		},
		func() interface{} {
			return &ResSocialTryDeleteBuddy{}
		})
	discovery.BindCoderFac(CdSocialTryDeleteBlacklist,
		func() interface{} {
			return &ReqSocialTryDeleteBlacklist{}
		},
		func() interface{} {
			return &ResSocialTryDeleteBlacklist{}
		})
	discovery.BindCoderFac(CdSocialTryDeleteEnemy,
		func() interface{} {
			return &ReqSocialTryDeleteEnemy{}
		},
		func() interface{} {
			return &ResSocialTryDeleteEnemy{}
		})
	discovery.BindCoderFac(CdSocialGetAllSocial,
		func() interface{} {
			return &ReqSocialGetAllSocial{}
		},
		func() interface{} {
			return &ResSocialGetAllSocial{}
		})
	discovery.BindCoderFac(CdSocialUnReadBuddyApplyCount,
		func() interface{} {
			return &ReqSocialUnReadBuddyApplyCount{}
		},
		func() interface{} {
			return &ResSocialUnReadBuddyApplyCount{}
		})
	discovery.BindCoderFac(CdSocialGetBuddyCidList,
		func() interface{} {
			return &ReqSocialGetBuddyCidList{}
		},
		func() interface{} {
			return &ResSocialGetBuddyCidList{}
		})
}
