package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdShortcutBarList     MsgCode = uint32(svc.BackpackShortcutBar)*10000 + 1 // Req：获取快捷栏参数；Res：返回快捷栏数据；
	CdShortcutBarPutInto  MsgCode = uint32(svc.BackpackShortcutBar)*10000 + 2 // Req：放入快捷栏请求参数；Res：返回快捷栏变动数据；
	CdShortcutBarRemove   MsgCode = uint32(svc.BackpackShortcutBar)*10000 + 3 // Req：移除快捷栏请求参数；Res：移除快捷栏变动数据；
	CdShortcutBarPutProps MsgCode = uint32(svc.BackpackShortcutBar)*10000 + 4 // Req：批量放入快捷栏请求参数；Res：批量返回快捷栏变动数据；
)

func InitCodecForBackpackShortcutBar(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdShortcutBarList,
		func() interface{} {
			return &ReqShortcutBarList{}
		},
		func() interface{} {
			return &ResShortcutBarList{}
		})
	discovery.BindCoderFac(CdShortcutBarPutInto,
		func() interface{} {
			return &ReqShortcutBarPutInto{}
		},
		func() interface{} {
			return &ResShortcutBarPutInto{}
		})
	discovery.BindCoderFac(CdShortcutBarRemove,
		func() interface{} {
			return &ReqShortcutBarRemove{}
		},
		func() interface{} {
			return &ResShortcutBarRemove{}
		})
	discovery.BindCoderFac(CdShortcutBarPutProps,
		func() interface{} {
			return &ReqShortcutBarPutProps{}
		},
		func() interface{} {
			return &ResShortcutBarPutProps{}
		})
}
