package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdBillTryCreate            MsgCode = uint32(svc.Bill)*10000 + 1 // Req：请求创建账单；Res：响应创建账单；
	CdBillGetPurchasePropCount MsgCode = uint32(svc.Bill)*10000 + 2 // Req：请求玩家购买指定商品数量；Res：响应玩家购买指定商品数量；
)

func InitCodecForBill(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdBillTryCreate,
		func() interface{} {
			return &ReqBillTryCreate{}
		},
		func() interface{} {
			return &ResBillTryCreate{}
		})
	discovery.BindCoderFac(CdBillGetPurchasePropCount,
		func() interface{} {
			return &ReqBillGetPurchasePropCount{}
		},
		func() interface{} {
			return &ResBillGetPurchasePropCount{}
		})
}
