package msg_dt

import (
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
)

/*
@Time   : 2021-11-09 2:23
@Author : wushu
@DESC   :
*/

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = CharacterInfo

// @MessageCode=1 请求角色信息
type ReqCharacterInfo struct {
	Uid int64
}

// @MessageCode=1 返回角色信息
type ResCharacterInfo struct {
	CharacterInfo *model.CharacterInfo
}

// @MessageCode=2 创建角色
type ReqTryCharacterCreate struct {
	Uid      int64
	Nickname string // 昵称
	Gender   uint8  // 性别
	Figure   uint8  // 形象
}

// @MessageCode=2 创建角色-返回角色信息
type ResTryCharacterCreate struct {
	CharacterInfo *model.CharacterInfo
}

// @MessageCode=3 角色信息变更
type EveCharacterInfoChange struct {
	Cid      int64
	IntInfo  map[character_info_e.Enum]int32  // 整型信息
	LongInfo map[character_info_e.Enum]int64  // 长整型信息
	StrInfo  map[character_info_e.Enum]string // 字符串信息
}

// @MessageCode=4 根据昵称查询角色id
type ReqCidByNickname struct {
	Nickname string // 昵称
}

// @MessageCode=4 根据昵称查询角色id
type ResCidByNickname struct {
	Cid int64
}

// @MessageCode = 99 测试接口
type ReqCharacterTest struct {
}

// @MessageCode = 99 测试接口
type ResCharacterTest struct {
}
