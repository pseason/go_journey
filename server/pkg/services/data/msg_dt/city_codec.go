package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdCityList                  MsgCode = uint32(svc.City)*10000 + 1  // Req：请求城邦列表；Res：响应城邦列表；
	CdCityGetCount              MsgCode = uint32(svc.City)*10000 + 2  // Req：请求城邦数量；Res：响应城邦数量；
	CdCityJoin                  MsgCode = uint32(svc.City)*10000 + 3  // Req：请求加入城邦；Res：响应加入城邦；
	CdCityMemberList            MsgCode = uint32(svc.City)*10000 + 4  // Req：请求城邦所有成员；Res：响应城邦所有成员；
	CdCitySetCityMemberIdentity MsgCode = uint32(svc.City)*10000 + 5  // Req：请求设置城邦成员身份；Res：响应设置城邦成员身份；
	CdCityExileCityMember       MsgCode = uint32(svc.City)*10000 + 6  // Req：请求流放城邦成员；Res：响应流放城邦成员；
	CdCityTryExitCity           MsgCode = uint32(svc.City)*10000 + 7  // Req：请求退出城邦；Res：响应流放城邦成员；
	CdCityLoadCity              MsgCode = uint32(svc.City)*10000 + 8  // Req：请求加载指定城邦；Res：响应加载指定城邦；
	CdCityUpCityData            MsgCode = uint32(svc.City)*10000 + 9  // Req：请求修改城邦数据；Res：响应修改城邦数据；
	CdCityIdByTemId             MsgCode = uint32(svc.City)*10000 + 10 // Req：请求所有对应城邦模板id的城邦id；Res：响应所有对应城邦模板id的城邦id；
	CdEveCityBuildingLoad       MsgCode = uint32(svc.City)*10000 + 11 // Eve：城邦建筑加载事件；
	CdCityTempIdByCityId        MsgCode = uint32(svc.City)*10000 + 12 // Req：请求城邦id查询模板id；Res：响应城邦id查询模板id；
)

func InitCodecForCity(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdCityList,
		func() interface{} {
			return &ReqCityList{}
		},
		func() interface{} {
			return &ResCityList{}
		})
	discovery.BindCoderFac(CdCityGetCount,
		func() interface{} {
			return &ReqCityGetCount{}
		},
		func() interface{} {
			return &ResCityGetCount{}
		})
	discovery.BindCoderFac(CdCityJoin,
		func() interface{} {
			return &ReqCityJoin{}
		},
		func() interface{} {
			return &ResCityJoin{}
		})
	discovery.BindCoderFac(CdCityMemberList,
		func() interface{} {
			return &ReqCityMemberList{}
		},
		func() interface{} {
			return &ResCityMemberList{}
		})
	discovery.BindCoderFac(CdCitySetCityMemberIdentity,
		func() interface{} {
			return &ReqCitySetCityMemberIdentity{}
		},
		func() interface{} {
			return &ResCitySetCityMemberIdentity{}
		})
	discovery.BindCoderFac(CdCityExileCityMember,
		func() interface{} {
			return &ReqCityExileCityMember{}
		},
		func() interface{} {
			return &ResCityExileCityMember{}
		})
	discovery.BindCoderFac(CdCityTryExitCity,
		func() interface{} {
			return &ReqCityTryExitCity{}
		},
		func() interface{} {
			return &ResCityTryExitCity{}
		})
	discovery.BindCoderFac(CdCityLoadCity,
		func() interface{} {
			return &ReqCityLoadCity{}
		},
		func() interface{} {
			return &ResCityLoadCity{}
		})
	discovery.BindCoderFac(CdCityUpCityData,
		func() interface{} {
			return &ReqCityUpCityData{}
		},
		func() interface{} {
			return &ResCityUpCityData{}
		})
	discovery.BindCoderFac(CdCityIdByTemId,
		func() interface{} {
			return &ReqCityIdByTemId{}
		},
		func() interface{} {
			return &ResCityIdByTemId{}
		})
	discovery.BindCoderFac(CdEveCityBuildingLoad,
		nil,
		func() interface{} {
			return &EveCityBuildingLoad{}
		})
	discovery.BindCoderFac(CdCityTempIdByCityId,
		func() interface{} {
			return &ReqCityTempIdByCityId{}
		},
		func() interface{} {
			return &ResCityTempIdByCityId{}
		})
}
