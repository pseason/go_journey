package msg_dt

import "hm/pkg/services/data/model"

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Event

// @MessageCode=1 请求创建事件
type ReqEventCreateEvent struct {
	Event []*model.Event
}

// @MessageCode=1 响应创建事件
type ResEventCreateEvent struct {

}

// @MessageCode=2 请求分页查询城邦事件
type ReqEventPageGetCityEvent struct {
	EventType int32
	CityId int64
	Page int32 //页数
	Size int32 //数量
}

// @MessageCode=2 响应分页查询城邦事件
type ResEventPageGetCityEvent struct {
	Events []*model.Event
	Total int32
}
