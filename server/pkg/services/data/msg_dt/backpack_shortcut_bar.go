package msg_dt

import (
	"hm/pkg/services/data/model"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = BackpackShortcutBar

// @MessageCode = 1 获取快捷栏参数
type ReqShortcutBarList struct {
	Cid int64
}

// @MessageCode = 1 返回快捷栏数据
type ResShortcutBarList struct {
	ShortcutBarList *model.BackpackShortcutBar
}

// @MessageCode = 2 放入快捷栏请求参数
type ReqShortcutBarPutInto struct {
	Cid int64
	PropId int32
	Index int32
}

// @MessageCode = 2 返回快捷栏变动数据
type ResShortcutBarPutInto struct {
	ShortcutBarList *model.BackpackShortcutBar
}

// @MessageCode = 3 移除快捷栏请求参数
type ReqShortcutBarRemove struct {
	Cid int64
	Index int32
}

// @MessageCode = 3 移除快捷栏变动数据
type ResShortcutBarRemove struct {
	ShortcutBarList *model.BackpackShortcutBar
}

// @MessageCode = 4 批量放入快捷栏请求参数
type ReqShortcutBarPutProps struct {
	Cid int64
	PutProps map[int32]int32 //propId=>Index
}

// @MessageCode = 4 批量返回快捷栏变动数据
type ResShortcutBarPutProps struct {
	ShortcutBarList *model.BackpackShortcutBar
}