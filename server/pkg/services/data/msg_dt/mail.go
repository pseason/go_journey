package msg_dt

import (
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/mail_e"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Mail

// @MessageCode=1 分页请求邮件
type ReqMailPage struct {
	Cid  int64
	Page int32 //页数
	Size int32 //数量
}

// @MessageCode=1 响应分页邮件
type ResMailPage struct {
	MailInfoList []*model.Mail //邮件列表
	Total        int32         //邮件总数
	Current      int32         //当前页数
}

// @MessageCode=2 请求删除邮件
type ReqMailDel struct {
	Cid    int64
	MailId int64
}

// @MessageCode=2 响应删除邮件
type ResMailDel struct {
	MailId []int64
	DeleteIdList []int64
}

// @MessageCode=3 请求领取邮件
type ReqMailReceive struct {
	Cid    int64
}

// @MessageCode=3 响应领取邮件
type ResMailReceive struct {
	MailId int64
	UpdateMap map[string]interface{}
}

// @MessageCode=4 请求读取邮件
type ReqMailRead struct {
	Cid    int64
	MailId int64
}

// @MessageCode=4 响应读取邮件
type ResMailRead struct {
	MailId int64
}


// @MessageCode=5 请求邮件数量
type ReqMailCount struct {
	Cid int64
}

// @MessageCode=5 响应邮件数量
type ResMailCount struct {
	UnReadCount int32 //未读邮件数量
}

// @MessageCode=6 请求邮件发送
type ReqMailSend struct {
	Mails []*model.Mail
}

// @MessageCode=6 响应邮件发送
type ResMailSend struct {
}

// @MessageCode=7 邮件通知
type EveMailNew struct {
	CidList  []int64
	MailType mail_e.MailType
}

// @MessageCode=8 请求查询邮件
type ReqMailGet struct {
	MailId int64
}

// @MessageCode=8 响应邮件查询
type ResMailGet struct {
	Mails *model.Mail
}

// @MessageCode=9 请求邮件数量
type ReqMailAutoOperation struct {
	MailOperationType int32
}

// @MessageCode=9 响应邮件数量
type ResMailAutoOperation struct {
	mails []int64
}

// @MessageCode=10 请求玩家所有邮件
type ReqMails struct {
}

// @MessageCode=10 响应玩家所有邮件
type ResMails struct {
	MailInfoList []*model.Mail //邮件列表
}

// @MessageCode=12 请求指定邮件
type ReqMailById struct {
	Tid    int64
	MailId int64
}

// @MessageCode=12 响应指定邮件
type ResMailById struct {
	Mail *model.Mail
}
