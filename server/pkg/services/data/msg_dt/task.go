package msg_dt

import (
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/task_e"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Task

// @MessageCode=1 请求当前任务列表
type ReqTaskList struct {
	Cid        int64
}

// @MessageCode=1 响应请求当前任务列表
type ResTaskList struct {
	Records []*model.Task
}

// @MessageCode=2 任务改变通知
type EveTaskChange struct {
	Cid     int64
	Records []*model.Task
}

// @MessageCode=3 任务领取奖励
type ReqTaskTryReceiveAward struct {
	Cid    int64
	TaskId int64
}

// @MessageCode=3 响应任务领取奖励
type ResTaskTryReceiveAward struct {
	Task *model.Task
	AddPropMap map[int32]int32
	AddSkillMap map[int32]int32
	AddInfoMap map[int32]int32
}

// @MessageCode=4 任务显示隐藏
type ReqTaskTryToggleHidden struct {
	Cid    int64
	TaskId int64
}

// @MessageCode=4 响应任务显示隐藏
type ResTaskTryToggleHidden struct {
	Task *model.Task
}

// @MessageCode=5 玩家登录初始化任务
type ReqTaskTryLoginInit struct {
	Cid []int64
	IsTaskNotify bool
}

// @MessageCode=5 玩家登录初始化任务
type ResTaskTryLoginInit struct {
}

// @MessageCode=6 玩家下线初始化任务
type ReqTaskTryOffLineInit struct {
	Cid int64
}


// @MessageCode=6 玩家下线初始化任务
type ResTaskTryOffLineInit struct {
}


// @MessageCode=7 玩家任务执行
type ReqTaskTryExecute struct {
	Cid int64
	PropId int32
	PropNum int32
	AftType task_e.TaskAftType
}

// @MessageCode=7 玩家任务执行
type ResTaskTryExecute struct {
	DelRedisTaskMap map[task_e.TaskAftType]map[int32][]int32
	AddRedisTempIdMap map[task_e.TaskAftType]map[int32][]int32
	UpTaskDb []*model.Task
	AddTaskDb []*model.Task
	AddPlotIdList []int32
	AddPropMap map[int32]int32
	AddSkillMap map[int32]int32
	AddInfoMap map[int32]int32
}

// @MessageCode=8 任务执行
type EveTaskExecute struct {
	Cid     int64
	PropId int32
	PropNum int32
	AftType task_e.TaskAftType
}