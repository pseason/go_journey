package msg_dt

import "hm/pkg/services/data/model"

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Unlock

// @MessageCode=1 请求当前解锁的信息
type ReqUnlockCurrentInfo struct {
	Cid int64
}

// @MessageCode=1 响应请求当前解锁的信息
type ResUnlockCurrentInfo struct {
	Records []int32
}

// @MessageCode=2 解锁改变通知
type EveUnlockChange struct {
	Cid     int64
	Records []int32
}

// @MessageCode=3 解锁操作执行
type ReqUnlockExecute struct {
	Cid         int64
	Opt         model.UnlockCommonOption
	TemplateIds []int32
}

// @MessageCode=3 解锁操作执行
type ResUnlockExecute struct {
}

// @MessageCode=4 解锁执行
type EveUnlockExecute struct {
	Cid         int64
	Opt         model.UnlockCommonOption
	TemplateIds []int32
}
