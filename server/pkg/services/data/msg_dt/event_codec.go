package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdEventCreateEvent      MsgCode = uint32(svc.Event)*10000 + 1 // Req：请求创建事件；Res：响应创建事件；
	CdEventPageGetCityEvent MsgCode = uint32(svc.Event)*10000 + 2 // Req：请求分页查询城邦事件；Res：响应分页查询城邦事件；
)

func InitCodecForEvent(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdEventCreateEvent,
		func() interface{} {
			return &ReqEventCreateEvent{}
		},
		func() interface{} {
			return &ResEventCreateEvent{}
		})
	discovery.BindCoderFac(CdEventPageGetCityEvent,
		func() interface{} {
			return &ReqEventPageGetCityEvent{}
		},
		func() interface{} {
			return &ResEventPageGetCityEvent{}
		})
}
