package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdCityFarmPlanting           MsgCode = uint32(svc.CityFarm)*10000 + 1 // Req：请求城邦畜牧场种植；Res：响应城邦畜牧场种植；
	CdCityFarmGiveOutRewards     MsgCode = uint32(svc.CityFarm)*10000 + 2 // Req：请求城邦畜牧场发放奖励；Res：响应城邦畜牧场发放奖励；
	CdCityFarmClear              MsgCode = uint32(svc.CityFarm)*10000 + 3 // Req：请求城邦畜牧场清空种植数据；Res：响应城邦畜牧场清空种植数据；
	CdCityFarmIrrigation         MsgCode = uint32(svc.CityFarm)*10000 + 4 // Req：请求城邦畜牧场灌溉；Res：响应城邦畜牧场灌溉；
	CdCityFarmCrop               MsgCode = uint32(svc.CityFarm)*10000 + 5 // Req：请求城邦畜牧场种植物；Res：响应城邦畜牧场种植物；
	CdCityFarmEradicateCrop      MsgCode = uint32(svc.CityFarm)*10000 + 6 // Req：请求城邦畜牧场铲除农作物；Res：响应城邦畜牧场铲除农作物；
	CdCityFarmGranary            MsgCode = uint32(svc.CityFarm)*10000 + 7 // Req：请求城邦畜牧场个人仓库；Res：响应城邦畜牧场个人仓库；
	CdCityFarmReceiveFood        MsgCode = uint32(svc.CityFarm)*10000 + 8 // Req：请求城邦畜牧场收获；Res：响应城邦畜牧场收获；
	CdCityFarmGranaryAndFarmInfo MsgCode = uint32(svc.CityFarm)*10000 + 9 // Req：请求城邦畜牧场信息及粮仓数据；Res：响应城邦畜牧场信息及粮仓数据；
)

func InitCodecForCityFarm(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdCityFarmPlanting,
		func() interface{} {
			return &ReqCityFarmPlanting{}
		},
		func() interface{} {
			return &ResCityFarmPlanting{}
		})
	discovery.BindCoderFac(CdCityFarmGiveOutRewards,
		func() interface{} {
			return &ReqCityFarmGiveOutRewards{}
		},
		func() interface{} {
			return &ResCityFarmGiveOutRewards{}
		})
	discovery.BindCoderFac(CdCityFarmClear,
		func() interface{} {
			return &ReqCityFarmClear{}
		},
		func() interface{} {
			return &ResCityFarmClear{}
		})
	discovery.BindCoderFac(CdCityFarmIrrigation,
		func() interface{} {
			return &ReqCityFarmIrrigation{}
		},
		func() interface{} {
			return &ResCityFarmIrrigation{}
		})
	discovery.BindCoderFac(CdCityFarmCrop,
		func() interface{} {
			return &ReqCityFarmCrop{}
		},
		func() interface{} {
			return &ResCityFarmCrop{}
		})
	discovery.BindCoderFac(CdCityFarmEradicateCrop,
		func() interface{} {
			return &ReqCityFarmEradicateCrop{}
		},
		func() interface{} {
			return &ResCityFarmEradicateCrop{}
		})
	discovery.BindCoderFac(CdCityFarmGranary,
		func() interface{} {
			return &ReqCityFarmGranary{}
		},
		func() interface{} {
			return &ResCityFarmGranary{}
		})
	discovery.BindCoderFac(CdCityFarmReceiveFood,
		func() interface{} {
			return &ReqCityFarmReceiveFood{}
		},
		func() interface{} {
			return &ResCityFarmReceiveFood{}
		})
	discovery.BindCoderFac(CdCityFarmGranaryAndFarmInfo,
		func() interface{} {
			return &ReqCityFarmGranaryAndFarmInfo{}
		},
		func() interface{} {
			return &ResCityFarmGranaryAndFarmInfo{}
		})
}
