package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdCommonQueryById        MsgCode = uint32(svc.Common_Data)*10000 + 10 // Req：请求根据id查询；Res：返回查询结果；
	CdCommonQueryByIds       MsgCode = uint32(svc.Common_Data)*10000 + 11 // Req：请求根据id批量查询 todo 批量查询的结果只可用作数据展示，切记不可用在事务中。(批量查询无法进协程同步操作，所以读取到的可能是脏数据)；Res：返回查询结果；
	CdTryCommonQueryByIds    MsgCode = uint32(svc.Common_Data)*10000 + 12 // Req：请求(用于更新的)批量查询，必须在事务中使用；Res：返回查询结果；
	CdTryCommonUpdateById    MsgCode = uint32(svc.Common_Data)*10000 + 20 // Req：尝试根据id更新数据；Res：响应更新；
	CdTryCommonUpdateNumById MsgCode = uint32(svc.Common_Data)*10000 + 21 // Req：尝试根据id进行数值类型的数据增减；Res：响应数值增减；
)

func InitCodecForCommon(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdCommonQueryById,
		func() interface{} {
			return &ReqCommonQueryById{}
		},
		func() interface{} {
			return &ResCommonQueryById{}
		})
	discovery.BindCoderFac(CdCommonQueryByIds,
		func() interface{} {
			return &ReqCommonQueryByIds{}
		},
		func() interface{} {
			return &ResCommonQueryByIds{}
		})
	discovery.BindCoderFac(CdTryCommonQueryByIds,
		func() interface{} {
			return &ReqTryCommonQueryByIds{}
		},
		func() interface{} {
			return &ResTryCommonQueryByIds{}
		})
	discovery.BindCoderFac(CdTryCommonUpdateById,
		func() interface{} {
			return &ReqTryCommonUpdateById{}
		},
		func() interface{} {
			return &ResTryCommonUpdateById{}
		})
	discovery.BindCoderFac(CdTryCommonUpdateNumById,
		func() interface{} {
			return &ReqTryCommonUpdateNumById{}
		},
		func() interface{} {
			return &ResTryCommonUpdateNumById{}
		})
}
