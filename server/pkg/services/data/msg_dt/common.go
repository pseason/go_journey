package msg_dt

import (
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
)

/*
@Time   : 2021-11-10 16:43
@Author : wushu
@DESC   :
*/
// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Common_Data

type IReqModel interface {
	GetModel() model_e.Model
}

// ---------------------------↓-查询-↓-------------------------------

// @MessageCode=10 请求根据id查询
type ReqCommonQueryById struct {
	Model   model_e.Model
	Fields  []model_field_e.Enum // 查询的字段，若为空则表示查询所有字段 todo 原则上应当只传递查询的数据，不应该为了省事而不传
	ModelId int64   // 数据主键
}

func (r *ReqCommonQueryById) GetModel() model_e.Model {
	return r.Model
}

// @MessageCode=10 返回查询结果
type ResCommonQueryById struct {
	Data interface{}
}

// @MessageCode=11 请求根据id批量查询 todo 批量查询的结果只可用作数据展示，切记不可用在事务中。(批量查询无法进协程同步操作，所以读取到的可能是脏数据)
type ReqCommonQueryByIds struct {
	Model    model_e.Model
	Fields   []model_field_e.Enum // model_field_e.Enum
	ModelIds []int64              // 数据主键
}

func (r *ReqCommonQueryByIds) GetModel() model_e.Model {
	return r.Model
}

// @MessageCode=11 返回查询结果
type ResCommonQueryByIds struct {
	Data map[int64]interface{} // key:id value:model
}

// @MessageCode=12 请求(用于更新的)批量查询，必须在事务中使用
type ReqTryCommonQueryByIds struct {
	Model    model_e.Model
	Fields   []model_field_e.Enum
	ModelIds []int64 // 数据主键
}

func (r *ReqTryCommonQueryByIds) GetModel() model_e.Model {
	return r.Model
}

// @MessageCode=12 返回查询结果
type ResTryCommonQueryByIds struct {
	Data map[int64]interface{} // key:id value:model
}

// ---------------------------↓-修改-↓-------------------------------

// @MessageCode=20 尝试根据id更新数据
type ReqTryCommonUpdateById struct {
	Model   model_e.Model
	UpData  map[model_field_e.Enum]interface{}
	ModelId int64 // 数据主键
}

func (r *ReqTryCommonUpdateById) GetModel() model_e.Model {
	return r.Model
}

// @MessageCode=20 响应更新
type ResTryCommonUpdateById struct {
	Data interface{} // 响应数据，根据需要自行使用。(仅提供这一个参数，若需要更多参数，Data应当是自定义的消息结构体或slice、map ...等)
}

// @MessageCode=21 尝试根据id进行数值类型的数据增减
type ReqTryCommonUpdateNumById struct {
	Model    model_e.Model
	IncrData map[model_field_e.Enum]int32 // value：增量，可负数
	ModelId  int64                        // 数据主键
}

func (r *ReqTryCommonUpdateNumById) GetModel() model_e.Model {
	return r.Model
}

// @MessageCode=21 响应数值增减
type ResTryCommonUpdateNumById struct {
	OrigData interface{} // 更新前的数据
	Data     interface{} // 响应数据，默认为nil。作为拓展需求预留(若要设置值需要服务重写相关接口)，自行使用。若需要更多参数，Data应当是自定义结构体。
}
