package msg_dt

import "hm/pkg/services/data/model"

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Bill

// @MessageCode=1 请求创建账单
type ReqBillTryCreate struct {
	Bill *model.Bill
}

// @MessageCode=1 响应创建账单
type ResBillTryCreate struct {

}

// @MessageCode=2 请求玩家购买指定商品数量
type ReqBillGetPurchasePropCount struct {
	Cid int64
	PropId int32
	StartTime int64
	EndTime int64
	TransactionType int32
}

// @MessageCode=2 响应玩家购买指定商品数量
type ResBillGetPurchasePropCount struct {
	PurchaseCount int32
}
