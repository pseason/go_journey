package msg_dt

import "hm/pkg/services/data/model"

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = CityFarm

type FarmData struct {
	ItemId int32
	Life   int32
	Forage int32
}

// @MessageCode=1 请求城邦畜牧场种植
type ReqCityFarmPlanting struct {
	Building model.CityBuilding
	PropId   int32
}

// @MessageCode=1 响应城邦畜牧场种植
type ResCityFarmPlanting struct {
	Farm *model.CityFarmland
}

// @MessageCode=2 请求城邦畜牧场发放奖励
type ReqCityFarmGiveOutRewards struct {
	Building       model.CityBuilding
	CityMemberList []model.CityMember
}

// @MessageCode=2 响应城邦畜牧场发放奖励
type ResCityFarmGiveOutRewards struct {
	Farm model.CityFarmland
}

// @MessageCode=3 请求城邦畜牧场清空种植数据
type ReqCityFarmClear struct {
	FarmId int64
}

// @MessageCode=3 响应城邦畜牧场清空种植数据
type ResCityFarmClear struct {
}

// @MessageCode=4 请求城邦畜牧场灌溉
type ReqCityFarmIrrigation struct {
	BuildingId int64
	BuildingLv int32
}

// @MessageCode=4 响应城邦畜牧场灌溉
type ResCityFarmIrrigation struct {
	FarmId int64
	UpMap map[string]interface{}
}

// @MessageCode=5 请求城邦畜牧场种植物
type ReqCityFarmCrop struct {
	BuildingId int64
}

// @MessageCode=5 响应城邦畜牧场种植物
type ResCityFarmCrop struct {
	CropId int32
}

// @MessageCode=6 请求城邦畜牧场铲除农作物
type ReqCityFarmEradicateCrop struct {
	BuildingId int64
}

// @MessageCode=6 响应城邦畜牧场铲除农作物
type ResCityFarmEradicateCrop struct {
}

// @MessageCode=7 请求城邦畜牧场个人仓库
type ReqCityFarmGranary struct {
	Cid int64
}

// @MessageCode=7 响应城邦畜牧场个人仓库
type ResCityFarmGranary struct {
	GranaryPropMap map[int32]int32
}

// @MessageCode=8 请求城邦畜牧场收获
type ReqCityFarmReceiveFood struct {
	Cid int64
}

// @MessageCode=8 响应城邦畜牧场收获
type ResCityFarmReceiveFood struct {
}

// @MessageCode=9 请求城邦畜牧场信息及粮仓数据
type ReqCityFarmGranaryAndFarmInfo struct {
	BuildingId int64
	Cid        int64
}

// @MessageCode=9 响应城邦畜牧场信息及粮仓数据
type ResCityFarmGranaryAndFarmInfo struct {
	FarmData   FarmData
	GranaryMap map[int32]int32
}
