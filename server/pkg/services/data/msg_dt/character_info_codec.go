package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdCharacterInfo          MsgCode = uint32(svc.CharacterInfo)*10000 + 1  // Req：请求角色信息；Res：返回角色信息；
	CdTryCharacterCreate     MsgCode = uint32(svc.CharacterInfo)*10000 + 2  // Req：创建角色；Res：创建角色-返回角色信息；
	CdEveCharacterInfoChange MsgCode = uint32(svc.CharacterInfo)*10000 + 3  // Eve：角色信息变更；
	CdCidByNickname          MsgCode = uint32(svc.CharacterInfo)*10000 + 4  // Req：根据昵称查询角色id；Res：根据昵称查询角色id；
	CdCharacterTest          MsgCode = uint32(svc.CharacterInfo)*10000 + 99 // Req：测试接口；Res：测试接口；
)

func InitCodecForCharacterInfo(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdCharacterInfo,
		func() interface{} {
			return &ReqCharacterInfo{}
		},
		func() interface{} {
			return &ResCharacterInfo{}
		})
	discovery.BindCoderFac(CdTryCharacterCreate,
		func() interface{} {
			return &ReqTryCharacterCreate{}
		},
		func() interface{} {
			return &ResTryCharacterCreate{}
		})
	discovery.BindCoderFac(CdEveCharacterInfoChange,
		nil,
		func() interface{} {
			return &EveCharacterInfoChange{}
		})
	discovery.BindCoderFac(CdCidByNickname,
		func() interface{} {
			return &ReqCidByNickname{}
		},
		func() interface{} {
			return &ResCidByNickname{}
		})
	discovery.BindCoderFac(CdCharacterTest,
		func() interface{} {
			return &ReqCharacterTest{}
		},
		func() interface{} {
			return &ResCharacterTest{}
		})
}
