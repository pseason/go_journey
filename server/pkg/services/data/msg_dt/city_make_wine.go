package msg_dt

import (
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/model"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = CityMakeWine

// @MessageCode = 1 获取酿酒数据
type ReqMakeWineList struct {
	BuildId int64
	Cid int64
}

// @MessageCode = 1 获取酿酒数据
type ResMakeWineList struct {
	List []*model.CityMakeWine
}

// @MessageCode = 2 开始酿酒
type ReqMakeWineStart struct {
	BuildId int64
	PropId int32	//固定10个
	Slot int32 		//酒槽位置
	Cid int64
	BuildLevel int32
}

// @MessageCode = 2 获取酿酒数据
type ResMakeWineStart struct {
	List *model.CityMakeWine
}

// @MessageCode = 3 取消酿酒
type ReqMakeWineCancel struct {
	Bid int64  //主键ID
	Cid int64
}

// @MessageCode = 3 取消酿酒
type ResMakeWineCancel struct {
	Bid int64  //主键ID
}

// @MessageCode = 4 收获酿酒
type ReqMakeWineReap struct {
	Bid int64  //主键ID
	Cid int64
}

// @MessageCode = 4 收获酿酒
type ResMakeWineReap struct {
	Bid int64  //主键ID
}

// @MessageCode = 5 销毁酿酒
type ReqMakeWineDestroy struct {
	BuildId int64
	Cid int64
}

// @MessageCode = 5 销毁酿酒
type ResMakeWineDestroy struct {
}

// @MessageCode = 6 获取酿酒信息
type ReqMakeWineListById struct {
	Bid int64  //主键ID
	Cid int64
	IsUpDelete bool	//更新数据状态
}

// @MessageCode = 6 获取酿酒信息
type ResMakeWineListById struct {
	List *model.CityMakeWine
}

// @messageCode = 7 打造物品
type ReqForgeItems struct {
	ForgeId int32
	BuildId int64
	Cid int64
}

// @messageCode = 7 打造物品
type ResForgeItems struct {
	ForgeTsv *tsv.ForgeTsv
}




