package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdBackpackList             MsgCode = uint32(svc.Backpack)*10000 + 1  // Req：获取背包数据；Res：获取背包数据；
	CdBackpackDiscard          MsgCode = uint32(svc.Backpack)*10000 + 2  // Req：丢弃物品；Res：丢弃物品；
	CdBackpackArrange          MsgCode = uint32(svc.Backpack)*10000 + 3  // Req：整理背包；Res：整理背包；
	CdBackpackDivision         MsgCode = uint32(svc.Backpack)*10000 + 4  // Req：拆分物品；Res：拆分物品；
	CdBackpackUseProp          MsgCode = uint32(svc.Backpack)*10000 + 5  // Req：使用物品；Res：客户端使用物品；
	CdBackpackMove             MsgCode = uint32(svc.Backpack)*10000 + 7  // Req：移动物品；Res：移动物品；
	CdEveBackpackGainNotify    MsgCode = uint32(svc.Backpack)*10000 + 8  // Eve：背包信息变更通知；
	CdEveBackpackSysAddNotify  MsgCode = uint32(svc.Backpack)*10000 + 9  // Eve：非背包功能物品增加飘字提示通知；
	CdBackpackGiftOpen         MsgCode = uint32(svc.Backpack)*10000 + 10 // Req：使用物品；Res：使用物品；
	CdBackpackGoldExchange     MsgCode = uint32(svc.Backpack)*10000 + 12 // Req：金币兑换；Res：金币兑换；
	CdBackpackIncrease         MsgCode = uint32(svc.Backpack)*10000 + 14 // Req：获得物品；Res：获得物品；
	CdBackpackSubtract         MsgCode = uint32(svc.Backpack)*10000 + 15 // Req：扣减物品；Res：扣减物品；
	CdBackpackGetProps         MsgCode = uint32(svc.Backpack)*10000 + 16 // Req：通过propID查询物品数量；Res：返回物品数量信息；
	CdBackpackGetPropById      MsgCode = uint32(svc.Backpack)*10000 + 17 // Req：通过BID查询单个物品信息；Res：返回单个物品信息；
	CdBackpackBatchEquip       MsgCode = uint32(svc.Backpack)*10000 + 18 // Req：批量穿戴；Res：批量穿戴；
	CdBackpackGetPropsByPropId MsgCode = uint32(svc.Backpack)*10000 + 19 // Req：查询单个物品信息；Res：返回单个物品信息；
	CdBackpackGetPropsByIds    MsgCode = uint32(svc.Backpack)*10000 + 20 // Req：通过主键ID查询物品信息；Res：返回通过主键ID查询物品信息；
	CdBackpackSubtractById     MsgCode = uint32(svc.Backpack)*10000 + 21 // Req：根据主键扣减物品；Res：根据主键扣减物品；
	CdBackpackIsFullLoad       MsgCode = uint32(svc.Backpack)*10000 + 22 // Req：查询背包是否满了；Res：查询背包是否满了；
)

func InitCodecForBackpack(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdBackpackList,
		func() interface{} {
			return &ReqBackpackList{}
		},
		func() interface{} {
			return &ResBackpackList{}
		})
	discovery.BindCoderFac(CdBackpackDiscard,
		func() interface{} {
			return &ReqBackpackDiscard{}
		},
		func() interface{} {
			return &ResBackpackDiscard{}
		})
	discovery.BindCoderFac(CdBackpackArrange,
		func() interface{} {
			return &ReqBackpackArrange{}
		},
		func() interface{} {
			return &ResBackpackArrange{}
		})
	discovery.BindCoderFac(CdBackpackDivision,
		func() interface{} {
			return &ReqBackpackDivision{}
		},
		func() interface{} {
			return &ResBackpackDivision{}
		})
	discovery.BindCoderFac(CdBackpackUseProp,
		func() interface{} {
			return &ReqBackpackUseProp{}
		},
		func() interface{} {
			return &ResBackpackUseProp{}
		})
	discovery.BindCoderFac(CdBackpackMove,
		func() interface{} {
			return &ReqBackpackMove{}
		},
		func() interface{} {
			return &ResBackpackMove{}
		})
	discovery.BindCoderFac(CdEveBackpackGainNotify,
		nil,
		func() interface{} {
			return &EveBackpackGainNotify{}
		})
	discovery.BindCoderFac(CdEveBackpackSysAddNotify,
		nil,
		func() interface{} {
			return &EveBackpackSysAddNotify{}
		})
	discovery.BindCoderFac(CdBackpackGiftOpen,
		func() interface{} {
			return &ReqBackpackGiftOpen{}
		},
		func() interface{} {
			return &ResBackpackGiftOpen{}
		})
	discovery.BindCoderFac(CdBackpackGoldExchange,
		func() interface{} {
			return &ReqBackpackGoldExchange{}
		},
		func() interface{} {
			return &ResBackpackGoldExchange{}
		})
	discovery.BindCoderFac(CdBackpackIncrease,
		func() interface{} {
			return &ReqBackpackIncrease{}
		},
		func() interface{} {
			return &ResBackpackIncrease{}
		})
	discovery.BindCoderFac(CdBackpackSubtract,
		func() interface{} {
			return &ReqBackpackSubtract{}
		},
		func() interface{} {
			return &ResBackpackSubtract{}
		})
	discovery.BindCoderFac(CdBackpackGetProps,
		func() interface{} {
			return &ReqBackpackGetProps{}
		},
		func() interface{} {
			return &ResBackpackGetProps{}
		})
	discovery.BindCoderFac(CdBackpackGetPropById,
		func() interface{} {
			return &ReqBackpackGetPropById{}
		},
		func() interface{} {
			return &ResBackpackGetPropById{}
		})
	discovery.BindCoderFac(CdBackpackBatchEquip,
		func() interface{} {
			return &ReqBackpackBatchEquip{}
		},
		func() interface{} {
			return &ResBackpackBatchEquip{}
		})
	discovery.BindCoderFac(CdBackpackGetPropsByPropId,
		func() interface{} {
			return &ReqBackpackGetPropsByPropId{}
		},
		func() interface{} {
			return &ResBackpackGetPropsByPropId{}
		})
	discovery.BindCoderFac(CdBackpackGetPropsByIds,
		func() interface{} {
			return &ReqBackpackGetPropsByIds{}
		},
		func() interface{} {
			return &ResBackpackGetPropsByIds{}
		})
	discovery.BindCoderFac(CdBackpackSubtractById,
		func() interface{} {
			return &ReqBackpackSubtractById{}
		},
		func() interface{} {
			return &ResBackpackSubtractById{}
		})
	discovery.BindCoderFac(CdBackpackIsFullLoad,
		func() interface{} {
			return &ReqBackpackIsFullLoad{}
		},
		func() interface{} {
			return &ResBackpackIsFullLoad{}
		})
}
