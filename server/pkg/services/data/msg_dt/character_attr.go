package msg_dt

/*
@Time   : 2021-11-10 16:26
@Author : wushu
@DESC   :
*/

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = CharacterAttr

// @MessageCode = 1 创建角色战斗属性数据
type ReqTryCharacterAttrCreate struct {
}

// @MessageCode = 1 创建角色战斗属性数据
type ResTryCharacterAttrCreate struct {
}