package err_dt

import (
	"95eh.com/eg/utils"
	"hm/pkg/game/svc"
)

/*
@Time   : 2021-11-12 17:40
@Author : wushu
@DESC   : 定义消息响应码(基本都是一些错误码)
	errCode在整个节点中是唯一的，以服务号作为前五位，后四位自增(但要注意iota不要超过10000)

	todo 错误码不需要太精确，精确的错误信息通过链路日志打印
*/

type ErrCode = utils.TErrCode

const (
	// 查询失败
	ErrQueryFailed = ErrCode(uint32(svc.Sys)*10000) + iota
	// 创建失败
	ErrCreateFailed
	// 修改失败
	ErrUpdateFailed
	// 删除失败
	ErrDelFailed
	// Try失败
	ErrTryFailed
)

// characterInfo
const (
	// 昵称被占用
	ErrCharacterNicknameBeingUse = ErrCode(uint32(svc.CharacterInfo)*10000) + iota
)
