package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdMakeWineList     MsgCode = uint32(svc.CityMakeWine)*10000 + 1 // Req：获取酿酒数据；Res：获取酿酒数据；
	CdMakeWineStart    MsgCode = uint32(svc.CityMakeWine)*10000 + 2 // Req：开始酿酒；Res：获取酿酒数据；
	CdMakeWineCancel   MsgCode = uint32(svc.CityMakeWine)*10000 + 3 // Req：取消酿酒；Res：取消酿酒；
	CdMakeWineReap     MsgCode = uint32(svc.CityMakeWine)*10000 + 4 // Req：收获酿酒；Res：收获酿酒；
	CdMakeWineDestroy  MsgCode = uint32(svc.CityMakeWine)*10000 + 5 // Req：销毁酿酒；Res：销毁酿酒；
	CdMakeWineListById MsgCode = uint32(svc.CityMakeWine)*10000 + 6 // Req：获取酿酒信息；Res：获取酿酒信息；
)

func InitCodecForCityMakeWine(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdMakeWineList,
		func() interface{} {
			return &ReqMakeWineList{}
		},
		func() interface{} {
			return &ResMakeWineList{}
		})
	discovery.BindCoderFac(CdMakeWineStart,
		func() interface{} {
			return &ReqMakeWineStart{}
		},
		func() interface{} {
			return &ResMakeWineStart{}
		})
	discovery.BindCoderFac(CdMakeWineCancel,
		func() interface{} {
			return &ReqMakeWineCancel{}
		},
		func() interface{} {
			return &ResMakeWineCancel{}
		})
	discovery.BindCoderFac(CdMakeWineReap,
		func() interface{} {
			return &ReqMakeWineReap{}
		},
		func() interface{} {
			return &ResMakeWineReap{}
		})
	discovery.BindCoderFac(CdMakeWineDestroy,
		func() interface{} {
			return &ReqMakeWineDestroy{}
		},
		func() interface{} {
			return &ResMakeWineDestroy{}
		})
	discovery.BindCoderFac(CdMakeWineListById,
		func() interface{} {
			return &ReqMakeWineListById{}
		},
		func() interface{} {
			return &ResMakeWineListById{}
		})
}
