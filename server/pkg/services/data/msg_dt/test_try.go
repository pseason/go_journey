package msg_dt

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = TestTry

// @MessageCode=1 请求订单
type ReqTestTryOrder struct {
	PropId uint32
	Count  uint16
}

// @MessageCode=1 返回订单花费
type ResTestTryOrder struct {
	PayType  uint8  //支付类型
	PayPrice uint32 //支付价格
}

// @MessageCode=2 请求冻结资产
type ReqTestTryHoldingAsset struct {
	PayType  uint8
	PayPrice uint32
}

// @MessageCode=2 返回冻结资产
type ResTestTryHoldingAsset struct {
}
