package msg_dt

import (
	"hm/pkg/services/data/model"
)

// 定义ServiceEnum用于自动生成编解码映射
// @ServiceEnum = Backpack

// @MessageCode = 1 获取背包数据
type ReqBackpackList struct {
	BackpackType model.BackpackType
	Cid int64
	WarehouseLevel int32
}

// @MessageCode = 1 获取背包数据
type ResBackpackList struct {
	BackpackList []*model.Backpack
	Num          int32 //剩余空格数
	WarehouseNum int32 //仓库格子数
}

// @MessageCode = 2 丢弃物品
type ReqBackpackDiscard struct {
	Bid int64
	Cid int64
}

// @MessageCode = 2 丢弃物品
type ResBackpackDiscard struct {
}

// @MessageCode = 3 整理背包
type ReqBackpackArrange struct {
	Cid int64
	Type model.BackpackType
}

// @MessageCode = 3 整理背包
type ResBackpackArrange struct {
	BackPackList []*model.Backpack
}

// @MessageCode = 4 拆分物品
type ReqBackpackDivision struct {
	Bid      int64
	SplitNum int32
	Cid int64
}

// @MessageCode = 4 拆分物品
type ResBackpackDivision struct {
	Ads *model.Backpack //被拆分新的物品列表
	Num int32			  //旧物品的剩余数量
}

// @MessageCode = 5 使用物品
type ReqBackpackUseProp struct {
	Bid int64
	Cid int64
}

// @MessageCode = 5 客户端使用物品
type ResBackpackUseProp struct {
	 Data *model.Backpack
	 Type int32
}

// @MessageCode = 7 移动物品
type ReqBackpackMove struct {
	Bid int64
	Cid int64
	WarehouseLevel int32
}
// @MessageCode = 7 移动物品
type ResBackpackMove struct {
	//背包信息变更 推送到客户端
}


// @MessageCode = 8 背包信息变更通知
type EveBackpackGainNotify struct {
	Cid int64
	Data []*model.Backpack
}

// @MessageCode = 9 非背包功能物品增加飘字提示通知
type EveBackpackSysAddNotify struct {
	Cid int64
	Data map[int32]int32
}

// @MessageCode = 10 使用物品
type ReqBackpackGiftOpen struct {
	Bid         int64
	SelectIndex int32
	Cid int64
}
// @MessageCode = 10 使用物品
type ResBackpackGiftOpen struct {
	//背包信息变更 推送到客户端
}

// @MessageCode = 12 金币兑换
type ReqBackpackGoldExchange struct {
	AttrId int64 //金币属性ID
	Num    int32
	Cid int64
}
// @MessageCode = 12 金币兑换
type ResBackpackGoldExchange struct {
	//角色金币信息变更 推送到客户端
}

// @MessageCode = 14 获得物品
type ReqBackpackIncrease struct {
	Props map[int32]int32
	Cid int64
	IsFullSendMail bool						//默认false-为背包满存不下|true-溢出物品发送邮件
}

// @MessageCode = 14 获得物品
type ResBackpackIncrease struct {
	//OverProps-需要发送附件,UpProps-更新物品数值,AddProps-新增物品，NoticeProps-通知客户端商品变更和获得物品飘字
	UpProps,AddProps,NoticeProps []*model.Backpack
	OverProps map[int32]int32
	Cid int64
}

// @MessageCode = 15 扣减物品
type ReqBackpackSubtract struct {
	PropType model.BackpackType
	Props map[int32]int32
	Cid int64
	IsFullSendMail bool
}

// @MessageCode = 15 扣减物品
type ResBackpackSubtract struct {
	UpProps,NoticePropIds []*model.Backpack
	DeleteIds []int64
	Cid int64
}

// @MessageCode = 16 通过propID查询物品数量
type ReqBackpackGetProps struct {
	PropIds []int32
	Cid int64
}

// @MessageCode = 16 返回物品数量信息
type ResBackpackGetProps struct {
	Props []*model.BackpackNum
}

// @MessageCode = 17 通过BID查询单个物品信息
type ReqBackpackGetPropById struct {
	Bid int64
	Cid int64
}

// @MessageCode = 17 返回单个物品信息
type ResBackpackGetPropById struct {
	Props *model.Backpack
}

// @MessageCode = 18 批量穿戴
type ReqBackpackBatchEquip struct {
	Props []int32
	Cid int64
}

// @MessageCode = 18 批量穿戴
type ResBackpackBatchEquip struct {
	//背包信息变更 推送到客户端
}

// @MessageCode = 19 查询单个物品信息
type ReqBackpackGetPropsByPropId struct {
	PropId int32
	Cid int64
}

// @MessageCode = 19 返回单个物品信息
type ResBackpackGetPropsByPropId struct {
	Props []*model.Backpack
}

// @MessageCode = 20 通过主键ID查询物品信息
type ReqBackpackGetPropsByIds struct {
	Type model.BackpackType
	Bids []int64
	Cid int64
}

// @MessageCode = 20 返回通过主键ID查询物品信息
type ResBackpackGetPropsByIds struct {
	Props []*model.Backpack
}

// @MessageCode = 21 根据主键扣减物品
type ReqBackpackSubtractById struct {
	PropType model.BackpackType
	Props map[int64]int32
	Cid int64
}

// @MessageCode = 21 根据主键扣减物品
type ResBackpackSubtractById struct {
	UpProps,NoticeProps []*model.Backpack
	DeleteIds []int64
	Cid int64
}

// @MessageCode = 22 查询背包是否满了
type ReqBackpackIsFullLoad struct {
	PropType model.BackpackType
	Cid int64
}

// @MessageCode = 22 查询背包是否满了
type ResBackpackIsFullLoad struct {
	IsFull bool
}