package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdTryTeamCreate                     MsgCode = uint32(svc.Team)*10000 + 1  // Req：请求创建队伍；Res：响应创建队伍；
	CdTeamGetTeamById                   MsgCode = uint32(svc.Team)*10000 + 2  // Req：请求查询队伍；Res：响应查询队伍；
	CdTeamApplyJoinTeam                 MsgCode = uint32(svc.Team)*10000 + 3  // Req：请求申请加入队伍；Res：响应申请加入队伍；
	CdTeamInviteJoinTeam                MsgCode = uint32(svc.Team)*10000 + 4  // Req：请求邀请加入队伍；Res：响应邀请加入队伍；
	CdTeamAgreeApplyJoinTeam            MsgCode = uint32(svc.Team)*10000 + 5  // Req：请求同意申请加入；Res：响应同意申请加入；
	CdTeamAgreeInviteJoinTeam           MsgCode = uint32(svc.Team)*10000 + 6  // Req：请求同意邀请加入；Res：响应同意邀请加入；
	CdEveTeamApplyNotify                MsgCode = uint32(svc.Team)*10000 + 7  // Eve：队伍申请通知；
	CdEveTeamInviteNotify               MsgCode = uint32(svc.Team)*10000 + 8  // Eve：队伍邀请通知；
	CdEveTeamMemberChange               MsgCode = uint32(svc.Team)*10000 + 9  // Eve：队伍成员更新；
	CdTeamRefuseApplyJoinTeam           MsgCode = uint32(svc.Team)*10000 + 10 // Req：请求拒绝申请加入；Res：响应拒绝申请加入；
	CdTryTeamUpdateTeamAutoJoin         MsgCode = uint32(svc.Team)*10000 + 11 // Req：请求设置自动同意；Res：响应设置自动同意；
	CdEveTeamAutoAgreeChange            MsgCode = uint32(svc.Team)*10000 + 12 // Eve：队伍自动同意变更；
	CdEveTeamAddNewMemberNotify         MsgCode = uint32(svc.Team)*10000 + 13 // Eve：队伍添加新成员通知；
	CdTeamOneClickApply                 MsgCode = uint32(svc.Team)*10000 + 14 // Req：请求一键申请；Res：响应一键申请；
	CdTeamUpdateTeamArms                MsgCode = uint32(svc.Team)*10000 + 15 // Req：请求设置队伍目标限制；Res：响应设置队伍目标限制；
	CdEveTeamUpdateTeamAimsChangeNotify MsgCode = uint32(svc.Team)*10000 + 16 // Eve：队伍修改目标变更通知；
	CdTryTeamLeaveTeam                  MsgCode = uint32(svc.Team)*10000 + 17 // Req：请求离开队伍；Res：响应离开队伍；
	CdTeamTransferTeam                  MsgCode = uint32(svc.Team)*10000 + 18 // Req：请求转移队长；Res：响应转移队长；
	CdTeamDisbandTeam                   MsgCode = uint32(svc.Team)*10000 + 19 // Req：请求解散队伍；Res：响应解散队伍；
	CdTryTeamPleaseLeaveTeam            MsgCode = uint32(svc.Team)*10000 + 20 // Req：请求踢出人员；Res：响应踢出人员；
	CdTeamPageQueryTeam                 MsgCode = uint32(svc.Team)*10000 + 21 // Req：请求分页查询队伍；Res：响应分页查询队伍；
	CdTeamApplyList                     MsgCode = uint32(svc.Team)*10000 + 22 // Req：请求队伍申请列表；Res：响应队伍申请列表；
	CdEveTeamLoadChange                 MsgCode = uint32(svc.Team)*10000 + 23 // Eve：队伍成员更新；
	CdTryTeamAutoAgreeJoin              MsgCode = uint32(svc.Team)*10000 + 24 // Req：请求自动同意加入队伍；Res：响应自动同意加入队伍；
)

func InitCodecForTeam(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdTryTeamCreate,
		func() interface{} {
			return &ReqTryTeamCreate{}
		},
		func() interface{} {
			return &ResTryTeamCreate{}
		})
	discovery.BindCoderFac(CdTeamGetTeamById,
		func() interface{} {
			return &ReqTeamGetTeamById{}
		},
		func() interface{} {
			return &ResTeamGetTeamById{}
		})
	discovery.BindCoderFac(CdTeamApplyJoinTeam,
		func() interface{} {
			return &ReqTeamApplyJoinTeam{}
		},
		func() interface{} {
			return &ResTeamApplyJoinTeam{}
		})
	discovery.BindCoderFac(CdTeamInviteJoinTeam,
		func() interface{} {
			return &ReqTeamInviteJoinTeam{}
		},
		func() interface{} {
			return &ResTeamInviteJoinTeam{}
		})
	discovery.BindCoderFac(CdTeamAgreeApplyJoinTeam,
		func() interface{} {
			return &ReqTeamAgreeApplyJoinTeam{}
		},
		func() interface{} {
			return &ResTeamAgreeApplyJoinTeam{}
		})
	discovery.BindCoderFac(CdTeamAgreeInviteJoinTeam,
		func() interface{} {
			return &ReqTeamAgreeInviteJoinTeam{}
		},
		func() interface{} {
			return &ResTeamAgreeInviteJoinTeam{}
		})
	discovery.BindCoderFac(CdEveTeamApplyNotify,
		nil,
		func() interface{} {
			return &EveTeamApplyNotify{}
		})
	discovery.BindCoderFac(CdEveTeamInviteNotify,
		nil,
		func() interface{} {
			return &EveTeamInviteNotify{}
		})
	discovery.BindCoderFac(CdEveTeamMemberChange,
		nil,
		func() interface{} {
			return &EveTeamMemberChange{}
		})
	discovery.BindCoderFac(CdTeamRefuseApplyJoinTeam,
		func() interface{} {
			return &ReqTeamRefuseApplyJoinTeam{}
		},
		func() interface{} {
			return &ResTeamRefuseApplyJoinTeam{}
		})
	discovery.BindCoderFac(CdTryTeamUpdateTeamAutoJoin,
		func() interface{} {
			return &ReqTryTeamUpdateTeamAutoJoin{}
		},
		func() interface{} {
			return &ResTryTeamUpdateTeamAutoJoin{}
		})
	discovery.BindCoderFac(CdEveTeamAutoAgreeChange,
		nil,
		func() interface{} {
			return &EveTeamAutoAgreeChange{}
		})
	discovery.BindCoderFac(CdEveTeamAddNewMemberNotify,
		nil,
		func() interface{} {
			return &EveTeamAddNewMemberNotify{}
		})
	discovery.BindCoderFac(CdTeamOneClickApply,
		func() interface{} {
			return &ReqTeamOneClickApply{}
		},
		func() interface{} {
			return &ResTeamOneClickApply{}
		})
	discovery.BindCoderFac(CdTeamUpdateTeamArms,
		func() interface{} {
			return &ReqTeamUpdateTeamArms{}
		},
		func() interface{} {
			return &ResTeamUpdateTeamArms{}
		})
	discovery.BindCoderFac(CdEveTeamUpdateTeamAimsChangeNotify,
		nil,
		func() interface{} {
			return &EveTeamUpdateTeamAimsChangeNotify{}
		})
	discovery.BindCoderFac(CdTryTeamLeaveTeam,
		func() interface{} {
			return &ReqTryTeamLeaveTeam{}
		},
		func() interface{} {
			return &ResTryTeamLeaveTeam{}
		})
	discovery.BindCoderFac(CdTeamTransferTeam,
		func() interface{} {
			return &ReqTeamTransferTeam{}
		},
		func() interface{} {
			return &ResTeamTransferTeam{}
		})
	discovery.BindCoderFac(CdTeamDisbandTeam,
		func() interface{} {
			return &ReqTeamDisbandTeam{}
		},
		func() interface{} {
			return &ResTeamDisbandTeam{}
		})
	discovery.BindCoderFac(CdTryTeamPleaseLeaveTeam,
		func() interface{} {
			return &ReqTryTeamPleaseLeaveTeam{}
		},
		func() interface{} {
			return &ResTryTeamPleaseLeaveTeam{}
		})
	discovery.BindCoderFac(CdTeamPageQueryTeam,
		func() interface{} {
			return &ReqTeamPageQueryTeam{}
		},
		func() interface{} {
			return &ResTeamPageQueryTeam{}
		})
	discovery.BindCoderFac(CdTeamApplyList,
		func() interface{} {
			return &ReqTeamApplyList{}
		},
		func() interface{} {
			return &ResTeamApplyList{}
		})
	discovery.BindCoderFac(CdEveTeamLoadChange,
		nil,
		func() interface{} {
			return &EveTeamLoadChange{}
		})
	discovery.BindCoderFac(CdTryTeamAutoAgreeJoin,
		func() interface{} {
			return &ReqTryTeamAutoAgreeJoin{}
		},
		func() interface{} {
			return &ResTryTeamAutoAgreeJoin{}
		})
}
