# 用于自动生成codec的语法

消息结构体以`Req`、`Res`、`Eve`开头

`Req`和`Res`一般是成对出现的

`Eve`用于主动推送。因为推送是基于事件机制，所以以`Eve`(event)开头；可以理解为通常印象中的`Notice`

每个消息结构体文件上用`// @ServiceEnum = xxx`指明消息所属的服务名，用作生成code枚举的值<br>
每个消息结构体上用`// @MessageCode = n`指明消息号，`n`的范围是四位数，不能超过10000


(ps：这里写的MessageCode，在生成的codec文件中会和服务号拼接最为最终的消息号)