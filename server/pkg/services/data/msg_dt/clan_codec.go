package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdClanTryCreate          MsgCode = uint32(svc.Clan)*10000 + 1 // Req：请求创建氏族；Res：响应创建氏族；
	CdClanLoad               MsgCode = uint32(svc.Clan)*10000 + 2 // Req：请求加载氏族；Res：响应加载氏族；
	CdClanTryUpNameOrSurname MsgCode = uint32(svc.Clan)*10000 + 3 // Req：请求修改氏族名称和姓氏；Res：响应修改氏族名称和姓氏；
	CdClanTryUpDeclaration   MsgCode = uint32(svc.Clan)*10000 + 4 // Req：请求修改氏族宣言；Res：响应修改氏族宣言；
	CdEveClanNoticeChange    MsgCode = uint32(svc.Clan)*10000 + 5 // Eve：响应修改氏族宣言；
	CdClanPageSearch         MsgCode = uint32(svc.Clan)*10000 + 6 // Req：分页请求氏族；Res：响应分页请求氏族；
	CdClanTrySignIn          MsgCode = uint32(svc.Clan)*10000 + 7 // Req：请求氏族签到；Res：响应氏族签到；
)

func InitCodecForClan(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdClanTryCreate,
		func() interface{} {
			return &ReqClanTryCreate{}
		},
		func() interface{} {
			return &ResClanTryCreate{}
		})
	discovery.BindCoderFac(CdClanLoad,
		func() interface{} {
			return &ReqClanLoad{}
		},
		func() interface{} {
			return &ResClanLoad{}
		})
	discovery.BindCoderFac(CdClanTryUpNameOrSurname,
		func() interface{} {
			return &ReqClanTryUpNameOrSurname{}
		},
		func() interface{} {
			return &ResClanTryUpNameOrSurname{}
		})
	discovery.BindCoderFac(CdClanTryUpDeclaration,
		func() interface{} {
			return &ReqClanTryUpDeclaration{}
		},
		func() interface{} {
			return &ResClanTryUpDeclaration{}
		})
	discovery.BindCoderFac(CdEveClanNoticeChange,
		nil,
		func() interface{} {
			return &EveClanNoticeChange{}
		})
	discovery.BindCoderFac(CdClanPageSearch,
		func() interface{} {
			return &ReqClanPageSearch{}
		},
		func() interface{} {
			return &ResClanPageSearch{}
		})
	discovery.BindCoderFac(CdClanTrySignIn,
		func() interface{} {
			return &ReqClanTrySignIn{}
		},
		func() interface{} {
			return &ResClanTrySignIn{}
		})
}
