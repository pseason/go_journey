package msg_dt

import (
	"95eh.com/eg/intfc"
	"hm/pkg/game/svc"
)

const (
	CdMarketList        MsgCode = uint32(svc.CityMarket)*10000 + 1 // Req：根据关键字或类型分页查询物品；Res：根据关键字分页查询物品；
	CdMarketRecord      MsgCode = uint32(svc.CityMarket)*10000 + 2 // Req：分页查询状态物品|默认上架；Res：分页查询状态物品|默认上架；
	CdMarketBuy         MsgCode = uint32(svc.CityMarket)*10000 + 3 // Req：购买-更新物品状态信息；Res：购买-更新物品状态信息；
	CdMarketPutInto     MsgCode = uint32(svc.CityMarket)*10000 + 4 // Req：上架；Res：返回上架后的新数据；
	CdMarketRemove      MsgCode = uint32(svc.CityMarket)*10000 + 5 // Req：手动下架；Res：手动下架；
	CdMarketGetUpCount  MsgCode = uint32(svc.CityMarket)*10000 + 6 // Req：请求获取上架的数量-编译game取建筑上限判断；Res：获取上架的数量；
	CdMarketGetPropById MsgCode = uint32(svc.CityMarket)*10000 + 7 // Req：获取单个物品信息；Res：获取单个物品信息；
	CdMarketAutoCancel  MsgCode = uint32(svc.CityMarket)*10000 + 8 // Req：自动更新物品状态信息；Res：自动更新物品状态信息；
)

func InitCodecForCityMarket(discovery intfc.IMDiscovery) {
	discovery.BindCoderFac(CdMarketList,
		func() interface{} {
			return &ReqMarketTitleList{}
		},
		func() interface{} {
			return &ResMarketList{}
		})
	discovery.BindCoderFac(CdMarketRecord,
		func() interface{} {
			return &ReqMarketRecord{}
		},
		func() interface{} {
			return &ResMarketRecord{}
		})
	discovery.BindCoderFac(CdMarketBuy,
		func() interface{} {
			return &ReqMarketBuy{}
		},
		func() interface{} {
			return &ResMarketBuy{}
		})
	discovery.BindCoderFac(CdMarketPutInto,
		func() interface{} {
			return &ReqMarketPutInto{}
		},
		func() interface{} {
			return &ResMarketPutInto{}
		})
	discovery.BindCoderFac(CdMarketRemove,
		func() interface{} {
			return &ReqMarketRemove{}
		},
		func() interface{} {
			return &ResMarketRemove{}
		})
	discovery.BindCoderFac(CdMarketGetUpCount,
		func() interface{} {
			return &ReqMarketGetUpCount{}
		},
		func() interface{} {
			return &ResMarketGetUpCount{}
		})
	discovery.BindCoderFac(CdMarketGetPropById,
		func() interface{} {
			return &ReqMarketGetPropById{}
		},
		func() interface{} {
			return &ResMarketGetPropById{}
		})
	discovery.BindCoderFac(CdMarketAutoCancel,
		func() interface{} {
			return &ReqMarketAutoCancel{}
		},
		func() interface{} {
			return &ResMarketAutoCancel{}
		})
}
