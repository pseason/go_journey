package common

/*
@Time   : 2021-12-21 22:33
@Author : wushu
@DESC   :
*/

// 错误描述
const (
	TPrimaryKeyIs0                 = "primary key is 0"                                // 主键为0
	TNoValidDbFields               = "no valid db fields"                              // 没有有效的数据库字段
	TTCCTryFail                    = "tcc-try fail"                                    // tcc-try阶段失败
	TTCCNoValidFields              = "tcc-try fail: no valid fields"                   // 没有有效字段
	TTCCTryFail_Negative           = "tcc-try fail: trying to update negative numbers" // tcc-try阶段失败，存在负值更新
	TTCCFieldCannotBeModified      = "field cannot be modified"                        // 字段不允许修改
	TTCCExistNonNumericField       = "exist non-numeric field"                         // 存在非数值字段
	TNonTxSession                  = "non transaction session"                         // 不是事务会话
	TNotImplementedIModelWithRedis = "not implemented misc.IModelWithRedis"            // 没有实现 misc.IModelWithRedis
)

// 非错误描述
const (
	TModelUpdate                                 = "model update"
	TAllValuesAreIgnoredWhenUpdatedToTheDatabase = "all values are ignored when updated to the database" // 向数据库更新时，所有的值都被忽略了
)
