package common

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"github.com/go-redis/redis/v8"
	consulApi "github.com/hashicorp/consul/api"
	"hm/pkg/misc"
	"hm/pkg/misc/design"
)

var (
	_Conf = &Config{}
)

func LoadConf(root string, paths ...string) (conf *Config, err error) {
	err = utils.StartLocalConf(root, _Conf, paths...)
	return _Conf, err
}

type Config struct {
	Debug        bool
	Discovery    intfc.DiscoveryConf
	Consul       *consulApi.Config
	MySQL        misc.MySQL
	Redis        *redis.Options
	Design       design.Design // 策划配置
	DataServices []string      // 当前数据服务节点提供的服务领域
}
