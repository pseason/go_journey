package common

import (
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"testing"
)

/*
@Time   : 2021-11-11 20:10
@Author : wushu
@DESC   :
*/

var redisClient *redis.Client

func init() {
	redisClient = redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
		//Password: "123456",
		//Ip:     "119.3.141.105:46379",
		//Password: "G#SrCb15COhN5Br&",
	})
}

func TestXAdd(t *testing.T) {
	utils.SetSnowflakeRegionNodeId(1)
	//id := utils.GenSnowflakeRegionNodeId()
	err := redisClient.XAdd(context.TODO(), &redis.XAddArgs{
		//ID:     strconv.FormatInt(id, 10),
		//ID:     "1-0",
		Stream: "stream",
		Values: map[string]interface{}{"code": 1000},
	}).Err()
	if err != nil {
		t.Error("add failed", err.Error())
		return
	}
	result, err := redisClient.XReadStreams(context.TODO(), "stream", "0").Result()
	if err != nil {
		t.Error("read failed", err.Error())
		return
	}
	fmt.Println(result)
}

func TestHGetAll(t *testing.T) {
	key := "tx:status:1461158611492278272"
	result, err := redisClient.HGetAll(context.Background(), key).Result()
	log.Debug("result", utils.M{
		"result": result,
		"error":  err.Error(),
	})
}

func TestHSetMap(t *testing.T) {
	m := map[string]interface{}{
		"1": "111",
		"2": "222",
	}
	//c := model.CharacterInfo{Uid: 1, Nickname: "嘻嘻", Age: 20, ClanId: 1111111111111111}
	//mapstructure.Decode(c, &m)
	//delete(m,"BaseModel")
	//result, err := redisClient.HSet(context.TODO(), "test:hset", 1, 2).Result()
	result, err := redisClient.HSet(context.TODO(), "test:hset", m).Result()
	fmt.Println(result, err)
}
