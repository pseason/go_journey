package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"encoding/json"
	"fmt"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/misc/cache_key"
	utils2 "hm/pkg/misc/utils"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
	"reflect"
)

/*
@Time   : 2021-12-21 17:47
@Author : wushu
@DESC   : 通用接口
*/

// 提取数据库字段
//  (保留数据库字段，但去掉id：id会在后边的逻辑添加)
func extractDbFields(m model_e.Model, fields []model_field_e.Enum) (dbFields []string) {
	for _, field := range fields {
		fieldName := model_e.ToFieldName(m, field)
		if model_e.IsInDb(m, field) && fieldName != misc.ModelIdFiledName {
			dbFields = append(dbFields, fieldName)
		}
	}
	return
}

func (bs *baseService) CommonQueryById(ac IActionCtx, m model_e.Model, fields []model_field_e.Enum, id int64) (data interface{}, iError utils.IError) {
	// 应当优先从缓存查询。缓存中没有的话，去数据库中查询。暂时只从数据库查询
	if id == 0 {
		return nil, utils.NewError(TPrimaryKeyIs0, nil)
	}

	// 优先从缓存中查询
	if model_e.IsWithRedis(m) {
		res, ids, iError := bs.GetFromRedis(ac, m, fields, id)
		if iError != nil {
			return nil, iError
		}
		if len(ids) == 0 {
			return res[id], nil
		}
	}

	// 缓存中没有的，从数据库中查询
	db := bs.Db()
	var selStmt []string
	if len(fields) != 0 {
		selStmt = extractDbFields(m, fields)
		if len(selStmt) != 0 {
			selStmt = append(selStmt, misc.ModelIdFiledName) // 默认追加查询id
			db = db.Select(selStmt)
		} else {
			return nil, utils.NewError(TNoValidDbFields, utils.M{"fields": fields})
		}
	}

	log.Debug("exec common query by id", utils.M{"model": model_e.ModelName(m), "selStmt": selStmt, "selFields": selStmt})

	data = model_e.NewModelPtr(m)
	err := db.First(data, id).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, utils.NewError(err.Error(), nil)
	}
	return data, nil
}

func (bs *baseService) CommonQueryByIds(ac IActionCtx, m model_e.Model, fields []model_field_e.Enum, ids []int64) (data map[int64]interface{}, iError utils.IError) {
	// 应当优先从缓存查询。缓存中没有的话，去数据库中查询。暂时只从数据库查询
	if len(ids) == 0 {
		return nil, utils.NewError(TPrimaryKeyIs0, nil)
	}

	data = make(map[int64]interface{})

	// 优先从缓存中查询
	if model_e.IsWithRedis(m) {
		data, ids, iError = bs.GetFromRedis(ac, m, fields, ids...) // 返回的ids是redis中没有的ids，需要在数据库中查询
		if iError != nil {
			return
		}
		if len(ids) == 0 {
			return data, nil
		}
	}

	// 缓存中没有的，从数据库中查询
	db := bs.Db()
	var selStmt []string
	if len(fields) != 0 {
		selStmt = extractDbFields(m, fields)
		if len(selStmt) != 0 {
			selStmt = append(selStmt, misc.ModelIdFiledName) // 默认追加查询id，用于转为map结果时的key
			db = db.Select(selStmt)
		} else {
			return nil, utils.NewError(TNoValidDbFields, utils.M{"fields": fields})
		}
	}

	log.Debug("exec common query by ids", utils.M{"model": model_e.ModelName(m), "selStmt": selStmt, "ids": ids, "selFields": selStmt})

	result := model_e.NewModelPtrSlicePtr(m)
	err := db.Model(model_e.NewModelPtr(m)).Find(result, ids).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, utils.NewError(err.Error(), nil)
	}

	// 通过反射将查询结果(slice)设置到返回的map中
	elem := reflect.ValueOf(result).Elem()
	for i := 0; i < elem.Len(); i++ {
		fc := elem.Index(i).Interface()
		iBaseModel := fc.(misc.IModel)
		id := iBaseModel.GetId()
		data[id] = fc
	}

	return data, nil
}

func (bs *baseService) TryCommonUpdateById(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (iError utils.IError) {
	app.Log().Debug("CdCommonUpdateById commonTxActions try", nil)

	// 验证有效字段
	for field, upVal := range upData {
		if !model_e.IsModifiable(m, field) {
			return utils.NewError(TTCCFieldCannotBeModified, utils.M{"field": model_e.ToFieldName(m, field), "value": upVal})
		}
	}

	if len(upData) == 0 {
		return utils.NewError(TTCCNoValidFields, utils.M{"upData": upData})
	}

	// 验证 (暂时没有需要验证的，具体业务中若有验证规则 则重写此接口进行验证)

	return nil
}

func (bs *baseService) ConfirmCommonUpdateById(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (iError utils.IError) {
	app.Log().Debug("CdCommonUpdateById commonTxActions confirm", nil)

	// 更新到数据库
	upStmt := make(map[string]interface{})
	for k, v := range upData {
		if model_e.IsInDb(m, k) {
			upStmt[model_e.ToFieldName(m, k)] = v
		}
	}
	if len(upStmt) != 0 {
		err := bs.Db().Model(model_e.NewModelPtr(m)).Where(fmt.Sprintf("%s = ?", misc.ModelIdFiledName), id).Updates(upStmt).Error
		if err != nil {
			return utils.NewError(err.Error(), utils.M{"id": id})
		}
	} else {
		app.Log().TDebug(ac.Tid(), TAllValuesAreIgnoredWhenUpdatedToTheDatabase, nil)
	}

	// 更新到redis
	if model_e.IsWithRedis(m) {
		iError := bs.UpdateToRedis(ac, m, upData, id)
		// todo 暂时不能抛出错误，否则confirm会不断重试，而这里没有做好幂等处理，重试时会再次更新到数据库
		if iError != nil {
			app.Log().TError(ac.Tid(), iError)
		}
	}

	return nil
}

func (bs *baseService) CancelCommonUpdateById(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (iError utils.IError) {
	app.Log().Debug("CdCommonUpdateById commonTxActions cancel", nil)
	return nil
}

// 查询数据，仅验证数值类型资源是否足够
func (bs *baseService) TryCommonUpdateNumById(ac IActionCtx, m model_e.Model, incrData map[model_field_e.Enum]int32, id int64) (origData interface{}, iError utils.IError) {
	app.Log().Debug("CdCommonUpdateNumById commonTxActions try", nil)

	// 验证有效字段
	var upFields []model_field_e.Enum
	for field, incr := range incrData {
		if !model_e.IsModifiable(m, field) {
			return nil, utils.NewError(TTCCFieldCannotBeModified, utils.M{"field": model_e.ToFieldName(m, field), "incr": incr})
		}
		if !model_e.IsInt32(m, field) || incr == 0 {
			return nil, utils.NewError(TTCCExistNonNumericField, utils.M{"field": model_e.ToFieldName(m, field), "incr": incr})
		}
		upFields = append(upFields, field)
	}

	if len(upFields) == 0 {
		return nil, utils.NewError(TTCCNoValidFields, utils.M{"incrData": incrData})
	}

	data, iError := bs.CommonQueryById(ac, m, upFields, id)
	if iError != nil {
		return nil, iError
	}
	// 验证数值是否足够
	iModel := data.(misc.IModel)
	for field, incr := range incrData {
		result := iModel.GetByEnum(field).(int32) + incr
		if result < 0 {
			return nil, utils.NewError(TTCCTryFail_Negative, utils.M{"field": model_e.ToFieldName(m, field), "updated": result})
		}
	}

	return data, nil
}

func (bs *baseService) ConfirmCommonUpdateNumById(ac IActionCtx, m model_e.Model, incrData map[model_field_e.Enum]int32, id int64, origData interface{}) (iError utils.IError) {
	app.Log().Debug("CdCommonUpdateNumById commonTxActions confirm", nil)

	// 更新到数据库
	upStmt := make(map[string]interface{})
	for fieldCode, upVal := range incrData {
		if model_e.IsInDb(m, fieldCode) {
			fieldName := model_e.ToFieldName(m, fieldCode)
			upStmt[fieldName] = gorm.Expr(fmt.Sprintf("%s + ?", fieldName), upVal)
		}
	}
	if len(upStmt) != 0 {
		err := bs.Db().Model(model_e.NewModelPtr(m)).Where(fmt.Sprintf("%s = ?", misc.ModelIdFiledName), id).Updates(upStmt).Error
		if err != nil {
			return utils.NewError(err.Error(), utils.M{"id": id})
		}
	} else {
		app.Log().TDebug(ac.Tid(), TAllValuesAreIgnoredWhenUpdatedToTheDatabase, nil)
	}

	// 更新到redis
	if model_e.IsWithRedis(m) {
		var updated = make(map[model_field_e.Enum]interface{})
		origModel := origData.(misc.IModel)
		for k, incr := range incrData {
			updated[k] = incr + origModel.GetByEnum(k).(int32)
		}
		iErr := bs.UpdateToRedis(ac, m, updated, id)
		// todo 暂时不能抛出错误，否则confirm会不断重试，而这里没有做好幂等处理，重试时会再次更新到数据库
		if iErr != nil {
			app.Log().TError(ac.Tid(), iErr)
		}
	}

	return nil
}

func (bs *baseService) CancelCommonUpdateNumById(ac IActionCtx, m model_e.Model, incrData map[model_field_e.Enum]int32, id int64) (iError utils.IError) {
	app.Log().Debug("CdCommonUpdateNumById commonTxActions cancel", nil)
	return nil
}

func (bs *baseService) CommonUpdateById(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (iError utils.IError) {
	if !utils2.IsTxConn(ac.TxDb()) {
		return utils.NewError(TNonTxSession, nil)
	}

	// 验证。(可以利用分布式事务的try进行验证)
	if iError = bs.TryCommonUpdateById(ac, m, upData, id); iError != nil {
		return iError
	}

	// 更新到数据库
	upStmt := make(map[string]interface{})
	for k, v := range upData {
		if model_e.IsInDb(m, k) {
			upStmt[model_e.ToFieldName(m, k)] = v
		}
	}
	if len(upStmt) != 0 {
		err := ac.TxDb().Model(model_e.NewModelPtr(m)).Where(fmt.Sprintf("%s = ?", misc.ModelIdFiledName), id).Updates(upStmt).Error
		if err != nil {
			return utils.NewError(err.Error(), utils.M{"id": id})
		}
	} else {
		app.Log().TDebug(ac.Tid(), TAllValuesAreIgnoredWhenUpdatedToTheDatabase, nil)
	}

	// 事务回调
	ac.AddTxCommitCallback(func() {
		// 数据更新回调
		updateMonitor := bs.iService.CommonUpdateMonitor()
		if updateMonitor != nil && updateMonitor[m] != nil {
			updated := make(map[model_field_e.Enum]interface{})
			for k, upVal := range upData {
				updated[k] = upVal
			}
			updateMonitor[m](ac, updated)
		}
		// 更新到redis
		if model_e.IsWithRedis(m) {
			iErr := bs.UpdateToRedis(ac, m, upData, id)
			// todo 暂时不能抛出错误，否则confirm会不断重试，而这里没有做好幂等处理，重试时会再次更新到数据库
			if iErr != nil {
				app.Log().TError(ac.Tid(), iErr)
			}
		}
	})

	return nil
}

// 先检查，再修改
func (bs *baseService) CommonUpdateNumById(ac IActionCtx, m model_e.Model, incrData map[model_field_e.Enum]int32, id int64) (iError utils.IError) {
	if !utils2.IsTxConn(ac.TxDb()) {
		return utils.NewError(TNonTxSession, nil)
	}

	// 验证。(可以利用分布式事务的try进行验证)
	origData, iError := bs.TryCommonUpdateNumById(ac, m, incrData, id)
	if iError != nil {
		return
	}

	// 更新到数据库
	upStmt := make(map[string]interface{})
	for fieldCode, upVal := range incrData {
		if model_e.IsInDb(m, fieldCode) {
			fieldName := model_e.ToFieldName(m, fieldCode)
			upStmt[fieldName] = gorm.Expr(fmt.Sprintf("%s + ?", fieldName), upVal)
		}
	}
	if len(upStmt) != 0 {
		err := ac.TxDb().Model(model_e.NewModelPtr(m)).Where(fmt.Sprintf("%s = ?", misc.ModelIdFiledName), id).Updates(upStmt).Error
		if err != nil {
			return utils.NewError(err.Error(), utils.M{"id": id})
		}
	} else {
		app.Log().TDebug(ac.Tid(), TAllValuesAreIgnoredWhenUpdatedToTheDatabase, nil)
	}

	// 事务回调
	ac.AddTxCommitCallback(func() {
		var updated = make(map[model_field_e.Enum]interface{})
		origModel := origData.(misc.IModel)
		for k, incr := range incrData {
			updated[k] = incr + origModel.GetByEnum(k).(int32)
		}
		// 数据更新回调
		updateMonitor := bs.iService.CommonUpdateMonitor()
		if updateMonitor != nil && updateMonitor[m] != nil {
			updateMonitor[m](ac, updated)
		}
		// 更新到redis
		if model_e.IsWithRedis(m) {
			iErr := bs.UpdateToRedis(ac, m, updated, id)
			// todo 暂时不能抛出错误，否则confirm会不断重试，而这里没有做好幂等处理，重试时会再次更新到数据库
			if iErr != nil {
				app.Log().TError(ac.Tid(), iErr)
			}
		}
	})

	return
}

func (bs *baseService) GetFromRedis(ac IActionCtx, m model_e.Model, fields []model_field_e.Enum, ids ...int64) (data map[int64]interface{}, nonexistentIds []int64, iError utils.IError) {
	data = make(map[int64]interface{})
	for _, id := range ids {
		redisKey := cache_key.CharacterInfo.Format(id)
		exists := bs.Redis().Exists(bs.Context(), redisKey).Val()
		if exists == 0 {
			nonexistentIds = append(nonexistentIds, id)
			continue
		}
		var dataMap map[string]string
		if len(fields) != 0 {
			var selStmt []string
			for _, f := range fields {
				selStmt = append(selStmt, model_e.ToFieldName(m, f))
			}
			data := bs.Redis().HMGet(bs.Context(), redisKey, selStmt...).Val()
			// 将结果按照顺序放到map中
			dataMap = make(map[string]string)
			for i, field := range fields {
				// data[i] 只会是string或nil
				s, ok := data[i].(string)
				if ok {
					dataMap[model_e.ToFieldName(m, field)] = s
				} else {
					dataMap[model_e.ToFieldName(m, field)] = ""
				}
			}
		} else {
			dataMap = bs.Redis().HGetAll(bs.Context(), redisKey).Val()
		}
		// map转结构体
		modelPtr := model_e.NewModelPtr(m)
		iModelWithRedis, ok := modelPtr.(misc.IModelWithRedis)
		if !ok {
			return nil, nil, utils.NewError(TNotImplementedIModelWithRedis, utils.M{"model": reflect.ValueOf(modelPtr).Type()})
		}
		iModelWithRedis.FetchRedisMapData(dataMap)
		iModelWithRedis.SetId(id) // 即使没有查询id，这里也补充上了
		data[id] = iModelWithRedis
	}
	return
}

func (bs *baseService) UpdateToRedis(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, ids ...int64) (iError utils.IError) {
	for _, id := range ids {
		redisKey := cache_key.CharacterInfo.Format(id)
		exists := bs.Redis().Exists(bs.Context(), redisKey).Val()
		if exists != 0 {
			var upStmt []interface{}
			for field, v := range upData {
				if model_e.IsModifiable(m, field) {
					if n, ok := v.(json.Number); ok { // tcc-confirm/cancel阶段的req是try阶段的req通过json序列化/反序列化得到的，json序列化会导致upData中的数值类型为json.Number
						upStmt = append(upStmt, model_e.ToFieldName(m, field), n.String())
					} else {
						upStmt = append(upStmt, model_e.ToFieldName(m, field), v)
					}
				}
			}
			err := bs.Redis().HSet(bs.Context(), redisKey, upStmt...).Err()
			if err != nil {
				return utils.NewError(err.Error(), nil)
			}
		}
	}
	return
}
