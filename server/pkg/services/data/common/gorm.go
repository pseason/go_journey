package common

import (
	"gorm.io/gorm"
	"hm/pkg/misc"
)

var (
	_GormDb *gorm.DB
)

func LoadGorm(config misc.MySQL) *gorm.DB {
	_GormDb = misc.GetGormDbConn(config)
	return _GormDb
}
