package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
)

// 逻辑处理上下文
type IActionCtx interface {
	// traceId
	Tid() int64
	// 当前服务的主体id，是game节点 baseController.RequestSvc(...) 中的id参数
	SubjectId() int64
	// 事务db
	TxDb() *gorm.DB
	// 设置事务db
	SetTxDb(tx *gorm.DB)
	// 事务redis
	TxRedis() redis.Pipeliner
	AddTxCommitCallback(fun ...func())
	GetTxCommitCallback() []func()
	// GenSTid 生成一个子链路id
	GenSTid(note string, params utils.M) int64
}

// 逻辑处理上下文
type actionCtx struct {
	tid              int64
	id               int64
	txDb             *gorm.DB
	txCommitCallback []func()
	txRedis          redis.Pipeliner
}

func NewActionCtx(tid int64, id int64) IActionCtx {
	return &actionCtx{tid: tid, id: id}
}

func NewActionCtxHasRedis(tid int64, id int64, tx *gorm.DB, txRedis redis.Pipeliner) IActionCtx {
	return &actionCtx{tid: tid, id: id, txDb: tx, txRedis: txRedis}
}

func (ac *actionCtx) Tid() int64 {
	return ac.tid
}

func (ac *actionCtx) SubjectId() int64 {
	return ac.id
}

func (ac *actionCtx) TxDb() *gorm.DB {
	return ac.txDb
}

func (ac *actionCtx) TxRedis() redis.Pipeliner {
	return ac.txRedis
}

func (ac *actionCtx) SetTxDb(tx *gorm.DB) {
	ac.txDb = tx
}

func (ac *actionCtx) AddTxCommitCallback(fun ...func()) {
	ac.txCommitCallback = append(ac.txCommitCallback, fun...)
}
func (ac *actionCtx) GetTxCommitCallback() []func() {
	return ac.txCommitCallback
}

func (ac *actionCtx) GenSTid(note string, params utils.M) int64 {
	if params != nil {
		params["note"] = note
	} else {
		params = utils.M{"note": note}
	}
	return app.Log().TSign(ac.tid, params)
}
