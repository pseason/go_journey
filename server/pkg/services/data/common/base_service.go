package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"context"
	"database/sql"
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

var (
	_defaultTxIsolationLevel = &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	}
)

type TxHandler struct {
	Try     func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode)
	Confirm func(ac IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError)
	Cancel  func(ac IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError)
}

type IBaseService interface {
	// IService 具体的service接口
	IService() (i IBaseService)
	// SetIService 注册具体的service接口
	SetIService(i IBaseService)
	// Actions 消息处理器；k=[messageCode]
	Actions() map[msg_dt.MsgCode]func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode)
	// TxActions 事务消息处理器
	TxActions() map[msg_dt.MsgCode]*TxHandler
	// Models
	Models() []interface{}
	// AfterInit 初始化完成后的回调
	AfterInit() error
	// GenSTid 生成一个子链路id
	// Deprecated: 请使用action_ctx.GenSTid
	GenSTid(tid int64, note string, params utils.M) int64
	// Context 本service的上下文 (可以用作操作redis接口时需要的context参数)
	Context() context.Context
	// Db
	Db() *gorm.DB
	// Tx 用于事务对象关联，若传入的db为nil，则通过Db()返回一个新的数据实例
	TxDb(db *gorm.DB) *gorm.DB
	// Redis
	Redis() *redis.Client
	// Conf
	Conf() *Config

	//  DispatchEvent
	//  @Description: 派发事件
	//  @param nodeType 发到哪个节点类型
	//  @param tid 链路id
	//  @param code 消息号
	//  @param reqBody 事件体
	DispatchEvent(nodeType misc.TNode, tid, subjectId int64, code msg_dt.MsgCode, eveBody interface{})
	// LocalChainTx
	//  @Description: 本地事务。支持多个 涉及到事务操作的接口 的嵌套调用。
	//  比如：`一级接口 -> 二级接口 -> 三级接口`  层层调用，每层都有事务操作
	//  `txAc.Tx()`是该事务链中的唯一事务会话，调用链中的所有事务都由该会话提交和回滚
	LocalChainTx(ac IActionCtx, action func(txAc IActionCtx) (commitCallback func(), iError utils.IError)) (iError utils.IError)

	// ------------↓↓↓---通用消息接口---↓↓↓------------- todo 注意: services节点内，不可跨服务调用数据接口

	// CommonActions 通用接口处理器
	CommonActions() map[msg_dt.MsgCode]func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode)
	// CommonTxActions 通用事务接口
	CommonTxActions() map[msg_dt.MsgCode]*TxHandler

	// CommonQueryById 根据id查询。用于分布式事务、本地调用
	//  @param id 数据主键
	//  @param fields 为nil则查询所有字段
	//  @return result model
	CommonQueryById(ac IActionCtx, m model_e.Model, fields []model_field_e.Enum, id int64) (result interface{}, iError utils.IError)
	// CommonQueryByIds 根据id批量查询。用于分布式事务、本地调用
	//  @param ids 数据主键
	//  @param fields 为nil则查询所有字段
	//  @return result model
	CommonQueryByIds(ac IActionCtx, m model_e.Model, fields []model_field_e.Enum, ids []int64) (result map[int64]interface{}, iError utils.IError)

	// TryCommonUpdateById 根据id更新数据。仅用于分布式事务
	//  @return origData 修改前的数据
	TryCommonUpdateById(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (iError utils.IError)
	// ConfirmCommonUpdateById
	ConfirmCommonUpdateById(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (iError utils.IError)
	// CancelCommonUpdateById
	CancelCommonUpdateById(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (iError utils.IError)

	// TryCommonUpdateNumById 根据id更新数值。仅用于分布式事务
	TryCommonUpdateNumById(ac IActionCtx, m model_e.Model, incrData map[model_field_e.Enum]int32, id int64) (origData interface{}, iError utils.IError)
	// ConfirmCommonUpdateNumById
	ConfirmCommonUpdateNumById(ac IActionCtx, m model_e.Model, incrData map[model_field_e.Enum]int32, id int64, origData interface{}) (iError utils.IError)
	// CancelCommonUpdateNumById
	CancelCommonUpdateNumById(ac IActionCtx, m model_e.Model, incrData map[model_field_e.Enum]int32, id int64) (iError utils.IError)

	// CommonUpdateMonitor
	//  @Description: model改变监听器。可以用来编写通知客户端数据变化、调用触发任务、计时器等各种逻辑
	//  @return map[model_e.Model]map[int32]func(beforeVal, afterVal interface{})
	// 			model_e.Model：监听的model，只能是 NewBaseService 时传入的model；todo 原则上一个service应该只对应一个model，这里兼容了一个service对应多个model的情况
	//			updated：更新的字段和更新后的值
	//			args：拓展参数，默认为nil，为拓展需求预留(若默认传递的数据不满足于使用，则可以重写该接口的调用代码，将自己需要的参数填至`args`中)
	CommonUpdateMonitor() map[model_e.Model]func(ac IActionCtx, updated map[model_field_e.Enum]interface{}, args ...interface{})

	// ------------↑↑↑---通用消息接口---↑↑↑-------------

	// ------------↓↓↓---本地通用事务接口---↓↓↓-------------

	// CommonUpdateById 通用更新接口，需要搭配 LocalTx 使用
	CommonUpdateById(ac IActionCtx, m model_e.Model, upData map[model_field_e.Enum]interface{}, id int64) (iError utils.IError)
	// CommonUpdateNumById 通用更新接口，需要搭配 LocalTx 使用
	CommonUpdateNumById(ac IActionCtx, m model_e.Model, incrData map[model_field_e.Enum]int32, id int64) (iError utils.IError)

	// ------------↑↑↑---本地通用事务接口---↑↑↑-------------

}

func NewBaseService(model ...interface{}) IBaseService {
	return &baseService{models: model, context: context.Background()}
}

type baseService struct {
	iService IBaseService // 具体的service接口
	models   []interface{}
	context  context.Context
}

func (bs *baseService) IService() (i IBaseService) {
	return bs.iService
}
func (bs *baseService) SetIService(i IBaseService) {
	bs.iService = i
}

func (bs *baseService) Actions() map[msg_dt.MsgCode]func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	panic("implement me")
}

func (bs *baseService) TxActions() map[msg_dt.MsgCode]*TxHandler {
	return nil
}

func (bs *baseService) Models() []interface{} {
	return bs.models
}

func (bs *baseService) AfterInit() error {
	return nil
}

func (bs *baseService) Context() context.Context {
	return bs.context
}

func (bs *baseService) Db() *gorm.DB {
	return _GormDb
}

func (bs *baseService) Redis() *redis.Client {
	return _Redis
}

func (bs *baseService) Conf() *Config {
	return _Conf
}

func (bs *baseService) TxDb(db *gorm.DB) *gorm.DB {
	if db == nil {
		return bs.Db()
	}
	return db
}

func (bs *baseService) GenSTid(tid int64, note string, params utils.M) int64 {
	if params != nil {
		params["note"] = note
	} else {
		params = utils.M{"note": note}
	}
	return app.Log().TSign(tid, params)
}

func (bs *baseService) DispatchEvent(nodeType misc.TNode, tid, subjectId int64, code msg_dt.MsgCode, eveBody interface{}) {
	// service：目前eg还没有明确这个参数的作用，统一传0
	app.Discovery().DispatchEvent(nodeType, 0, tid, subjectId, code, eveBody)
}

// 本地事务
func (bs *baseService) LocalChainTx(ac IActionCtx, action func(txAc IActionCtx) (commitCallback func(), iError utils.IError)) (iError utils.IError) {
	if ac == nil {
		ac = NewActionCtxHasRedis(0, 0, nil, nil)
	}
	var txAc IActionCtx // 整个事务链的唯一上下文
	header := false
	if ac.TxDb() == nil {
		pipeline := bs.Redis().TxPipeline()
		tx := bs.Db().Begin(_defaultTxIsolationLevel)
		header = true
		txAc = NewActionCtxHasRedis(ac.Tid(), ac.SubjectId(), tx, pipeline)
	} else {
		txAc = ac
	}
	callback, iError := action(txAc)
	if iError != nil {
		if header {
			txAc.TxDb().Rollback()
		}
		return iError
	}
	txAc.AddTxCommitCallback(callback)

	if header {
		err := txAc.TxDb().Commit().Error
		if err != nil {
			return utils.NewError(err.Error(), nil)
		}
		_, err = txAc.TxRedis().Exec(bs.context)
		if err != nil {
			return utils.NewError(err.Error(), nil)
		}
		callbacks := txAc.GetTxCommitCallback()
		if len(callbacks) > 0 {
			for _, callback := range callbacks {
				if callback != nil {
					callback()
				}
			}
		}
	}
	return
}
