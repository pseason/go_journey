package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"github.com/mitchellh/mapstructure"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

/*
@Time   : 2021-12-21 17:47
@Author : wushu
@DESC   : 通用接口
*/

func (bs *baseService) CommonActions() map[msg_dt.MsgCode]func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	iService := bs.IService()
	return map[msg_dt.MsgCode]func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		msg_dt.CdCommonQueryById: func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCommonQueryById)
			data, err := iService.CommonQueryById(ac, req.Model, req.Fields, req.ModelId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			}
			resBody = &msg_dt.ResCommonQueryById{Data: data}
			return
		},
		msg_dt.CdCommonQueryByIds: func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCommonQueryByIds)
			result, err := iService.CommonQueryByIds(ac, req.Model, req.Fields, req.ModelIds)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			}
			resBody = &msg_dt.ResCommonQueryByIds{Data: result}
			return
		},
	}
}

func (bs *baseService) CommonUpdateMonitor() map[model_e.Model]func(ac IActionCtx, updated map[model_field_e.Enum]interface{}, args ...interface{}) {
	return nil
}

func (bs *baseService) CommonTxActions() map[msg_dt.MsgCode]*TxHandler {
	iService := bs.IService()
	return map[msg_dt.MsgCode]*TxHandler{
		msg_dt.CdTryCommonQueryByIds: {
			Try: func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTryCommonQueryByIds)
				result, err := iService.CommonQueryByIds(ac, req.Model, req.Fields, req.ModelIds)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					errCode = err_dt.ErrQueryFailed
				}
				resBody = &msg_dt.ResTryCommonQueryByIds{Data: result}
				return
			},
			Confirm: func(ac IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
			Cancel: func(ac IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTryCommonUpdateById: {
			Try: func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				// todo 需要框架封装同步措施：从tx-session获取互斥锁，如果有锁且被占用则返回错误 (可以没有锁)
				tryReq := reqBody.(*msg_dt.ReqTryCommonUpdateById)
				iError := iService.TryCommonUpdateById(ac, tryReq.Model, tryReq.UpData, tryReq.ModelId)
				if iError != nil {
					app.Log().TError(ac.Tid(), iError)
					return nil, err_dt.ErrTryFailed
				}
				return nil, 0
			},
			Confirm: func(ac IActionCtx, reqBody, resBody interface{}) (iError utils.IError) {
				tryReq := reqBody.(*msg_dt.ReqTryCommonUpdateById)
				iError = iService.ConfirmCommonUpdateById(ac, tryReq.Model, tryReq.UpData, tryReq.ModelId)
				if iError != nil {
					return
				}
				// 数据更新回调
				updateMonitor := bs.iService.CommonUpdateMonitor()
				if updateMonitor != nil && updateMonitor[tryReq.Model] != nil {
					updated := make(map[model_field_e.Enum]interface{})
					for k, upVal := range tryReq.UpData {
						updated[k] = upVal
					}
					updateMonitor[tryReq.Model](ac, updated)
				}
				return
			},
			Cancel: func(ac IActionCtx, reqBody, resBody interface{}) (iError utils.IError) {
				tryReq := reqBody.(*msg_dt.ReqTryCommonUpdateById)
				iError = iService.CancelCommonUpdateById(ac, tryReq.Model, tryReq.UpData, tryReq.ModelId)
				return
			},
		},
		msg_dt.CdTryCommonUpdateNumById: {
			Try: func(ac IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				tryCommonUpdate := reqBody.(*msg_dt.ReqTryCommonUpdateNumById)
				origData, iError := iService.TryCommonUpdateNumById(ac, tryCommonUpdate.Model, tryCommonUpdate.IncrData, tryCommonUpdate.ModelId)
				if iError != nil {
					app.Log().TError(ac.Tid(), iError)
					return nil, err_dt.ErrTryFailed
				}
				return &msg_dt.ResTryCommonUpdateNumById{OrigData: origData}, 0
			},
			Confirm: func(ac IActionCtx, reqBody, resBody interface{}) (iError utils.IError) {
				tryReq := reqBody.(*msg_dt.ReqTryCommonUpdateNumById)
				tryRes := resBody.(*msg_dt.ResTryCommonUpdateNumById)
				origData := model_e.NewModelPtr(tryReq.Model)
				if err := mapstructure.Decode(tryRes.OrigData, origData); err != nil {
					return utils.NewError(err.Error(), nil)
				}

				iError = iService.ConfirmCommonUpdateNumById(ac, tryReq.Model, tryReq.IncrData, tryReq.ModelId, origData)
				if iError != nil {
					return
				}
				// 数据更新回调
				updateMonitor := bs.iService.CommonUpdateMonitor()
				if updateMonitor != nil && updateMonitor[tryReq.Model] != nil {
					updated := make(map[model_field_e.Enum]interface{})
					origModel := origData.(misc.IModel)
					for k, incr := range tryReq.IncrData {
						updated[k] = incr + origModel.GetByEnum(k).(int32)
					}
					updateMonitor[tryReq.Model](ac, updated)
				}
				return
			},
			Cancel: func(ac IActionCtx, reqBody, resBody interface{}) (iError utils.IError) {
				tryCommonUpdate := reqBody.(*msg_dt.ReqTryCommonUpdateNumById)
				iError = iService.CancelCommonUpdateNumById(ac, tryCommonUpdate.Model, tryCommonUpdate.IncrData, tryCommonUpdate.ModelId)
				return
			},
		},
	}
}
