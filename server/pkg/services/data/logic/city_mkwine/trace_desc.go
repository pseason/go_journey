package city_mkwine

const (
	//酿酒数据查询失败
	TMakeWineDbFindFail = "Failed to query knapsack data"
	//获取酿酒物品模板失败
	TMakeWineGetTemplateFail = "Failed to get the item template"
	//卡槽已有物品
	TMakeWineSlotNoEmpty = "There is something in the card slot"
	//上架已达到上限
	TMakeWineUnfinished = "Brewing unfinished"
	//不允许购买自己物品
	TMakeWineNotCancelBrewing = "Can not cancel the brewing time of the card slot"

)