package city_mkwine

import (
	"95eh.com/eg/utils"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"math/rand"
	"sort"
	"time"
)

type makeWineService struct {
	common.IBaseService
	tavernTsv    map[int32]*tsv.TavernTsv
	tavernMakTsv map[int32]*tsv.TavernMakingsTsv
}

func NewMakeWineService() IMakeWineService {
	return &makeWineService{
		IBaseService: common.NewBaseService(&model.CityMakeWine{}),
	}
}

func (mkwSvc *makeWineService) AfterInit() error {
	mkwSvc.tavernTsv = tsv.GetTsvManager(tsv.Tavern).(*tsv.TavernTsvManager).TsvMap
	mkwSvc.tavernMakTsv = tsv.GetTsvManager(tsv.TavernMakings).(*tsv.TavernMakingsTsvManager).TsvMap
	return nil
}

func (mkwSvc *makeWineService) GetMakeWineList(ac common.IActionCtx, Cid, BuildId int64) (res []*model.CityMakeWine, err utils.IError) {
	//查询数据信息
	db := mkwSvc.Db().Model(model.CityMakeWine{})
	errDb := db.Where("cid = ? AND build_id = ?", Cid, BuildId).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}

func (mkwSvc *makeWineService) MakeWineByBid(ac common.IActionCtx, Cid, Bid int64) (res *model.CityMakeWine, err utils.IError) {
	errDb := mkwSvc.Db().Model(model.CityMakeWine{}).Where("cid = ? AND id = ?", Cid, Bid).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	if res != nil {
		return
	}
	return
}

func (mkwSvc *makeWineService) MakeWineStart(ac common.IActionCtx, Cid, BuildId int64, PropId, Slot, BuildLevel int32) (res *model.CityMakeWine, err utils.IError) {
	//3011026,3011025 - 10
	traver, has := mkwSvc.tavernMakTsv[PropId]
	if !has {
		err = utils.NewError(TMakeWineGetTemplateFail, nil)
		return
	}

	db := mkwSvc.Db().Model(model.CityMakeWine{})
	//检查卡槽是否为空
	errDb := mkwSvc.Db().Model(model.CityMakeWine{}).Where("cid =? AND build_id = ? AND slot = ?", Cid, BuildId, Slot).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	if res.Id > 0 {
		err = utils.NewError(TMakeWineSlotNoEmpty, nil)
		return
	}
	//概率随机选择一个最终的物品[开始]
	RandomSlice := traver.QualityRandom
	sort.Slice(RandomSlice, func(i, j int) bool {
		return RandomSlice[i] > RandomSlice[j]
	})
	RandomMap := make(map[int]int32, 0)
	RandomSliceSum := 0
	for index, v := range RandomSlice {
		RandomMap[index] = v
		RandomSliceSum += int(v)
	}
	rand.Seed(time.Now().UnixNano())
	randIndex := 0
	for i, proSur := range RandomMap {
		randNum := rand.Intn(RandomSliceSum)
		if randNum <= int(proSur) {
			randIndex = i
			break
		} else {
			RandomSliceSum -= int(proSur)
		}
	}
	//概率随机选择一个最终的物品[结束]
	resPropId := traver.QualityID[randIndex][0]
	RandInt64 := func(min, max int64) int64 {
		if min >= max  {
			return max
		}
		return rand.Int63n(max-min) + min
	}
	duceSlice, has := mkwSvc.tavernTsv[BuildLevel]
	if !has {
		err = utils.NewError(TMakeWineGetTemplateFail, nil)
		return
	}
	num := RandInt64(int64(duceSlice.Produce[0]), int64(duceSlice.Produce[1]))
	adds := &model.CityMakeWine{
		Cid:     Cid,
		ForgeId: PropId,
		BuildId: BuildId,
		Num:     int32(num),
		EndTime: time.Now().Unix() + int64(duceSlice.Time),
		PropId:  resPropId,
		Slot:    Slot,
	}
	errDb = db.Create(adds).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	errDb = db.Delete(&model.CityMakeWine{}, adds.Id).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return adds, nil
}

func (mkwSvc *makeWineService) MakeWineCancel(ac common.IActionCtx, Cid, Bid int64) (res *model.CityMakeWine, err utils.IError) {
	db := mkwSvc.Db().Model(model.CityMakeWine{})
	errDb := db.Where("cid = ? AND id = ?", Cid, Bid).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	if res.Id <= 0 {
		err = utils.NewError(TMakeWineDbFindFail, nil)
		return
	}
	if res.EndTime > time.Now().Unix() {
		err = utils.NewError(TMakeWineNotCancelBrewing, nil)
		return
	}
	//清除记录
	errDb = db.Delete(&model.CityMakeWine{}, Bid).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}

func (mkwSvc *makeWineService) MakeWineReap(ac common.IActionCtx, Cid, Bid int64) (res *model.CityMakeWine, err utils.IError) {
	//先获取酿酒物品信息判断;删除物品;发放物品
	db := mkwSvc.Db().Model(&model.CityMakeWine{Cid: Cid})
	errDb := db.Where("id = ?", Bid).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	if res.Id <= 0 {
		err = utils.NewError(TMakeWineDbFindFail, nil)
		return
	}
	if res.EndTime >= time.Now().Unix() {
		err = utils.NewError(TMakeWineUnfinished, nil)
		return
	}
	//清除记录
	errDb = db.Delete(&model.CityMakeWine{}, Bid).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}

func (mkwSvc *makeWineService) MakeWineDestroy(ac common.IActionCtx, Cid, BuildId int64) (err utils.IError) {
	errDb := mkwSvc.Db().Model(model.CityMakeWine{}).Where("build_id = ?", BuildId).Delete(&model.CityMakeWine{}).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}

func (mkwSvc *makeWineService) GetMakeWineReapOrUpStatus(ac common.IActionCtx, Cid, Bid int64, IsUpDelete bool) (res *model.CityMakeWine, err utils.IError) {

	if IsUpDelete {
		errDb := mkwSvc.Db().Unscoped().Model(model.CityMakeWine{}).Where("id = ? AND cid = ?", Bid, Cid).Update("deleted", nil).Error
		if errDb != nil {
			err = utils.NewError(errDb.Error(), nil)
			return
		}
	}
	errDb := mkwSvc.Db().Model(model.CityMakeWine{}).Where("id = ? AND cid = ?", Bid, Cid).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}
