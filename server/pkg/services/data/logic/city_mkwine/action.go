package city_mkwine

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type IMakeWineService interface {
	common.IBaseService
	//获取酿酒的数据
	GetMakeWineList(ac common.IActionCtx, Cid, BuildId int64) (res []*model.CityMakeWine, err utils.IError)
	//开始酿酒
	MakeWineStart(ac common.IActionCtx, Cid, BuildId int64, PropId, Slot, BuildLevel int32) (res *model.CityMakeWine, err utils.IError)
	//取消酿酒
	MakeWineCancel(ac common.IActionCtx, Cid, Bid int64) (res *model.CityMakeWine, err utils.IError)
	//收获酿酒
	MakeWineReap(ac common.IActionCtx, Cid, Bid int64) (res *model.CityMakeWine, err utils.IError)
	//销毁酿酒
	MakeWineDestroy(ac common.IActionCtx, Cid, BuildId int64) (err utils.IError)
	//获取酿酒信息
	GetMakeWineReapOrUpStatus(ac common.IActionCtx, Cid, Bid int64, IsUpDelete bool) (res *model.CityMakeWine, err utils.IError)
}

func (mkwSvc *makeWineService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (interface{}, err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (i interface{}, errCode err_dt.ErrCode){
		msg_dt.CdMakeWineList: func(ac common.IActionCtx, reqBody interface{}) (i interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMakeWineList)
			res, err := mkwSvc.GetMakeWineList(ac, req.Cid, req.BuildId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMakeWineList{
				List: res,
			}, 0
		},

		msg_dt.CdMakeWineStart: func(ac common.IActionCtx, reqBody interface{}) (i interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMakeWineStart)
			res, err := mkwSvc.MakeWineStart(ac, req.Cid, req.BuildId, req.PropId, req.Slot, req.BuildLevel)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMakeWineStart{
				List: res,
			}, 0
		},

		msg_dt.CdMakeWineCancel: func(ac common.IActionCtx, reqBody interface{}) (i interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMakeWineCancel)
			_, err := mkwSvc.MakeWineCancel(ac, req.Cid, req.Bid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMakeWineCancel{
				Bid: req.Bid,
			}, 0
		},

		msg_dt.CdMakeWineReap: func(ac common.IActionCtx, reqBody interface{}) (i interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMakeWineReap)
			_, err := mkwSvc.MakeWineReap(ac, req.Cid, req.Bid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMakeWineReap{
				Bid: req.Bid,
			}, 0
		},

		msg_dt.CdMakeWineDestroy: func(ac common.IActionCtx, reqBody interface{}) (i interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMakeWineDestroy)
			err := mkwSvc.MakeWineDestroy(ac, req.Cid, req.BuildId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMakeWineDestroy{}, 0
		},

		msg_dt.CdMakeWineListById: func(ac common.IActionCtx, reqBody interface{}) (i interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMakeWineListById)
			res, err := mkwSvc.GetMakeWineReapOrUpStatus(ac, req.Cid, req.Bid, req.IsUpDelete)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMakeWineListById{
				List: res,
			}, 0
		},
	}
}
