package attr

import (
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
)

/*
@Time   : 2021-11-24 19:04
@Author : wushu
@DESC   : 角色战斗属性
*/

type characterAttrService struct {
	common.IBaseService
}

func NewCharacterAttrService() ICharacterAttrService {
	return &characterAttrService{
		IBaseService: common.NewBaseService(&model.CharacterAttr{}),
	}
}

func (cas *characterAttrService) Create(ac common.IActionCtx, cid int64) (iError utils.IError) {
	err := cas.Db().Create(&model.CharacterAttr{Cid: cid}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return
}
