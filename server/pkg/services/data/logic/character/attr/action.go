package attr

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

/*
@Time   : 2021-11-24 19:03
@Author : wushu
@DESC   :
*/

type ICharacterAttrService interface {
	common.IBaseService
	// 创建属性
	Create(ac common.IActionCtx, cid int64) (iError utils.IError)
}

func (cas *characterAttrService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode){
	}
}

func (cas *characterAttrService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdTryCharacterAttrCreate: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				cid := ac.SubjectId()
				err := cas.Create(ac, cid)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					errCode = err_dt.ErrCreateFailed
				}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				// 删除
				err := cas.Db().Unscoped().Delete(&model.CharacterAttr{}, "cid = ?", ac.SubjectId()).Error
				if err != nil {
					return utils.NewError(TDelFail, utils.M{"cid": ac.SubjectId()})
				}
				return
			},
		},
	}
}
