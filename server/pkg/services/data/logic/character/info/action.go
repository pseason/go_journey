package info

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"encoding/json"
	"fmt"
	"hm/pkg/misc"
	utils2 "hm/pkg/misc/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/model/enum/model_e/model_field_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
	"math"
)

/*
@Time   : 2021-11-10 14:10
@Author : wushu
@DESC   :
*/

// 列出本服务的所有接口。仅用来给自己看自己都写了哪些接口，没有其他任何用途
type ICharacterInfoService interface {
	common.IBaseService
	// 创建角色
	Create(ac common.IActionCtx, uid int64, nickname string, gender uint8, figure model.FigureType) (characterInfo *model.CharacterInfo, iError utils.IError)
	// 通过uid获取角色信息(登录时调用)
	CharacterInfoByUid(ac common.IActionCtx, uid int64) (characterInfo *model.CharacterInfo, iError utils.IError)
	// 根据昵称查询cid
	CidByNickname(ac common.IActionCtx, nickname string) (cid int64, iError utils.IError)
}

// 消息处理器
func (cis *characterInfoService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		// 根据uid查询角色信息
		msg_dt.CdCharacterInfo: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCharacterInfo)
			info, err := cis.CharacterInfoByUid(ac, req.Uid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			} else {
				resBody = &msg_dt.ResCharacterInfo{CharacterInfo: info}
			}
			return
		},
		// 根据昵称查询cid
		msg_dt.CdCidByNickname: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCidByNickname)
			cid, err := cis.CidByNickname(ac, req.Nickname)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			} else {
				resBody = &msg_dt.ResCidByNickname{Cid: cid}
			}
			return
		},
		// 本地事务+通用接口测试用例
		msg_dt.CdCharacterTest: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			iError := cis.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
				iError = cis.CommonUpdateById(txAc, model_e.CharacterInfo, map[character_info_e.Enum]interface{}{
					character_info_e.Nickname: "本地事务更新",
					character_info_e.Gold:     100,
				}, 1470957893951049728)
				if iError != nil {
					return
				}
				iError = cis.CommonUpdateNumById(txAc, model_e.CharacterInfo, map[character_info_e.Enum]int32{
					character_info_e.Nickname: 2, // 不是数值类型，会被忽略
					character_info_e.Gold:     -50,
				}, 1470957893951049728)
				if iError != nil {
					return
				}
				return
			})
			if iError != nil {
				app.Log().TError(ac.Tid(), iError)
				return nil, err_dt.ErrQueryFailed
			}
			return
		},
	}
}

func (cis *characterInfoService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdTryCharacterCreate: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				// 创建角色
				req := reqBody.(*msg_dt.ReqTryCharacterCreate)
				info, err := cis.Create(ac, req.Uid, req.Nickname, req.Gender, model.FigureType(req.Figure))
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					if err.Error() == TNicknameBeingUse {
						errCode = err_dt.ErrCharacterNicknameBeingUse
					} else {
						errCode = err_dt.ErrCreateFailed
					}
				} else {
					resBody = &msg_dt.ResTryCharacterCreate{CharacterInfo: info}
				}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				// 删除
				res := tryResBody.(*msg_dt.ResTryCharacterCreate)
				err := cis.Db().Unscoped().Delete(res.CharacterInfo).Error
				if err != nil {
					return utils.NewError(TDelFail, utils.M{"id": res.CharacterInfo.Id})
				}
				return
			},
		},
	}

}

// model改变监听器。可以编写通知客户端值改变、调用任务、计时器等各种逻辑
func (cis *characterInfoService) CommonUpdateMonitor() map[model_e.Model]func(ac common.IActionCtx, updated map[model_field_e.Enum]interface{}, args ...interface{}) {
	return map[model_e.Model]func(ac common.IActionCtx, updated map[character_info_e.Enum]interface{}, args ...interface{}){
		model_e.CharacterInfo: func(ac common.IActionCtx, updated map[character_info_e.Enum]interface{}, args ...interface{}) {
			var ntcIntInfos = make(map[character_info_e.Enum]int32)
			var ntcLongInfos = make(map[character_info_e.Enum]int64)
			var ntcStrInfos = make(map[character_info_e.Enum]string)
			//var bcIntInfos = make(map[int32]int32)
			//var bcLongInfos = make(map[int32]int64)
			//var bcStrInfos = make(map[int32]string)
			var hasBc bool
			var hasNtc bool
			for field, val := range updated {
				hasNtc = true
				switch v := val.(type) {
				case int8, uint8, int16, uint16, int32, uint32: // 处理msgpack反序列化`int`类型可能出现的情况。(正常修改角色信息时，反序列化出的uint32实际上不会超过int32)
					// 如果是0，也可能是int64
					if character_info_e.Type(field) == "int64" {
						if i, err := utils2.ToInt64(v); err == nil {
							ntcLongInfos[field] = i
							continue
						}
					}

					if i, err := utils2.ToInt32(v); err == nil {
						ntcIntInfos[field] = i
					} else {
						app.Log().TWarn(ac.Tid(), err.Error(), utils.M{"val": v, "type": fmt.Sprintf("%T", v)})
					}
				case int64, uint64: // 处理msgpack反序列化`int`类型可能出现的情况。(正常修改角色信息时，反序列化出的uint32实际上不会超过int32)
					if i, err := utils2.ToInt64(v); err == nil {
						ntcLongInfos[field] = i
					} else {
						app.Log().TWarn(ac.Tid(), err.Error(), utils.M{"val": v, "type": fmt.Sprintf("%T", v)})
					}
				case string:
					ntcStrInfos[field] = v
				case json.Number: // tcc-confirm/cancel阶段的req是try阶段的req通过json序列化/反序列化得到的，json序列化会导致upData中的数值类型为json.Number
					if i, err := v.Int64(); err == nil { // 只处理int，正常修改角色信息时不会存在float的值
						if i > math.MaxInt32 || (i == 0 && character_info_e.Type(field) == "int64") {
							ntcLongInfos[field] = i
						} else {
							ntcIntInfos[field] = int32(i)
						}
					}
				default:
					app.Log().TWarn(ac.Tid(), TNonCanonicalType, utils.M{character_info_e.CodeToFieldName(field): v})
					continue
				}
				//if character_info_e.IsBroadcast(field) {
				//	hasBc = true
				//	switch v := val.(type) {
				//	case int32:
				//		bcIntInfos[field] = v
				//	case int64:
				//		bcLongInfos[field] = v
				//	case string:
				//		bcStrInfos[field] = v
				//	default:
				//		app.Log().TWarn(ac.Tid(), TNonCanonicalType, utils.M{character_info_e.CodeToFieldName(field): v})
				//		continue
				//	}
				//} else if character_info_e.IsNoticeClient(field) {
				//	hasNtc = true
				//	switch v := val.(type) {
				//	case int32:
				//		ntcIntInfos[field] = v
				//	case int64:
				//		ntcLongInfos[field] = v
				//	case string:
				//		ntcStrInfos[field] = v
				//	default:
				//		app.Log().TWarn(ac.Tid(), TNonCanonicalType, utils.M{character_info_e.CodeToFieldName(field): v})
				//		continue
				//	}
				//}
			}

			// 通知客户端，某个角色的信息改变
			if hasNtc {
				cis.DispatchEvent(misc.NodeGame, ac.Tid(), ac.SubjectId(), msg_dt.CdEveCharacterInfoChange, &msg_dt.EveCharacterInfoChange{
					Cid:      ac.SubjectId(),
					IntInfo:  ntcIntInfos,
					LongInfo: ntcLongInfos,
					StrInfo:  ntcStrInfos,
				})
			}

			// 转发给space广播，某个角色的信息改变
			if hasBc {
				// todo
			}

		},
	}
}
