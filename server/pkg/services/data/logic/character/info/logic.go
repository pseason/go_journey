package info

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"fmt"
	"gorm.io/gorm"
	"hm/pkg/misc/cache_key"
	"hm/pkg/misc/tools"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/model_e"
	"strconv"
	"sync"
	"time"
	"unicode"
)

/*
@Time   : 2021-11-10 14:12
@Author : wushu
@DESC   :
*/

var (
	characterCreateMutex sync.Mutex
)

type characterInfoService struct {
	common.IBaseService
}

func NewCharacterInfoService() ICharacterInfoService {
	return &characterInfoService{
		IBaseService: common.NewBaseService(model_e.CharacterInfo),
	}
}

func (cis *characterInfoService) Create(ac common.IActionCtx, uid int64, nickname string, gender uint8, figure model.FigureType) (characterInfo *model.CharacterInfo, iError utils.IError) {
	// 检测敏感词
	if tools.IsSensitiveWords(nickname) {
		return nil, utils.NewError(TSensitiveNickname, utils.M{"nickname": nickname})
	}

	// 昵称只能是中文
	for _, s := range nickname {
		if !unicode.Is(unicode.Han, s) {
			return nil, utils.NewError(TNicknameMustBeInChinese, utils.M{"nickname": nickname})
		}
	}
	if len([]rune(nickname)) < 2 || len([]rune(nickname)) > 6 {
		return nil, utils.NewError(TNicknameLengthIllegal, utils.M{"nickname": nickname})
	}

	if gender != 0 && gender != 1 {
		return nil, utils.NewError(TGenderIllegal, utils.M{"gender": gender})
	}

	characterCreateMutex.Lock()
	defer characterCreateMutex.Unlock()

	db := cis.Db()

	characterInfo = &model.CharacterInfo{}
	// 检测是否已创建角色
	err := db.Where(&model.CharacterInfo{Uid: uid}).First(characterInfo).Error
	if characterInfo.Id != 0 {
		return nil, utils.NewError(TCharacterAlreadyExisted, utils.M{"cid": characterInfo.Id})
	}
	// 检测昵称是否被占用
	err = db.Where(&model.CharacterInfo{Nickname: nickname}).First(characterInfo).Error
	if characterInfo.Id != 0 {
		return nil, utils.NewError(TNicknameBeingUse, utils.M{"nickname": characterInfo.Nickname})
	}

	characterInfo = &model.CharacterInfo{
		Uid: uid, Nickname: nickname, Gender: gender, Figure: figure, Age: 10, LifeSkillPoints: 3, Energy: 1000,
		EnergyAutoRefreshStartTime: time.Now().Unix(),
	}
	err = db.Create(characterInfo).Error
	if err != nil {
		return nil, utils.NewError(err.Error(), nil)
	}

	return
}

func (cis *characterInfoService) enterGame(ac common.IActionCtx, characterInfo *model.CharacterInfo) (iError utils.IError) {
	app.Log().TInfo(ac.Tid(), TEnteringGame, utils.M{"uid": characterInfo.Uid, "cid": characterInfo.Id, "nickname": characterInfo.Nickname})
	// 角色信息添加到缓存
	if err := cis.InfoToCache(ac, characterInfo); err != nil {
		return err
	}
	// 添加到在线列表
	if err := cis.UpdateOnlineState(ac, characterInfo.Id, true); err != nil {
		return err
	}

	app.Log().TInfo(ac.Tid(), TEnteredGame, utils.M{"uid": characterInfo.Uid, "cid": characterInfo.Id, "nickname": characterInfo.Nickname})
	return
}

func (cis *characterInfoService) CharacterInfoByUid(ac common.IActionCtx, uid int64) (characterInfo *model.CharacterInfo, iError utils.IError) {
	if uid == 0 {
		return nil, utils.NewError(TPrimaryKeyIs0, utils.M{"uid": uid})
	}
	characterInfo = &model.CharacterInfo{}
	wrapper := &model.CharacterInfo{Uid: uid}
	err := cis.Db().Where(wrapper).First(characterInfo).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, utils.NewError(err.Error(), nil)
	}
	return
}

func (cis *characterInfoService) CidByNickname(ac common.IActionCtx, nickname string) (cid int64, iError utils.IError) {
	characterInfo := &model.CharacterInfo{}
	err := cis.Db().Where(fmt.Sprintf("%s = ?", character_info_e.CodeToFieldName(character_info_e.Nickname)), nickname).First(characterInfo).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return 0, utils.NewError(err.Error(), nil)
	}
	return characterInfo.Id, nil
}

func (cis *characterInfoService) InfoToCache(ac common.IActionCtx, characterInfo *model.CharacterInfo) (iError utils.IError) {
	redisKey := cache_key.CharacterInfo.Format(characterInfo.Id)
	set := cis.Redis().HSet(cis.Context(), redisKey,
		// 基本信息
		character_info_e.CodeToFieldName(character_info_e.Id), characterInfo.Id,
		character_info_e.CodeToFieldName(character_info_e.Created), characterInfo.Created,
		character_info_e.CodeToFieldName(character_info_e.Uid), characterInfo.Uid,
		character_info_e.CodeToFieldName(character_info_e.Nickname), characterInfo.Nickname,
		character_info_e.CodeToFieldName(character_info_e.Gender), characterInfo.Gender,
		character_info_e.CodeToFieldName(character_info_e.Figure), int32(characterInfo.Figure),
	)
	if set.Err() != nil {
		return utils.NewError(set.Err().Error(), nil)
	}
	return
}

func (cis *characterInfoService) UpdateOnlineState(ac common.IActionCtx, cid int64, flag bool) (iError utils.IError) {
	if flag == true { // 添加
		redisKey := string(cache_key.CharacterOnlineList)
		err := cis.Redis().HSet(cis.Context(), redisKey, cid, time.Now().Unix()).Err()
		if err != nil {
			return utils.NewError(err.Error(), nil)
		}
	} else { // 移除
		redisKey := cache_key.CharacterOnlineList
		err := cis.Redis().HDel(cis.Context(), string(redisKey), strconv.FormatInt(cid, 10)).Err()
		if err != nil {
			return utils.NewError(err.Error(), nil)
		}
	}
	return
}
