package info

/*
@Time   : 2021-11-05 10:47
@Author : wushu
@DESC   : 这里定义链路日志的描述信息
	这些字符串常量用于创建错误、打印日志时作为描述参数传入
	参见开发文档"链路日志和error处理机制"
*/

// 错误描述 (创建自定义错误时使用)
const (
	TDelFail                 = "delete fail"                       // 删除失败
	TPrimaryKeyIs0           = "primary key is 0"                  // 主键为0
	TSensitiveNickname       = "nickname contains sensitive words" // 昵称包含敏感字
	TNicknameMustBeInChinese = "nickname must be in chinese"       // 昵称必须是中文
	TNicknameLengthIllegal   = "nickname length illegal"           // 昵称长度不符
	TGenderIllegal           = "gender illegal"                    // 性别无效
	TCharacterAlreadyExisted = "character already existed"         // 已经创建了角色
	TNicknameBeingUse        = "nickname being use"                // 昵称被占用
)

// 非错误描述
const (
	TEnteringGame     = "character entering the game" // 角色开始进入游戏
	TEnteredGame      = "character entered the game"  // 角色进入了游戏
	TNonCanonicalType = "non-canonical type"          // 不支持的类型
)
