package mail

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"encoding/json"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/mail_e"
	"hm/pkg/services/data/msg_dt"
	"time"
)

type mailService struct {
	common.IBaseService
	systemMailTemplates *tsv.SystemMailTsvManager
	imTimer             intfc.IMTimer
}

func NewMailService() *mailService {
	return &mailService{IBaseService: common.NewBaseService(&model.Mail{}, &model.SystemMail{})}
}

func (m *mailService) AfterInit() error {
	m.imTimer = app.Timer()
	m.imTimer.Start()
	m.systemMailTemplates = tsv.GetTsvManager(tsv.SystemMail).(*tsv.SystemMailTsvManager)
	m.InitSystemMail()
	//m.GetPageMailByCidList(0,1486540545907515392,0,10,true)
	return nil
}

// GetMails 通过玩家id查询所有邮件 query character all mail by cid
func (m *mailService) GetMails(cid int64, isDesc bool) (res []*model.Mail, iErr utils.IError) {
	db := m.Db().Model(model.Mail{})

	order := "created"
	if isDesc {
		order = "created desc"
	}
	err := db.Where("cid = ?", cid).
		Order(order).Find(&res).Error
	if err != nil {
		return res, utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return
}

// GetMailBySystemId 通过系统邮件id查询玩家是否发送过该系统邮件 query character mail whether to send systemMail by systemMailId
func (m *mailService) GetMailBySystemId(cid int64, systemId int64) (res *model.Mail, iErr utils.IError) {
	db := m.Db().Model(model.Mail{})
	var t model.Mail
	err := db.Unscoped().Where(&model.Mail{
		Cid:          cid,
		SystemMailId: systemId,
	}).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"cid": cid})
		}
	}
	if t.Id != 0 {
		res = &t
	}
	return
}

// GetMailCount 通过玩家id查询未读邮件的数量 query character unread mail number by cid
func (m *mailService) GetMailCount(cid int64, isRead bool) (count int64, iErr utils.IError) {
	db := m.Db().Model(model.Mail{})
	db.Where("cid = ?", cid)
	if isRead {
		db.Where("is_mail_read = ?", false)
	}
	err := db.Count(&count).Error
	if err != nil {
		return 0, utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return
}

// GetPageMailByCidList 通过玩家id、分页书、查询个数分页查询邮件 Pagination query character mail by page and size
func (m *mailService) GetPageMailByCidList(tid, cid int64, page, size int32, isLoadSystemMail bool) (res []*model.Mail, total int32, iErr utils.IError) {
	if isLoadSystemMail {
		systemMailList, err := m.GetAllSystemMailList()
		if err != nil {
			return nil, 0, err
		}
		for _, systemMail := range systemMailList {
			if time.Now().Unix()-systemMail.Created > int64(systemMail.EnclosuresOverdueTime)*24*60*60 {
				err = m.DelSystemMail(systemMail.Id)
				if err != nil {
					return nil, 0, err
				}
				continue
			}
			checkMail, err := m.GetMailBySystemId(cid, systemMail.Id)
			if err != nil {
				return nil, 0, err
			}

			if checkMail == nil {
				propMap := make(map[int64]int32, 0)
				if systemMail.SystemMailEnclosures != "0" && systemMail.SystemMailEnclosures != "" {
					json.Unmarshal([]byte(systemMail.SystemMailEnclosures), &propMap)
				}

				mail, err := model.NewSystemMail(cid, systemMail.SystemMailTitle, systemMail.SystemMailContent, propMap)
				if err != nil {
					return nil, 0, utils.NewError(err.Error(), utils.M{"system_mail": mail})
				}
				mail.SystemMailId = systemMail.Id
				mail.MailTime = int32(systemMail.EnclosuresOverdueTime)
				_, iError := m.SendMailByCid(tid, false, mail)
				if iError != nil {
					app.Log().TError(tid, iError)
				}

			}
		}
	}
	count, iErr := m.GetMailCount(cid, false)
	if iErr != nil {
		return nil, 0, iErr
	}

	total = int32(count)
	if size == 0 || size > 20 {
		size = 10
	}
	db := m.Db().Model(model.Mail{})
	err := db.Where("cid = ?", cid).Order("created desc").Offset(int(page * size)).Limit(int(size)).Find(&res).Error
	if err != nil {
		return res, 0, utils.NewError(err.Error(), nil)
	}
	return
}

// GetMailById 通过邮件id查询邮件 query character mail By mailId
func (m *mailService) GetMailById(mailId int64) (mail *model.Mail, iErr utils.IError) {
	db := m.Db().Model(model.Mail{})
	var t model.Mail
	err := db.Where("id = ?", mailId).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"mailId": mailId})
		}
	}
	if t.Id > 0 {
		return &t, nil
	}
	return
}

//UpMailByMailIdAndUpMap 修改邮件 update mail by mail id and upDate data
func (m *mailService) UpMailByMailIdAndUpMap(mailId int64, upData map[string]interface{}) utils.IError {
	db := m.Db().Model(&model.Mail{})
	err := db.Where("id = ?", mailId).Updates(upData).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"mailId": mailId})
	}
	return nil
}

//UnscopedDelMailByMailId 通过邮件id彻底删除邮件 unscoped del mail by mail id
func (m *mailService) UnscopedDelMailByMailId(mailId ...int64) utils.IError {
	db := m.Db().Model(&model.Mail{})
	err := db.Unscoped().Where("id in ?", mailId).Delete(&model.Mail{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"mail id ": mailId})
	}
	return nil
}

//ReductionDelMail 还原软删除邮件 reduction delete mail by all mail id
func (m *mailService) ReductionDelMail(tid int64, mailIds []int64) (err error) {
	db := m.Db().Model(&model.Mail{})
	err = db.Unscoped().Where("id in ?", mailIds).Update("deleted", nil).Error
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), utils.M{"reduction mail id list ": mailIds}))
		return err
	}
	return nil
}

// SendMailByCid  实时发送邮件 real time send character mail
func (m *mailService) SendMailByCid(tid int64, isEveNotify bool, mails ...*model.Mail) (sendMailIdList []int64, iErr utils.IError) {
	if len(mails) > 0 {
		mailCount, iErr := m.GetMailCount(mails[0].Cid, false)
		if iErr != nil {
			return sendMailIdList, iErr
		}
		if mailCount >= 100 {
			list, _, iErr := m.GetPageMailByCidList(tid, mails[0].Cid, 0, int32(len(mails)), false)
			if iErr != nil {
				return sendMailIdList, iErr
			}
			delMalls := make([]int64, 0)
			for _, delMail := range list {
				delMalls = append(delMalls, delMail.Id)
			}
			if len(delMalls) > 0 {
				db := m.Db().Model(&model.Mail{})
				err := db.Where("id in ?", delMalls).Delete(&model.Mail{}).Error
				if err != nil {
					return sendMailIdList, utils.NewError(err.Error(), utils.M{"err": err})
				}
			}

		}
		db := m.Db().Model(&model.Mail{})
		err := db.Create(mails).Error
		if err != nil {
			return sendMailIdList, utils.NewError(err.Error(), utils.M{"err": err})
		}
		cidList := make([]int64, 0)
		sendMailIdList = make([]int64, 0)
		for _, mail := range mails {
			if mail.Cid > 0 {
				isNotify := true
				for _, cid := range cidList {
					if mail.Cid == cid {
						isNotify = false
						break
					}
				}
				if isNotify {
					cidList = append(cidList, mail.Cid)
				}
				sendMailIdList = append(sendMailIdList, mail.Id)
			}
		}
		if isEveNotify {
			m.DispatchEvent(misc.NodeGame, tid, 0, msg_dt.CdEveMailNew, &msg_dt.EveMailNew{
				CidList:  cidList,
				MailType: mail_e.Personal,
			})
		}
	}
	return sendMailIdList, nil
}

// DelMallById 通过邮件id删除邮件 delete player mail by mailId
func (m *mailService) DelMallById(mailId int64) (iErr utils.IError) {
	db := m.Db().Model(model.Mail{})
	err := db.Where("id = ?", mailId).Delete(&model.Mail{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"mailId": mailId})
	}
	return nil
}

// DelAllMallById 通过邮件id删除邮件 delete player All mail by mailId
func (m *mailService) DelAllMallById(mailId ...int64) (iErr utils.IError) {
	db := m.Db().Model(model.Mail{})
	err := db.Where("id in ?", mailId).Delete(&model.Mail{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"mailId": mailId})
	}
	return nil
}

// ReadMail 通过邮件id读取玩家邮件 read player mail by mailId
func (m *mailService) ReadMail(mailId int64) (iErr utils.IError) {
	mail, iErr := m.GetMailById(mailId)
	if iErr != nil {
		return
	}
	if mail == nil {
		iErr = utils.NewError(TErrMailNotExits, utils.M{"mailId": mailId})
		return
	}
	return nil
}

// ClearMailAll 通过玩家id一键清除所有已读已领邮件 one key clear have read and received mail by cid
func (m *mailService) ClearMailAll(cid int64) (list []int64, iErr utils.IError) {
	mailList, iErr := m.GetMails(cid, true)
	if iErr != nil {
		return nil, iErr
	}
	if len(mailList) > 0 {
		for _, v := range mailList {
			if v.IsMailRead && v.MailEnclosures == "0" || v.MailEnclosures == "" {
				list = append(list, v.Id)
			}
		}
	}

	return
}

// ReceiveEnclosureAll 一键领取邮件 one key received and have read by cid
func (m *mailService) ReceiveEnclosureAll(mailId int64) (id int64, reductionMap map[string]interface{}, iErr utils.IError) {
	if mailId > 0 {
		mail, iErr := m.GetMailById(mailId)
		if iErr != nil {
			return 0, reductionMap, iErr
		}
		if mail == nil {
			return 0, reductionMap, utils.NewError(TErrMailNotExits, utils.M{"mailId": mailId})
		}
		reductionMap = make(map[string]interface{})
		if !mail.IsMailRead {
			reductionMap["is_mail_read"] = false
		}
		if mail.MailEnclosures != "0" && mail.MailEnclosures != "" {
			reductionMap["mail_enclosures"] = ""
		}
		return mailId, reductionMap, nil
	}
	return
}
