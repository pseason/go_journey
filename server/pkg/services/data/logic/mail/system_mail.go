package mail

import (
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"errors"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/mail_e"
	"hm/pkg/services/data/msg_dt"
	"time"
)

// InitSystemMail 初始化系统邮件 init systemMail
func (m *mailService) InitSystemMail() {
	addSystemMails := make([]*model.SystemMail, 0)
	for _, mailTsv := range m.systemMailTemplates.TsvSlice {
		if mailTsv.SystemMailState == 1 {
			continue
		}
		mail, errCode := m.GetSystemMailById(0, int64(mailTsv.Id))
		if errCode > 0 {
			log.Warn("query systemMail fail by systemMailId", utils.M{
				"errCode:":      errCode,
				"systemMailId：": mailTsv.Id,
			})
			continue
		}

		if mail == nil {
			if mailTsv.StartTime == "0" {
				if mailTsv.MaxAmoust > 0 && mailTsv.CD > 0 {
					startTime := int64(mailTsv.CD) * int64(time.Minute)
					m.imTimer.After(startTime, func() {
						m.continuedSendMail(mailTsv.MaxAmoust, mailTsv)
					})
				}
			} else {
				systemMail := &model.SystemMail{
					SystemMailTitle:                 mailTsv.SystemMailTitle,
					SystemMailContent:               mailTsv.SystemMailContent,
					SystemMailEnclosures:            mailTsv.SystemMailEnclosures,
					EnclosuresOverdueTime:           int(mailTsv.EnclosuresOverdueTime),
					SystemMailReincarnationDelState: int(mailTsv.SystemMailReincarnationDelState),
				}
				systemMail.Id = int64(mailTsv.Id)
				if mailTsv.StartTime != "0" {
					systemMail.Created = TimeConversion(mailTsv.StartTime)
				}
				addSystemMails = append(addSystemMails, systemMail)
			}
		}

	}
	if len(addSystemMails) > 0 {
		errCode := m.AddSystemMail(0, addSystemMails...)
		if errCode > 0 {
			log.Warn("init systemMail fail", utils.M{
				"errCode:":     errCode,
				"addMailList：": addSystemMails,
			})
		}
	}
}

//  continuedSendMail 间隔发送系统邮件 continued send mail
func (m *mailService) continuedSendMail(sendCount int32, mailTsv *tsv.SystemMailTsv) {
	if sendCount > 0 {
		title := mailTsv.SystemMailTitle
		number := mailTsv.MaxAmoust - sendCount + 1
		if number > 1 {
			title += fmt.Sprintf("%d", number)
		}
		//查看系统邮件是否存在
		isSystemMail, errCode := m.GetSystemMailByName(common.NewActionCtx(0, 0), title)
		if errCode > 0 {
			log.Warn("query systemMail by systemId fail", utils.M{
				"errCode: ":     errCode,
				"systemMailId：": mailTsv.Id,
			})
		}
		if isSystemMail == nil {
			systemMail := &model.SystemMail{
				SystemMailTitle:                 title,
				SystemMailContent:               mailTsv.SystemMailContent,
				SystemMailEnclosures:            mailTsv.SystemMailEnclosures,
				EnclosuresOverdueTime:           int(mailTsv.EnclosuresOverdueTime),
				SystemMailReincarnationDelState: int(mailTsv.SystemMailReincarnationDelState),
			}
			errCode = m.AddSystemMail(0, systemMail)
			if errCode > 0 {
				log.Warn("system timing mail send fail", utils.M{"errCode: ": errCode})
			}
			sTid := m.GenSTid(0, "system timing send mail", nil)
			m.DispatchEvent(misc.NodeGame, sTid, 0, msg_dt.CdEveMailNew, msg_dt.EveMailNew{MailType: mail_e.SystemType})
		}
		if sendCount-1 > 0 {
			startTime := int64(mailTsv.CD) * int64(time.Minute)

			m.imTimer.After(startTime, func() {
				m.continuedSendMail(sendCount-1, mailTsv)
			})
		}
	}

}

// GetSystemMailById 通过系统邮件id查询系统邮件 query systemMail by systemMailId
func (m *mailService) GetSystemMailById(tid, systemMailId int64) (systemMail *model.SystemMail, errCode utils.TErrCode) {
	db := m.Db().Model(model.SystemMail{})
	var t model.SystemMail
	err := db.Where("id = ? ", systemMailId).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, 1
		}
	}
	if t.Id > 0 {
		systemMail = &t
	}
	return
}

// GetSystemMailByName 通过邮件名称查询系统邮件 query systemMail by mailTitle
func (m *mailService) GetSystemMailByName(ac common.IActionCtx, mailTitle string) (res *model.SystemMail, errCode utils.TErrCode) {
	db := m.Db().Model(model.SystemMail{})
	var t model.SystemMail
	err := db.Where(&model.SystemMail{
		SystemMailTitle: mailTitle,
	}).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, 1
		}
	}
	if t.Id != 0 {
		res = &t
	}
	return
}

// GetAllSystemMailList 查询所有系统邮件 query all systemMail
func (m *mailService) GetAllSystemMailList() (res []*model.SystemMail, iErr utils.IError) {
	db := m.Db().Model(model.SystemMail{})
	err := db.Find(&res).Error
	if err != nil {
		return nil, utils.NewError(err.Error(), nil)
	}
	return
}

// AddSystemMail 添加系统邮件 add systemMail
func (m *mailService) AddSystemMail(tid int64, mails ...*model.SystemMail) (errCode utils.TErrCode) {
	db := m.Db().Model(model.SystemMail{})
	err := db.Create(mails).Error
	if err != nil {
		return 1
	}
	return
}

// DelSystemMail 删除系统邮件 delete systemMail by systemMailId
func (m *mailService) DelSystemMail(systemId int64) (iErr utils.IError) {
	db := m.Db().Model(model.SystemMail{})
	err := db.Where("id = ?", systemId).Delete(&model.SystemMail{}).Error
	if err != nil {
		iErr = utils.NewError(err.Error(), utils.M{"del systemMail fail,systemMailId ": systemId})
	}
	return
}

// SendSystemMail 发送系统邮件 send systemMail
func (m *mailService) SendSystemMail(title, context string, propMap map[int32]int32) {
	se, _ := jsoniter.Marshal(propMap)
	systemMail := &model.SystemMail{
		SystemMailTitle:                 title,
		SystemMailContent:               context,
		SystemMailEnclosures:            string(se),
		EnclosuresOverdueTime:           30,
		SystemMailReincarnationDelState: 0,
	}
	errCode := m.AddSystemMail(0, systemMail)
	if errCode > 0 {
		log.Warn("system timing mail send fail", utils.M{"errCode:": errCode})
	}
	sTid := m.GenSTid(0, "send system mail", nil)
	m.DispatchEvent(misc.NodeGame, sTid, 0, msg_dt.CdEveMailNew, msg_dt.EveMailNew{MailType: mail_e.SystemType})
}

// TimeConversion 获取当然0点时间戳
func TimeConversion(seasonTimeStr string) int64 {
	t, _ := time.Parse("2006-01-02", seasonTimeStr)
	//Unix返回早八点的时间戳，减去8个小时
	return t.UTC().Unix() - 8*3600
}
