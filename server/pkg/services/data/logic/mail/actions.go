package mail

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type IMailService interface {
	common.IBaseService
	GetPageMailByCidList(ac common.IActionCtx, page, size int32) (res []*model.Mail, total int32, errCode err_dt.ErrCode)
	MailOperation(ac common.IActionCtx, mailId int64, operationId int32) (mails []int64, errCode err_dt.ErrCode)
	GetMailCount(ac common.IActionCtx, isRead bool) (count int64, errCode err_dt.ErrCode)
}

func (m *mailService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode){
		msg_dt.CdMailPage: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqMailPage)
			mailList, total, err := m.GetPageMailByCidList(ac.Tid(), req.Cid, req.Page, req.Size, true)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return msg_dt.ResMailPage{
				MailInfoList: mailList,
				Total:        total,
				Current:      req.Page,
			}, 0
		},
		msg_dt.CdMailCount: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqMailCount)
			count, err := m.GetMailCount(req.Cid, true)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return msg_dt.ResMailCount{UnReadCount: int32(count)}, 0
		},
		msg_dt.CdMails: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			mails, err := m.GetMails(ac.SubjectId(), true)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return msg_dt.ResMails{MailInfoList: mails}, 0
		},
		msg_dt.CdMailById: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqMailById)
			mail, err := m.GetMailById(req.MailId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMailById{Mail: mail}, 0
		},
	}
}

func (m *mailService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdMailDel: {
			Try: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
				req := body.(*msg_dt.ReqMailDel)
				delMailId := make([]int64, 0)
				var err utils.IError
				if req.MailId > 0 {
					mail, err := m.GetMailById(req.MailId)
					if err != nil {
						app.Log().TError(ac.Tid(), err)
						return nil, err_dt.ErrUpdateFailed
					}
					if mail != nil {
						if mail.MailEnclosures != "" && mail.MailEnclosures != "0"{
							return nil, err_dt.ErrUpdateFailed
						}
						delMailId = append(delMailId, mail.Id)
					}
				} else {
					delMailId, err = m.ClearMailAll(req.Cid)
					if err != nil {
						app.Log().TError(ac.Tid(), err)
						return nil, err_dt.ErrUpdateFailed
					}
				}
				return msg_dt.ResMailDel{MailId: delMailId, DeleteIdList: delMailId}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := tryResBody.(*msg_dt.ResMailDel)
				iError = m.DelAllMallById(req.DeleteIdList...)
				if iError != nil {
					app.Log().TError(ac.Tid(), iError)
					return
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdMailReceive: {
			Try: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
				receiveMailId, updateMap, err := m.ReceiveEnclosureAll(ac.SubjectId())
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return nil, err_dt.ErrUpdateFailed
				}
				return msg_dt.ResMailReceive{MailId: receiveMailId, UpdateMap: updateMap}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResMailReceive)
				err := m.UpMailByMailIdAndUpMap(res.MailId, res.UpdateMap)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return err
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdMailRead: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				err := m.ReadMail(ac.SubjectId())
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					resErrCode = err_dt.ErrUpdateFailed
				}
				return &msg_dt.ResMailRead{MailId: ac.SubjectId()}, resErrCode
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				err := m.UpMailByMailIdAndUpMap(ac.SubjectId(), map[string]interface{}{"is_mail_read": true})
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return err
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				err := m.UpMailByMailIdAndUpMap(ac.SubjectId(), map[string]interface{}{"is_mail_read": false})
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return err
				}
				return nil
			},
		},
		msg_dt.CdMailSend: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				return nil, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqMailSend)
				_, err := m.SendMailByCid(ac.Tid(), true, req.Mails...)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
	}
}
