package testTx

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type testTxService struct {
	common.IBaseService
}

func NewTxService() ITestTxService {
	return &testTxService{
		IBaseService: common.NewBaseService(),
	}
}

func (t *testTxService) TryOrder(ac common.IActionCtx, propId uint32, count uint16) (payType uint8, payPrice uint32, errCode err_dt.ErrCode) {
	app.Log().Info("try order", utils.M{
		"tid":    ac.Tid(),
		"propId": propId,
		"count":  count,
	})
	if propId == 2 {
		errCode = 1
		return
	}
	payType = 1
	payPrice = uint32(count) * 50
	return
}

func (t *testTxService) ConfirmOrder(ac common.IActionCtx, propId uint32, count uint16) utils.IError {
	app.Log().Info("confirm order", utils.M{
		"tid":    ac.Tid(),
		"propId": propId,
		"count":  count,
	})
	return nil
}

func (t *testTxService) CancelOrder(ac common.IActionCtx, propId uint32, count uint16) utils.IError {
	app.Log().Info("cancel order", utils.M{
		"tid":    ac.Tid(),
		"propId": propId,
		"count":  count,
	})
	return nil
}

func (t *testTxService) TryHoldingAsset(ac common.IActionCtx, payType uint8, payPrice uint32) (errCode err_dt.ErrCode) {
	app.Log().Info("try holding asset", utils.M{
		"tid":       ac.Tid(),
		"pay type":  payType,
		"pay price": payPrice,
	})
	if payPrice > 500 {
		errCode = 1
		return
	}
	return
}

func (t *testTxService) ConfirmHoldingAsset(ac common.IActionCtx, payType uint8, payPrice uint32) utils.IError {
	app.Log().Info("confirm holding asset", utils.M{
		"tid":       ac.Tid(),
		"pay type":  payType,
		"pay price": payPrice,
	})
	return nil
}

func (t *testTxService) CancelHoldingAsset(ac common.IActionCtx, payType uint8, payPrice uint32) utils.IError {
	app.Log().Info("cancel holding asset", utils.M{
		"tid":       ac.Tid(),
		"pay type":  payType,
		"pay price": payPrice,
	})
	return nil
}
