package testTx

import (
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type ITestTxService interface {
	common.IBaseService
	TryOrder(ac common.IActionCtx, propId uint32, count uint16) (payType uint8, payPrice uint32, errCode err_dt.ErrCode)
	ConfirmOrder(ac common.IActionCtx, propId uint32, count uint16) utils.IError
	CancelOrder(ac common.IActionCtx, propId uint32, count uint16) utils.IError
	TryHoldingAsset(ac common.IActionCtx, payType uint8, payPrice uint32) (ec utils.TErrCode)
	ConfirmHoldingAsset(ac common.IActionCtx, payType uint8, payPrice uint32) utils.IError
	CancelHoldingAsset(ac common.IActionCtx, payType uint8, payPrice uint32) utils.IError
}

func (t *testTxService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return nil
}

func (t *testTxService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdTestTryOrder: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTestTryOrder)
				payType, payPrice, ec := t.TryOrder(ac, req.PropId, req.Count)
				if ec > 0 {
					errCode = err_dt.ErrQueryFailed
					return
				}
				resBody = &msg_dt.ResTestTryOrder{
					PayType:  payType,
					PayPrice: payPrice,
				}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				log.Debug("test try order confirm", utils.M{
					"req": reqBody,
					"res": resBody,
				})
				req := reqBody.(*msg_dt.ReqTestTryOrder)
				return t.ConfirmOrder(ac, req.PropId, req.Count)
			},
			Cancel: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				log.Debug("test try order cancel", utils.M{
					"req": reqBody,
					"res": resBody,
				})
				req := reqBody.(*msg_dt.ReqTestTryOrder)
				return t.CancelOrder(ac, req.PropId, req.Count)
			},
		},
		msg_dt.CdTestTryHoldingAsset: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTestTryHoldingAsset)
				ec := t.TryHoldingAsset(ac, req.PayType, req.PayPrice)
				if ec > 0 {
					errCode = err_dt.ErrQueryFailed
				}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				log.Debug("test try holding confirm", utils.M{
					"req": reqBody,
					"res": resBody,
				})
				req := reqBody.(*msg_dt.ReqTestTryHoldingAsset)
				return t.ConfirmHoldingAsset(ac, req.PayType, req.PayPrice)
			},
			Cancel: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				log.Debug("test try holding cancel", utils.M{
					"req": reqBody,
					"res": resBody,
				})
				req := reqBody.(*msg_dt.ReqTestTryHoldingAsset)
				return t.CancelHoldingAsset(ac, req.PayType, req.PayPrice)
			},
		},
	}
}
