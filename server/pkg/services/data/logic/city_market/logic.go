package city_market

import (
	"95eh.com/eg/utils"
	"github.com/golang-module/carbon"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"math"
	"strings"
	"time"
)

type marketService struct {
	common.IBaseService
	pTsv map[int32]*tsv.PropTsv
}

func NewMarketService() IMarketService {
	r := &marketService{
		IBaseService: common.NewBaseService(&model.CityMarket{}),
	}
	return r
}

func (mtSvc *marketService) AfterInit() error {
	mtSvc.pTsv = tsv.GetTsvManager(tsv.Prop).(*tsv.PropTsvManager).TsvMap
	return nil
}

func (mtSvc *marketService) GetTitleSearch(ac common.IActionCtx, Cid, BuildId int64, Title string, Type map[int32]int32, Page, Size int64) (res model.ResPageList, err utils.IError) {
	if Page <= 0 || Size <= 0 {
		err = utils.NewError(TMarketParamFail, nil)
		return
	}
	_where := &model.CityMarket{BuildId: BuildId, Status: model.MarketStatus_UP}

	db := mtSvc.Db().Model(model.CityMarket{})
	if len(Type) > 0 {
		for mainType, subType := range Type {
			if mainType > 0 {
				_where.MainType = mainType
			}
			if subType > 0 {
				_where.SubType = subType
			}
		}
		db.Where(_where)
	} else if len(Title) > 0 {
		db.Where(_where)
		Title = strings.TrimSpace(Title)
		if len(Title) > 0 {
			db.Where("`name` LIKE ?", "%"+Title+"%")
		}
	}

	var Count int64 = 0
	if Page > 0 && Size > 0 {
		errDb := db.Count(&Count).Error
		if errDb != nil {
			err = utils.NewError(errDb.Error(), nil)
			return
		}
		res.Page = Page
		res.TotalPage = int64(math.Ceil(float64(Count) / float64(Size)))

		errDb = db.Limit(int(Size)).Offset(int((Page - 1) * Size)).Count(&res.Rows).Error
		if errDb != nil {
			err = utils.NewError(errDb.Error(), nil)
			return
		}

		errDb = db.Order("prop_id desc").Order("price").Find(&res.List).Error
		if errDb != nil {
			err = utils.NewError(errDb.Error(), nil)
			return
		}
	}
	return
}

func (mtSvc *marketService) GetSellingRecords(ac common.IActionCtx, Cid, BuildId int64, Status model.MarketStatus, Page, Size int64) (res model.ResPageList, err utils.IError) {
	_where := &model.CityMarket{BuildId: BuildId, Cid: Cid, Status: model.MarketStatus_UP}
	db := mtSvc.Db().Model(model.CityMarket{})
	var Count int64 = 0
	if Page <= 0 {
		Page = 1
	}
	if Size <= 0 {
		Size = 20
	}
	errDb := db.Count(&Count).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	res.Page = Page
	res.TotalPage = int64(math.Ceil(float64(Count) / float64(Size)))

	errDb = db.Where(_where).Limit(int(Size)).Offset(int((Page - 1) * Size)).Count(&res.Rows).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}

	errDb = db.Find(&res.List).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}

	return
}

func (mtSvc *marketService) PutInto(ac common.IActionCtx, Cid, BuildId int64, PtParams []*model.MarketPutParams) (res []*model.CityMarket, err utils.IError) {
	if len(PtParams) <= 0 {
		err = utils.NewError(TMarketParamFail, nil)
		return
	}
	for _, params := range PtParams {
		expiration := carbon.Now().AddDays(int(params.Day)).Time.Unix()
		marketProp := &model.CityMarket{
			Cid:        Cid,
			BuildId:    BuildId,
			PropId:     params.PropId,
			MainType:   mtSvc.pTsv[params.PropId].MarketMainType,
			SubType:    mtSvc.pTsv[params.PropId].MarketType,
			Number:     params.Num,
			Price:      params.Price,
			Day:        params.Day,
			Expiration: expiration,
			Status:     model.MarketStatus_UP,
			Name:       mtSvc.pTsv[params.PropId].Name,
		}
		res = append(res, marketProp)
	}
	if len(res) > 0 {
		errDb := mtSvc.Db().Model(model.CityMarket{}).Create(res).Error
		if errDb != nil {
			err = utils.NewError(errDb.Error(), nil)
			return
		}
	}
	return
}

func (mtSvc *marketService) Remove(ac common.IActionCtx, Cid int64, Bid int64) (res *model.CityMarket, err utils.IError) {
	if Bid <= 0 {
		return
	}
	ds, err := mtSvc.GetMarkPropById(ac, Cid, Bid)
	if ds == nil || err != nil {
		return
	}
	db := mtSvc.Db().Model(model.CityMarket{})
	ds.Status = model.MarketStatus_DODOWN
	errDb := db.Where("id = ?", Bid).Save(&ds).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}

func (mtSvc *marketService) ConfirmBuy(ac common.IActionCtx, Bid int64,ds *model.CityMarket) ( err utils.IError) {
	errDb := mtSvc.Db().Model(model.CityMarket{}).Where("id = ?",ds.Id).Save(&ds).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	return
}

func (mtSvc *marketService) TryBuy(ac common.IActionCtx, Cid int64, Bid int64) (ds *model.CityMarket, err utils.IError) {
	if Bid <= 0 {
		err = utils.NewError(TMarketParamFail, nil)
		return
	}
	ds, err = mtSvc.GetMarkPropById(ac, Cid, Bid)
	if ds == nil || err != nil {
		return
	}
	if ds.Status != model.MarketStatus_UP {
		err = utils.NewError(TMarketNotAllowedSell, nil)
		return
	}
	if ds.Cid == Cid {
		err = utils.NewError(TMarketNotAllowedBuySelf, nil)
		return
	}
	if ds.Expiration <= time.Now().Unix() {
		err = utils.NewError(TMarketExpired, nil)
		return
	}

	ds.Status = model.MarketStatus_SELLED
	ds.BuyCid = Cid
	return
}

func (mtSvc *marketService) ConfirmAutoCancel(ac common.IActionCtx, upIds []int64) (err utils.IError) {
	if len(upIds)>0 {
		errDb := mtSvc.Db().Model(model.CityMarket{}).Where("id in ?",upIds).Update("status",model.MarketStatus_EXDOWN).Error
		if errDb != nil {
			err = utils.NewError(errDb.Error(), nil)
			return
		}
	}
	return
}

func (mtSvc *marketService) TryAutoCancel(ac common.IActionCtx) (ds []*model.MarketToMail, upIds []int64, err utils.IError) {
	res := []*model.CityMarket{}
	errDb := mtSvc.Db().Model(model.CityMarket{}).Where("status = 1 AND expiration < ?",time.Now().Unix()).Limit(300).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	if len(res) >0 {
		for _,item := range res{
			record := &model.MarketToMail{
				Id:item.Id,
				Cid:item.Cid,
				PropId:item.PropId,
				Num:item.Number,
				Name:item.Name,
				Price:item.Price,
			}
			upIds = append(upIds,item.Id)
			ds = append(ds,record)
		}
	}
	return
}

func (mtSvc *marketService) GetMarkCount(ac common.IActionCtx, Cid int64, BuildId int64) (num int64, err utils.IError) {
	errDb := mtSvc.Db().Model(model.CityMarket{}).Where("cid = ? AND build_id = ? AND status = ?", Cid, BuildId, model.MarketStatus_UP).Count(&num).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}

func (mtSvc *marketService) GetMarkPropById(ac common.IActionCtx, Cid int64, Bid int64) (res *model.CityMarket, err utils.IError) {
	errDb := mtSvc.Db().Model(model.CityMarket{}).Where("id = ?", Bid).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}
