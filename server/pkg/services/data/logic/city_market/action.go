package city_market

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type IMarketService interface {
	common.IBaseService
	//获取搜索关键字的物品
	GetTitleSearch(ac common.IActionCtx, Cid, BuildId int64, Title string, Type map[int32]int32, CurrentPage, TotalPage int64) (res model.ResPageList, err utils.IError)
	//获取上架物品的记录-默认:正售出(上架)
	GetSellingRecords(ac common.IActionCtx, Cid, BuildId int64, Status model.MarketStatus, CurrentPage, TotalPage int64) (res model.ResPageList, err utils.IError)
	//上架
	PutInto(ac common.IActionCtx, Cid, BuildId int64, PtParams []*model.MarketPutParams) (res []*model.CityMarket, err utils.IError)
	//手动下架
	Remove(ac common.IActionCtx, Cid int64, Bid int64) (res *model.CityMarket, err utils.IError)
	// 购买-本业务只更新状态和购买人
	TryBuy(ac common.IActionCtx, Cid int64, Bid int64) (res *model.CityMarket, err utils.IError)
	//挂单方时间过期自动下架
	//AutoRemove(ac common.IActionCtx,Cid int64, Bids []int64) (err utils.IError)
	//获取单个上架商品的信息
	GetMarkPropById(ac common.IActionCtx, Cid int64, Bid int64) (res *model.CityMarket, err utils.IError)
}

func (mtSvc *marketService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		msg_dt.CdMarketList: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMarketTitleList)
			res, err := mtSvc.GetTitleSearch(ac, req.Cid, req.BuildId, req.Title, req.Type, req.Page, req.Size)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMarketList{
				MarketList: res,
			}, 0
		},

		msg_dt.CdMarketRecord: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMarketRecord)
			res, err := mtSvc.GetSellingRecords(ac, req.Cid, req.BuildId, model.MarketStatus_UP, req.Page, req.Size)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMarketRecord{
				MarketList: res,
			}, 0
		},

		msg_dt.CdMarketPutInto: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMarketPutInto)
			res, err := mtSvc.PutInto(ac, req.Cid, req.BuildId, req.PtParams)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMarketPutInto{
				MarketList: res,
			}, 0
		},
		msg_dt.CdMarketRemove: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMarketRemove)
			res, err := mtSvc.Remove(ac, req.Cid, req.Bid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMarketRemove{
				MarketList: res,
			}, 0
		},

		msg_dt.CdMarketGetUpCount: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, code err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMarketGetUpCount)
			num, err := mtSvc.GetMarkCount(ac, req.Cid, req.BuildId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMarketGetUpCount{
				Num: num,
			}, 0
		},

		msg_dt.CdMarketGetPropById: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, code err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqMarketGetPropById)
			res, err := mtSvc.GetMarkPropById(ac, req.Cid, req.Bid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResMarketGetPropById{
				MarketList: res,
			}, 0
		},
	}

}

func (mtSvc *marketService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdMarketBuy: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqMarketBuy)
				ups, err := mtSvc.TryBuy(ac, req.Cid, req.Bid)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					resErrCode = err_dt.ErrQueryFailed
					return
				}
				resBody = &msg_dt.ResMarketBuy{
					MarketList:ups,
				}
				resErrCode = 0
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				req := resBody.(*msg_dt.ResMarketBuy)
				IError := mtSvc.ConfirmBuy(ac, req.Bid, req.MarketList)
				if IError != nil {
					app.Log().TError(ac.Tid(), IError)
					return IError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				return nil
			},
		},

		msg_dt.CdMarketAutoCancel: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				//_ = reqBody.(*msg_dt.ReqMarketAutoCancel)
				res,ups, err := mtSvc.TryAutoCancel(ac)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					resErrCode = err_dt.ErrQueryFailed
					return
				}
				resBody = &msg_dt.ResMarketAutoCancel{
					MarketList:res,
					UpIds:ups,
				}
				resErrCode = 0
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				req := resBody.(*msg_dt.ResMarketAutoCancel)
				IError := mtSvc.ConfirmAutoCancel(ac, req.UpIds)
				if IError != nil {
					app.Log().TError(ac.Tid(), IError)
					return IError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				return nil
			},
		},
	}
}
