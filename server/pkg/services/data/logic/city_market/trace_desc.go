package city_market

const (
	//建筑市场参数错误
	TMarketParamFail = "Wrong construction market parameters"
	//物品不允许上架
	TMarketNotAllowedSell = "Merchandise is not allowed on shelves"
	//上架已达到上限
	TMarketOverLimit = "Shelves have reached their limit"
	//不允许购买自己物品
	TMarketNotAllowedBuySelf = "You are not allowed to buy your own goods"
	//物品已过期
	TMarketExpired = "Shelf item expired"
)