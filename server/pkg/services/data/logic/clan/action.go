package clan

import (
	"95eh.com/eg/app"
	eg_utils "95eh.com/eg/utils"
	"hm/pkg/misc/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type IClanService interface {
	common.IBaseService
}

// 消息处理器
func (c *clanService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode){
		msg_dt.CdClanLoad: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqClanLoad)
			clan, iErr := c.GetClanById(req.ClanId)
			if iErr != nil {
				app.Log().TError(ac.Tid(), iErr)
				return nil, err_dt.ErrQueryFailed
			}
			clanMember, iErr := c.GetClanMemberByCid(req.Cid)
			return &msg_dt.ResClanLoad{
				Clan:     clan,
				IsSignIn: int64(clanMember.SignInTime) > utils.GetCurrentDayTimestamp(),
			}, 0
		},
		msg_dt.CdClanPageSearch: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqClanPageSearch)
			total, clanList, err := c.PageGetClanList(req.Page, req.Size)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return &msg_dt.ResClanPageSearch{Total: int32(total), ClanList: clanList}, 0
		},
	}
}

func (c *clanService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdClanTryCreate: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqClanTryCreate)
				clan, err := c.CheckCreateClan(req.Cid, req.ClanName, req.ClanSurname, req.MemberList)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return nil, err_dt.ErrCreateFailed
				}
				return &msg_dt.ResClanTryCreate{Clan: clan}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError eg_utils.IError) {
				res := tryResBody.(*msg_dt.ResClanTryCreate)
				err := c.CreateClan(ac, res.Clan)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return err
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError eg_utils.IError) {
				return nil
			},
		},
		msg_dt.CdClanTryUpNameOrSurname: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqClanTryUpNameOrSurname)
				upType, upMap, iErr := c.UpNameOrSurname(req.ClanName, req.ClanSurname)
				if iErr != nil {
					app.Log().TError(ac.Tid(), iErr)
					return nil, err_dt.ErrUpdateFailed
				}
				return &msg_dt.ResClanTryUpNameOrSurname{ClanId: req.ClanId, UpType: upType, UpMap: upMap}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError eg_utils.IError) {
				res := tryResBody.(*msg_dt.ResClanTryUpNameOrSurname)
				iErr := c.UpClanMap(ac, res.UpType, res.UpMap, res.ClanId)
				if iErr != nil {
					app.Log().TError(ac.Tid(), iErr)
					return iErr
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError eg_utils.IError) {
				return nil
			},
		},
		msg_dt.CdClanTryUpDeclaration: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqClanTryUpDeclaration)
				upType, upMap, iErr := c.UpDeclaration(req.ClanDeclaration)
				if iErr != nil {
					app.Log().TError(ac.Tid(), iErr)
					return nil, err_dt.ErrUpdateFailed
				}
				return &msg_dt.ResClanTryUpDeclaration{ClanId: req.ClanId, UpType: upType, UpMap: upMap}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError eg_utils.IError) {
				res := tryResBody.(*msg_dt.ResClanTryUpDeclaration)
				iErr := c.UpClanMap(ac, res.UpType, res.UpMap, res.ClanId)
				if iErr != nil {
					app.Log().TError(ac.Tid(), iErr)
					return iErr
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError eg_utils.IError) {
				return nil
			},
		},
		msg_dt.CdClanTrySignIn: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqClanTrySignIn)
				clanId, upMap, iErr := c.ClanSignIn(req.Cid)
				if iErr != nil {
					app.Log().TError(ac.Tid(), iErr)
					return nil, err_dt.ErrUpdateFailed
				}
				return &msg_dt.ResClanTrySignIn{UpMap: upMap, ClanMemberId: clanId}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError eg_utils.IError) {
				res := tryResBody.(*msg_dt.ResClanTrySignIn)
				iErr := c.UpClanMemberByMap(ac, res.UpMap, res.ClanMemberId)
				if iErr != nil {
					app.Log().TError(ac.Tid(), iErr)
					return iErr
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError eg_utils.IError) {
				return nil
			},
		},
	}
}
