package clan

import (
	"95eh.com/eg/utils"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
)

func (c *clanService) UpClanMemberByMap(ac common.IActionCtx, upMap map[string]interface{}, id int64) utils.IError {
	iErr := c.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
		txDb := txAc.TxDb().Model(&model.ClanMember{})
		err := txDb.Where("cid = ?", id).Updates(upMap).Error
		if err != nil {
			iError = utils.NewError(err.Error(), utils.M{"id": id, "upMap": upMap})
		}
		return
	})
	if iErr != nil {
		return iErr
	}
	return iErr
}

func (c *clanService) GetClanMemberByCid(cid int64) (clanMember *model.ClanMember, iErr utils.IError) {
	db := c.Db().Model(&model.ClanMember{})
	var t model.ClanMember
	err := db.Where("cid = ?", cid).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"cid": cid})
		}
	}
	if t.Id == 0 {
		iErr = utils.NewError("", utils.M{})
		return
	}
	return &t, nil
}
