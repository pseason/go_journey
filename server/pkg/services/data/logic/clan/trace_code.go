package clan

const (
	TErrClanNameWordLimit    = "ClanName word limit"
	TErrClanSurnameWordLimit = "ClanSurname word limit"
	TErrNameNickIllegal      = "ClanNameNickIllegal"
	TErrSensitiveNickname    = "nickname contains sensitive words"
	TErrClanNameRepeat       = "clanName is Repeat"
	TErrClanNotFail          = "clan not fail"
	TErrUpClanNameAndClanSurnameDataIsEmpty = "update clanName And clanSurname data is empty"
	TErrUpDeclarationDataIsEmpty = "update clan declaration data is empty"
	TErrNoCheckInTime = "character No check-in time"
	TErrCreateClanMemberNeed = "TErrCreateClanMemberNeed"
)
