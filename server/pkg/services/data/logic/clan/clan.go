package clan

import (
	"95eh.com/eg/utils"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/misc/tools"
	util "hm/pkg/misc/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"time"
	"unicode"
)

type clanService struct {
	common.IBaseService
}

func NewClanService() *clanService {
	return &clanService{IBaseService: common.NewBaseService(&model.Clan{}, &model.ClanMember{})}
}

func (c *clanService) AfterInit() error {
	return nil
}

//GetClanById 通过氏族id查询氏族
func (c *clanService) GetClanById(clanId int64) (clan *model.Clan, iErr utils.IError) {
	db := c.Db().Model(&model.Clan{})
	var t model.Clan
	err := db.Where("id = ?", clanId).First(&t).Error
	if err != nil {
		if !errors.Is(gorm.ErrRecordNotFound, err) {
			iErr = utils.NewError(err.Error(), utils.M{"clanId": clanId})
			return
		}
	}
	if t.Id == 0 {
		iErr = utils.NewError(TErrClanNotFail, utils.M{"clanId": clanId})
		return
	}

	return &t, nil
}

//PageGetClanList 分页查询氏族列表
func (c *clanService) PageGetClanList(page, size int32) (count int64, res []*model.Clan, iErr utils.IError) {
	db := c.Db().Model(&model.Clan{})
	err := db.Count(&count).Error
	if err != nil {
		return count, nil, utils.NewError(err.Error(), utils.M{"page": page, "size": size})
	}
	if count > 0 {
		err = db.Limit(int(page)).Offset(int(page * size)).Find(&res).Error
		if err != nil {
			return count, nil, utils.NewError(err.Error(), utils.M{"page": page, "size": size})
		}
		return count, res, nil
	}

	return count, make([]*model.Clan, 0), nil

}

//UpClanMap 修改氏族信息
func (c *clanService) UpClanMap(ac common.IActionCtx, upType int32, upMap map[string]interface{}, clanId int64) utils.IError {
	return c.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
		tx := txAc.TxDb().Model(&model.Clan{})
		err := tx.Where("id = ?", clanId).Updates(upMap).Error
		if err != nil {
			iError = utils.NewError(err.Error(), utils.M{"upClanId": clanId, "upClanMap": upMap})
		}
		if upType > 0 {
			clan, iErr := c.GetClanById(clanId)
			if iErr != nil {
				return nil, iErr
			}
			eventData := &msg_dt.EveClanNoticeChange{
				ClanId:  clanId,
				CidList: clan.GetClanMemberList(),
				UpType:  upType,
			}
			switch upType {
			case 1:
				eventData.ClanName = upMap["clan_name"].(string)
			case 2:
				eventData.ClanSurname = upMap["clan_surname"].(string)
			case 3:
				eventData.ClanName = upMap["clan_name"].(string)
				eventData.ClanSurname = upMap["clan_surname"].(string)
			case 4:
				eventData.ClanDeclaration = upMap["clan_declaration"].(string)

			}
			c.DispatchEvent(misc.NodeGame, ac.Tid(), clanId, msg_dt.CdEveClanNoticeChange, eventData)
		}
		return
	})
}

//CreateClan 创建氏族
func (c *clanService) CreateClan(ac common.IActionCtx, clan *model.Clan) (iError utils.IError) {
	iError = c.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
		tx := txAc.TxDb().Model(model.Clan{})
		err := tx.Create(clan).Error
		if err != nil {
			iError = utils.NewError(err.Error(), utils.M{"addClan": clan})
			return
		}
		clanMemberCidList := clan.GetClanMemberList()
		if len(clanMemberCidList) == 0 {
			iError = utils.NewError(TErrCreateClanMemberNeed, utils.M{"clanMemberList": 0})
		}
		txMember := txAc.TxDb().Model(model.ClanMember{})
		memberList := make([]*model.ClanMember, 0)
		for _, cid := range clanMemberCidList {
			memberList = append(memberList, &model.ClanMember{
				Cid:    cid,
				ClanId: clan.Id,
			})
		}
		err = txMember.Create(&memberList).Error
		if err != nil {
			iError = utils.NewError(err.Error(), utils.M{"createClanMemberFail": memberList})
			return
		}

		return
	})
	return
}

//LoadClan 加载氏族
func (c *clanService) LoadClan(clanId int64) (clan *model.Clan, iErr utils.IError) {
	clan, iErr = c.GetClanById(clanId)
	if iErr != nil {
		return
	}
	return
}

//UpNameOrSurname 修改氏族名称和姓氏
func (c *clanService) UpNameOrSurname(clanName, surname string) (upType int32, upMap map[string]interface{}, iErr utils.IError) {
	if len([]rune(clanName)) > 4 {
		iErr = utils.NewError(TErrClanNameWordLimit, utils.M{"clanName": clanName})
		return
	}
	if len([]rune(surname)) > 3 {
		iErr = utils.NewError(TErrClanSurnameWordLimit, utils.M{"clanSurname": surname})
		return
	}

	iErr = CheckNameIllegal(clanName)
	if iErr != nil {
		return
	}
	iErr = CheckNameIllegal(surname)
	if iErr != nil {
		return
	}
	iErr = c.CheckRepeatName(clanName)
	if iErr != nil {
		return
	}

	upMap = make(map[string]interface{})
	if clanName != "" {
		upType = 1
		upMap["clan_name"] = clanName
	}
	if surname != "" {
		if upType > 0 {
			upType = 3
		} else {
			upType = 2
		}
		upMap["clan_surname"] = surname
	}
	if upType == 0 {
		iErr = utils.NewError(TErrUpClanNameAndClanSurnameDataIsEmpty, utils.M{
			"upClanName":    clanName,
			"upClanSurname": surname,
		})
	}
	return
}

//UpDeclaration 修改氏族宣言
func (c *clanService) UpDeclaration(clanDeclaration string) (upType int32, upMap map[string]interface{}, iErr utils.IError) {
	upMap = make(map[string]interface{})
	if clanDeclaration != "" {
		upType = 4
		upMap["clan_declaration"] = clanDeclaration
	}

	if upType == 0 {
		iErr = utils.NewError(TErrUpDeclarationDataIsEmpty, utils.M{
			"clanDeclaration": clanDeclaration,
		})
	}
	return
}

//ClanSignIn 氏族签到
func (c *clanService) ClanSignIn(cid int64) (clanId int64, upMap map[string]interface{}, iErr utils.IError) {
	clanMember, iErr := c.GetClanMemberByCid(cid)
	if iErr != nil {
		return clanId, nil, iErr
	}
	if int64(clanMember.SignInTime) > util.GetCurrentDayTimestamp() {
		iErr = utils.NewError(TErrNoCheckInTime, utils.M{"cid": cid})
		return
	}
	clanId = cid
	upMap = make(map[string]interface{})
	upMap["sign_in_time"] = int32(time.Now().Unix())
	return clanId, upMap, nil
}

//CheckRepeatName 效验重复氏族名称
func (c *clanService) CheckRepeatName(name string) utils.IError {
	db := c.Db().Model(&model.Clan{})
	var count int64
	err := db.Where("clan_name = ?", name).Count(&count).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"name": name})
	}
	if count > 0 {
		return utils.NewError(TErrClanNameRepeat, utils.M{"name": name})
	}
	return nil
}

//CheckCreateClan 效验氏族名称、姓氏 check clan clanName and clanSurname word
func (c *clanService) CheckCreateClan(cid int64, clanName, clanSurname string, memberList []int64) (clan *model.Clan, iErr utils.IError) {
	if len([]rune(clanName)) > 4 {
		return nil, utils.NewError(TErrClanNameWordLimit, utils.M{"clanName": clanName})
	}
	if len([]rune(clanSurname)) > 3 {
		return nil, utils.NewError(TErrClanSurnameWordLimit, utils.M{"clanSurname": clanSurname})
	}

	iErr = CheckNameIllegal(clanName)
	if iErr != nil {
		return
	}
	iErr = CheckNameIllegal(clanSurname)
	if iErr != nil {
		return
	}
	iErr = c.CheckRepeatName(clanName)
	if iErr != nil {
		return
	}
	return model.GenDefaultClan(clanName, clanSurname, memberList, cid), nil
}

func CheckNameIllegal(name string) (iErr utils.IError) {
	if tools.IsSensitiveWords(name) {
		return utils.NewError(TErrSensitiveNickname, utils.M{"name": name})
	}
	for _, s := range name {
		if !unicode.Is(unicode.Han, s) {
			return utils.NewError(TErrNameNickIllegal, utils.M{"s": s, "clanName": name})
		}
	}
	return nil
}
