package task

import (
	"95eh.com/eg/utils"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/task_e"
)

//GetTaskById 通过任务id查询任务 query task by task id
func (t *taskService) GetTaskById(taskId int64) (*model.Task,utils.IError) {
	db := t.Db().Model(&model.Task{})
	var task model.Task
	err := db.Where("id = ?", taskId).First(&task).Error
	if err != nil {
		if !errors.Is(err,gorm.ErrRecordNotFound) {
			return nil,utils.NewError(err.Error(),utils.M{"taskId":taskId})
		}
	}
	if task.Id > 0 {
		return &task,nil
	}
	return nil,nil
}


//GetTaskListByCidAndTempId 通过玩家id和任务模板id查询任务列表 query task list by character id and task template Id
func (t *taskService) GetTaskListByCidAndTempId(cid,startTime, endTime int64,tempIdList ...int32) (res []*model.Task,iErr utils.IError) {
	db := t.Db().Model(&model.Task{})
	var checkTime string
	if startTime > 0 {
		checkTime = fmt.Sprintf(" and created between %d and %d",startTime, endTime)
	}
	err := db.Where("cid = ? and task_type in ?" + checkTime, cid, tempIdList).Find(&res).Error
	if err != nil {
		return nil,utils.NewError(err.Error(),utils.M{"cid":cid,"tempIdList":tempIdList})
	}
	return res,nil
}

//GetTaskListByCidAndFilterTempId 通过玩家id和过滤任务模板id查询任务列表 query task list by character id and task template Id
func (t *taskService) GetTaskListByCidAndFilterTempId(cid int64,tempIdList ...int32) (res []*model.Task,iErr utils.IError) {
	db := t.Db().Model(&model.Task{})
	err := db.Where("cid = ? and task_type not in ? ", cid, tempIdList).Find(&res).Error
	if err != nil {
		return nil,utils.NewError(err.Error(),utils.M{"cid":cid,"tempIdList":tempIdList})
	}
	return res,nil
}

//GetTaskListByCidAndTaskTempId queryTaskList by cid and taskTempId
func (t *taskService) GetTaskListByCidAndTaskTempId(cid int64,taskTempIdList ...int32) (res []*model.Task,iErr utils.IError) {
	db := t.Db().Model(&model.Task{})
	err := db.Where("cid = ? and template_id in ?", cid, taskTempIdList).Find(&res).Error
	if err != nil {
		return nil,utils.NewError(err.Error(),utils.M{"cid": cid,"taskTempIdList": taskTempIdList})
	}
	return res,nil
}

//DelTaskAllByTempId del character all task by cid and taskTempIdList
func (t *taskService) DelTaskAllByTempId(cid int64,taskTempIdList ...task_e.TaskType) utils.IError {
	db := t.Db().Model(&model.Task{})
	err := db.Where("cid = ? and task_type in ?", cid, taskTempIdList).Delete(&model.Task{}).Error
	if err != nil {
		return utils.NewError(err.Error(),utils.M{"cid": cid,"taskTempIdList": taskTempIdList})
	}
	return nil
}

//UpTaskByUpMap 修改任务
func (t *taskService) UpTaskByUpMap(ac common.IActionCtx,taskId int64,upMap map[string]interface{}) utils.IError {
	iError := t.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
		db := txAc.TxDb().Model(&model.Task{})
		err := db.Where("id = ? ", taskId).Updates(upMap).Error
		if err != nil {
			iError = utils.NewError(err.Error(),utils.M{})
			return
		}
		return
	})
	if iError != nil {
		return iError
	}
	return nil
}