package task

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

func (t *taskService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		msg_dt.CdTaskList: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTaskList)
			tasks, err := t.GetTasks(ac,req.Cid)
			if err != nil {
				return nil, err_dt.ErrQueryFailed
			}
			return &msg_dt.ResTaskList{Records: tasks}, 0
		},
	}
}

func (t *taskService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdTaskTryLoginInit: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return &msg_dt.ResTaskTryLoginInit{},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqTaskTryLoginInit)
				t.LoginInitTask(ac,req.IsTaskNotify,req.Cid...)
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTaskTryOffLineInit: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return &msg_dt.ResTaskTryOffLineInit{},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqTaskTryOffLineInit)
				iError = t.OffLineTask(req.Cid)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTaskTryExecute: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTaskTryExecute)
				addRedisTempIdMap, delRedisTempIdMap, addTaskList, upTaskList, addPlotTempId, propMap, skillMap, infoMap, iErr := t.TaskExecute(req.Cid, req.AftType, req.PropId, req.PropNum)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return nil,err_dt.ErrTryFailed
				}
				return &msg_dt.ResTaskTryExecute{
					DelRedisTaskMap: delRedisTempIdMap,
					AddRedisTempIdMap: addRedisTempIdMap,
					UpTaskDb: upTaskList, AddTaskDb: addTaskList,
					AddPlotIdList: addPlotTempId, AddPropMap: propMap,
					AddSkillMap: skillMap,
					AddInfoMap: infoMap},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqTaskTryExecute)
				res := tryResBody.(*msg_dt.ResTaskTryExecute)
				iError = t.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
					if len(res.AddTaskDb) > 0 {
						iError = t.AddTaskDb(txAc, res.AddTaskDb)
						if iError != nil {
							return
						}
					}
					if len(res.UpTaskDb) > 0 {
						iError = t.UpTaskDb(txAc,res.UpTaskDb)
						if iError != nil {
							return
						}
						notifyTaskList := make([]*model.Task,0)
						if len(res.AddTaskDb) > 0 {
							notifyTaskList = append(notifyTaskList,res.AddTaskDb...)
						}
						t.NotifyTaskChange(req.Cid,ac.Tid(),append(notifyTaskList,res.UpTaskDb...)...)
					}
					if len(res.AddRedisTempIdMap) > 0 {
						iError = t.AddTaskRedis(req.Cid,res.AddRedisTempIdMap)
						if iError != nil {
							return
						}
					}
					if len(res.DelRedisTaskMap) > 0 {
						iError = t.DelTaskRedis(req.Cid,res.DelRedisTaskMap)
						if iError != nil {
							return
						}
					}

					return
				})
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTaskTryToggleHidden: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTaskTryToggleHidden)
				task,iErr := t.TaskToggleHidden(req.TaskId)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return nil,err_dt.ErrQueryFailed
				}
				return &msg_dt.ResTaskTryToggleHidden{Task: task},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResTaskTryToggleHidden)
				iError = t.UpTaskByUpMap(ac,res.Task.Id, map[string]interface{}{"hidden": true})
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				t.NotifyTaskChange(res.Task.Cid,ac.Tid(),res.Task)
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTaskTryReceiveAward: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTaskTryReceiveAward)
				addPropMap,addSkillMap,addInfoMap,task, iErr := t.TaskReceiveAward(req.TaskId)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return nil,err_dt.ErrQueryFailed
				}
				return &msg_dt.ResTaskTryReceiveAward{Task: task,AddPropMap: addPropMap,AddSkillMap: addSkillMap,AddInfoMap: addInfoMap},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResTaskTryReceiveAward)
				iError = t.UpTaskByUpMap(ac,res.Task.Id, map[string]interface{}{"status": res.Task.Status})
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				t.NotifyTaskChange(res.Task.Cid,ac.Tid(),res.Task)
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},

	}
}
