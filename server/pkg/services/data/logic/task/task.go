package task

import (
	"95eh.com/eg/app"
	eg_utils "95eh.com/eg/utils"
	"bytes"
	"fmt"
	"github.com/golang-module/carbon"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	"hm/pkg/misc/cache_key"
	"hm/pkg/misc/tsv"
	"hm/pkg/misc/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/task_e"
	"hm/pkg/services/data/msg_dt"
	"math/rand"
	"time"
)

type taskService struct {
	common.IBaseService
	template *tsv.TaskTsvManager
}

func NewTaskService() *taskService {
	return &taskService{
		IBaseService: common.NewBaseService(&model.Task{}),
	}
}

func (t *taskService) AfterInit() error {
	t.template = tsv.GetTsvManager(tsv.Task).(*tsv.TaskTsvManager)
	//t.LoginInitTask(common.NewActionCtx(0,1111),false,1111)
	//go func() {
	//	time.Sleep(3*time.Second)
	//	t.TaskExecute(1111,1,3090017,100)
	//}()

	return nil
}

func (t *taskService) GenCacheKeyString(cid int64, aftType task_e.TaskAftType,needPropId int32) string {
	return cache_key.TaskCache.Format(cid, aftType,needPropId)
}

func (t *taskService) GenCacheKeyCid(cid int64) string {
	return cache_key.TaskCacheAllPattern.Format(cid)
}

func (t *taskService) GenCacheKeyRefresh(cid int64) string {
	return cache_key.TaskRefresh.Format(cid)
}

//玩家登录任务初始化话
func (t *taskService) LoginInitTask(ac common.IActionCtx,isNotNotify bool,cidList ...int64)  {
	taskConfig := t.Conf().Design.Task
	refreshTime := int64(taskConfig.DailyTaskResetTime)
	for _,cid := range cidList {
		actorId := cid
		app.Worker().DoActions(actorId, func() {
			app.Log().TInfo(ac.Tid(),"character start init task",eg_utils.M{"Cid": actorId})
			addDailyTaskList,addDailyTaskTypeList,iErr := t.CheckTask(actorId,task_e.Daily,refreshTime,taskConfig.DailyInitTaskTypes...)
			if iErr != nil {
				app.Log().TError(ac.Tid(),iErr)
			}
			addWeekTaskList,addWeekTaskTypeList,iErr := t.CheckTask(actorId,task_e.Week,refreshTime,taskConfig.WeekInitTaskTypes...)
			if iErr != nil {
				app.Log().TError(ac.Tid(),iErr)
			}
			addDailyTaskTypeList = append(addDailyTaskTypeList,addWeekTaskTypeList...)
			addDailyTaskList = append(addDailyTaskList,addWeekTaskList...)
			if len(addDailyTaskTypeList) > 0 {
				iErr = t.DelTaskAllByTempId(cid, addDailyTaskTypeList...)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return
				}
				iError := t.AddTask(ac, actorId, addDailyTaskTypeList, taskConfig.DailyTaskCap, isNotNotify)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return
				}
			}
			if len(addDailyTaskList) > 0{


			}
			app.Log().TInfo(ac.Tid(),"character end init task",eg_utils.M{"Cid": actorId})
			t.Redis().Set(t.Context(),t.GenCacheKeyRefresh(actorId),"",0)
		})
	}
}

//玩家下线任务删除
func (t taskService) OffLineTask(cid int64) eg_utils.IError {
	result,err := t.Redis().Keys(t.Context(), t.GenCacheKeyCid(cid)).Result()
	if err != nil {
		return eg_utils.NewError(err.Error(), nil)
	}
	result = append(result,t.GenCacheKeyRefresh(cid))
	if len(result) > 0 {
		err = t.Redis().Del(t.Context(),result...).Err()
		if err != nil {
			return eg_utils.NewError(err.Error(), nil)
		}

	}
	return nil
}

//效验玩家任务是否有新增
func (t taskService) CheckTask(cid int64,checkTimeType task_e.ReSetTimeType,resetTime int64,taskTypeList ...int32) (addTask []*model.Task,addTaskType []task_e.TaskType,iErr eg_utils.IError) {
	//任务完成计数服务条件的队伍一起添加
	//1.查询玩家所拥有的任务类型
	startTimestamp,endTimeNum := t.GetDailyOrWeekTime(resetTime,checkTimeType)
	addTask, iErr = t.GetTaskListByCidAndTempId(cid,startTimestamp,endTimeNum, taskTypeList...)
	if iErr != nil {
		return nil,nil,iErr
	}
	//2.比对checkType和查询出的type对比对->得到要添加的类型
	addTaskType = make([]task_e.TaskType,0)
	for _,taskType := range taskTypeList {
		isType := true
		for _,task := range addTask {
			if isType && task.TaskType  == task_e.TaskType(taskType) {
				isType = false
				break
			}
		}
		if isType {
			addTaskType = append(addTaskType,task_e.TaskType(taskType))
		}
	}
	return addTask,addTaskType,nil

}

//添加任务
func (t *taskService) AddTask(ac common.IActionCtx,cid int64,addTaskTypeList []task_e.TaskType,dailyTaskCap int32,isNotNotify bool) (sErr eg_utils.IError) {
	addTask := make([]*model.Task,0)
	addTaskAftTypeMap := make(map[task_e.TaskAftType]map[int32][]int32)
	for _,taskType := range addTaskTypeList {
		if taskType == task_e.TaskTypeDaily {
			taskTsvList := t.GetTemplateListByTaskType(int32(taskType))
			rand.Seed(time.Now().Unix())
			for i := 1; i <= int(dailyTaskCap); i++ {
				randomNum := rand.Intn(len(taskTsvList)-1)
				addTask = append(addTask,model.GenerateTask(cid,taskTsvList[randomNum]))
				needMap,ok := addTaskAftTypeMap[task_e.TaskAftType(taskTsvList[randomNum].Afttype)]
				if !ok {
					addTaskAftTypeMap[task_e.TaskAftType(taskTsvList[randomNum].Afttype)] = make(map[int32][]int32)
					for _,taskSli := range taskTsvList[randomNum].Needprop {
						data,ok1 := addTaskAftTypeMap[task_e.TaskAftType(taskTsvList[randomNum].Afttype)][taskSli[0]]
						if ok1 {
							addTaskAftTypeMap[task_e.TaskAftType(taskTsvList[randomNum].Afttype)][taskSli[0]] = append(data,taskTsvList[randomNum].Id)
							continue
						}
						addTaskAftTypeMap[task_e.TaskAftType(taskTsvList[randomNum].Afttype)][taskSli[0]] = []int32{taskTsvList[randomNum].Id}
					}
				}else{
					for _,taskSli := range taskTsvList[randomNum].Needprop {
						data,ok1 := needMap[taskSli[0]]
						if ok1 {
							needMap[taskSli[0]] = append(data,taskTsvList[randomNum].Id)
							continue
						}
						needMap[taskSli[0]] = []int32{taskTsvList[randomNum].Id}
					}
				}
			}
		}else {
			for _,taskTypeTsv := range t.template.TsvSlice {
				if taskTypeTsv.Type == int32(taskType) {
					task := model.GenerateTask(cid, taskTypeTsv)
					addTask = append(addTask,task)
					isEndTask, _ := t.checkTaskDirectEnd(task, 0, 0, taskTypeTsv,
						nil,nil,nil,nil,nil,nil,nil,false)
					if !isEndTask{
						if taskTypeTsv.ReceiveRewards == 0  {
							task.Status = task_e.TaskWaitHandel
						}
						task.Status = task_e.TaskStatusCompleted
						continue
					}
					needMap,ok := addTaskAftTypeMap[task_e.TaskAftType(taskTypeTsv.Afttype)]
					if !ok {
						addTaskAftTypeMap[task_e.TaskAftType(taskTypeTsv.Afttype)] = make(map[int32][]int32)
						for _,taskSli := range taskTypeTsv.Needprop {
							data,ok1 := addTaskAftTypeMap[task_e.TaskAftType(taskTypeTsv.Afttype)][taskSli[0]]
							if ok1 {
								addTaskAftTypeMap[task_e.TaskAftType(taskTypeTsv.Afttype)][taskSli[0]] = append(data,taskTypeTsv.Id)
								continue
							}
							addTaskAftTypeMap[task_e.TaskAftType(taskTypeTsv.Afttype)][taskSli[0]] = []int32{taskTypeTsv.Id}
						}
					}else{
						for _,taskSli := range taskTypeTsv.Needprop {
							data,ok1 := needMap[taskSli[0]]
							if ok1 {
								needMap[taskSli[0]] = append(data,taskTypeTsv.Id)
								continue
							}
							needMap[taskSli[0]] = []int32{taskTypeTsv.Id}
						}
					}
				}
			}
		}
	}

	sErr = t.LocalChainTx(ac, func(txAc common.IActionCtx) (caf func(), sErr eg_utils.IError) {
		err := t.TxDb(ac.TxDb()).Create(addTask).Error
		if err != nil {
			sErr = eg_utils.NewError(err.Error(), nil)
			return
		}
		if len(addTaskAftTypeMap) > 0 {
			sErr = t.AddTaskRedis(cid,addTaskAftTypeMap)
			if sErr != nil {
				return
			}
		}
		return
	})
	if sErr != nil {
		return sErr
	}
	return nil
}

//执行任务
func (t *taskService) TaskExecute(cid int64,aftType task_e.TaskAftType,propType,propNum int32) (
	addRedisTempIdMap,delRedisTempIdMap map[task_e.TaskAftType]map[int32][]int32,
	addTaskList,UpTaskList []*model.Task,
	addPlotTempId []int32,
	addPropMap,addSkillMap,addInfoMap map[int32]int32,
	iErr eg_utils.IError) {
	result, err := t.Redis().Get(t.Context(), t.GenCacheKeyString(cid, aftType, propType)).Result()
	if err != nil {
		iErr = eg_utils.NewError(err.Error(), eg_utils.M{"cid":cid,"aftType":aftType,"propType":propType})
		return
	}
	taskTempIdList := model.GetAuthList(result)
	UpTaskList, iErr = t.GetTaskListByCidAndTaskTempId(cid, taskTempIdList...)
	
	if iErr != nil {
		return
	}
	delRedisTempIdMap = make(map[task_e.TaskAftType]map[int32][]int32)
	addRedisTempIdMap = make(map[task_e.TaskAftType]map[int32][]int32)
	addTaskList = make([]*model.Task,0)
	addPlotTempId = make([]int32,0)
	addPropMap = make(map[int32]int32)
	addSkillMap = make(map[int32]int32)
	addInfoMap = make(map[int32]int32)


	for _,task := range UpTaskList {
		taskTsv,ok := t.template.TsvMap[task.TemplateId]
		if !ok {
			iErr = eg_utils.NewError(TErrTaskNotTemplate,eg_utils.M{"TemplateId":task.TemplateId})
			return
		}
		//无配置模板异常
		t.checkTaskDirectEnd(task,propType,propNum,taskTsv,&addPlotTempId,addRedisTempIdMap,delRedisTempIdMap,
			addPropMap,addSkillMap,addInfoMap,addTaskList,true)
	}
	return
}

//效验任务是否直接完成
func (t taskService) checkTaskDirectEnd(task *model.Task,propId,propNum int32,taskTsv *tsv.TaskTsv,
	addPlotTempId *[]int32,addRedisTempIdMap,delRedisTempIdMap map[task_e.TaskAftType]map[int32][]int32,
	addPropMap,addSkillMap,addInfoMap map[int32]int32,addTask []*model.Task,
	isHandel bool) (isEndTask bool,iErr eg_utils.IError) {
	if taskTsv == nil {
		ok := false
		taskTsv,ok = t.template.TsvMap[task.TemplateId]
		if !ok {
			return false,eg_utils.NewError(TErrTaskNotTemplate,eg_utils.M{"TemplateId":task.TemplateId})
		}
	}
	if len(taskTsv.Needprop) > 0 {
		switch task.AftType {
		case task_e.TaskAftPick:
			for i,num := range taskTsv.Needprop {
				if task.Record[i] == num[1]  {
					continue
				}else if task.Record[i]+propNum < num[1] {
					task.Record[i] = task.Record[i]+propNum
					isEndTask = true
				}else{
					task.Record[i] = num[1]
				}
			}

		}
		if !isEndTask {
			task.Status = task_e.TaskStatusCompleted
			data,ok := delRedisTempIdMap[task.AftType]
			if ok {
				data1,ok1 := data[propId]
				if ok1 {
					data[propId] = append(data1,taskTsv.Id)
				}else{
					data[propId] = []int32{taskTsv.Id}
				}
			}else{
				delRedisTempIdMap[task.AftType] = map[int32][]int32{propId:{taskTsv.Id}}
			}
		}
	}
	if !isEndTask && isHandel{
		//判断奖励是否自动领取
		if taskTsv.ReceiveRewards == 0 {
			task.Status = task_e.TaskStatusReceived
			//物品
			t.ProReward(taskTsv.Getprop,addPropMap)
			//技能
			t.ProReward(taskTsv.Skill,addSkillMap)
			//货币属性
			t.ProReward(taskTsv.GetMix,addInfoMap)

			//判断是否有后续任务
			if taskTsv.NextId > 0 {
				//新增任务实体
				//效验任务是否能直接完成
				//将不能完成的任务，需要其他服务判断的任务加入game服务，取值请求批量任务处理判断
				addTaskTempTsv,ok := t.template.TsvMap[taskTsv.NextId]
				if ok {
					newTask := model.GenerateTask(task.Cid,addTaskTempTsv)
					addTask = append(addTask,newTask)
					_, err := t.checkTaskDirectEnd(newTask, 0, 0, addTaskTempTsv,
						addPlotTempId,addRedisTempIdMap,delRedisTempIdMap, addPropMap, addSkillMap, addInfoMap, addTask, true)
					if err != nil {
						app.Log().TError(0,eg_utils.NewError("新增任务失败",eg_utils.M{"newTask":newTask,"addTaskTempTsv":addTaskTempTsv}))
						return
					}
					data,ok := addRedisTempIdMap[newTask.AftType]
					if ok {
						data1,ok1 := data[propId]
						if ok1 {
							data1 = append(data1,taskTsv.Id)
						}else{
							data[propId] = []int32{taskTsv.Id}
						}
					}else{
						addRedisTempIdMap[newTask.AftType] = map[int32][]int32{propId:{taskTsv.Id}}
					}
				}

			}
			//判断是否有后续剧情
			if taskTsv.Plot > 0 {
				*addPlotTempId = append(*addPlotTempId,taskTsv.Plot)
			}
		}

	}
	return isEndTask,nil
}

//请求任务
func (t *taskService) GetTasks(ac common.IActionCtx, cid int64) (res []*model.Task, iErr eg_utils.IError) {
	taskConfig := t.Conf().Design.Task
	refreshTime := int64(taskConfig.DailyTaskResetTime)
	dailyStartTime, dailyEndTime := t.GetDailyOrWeekTime(refreshTime, task_e.Daily)
	weekStartTime, weekEndTime := t.GetDailyOrWeekTime(refreshTime, task_e.Week)
	dailyTask, iErr := t.GetTaskListByCidAndTempId(cid,dailyStartTime, dailyEndTime, taskConfig.DailyInitTaskTypes...)
	if iErr != nil {
		return nil,iErr
	}
	weekTask, iErr := t.GetTaskListByCidAndTempId(cid,weekStartTime, weekEndTime, taskConfig.WeekInitTaskTypes...)
	if iErr != nil {
		return nil,iErr
	}

	ordinaryTask, iErr := t.GetTaskListByCidAndFilterTempId(cid, append(taskConfig.DailyInitTaskTypes, taskConfig.WeekInitTaskTypes...)...)

	if iErr != nil {
		return nil,iErr
	}
	res = make([]*model.Task,0)
	res = append(append(dailyTask,weekTask...),ordinaryTask...)
	t.convertTaskAndSetRedis(cid,res)

	return res,nil
}

//TaskToggleHidden 任务隐藏
func (t *taskService) TaskToggleHidden(taskId int64) (task *model.Task,iErr eg_utils.IError) {
	task, iErr = t.GetTaskById(taskId)
	if iErr != nil {
		return task,iErr
	}
	if task == nil {
		return task,eg_utils.NewError(TErrCharacterTaskNotExits,eg_utils.M{"hidden": task.Hidden})
	}
	if task.Hidden {
		return task,eg_utils.NewError(TErrCharacterTaskIsToggleHidden,eg_utils.M{"hidden": task.Hidden})
	}
	return task,nil
}


//TaskReceiveAward 任务完成领取奖励
func (t *taskService) TaskReceiveAward(taskId int64) (addPropMap,addSkillMap,addInfoMap map[int32]int32,task *model.Task,iErr eg_utils.IError) {
	task, iErr = t.GetTaskById(taskId)
	if iErr != nil {
		return nil,nil,nil,task,iErr
	}
	if task == nil {
		return nil,nil,nil,task,eg_utils.NewError(TErrCharacterTaskNotExits,eg_utils.M{"hidden": task.Hidden})
	}
	if task.Hidden {
		return nil,nil,nil,task,eg_utils.NewError(TErrCharacterTaskIsToggleHidden,eg_utils.M{"hidden": task.Hidden})
	}
	task.Status = task_e.TaskStatusReceived
	addPropMap = make(map[int32]int32)
	addSkillMap = make(map[int32]int32)
	addInfoMap = make(map[int32]int32)
	taskTsv,ok := t.template.TsvMap[task.TemplateId]
	if !ok {
		return nil,nil,nil,task,eg_utils.NewError(TErrTaskNotTemplate,eg_utils.M{"taskTemplateId": task.TemplateId})
	}
	t.ProReward(taskTsv.Getprop,addPropMap)
	t.ProReward(taskTsv.Skill,addSkillMap)
	t.ProReward(taskTsv.GetMix,addInfoMap)

	return addPropMap,addSkillMap,addInfoMap,task,nil
}

func (t taskService) checkTaskState(tasks []*model.Task)  {
	//upMap := make(map[int64]map[string]interface{})
	//delRedisTempIdMap := make(map[task_e.TaskAftType]map[int32][]int32)
	for _,task := range tasks {
		taskTsv,ok := t.template.TsvMap[task.TemplateId]
		if !ok {
			continue
		}
		if len(taskTsv.Needprop) > 0 {
			switch task.AftType {
			}
		}
	}

}


func (t taskService) ProReward(getProp [][]int32,addPropMap map[int32]int32) {
	if len(getProp) > 0 {
		for _,prop := range getProp {
			if len(prop) > 1 {
				_,ok := addPropMap[prop[0]]
				if ok {
					addPropMap[prop[0]] += prop[1]
				}else{
					addPropMap[prop[0]] = prop[1]
				}
			}
		}
	}

}

//添加任务到db
func (t taskService) AddTaskDb(ac common.IActionCtx,addTaskList []*model.Task) eg_utils.IError {
	tx := ac.TxDb().Model(&model.Task{})
	err := tx.Create(addTaskList).Error
	if err != nil {
		return eg_utils.NewError(err.Error(),eg_utils.M{"addTaskList":addTaskList})
	}
	return nil
}

//批量更新任务到db
func (t taskService) UpTaskDb(ac common.IActionCtx,upTaskList []*model.Task) eg_utils.IError {
	tx := ac.TxDb().Model(&model.Task{})
	sql := bytes.Buffer{}
	sql.WriteString("UPDATE `task` SET  `record` = CASE `id` ")
	updateIds := make([]int64, 0)
	for _, up := range upTaskList {
		value := model.GetAuthString(up.Record...)
		sql.WriteString(fmt.Sprintf("WHEN %d THEN '%s' ", up.Id, value))
		updateIds = append(updateIds, up.Id)
	}
	sql.WriteString("END , `status` = CASE `id` ")
	for _, up := range upTaskList {
		sql.WriteString(fmt.Sprintf("WHEN %d THEN %d ", up.Id, up.Status))
	}
	sql.WriteString("END ")
	sql.WriteString("WHERE `id` IN ?")
	rows := tx.Exec(sql.String(), updateIds).RowsAffected
	if rows <= 0 {
		tx.Rollback()
		return eg_utils.NewError("批量修改任务失败",eg_utils.M{"addTaskList":upTaskList})
	}
	return nil
}

func (t *taskService) NotifyTaskChange(cid,tid int64,upTaskList ...*model.Task)  {
	app.Discovery().DispatchEvent(misc.NodeGame,svc.Task,tid,cid,msg_dt.CdEveTaskChange,&msg_dt.EveTaskChange{
		Cid: cid,
		Records: upTaskList,
	})
}

//将任务set redis
func (t *taskService) convertTaskAndSetRedis(cid int64,taskList []*model.Task)  {
	if len(taskList) > 0 {
		addRedisTaskMap := make(map[task_e.TaskAftType]map[int32][]int32)
		for _,task := range taskList {
			if task.Status == task_e.TaskStatusWait {
				taskTsv,ok := t.template.TsvMap[task.TemplateId]
				if !ok {
					app.Log().TError(0,eg_utils.NewError("查询任务模板不存在",eg_utils.M{"cid": cid,"task": task}))
					continue
				}
				data,ok := addRedisTaskMap[task.AftType]
				if ok {
					for _,prop := range taskTsv.Needprop {
						data1,ok1 := data[prop[0]]
						if ok1 {
							data[prop[0]] = append(data1,taskTsv.Id)
						}else {
							data[prop[0]] = []int32{taskTsv.Id}
						}
					}
				}else{
					newMap := make(map[int32][]int32)
					for _,prop := range taskTsv.Needprop {
						newMap[prop[0]] = []int32{taskTsv.Id}
					}
					addRedisTaskMap[task.AftType] = newMap
				}
			}
		}

		iErr := t.AddTaskRedis(cid, addRedisTaskMap)
		if iErr != nil {
			app.Log().TError(0,iErr)
		}
	}
}

//获取日刷新或周刷新时间
func (t taskService) GetDailyOrWeekTime(resetTime int64, checkTimeType task_e.ReSetTimeType) (startTimestamp,endTimeNum int64) {
	fixedTime := resetTime * 3600000
	timestamp, timeNum := utils.GetTimestamp()
	checkTimestamp := timestamp + fixedTime
	if checkTimeType == task_e.Daily {
		//每日刷新
		//当前时间小于4点，昨日4-当天4点
		if time.Now().UnixNano() / 1e6 < checkTimestamp {
			startTimestamp = timestamp - (24 - resetTime) * 3600000
			endTimeNum = timestamp + fixedTime
		} else {
			//当前时间大于4点，当前时间当天4点-次日4点
			startTimestamp = timestamp + fixedTime
			endTimeNum = (timeNum + 1) + fixedTime
		}
	} else if checkTimeType == task_e.Week {
		//每周刷新
		//当前时间是周一，如果是周一则判断 当前时间小于4点，上周一4点 - 当天4点
		if time.Now().Weekday() == 1 {
			if time.Now().UnixNano() / 1e6 < checkTimestamp {
				startTimestamp = checkTimestamp - carbon.SecondsPerWeek*1000
				endTimeNum = checkTimestamp
			}else{
				startTimestamp = utils.GetFirstDateOfWeek() + fixedTime
				endTimeNum = startTimestamp + carbon.SecondsPerWeek*1000
			}
		} else {
			//当前时间不是周一,本周一4点 - 下周一4点
			startTimestamp = utils.GetFirstDateOfWeek() + fixedTime
			endTimeNum = startTimestamp + carbon.SecondsPerWeek*1000
		}
	}
	return
}

//添加任务到redis
func (t *taskService) AddTaskRedis(cid int64,addTaskAftTypeMap map[task_e.TaskAftType]map[int32][]int32) eg_utils.IError {
	if len(addTaskAftTypeMap) == 0 {
		return nil
	}
	for taskAftType,propIdList := range addTaskAftTypeMap {
		for propId,taskTempListId := range propIdList {
			err := t.Redis().Set(t.Context(), t.GenCacheKeyString(cid, taskAftType,propId),model.GetAuthString(taskTempListId...),0).Err()
			if err != nil {
				return eg_utils.NewError(err.Error(), nil)
			}
		}
	}
	return nil
}

//redis删除任务
func (t *taskService) DelTaskRedis(cid int64,delTaskAftTypeMap map[task_e.TaskAftType]map[int32][]int32) eg_utils.IError {
	if len(delTaskAftTypeMap) == 0 {
		return nil
	}
	for taskAftType,propIdList := range delTaskAftTypeMap {
		for propId,taskTempListId := range propIdList {
			result,err := t.Redis().Get(t.Context(), t.GenCacheKeyString(cid, taskAftType,propId)).Result()
			if err != nil {
				return eg_utils.NewError(err.Error(), nil)
			}
			taskTempIds := model.GetAuthList(result)
			if len(taskTempIds) > 0  {
				for _,tempId := range taskTempListId {
					for i,redisTaskTempId := range taskTempIds {
						if tempId == redisTaskTempId {
							taskTempIds = append(taskTempIds[:i],taskTempIds[i+1:]...)
							break
						}
					}
				}
			}
			if len(taskTempIds) > 0 {
				err := t.Redis().Set(t.Context(), t.GenCacheKeyString(cid, taskAftType,propId),model.GetAuthString(taskTempIds...),0).Err()
				if err != nil {
					return eg_utils.NewError(err.Error(), nil)
				}
			}else {
				err := t.Redis().Del(t.Context(), t.GenCacheKeyString(cid, taskAftType,propId)).Err()
				if err != nil {
					return eg_utils.NewError(err.Error(), nil)
				}
			}

		}
	}
	return nil
}

func (t *taskService) GetTemplateListByTaskType(taskType int32) (taskTsvList []*tsv.TaskTsv){
	taskTsvList = make([]*tsv.TaskTsv,0)
	for _,taskTsv := range t.template.TsvSlice {
		if taskTsv.Type == taskType {
			taskTsvList = append(taskTsvList,taskTsv)
		}
	}
	return taskTsvList
}
