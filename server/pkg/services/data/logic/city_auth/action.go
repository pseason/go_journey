package city_auth

import (
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type ICityAuthService interface {
	common.IBaseService
}

// 消息处理器
func (c *cityAuthService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode){}
}
