package city_auth

import (
	"95eh.com/eg/utils"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/city_e"
)

type cityAuthService struct {
	common.IBaseService
}

func NewCityAuthService() *cityAuthService {
	return &cityAuthService{IBaseService: common.NewBaseService(&model.CityAuth{})}
}

func (c *cityAuthService) AfterInit() error {
	return nil
}

// GetCityAuthByPosition 通过城邦id和城邦身份查询城邦身份权限  query city auth by city id and identity type
func (c *cityAuthService) GetCityAuthByPosition(ac common.IActionCtx, cityId int64, identityType city_e.Identity) (res *model.CityAuth, iErr utils.IError) {
	db := c.Db().Model(&model.CityAuth{})
	var t model.CityAuth
	err := db.Where("city_id = ? and position = ?", cityId, int32(identityType)).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"cityId": cityId, "positionId": int32(identityType)})
		}
	}
	if t.Id > 0 {
		return &t, nil
	}
	return nil, nil
}

// UpCityAuthByPositionIdAndCityId 修改当前身份权限 update current Identity auth
func (c *cityAuthService) UpCityAuthByPositionIdAndCityId(ac common.IActionCtx, authId int32, identityType city_e.Identity, cityId int64) utils.IError {
	//check authId correctness TODO 等待策划考虑城邦身份后添加身份权限表后加入逻辑 wait cityAuth.tsv and cityPosition.tsv table draw up

	cityAuth, iErr := c.GetCityAuthByPosition(ac, cityId, identityType)
	if iErr != nil {
		return iErr
	}

	if cityAuth == nil {
		return utils.NewError(TErrPositionTypeNotFail, utils.M{"identityTypeId": int32(identityType)})
	}
	db := c.Db().Model(&model.CityAuth{})
	err := db.Where("id = ?", cityAuth.CityId).Update("auth_id", authId).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"identityTypeId": int32(identityType)})
	}
	return nil
}

// CheckPlayCityAuth 校验玩家城邦权限 check character city auth
func (c *cityAuthService) CheckPlayCityAuth(ac common.IActionCtx, cityId int64, authId int32, identityType city_e.Identity) (isAuth bool, iErr utils.IError) {
	cityAuth, err := c.GetCityAuthByPosition(ac, cityId, identityType)
	if err != nil {
		return false, err
	}

	if cityAuth == nil {
		return false, utils.NewError(TErrPositionTypeNotFail, utils.M{"authId": authId})
	}
	if cityAuth.AuthId == -1 {
		return false, utils.NewError(TErrPlayerPositionNotAuth, utils.M{"authId": authId})
	}
	if cityAuth.AuthId == 0 || 1&(cityAuth.AuthId>>0) == 1 || 1&(cityAuth.AuthId>>authId) == 1 {
		return true, nil
	}
	return false, nil
}
