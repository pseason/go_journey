package bill

import (
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
)

type billService struct {
	common.IBaseService
}

func NewBillService() *billService {
	return &billService{IBaseService: common.NewBaseService(&model.Bill{})}
}

func (b *billService) AfterInit() error {
	return nil
}

//GetBillCount 获取玩家指定商品订单数 //query character commodity buy order count
func (b *billService) GetBillCount(cid int64, propId int32, startTime, endTime int64, transactionType int32) (count int32, iErr utils.IError) {
	db := b.Db().Model(&model.Bill{})
	bills := make([]model.Bill, 0)
	err := db.Where("prop_id = ? and cid = ? and transaction_type = ? and created between ? and ?",
		propId, cid, transactionType, startTime, endTime).
		Find(&bills).Error
	if err != nil {
		return 0, utils.NewError(err.Error(), utils.M{"cid": cid, "propId": propId, "startTime": startTime, "endTime": endTime})
	}
	for _, v := range bills {
		count += v.Num
	}
	return count, nil
}

//CreateBill 创建订单记录 record currency merchandise
func (b *billService) CreateBill(bill *model.Bill) (iErr utils.IError) {
	db := b.Db().Model(&model.Bill{})
	err := db.Create(bill).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"bill": bill})
	}
	return nil
}
