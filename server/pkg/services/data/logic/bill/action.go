package bill

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type IBillService interface {
	common.IBaseService
}

// 消息处理器
func (b *billService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode){
		msg_dt.CdBillGetPurchasePropCount: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqBillGetPurchasePropCount)
			count, iErr := b.GetBillCount(req.Cid, req.PropId, req.StartTime, req.EndTime, req.TransactionType)
			if iErr != nil {
				app.Log().TError(ac.Tid(), iErr)
				return nil, err_dt.ErrQueryFailed
			}
			return &msg_dt.ResBillGetPurchasePropCount{PurchaseCount: count}, 0
		},
	}
}

func (b *billService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdBillTryCreate: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return &msg_dt.ResBillTryCreate{}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqBillTryCreate)
				iError = b.CreateBill(req.Bill)
				if iError != nil {
					app.Log().TError(ac.Tid(), iError)
					return
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
	}
}
