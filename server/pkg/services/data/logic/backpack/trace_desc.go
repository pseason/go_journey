package backpack

const (
	//背包数据查询失败
	TBackPackDbFindFail = "Failed to query knapsack data"
	//背包数据更新失败
	TBackPackDbUpFail = "Failed to update backpack data"
	//背包数据增加失败
	TBackPackDbSaveFail = "Failed to add knapsack data"
	//背包数据删除失败
	TBackPackDbDelFail = "Failed to delete knapsack data"
	//目标物品不存在
	TBackPackNotFound = "Target item does not exist"
	//无效的背包类型判断
	TBackPackInvalidType = "Invalid knapsack type judgment"
	//添加物品列表为空
	TBackPackAddListIsEmpty = "Add item list is empty"
	//物品数量减扣失败
	TBackPackItemQuantityDeductionFailed = "Item quantity deduction failed"
	//没有目标减扣物品
	TBackPackNoTargetDeductions = "No target deduction items"
	//物品数量不足
	TBackPackNotEnoughItems = "Insufficient quantity of goods"
	//目标物品不存在
	TBackPackTargetPropDoesNotExist = "Target item does not exist"
	//背包没有足够空格
	TBackPackNotEnoughSpace = "There's not enough space"
	//您的背包已满，无法进行此装备操作
	TBackPackFullEquipmentOptFail = "Your backpack is full and cannot perform this equipment operation"
	//出售物品不能为空
	TBackPackSellItemsCannotBeEmpty = "Items for sale cannot be empty"
	//获取物品模板失败
	TBackPackGetTemplateFail = "Failed to get the item template"
	//您的背包已满，无法放入
	TBackPackIsOverIn = "Your backpack is full and cannot be put in"
	//物品无效数值
	TBackPackInvalidValue = "Item invalid value"
	//物品不支持合成
	TBackPackNotSupportCraft = "Items do not support crafting"
	//此物品不能打开
	TBackPackCannotOpen = "This item cannot be opened"
	//无效的选择数据
	TBackPackInvalidSelectData = "Invalid selection data"
	//目标所穿的装备模板不存在
	TBackpackNeedEquipTemplateNotFound = "Item template worn by target does not exist"
	//目标物品还在冻结时间，无法使用
	TBackpackUsePropInEdibleFreezing = "The target item is still frozen in time and cannot be used"
	//出售物品的数量超过范围
	TBackpackSellNumberOverflow = "Number of items for sale exceeds range"
	//设置使用物品的冻结时间失败
	TBackpackSetCdFail = "Failed to set the freeze time for the used item"
)
