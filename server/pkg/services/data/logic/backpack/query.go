package backpack

import (
	"95eh.com/eg/utils"
	"bytes"
	"fmt"
	"gorm.io/gorm"
	"hm/pkg/misc/cache_key"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"sort"
	"strconv"
	"time"
)

const (
	//普通背包最大格子
	MaxDefaultNumber = 50
)

type backpackService struct {
	common.IBaseService
	ptsv  map[int32]*tsv.PropTsv
	pgtsv map[int32]*tsv.PropGiftTsv
}

func NewBackPackService() IBackpackService {
	r := &backpackService{
		IBaseService: common.NewBaseService(&model.Backpack{}),
	}
	return r
}

func (b *backpackService) AfterInit() error {
	b.ptsv = tsv.GetTsvManager(tsv.Prop).(*tsv.PropTsvManager).TsvMap
	b.pgtsv = tsv.GetTsvManager(tsv.PropGift).(*tsv.PropGiftTsvManager).TsvMap
	return nil
}

func (b *backpackService) GetAllPropsByCid(ac common.IActionCtx, Cid int64, t model.BackpackType,WarehouseLevel int32) (res []*model.Backpack, num ,warehouseNum int32, err utils.IError) {
	_where := model.Backpack{Cid: Cid}
	if t != model.BackpackTypeAll {
		_where.Type = t
	}
	db := b.Db().Model(model.Backpack{})
	errDb := db.Where(_where).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	n := b.GetWearHouseMaxSnByLevel(WarehouseLevel)
	warehouseNum = int32(n)
	num = MaxDefaultNumber
	return
}

func (b *backpackService) GetAllCountPropsByCid(ac common.IActionCtx, Cid int64, t model.BackpackType) (res int64, err utils.IError) {
	_where := model.Backpack{Cid: Cid}
	if t != model.BackpackTypeAll {
		_where.Type = t
	}
	db := b.Db().Model(model.Backpack{})
	errDb := db.Where(_where).Count(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	return
}

func (b *backpackService) GetCountItemsByPropIdCid(ac common.IActionCtx, Cid int64, t model.BackpackType, propIds []int32) (res []*model.BackpackNum, err utils.IError) {
	_where := model.Backpack{Cid: Cid}
	if t != model.BackpackTypeAll {
		_where.Type = t
	}
	db := b.Db().Model(model.Backpack{})
	errDb := db.Select("prop_id", "num").Where(_where).Where("prop_id in ? ", propIds).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	return
}

func (b *backpackService) GetPropByIdAndCid(ac common.IActionCtx, Cid int64, id int64, t model.BackpackType) (res *model.Backpack, err utils.IError) {
	errDb := b.Db().Model(&model.Backpack{Cid: Cid, Type: t}).Where("id = ?", id).First(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	return
}

func (b *backpackService) GetPropsByPropIdAndCid(ac common.IActionCtx, Cid int64, propId int32) (res []*model.Backpack, err utils.IError) {
	_where := model.Backpack{Cid: Cid, PropId: propId, Type: model.BackpackTypeDefault}
	errDb := b.Db().Model(model.Backpack{}).Where(_where).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	return
}

func (b *backpackService) IsFullLoad(ac common.IActionCtx, Cid int64, t model.BackpackType) (res bool, err utils.IError) {
	backCount, err := b.GetAllCountPropsByCid(ac, Cid, t)
	if err != nil {
		return
	}
	return backCount >= MaxDefaultNumber, nil
}

func (b *backpackService) GetCountPropsByPropIdCid(ac common.IActionCtx, Cid int64, t model.BackpackType, propId ...int32) (res []*model.BackpackNum, err utils.IError) {
	errDb := b.Db().Model(model.Backpack{}).Select("prop_id, SUM(`num`) as num").
		Where("cid = ? AND type = ? AND prop_id in ?", Cid, t, propId).
		Group("prop_id").Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	return
}

func (b *backpackService) GetPropsByIdsAndCid(ac common.IActionCtx, Cid int64, bids []int64, t model.BackpackType) (res []*model.Backpack, err utils.IError) {
	db := b.Db().Model(&model.Backpack{}).Where("cid = ?",Cid)
	if t == model.BackpackTypeWarehouseCombine {
		db.Where("type in (?)",[]int32{int32(model.BackpackTypeDefault),int32(model.BackpackTypeWarehouse)})
	}else if t != model.BackpackTypeAll{
		db.Where("type = ?",t)
	}
	errDb := db.Where("id in ?", bids).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	return
}

func (b *backpackService) Equips(ac common.IActionCtx, Cid, tid int64) (equips []int32, err utils.IError) {
	bps, _,_, err := b.GetAllPropsByCid(ac, Cid, model.BackpackTypeWear,0)
	if err != nil {
		return
	}
	if bps != nil {
		equips = make([]int32, 0, len(bps))
		for _, bp := range bps {
			equips = append(equips, bp.PropId)
		}
		return
	}
	return
}

func (b *backpackService) GetIdleSn(ac common.IActionCtx, Cid int64, Type model.BackpackType) (res []int32, err utils.IError) {
	ads, _,_, err := b.GetAllPropsByCid(ac, Cid, Type,0)
	if err != nil {
		return
	}
	SnMax := MaxDefaultNumber
	initMap := make(map[int32]int32, SnMax)
	var i int32
	for i = 1; i <= int32(SnMax); i++ {
		initMap[i] = 1
	}
	for _, item := range ads {
		if _, ok := initMap[item.Sn]; ok {
			delete(initMap, item.Sn)
		}
	}
	if len(initMap) > 0 {
		for k := range initMap {
			res = append(res, k)
		}
		sort.Slice(res, func(i, j int) bool { return res[i] < res[j] })
	}
	return
}

func (b *backpackService) ToPropIdGroupSumMap(ac common.IActionCtx, bps []*model.Backpack) (res map[int32]int32) {
	res = make(map[int32]int32)
	for _, v := range bps {
		n, has := res[v.PropId]
		if !has {
			n = v.Num
		} else {
			n += v.Num
		}
		res[v.PropId] = n
	}
	return
}

func (b *backpackService) IsStack(ac common.IActionCtx, propId int32) (res bool, num int32) {
	propTsv, has := b.ptsv[propId]
	if has && propTsv.Stack > 0 {
		return true, propTsv.Stack
	}
	return
}

func (b *backpackService) GetStackNum(ac common.IActionCtx, propId int32) (num int32) {
	stack, n := b.IsStack(ac, propId)
	if !stack {
		return 1
	}
	return n
}

func (b *backpackService) InCD(ac common.IActionCtx, Cid int64, propId int32) (res bool) {
	propTsv, has := b.ptsv[propId]
	if !has {
		return
	}
	cacheKey := cache_key.ShortcutBarFreeze.Format(Cid, propTsv.EdibleType)
	if propTsv.EdibleType > 0 {
		cacheValue, err := b.Redis().Get(b.Context(), cacheKey).Result()
		if err != nil {
			return
		}
		cacheInt, err := strconv.ParseInt(cacheValue, 10, 64)
		if err != nil {
			return
		}
		if cacheInt >= time.Now().Unix() {
			return true
		}
	}
	return
}

func (b *backpackService) AddCD(ac common.IActionCtx, Cid int64, propId int32) (err utils.IError) {
	propTsv, has := b.ptsv[propId]
	if !has {
		return utils.NewError(TBackPackGetTemplateFail,utils.M{"cid":Cid,"propId":propId})
	}
	cacheKey := cache_key.ShortcutBarFreeze.Format(Cid, propTsv.EdibleType)
	if propTsv.EdibleType > 0 {
		errs := b.Redis().Set(b.Context(), cacheKey, time.Now().Unix()+int64(propTsv.EdibleFreezingTime), time.Second*time.Duration(propTsv.EdibleFreezingTime)).Err()
		if errs != nil {
			return utils.NewError(errs.Error(),utils.M{"redis_set_key":cacheKey})
		}
	}
	return
}

//以下会合并至公共model使用
func (b *backpackService) updateManyBpNum(currentDb *gorm.DB, Cid int64, updates []*model.Backpack) (err error) {
	db := currentDb.Model(model.Backpack{})
	sql := bytes.Buffer{}
	sql.WriteString("UPDATE `backpack` SET `num` = ( CASE `id` ")
	updateIds := make([]int64, 0)
	for _, up := range updates {
		sql.WriteString(fmt.Sprintf("WHEN %d THEN %d ", up.Id, up.Num))
		updateIds = append(updateIds, up.Id)
	}
	sql.WriteString("END ), `sn` = ( CASE `id` ")
	for _, up := range updates {
		sql.WriteString(fmt.Sprintf("WHEN %d THEN %d ", up.Id, up.Sn))
	}
	sql.WriteString("END ), ")
	sql.WriteString(b.UpdateManyGenUpdatedColumn(updateIds))
	sql.WriteString("WHERE `cid` = ? AND `id` IN ?")
	sql.WriteString(b.GetLogicDelStr())
	rows := db.Exec(sql.String(), Cid, updateIds).RowsAffected
	if rows <= 0 {
		return
	}
	return
}

func (b *backpackService) updateManyBpType(currentDb *gorm.DB, Cid int64, updates []*model.Backpack) (err error) {
	db := currentDb.Model(model.Backpack{})
	sql := bytes.Buffer{}
	sql.WriteString("UPDATE `backpack` SET `type` = ( CASE `id` ")
	updateIds := make([]int64, 0)
	for _, up := range updates {
		sql.WriteString(fmt.Sprintf("WHEN %d THEN %d ", up.Id, up.Type))
		updateIds = append(updateIds, up.Id)
	}
	sql.WriteString("END ), `sn` = ( CASE `id` ")
	for _, up := range updates {
		sql.WriteString(fmt.Sprintf("WHEN %d THEN %d ", up.Id, up.Sn))
	}
	sql.WriteString("END ), `position` = ( CASE `id` ")
	for _, up := range updates {
		sql.WriteString(fmt.Sprintf("WHEN %d THEN %s ", up.Id, up.Position))
	}
	sql.WriteString("END ), ")
	sql.WriteString(b.UpdateManyGenUpdatedColumn(updateIds))
	sql.WriteString("WHERE `cid` = ? AND `id` IN ?")
	sql.WriteString(b.GetLogicDelStr())
	rows := db.Exec(sql.String(), Cid, updateIds).RowsAffected
	if rows <= 0 {
		return
	}
	return
}

func (b *backpackService) UpdateManyGenUpdatedColumn(ids []int64) string {
	sql := &bytes.Buffer{}
	sql.WriteString("`updated` = ( CASE `id` ")
	t := time.Now().UnixNano() / 1e6
	for _, id := range ids {
		sql.WriteString(fmt.Sprintf("WHEN %d THEN %v ", id, t))
	}
	sql.WriteString("END ) ")
	return sql.String()
}

func (b *backpackService) GetLogicDelStr() string {
	return " AND `deleted` IS NULL"
}
