package backpack

const (
	//货币类
	Currency int32 = 101
	//基础材料类
	BaseMaterial int32 = 301
	//合成材料类
	ComplexMaterial int32 = 302
)

const (
	//恢复玩家属性类道具
	RestorePlayerAttributeProps int32 = iota + 201
	//改变玩家属性类道具
	ChangePlayerAttributeProps
	//buff类
	Addition
	//换道具
	ChangeProps
)

const (
	//装备类-武器
	Weaponry int32 = iota + 401
	//装备类-头部
	HeadGear
	//装备类-胸部
	ChestEquipment
	//装备类-腿部
	LegEquipment
	//装备类-脚部
	FootEquipment
	//装备类-宝物
	TreasureEquipment
	//装备类-戒指
	Ring
	//装备类-手套
	Glove
	//装备类-项链
	Necklace
)

const (
	RestorePlayerAttrProps = iota + 501
)

//是否是装备
func IsEquip(propType int32) bool {
	return propType == Weaponry || propType == HeadGear || propType == ChestEquipment ||
		propType == LegEquipment || propType == FootEquipment ||
		propType == TreasureEquipment || propType == Ring ||
		propType == Glove || propType == Necklace
}
