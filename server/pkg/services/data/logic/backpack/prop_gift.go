package backpack

import (
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
)

func (b *backpackService) PropGiftOpen(ac common.IActionCtx, Cid int64, backpackId int64, selectIndex int32) (res *model.Backpack, err utils.IError) {
	bp, err := b.GetPropByIdAndCid(ac, Cid, backpackId, model.BackpackTypeDefault)
	if err != nil {
		return
	}
	c, has := b.pgtsv[bp.PropId]
	if !has {
		return
	}
	var skills = make(map[int32]int32)
	if len(c.SkillManyOne) > 0 { //技能三选一
		if selectIndex < 0 || selectIndex > int32(len(c.SkillManyOne)-1) {
			return
		}
		b.GiftMapLoadData(skills, map[int32]int32{c.SkillManyOne[selectIndex][0]: c.SkillManyOne[selectIndex][1]})
	} else {
		//整理角色属性数据
	}
	//数据入库
	return
}

func (b *backpackService) GiftMapLoadData(m map[int32]int32, r map[int32]int32) {
	for k, v := range r {
		n, has := m[k]
		if !has {
			n = v
		} else {
			n += v
		}
		m[k] = n
	}
}
