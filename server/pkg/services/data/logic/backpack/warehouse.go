package backpack

import (
	"95eh.com/eg/app"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"sort"
)

func (b *backpackService) GetWearHouseMaxSnByLevel(wearHouseLevel int32) int {
	constWearHouseLevelToSn := map[int32]int{
		0:30,
		1:30,
		2:50,
		3:70,
	}
	return constWearHouseLevelToSn[wearHouseLevel]
}

// 移动(移入仓库判定仓库等级)，回收，整理与背包通用
func (b *backpackService) GetWarehouseIdleSn(ac common.IActionCtx, Cid int64, warehouseLevel int32) (res []int32, err error) {
	ads, _,_, err := b.GetAllPropsByCid(ac, Cid, model.BackpackTypeWarehouse,0)
	if err != nil {
		app.Log().Error(err.Error(), nil)
		return
	}
	SnMax := b.GetWearHouseMaxSnByLevel(warehouseLevel)
	initMap := make(map[int32]int32, SnMax)
	var i int32
	for i = 1; i <= int32(SnMax); i++ {
		initMap[i] = 1
	}
	for _, item := range ads {
		if _, ok := initMap[item.Sn]; ok {
			delete(initMap, item.Sn)
		}
	}
	if len(initMap) > 0 {
		for k := range initMap {
			res = append(res, k)
		}
		sort.Slice(res, func(i, j int) bool { return res[i] < res[j] })
	}
	return
}