package backpack

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)


//背包
type IBackpackService interface {
	common.IBaseService
	//获取指定Cid玩家的所有物品
	GetAllPropsByCid(ac common.IActionCtx, Cid int64, t model.BackpackType,WarehouseLevel int32) (res []*model.Backpack, num ,warehouse int32, err utils.IError)
	//获取指定Cid玩家的所有物品数量
	GetAllCountPropsByCid(ac common.IActionCtx, Cid int64, t model.BackpackType) (res int64, err utils.IError)
	//获取指定Cid玩家的指定物品数量
	GetCountPropsByPropIdCid(ac common.IActionCtx, Cid int64, t model.BackpackType, propId ...int32) (res []*model.BackpackNum, err utils.IError)
	//获取指定主键id和Cid的玩家的单个物品，默认从普通背包获取
	GetPropByIdAndCid(ac common.IActionCtx, Cid int64, id int64, t model.BackpackType) (res *model.Backpack, err utils.IError)
	//根据主键获取玩家物品
	GetPropsByIdsAndCid(ac common.IActionCtx, Cid int64, id []int64, t model.BackpackType) (res []*model.Backpack, err utils.IError)
	//获取指定物品id和Cid的玩家的多个物品，仅从普通背包获取
	GetPropsByPropIdAndCid(ac common.IActionCtx, Cid int64, propId int32) (res []*model.Backpack, err utils.IError)
	//背包是否已满
	IsFullLoad(ac common.IActionCtx, Cid int64, t model.BackpackType) (res bool, err utils.IError)
	//扣除物品-事务(面向服务组调用)
	TrySubtract(ac common.IActionCtx, Cid int64, props map[int32]int32, backpackType model.BackpackType) (upProps,noticeProps []*model.Backpack,deleteIds []int64, err utils.IError)
	//根据主键ID扣除物品-事务(面向服务组调用)
	TrySubtractById(ac common.IActionCtx, Cid int64, props map[int64]int32, backpackType model.BackpackType) (upProps,notices []*model.Backpack,deleteIds []int64, err utils.IError)
	//获得物品(面向服务组调用)
	TryIncrease(ac common.IActionCtx, Cid int64, props map[int32]int32, isFullSendMail bool) (overProps map[int32]int32,upProps,addProps,noticeProps []*model.Backpack,err utils.IError)
	//整理物品
	Arrange(ac common.IActionCtx, Cid int64, backpackType model.BackpackType) (err utils.IError)
	//丢弃物品
	Discard(ac common.IActionCtx, Cid int64, bid int64) (err utils.IError)
	//拆分物品
	Division(ac common.IActionCtx, Cid int64, bid int64, num int32) (err utils.IError)
	//背包使用物品-穿戴装备
	UseProp(ac common.IActionCtx, Cid int64, bid int64) (res *model.Backpack, Type int32, err utils.IError)
	// 玩家穿戴列表
	Equips(ac common.IActionCtx, Cid, tid int64) (equips []int32, err utils.IError)
	//移动物品
	Move(ac common.IActionCtx, Cid int64, bid int64, warehouseLevel int32) (err utils.IError)
	//获取空闲的位置编号
	GetIdleSn(ac common.IActionCtx, Cid int64, Type model.BackpackType) (res []int32, err utils.IError)
	// 生成以PropId为Key，数量总和为值的map
	ToPropIdGroupSumMap(ac common.IActionCtx, bps []*model.Backpack) (res map[int32]int32)
	//打开礼包道具
	PropGiftOpen(ac common.IActionCtx, Cid int64, backpackId int64, selectIndex int32) (res *model.Backpack, err utils.IError)
	//是否可以堆叠
	IsStack(ac common.IActionCtx, propId int32) (res bool, num int32)
	//获取堆叠数量
	GetStackNum(ac common.IActionCtx, propId int32) (num int32)
	//判断此物品类型是否在CD时间内(propTsv可为nil)
	InCD(ac common.IActionCtx, Cid int64, propId int32) (res bool)
	//增加CD(ac common.IActionCtx,propTsv可为nil)
	AddCD(ac common.IActionCtx, Cid int64, propId int32) (err utils.IError)
	//金币兑换
	GoldExchange(ac common.IActionCtx, Cid int64, AttrId int64, Num int32) (err utils.IError)
	//批量穿戴
	BatchEquip(ac common.IActionCtx, Cid int64, Props []int32) (err utils.IError)
}

func (b *backpackService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		msg_dt.CdBackpackList: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackList)
			ds, num,warehouseNum, err := b.GetAllPropsByCid(ac, req.Cid, req.BackpackType,req.WarehouseLevel)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return msg_dt.ResBackpackList{
				BackpackList: ds,
				Num:          num,
				WarehouseNum: warehouseNum,
			}, 0
		},
		msg_dt.CdBackpackGetPropsByIds: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackGetPropsByIds)
			res, err := b.GetPropsByIdsAndCid(ac, req.Cid, req.Bids, req.Type)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return msg_dt.ResBackpackGetPropsByIds{
				Props:res,
			}, 0
		},

		msg_dt.CdBackpackDiscard: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackDiscard)
			err := b.Discard(ac, req.Cid, req.Bid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackDiscard{}, 0
		},

		msg_dt.CdBackpackArrange: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackArrange)
			err := b.Arrange(ac, req.Cid, req.Type)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return nil, 0
		},

		msg_dt.CdBackpackDivision: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackDivision)
			err := b.Division(ac, req.Cid, req.Bid, req.SplitNum)
			if err != nil {
				app.Log().Debug(err.Error(),nil)
				return nil, err_dt.ErrCreateFailed
			}
			return nil, 0
		},

		msg_dt.CdBackpackUseProp: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackUseProp)
			res, t, err := b.UseProp(ac, req.Cid, req.Bid)
			if err != nil {
				app.Log().Debug(err.Error(),nil)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackUseProp{
				Data: res,
				Type: t,
			}, 0
		},

		msg_dt.CdBackpackMove: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackMove)
			err := b.Move(ac, req.Cid, req.Bid, req.WarehouseLevel)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return nil, 0
		},

		msg_dt.CdBackpackGiftOpen: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackGiftOpen)
			_, err := b.PropGiftOpen(ac, req.Cid, req.Bid, req.SelectIndex)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackGiftOpen{}, 0
		},

		msg_dt.CdBackpackGoldExchange: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackGoldExchange)
			err := b.GoldExchange(ac, req.Cid, req.AttrId, req.Num)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackGoldExchange{}, 0
		},

		//服务组调用-通过propId查询物品
		msg_dt.CdBackpackGetProps: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackGetProps)
			res, err := b.GetCountItemsByPropIdCid(ac, req.Cid, model.BackpackTypeDefault, req.PropIds)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackGetProps{
				Props: res,
			}, 0
		},

		//通过主键查询背包单个物品
		msg_dt.CdBackpackGetPropById: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackGetPropById)
			res, err := b.GetPropByIdAndCid(ac, req.Cid, req.Bid, model.BackpackTypeDefault)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackGetPropById{
				Props: res,
			}, 0
		},

		//通过propID查询背包物品
		msg_dt.CdBackpackGetPropsByPropId: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackGetPropsByPropId)
			res, err := b.GetPropsByPropIdAndCid(ac, req.Cid, req.PropId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackGetPropsByPropId{
				Props: res,
			}, 0
		},

		msg_dt.CdBackpackBatchEquip: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackBatchEquip)
			err := b.BatchEquip(ac, req.Cid, req.Props)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackBatchEquip{
			}, 0
		},

		msg_dt.CdBackpackIsFullLoad: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqBackpackIsFullLoad)
			isFull,err := b.IsFullLoad(ac, req.Cid, req.PropType)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResBackpackIsFullLoad{
				IsFull:isFull,
			}, 0
		},
	}
}

func (b *backpackService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdBackpackSubtract: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqBackpackSubtract)
				ups,noticeProps,deleteIds,err := b.TrySubtract(ac, req.Cid, req.Props, req.PropType)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					resErrCode = err_dt.ErrQueryFailed
					return
				}
				resBody = &msg_dt.ResBackpackSubtract{
					UpProps:ups,
					NoticePropIds:noticeProps,
					DeleteIds:deleteIds,
					Cid:req.Cid,
				}
				resErrCode = 0
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				req := resBody.(*msg_dt.ResBackpackSubtract)
				IError := b.ConfirmSubtract(ac, req.Cid, req.UpProps,req.NoticePropIds,req.DeleteIds)
				if IError != nil {
					app.Log().TError(ac.Tid(), IError)
					return IError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError{
				return nil
			},
		},
		msg_dt.CdBackpackSubtractById: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqBackpackSubtractById)
				ups,notices,deleteIds,err := b.TrySubtractById(ac, req.Cid, req.Props, req.PropType)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					resErrCode = err_dt.ErrQueryFailed
					return
				}
				resBody = &msg_dt.ResBackpackSubtractById{
					UpProps:ups,
					NoticeProps:notices,
					DeleteIds:deleteIds,
					Cid:req.Cid,
				}
				resErrCode = 0
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				req := resBody.(*msg_dt.ResBackpackSubtractById)
				IError := b.ConfirmSubtractById(ac, req.Cid, req.UpProps,req.NoticeProps,req.DeleteIds)
				if IError != nil {
					app.Log().TError(ac.Tid(), IError)
					return IError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError{
				return nil
			},
		},
		msg_dt.CdBackpackIncrease:{
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqBackpackIncrease)
				overProps,upProps,addProps,noticeProps,err := b.TryIncrease(ac, req.Cid, req.Props, req.IsFullSendMail)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					resErrCode = err_dt.ErrQueryFailed
					return
				}
				resBody = &msg_dt.ResBackpackIncrease{
					OverProps:overProps,
					UpProps:upProps,
					AddProps:addProps,
					NoticeProps:noticeProps,
					Cid:req.Cid,
				}
				resErrCode = 0
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				req := resBody.(*msg_dt.ResBackpackIncrease)
				_req := reqBody.(*msg_dt.ReqBackpackIncrease)
				IError := b.ConfirmIncrease(ac, req.Cid, _req.Props, req.UpProps, req.AddProps, req.NoticeProps)
				if IError != nil {
					app.Log().TError(ac.Tid(), IError)
					return IError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, resBody interface{}) utils.IError {
				return nil
			},
		},
	}
}
