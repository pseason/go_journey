package backpack

import (
	"95eh.com/eg/utils"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"strconv"
)

func (b *backpackService) UseProp(ac common.IActionCtx, Cid int64, bid int64) (useBp *model.Backpack, Type int32, err utils.IError) {
	useBp, trace := b.GetPropByIdAndCid(ac, Cid, bid, model.BackpackTypeDefault)
	if trace != nil {
		err = utils.NewError(trace.Error(),nil)
		return
	}
	if useBp.Id == 0 {
		useBp, trace = b.GetPropByIdAndCid(ac, Cid, bid, model.BackpackTypeWear)
		if trace != nil {
			err = utils.NewError(trace.Error(),nil)
			return
		}
	}
	if useBp.Id == 0 {
		err = utils.NewError(TBackPackNotFound,nil)
		return
	}
	propTsv, has := b.ptsv[useBp.PropId]
	if !has {
		err = utils.NewError(TBackPackGetTemplateFail,nil)
		return
	}
	if b.InCD(ac, Cid, useBp.PropId) {
		err = utils.NewError(TBackpackUsePropInEdibleFreezing,nil)
		return
	}
	Type = propTsv.Type
	if IsEquip(propTsv.Type) {
		trace = b._usePropEquip(ac, Cid, useBp, propTsv)
		if trace == nil {
			err = b.AddCD(ac, Cid, useBp.PropId)
			if err != nil{
				return
			}
		}
		return
	}
	return
}

//使用装备
func (b *backpackService) _usePropEquip(ac common.IActionCtx, Cid int64, useBp *model.Backpack, propCsv *tsv.PropTsv) (err utils.IError) {
	// 查看当前同位置穿戴的装备
	var wearBackpack model.Backpack
	res := b.Db().Model(model.Backpack{}).Where("cid = ? AND type = ? AND position = ?", Cid, model.BackpackTypeWear, strconv.Itoa(int(propCsv.Type))).First(&wearBackpack)
	if res.Error != nil && !errors.Is(res.Error, gorm.ErrRecordNotFound) {
		err = utils.NewError(res.Error.Error(),nil)
		return
	}
	//直接穿戴
	useBp.Type = model.BackpackTypeWear
	if res.RowsAffected <= 0 {
		errDb := b.Db().Model(&model.Backpack{}).Where("id= ?", useBp.Id).Save(&useBp).Error
		if errDb != nil {
			return utils.NewError(errDb.Error(),nil)
		}
	} else {
		//检查背包格子是否多一个
		count, errDb := b.GetIdleSn(ac, Cid, model.BackpackTypeDefault)
		if errDb != nil {
			return utils.NewError(errDb.Error(),nil)
		}
		if len(count) <= 0 {
			err = utils.NewError(TBackPackNotEnoughSpace,nil)
			return
		}

		useBp.Type = model.BackpackTypeDefault
		useBp.Sn = count[0]
		errTx := b.LocalChainTx(ac, func(sAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
			db := sAc.TxDb().Model(model.Backpack{})
			errDb := db.Where("id= ?", useBp.Id).Save(&useBp).Error
			if errDb != nil {
				sErr = utils.NewError(errDb.Error(),nil)
				return
			}
			return
		})
		if errTx != nil {
			err = utils.NewError(errTx.Error(),nil)
			return
		}
	}
	ups := []*model.Backpack{
		useBp,
	}
	b.ChangeNotify(ac, Cid, ups)
	return
}

//批量穿戴
func (b *backpackService) BatchEquip(ac common.IActionCtx, Cid int64, Props []int32) (err utils.IError) {
	//过滤数据
	if len(Props) <0 || len(Props)>10{
		err = utils.NewError(TBackPackInvalidValue,nil)
		return
	}
	filterParam := func(filterSlice []int32,propCsv map[int32]*tsv.PropTsv)(mapRes map[string]int32){
		TypeToPropRes := make(map[string]int32, 0)
		temp := map[int32]struct{}{}
		for _, item := range filterSlice {
			if _, ok := temp[item]; !ok && item >0 && IsEquip(propCsv[item].Type) {
				PropAttr,has := propCsv[item]
				if !has {
					continue
				}
				if IsEquip(PropAttr.Type){
					temp[item] = struct{}{}
					PropAttrStr :=strconv.Itoa(int(PropAttr.Type))
					TypeToPropRes[PropAttrStr] = item
				}
			}
		}
		return TypeToPropRes
	}
	filterTypeToProp := filterParam(Props,b.ptsv)
	if len(filterTypeToProp) <=0 {
		err = utils.NewError(TBackPackInvalidValue,nil)
		return
	}

	//查询角色穿戴物品
	IsWearRes,_,_,err :=b.GetAllPropsByCid(ac,Cid,model.BackpackTypeWear,0)
	if err != nil{
		return
	}
	filterProps := make([]int32,0)
	if len(IsWearRes) > 0{
		for _,wearProp:= range IsWearRes{
			if _,ok := filterTypeToProp[wearProp.Position]; ok{
				delete(filterTypeToProp,wearProp.Position)
				filterProps = append(filterProps,wearProp.PropId)
			}
		}
	}

	//查询背包物品
	if len(filterProps) <=0 {
		err = utils.NewError("wear backpack data is invalid",nil)
		return
	}
	res := make([]*model.Backpack,0)
	errDb :=b.Db().Model(&model.Backpack{}).Where("cid = ? AND type = ? AND prop_id in ?", Cid,model.BackpackTypeDefault,filterProps).Find(&res).Error
	if errDb != nil{
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	if len(res) <=0{
		err = utils.NewError("backpack data is empty",nil)
		return
	}
	upIds := make([]int64,0)
	for _,prop := range res{
		prop.Type = model.BackpackTypeWear
		upIds = append(upIds,prop.Id)
	}
	//更新
	errDb = b.updateManyBpType(b.Db(),Cid,res)
	if errDb != nil{
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	b.ChangeNotify(ac, Cid, res)
	return
}
