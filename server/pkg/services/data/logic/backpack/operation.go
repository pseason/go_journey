package backpack

import (
	"95eh.com/eg/app"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"hm/pkg/misc"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"sort"
	"strconv"
)

func (b *backpackService) Arrange(ac common.IActionCtx, Cid int64, backpackType model.BackpackType) (err utils.IError) {
	//获取装备
	ds, _,_, err := b.GetAllPropsByCid(ac, Cid, backpackType,0)
	if err != nil {
		return
	}
	//同物品数量叠加
	mpds := make(map[int32]*model.Backpack)
	data := make([]*model.Backpack, 0)
	for _, items := range ds {
		_, ok := mpds[items.PropId]
		if ok {
			mpds[items.PropId].Num += items.Num
		} else {
			mpds[items.PropId] = items
		}
	}

	//按最大叠加数量拆分
	for _, items := range mpds {
		//获取物品最大叠加数
		_, ok := b.ptsv[items.PropId]
		if !ok {
			err = utils.NewError(TBackPackGetTemplateFail, nil)
			return
		}
		isStack, maxNumStack := b.IsStack(ac, items.PropId)
		app.Log().Debug("Arrange is",utils.M{"isStack":isStack,"maxNumStack":maxNumStack})
		//按最大值拆分
		if isStack && items.Num > maxNumStack {
			over := items.Num % maxNumStack
			repeat := (items.Num - over) / maxNumStack
			var i int32
			for i = 0; i < repeat; i++ {
				items.Num = maxNumStack
				data = append(data, &model.Backpack{
					Num:      items.Num,
					Cid:      items.Cid,
					Type:     items.Type,
					Position: items.Position,
					Sn:       items.Sn,
					PropId:   items.PropId,
				})
			}
			if over > 0 {
				items.Num = over
				data = append(data, &model.Backpack{
					Num:      items.Num,
					Cid:      items.Cid,
					Type:     items.Type,
					Position: items.Position,
					Sn:       items.Sn,
					PropId:   items.PropId,
				})
			}
		} else {
			if !isStack {
				for j := 0; j < int(items.Num); j++ {
					data = append(data, &model.Backpack{
						Num:      1,
						Cid:      items.Cid,
						Type:     items.Type,
						Position: items.Position,
						Sn:       items.Sn,
						PropId:   items.PropId,
					})
				}
			}else{
				data = append(data, &model.Backpack{
					Num:      items.Num,
					Cid:      items.Cid,
					Type:     items.Type,
					Position: items.Position,
					Sn:       items.Sn,
					PropId:   items.PropId,
				})
			}

		}
	}
	//没有需要整理的
	if len(data) <= 0 {
		return
	}

	//多重排序
	sort.Slice(data, func(i, j int) bool {
		//优先类型正序排序
		if data[i].Type != data[j].Type {
			return data[i].Type < data[j].Type
		}
		//再进行itemId正序排序
		if data[i].PropId != data[j].PropId {
			return data[i].PropId < data[j].PropId
		}
		//再进行数量倒序排序
		return data[i].Num > data[j].Num
	})

	//重置格子排序
	var p int32 = 1
	for _, item := range data {
		item.Sn = p
		p++
	}

	//删除原有装备并增加最新排好序的物品
	errTx := b.LocalChainTx(ac, func(sAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
		db := sAc.TxDb().Model(model.Backpack{})
		errDb := db.Where(&model.Backpack{Cid: Cid, Type: backpackType}).Delete(&model.Backpack{}).Error
		if errDb != nil {
			sErr = utils.NewError(errDb.Error(), nil)
			return
		}
		errDb = db.Create(&data).Error
		if errDb != nil {
			sErr = utils.NewError(errDb.Error(), nil)
			return
		}
		return
	})
	if errTx != nil {
		return utils.NewError(errTx.Error(), nil)
	}
	//组合要清理的Id
	for _, item := range ds {
		item.Num = 0
		data = append(data, item)
	}
	b.ChangeNotify(ac, Cid, data)
	return
}

func (b *backpackService) Division(ac common.IActionCtx, Cid int64, bid int64, num int32) (err utils.IError) {
	ds, err := b.GetPropByIdAndCid(ac, Cid, bid, model.BackpackTypeDefault)
	if err != nil {
		return
	}

	if ds.Num <= num {
		return utils.NewError(TBackPackNotEnoughItems, nil)
	}

	//查找空格
	initMap, err := b.GetIdleSn(ac, Cid, model.BackpackTypeDefault)
	if err != nil {
		return
	}
	if len(initMap) <= 0 {
		return utils.NewError(TBackPackNotEnoughSpace, nil)
	}
	ds.Num -= num
	addBp := &model.Backpack{Cid: ds.Cid, Type: ds.Type, PropId: ds.PropId, Num: num, Position: ds.Position, Sn: initMap[0]}

	errTx := b.LocalChainTx(ac, func(sAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
		db := sAc.TxDb().Model(&model.Backpack{})
		errDb := b.Db().Model(&model.Backpack{}).Where("id = ?", ds.Id).Update("num", ds.Num).Error
		if errDb != nil {
			sErr = utils.NewError(errDb.Error(), nil)
			return
		}
		_ = addBp.BeforeCreate(db)
		errDb = db.Create(&addBp).Error
		if errDb != nil {
			sErr = utils.NewError(errDb.Error(), nil)
			return
		}
		return
	})
	if errTx != nil {
		return utils.NewError(errTx.Error(), nil)
	}
	Ups := []*model.Backpack{
		ds,
		addBp,
	}
	b.ChangeNotify(ac, Cid, Ups)
	return
}

func (b *backpackService) queryMapPropIds(ac common.IActionCtx, Cid int64, props map[int32]int32, backpackType model.BackpackType) (res []*model.Backpack, err utils.IError) {
	propSlice := make([]int32, 0)
	for propIds := range props {
		propSlice = append(propSlice, propIds)
	}
	//查询物品数据
	db := b.Db().Model(&model.Backpack{}).Where("cid = ?", Cid)
	if backpackType == model.BackpackTypeWarehouseCombine {
		db.Where("type in (?)", []int32{int32(model.BackpackTypeDefault), int32(model.BackpackTypeWarehouse)})
	} else if backpackType != model.BackpackTypeAll {
		db.Where("type = ?", backpackType)
	}
	errDb := db.Where("prop_id in ?", propSlice).Find(&res).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}
	return
}

func (b *backpackService) Discard(ac common.IActionCtx, Cid int64, bid int64) (err utils.IError) {
	useBp, trace := b.GetPropByIdAndCid(ac, Cid, bid, model.BackpackTypeDefault)
	if trace != nil {
		return
	}
	if useBp.Id <= 0 {
		return
	}
	errDb := b.Db().Model(&model.Backpack{Cid: Cid, Type: model.BackpackTypeDefault}).Delete(&model.Backpack{}, bid).Error
	if errDb != nil {
		return utils.NewError(errDb.Error(), nil)
	}

	useBp.Num = 0
	Des := []*model.Backpack{
		useBp,
	}
	b.ChangeNotify(ac, Cid, Des)
	return
}

func (b *backpackService) TrySubtractById(ac common.IActionCtx, Cid int64, props map[int64]int32, backpackType model.BackpackType) (updates,notices []*model.Backpack,removeIds []int64, err utils.IError) {
	bids := make([]int64, 0)
	for bid := range props {
		bids = append(bids, bid)
	}
	res := make([]*model.Backpack, 0)
	//查询物品数据
	res, errDb := b.GetPropsByIdsAndCid(ac, Cid, bids, backpackType)
	if errDb != nil {
		err = utils.NewError(errDb.Error(), nil)
		return
	}

	for _, bp := range res {
		sellNumber := props[bp.Id]
		if sellNumber > bp.Num {
			err = utils.NewError(TBackPackNotEnoughItems, nil)
			return
		}
		if sellNumber == bp.Num {
			bp.Num = 0
			notices = append(notices, bp)
			removeIds = append(removeIds, bp.Id)
		} else {
			bp.Num -= sellNumber
			updates = append(updates, bp)
			notices = append(notices, bp)
		}
	}
	return
}

func (b *backpackService) ConfirmSubtractById(ac common.IActionCtx, Cid int64, updateProps,noticeProps []*model.Backpack,deleteIds []int64) (err utils.IError) {
	errTx := b.LocalChainTx(ac, func(sAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
		if len(updateProps) > 0 {
			errDb := b.updateManyBpNum(sAc.TxDb(), Cid, updateProps)
			if errDb != nil {
				sErr = utils.NewError(errDb.Error(), nil)
				return
			}
		}
		if len(deleteIds) > 0 {
			errDbDel := b.Db().Model(&model.Backpack{Cid: Cid}).Delete(&model.Backpack{}, deleteIds).Error
			if errDbDel != nil {
				sErr = utils.NewError(errDbDel.Error(), nil)
				return
			}
		}
		return
	})
	if errTx != nil {
		err = utils.NewError(errTx.Error(), nil)
		return
	}
	b.ChangeNotify(ac, Cid, noticeProps)
	return
}

func (b *backpackService) ConfirmSubtract(ac common.IActionCtx, Cid int64, updateProps, noticeProps []*model.Backpack, deleteIds []int64) (err utils.IError) {
	errTx := b.LocalChainTx(ac, func(sAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
		if len(updateProps) > 0 {
			errDb := b.updateManyBpNum(sAc.TxDb(), Cid, updateProps)
			if errDb != nil {
				sErr = utils.NewError(errDb.Error(), nil)
				return
			}
		}
		if len(deleteIds) > 0 {
			errDbDel := b.Db().Model(&model.Backpack{Cid: Cid}).Delete(&model.Backpack{}, deleteIds).Error
			if errDbDel != nil {
				sErr = utils.NewError(errDbDel.Error(), nil)
				return
			}
		}
		return
	})
	if errTx != nil {
		err = utils.NewError(errTx.Error(), nil)
		return
	}
	b.ChangeNotify(ac, Cid, noticeProps)
	return
}

func (b *backpackService) TrySubtract(ac common.IActionCtx, Cid int64, props map[int32]int32, backpackType model.BackpackType) (upProps, noticeProps []*model.Backpack, deleteIds []int64, err utils.IError) {
	//检查数据合理性
	for pId, Num := range props {
		if _, has := b.ptsv[pId]; !has {
			err = utils.NewError(TBackPackGetTemplateFail, nil)
			return
		}
		if Num <= 0 {
			err = utils.NewError(TBackPackInvalidValue, nil)
			return
		}
	}
	//查询数据
	noticeProps, err = b.queryMapPropIds(ac, Cid, props, backpackType)
	if err != nil {
		return
	}
	deleteIds = make([]int64, 0)
	upProps = make([]*model.Backpack, 0)

	for i, v := range noticeProps {
		proId := v.PropId

		if v.Num <= props[proId] {
			props[proId] -= v.Num

			noticeProps[i].Num = 0
			deleteIds = append(deleteIds, v.Id)
			//upProps = append(upProps, bps[i])
		} else {
			noticeProps[i].Num -= props[proId]
			props[proId] = 0
			upProps = append(upProps, noticeProps[i])
		}
	}
	for _, Num := range props {
		if Num > 0 {
			err = utils.NewError(TBackPackNotEnoughItems, nil)
			return
		}
	}
	log.Debug("背包扣减数值", utils.M{"notice": noticeProps, "upProps": upProps, "deleteIds": deleteIds, "err": err})
	return
}

func (b *backpackService) TryIncrease(ac common.IActionCtx, Cid int64, props map[int32]int32, isFullSendMail bool) (overProps map[int32]int32, upProps, addProps, noticeProps []*model.Backpack, err utils.IError) {
	//查询背包格子是否足够，必须1种材料合成需要1个空格
	gs, err := b.GetIdleSn(ac, Cid, model.BackpackTypeDefault)
	if err != nil {
		return
	}
	//初略检测物品占据格子数是否满足
	overProps = make(map[int32]int32,0)
	if len(gs) < len(props) {
		if isFullSendMail {
			overProps = props
		}else{
			//格子数不足
			err = utils.NewError(TBackPackNotEnoughSpace, nil)
		}
		return
	}
	var totalGs int32 = 0
	ups := make([]*model.Backpack, 0)
	//查询背包是否有同样物品可以堆叠不占格子数
	res, err := b.queryMapPropIds(ac, Cid, props, model.BackpackTypeDefault)
	if len(res) > 0 {
		for _, its := range res {
			//获取合成的材料叠套最大值所需的格子数
			p, has := b.ptsv[its.PropId]
			if !has {
				//道具不存在
				err = utils.NewError(TBackPackTargetPropDoesNotExist, nil)
				return
			}
			//过滤堆叠上限的物品
			if p.Stack == 0 || (p.Stack > 0 && its.Num >= p.Stack) {
				continue
			}
			if props[its.PropId] <= 0 {
				continue
			}
			sumNums := its.Num + props[its.PropId]
			if p.Stack > 0 && sumNums > p.Stack {
				v := p.Stack - its.Num
				props[its.PropId] -= v
				its.Num = p.Stack
				ups = append(ups, its)
			} else {
				its.Num = sumNums
				props[its.PropId] = 0
				ups = append(ups, its)
			}
		}
	}
	//剩余数量-检测道具堆叠数所需的格子数是否满足
	data := make([]*model.Backpack, 0)
	for pId, n := range props {
		if n <= 0 {
			continue
		}
		//获取合成的材料叠套最大值所需的格子数
		p, has := b.ptsv[int32(pId)]
		if !has {
			//道具不存在
			err = utils.NewError(TBackPackTargetPropDoesNotExist, nil)
			return
		}
		//根据最大堆叠拆分
		if p.Stack > 0 && p.Stack < n {
			over := n % p.Stack
			repeat := (n - over) / p.Stack
			totalGs += repeat
			var i int32
			for i = 0; i < repeat; i++ {
				data = append(data, &model.Backpack{
					Num:      p.Stack,
					Cid:      Cid,
					Type:     model.BackpackTypeDefault,
					Position: strconv.Itoa(int(p.Type)),
					PropId:   int32(pId),
				})
			}
			if over > 0 {
				totalGs += 1
				data = append(data, &model.Backpack{
					Num:      over,
					Cid:      Cid,
					Type:     model.BackpackTypeDefault,
					Position: strconv.Itoa(int(p.Type)),
					PropId:   int32(pId),
				})
			}
		} else {
			if p.Stack == 0 {
				totalGs += n
				for j := 0; j < int(n); j++ {
					data = append(data, &model.Backpack{
						Num:      1,
						Cid:      Cid,
						Type:     model.BackpackTypeDefault,
						Position: strconv.Itoa(int(p.Type)),
						PropId:   int32(pId),
					})
				}
			}else{
				totalGs += 1
				data = append(data, &model.Backpack{
					Num:      n,
					Cid:      Cid,
					Type:     model.BackpackTypeDefault,
					Position: strconv.Itoa(int(p.Type)),
					PropId:   int32(pId),
				})
			}
		}
	}
	if len(gs) < int(totalGs) && !isFullSendMail {
		//格子数不足
		err = utils.NewError(TBackPackNotEnoughSpace, nil)
		return
	}
	if len(data) <= 0 && len(ups) <= 0 {
		//组合数据失败
		err = utils.NewError(TBackPackNoTargetDeductions, nil)
		return
	}
	//赋予剩余格子编号
	upProps = ups
	addProps = make([]*model.Backpack, 0)
	for _, item := range data {
		if len(gs) > 0 {
			item.Sn = gs[0]
			addProps = append(addProps, item)
			gs = gs[1:]
		} else {
			if _,has := overProps[item.PropId];!has{
				overProps[item.PropId] = item.Num
			}else{
				overProps[item.PropId] += item.Num
			}
		}
	}
	noticeProps = ups
	log.Debug("背包增加数值", utils.M{"notice": noticeProps, "upProps": upProps, "addProps": addProps, "err": err})
	return
}

func (b *backpackService) ConfirmIncrease(ac common.IActionCtx, Cid int64, props map[int32]int32, upProps, addProps, noticeProps []*model.Backpack) (err utils.IError) {
	errTx := b.LocalChainTx(ac, func(sAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
		db := sAc.TxDb().Model(model.Backpack{})
		if len(upProps) > 0 {
			errDb := b.updateManyBpNum(sAc.TxDb(), Cid, upProps)
			if errDb != nil {
				sErr = utils.NewError(errDb.Error(), nil)
				return
			}
		}
		if len(addProps) > 0 {
			errDbAdd := db.Create(addProps).Error
			if errDbAdd != nil {
				sErr = utils.NewError(errDbAdd.Error(), nil)
				return
			}
		}
		return
	})
	if errTx != nil {
		err = utils.NewError(errTx.Error(), nil)
		return
	}
	for _, v := range addProps {
		noticeProps = append(noticeProps, v)
	}
	b.ChangeNotify(ac, Cid, noticeProps)
	//获得物品飘字
	b.DispatchEvent(misc.NodeGame, ac.Tid(), Cid, msg_dt.CdEveBackpackSysAddNotify, msg_dt.EveBackpackSysAddNotify{
		Cid:  Cid,
		Data: props,
	})
	return
}

func (b *backpackService) Move(ac common.IActionCtx, Cid int64, bid int64, warehouseLevel int32) (err utils.IError) {
	var useBp *model.Backpack
	errDb := b.Db().Model(model.Backpack{}).Where("id = ?", bid).First(&useBp).Error
	if errDb != nil {
		return utils.NewError(errDb.Error(), nil)
	}

	var toType model.BackpackType
	snSlice := make([]int32, 0)
	if useBp.Type == model.BackpackTypeDefault {
		//判断仓库等级
		snSlice, errDb = b.GetWarehouseIdleSn(ac, Cid, warehouseLevel)
		if errDb != nil {
			return utils.NewError(errDb.Error(), nil)
		}
		if len(snSlice) <= 0 {
			return utils.NewError(TBackPackFullEquipmentOptFail, nil)
		}
		toType = model.BackpackTypeWarehouse

	} else {
		//是否有空格
		snSlice, err = b.GetIdleSn(ac, Cid, toType)
		if err != nil {
			return
		}
		if len(snSlice) <= 0 {
			return utils.NewError(TBackPackFullEquipmentOptFail, nil)
		}
		toType = model.BackpackTypeDefault
	}
	useBp.Type = toType
	useBp.Sn = snSlice[0]
	errDb = b.Db().Model(&model.Backpack{}).Where("id = ?", bid).Save(useBp).Error
	if errDb != nil {
		return utils.NewError(errDb.Error(), nil)
	}

	Des := []*model.Backpack{
		useBp,
	}
	b.ChangeNotify(ac, Cid, Des)
	return
}

func (b *backpackService) ChangeNotify(ac common.IActionCtx, Cid int64, data []*model.Backpack) {
	b.DispatchEvent(misc.NodeGame, ac.Tid(), Cid, msg_dt.CdEveBackpackGainNotify, msg_dt.EveBackpackGainNotify{
		Cid:  Cid,
		Data: data,
	})
}

func (b *backpackService) GoldExchange(ac common.IActionCtx, Cid int64, AttrId int64, Num int32) (err utils.IError) {
	return
}
