package event

import (
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
)

type eventService struct {
	common.IBaseService
}

func NewEventService() *eventService {
	return &eventService{IBaseService: common.NewBaseService(&model.Event{})}
}

func (e *eventService) AfterInit() error {

	return nil
}

//CreateEvent 创建事件 create event
func (e *eventService) CreateEvent(event ...*model.Event) utils.IError {
	db := e.Db().Model(&model.Event{})
	err := db.Create(event).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"err": err.Error()})
	}
	return nil
}

//查询城邦事件
func (e *eventService) GetCityEvent(cityId int64, eventType, page, size int32) (res []*model.Event, total int64, iErr utils.IError) {
	db := e.Db().Model(&model.Event{})
	err := db.Where("city_id = ? And event_type = ? And kingdom_id = 0", cityId, eventType).Count(&total).Error
	if err != nil {
		return res, total, utils.NewError(err.Error(), utils.M{"cityId": cityId, "eventType": eventType})
	}
	if page > int32(total/5) {
		return res, total, nil
	}
	err = db.Order("created desc ").Offset(int(page * size)).Limit(int(size)).Find(&res).Error
	if err != nil {
		return res, total, utils.NewError(err.Error(), utils.M{"cityId": cityId, "eventType": eventType})
	}
	return res, total, nil
}

func (e *eventService) CityEventManager(cityId int64, page, size, eventType int32) (res []*model.Event, total int64, iErr utils.IError) {
	res, total, iErr = e.GetCityEvent(cityId, eventType, page, size)
	if iErr != nil {
		return nil, total, iErr
	}
	return res, total, nil
}
