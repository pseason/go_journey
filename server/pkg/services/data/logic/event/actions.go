package event

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type IEventService interface {
	common.IBaseService
}

func (e *eventService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		msg_dt.CdEventPageGetCityEvent: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqEventPageGetCityEvent)
			events, total, err := e.GetCityEvent(req.CityId, req.EventType, req.Page, req.Size)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResEventPageGetCityEvent{Events: events, Total: int32(total)}, 0
		},
	}
}

func (e *eventService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdEventCreateEvent: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return nil, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqEventCreateEvent)
				err := e.CreateEvent(req.Event...)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return err
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
	}
}
