package city_building

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"encoding/json"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	build_e "hm/pkg/services/data/model/enum/building_e"
	"hm/pkg/services/data/msg_dt"
)

type cityBuildingService struct {
	common.IBaseService
	cityLock          intfc.IMTxProducer
	buildingTemplates *tsv.BuildingAttributeTsvManager
}

func (b *cityBuildingService) AfterInit() error {
	b.buildingTemplates = tsv.GetTsvManager(tsv.BuildingAttribute).(*tsv.BuildingAttributeTsvManager)
	return nil
}

func NewBuildingService() *cityBuildingService {
	return &cityBuildingService{IBaseService: common.NewBaseService(&model.CityBuilding{})}
}

// update 通过建筑id和修改参数修改城邦建筑数据  update city building by buildingId and upData
func (b *cityBuildingService) update(tid,cid int64,upData map[string]interface{}, buildingId int64) utils.IError {
	db := b.Db()
	err := db.Model(&model.CityBuilding{}).Where("id = ? ", buildingId).Updates(upData).Error
	if err != nil {
		return utils.NewError(err.Error(),utils.M{"upData":upData,"buildingId": buildingId})
	}
	var upType int32
	var state int32 = -1
	var lv int32 = -1
	level,ok := upData["level"]
	if ok {
		upType = 2
		switch level.(type) {
		case json.Number:
			f, err := level.(json.Number).Float64()
			if err == nil {
				lv = int32(f)
			}
		case int32:
			lv = level.(int32)
		}
	}

	buildingState,ok := upData["building_state"]
	if ok {
		upType += 1

		switch buildingState.(type) {
		case json.Number:
			f, err := buildingState.(json.Number).Float64()
			if err == nil {
				state = int32(f)
			}
		case int32:
			state = buildingState.(int32)
		}

	}
	if upType > 0 {
		b.DispatchEvent(misc.NodeGame,tid,buildingId,msg_dt.CdEveCityBuildingState,&msg_dt.EveCityBuildingState{
			BuildingId: buildingId,
			UpdateType: upType,
			BuildingState:  state,
			BuildingLv: lv,
			Cid: cid,
		})
	}



	return nil
}

// GetCityBuildingById 通过建筑id查询城邦建筑 query city build by buildingId
func (b *cityBuildingService) GetCityBuildingById(buildingId int64) (build *model.CityBuilding, iErr utils.IError) {
	db := b.Db().Model(&model.CityBuilding{})
	var t model.CityBuilding
	err := db.Where("id = ?", buildingId).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(),utils.M{"buildingId": buildingId})
		}
	}
	if t.Id > 0 {
		build = &t
	}
	return
}

// GetCityBuildingByCityId 通过城邦id查询所有城邦建筑 query all city building by city id
func (b *cityBuildingService) GetCityBuildingByCityId(cityId int64) (buildList []*model.CityBuilding, iErr utils.IError) {
	db := b.Db().Model(&model.CityBuilding{})
	err := db.Where("city_id = ?", cityId).Find(&buildList).Error
	if err != nil {
		return nil, utils.NewError(err.Error(),utils.M{"cityId": cityId})
	}
	return
}

// GetCountByTypeAndLevel 通过城邦id、建筑类型、建筑等级查询建筑数量 query building mapper level number by cityId and buildingType and level
func (b *cityBuildingService) GetCountByTypeAndLevel(cityId int64, lv int32, buildingType build_e.BuildingType) (res int64, iErr utils.IError) {
	db := b.Db().Model(&model.CityBuilding{})
	err := db.Where("city_id = ? and building_type = ? and level >= ? ", cityId, buildingType, lv).Count(&res).Error
	if err != nil {
		return 0, utils.NewError(err.Error(),utils.M{"cityId": cityId,"buildingLv": lv,"buildingType": int32(buildingType)})
	}
	return res, nil
}

// GetBuildingLvByTypeAndLevel 通过建筑id、建筑类型、建筑等级查询建筑等级 query building mapper level number by cityId and buildingType and level
func (b *cityBuildingService) GetBuildingLvByTypeAndLevel(cityId int64, buildingType build_e.BuildingType) (lv int32, iErr utils.IError) {
	db := b.Db().Model(&model.CityBuilding{})
	var t model.CityBuilding
	err := db.Select("id,level").Where("city_id = ? and building_type = ?", cityId, buildingType).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return 0, utils.NewError(err.Error(),utils.M{"cityId": cityId})
		}
	}
	if t.Id > 0 {
		lv = t.Level
	}
	return lv, nil
}

// GetBuildingCount 查询所有城邦建筑数量 query all city building number
func (b *cityBuildingService) GetBuildingCount() (count int64, iErr utils.IError) {
	db := b.Db().Model(&model.CityBuilding{})
	err := db.Count(&count).Error
	if err != nil {
		return count, utils.NewError(err.Error(),nil)
	}
	return
}

//GetBuildingIdAndCityTemId 查询指定城市所有建筑属性
func (c *cityBuildingService) GetBuildingIdAndCityTemId(ids int64) (res []*model.CityBuilding,iErr utils.IError) {
	db := c.Db().Model(model.CityBuilding{})
	err := db.Select("id,building_type,level,building_state,building_temp_id").Where("city_id = ?",ids).Find(&res).Error
	if err != nil {
		return nil,utils.NewError(err.Error(),utils.M{"cityIdList":ids})
	}
	return res,nil
}

// InvestResources 通过城邦建筑id、投入材料添加建筑资源 add building resource by cityBuildId and  putResourceMap
func (b *cityBuildingService) InvestResources(buildId int64, putResourceMap map[int32]int32) (buildResourceMap map[int32]int32,depleteMap map[int32]int32,upMap map[string]interface{}, iErr utils.IError) {
	build, iErr := b.GetCityBuildingById(buildId)
	if iErr != nil {
		return nil,nil,nil, iErr
	}
	if build == nil {
		return nil,nil,nil, utils.NewError(TErrBuildingNotFail,utils.M{"buildId":buildId,"putResourceMap":putResourceMap})
	}
	if build.BuildingState != build_e.InvestResource {
		return nil,nil,nil, utils.NewError(TErrBuildStateNotWith,utils.M{"buildId":buildId,"putResourceMap":putResourceMap})
	}
	buildResourceMap = make(map[int32]int32)
	err := json.Unmarshal([]byte(build.PutResource), &buildResourceMap)
	if err != nil {
		return nil,nil,nil, utils.NewError(err.Error(),utils.M{"buildId":buildId,"putResourceMap":putResourceMap})
	}
	//检测当前建筑是否需要指定建造等级
	checkCityCsv := b.GetBuildTemple(int32(build.BuildingType), build.Level)
	iErr = b.checkNeedBuildAndLevel(build.CityId, checkCityCsv)
	if err != nil {
		return nil,nil,nil, iErr
	}
	buildTsv := b.GetBuildTemple(int32(build.BuildingType), build.Level+1)
	if buildTsv == nil {
		return nil,nil,nil, utils.NewError(TErrBuildTempNotFail,utils.M{"buildId":buildId,"putResourceMap":putResourceMap})
	}
	//转换该建筑需要的升级的资源
	needResource := make(map[int32]int32)
	for _, value := range buildTsv.Cost {
		needResource[value[0]] = value[1]
	}
	depleteMap = make(map[int32]int32)
	//检测当前建筑是否需要投入此资源
	for key, value := range putResourceMap {
		needNum := needResource[key]
		if needNum <= 0 {
			return nil,nil,nil, utils.NewError(TErrPutResourceAndBuildingNeedResourceNotWith,utils.M{"buildId":buildId,"putResourceMap":putResourceMap})
		}
		if value > 0 {
			num := buildResourceMap[key]
			if num == needNum {
				continue
			}
			if value+num > needNum {
				depleteMap[key] = needNum - num
				buildResourceMap[key] += depleteMap[key]
			} else {
				depleteMap[key] = value
				buildResourceMap[key] += value
			}
		}
	}

	//判断建筑需要所有资源是否投满
	isNextLevel := true
	for key, value := range needResource {
		if buildResourceMap[key] < value {
			isNextLevel = false
			break
		}
	}

	if isNextLevel {
		upState := build_e.InvestEnergy
		//获取建筑模板
		cityCsv := b.GetBuildTemple(int32(build.BuildingType), build.Level)
		if cityCsv.BuildProgress == 0 {
			upMap = map[string]interface{}{
				"building_state": int32(0),
				"level": build.Level+1,
				"building_progress": 0,
				"put_resource": "{}"}
			return
		}
		upMap = map[string]interface{}{"building_state": int32(upState), "put_resource": "{}"}
	}else{
		resources, err := json.Marshal(buildResourceMap)
		if err != nil {
			return nil,nil,nil, utils.NewError(TErrPutResourceAndBuildingNeedResourceNotWith,utils.M{"buildId":buildId,"putResourceMap":putResourceMap})
		}
		if len(depleteMap) > 0 {
			upMap = make(map[string]interface{})
			upMap["put_resource"] = string(resources)
		}
	}

	return
}

// InvestmentInConstruction 通过建筑id、玩家精力值添加建筑升级所需建设度 add Construction by buildingId and character energy value
func (b *cityBuildingService) InvestmentInConstruction(tid,cid,buildingId int64) (upMap map[string]interface{},currentNum int32, iErr utils.IError) {
	building, err := b.GetCityBuildingById(buildingId)
	if err != nil {
		return upMap, currentNum, err
	}
	//检验建筑是否存在
	if building == nil {
		return upMap,currentNum, utils.NewError(TErrBuildingNotFail,utils.M{"buildingId":buildingId})
	}
	//检验建筑等级是否已到达顶级
	if b.GetBuildTemple(int32(building.BuildingType), building.Level+1) == nil {
		return upMap,currentNum, utils.NewError(TErrBuildLevelMax,utils.M{"buildingId":buildingId})
	}
	//获取建筑模板
	cityCsv := b.GetBuildTemple(int32(building.BuildingType), building.Level)
	//检测建筑是否处于建筑中
	if building.BuildingState != build_e.InvestEnergy {
		return upMap,currentNum, utils.NewError(TErrBuildStateNotWith,utils.M{"buildingId":buildingId})
	}
	cityBuildingConfig := b.Conf().Design.CityBuilding
	//建筑精力升级+1
	building.BuildingProgress += cityBuildingConfig.EnergyConversionBuildingTotal
	upMap = make(map[string]interface{})
	if building.BuildingProgress >= cityCsv.BuildProgress {
		building.Level += 1
		upMap = map[string]interface{}{
			"building_state":    int32(build_e.InvestResource),
			"building_progress": 0,
			"level":             building.Level,
		}
	} else {
		currentNum= building.BuildingProgress
		upMap = map[string]interface{}{
			"building_progress": building.BuildingProgress,
		}
	}

	return upMap,currentNum, nil
}

// CityBuildingEnergy 通过城邦建筑id查询城邦建筑建设度 query city building energy by city building id
func (b *cityBuildingService) CityBuildingEnergy(buildingId int64) (needBuildingLv map[int32]int32,currentEnergy int32, iErr utils.IError) {
	building, iErr := b.GetCityBuildingById(buildingId)
	if iErr != nil {
		return needBuildingLv,currentEnergy, iErr
	}
	if building == nil {
		return needBuildingLv,currentEnergy, utils.NewError(TErrBuildingNotFail,utils.M{"buildingId": buildingId})
	}
	//检测当前建筑是否需要指定建造等级
	checkCityCsv := b.GetBuildTemple(int32(building.BuildingType), building.Level)
	if len(checkCityCsv.NeedBuildLevel) > 0 {
		for _, needErectLevel := range checkCityCsv.NeedBuildLevel {
			level, err := b.GetBuildingLvByTypeAndLevel(building.CityId, build_e.BuildingType(needErectLevel[0]))
			if err != nil {
				return needBuildingLv,currentEnergy, utils.NewError(err.Error(),utils.M{"buildingId": buildingId})
			}
			needBuildingLv[needErectLevel[0]] = level
		}
	}
	return needBuildingLv,building.BuildingProgress, nil
}

// CityBuildingResource 通过城邦建筑id查询城邦建筑已投入资源数 cityBuildingResource by buildingId
func (b *cityBuildingService) CityBuildingResource(ac common.IActionCtx, buildingId int64) (needBuildingLv map[int32]int32,resources map[int32]int32, iErr utils.IError) {
	building, iErr := b.GetCityBuildingById(buildingId)
	if iErr != nil {
		return needBuildingLv,resources, iErr
	}
	if building == nil {
		return needBuildingLv,resources, utils.NewError(TErrBuildingNotFail,utils.M{"buildingId": buildingId})
	}
	resources = make(map[int32]int32)
	needBuildingLv = make(map[int32]int32)
	err := json.Unmarshal([]byte(building.PutResource), &resources)
	if err != nil {
		return needBuildingLv,resources, utils.NewError(err.Error(),utils.M{"putResource": building.PutResource})
	}
	//检测当前建筑是否需要指定建造等级
	checkCityCsv := b.GetBuildTemple(int32(building.BuildingType), building.Level)
	if len(checkCityCsv.NeedBuildLevel) > 0 {
		for _, needErectLevel := range checkCityCsv.NeedBuildLevel {
			level, err := b.GetBuildingLvByTypeAndLevel(building.CityId, build_e.BuildingType(needErectLevel[0]))
			if err != nil {
				return needBuildingLv,resources, utils.NewError(err.Error(),utils.M{"putResource": building.PutResource})
			}
			needBuildingLv[needErectLevel[0]] = level
		}
	}
	return needBuildingLv,resources, nil
}

// checkNeedBuildAndLevel 校验城邦建筑升级所需其他建筑等级 check current city building need other buildings reach the corresponding level
func (b *cityBuildingService) checkNeedBuildAndLevel(cityId int64, cityCsv *tsv.BuildingAttributeTsv) utils.IError {
	if len(cityCsv.NeedBuildLevel) > 0 {
		for _, needErectLevel := range cityCsv.NeedBuildLevel {
			count, iErr := b.GetCountByTypeAndLevel(cityId, needErectLevel[1], build_e.BuildingType(needErectLevel[0]))
			if iErr != nil {
				return iErr
			}
			if count == 0 {
				return utils.NewError(TErrBuildingUpgradeNotMatch,utils.M{"upgrade building temp": cityCsv})
			}
		}
	}
	return nil
}

// GetBuildTemple 获取建筑模板 get city building template
func (b *cityBuildingService) GetBuildTemple(objectId, lv int32) *tsv.BuildingAttributeTsv {
	for _, v := range b.buildingTemplates.TsvSlice {
		if v.ObjectId == objectId && v.Level == lv {
			return v
		}
	}
	return nil
}
