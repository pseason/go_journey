package city_building

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

func (b *cityBuildingService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		// 请求城邦所有建筑
		msg_dt.CdCityBuildingList: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityBuildingList)
			buildingList, iErr := b.GetCityBuildingByCityId(req.CityId)
			if iErr != nil {
				app.Log().TError(ac.Tid(),iErr)
				resErrCode = err_dt.ErrCreateFailed
			} else {
				resBody = &msg_dt.ResCityBuildingList{Buildings: buildingList}
			}
			return
		},
		// 请求指定建筑
		msg_dt.CdCityBuildingGetBuilding: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityBuildingGetBuilding)
			building, iErr := b.GetCityBuildingById(req.BuildingId)
			if iErr != nil{
				app.Log().TError(ac.Tid(),iErr)
				resErrCode = err_dt.ErrQueryFailed
			} else {
				if building == nil  {
					resErrCode = err_dt.ErrQueryFailed
					return
				}
				resBody = &msg_dt.ResCityBuildingGetBuilding{Building: *building}
			}
			return
		},
		// 请求指定建筑投入材料
		msg_dt.CdCityBuildingResource: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityBuildingResource)
			needBuildingLv,resourceMap, iErr := b.CityBuildingResource(ac,req.BuildingId)
			if iErr != nil{
				app.Log().TError(ac.Tid(),iErr)
				resErrCode = err_dt.ErrQueryFailed
			} else {
				resBody = &msg_dt.ResCityBuildingResource{Resources: resourceMap,NeedBuildingLv: needBuildingLv}
			}
			return
		},
		// 请求指定建筑投入建设度
		msg_dt.CdCityBuildingEnergy: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityBuildingEnergy)
			needBuildingLv,currentPutEnergy, iErr := b.CityBuildingEnergy(req.BuildingId)
			if iErr != nil{
				app.Log().TError(ac.Tid(),iErr)
				resErrCode = err_dt.ErrQueryFailed
			} else {
				resBody = &msg_dt.ResCityBuildingEnergy{CurrentEnergyNum: currentPutEnergy,NeedBuildingLv: needBuildingLv}
			}
			return
		},
		msg_dt.CdLoadCityBuildingList: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqLoadCityBuildingList)
			cityBuildings, iErr := b.GetBuildingIdAndCityTemId(req.CityId)
			if iErr != nil {
				app.Log().TError(ac.Tid(),iErr)
				return nil,err_dt.ErrQueryFailed
			}
			return &msg_dt.ResLoadCityBuildingList{Buildings: cityBuildings},0
		},
	}
}

func (b *cityBuildingService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdCityBuildingTryInvestResources: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqCityBuildingTryInvestResources)
				resourceMap,buildingList,upMap, iErr := b.InvestResources(req.BuildingId, req.InvestResources)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					errCode = err_dt.ErrUpdateFailed
				} else {
					resBody = &msg_dt.ResCityBuildingTryInvestResources{Cid: req.Cid,CurrentPutResources: resourceMap,
						InvestResources: buildingList,UpMap: upMap,BuildingId: req.BuildingId}
				}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResCityBuildingTryInvestResources)
				iErr := b.update(ac.Tid(),res.Cid,res.UpMap, res.BuildingId)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return iErr
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdCityBuildingTryInvestEnergy: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqCityBuildingTryInvestEnergy)
				upMap,currentNum, iErr := b.InvestmentInConstruction(ac.Tid(),req.Cid,req.BuildingId)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					errCode = err_dt.ErrUpdateFailed
				} else {
					resBody = &msg_dt.ResCityBuildingTryInvestEnergy{CurrentNum: currentNum,UpMap: upMap}
				}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqCityBuildingTryInvestEnergy)
				res := tryResBody.(*msg_dt.ResCityBuildingTryInvestEnergy)
				iErr := b.update(ac.Tid(),req.Cid,res.UpMap, req.BuildingId)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return iErr
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
	}
}