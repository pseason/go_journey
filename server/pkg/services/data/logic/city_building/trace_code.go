package city_building

const (
	//建筑模板不存在
	TErrBuildTempNotFail = "building temp not existence"
	//建筑数量查询失败
	TErrBuildNumberQueryFailed
	//建筑建设状态不符
	TErrBuildStateNotWith = "building build state mismatch"
	//投入材料与建设所需材料不符
	TErrPutResourceAndBuildingNeedResourceNotWith = "put resource and building inconsistent"
	//建筑不存在
	TErrBuildingNotFail = "building not existence"
	//该建筑投入的精力值已满
	TErrPutEnergyNumExceed
	//建筑等级已满
	TErrBuildLevelMax = "build level max "
	//建筑升级所需建筑等级不匹配
	TErrBuildingUpgradeNotMatch = "building upgrade need build Get to level"
)
