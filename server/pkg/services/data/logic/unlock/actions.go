package unlock

import (
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

func (u *unlockService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode){
		msg_dt.CdUnlockCurrentInfo: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			state, err := u.GetState(ac)
			if err != nil {
				return nil, err_dt.ErrQueryFailed
			}
			return &msg_dt.ResUnlockCurrentInfo{Records: state.Records}, 0
		},
		msg_dt.CdUnlockExecute: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqUnlockExecute)
			err := u.CommonUnlockOptions(ac, req.Opt, req.TemplateIds)
			if err != nil {
				return nil, err_dt.ErrUpdateFailed
			}
			return &msg_dt.ResUnlockExecute{}, 0
		},
	}
}
