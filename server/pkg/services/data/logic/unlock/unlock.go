package unlock

import (
	"95eh.com/eg/app"
	eg_utils "95eh.com/eg/utils"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/misc/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
)

type unlockService struct {
	common.IBaseService
	tsv *tsv.UnlockTsvManager
}

func NewUnlockService() *unlockService {
	r := &unlockService{
		IBaseService: common.NewBaseService(&model.Unlock{}),
	}
	return r
}

func (u *unlockService) AfterInit() error {
	u.tsv = tsv.GetTsvManager(tsv.Unlock).(*tsv.UnlockTsvManager)
	return nil
}

func (u *unlockService) GetState(ctx common.IActionCtx) (res model.Unlock, err error) {
	cid := ctx.SubjectId()
	err = u.Db().Model(&model.Unlock{}).Where("cid=?", cid).First(&res).Error
	if err != nil && !utils.IsOrmNotFound(err) {
		return
	}
	err = nil
	if res.Id == 0 {
		res.Records = make([]int32, 0, 0)
		res.Cid = cid
		err = u.Db().Create(&res).Error
	}
	return
}

func (u *unlockService) GetUnlockRuleFromTsv(tye int32, idList []int32) (res []int32) {
	res = make([]int32, 0, 3)
	if len(idList) == 0 {
		return
	}
	idMap := make(map[int32]struct{})
	for _, id := range idList {
		idMap[id] = struct{}{}
	}
	for _, n := range u.tsv.TsvSlice {
		if n.ConditionType == tye {
			_, has := idMap[n.ConditionId]
			if has {
				res = append(res, n.Id)
			}
		}
	}
	return res
}

func (u *unlockService) CommonUnlockOptions(ctx common.IActionCtx, option model.UnlockCommonOption, templateIdList []int32) (err error) {
	return u.OptionExecute(ctx, int32(option), templateIdList, true, nil)
}

func (u *unlockService) OptionExecute(ac common.IActionCtx, option int32, templateIdList []int32, judgmentType bool, ruleIds []int32) (err error) {
	cid := ac.SubjectId()
	var rules []int32
	if judgmentType {
		rules = u.GetUnlockRuleFromTsv(option, templateIdList)
		if len(rules) == 0 {
			return
		}
	} else {
		if ruleIds != nil {
			rules = ruleIds
		}
	}
	var curUnlock model.Unlock
	err = u.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), sErr eg_utils.IError) {
		err := txAc.TxDb().Model(&model.Unlock{}).Where("cid=?", cid).First(&curUnlock).Error
		if err != nil && !utils.IsOrmNotFound(err) {
			app.Log().Error("unlock data find failed", nil)
			sErr = eg_utils.NewError(err.Error(), nil)
			return
		}
		if curUnlock.Id == 0 {
			//不存在则创建
			curUnlock = model.Unlock{
				Cid:     ac.SubjectId(),
				Records: rules,
			}
			err = txAc.TxDb().Model(&model.Unlock{}).Create(&curUnlock).Error
			if err != nil {
				app.Log().Error("unlock data create failed", nil)
				sErr = eg_utils.NewError(err.Error(), nil)
				return
			}
		} else {
			currentMap := make(map[int32]struct{})
			for _, v := range curUnlock.Records {
				_, has := currentMap[v]
				if !has {
					currentMap[v] = struct{}{}
				}
			}
			for _, v := range rules {
				_, has := currentMap[v]
				if !has {
					currentMap[v] = struct{}{}
				}
			}
			latestRules := make([]int32, 0, len(currentMap))
			for k := range currentMap {
				latestRules = append(latestRules, k)
			}
			//latestRuleSlice := utils.SortInt32Slice(latestRules)
			//latestRuleSlice.Sort()
			err = txAc.TxDb().Model(&model.Unlock{}).Where("id=?", curUnlock.Id).Update("records", latestRules).Error
			if err != nil {
				app.Log().Error("unlock data update failed", nil)
				sErr = eg_utils.NewError(err.Error(), nil)
				return
			}
			curUnlock.Records = latestRules
		}
		return
	})
	if err != nil {
		return
	}
	u.DispatchEvent(misc.NodeGame, ac.Tid(), cid, msg_dt.CdEveUnlockChange, &msg_dt.EveUnlockChange{
		Cid:     cid,
		Records: curUnlock.Records,
	})
	return
}
