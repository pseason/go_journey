package team

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/misc"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/team_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type ITeamService interface {
	common.IBaseService
}

func (t *teamService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode){
		msg_dt.CdTeamGetTeamById: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamGetTeamById)
			team, err := t.GetTeamById(req.TeamId)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrQueryFailed
			}
			return &msg_dt.ResTeamGetTeamById{Team: team}, 0
		},
		msg_dt.CdTeamInviteJoinTeam: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamInviteJoinTeam)
			err := t.InviteTeam(ac, req.Cid, req.Oid, req.CityId)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResTeamInviteJoinTeam{}, 0
		},
		msg_dt.CdTeamAgreeApplyJoinTeam: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamAgreeApplyJoinTeam)
			teamId,cidList,err := t.AgreeAllTeamApply(ac,req.Cid,req.Oid)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrUpdateFailed
			}
			return &msg_dt.ResTeamAgreeApplyJoinTeam{TeamId: teamId,CidList: cidList}, 0
		},
		msg_dt.CdTeamAgreeInviteJoinTeam: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamAgreeInviteJoinTeam)
			err := t.AgreeInviteJoinTeam(ac,req.TeamId, req.Cid)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrUpdateFailed
			}
			return &msg_dt.ResTeamAgreeInviteJoinTeam{}, 0
		},
		msg_dt.CdTeamRefuseApplyJoinTeam: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamRefuseApplyJoinTeam)
			err := t.RefuseApplyJoinTeam(ac, req.Cid,req.Oid)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrUpdateFailed
			}
			return &msg_dt.ResTeamRefuseApplyJoinTeam{}, 0
		},
		msg_dt.CdTeamOneClickApply: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamOneClickApply)
			teamId,err := t.OneClickApply(ac,req.Cid,req.AimsTempId,req.Age,req.BattlePower)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrCreateFailed
			}
			return &msg_dt.ResTeamOneClickApply{TeamId: teamId},0
		},
		msg_dt.CdTeamTransferTeam: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamTransferTeam)
			err := t.TransferTeam(ac,req.Cid,req.Oid)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrUpdateFailed
			}
			return &msg_dt.ResTeamTransferTeam{},0
		},
		msg_dt.CdTeamDisbandTeam: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamDisbandTeam)
			err := t.DisbandTeam(ac,req.Cid)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrDelFailed
			}
			return &msg_dt.ResTeamDisbandTeam{},0
		},
		msg_dt.CdTeamPageQueryTeam: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamPageQueryTeam)
			cap,teamList,err := t.PageGetTeamAll(req.Page,req.Size,req.AimsId,req.CityId)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrUpdateFailed
			}
			return &msg_dt.ResTeamPageQueryTeam{TeamList: teamList,Total: int32(cap)},0
		},
		msg_dt.CdTeamApplyList: func(ac common.IActionCtx, body interface{}) (interface{}, err_dt.ErrCode) {
			req := body.(*msg_dt.ReqTeamApplyList)
			team, err := t.GetTeamByLeaderId(req.Cid)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrQueryFailed
			}
			relationshipList, err := t.GetTeamRelationshipList(team.Id, team_e.ApplyTeam)
			if err != nil {
				app.Log().TError(ac.Tid(),err)
				return nil, err_dt.ErrQueryFailed
			}
			return &msg_dt.ResTeamApplyList{ApplyList: relationshipList},0
		},
	}
}

func (t *teamService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdTryTeamCreate: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTryTeamCreate)
				team := model.GenDefaultTeam(req.CharacterNick, req.Cid,req.CityId)
				resBody = &msg_dt.ResTryTeamCreate{Team: team}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := tryResBody.(*msg_dt.ResTryTeamCreate)
				err := t.CreateTeam(ac, req.Team)
				if err != nil {
					app.Log().TError(ac.Tid(),err)
					return err
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTryTeamLeaveTeam: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return &msg_dt.ResTryTeamLeaveTeam{},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqTryTeamLeaveTeam)
				err := t.LeaveTeam(ac,req.TeamId,req.Cid)
				if err != nil {
					app.Log().TError(ac.Tid(),err)
					return err
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTeamApplyJoinTeam: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTeamApplyJoinTeam)
				leaderId,err := t.ApplyTeam(ac,req.TeamId,req.CityId)
				if err != nil {
					app.Log().TError(ac.Tid(),err)
					return nil, err_dt.ErrCreateFailed
				}
				return &msg_dt.ResTeamApplyJoinTeam{TeamId: req.TeamId,Cid: req.Cid,TeamLeaderId: leaderId}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := tryResBody.(*msg_dt.ResTeamApplyJoinTeam)
				//新增申请消息
				iError = t.CreateTeamRelationship(req.TeamId, req.Cid, team_e.ApplyTeam)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				//通知队长
				t.DispatchEvent(misc.NodeGame, ac.Tid(), req.Cid, msg_dt.CdEveTeamApplyNotify, &msg_dt.EveTeamApplyNotify{Cid: req.TeamLeaderId})
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTeamUpdateTeamArms: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTeamUpdateTeamArms)
				team,upMap,err := t.UpTeamAims(ac,req.Cid,req.TeamArms)
				if err != nil {
					app.Log().TError(ac.Tid(),err)
					return nil, err_dt.ErrUpdateFailed
				}
				return &msg_dt.ResTeamUpdateTeamArms{Team: team,UpMap: upMap},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResTeamUpdateTeamArms)
				iError = t.UpDateTeamByUpMap(res.Team.Id, res.UpMap)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				//team aims change notify
				t.DispatchEvent(misc.NodeGame, ac.Tid(), res.Team.Id, msg_dt.CdEveTeamUpdateTeamAimsChangeNotify, &msg_dt.EveTeamUpdateTeamAimsChangeNotify{
					TeamId: res.Team.Id,
					TeamAims: msg_dt.TeamArms{
						TeamAimsId: res.Team.TeamAimsTemId,
						MinAgeNum:  res.Team.MinAgeClaim,
						MaxAgeNum:  res.Team.MaxAgeClaim,
						PowerNum:   res.Team.PowerClaim,
					},
					OidList: model.GetTeamMemberList(res.Team.TeamMember),
				})
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTryTeamPleaseLeaveTeam: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTryTeamPleaseLeaveTeam)
				upMap,team,err := t.PleaseLeaveTheTeam(ac,req.Cid,req.Oid)
				if err != nil {
					app.Log().TError(ac.Tid(),err)
					return nil, err_dt.ErrUpdateFailed
				}
				return &msg_dt.ResTryTeamPleaseLeaveTeam{UpMap: upMap,Team: team},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqTryTeamPleaseLeaveTeam)
				res := tryResBody.(*msg_dt.ResTryTeamPleaseLeaveTeam)
				iError = t.UpDateTeamByUpMap(res.Team.Id, res.UpMap)
				if iError != nil {
					return iError
				}
				t.DispatchEvent(misc.NodeGame, ac.Tid(), req.Cid, msg_dt.CdEveTeamMemberChange,
					&msg_dt.EveTeamMemberChange{MemberList: model.GetTeamMemberList(res.Team.TeamMember), TeamId: res.Team.Id, Cid: req.Oid, Type: 2})
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTryTeamUpdateTeamAutoJoin: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTryTeamUpdateTeamAutoJoin)
				team,err := t.UpdateTeamAutoJoin(req.Cid)
				if err != nil {
					app.Log().TError(ac.Tid(),err)
					return nil, err_dt.ErrUpdateFailed
				}
				return &msg_dt.ResTryTeamUpdateTeamAutoJoin{Team: team}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResTryTeamUpdateTeamAutoJoin)
				iError = t.UpDateTeamByUpMap(res.Team.Id, map[string]interface{}{"auto_agree": res.Team.AutoAgree})
				if iError != nil {
					return iError
				}
				//team auto agree change notify
				t.DispatchEvent(misc.NodeGame, ac.Tid(), res.Team.LeaderId, msg_dt.CdEveTeamAutoAgreeChange, &msg_dt.EveTeamAutoAgreeChange{
					TeamId:      res.Team.Id,
					IsAutoAgree: res.Team.AutoAgree,
					CidList:     model.GetTeamMemberList(res.Team.TeamMember),
				})
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdTryTeamAutoAgreeJoin: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqTryTeamAutoAgreeJoin)
				team,upMap,memberList,oid,err := t.AutoAgreeJoinTeam(ac, req.Team, req.Cid)
				if err != nil {
					app.Log().TError(ac.Tid(),err)
					return nil, err_dt.ErrUpdateFailed
				}
				return &msg_dt.ResTryTeamAutoAgreeJoin{Team: team,UpData:upMap,MemberList: memberList,Oid: oid},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResTryTeamAutoAgreeJoin)
				iErr := t.UpDateTeamByUpMap(res.Team.Id, res.UpData)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return iErr
				}
				iErr = t.DelTeamApplyRelationByTeamIdAndCid(res.Team.Id, res.Oid)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return iErr
				}
				t.NewTeamMemberNotify(ac.Tid(), res.Team.Id, res.Oid, res.MemberList...)
				t.TeamLoadNotify(ac.Tid(),res.Oid,res.Team)
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
	}
}