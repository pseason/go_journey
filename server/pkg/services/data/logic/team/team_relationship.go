package team

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/team_e"
)

//CreateTeamRelationship 创建队伍申请关系 create team apply relation
func (t *teamService) CreateTeamRelationship(teamId, cid int64, applyType team_e.RelationshipType) (utils.IError) {
	b, iErr := t.CheckTeamRelationshipByTeamIdAndCid(teamId, cid)
	if iErr != nil {
		return iErr
	}
	if b {
		app.Log().TError(0,utils.NewError(TErrAlreadyApplyJoinTeam, utils.M{"teamId": teamId, "cid": cid}))
		return nil
	}
	teamRelationship := model.GenRelationshipTeam(teamId, cid, applyType)
	db := t.Db().Model(&model.TeamRelationship{})
	err := db.Create(teamRelationship).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"teamId": teamId, "cid": cid})
	}
	return nil
}

//GetTeamRelationshipList 获取队伍申请关系集合 get team apply relation list
func (t *teamService) GetTeamRelationshipList(teamId int64, applyType team_e.RelationshipType) (res []model.TeamRelationship, iErr utils.IError) {
	db := t.Db().Model(&model.TeamRelationship{})
	err := db.Where("team_id = ? and  apply_type = ?", teamId, applyType).Order("created desc").Find(&res).Error
	if err != nil {
		return nil, utils.NewError(err.Error(), utils.M{"teamId": teamId})
	}
	return res, nil
}

//CheckTeamRelationshipByTeamIdAndCid 通过队伍id玩家id查询队伍申请关系 query teamRelationship by team id and cid
func (t *teamService) CheckTeamRelationshipByTeamIdAndCid(teamId, cid int64) (b bool, iErr utils.IError) {
	db := t.Db().Model(&model.TeamRelationship{})
	var count int64
	err := db.Where("team_id = ? and cid = ?", teamId, cid).Count(&count).Error
	if err != nil {
		return false, utils.NewError(err.Error(), utils.M{"teamId": teamId, "cid": cid})
	}
	if count > 0 {
		return true, nil
	}
	return
}

//DelTeamApplyRelationByTeamIdAndCid 通过队伍id玩家id删除队伍关系 del team apply relation by team id and cid
func (t *teamService) DelTeamApplyRelationByTeamIdAndCid(teamId, cid int64) utils.IError {
	db := t.Db().Model(&model.TeamRelationship{})
	err := db.Unscoped().Where("team_id = ? and cid = ?", teamId, cid).Delete(&model.TeamRelationship{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"teamId": teamId, "cid": cid})
	}
	return nil
}

//DelAllTeamApplyRelationByTeamId 通过队伍id删除队伍所有申请关系 del all team apply relation by team id
func (t *teamService) DelAllTeamApplyRelationByTeamId(teamId int64) utils.IError {
	db := t.Db().Model(&model.TeamRelationship{})
	err := db.Unscoped().Where("team_id = ?", teamId).Delete(&model.TeamRelationship{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"teamId": teamId})
	}
	return nil
}
