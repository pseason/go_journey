package team

const (
	//玩家不是队长
	TErrCharacterNotTeamLeader = " character not team leader"
	//队伍人数已达上限
	TErrTeamCharacterCap = "team character cap"
	//玩家已加入队伍
	TErrCharacterAlreadyJoinTeam = "character already join team"
	//玩家未加入队伍
	TErrCharacterNotJoinTeam = "character not join team"
	//队伍不存在
	TErrTeamNotExist = "Team does not exist "
	//玩家已申请加入队伍
	TErrAlreadyApplyJoinTeam = "character already apply join team"
	//队伍申请关系不存在
	TErrTeamApplyRelationNotExist = "Team apply relation does not exist "
	//城邦创建队伍与玩家不在同一城邦
	TErrCityCreateTeamAndCharacterNotSameCity = "TErrCityCreateTeamAndCharacterNotSameCity"
	//队长不能交换位置
	TErrTeamLeaderNotLocationCutOver = "character is team leader not location cut over"
)
