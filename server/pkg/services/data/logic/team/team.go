package team

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/team_e"
	"hm/pkg/services/data/msg_dt"
	"strings"
)

type  teamService struct {
	common.IBaseService
}

func NewTeamService() *teamService {
	return &teamService{IBaseService: common.NewBaseService(&model.Team{}, &model.TeamRelationship{})}
}

func (t *teamService) AfterInit() error {
	return nil
}

//UpDateTeamByUpMap 通过队伍id和修改参数修改队伍数据 update team data by team id and upMap data
func (t *teamService) UpDateTeamByUpMap(teamId int64, upMap map[string]interface{}) utils.IError {
	db := t.Db().Model(&model.Team{})
	err := db.Where("id = ?", teamId).Updates(upMap).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"teamId": teamId, "upMap": upMap})
	}
	return nil
}

//CreateTeam 创建队伍 create team by character id and character nick
func (t *teamService) CreateTeam(ac common.IActionCtx, addTeam *model.Team) (iErr utils.IError) {
	iErr = t.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
		db := txAc.TxDb().Model(&model.Team{})
		err := db.Create(addTeam).Error
		if err != nil {
			iErr = utils.NewError(err.Error(), utils.M{"team": addTeam})
		}
		return
	})
	return
}

//DeleteTeam 删除队伍 delete team by team id
func (t *teamService) DeleteTeam(teamId int64) utils.IError {
	db := t.Db().Model(&model.Team{})
	err := db.Unscoped().Where("id = ?", teamId).Delete(&model.Team{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"delete team id": teamId})
	}
	iErr := t.DelAllTeamApplyRelationByTeamId(teamId)
	if iErr != nil {
		return iErr
	}
	return nil
}

//PageGetTeamAll 分页请求所有队伍 page query all team
func (t *teamService) PageGetTeamAll(page, size, armsId int32, cityId int64) (cap int64, res []*model.Team, iErr utils.IError) {
	db := t.Db().Model(&model.Team{})
	teamAimsTem := ""
	if armsId > 0 {
		teamAimsTem = fmt.Sprintf("and team_aims_tem_id = %d", armsId)
	}
	err := db.Where("city_id = ? "+teamAimsTem, cityId).Count(&cap).Error
	if err != nil {
		return cap, nil, utils.NewError(err.Error(), utils.M{"page": page, "size": size})
	}
	if cap == 0 {
		return cap, make([]*model.Team, 0), nil
	}
	err = db.Where("city_id = ? "+teamAimsTem, cityId).Offset(int(page * size)).Limit(int(size)).Find(&res).Error
	if err != nil {
		return cap, nil, utils.NewError(err.Error(), utils.M{"page": page, "size": size})
	}
	return
}

//GetTeamById 通过队伍id查询队伍 query team by team id
func (t *teamService) GetTeamById(teamId int64) (res *model.Team, iErr utils.IError) {
	db := t.Db().Model(&model.Team{})
	var team model.Team
	err := db.Where("id = ? ", teamId).First(&team).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"teamId": teamId})
		}
	}
	if team.Id > 0 {
		res = &team
	}
	return res, nil
}

//GetTeamByTeamAimsTemId 通过队伍目标id查询所有符合队伍 query all team by teamAimsTemId
func (t *teamService) GetTeamByTeamAimsTemId(teamAimsTemId int32) (res []*model.Team, iErr utils.IError) {
	db := t.Db().Model(&model.Team{})
	err := db.Where("team_aims_tem_id = ?", teamAimsTemId).Find(&res).Error
	if err != nil {
		return nil, utils.NewError(err.Error(), utils.M{"teamAimsTemId": teamAimsTemId})
	}
	return res, nil
}

//GetTeamByLeaderId 通过队长id查询队伍 query team by team leader id
func (t *teamService) GetTeamByLeaderId(leaderId int64) (res *model.Team, iErr utils.IError) {
	db := t.Db().Model(&model.Team{})
	var team model.Team
	err := db.Where("leader_id = ? ", leaderId).First(&team).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"leaderId": leaderId})
		}
	}
	if team.Id > 0 {
		res = &team
	}
	return res, nil
}

//ApplyTeam 申请加入队伍 apply join team by team id and character id
func (t *teamService) ApplyTeam(ac common.IActionCtx, teamId, cityId int64) (int64, utils.IError) {
	team, iErr := t.GetTeamById(teamId)
	if iErr != nil {
		return 0, iErr
	}
	if team == nil {
		return 0, utils.NewError(TErrTeamNotExist, utils.M{"teamId": teamId})
	}
	if team.CityId != cityId {
		return 0, utils.NewError(TErrCityCreateTeamAndCharacterNotSameCity, utils.M{"cityId": cityId, "team_cityId": team.CityId})
	}
	return team.LeaderId, nil
}

//InviteTeam 邀请加入队伍 invite join team by team id and character id
func (t *teamService) InviteTeam(ac common.IActionCtx, cid, oid, cityId int64) utils.IError {
	team, err := t.GetTeamByLeaderId(cid)
	if err != nil {
		return err
	}
	if team == nil {
		return utils.NewError(TErrTeamNotExist, utils.M{"team leader id": cid})
	}
	//新增邀请加入队伍
	//err = t.CreateTeamRelationship(ac.Tid(), team.Id, oid, team_e.InviteTeam)
	//if err != nil {
	//	return err
	//}
	if team.CityId != cityId {
		return utils.NewError(TErrCityCreateTeamAndCharacterNotSameCity, utils.M{"cityId": cityId, "team_cityId": team.CityId})
	}
	//通知玩家
	t.DispatchEvent(misc.NodeGame, ac.Tid(), cid, msg_dt.CdEveTeamInviteNotify, &msg_dt.EveTeamInviteNotify{Cid: oid, TeamId: team.Id})
	return nil
}

//AutoAgreeJoinTeam 自动同意加入队伍
func (t *teamService) AutoAgreeJoinTeam(ac common.IActionCtx, team *model.Team, oid int64) (*model.Team,map[string]interface{},[]int64,int64,utils.IError) {
	if team.CurrentNumber+1 > 15 {
		return nil,nil,nil,0,utils.NewError(TErrTeamCharacterCap, utils.M{"currentCap": team.CurrentNumber})
	}
	if strings.Contains(team.TeamMember, fmt.Sprintf("%d", oid)) {
		return nil,nil,nil,0,utils.NewError(TErrCharacterAlreadyJoinTeam, utils.M{"teamMemberList": team.TeamMember, "joinCid": oid})
	}
	memberList := model.GetTeamMemberList(team.TeamMember)
	upMap := make(map[string]interface{})
	upMap["current_number"] = team.CurrentNumber + 1
	upMap["team_member"] = model.GetTeamMemberString(append(memberList, oid)...)
	team.CurrentNumber += 1
	team.TeamMember = model.GetTeamMemberString(append(memberList, oid)...)
	return team,upMap,memberList,oid,nil
}

//AgreeJoinTeam 同意申加入队伍 agree join team by team leader id and apply character id
func (t *teamService) AgreeJoinTeam(ac common.IActionCtx, team *model.Team, oid int64) utils.IError {
	if team.CurrentNumber+1 > 15 {
		return utils.NewError(TErrTeamCharacterCap, utils.M{"currentCap": team.CurrentNumber})
	}
	b, iErr := t.CheckTeamRelationshipByTeamIdAndCid(team.Id, oid)
	if iErr != nil {
		return iErr
	}
	if !b {
		return utils.NewError(TErrTeamApplyRelationNotExist, utils.M{"teamId": team.Id, "oid": oid})
	}
	if strings.Contains(team.TeamMember, fmt.Sprintf("%d", oid)) {
		return utils.NewError(TErrCharacterAlreadyJoinTeam, utils.M{"teamMemberList": team.TeamMember, "joinCid": oid})
	}
	memberList := model.GetTeamMemberList(team.TeamMember)
	upMap := make(map[string]interface{})
	upMap["current_number"] = team.CurrentNumber + 1
	upMap["team_member"] = model.GetTeamMemberString(append(memberList, oid)...)
	team.CurrentNumber += 1
	team.TeamMember = model.GetTeamMemberString(append(memberList, oid)...)
	iErr = t.UpDateTeamByUpMap(team.Id, upMap)
	if iErr != nil {
		return iErr
	}
	iErr = t.DelTeamApplyRelationByTeamIdAndCid(team.Id, oid)
	if iErr != nil {
		return iErr
	}
	t.NewTeamMemberNotify(ac.Tid(), team.Id, oid, memberList...)
	t.TeamLoadNotify(ac.Tid(),oid,team)
	return nil
}

func (t *teamService) AgreeAllTeamApply(ac common.IActionCtx, cid, oid int64) (teamId int64,cidList []int64,iErr utils.IError) {
	team, iErr := t.GetTeamByLeaderId(cid)
	if iErr != nil {
		return teamId,cidList,iErr
	}
	if team == nil {
		return teamId,cidList,utils.NewError(TErrTeamNotExist, utils.M{"teamLeaderId": cid})
	}
	if oid > 0 {
		iErr = t.AgreeJoinTeam(ac, team, oid)
		if iErr != nil {
			return teamId,cidList,iErr
		}
		return team.Id,[]int64{oid},nil
	}

	relationshipList, iErr := t.GetTeamRelationshipList(team.Id, team_e.ApplyTeam)
	if iErr != nil {
		return teamId,cidList,iErr
	}
	if len(relationshipList) > 0 {
		cidList = make([]int64,0)
		for _, relationship := range relationshipList {
			iErr = t.AgreeJoinTeam(ac, team, relationship.Cid)
			if iErr != nil {
				return teamId,cidList,iErr
			}
			cidList = append(cidList,relationship.Cid)
		}
	}
	return team.Id,cidList,nil
}

//RefuseApplyJoinTeam 拒绝加入队伍 refuse join team
func (t *teamService) RefuseApplyJoinTeam(ac common.IActionCtx, cid, oid int64) utils.IError {
	team, iErr := t.GetTeamByLeaderId(cid)
	if iErr != nil {
		return iErr
	}
	if team == nil {
		return utils.NewError(TErrTeamNotExist, utils.M{"teamId": ac.SubjectId()})
	}
	if oid == 0 {
		iErr = t.DelAllTeamApplyRelationByTeamId(team.Id)
		if iErr != nil {
			return iErr
		}
		return nil
	}
	b, iErr := t.CheckTeamRelationshipByTeamIdAndCid(team.Id, oid)
	if iErr != nil {
		return iErr
	}
	if !b {
		return utils.NewError(TErrTeamApplyRelationNotExist, utils.M{"teamId": team.Id, "oid": oid})
	}
	iErr = t.DelTeamApplyRelationByTeamIdAndCid(team.Id, oid)
	if iErr != nil {
		return iErr
	}
	return nil
}

//AgreeInviteJoinTeam 同意邀请加入队伍 agree invite join team by team id and character id
func (t *teamService) AgreeInviteJoinTeam(ac common.IActionCtx, teamId, cid int64) utils.IError {
	team, iErr := t.GetTeamById(teamId)
	if iErr != nil {
		return iErr
	}
	if team == nil {
		return utils.NewError(TErrTeamNotExist, utils.M{"teamId": teamId})
	}
	if team.CurrentNumber+1 > 15 {
		return utils.NewError(TErrTeamCharacterCap, utils.M{"currentCap": team.CurrentNumber})
	}
	if strings.Contains(team.TeamMember, fmt.Sprintf("%d", cid)) {
		return utils.NewError(TErrCharacterAlreadyJoinTeam, utils.M{"teamMemberList": team.TeamMember, "joinCid": cid})
	}
	memberList := model.GetTeamMemberList(team.TeamMember)
	upMap := make(map[string]interface{})
	upMap["current_number"] = team.CurrentNumber + 1
	upMap["team_member"] = model.GetTeamMemberString(append(memberList, cid)...)
	team.CurrentNumber += 1
	team.TeamMember = model.GetTeamMemberString(append(memberList, cid)...)
	iErr = t.UpDateTeamByUpMap(team.Id, upMap)
	if iErr != nil {
		return iErr
	}
	t.NewTeamMemberNotify(ac.Tid(), team.Id, cid, memberList...)
	t.TeamLoadNotify(ac.Tid(),cid,team)
	return nil
}

//LeaveTeam 离开队伍 character leave team by team id and character id
func (t *teamService) LeaveTeam(ac common.IActionCtx, teamId, cid int64) utils.IError {
	team, iErr := t.GetTeamById(teamId)
	if iErr != nil {
		return iErr
	}
	memberList := model.GetTeamMemberList(team.TeamMember)
	if strings.Contains(team.TeamMember, fmt.Sprintf("%d", cid)) {
		if team.LeaderId != cid {
			members := make([]int64, len(memberList))
			copy(members, memberList)
			members = delArray(members, cid)
			upMap := make(map[string]interface{})
			upMap["current_number"] = team.CurrentNumber - 1
			upMap["team_member"] = model.GetTeamMemberString(members...)
			iErr = t.UpDateTeamByUpMap(team.Id, upMap)
			if iErr != nil {
				return iErr
			}
			t.DispatchEvent(misc.NodeGame, ac.Tid(), cid, msg_dt.CdEveTeamMemberChange,
				&msg_dt.EveTeamMemberChange{MemberList: memberList, TeamId: team.Id, Cid: cid, Type: 1})
		} else {
			if team.CurrentNumber > 1 {
				members := make([]int64, len(memberList))
				copy(members, memberList)
				members = delArray(members, cid)
				upMap := make(map[string]interface{})
				upMap["leader_id"] = members[0]
				upMap["current_number"] = team.CurrentNumber - 1
				upMap["team_member"] = model.GetTeamMemberString(members...)
				iErr = t.UpDateTeamByUpMap(team.Id, upMap)
				if iErr != nil {
					return iErr
				}
				//先转移队长
				t.DispatchEvent(misc.NodeGame, ac.Tid(), cid, msg_dt.CdEveTeamMemberChange,
					&msg_dt.EveTeamMemberChange{MemberList: memberList, TeamId: team.Id, Cid: members[0], Type: 4})
				//离开队伍
				t.DispatchEvent(misc.NodeGame, ac.Tid(), cid, msg_dt.CdEveTeamMemberChange,
					&msg_dt.EveTeamMemberChange{MemberList: memberList, TeamId: team.Id, Cid: cid, Type: 1})
			} else {
				iErr = t.DisbandTeam(ac, cid)
				if iErr != nil {
					return iErr
				}
			}
		}
	} else {
		return utils.NewError(TErrCharacterNotJoinTeam, utils.M{"teamMemberList": team.TeamMember, "needJoinTeamCharacterId": cid})
	}
	return nil
}

//DisbandTeam 解散队伍 disband team by team id and character id
func (t *teamService) DisbandTeam(ac common.IActionCtx, cid int64) utils.IError {
	team, iErr := t.GetTeamByLeaderId(cid)
	if iErr != nil {
		return iErr
	}
	iErr = t.DeleteTeam(team.Id)
	if iErr != nil {
		return iErr
	}
	t.DispatchEvent(misc.NodeGame, ac.Tid(), cid, msg_dt.CdEveTeamMemberChange,
		&msg_dt.EveTeamMemberChange{MemberList: model.GetTeamMemberList(team.TeamMember), TeamId: team.Id, Cid: cid, Type: 3})
	return nil
}

//PleaseLeaveTheTeam 请离队伍 please leave the team
func (t *teamService) PleaseLeaveTheTeam(ac common.IActionCtx, cid, oid int64) (upMap map[string]interface{},team *model.Team,iErr utils.IError) {
	team, iErr = t.GetTeamByLeaderId(cid)
	if iErr != nil {
		return nil,nil,iErr
	}
	if strings.Contains(team.TeamMember, fmt.Sprintf("%d", oid)) {
		memberList := model.GetTeamMemberList(team.TeamMember)
		members := make([]int64, len(memberList))
		copy(members, memberList)
		members = delArray(members, oid)
		upMap = make(map[string]interface{})
		upMap["current_number"] = team.CurrentNumber - 1
		upMap["team_member"] = model.GetTeamMemberString(members...)
	} else {
		return nil,nil,utils.NewError(TErrCharacterNotJoinTeam, utils.M{"teamMemberList": team.TeamMember, "needJoinTeamCharacterId": cid})
	}
	return
}

//UpdateTeamAutoJoin 设置队伍自动加入 update team auto join
func (t *teamService) UpdateTeamAutoJoin(cid int64) (team *model.Team,iErr utils.IError) {
	team, iErr = t.GetTeamByLeaderId(cid)
	if iErr != nil {
		return nil,iErr
	}
	if team == nil {
		return nil,utils.NewError(TErrTeamNotExist, utils.M{"cid": cid})
	}
	if team.LeaderId != cid {
		return nil,utils.NewError(errors.New(TErrCharacterNotTeamLeader).Error(), utils.M{"team leader": team.LeaderId, "cid": cid})
	}
	team.AutoAgree = !team.AutoAgree
	return team,nil
}

//UpTeamAims 设置队伍目标 update team aims
func (t *teamService) UpTeamAims(ac common.IActionCtx, cid int64, teamArms msg_dt.TeamArms) (team *model.Team,upMap map[string]interface{},iErr utils.IError) {
	team, iErr = t.GetTeamByLeaderId(cid)
	if iErr != nil {
		return nil,nil,iErr
	}
	if team == nil {
		return nil,nil,utils.NewError(TErrTeamNotExist, utils.M{"cid": cid})
	}
	upMap = map[string]interface{}{
		"team_aims_tem_id": teamArms.TeamAimsId,
		"min_age_claim":      teamArms.MinAgeNum,
		"max_age_claim":      teamArms.MaxAgeNum,
		"power_claim":        teamArms.PowerNum,
	}
	team.TeamAimsTemId = teamArms.TeamAimsId
	team.MinAgeClaim = teamArms.MinAgeNum
	team.MaxAgeClaim = teamArms.MaxAgeNum
	team.PowerClaim = teamArms.PowerNum
	return team,upMap,nil
}

//TransferTeam 移交队伍
func (t *teamService) TransferTeam(ac common.IActionCtx, cid, oid int64) utils.IError {
	team, iErr := t.GetTeamByLeaderId(cid)
	if iErr != nil {
		return iErr
	}
	if team == nil {
		return utils.NewError(TErrTeamNotExist, utils.M{"cid": cid})
	}
	if strings.Contains(team.TeamMember, fmt.Sprintf("%d", oid)) {
		upMap := make(map[string]interface{})
		upMap["leader_id"] = oid
		iErr = t.UpDateTeamByUpMap(team.Id, upMap)
		if iErr != nil {
			return iErr
		}
		t.DispatchEvent(misc.NodeGame, ac.Tid(), cid, msg_dt.CdEveTeamMemberChange,
			&msg_dt.EveTeamMemberChange{MemberList: model.GetTeamMemberList(team.TeamMember), TeamId: team.Id, Cid: oid, Type: 4})
	} else {
		return utils.NewError(TErrCharacterNotJoinTeam, utils.M{"cid": oid})
	}
	return nil
}

//OneClickApply 一键申请
func (t *teamService) OneClickApply(ac common.IActionCtx, cid int64, armsId, age, battlePower int32) (int64,utils.IError) {
	teams, iErr := t.GetTeamByTeamAimsTemId(armsId)
	if iErr != nil {
		return 0,iErr
	}
	if len(teams) > 0 {
		applyTeamIds := make([]*model.Team, 0)
		for _, team := range teams {
			//判断战力、年龄
			if team.TeamAimsTemId == 0 || battlePower >= team.PowerClaim && age > team.MaxAgeClaim &&
				age < team.MinAgeClaim {
				applyTeamIds = append(applyTeamIds, team)
			}
			//判断是否自动同意加入队伍
			if team.AutoAgree {
				iErr = t.AgreeInviteJoinTeam(ac, team.Id, cid)
				if iErr != nil {
					return 0,iErr
				}
				return team.Id,nil
			}
		}
		if len(applyTeamIds) > 0 {
			for _, applyTeam := range applyTeamIds {
				iErr = t.CreateTeamRelationship(applyTeam.Id, cid, team_e.ApplyTeam)
				if iErr != nil {
					app.Log().TError(ac.Tid(), iErr)
					continue
				}
				//通知队长
				t.DispatchEvent(misc.NodeGame, ac.Tid(), cid, msg_dt.CdEveTeamApplyNotify, &msg_dt.EveTeamApplyNotify{Cid: applyTeam.LeaderId})
			}
		}
	}
	return 0,nil
}

//队伍成员位置切换
func (t *teamService) TeamMemberLocationCutOver(teamId, cid, oid int64) utils.IError {
	team, iErr := t.GetTeamById(teamId)
	if iErr != nil {
		return iErr
	}
	if !strings.Contains(team.TeamMember, fmt.Sprintf("%d", cid)) {
		return utils.NewError(TErrCharacterNotJoinTeam, utils.M{"cid": cid})
	}
	if !strings.Contains(team.TeamMember, fmt.Sprintf("%d", oid)) {
		return utils.NewError(TErrCharacterNotJoinTeam, utils.M{"oid": oid})
	}
	if team.LeaderId == cid || team.LeaderId == oid {
		return utils.NewError(TErrTeamLeaderNotLocationCutOver, utils.M{"oid": oid})
	}
	list := model.GetTeamMemberList(team.TeamMember)
	for _, memberId := range list {
		if memberId == cid {
			memberId = oid
			continue
		}
		if memberId == oid {
			memberId = cid
			continue
		}
	}

	iErr = t.UpDateTeamByUpMap(teamId, map[string]interface{}{"team_member": model.GetTeamMemberString(list...)})
	if iErr != nil {
		return iErr
	}
	return nil

}

//NewTeamMemberNotify 新成员通知 new team member notify
func (t *teamService) NewTeamMemberNotify(tid, teamId, cid int64, oidList ...int64) {
	t.DispatchEvent(misc.NodeGame, tid, cid, msg_dt.CdEveTeamAddNewMemberNotify, &msg_dt.EveTeamAddNewMemberNotify{
		Cid:     cid,
		TeamId:  teamId,
		OidList: oidList,
	})
}

//TeamLoadNotify 队伍加载通知 team load notify
func (t *teamService) TeamLoadNotify(tid, cid int64, team *model.Team) {
	t.DispatchEvent(misc.NodeGame, tid, cid, msg_dt.CdEveTeamLoadChange, &msg_dt.EveTeamLoadChange{
		Cid:  cid,
		Team: team,
	})
}

//delArray team member list del common
func delArray(members []int64, removeCId int64) (cidList []int64) {
	cidList = make([]int64, 0)
	for _, cid := range members {
		if cid != removeCId {
			cidList = append(cidList, cid)
		}
	}
	return cidList
}
