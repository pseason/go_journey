package backpack_shortcut_bar

import (
	"95eh.com/eg/utils"
	jsoniter "github.com/json-iterator/go"
	"hm/pkg/misc/cache_key"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"strconv"
)

const (
	MaxShortcutBarIndex = 3
)

type shortcutBarService struct {
	common.IBaseService
	pTsv map[int32]*tsv.PropTsv
}

func NewShortcutBarService() IShortcutBarService {
	r := &shortcutBarService{
		IBaseService: common.NewBaseService(&model.BackpackShortcutBar{}),
	}
	return r
}

func (stbSvc *shortcutBarService) AfterInit() error {
	stbSvc.pTsv = tsv.GetTsvManager(tsv.Prop).(*tsv.PropTsvManager).TsvMap
	return nil
}

func (stbSvc *shortcutBarService) GetConversionRecord(ac common.IActionCtx, Cid int64) (res *model.BackpackShortcutBar, err utils.IError) {
	ads := model.BackpackShortcutBar{}
	ads.Data = "[0,0,0,0]"
	errDb := stbSvc.Db().Model(model.BackpackShortcutBar{}).Where(&model.BackpackShortcutBar{Cid: Cid}).FirstOrCreate(&ads).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	var ds []int32
	errJson := jsoniter.Unmarshal([]byte(ads.Data), &ds)
	if errJson != nil {
		err = utils.NewError(TShortcutBarConversionFail,nil)
		return
	}
	ads.Ds = ds
	stbSvc.GetCdsByCache(Cid,&ads)
	res = &ads
	return
}

func (stbSvc *shortcutBarService) PutInto(ac common.IActionCtx, Cid int64, PropId int32, Index int32) (res *model.BackpackShortcutBar, err utils.IError) {
	if Index > MaxShortcutBarIndex || Index < 0 {
		err = utils.NewError(TShortcutBarTorIndex,nil)
		return
	}

	propTsv, has := stbSvc.pTsv[PropId]
	if !has {
		err = utils.NewError(TShortcutBarNotHas,nil)
		return
	}
	if propTsv.EdibleType == 0 {
		err = utils.NewError(TShortcutBarDontIn,nil)
		return
	}
	res, err = stbSvc.GetConversionRecord(ac, Cid)
	if err != nil {
		return
	}
	if res.Ds[Index] == PropId {
		err = utils.NewError(TShortcutBarHas,nil)
		return
	}
	//快捷栏物品-移动|交换位置
	for i,pId:=range res.Ds{
		if pId == PropId {
			res.Ds[i] = 0
			if res.Ds[Index] >0 {
				res.Ds[i] = res.Ds[Index]
			}
		}
	}

	res.Ds[Index] = PropId
	err = stbSvc.updateShortcutBar(ac, res)
	if err != nil{
		return
	}
	stbSvc.GetCdsByCache(Cid,res)
	return
}

func (stbSvc *shortcutBarService) GetCdsByCache(Cid int64,res *model.BackpackShortcutBar){
	//增加使用消耗品501的cd值
	res.Cd = []int64{0,0,0,0}
	for index, propId := range res.Ds {
		if propId > 0 {
			cacheKey := cache_key.ShortcutBarFreeze.Format(Cid, stbSvc.pTsv[propId].EdibleType)
			cacheValue, err := stbSvc.Redis().Get(stbSvc.Context(), cacheKey).Result()
			if err != nil {
				res.Cd[index] = 0
				continue
			}
			cacheInt, err := strconv.ParseInt(cacheValue, 10, 64)
			if err != nil {
				res.Cd[index] = 0
				continue
			}
			res.Cd[index] = int64(cacheInt)
		}
	}
}

func (stbSvc *shortcutBarService) PutProps(ac common.IActionCtx,Cid int64, props map[int32]int32) (res *model.BackpackShortcutBar,err utils.IError) {
	res, trace := stbSvc.GetConversionRecord(ac,Cid)
	if trace != nil {
		return
	}
	for k, v := range props {
		if v-1 > MaxShortcutBarIndex || v < 1 {
			err = utils.NewError(TShortcutBarTorIndex,nil)
			return
		}

		propCsv, has := stbSvc.pTsv[k]
		if !has {
			err = utils.NewError(TBackPackGetTemplateFail,nil)
			continue
		}
		if propCsv.EdibleType == 0 {
			err = utils.NewError(TShortcutBarDontIn,nil)
			continue
		}
		res.Ds[v-1] = k
	}
	err = stbSvc.updateShortcutBar(ac, res)
	if err != nil{
		return
	}
	stbSvc.GetCdsByCache(Cid,res)
	return
}

func (stbSvc *shortcutBarService) Remove(ac common.IActionCtx, Cid int64, Index int32) (res *model.BackpackShortcutBar, err utils.IError) {
	if Index > MaxShortcutBarIndex || Index < 0 {
		err = utils.NewError(TShortcutBarTorIndex,nil)
		return
	}
	res, err = stbSvc.GetConversionRecord(ac, Cid)
	if err != nil {
		return
	}
	res.Ds[Index] = 0
	err = stbSvc.updateShortcutBar(ac, res)
	if err != nil{
		return
	}
	stbSvc.GetCdsByCache(Cid,res)
	return
}

func (stbSvc *shortcutBarService) updateShortcutBar(ac common.IActionCtx, record *model.BackpackShortcutBar) (err utils.IError) {
	mds, errJson := jsoniter.Marshal(record.Ds)
	if errJson != nil {
		err = utils.NewError(TShortcutBarConversionFail,nil)
		return
	}
	errDb := stbSvc.Db().Model(model.BackpackShortcutBar{}).Where("id = ?", record.Id).Updates(&model.BackpackShortcutBar{Data: string(mds)}).Error
	if errDb != nil {
		err = utils.NewError(errDb.Error(),nil)
		return
	}
	return nil
}
