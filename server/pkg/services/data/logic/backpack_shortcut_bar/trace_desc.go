package backpack_shortcut_bar


const (
	//快捷栏数据查询失败
	TShortcutBarDbFindFail = "Failed to query shortcut bar data"
	//快捷栏数据更新失败
	TShortcutBarDbUpFail = "Failed to update shortcut bar data"
	//快捷栏数据增加失败
	TShortcutBarDbSaveFail = "Failed to add shortcut bar data"
	//快捷栏数据删除失败
	TShortcutBarDbDelFail = "Failed to delete shortcut bar data"
	//获取物品模板失败
	TBackPackGetTemplateFail = "Failed to get the item template"
	//快捷栏数据转换异常
	TShortcutBarConversionFail = "The shortcut bar data conversion is abnormal"
	//快捷栏已存在此物品
	TShortcutBarHas = "This item already exists in the shortcut bar"
	//快捷栏不存在此物品
	TShortcutBarNotHas = "This item does not exist in the shortcut bar"
	//无效的快捷栏索引
	TShortcutBarTorIndex = "Invalid shortcut bar index"
	//此物品无法放入快捷栏
	TShortcutBarDontIn = "This item cannot be placed in the shortcut bar"
)
