package backpack_shortcut_bar

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type IShortcutBarService interface {
	common.IBaseService
	//获取快捷栏信息
	GetConversionRecord(ac common.IActionCtx, Cid int64) (res *model.BackpackShortcutBar, err utils.IError)
	//放入快捷栏
	PutInto(ac common.IActionCtx, Cid int64, propId int32, index int32) (res *model.BackpackShortcutBar, err utils.IError)
	// 移除快捷栏
	Remove(ac common.IActionCtx, Cid int64, index int32) (res *model.BackpackShortcutBar, err utils.IError)
	//多个物品放入快捷栏propId=>Index
	PutProps(ac common.IActionCtx, Cid int64, props map[int32]int32) (res *model.BackpackShortcutBar, err utils.IError)
}

func (stbSvc *shortcutBarService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode){
		msg_dt.CdShortcutBarList: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqShortcutBarList)
			res, err := stbSvc.GetConversionRecord(ac, req.Cid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrQueryFailed
			}
			return msg_dt.ResShortcutBarList{
				ShortcutBarList: res,
			}, 0
		},

		msg_dt.CdShortcutBarPutInto: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqShortcutBarPutInto)
			res, err := stbSvc.PutInto(ac, req.Cid, req.PropId, req.Index)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return msg_dt.ResShortcutBarPutInto{
				ShortcutBarList: res,
			}, 0
		},

		msg_dt.CdShortcutBarPutProps: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqShortcutBarPutProps)
			res, err := stbSvc.PutProps(ac, req.Cid, req.PutProps)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrCreateFailed
			}
			return msg_dt.ResShortcutBarPutProps{
				ShortcutBarList: res,
			}, 0
		},

		msg_dt.CdShortcutBarRemove: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqShortcutBarRemove)
			res, err := stbSvc.Remove(ac, req.Cid, req.Index)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				return nil, err_dt.ErrDelFailed
			}
			return msg_dt.ResShortcutBarRemove{
				ShortcutBarList: res,
			}, 0
		},
	}
}
