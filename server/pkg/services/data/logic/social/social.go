package social

import (
	"95eh.com/eg/utils"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/social_e"
	"hm/pkg/services/data/msg_dt"
	"time"
)

type socialService struct {
	common.IBaseService
}

func NewSocialService() *socialService {
	return &socialService{IBaseService: common.NewBaseService(&model.Social{})}
}

func (s *socialService) AfterInit() error {
	return nil
}

//GetSocialByCidAndOid 查询玩家与玩家对应关系 query character mapping relation
func (s *socialService) GetSocialByCidAndOid(cid, oid int64, friendRelationEnum ...social_e.FriendRelationEnum) (res *model.Social, iErr utils.IError) {
	db := s.Db().Model(&model.Social{})
	var t model.Social
	err := db.Where("cid = ? and oid = ? and relation_status in ?", cid, oid, friendRelationEnum).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{
				"cid":            cid,
				"oid":            oid,
				"relationStatus": friendRelationEnum,
			})
		}
	}
	if t.Id > 0 {
		res = &t
	}
	return res, nil
}

//CreateSocial 添加玩家社交关系 add character social relation
func (s *socialService) CreateSocial(ac common.IActionCtx, social ...*model.Social) utils.IError {
	db := ac.TxDb().Model(&model.Social{})
	err := db.Create(social).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{
			"social data": social,
		})
	}
	return nil
}

func (s *socialService) UpdateSocialByUpMap(ac common.IActionCtx, socialId []int64, upMap map[string]interface{}) utils.IError {
	db := ac.TxDb().Model(&model.Social{})
	err := db.Where("id in ?", socialId).Updates(upMap).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"socialId": socialId, "upMap": upMap})
	}
	return nil
}

func (s *socialService) DelSocialById(ac common.IActionCtx, socialId ...int64) utils.IError {
	db := ac.TxDb().Model(&model.Social{})
	err := db.Unscoped().Where("id in ?", socialId).Delete(&model.Social{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"socialId": socialId})
	}
	return nil
}

//SocialApply 社交关系申请 social Apply
func (s *socialService) SocialApply(ac common.IActionCtx, cid, oid int64) (addData *model.Social, iErr utils.IError) {
	social, iErr := s.GetSocialByCidAndOid(cid,oid, social_e.BlacklistStatus, social_e.BlacklistAndEnemyStatus)
	if iErr != nil {
		return nil, iErr
	}
	if social != nil {
		return nil, utils.NewError(TErrCharacterIsBlackList,utils.M{"social": social})
	}
	social, iErr = s.GetSocialByCidAndOid(oid, cid,social_e.ApplyStatus, social_e.BlacklistStatus, social_e.BlacklistAndEnemyStatus,social_e.BuddyStatus)
	if iErr != nil {
		return nil, iErr
	}
	if social != nil {
		return nil, utils.NewError(TErrCharacterIsBlackList,utils.M{"social": social})
	}
	buddyLimit := s.Conf().Design.Social.BuddyLimit
	count, iErr := s.GetSocialCountByCid(cid)
	if iErr != nil {
		return nil, iErr
	}
	if int32(count) >= buddyLimit {
		return nil,utils.NewError(TErrBuddyIsLimit,utils.M{"cid":oid,"count":count})
	}
	count, iErr = s.GetSocialCountByCid(oid)
	if iErr != nil {
		return nil, iErr
	}
	if int32(count) >= buddyLimit {
		return nil,utils.NewError(TErrBuddyIsLimit,utils.M{"cid":oid,"count":count})
	}
	if iErr != nil {
		return nil, iErr
	}
	if social == nil {
		addData = &model.Social{
			Cid:            oid,
			Oid:            cid,
			RelationStatus: social_e.ApplyStatus,
		}
	}

	return addData, nil
}

//AgreeBuddyApply 同意好友申请 agree buddy apply
func (s *socialService) AgreeBuddyApply(ac common.IActionCtx,cid, oid int64) ([]int64, *model.Social, utils.IError) {
	social, iErr := s.GetSocialByCidAndOid(cid, oid, social_e.ApplyStatus)
	if iErr != nil {
		return nil, nil, iErr
	}
	if social == nil {
		return nil, nil, utils.NewError(TErrSocialApplyNotExits, utils.M{"cid": cid, "oid": oid})
	}
	upCidList := make([]int64,0)
	upCidList = append(upCidList,social.Id)
	social1, iErr := s.GetSocialByCidAndOid(oid,cid, social_e.BuddyStatus,social_e.BlacklistStatus)
	if iErr != nil {
		return nil, nil, iErr
	}
	if social1 != nil {
		iErr =s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
			db := txAc.TxDb().Model(&model.Social{})
			err := db.Unscoped().Where("cid = ? and oid = ? and relation_status = ? ",cid,oid,social_e.ApplyStatus).Delete(&model.Social{}).Error
			if err != nil {
				iError = utils.NewError(err.Error(), utils.M{"cid": cid, "oid": oid})
				return
			}
			db1 := txAc.TxDb().Model(&model.Social{})
			err = db1.Unscoped().Where("cid = ? and oid = ? and relation_status = ? ",oid,cid,social_e.ApplyStatus).Delete(&model.Social{}).Error
			if err != nil {
				iError = utils.NewError(err.Error(), utils.M{"cid": cid, "oid": oid})
				return
			}
			return
		})
		if iErr != nil {
			return nil,nil,iErr
		}
		return nil, nil, utils.NewError(TErrCharacterIsBlackList, utils.M{"cid": cid, "oid": oid})
	}
	social2, iErr := s.GetSocialByCidAndOid(oid, cid, social_e.ApplyStatus,social_e.BlacklistStatus)
	if iErr != nil {
		return nil, nil, iErr
	}
	if social2 != nil {
		if social2.RelationStatus == social_e.ApplyStatus {
			upCidList = append(upCidList,social2.Id)
			return upCidList,nil,nil
		}else if social2.RelationStatus == social_e.ApplyStatus {
			iErr =s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
				db := txAc.TxDb().Model(&model.Social{})
				err := db.Unscoped().Where("cid = ? and oid = ? ",cid,oid).Delete(&model.Social{}).Error
				if err != nil {
					iError = utils.NewError(err.Error(), utils.M{"cid": cid, "oid": oid})
					return
				}
				db1 := txAc.TxDb().Model(&model.Social{})
				err = db1.Unscoped().Where("cid = ? and oid = ? ",oid,cid).Delete(&model.Social{}).Error
				if err != nil {
					iError = utils.NewError(err.Error(), utils.M{"cid": cid, "oid": oid})
					return
				}
				return
			})
			if iErr != nil {
				return nil,nil,iErr
			}
			return nil, nil, utils.NewError(TErrCharacterIsBlackList, utils.M{"cid": cid, "oid": oid})
		}
	}
	buddyLimit := s.Conf().Design.Social.BuddyLimit
	count, iErr := s.GetSocialCountByCid(cid)
	if iErr != nil {
		return nil,nil, iErr
	}
	if int32(count) >= buddyLimit {
		return nil,nil,utils.NewError(TErrBuddyIsLimit,utils.M{"cid":oid,"count":count})
	}
	count, iErr = s.GetSocialCountByCid(oid)
	if iErr != nil {
		return nil,nil, iErr
	}
	if int32(count) >= buddyLimit {
		return nil,nil,utils.NewError(TErrBuddyIsLimit,utils.M{"cid":oid,"count":count})
	}
	return []int64{social.Id}, &model.Social{Cid: oid, Oid: cid, RelationStatus: social_e.BuddyStatus}, nil
}

//RefuseBuddyApply 拒绝好友申请  refuse buddy apply
func (s *socialService) RefuseBuddyApply(ac common.IActionCtx, cid, oid int64) (int64, utils.IError) {
	social, iErr := s.GetSocialByCidAndOid(cid, oid, social_e.ApplyStatus)
	if iErr != nil {
		return 0, iErr
	}
	if social == nil {
		return 0, utils.NewError(TErrSocialApplyNotExits, utils.M{"cid": cid, "oid": oid})
	}

	return social.Id, nil
}

//GetUnHandelApplyCount 查询玩家好友申请数量
func (s *socialService) GetUnHandelApplyCount(cid int64) (count int64,iErr utils.IError) {
	db := s.Db().Model(&model.Social{})
	err := db.Where("cid = ? and  relation_status = ?", cid, 0).Count(&count).Error
	if err != nil {
		return count,utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return
}

func (s *socialService) GetSocialCountByCid(cid int64) (count int64,iErr utils.IError) {
	db := s.Db().Model(&model.Social{})
	err := db.Where("cid = ?",cid).Count(&count).Error
	if err != nil {
		return count,utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return count,nil
}

//GetAllSocialByCid 查询玩家所有社交关系 query character all social relation by cid
func (s *socialService) GetAllSocialByCid(cid int64,page, size int32,socialType []social_e.FriendRelationEnum) (count int64,res []*model.Social, iErr utils.IError) {
	db := s.Db().Model(&model.Social{})
	err := db.Where("cid = ? and relation_status in ?", cid, socialType).Count(&count).Error
	if err != nil {
		return count,nil, utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	err = db.Offset(int(page * size)).Limit(int(size)).Find(&res).Error
	if err != nil {
		return count,nil, utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return count,res,nil
}

//AddBlacklist 添加黑名单
func (s *socialService) AddBlacklist(ac common.IActionCtx, cid, oid int64) utils.IError {
	social, iErr := s.GetSocialByCidAndOid(cid, oid, social_e.BuddyStatus,
		social_e.BlacklistStatus,
		social_e.EnemyStatus,
		social_e.BlacklistAndEnemyStatus)
	if iErr != nil {
		return iErr
	}
	if social != nil {
		switch social.RelationStatus {
		case social_e.BuddyStatus:
			iErr = s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
				iError = s.UpdateSocialByUpMap(txAc, []int64{social.Id}, map[string]interface{}{"relation_status": int8(social_e.BlacklistStatus)})
				if iError != nil {
					return
				}
				social1, iError := s.GetSocialByCidAndOid(oid, cid, social_e.BuddyStatus)
				if iError != nil {
					return
				}
				if social1 != nil {
					iError = s.DelSocialById(txAc, social1.Id)
				}
				//join buddy notify
				s.DispatchEvent(misc.NodeGame,ac.Tid(),oid,msg_dt.CdEveSocialJoinBuddyNotify,&msg_dt.EveSocialJoinBuddyNotify{
					Cid: oid,
					Oid: cid,
					SocialType: 3,
				})
				return
			})

			if iErr != nil {
				return iErr
			}
		case social_e.BlacklistStatus:
			fallthrough
		case social_e.BlacklistAndEnemyStatus:
			return utils.NewError(TErrCharacterJoinBlacklistFailed, utils.M{
				"cid": cid,
				"oid": oid,
			})
		case social_e.EnemyStatus:
			iErr = s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
				iError = s.UpdateSocialByUpMap(txAc, []int64{social.Id}, map[string]interface{}{"relation_status": int8(social_e.BlacklistAndEnemyStatus)})
				return
			})
			if iErr != nil {
				return iErr
			}
		}
	} else {
		iErr = s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
			iError = s.CreateSocial(txAc, &model.Social{Cid: cid, Oid: oid, RelationStatus: social_e.BlacklistStatus})
			return
		})
		if iErr != nil {
			return iErr
		}
	}

	return nil
}

//AddEnemy 添加仇人
func (s *socialService) AddEnemy(ac common.IActionCtx, cid, oid int64) utils.IError {
	social, iErr := s.GetSocialByCidAndOid(cid, oid, social_e.BuddyStatus,
		social_e.BlacklistStatus,
		social_e.EnemyStatus,
		social_e.BlacklistAndEnemyStatus)
	if iErr != nil {
		return iErr
	}
	if social != nil {
		switch social.RelationStatus {
		case social_e.BuddyStatus:
		case social_e.BlacklistStatus:
			iErr = s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
				iError = s.UpdateSocialByUpMap(txAc, []int64{social.Id}, map[string]interface{}{
					"relation_status": int8(social_e.BlacklistAndEnemyStatus),
					"join_enemy_time": time.Now().Second(),
				})
				return
			})

			if iErr != nil {
				return iErr
			}
		case social_e.BlacklistAndEnemyStatus:
			fallthrough
		case social_e.EnemyStatus:
			return utils.NewError(TErrCharacterJoinEnemyFailed, utils.M{
				"cid": cid,
				"oid": oid,
			})
		}
	} else {
		iErr = s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
			iError = s.CreateSocial(txAc, &model.Social{Cid: cid, Oid: oid, RelationStatus: social_e.EnemyStatus, JoinEnemyTime: int32(time.Now().Second())})
			return
		})
		if iErr != nil {
			return iErr
		}
	}

	return nil
}

//删除好友
func (s *socialService) DeleteBuddy(cid, oid int64) ([]int64, utils.IError) {
	social, iErr := s.GetSocialByCidAndOid(cid, oid, social_e.BuddyStatus)
	if iErr != nil {
		return nil, iErr
	}
	if social == nil {
		return nil, utils.NewError(TErrCharacterNotBuddy, utils.M{"cid": cid, "oid": oid})
	}
	social1, iErr := s.GetSocialByCidAndOid(oid, cid, social_e.BuddyStatus)
	if iErr != nil {
		return nil, iErr
	}
	if social1 == nil {
		return nil, utils.NewError(TErrCharacterNotBuddy, utils.M{"cid": cid, "oid": oid})
	}

	return []int64{social.Id,social1.Id}, nil
}

//删除黑名单
func (s *socialService) DeleteBlacklist(ac common.IActionCtx, social *model.Social) utils.IError {
	iErr := s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
		switch social.RelationStatus {
		case social_e.BlacklistStatus:
			iError = s.DelSocialById(txAc, social.Id)
		case social_e.BlacklistAndEnemyStatus:
			iError = s.UpdateSocialByUpMap(txAc, []int64{social.Id}, map[string]interface{}{
				"relation_status": int8(social_e.EnemyStatus),
			})
		}
		return
	})

	if iErr != nil {
		return iErr
	}
	return nil
}

//删除仇人
func (s *socialService) DeleteEnemy(ac common.IActionCtx, social *model.Social) utils.IError {
	iErr := s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
		switch social.RelationStatus {
		case social_e.EnemyStatus:
				iError = s.DelSocialById(txAc, social.Id)
		case social_e.BlacklistAndEnemyStatus:
				iError = s.UpdateSocialByUpMap(txAc, []int64{social.Id}, map[string]interface{}{
					"relation_status": int8(social_e.BlacklistStatus),
				})
		}
		return
	})

	if iErr != nil {
		return iErr
	}
	return nil
}

//获取社交列表
func (s *socialService) GetBuddyApplyList(cid int64,socialType ...social_e.FriendRelationEnum) (cidList []int64, iErr utils.IError) {
	db := s.Db().Model(&model.Social{})
	cidList = make([]int64, 0)
	err := db.Select("oid").Where("cid = ? and relation_status in ?", cid, socialType).Find(&cidList).Error
	if err != nil {
		return nil, utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return cidList,nil
}