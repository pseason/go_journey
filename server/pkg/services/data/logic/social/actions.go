package social

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/misc"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model/enum/social_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)


func (s *socialService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		msg_dt.CdSocialGetAllSocial: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqSocialGetAllSocial)
			count,socials, iErr := s.GetAllSocialByCid(req.Cid,req.Page,req.Size,req.SocialType)
			if iErr != nil {
				app.Log().TError(ac.Tid(),iErr)
				return nil,err_dt.ErrQueryFailed
			}
			return &msg_dt.ResSocialGetAllSocial{SocialAll: socials,Count: int32(count)},0
		},
		msg_dt.CdSocialUnReadBuddyApplyCount: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqSocialUnReadBuddyApplyCount)
			count, iErr := s.GetUnHandelApplyCount(req.Cid)
			if iErr != nil {
				app.Log().TError(ac.Tid(),iErr)
				return nil,err_dt.ErrQueryFailed
			}
			return &msg_dt.ResSocialUnReadBuddyApplyCount{Count: int32(count)},0
		},
		//
		msg_dt.CdSocialGetBuddyCidList: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqSocialGetBuddyCidList)
			cidList, iErr := s.GetBuddyApplyList(req.Cid,req.SocialType...)
			if iErr != nil {
				app.Log().TError(ac.Tid(),iErr)
				return nil,err_dt.ErrQueryFailed
			}
			return &msg_dt.ResSocialGetBuddyCidList{CidList: cidList},0
		},
	}
}

func (s *socialService) TxActions() map[msg_dt.MsgCode]*common.TxHandler  {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdSocialTryApplyJoinBuddy:{
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqSocialTryApplyJoinBuddy)
				addSocial,err := s.SocialApply(ac,req.Cid,req.Oid)
				if err != nil {
					app.Log().TError(ac.Tid(),err)
					return nil,err_dt.ErrCreateFailed
				}
				return &msg_dt.ResSocialTryApplyJoinBuddy{
					AddSocial: addSocial,
				},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqSocialTryApplyJoinBuddy)
				res:= tryResBody.(*msg_dt.ResSocialTryApplyJoinBuddy)
				s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iErr utils.IError) {
					iError = s.CreateSocial(txAc,res.AddSocial)
					if iError != nil{
						iErr = iError
					}
					return
				})
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				//申请好友通知
				s.DispatchEvent(misc.NodeGame,ac.Tid(),res.AddSocial.Id,msg_dt.CdEveSocialJoinBuddyNotify,&msg_dt.EveSocialJoinBuddyNotify{
					Cid: req.Oid,
					Oid: req.Cid,
					SocialType: 2,
				})
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},

		msg_dt.CdSocialTryAgreeFriends: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqSocialTryAgreeFriends)
				upDataId, social, iError := s.AgreeBuddyApply(ac,req.Cid, req.Oid)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return nil,err_dt.ErrQueryFailed
				}
				if social == nil {
					return &msg_dt.ResSocialTryAgreeFriends{
						UpDataId: upDataId,
					},0
				}else{
					return &msg_dt.ResSocialTryAgreeFriends{
						UpDataId: upDataId,
						AddSocial: social,
					},0
				}

			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqSocialTryAgreeFriends)
				res := tryResBody.(*msg_dt.ResSocialTryAgreeFriends)
				iError = s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
					iError = s.UpdateSocialByUpMap(txAc,res.UpDataId, map[string]interface{}{"relation_status": int8(social_e.BuddyStatus)})
					if iError != nil {
						return
					}
					if res.AddSocial != nil {
						iError = s.CreateSocial(txAc,res.AddSocial)
						if iError != nil {
							return
						}
					}
					return
				})
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				//join buddy notify
				s.DispatchEvent(misc.NodeGame,ac.Tid(),req.Oid,msg_dt.CdEveSocialJoinBuddyNotify,&msg_dt.EveSocialJoinBuddyNotify{
					Cid: req.Oid,
					Oid: req.Cid,
					SocialType: 0,
				})
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdSocialTryRejectFriends: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqSocialTryRejectFriends)
				socialId, iError := s.RefuseBuddyApply(ac, req.Cid, req.Oid)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					errCode = err_dt.ErrQueryFailed
					return
				}
				return &msg_dt.ResSocialTryRejectFriends{
					SocialId: socialId,
				},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResSocialTryRejectFriends)
				s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iErr utils.IError) {
					iErr = s.DelSocialById(txAc,res.SocialId)
					if iError != nil {
						iError = iErr
						return
					}
					return
				})
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdSocialTryAddBlacklist: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return &msg_dt.ResSocialTryAddBlacklist{},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqSocialTryAddBlacklist)
				iError = s.AddBlacklist(ac, req.Cid, req.Oid)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}

				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdSocialTryAddEnemy: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return &msg_dt.ResSocialTryAddEnemy{},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqSocialTryAddEnemy)
				iError = s.AddEnemy(ac, req.Cid, req.Oid)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdSocialTryDeleteBuddy: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqSocialTryDeleteBuddy)
				socialIdList, iError := s.DeleteBuddy(req.Cid, req.Oid)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return nil,err_dt.ErrQueryFailed
				}
				return &msg_dt.ResSocialTryDeleteBuddy{SocialIdList: socialIdList},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResSocialTryDeleteBuddy)
				iError = s.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), iError utils.IError) {
					iError = s.DelSocialById(txAc,res.SocialIdList...)
					return
				})
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdSocialTryDeleteBlacklist: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqSocialTryDeleteBlacklist)
				social, iErr := s.GetSocialByCidAndOid(req.Cid, req.Oid, social_e.EnemyStatus,social_e.BlacklistStatus)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return nil,err_dt.ErrQueryFailed
				}
				if social == nil {
					app.Log().TError(ac.Tid(),utils.NewError(TErrCharacterNotBlacklist,utils.M{"cid":req.Cid,"oid": req.Oid}))
					return nil,err_dt.ErrQueryFailed
				}
				return &msg_dt.ResSocialTryDeleteBlacklist{Social: social},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := tryResBody.(*msg_dt.ResSocialTryDeleteBlacklist)
				iError = s.DeleteBlacklist(ac, req.Social)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdSocialTryDeleteEnemy: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqSocialTryDeleteEnemy)
				social, iErr := s.GetSocialByCidAndOid(req.Cid, req.Oid, social_e.EnemyStatus, social_e.BlacklistAndEnemyStatus)
				if iErr != nil {
					app.Log().TError(ac.Tid(),iErr)
					return nil,err_dt.ErrQueryFailed
				}
				if social == nil {
					app.Log().TError(ac.Tid(),utils.NewError(TErrCharacterNotBlacklist,utils.M{"cid":req.Cid,"oid": req.Oid}))
					return nil,err_dt.ErrQueryFailed
				}
				return &msg_dt.ResSocialTryDeleteEnemy{Social: social},0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := tryResBody.(*msg_dt.ResSocialTryDeleteEnemy)
				iError = s.DeleteEnemy(ac, req.Social)
				if iError != nil {
					app.Log().TError(ac.Tid(),iError)
					return iError
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
	}
}