package social

const (
	//好友申请记录不存在
	TErrSocialApplyNotExits = "not character buddy apply"
	//玩家加入黑名单失败
	TErrCharacterJoinBlacklistFailed = "character Has been join Blacklist"
	//玩家加入仇人失败
	TErrCharacterJoinEnemyFailed = "character Has been join Enemy"
	//玩家不在你的好友名单
	TErrCharacterNotBuddy = "character not user buddy"
	//玩家不在你的黑名单
	TErrCharacterNotBlacklist = "character not user Blacklist"
	//玩家不在你的仇人名单
	TErrCharacterNotEnemy = "character not user Enemy"
	//玩家已有該好友
	TErrCharacterIsBuddy = "character is my buddy"
	//玩家好友上限
	TErrBuddyIsLimit = "character buddy Limit"
	//玩家在黑名单中
	TErrCharacterIsBlackList = "character is blacklist"
)
