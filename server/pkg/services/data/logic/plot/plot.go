package plot

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	utils2 "hm/pkg/misc/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
)

type plotService struct {
	common.IBaseService
	template *tsv.PlotTsvManager
}

func NewPlotService() *plotService {
	return &plotService{
		IBaseService: common.NewBaseService(&model.Plot{}),
	}
}

func (p *plotService) AfterInit() error {
	p.template = tsv.GetTsvManager(tsv.Plot).(*tsv.PlotTsvManager)
	return nil
}

func (p *plotService) RegisterInitPlot(ac common.IActionCtx) (err error) {
	initId := p.Conf().Design.Plot.InitId
	if initId == 0 {
		return
	}
	template, has := p.template.TsvMap[initId]
	if !has {
		err = errors.New("init plot template not found ")
		app.Log().Error(err.Error(), utils.M{
			"ac":         ac,
			"templateId": initId,
		})
		return
	}
	entity := model.GeneratePlot(template, ac.SubjectId(), 0)
	err = p.Db().Model(&model.Plot{}).Create(entity).Error
	if err != nil {
		app.Log().Error("failed the init plot", utils.M{
			"ac":         ac,
			"templateId": initId,
		})
		return
	}
	//notify unlock service, execute commonUnlockOption
	p.DispatchEvent(misc.NodeGame, ac.Tid(), ac.SubjectId(), msg_dt.CdEveUnlockExecute, &msg_dt.EveUnlockExecute{
		Cid:         ac.SubjectId(),
		Opt:         model.UnlockCommonPlotObtain,
		TemplateIds: []int32{entity.TemplateId},
	})
	return
}

func (p *plotService) FightOption(ac common.IActionCtx, success bool) (changeFn func(), err error) {
	var plots []*model.Plot
	cid := ac.SubjectId()
	err = p.Db().Model(&model.Plot{}).Where(&model.Plot{Cid: cid, FightOpt: true, Status: model.PlotStatusLink}).
		Find(&plots).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = nil
			return
		}
		app.Log().Error("plot fight links not found", utils.M{
			"cid":    cid,
			"result": success,
		})
		return
	}
	adds := make([]*model.Plot, 0, 1)
	removeIds := make([]int64, 0, 1)
	changes := make([]*model.Plot, 0, 1)
	if len(plots) > 0 {
		for _, plot := range plots {
			plotTemplate, has := p.template.TsvMap[plot.TemplateId]
			if !has {
				continue
			}
			toPlotId := plotTemplate.Fight_plot[0]
			if !success {
				toPlotId = plotTemplate.Fight_plot[1]
			}
			if toPlotId > 0 {
				toPlotTemplate, has := p.template.TsvMap[toPlotId]
				if !has {
					continue
				}
				adds = append(adds, model.GeneratePlot(toPlotTemplate, cid, plot.InnerId))
				removeIds = append(removeIds, plot.Id)
				changes = append(changes, plot)
			}
		}
	}
	err = p.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
		if len(removeIds) > 0 {
			err := p.TxDb(txAc.TxDb()).Unscoped().Delete(&model.Plot{}, removeIds).Error
			if err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
		}
		if len(adds) > 0 {
			err := p.TxDb(txAc.TxDb()).Create(plots).Error
			if err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
		}
		return
	})
	if err != nil {
		return
	}
	adds = append(adds, changes...)
	changeFn = func() {
		p.ChangeNotify(ac, adds)
	}
	return
}

func (p *plotService) GetPlot(ac common.IActionCtx, plotId int64) (res *model.Plot, err error) {
	var plot model.Plot
	err = p.TxDb(ac.TxDb()).Model(&model.Plot{}).Where("id=?", plotId).Find(&plot).Error
	if err != nil {
		if !utils2.IsOrmNotFound(err) {
			return
		}
	}
	if plot.Id > 0 {
		res = &plot
	}
	return
}

func (p *plotService) LoadPlots(ac common.IActionCtx) (err error) {
	var plots []*model.Plot
	err = p.TxDb(ac.TxDb()).Model(&model.Plot{}).Where("cid=?", ac.SubjectId()).Find(&plots).Error
	if err != nil {
		if !utils2.IsOrmNotFound(err) {
			return
		}
	}
	return
}

func (p *plotService) Plots(ac common.IActionCtx, plotId int64) (res []*model.Plot, err error) {
	query := p.TxDb(ac.TxDb()).Where("cid=?", ac.SubjectId())
	if plotId > 0 {
		query.Where("id=?", plotId)
	}
	err = query.Find(&res).Error
	if err != nil {
		if !utils2.IsOrmNotFound(err) {
			return
		}
		res = make([]*model.Plot, 0)
	}
	return
}

func (p *plotService) BatchExecutePlot(ac common.IActionCtx, data *msg_dt.ReqPlotBatchExecute) (res *msg_dt.PlotBatchExecuteResBody, err error) {
	var beforeUpdate []*model.Plot
	err = p.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
		if len(data.Adds) > 0 {
			if err := p.TxDb(txAc.TxDb()).Create(&data.Adds).Error; err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
		}
		if len(data.Updates) > 0 {
			updateIds := p.GetModelSliceIdList(data.Updates)
			if err := p.TxDb(txAc.TxDb()).Model(&model.Plot{}).Where("id in ?", updateIds).Find(&beforeUpdate).Error; err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
			if err := p.TxDb(txAc.TxDb()).Updates(&data.Updates).Error; err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
		}
		if len(data.Removes) > 0 {
			if err := p.TxDb(txAc.TxDb()).Delete(&model.Plot{}, p.GetModelSliceIdList(data.Removes)).Error; err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
		}
		return
	})
	if err == nil {
		res = &msg_dt.PlotBatchExecuteResBody{
			Adds:          data.Adds,
			Updates:       data.Updates,
			BeforeUpdates: beforeUpdate,
			Removes:       data.Removes,
		}
	}
	return
}

func (p *plotService) BatchUpdateTaskRollback(ac common.IActionCtx, req *msg_dt.PlotBatchExecuteResBody) (err error) {
	err = p.LocalChainTx(ac, func(txAc common.IActionCtx) (commitCallback func(), sErr utils.IError) {
		if len(req.Adds) > 0 {
			if err := p.TxDb(txAc.TxDb()).Delete(&model.Plot{}, p.GetModelSliceIdList(req.Adds)).Error; err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
		}
		if len(req.Updates) > 0 {
			if err := p.TxDb(txAc.TxDb()).Updates(&req.Updates).Error; err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
		}
		if len(req.Removes) > 0 {
			if err := p.TxDb(txAc.TxDb()).Model(&model.Plot{}).Unscoped().Where("id in ?", p.GetModelSliceIdList(req.Removes)).
				Update("deleted", nil).Error; err != nil {
				sErr = utils.NewError(err.Error(), nil)
				return
			}
		}
		return
	})
	return
}

func (p *plotService) GetModelSliceIdList(models []*model.Plot) []int64 {
	res := make([]int64, 0, len(models))
	for _, v := range models {
		res = append(res, v.Id)
	}
	return res
}

func (p *plotService) GetModelSliceTemplateIdList(models []*model.Plot) []int32 {
	res := make([]int32, 0, len(models))
	for _, v := range models {
		res = append(res, v.TemplateId)
	}
	return res
}

func (p *plotService) ChangeNotify(ac common.IActionCtx, data []*model.Plot) {
	p.DispatchEvent(misc.NodeGame, ac.Tid(), ac.SubjectId(), msg_dt.CdEvePlotChange, &msg_dt.EvePlotChange{
		Cid:     ac.SubjectId(),
		Records: data,
	})
}
