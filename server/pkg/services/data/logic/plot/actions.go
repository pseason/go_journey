package plot

import (
	"95eh.com/eg/utils"
	"hm/pkg/misc"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

func (p *plotService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		msg_dt.CdPlotList: func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := body.(*msg_dt.ReqPlotList)
			plots, err := p.Plots(ac, req.PlotId)
			if err != nil {
				return nil, err_dt.ErrQueryFailed
			}
			return &msg_dt.ResPlotList{Records: plots}, utils.ErrCodeNil
		},
	}
}

func (p *plotService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdPlotBatchExecute: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqPlotBatchExecute)
				res, err := p.BatchExecutePlot(ac, req)
				if err != nil {
					return nil, err_dt.ErrUpdateFailed
				}
				return res, utils.ErrCodeNil
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				//finish options
				tryBody := tryResBody.(*msg_dt.PlotBatchExecuteResBody)
				if len(tryBody.Adds) > 0 {
					//unlock: plot obtain
					p.DispatchEvent(misc.NodeGame, ac.Tid(), ac.SubjectId(), msg_dt.CdEveUnlockExecute, &msg_dt.EveUnlockExecute{
						Cid:         ac.SubjectId(),
						Opt:         model.UnlockCommonPlotObtain,
						TemplateIds: p.GetModelSliceTemplateIdList(tryBody.Adds),
					})
				}
				if len(tryBody.Updates) > 0 {
					//unlock: plot completed
					completedTemplateIds := make([]int32, 0, 1)
					for _, v := range tryBody.Updates {
						if v.Status == model.PlotStatusLink || v.Status == model.PlotStatusCompleted {
							completedTemplateIds = append(completedTemplateIds, v.TemplateId)
						}
					}
					p.DispatchEvent(misc.NodeGame, ac.Tid(), ac.SubjectId(), msg_dt.CdEveUnlockExecute, &msg_dt.EveUnlockExecute{
						Cid:         ac.SubjectId(),
						Opt:         model.UnlockCommonPlotComplete,
						TemplateIds: completedTemplateIds,
					})
				}
				//change notify
				changes := append(append(tryBody.Adds, tryBody.Updates...), tryBody.Removes...)
				p.ChangeNotify(ac, changes)
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				tryBody := tryResBody.(*msg_dt.PlotBatchExecuteResBody)
				err := p.BatchUpdateTaskRollback(ac, tryBody)
				if err != nil {
					return utils.NewError(err.Error(), nil)
				}
				return nil
			},
		},
	}
}
