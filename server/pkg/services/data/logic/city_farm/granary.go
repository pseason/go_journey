package city_farm

import (
	"95eh.com/eg/utils"
	"bytes"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
)

// GetGranaryByCid 通过玩家id查询粮仓所有物品 query items stacked in the character's granary by cid
func (f *cityFarmService) GetGranaryByCid(cid int64) (res []*model.CityGranary, iErr utils.IError) {
	db := f.Db().Model(&model.CityGranary{})
	err := db.Where("cid = ? ", cid).Find(&res).Error
	if err != nil {
		return nil, utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return
}

// GetGranaryByCidAndOmid 通过物品id查询玩家粮仓对应物品堆叠数量 query the designated items stacked in the character's granary by cid and propId
func (f *cityFarmService) GetGranaryByCidAndOmid(cid int64, propId int32) (res *model.CityGranary, iErr utils.IError) {
	db := f.Db().Model(&model.CityGranary{})
	var t model.CityGranary
	err := db.Where("cid = ? AND prop_id = ? ", cid, propId).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"cid": cid, "propId": propId})
		}
	}
	if t.Id > 0 {
		res = &t
	}
	return
}

// GetPropCountByCid 通过玩家id查询粮仓物品堆叠数量 query the total number of items accumulated in the character’s granary by cid
func (f *cityFarmService) GetPropCountByCid(cid int64) (res int32, iErr utils.IError) {
	db := f.Db().Model(&model.CityGranary{})
	var list []*model.CityGranary
	err := db.Where("cid = ? ", cid).Find(&list).Error
	if err != nil {
		return 0, utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	if len(list) > 0 {
		for _, granary := range list {
			res += granary.Num
		}
	}
	return res, nil
}

// GetPropSumByCid 通过玩家id查询粮仓所有堆叠物品 the total number of items stacked in the character’s granary by cid
func (f *cityFarmService) GetPropSumByCid(cid int64) (res map[int32]int32, iErr utils.IError) {
	db := f.Db().Model(&model.CityGranary{})
	var list []*model.CityGranary
	err := db.Where("cid = ? ", cid).Find(&list).Error
	if err != nil {
		return map[int32]int32{}, utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	if len(list) > 0 {
		res = make(map[int32]int32)
		for _, granary := range list {
			num, ok := res[granary.PropId]
			if ok {
				res[granary.PropId] = num + granary.Num
			} else {
				res[granary.PropId] = granary.Num
			}
		}
	}
	return res, nil
}

// Save 添加粮仓物品 add granary prop
func (f *cityFarmService) Save(granary ...*model.CityGranary) (iErr utils.IError) {
	db := f.Db().Model(&model.CityGranary{})
	err := db.Create(&granary).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"granaryList": granary})
	}
	return
}

// DelAll 通过玩家id删除粮仓所有物品 delete character all stacked items
func (f *cityFarmService) DelAll(cid int64) (iErr utils.IError) {
	db := f.Db().Model(&model.CityGranary{})
	err := db.Unscoped().Where("cid = ? ", cid).Delete(&model.CityGranary{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"cid": cid})
	}
	return nil
}

// UpGranary 修改粮仓数据 update character stacked items number
func (f *cityFarmService) UpGranary(granary *model.CityGranary, id int64) (iErr utils.IError) {
	db := f.Db().Model(&model.CityGranary{})
	err := db.Where("id = ? ", id).Updates(&granary).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"granary": granary})
	}
	return
}

// UpGranaryAll 批量修改粮仓数据 all update cityGranary prop number
func (f *cityFarmService) UpGranaryAll(ac common.IActionCtx, upProp map[int64]int32, cid int64) utils.IError {
	sql := bytes.Buffer{}
	sql.WriteString("UPDATE `city_granary` SET `num` = ( CASE `id` ")
	updateIds := make([]int64, 0)
	for key, value := range upProp {
		sql.WriteString(fmt.Sprintf("WHEN %d THEN num+%d ", key, value))
		updateIds = append(updateIds, key)
	}
	sql.WriteString("END ) ")
	sql.WriteString(" WHERE `cid` = ? AND `id` IN ?")
	rows := f.Db().Model(&model.CityGranary{}).Exec(sql.String(), cid, updateIds).RowsAffected
	if rows <= 0 {
		return utils.NewError(TErrUpdateFailed, utils.M{"upProp": upProp, "cid": cid})
	}
	return nil
}
