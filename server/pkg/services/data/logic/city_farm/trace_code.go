package city_farm

const (
	//批量修改粮仓数据失败
	TErrUpdateFailed = "all update granary data failed"
	//农场已经种植了农作物
	TErrTheFarmHasGrownCrops = "the farm has grown crops"
	//农场农作物未解锁
	TErrFarmCropNotUnlock = "farm crop not unlock"
	//种植的农作物不存在
	TErrPlantedCropsDoNotExist = "planted crops do not exist"
	//农场还未种植农作物
	TErrNotFarmCrop = "farm not has grown crops"
	//灌溉资源已满
	TErrIrrigationResourcesMax = "ErrIrrigationResourcesMax"
)
