package city_farm

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"errors"
	"fmt"
	"github.com/RussellLuo/timingwheel"
	"gorm.io/gorm"
	"hm/pkg/misc/tsv"
	times "hm/pkg/misc/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"math/rand"
	"time"
)

type cityFarmService struct {
	common.IBaseService
	tw             *timingwheel.TimingWheel
	delayIdToTimer map[int64]*timingwheel.Timer
	cropCsv        *tsv.CropTsvManager
	farmland       *tsv.FarmlandTsvManager
}

func (f *cityFarmService) AfterInit() error {
	f.delayIdToTimer = make(map[int64]*timingwheel.Timer)
	f.tw = timingwheel.NewTimingWheel(time.Millisecond, 20)
	f.tw.Start()
	f.cropCsv = tsv.GetTsvManager(tsv.Crop).(*tsv.CropTsvManager)
	f.farmland = tsv.GetTsvManager(tsv.Farmland).(*tsv.FarmlandTsvManager)

	return nil
}

func NewFarmService() *cityFarmService {
	return &cityFarmService{IBaseService: common.NewBaseService(&model.CityFarmland{}, &model.CityGranary{})}
}

// SaveFarm 添加农场 add farm
func (f *cityFarmService) SaveFarm(ac common.IActionCtx, farmland *model.CityFarmland) utils.IError {
	db := f.Db().Model(&model.CityFarmland{})
	err := db.Create(&farmland).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"create farm data": farmland})
	}
	return nil
}

// GetFarmlandByBuildId
//  @Description: 通过建筑id查询农场数据 query farmland by buildId
func (f *cityFarmService) GetFarmlandByBuildId(buildId int64) (res *model.CityFarmland, iErr utils.IError) {
	db := f.Db().Model(&model.CityFarmland{})
	var t model.CityFarmland
	err := db.Where("build_id = ? ", buildId).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"buildingId": buildId})
		}
	}
	if t.Id != 0 {
		res = &t
	}
	return res, nil
}

// UpFarmland 通过农场id修改农场数据 update farmland data by upData and farmlandId
func (f *cityFarmService) UpFarmland(upData map[string]interface{}, farmlandId int64) utils.IError {
	db := f.Db()
	err := db.Model(&model.CityFarmland{}).Where(" id = ?", farmlandId).Updates(upData).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"upData": upData, "farmDataId": farmlandId})
	}

	return nil
}

// GetFarmCropByBuildingId 通过建筑id查询城邦建筑农场 query farmland cropOmId by buildingId
func (f *cityFarmService) GetFarmCropByBuildingId(buildingId int64) (cropId int32, iErr utils.IError) {
	db := f.Db().Model(&model.CityFarmland{})
	var t model.CityFarmland
	err := db.Select("crop_om_id").Where("build_id = ?", buildingId).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return cropId, utils.NewError(err.Error(), utils.M{"buildingId": buildingId})
		}
	}
	if t.CropOmId > 0 {
		return t.CropOmId, nil
	}
	return
}

// GetFarmData 通过玩家id和建筑id查询农场和粮仓数据 query farmland data and cargo data by cid and buildingId
func (f *cityFarmService) GetFarmData(ac common.IActionCtx, cid, buildId int64) (farmData msg_dt.FarmData, cargoDataMap map[int32]int32, iErr utils.IError) {
	//查询指定建造
	farm, iErr := f.GetFarmlandByBuildId(buildId)
	if iErr != nil {
		return
	}
	if farm == nil {
		farmData = msg_dt.FarmData{}
	} else {
		var isRefresh bool
		if times.GetCurrentMillisecond() > farm.CropHp {
			err := f.Db().Model(&model.CityFarmland{}).Where("build_id =?", buildId).Delete(&model.CityFarmland{}).Error
			if err != nil {
				iErr = utils.NewError(err.Error(), utils.M{"delete farm data id": buildId})
				return
			}
			isRefresh = true
		}
		if farm != nil && !isRefresh {
			farmData = msg_dt.FarmData{
				ItemId: farm.CropOmId,
				Life:   int32(farm.CropHp-GetCurMillisecond()) / 1000,
				Forage: farm.CropPabulum,
			}
		}
	}

	omMap, iErr := f.GetPropSumByCid(cid)
	if iErr != nil {
		return
	}
	if len(omMap) > 0 {
		cargoDataMap = make(map[int32]int32)
		cargoDataMap = omMap
	}

	return farmData, cargoDataMap, nil
}

// Planting 通过城邦建筑和种植物品种植农作物 farmland planting by building and propId
func (f *cityFarmService) Planting(ac common.IActionCtx, building model.CityBuilding, propId int32) (farmland *model.CityFarmland, iErr utils.IError) {
	farmland, iErr = f.GetFarmlandByBuildId(building.Id)
	if iErr != nil {
		return
	}
	if farmland != nil {
		iErr = utils.NewError(TErrTheFarmHasGrownCrops, utils.M{"buildingId": building.Id})
		return
	}
	cropCsv, ok := f.cropCsv.TsvMap[propId]
	if !f.checkOmId(int32(building.BuildingType), building.Level, propId) {
		iErr = utils.NewError(TErrTheFarmHasGrownCrops, utils.M{"Planting level not fail": building.Level})
		return
	}
	if !ok {
		iErr = utils.NewError(TErrPlantedCropsDoNotExist, utils.M{"building level": building.Level, "propId": propId})
		return
	}
	farmland = model.GenDefaultFarmland(building.CityId, building.Id)
	endHpTime := time.Now().UnixNano()/1e6 + int64(cropCsv.CropHp)*60*1000
	farmland.CropOmId = propId
	farmland.CropHp = endHpTime

	return farmland, nil
}

// EradicateCrop 通过建筑id查询种植种子 eradicate farmland Crop by buildingId
func (f *cityFarmService) EradicateCrop(ac common.IActionCtx, buildingId int64) utils.IError {
	farmland, iErr := f.GetFarmlandByBuildId(buildingId)
	if iErr != nil {
		return iErr
	}
	if farmland == nil {
		return utils.NewError(TErrNotFarmCrop, utils.M{"buildingId": buildingId})
	}
	iErr = f.DelFarmById(farmland.Id)
	if iErr != nil {
		return iErr
	}
	return nil
}

// Irrigation 灌溉浇水农作物 watering
func (f *cityFarmService) Irrigation(ac common.IActionCtx, buildingId int64, buildingLv int32) (farmId int64, upMap map[string]interface{}, iErr utils.IError) {
	farmland, iErr := f.GetFarmlandByBuildId(buildingId)
	if iErr != nil {
		return
	}
	if farmland == nil {
		iErr = utils.NewError(TErrNotFarmCrop, utils.M{"buildingId": buildingId})
		return
	}
	cropCsv := f.cropCsv.TsvMap[farmland.CropOmId]
	//判断养料是否已满
	farmlandCsv := f.farmland.TsvMap[buildingLv]
	if farmland.CropPabulum == farmlandCsv.PabulumMax {
		iErr = utils.NewError(TErrIrrigationResourcesMax, utils.M{"cropOmId": farmland.CropOmId})
		return
	}
	return farmland.Id, map[string]interface{}{"crop_pabulum": farmland.CropPabulum + cropCsv.NeedResource[2]}, nil
}

// ReceiveFood 领取粮仓堆积物品 The granary is packaged
func (f *cityFarmService) ReceiveFood(ac common.IActionCtx, cid int64) utils.IError {
	iErr := f.DelAll(cid)
	if iErr != nil {
		return iErr
	}
	return nil
}

// GiveOutRewards 发放奖励至粮仓\食物仓,溢出不发邮件,如果下次收益时间已经超过种植物生命截至时间,则开启种植物销毁倒计时
func (f *cityFarmService) GiveOutRewards(ac common.IActionCtx, building model.CityBuilding, cityMembers []model.CityMember) (farm model.CityFarmland, iErr utils.IError) {
	farmland, _ := f.GetFarmlandByBuildId(building.Id)
	if farmland == nil {
		iErr = utils.NewError(TErrNotFarmCrop, utils.M{"buildingId": building.Id})
		return
	}
	farm = *farmland
	app.Log().Info("Granary harvest", utils.M{"send farmland:": farmland.Id})
	farmlandCsv := f.farmland.TsvMap[building.Level]
	cropCsv := f.cropCsv.TsvMap[farmland.CropOmId]
	//判断种植物养料能否满足发放奖励
	if farmland.CropPabulum >= cropCsv.Consume {
		f.UpFarmland(map[string]interface{}{
			"crop_pabulum": farmland.CropPabulum - cropCsv.Consume,
		}, farmland.Id)
		if len(cityMembers) > 0 {
			propMap := make(map[int32]int32)
			//随机种植物有几率产出的物品
			rand.Seed(time.Now().Unix())
			for _, v := range cropCsv.Reward {
				rewardAddition := farmlandCsv.RewardAddition * float32(v[2])
				if v[1] == 100 {
					propMap[v[0]] = v[2] + int32(rewardAddition)
				} else {
					randNum := rand.Intn(100)
					if int32(randNum) <= v[1] {
						propMap[v[0]] = v[2] + int32(rewardAddition)
					}
				}
			}
			//判断发放物品的个数，给所有成员发放奖励
			if len(propMap) > 0 {
				for _, member := range cityMembers {
					//发送奖励至个人粮仓
					fmt.Println(member)
					count, _ := f.GetPropCountByCid(member.Cid)
					if count >= 1000 {
						continue
					}
					addProp := make([]*model.CityGranary, 0)
					upProp := make(map[int64]int32, 0)
					for key, num := range propMap {
						granary, _ := f.GetGranaryByCidAndOmid(member.Cid, key)
						if granary != nil {
							upProp[granary.Id] = num
						} else {
							addProp = append(addProp, &model.CityGranary{
								PropId: key,
								Num:    num,
								CityId: building.CityId,
								Cid:    member.Cid,
							})
						}
					}
					if len(upProp) > 0 {
						f.UpGranaryAll(ac, upProp, member.Cid)
					}
					if len(addProp) > 0 {
						f.Save(addProp...)
					}
				}
			}
		}
	}
	return
}

// checkOmId 校验种植物是否解锁 check Grow plants whether to unlock
func (f *cityFarmService) checkOmId(buildType, lv, propId int32) (b bool) {
	for _, crop := range f.cropCsv.TsvSlice {
		if crop.BuildType == buildType && crop.CropId == propId && lv >= crop.Needlevel {
			return true
		}
	}
	return false
}

// DelFarmById 删除种植数据 delete farm by farmId
func (f *cityFarmService) DelFarmById(farmId int64) utils.IError {
	db := f.Db().Model(&model.CityFarmland{})
	err := db.Where("id = ?", farmId).Delete(&model.CityFarmland{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"farmId": farmId})
	}
	app.Log().Info("关闭粮仓", utils.M{"关闭粮仓编号为:": farmId})
	return nil
}

// GetCurMillisecond 获得毫秒 get milli time
func GetCurMillisecond() int64 {
	return time.Now().UnixNano() / 1e6
}
