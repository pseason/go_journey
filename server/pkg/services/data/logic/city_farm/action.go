package city_farm

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

func (f *cityFarmService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, body interface{}) (resBody interface{}, errCode err_dt.ErrCode){
		// 请求发放奖励
		msg_dt.CdCityFarmGiveOutRewards: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityFarmGiveOutRewards)
			farm, err := f.GiveOutRewards(ac, req.Building, req.CityMemberList)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrCreateFailed
			} else {
				resBody = msg_dt.ResCityFarmGiveOutRewards{Farm: farm}
			}
			return
		},
		// 请求清空农场种植物
		msg_dt.CdCityFarmClear: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityFarmClear)
			err := f.DelFarmById(req.FarmId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrDelFailed
			} else {
				resBody = msg_dt.ResCityFarmClear{}
			}
			return
		},

		// 请求种植物
		msg_dt.CdCityFarmCrop: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityFarmCrop)
			cropId, err := f.GetFarmCropByBuildingId(req.BuildingId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			} else {
				resBody = msg_dt.ResCityFarmCrop{CropId: cropId}
			}
			return
		},
		// 铲除农作物
		msg_dt.CdCityFarmEradicateCrop: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityFarmEradicateCrop)
			err := f.EradicateCrop(ac, req.BuildingId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			} else {
				resBody = msg_dt.ResCityFarmEradicateCrop{}
			}
			return
		},
		// 请求个人仓库
		msg_dt.CdCityFarmGranary: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityFarmGranary)
			resMap, err := f.GetPropSumByCid(req.Cid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			} else {
				resBody = msg_dt.ResCityFarmGranary{GranaryPropMap: resMap}
			}
			return
		},
		// 请求收获粮仓
		msg_dt.CdCityFarmReceiveFood: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityFarmReceiveFood)
			err := f.ReceiveFood(ac, req.Cid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			} else {
				resBody = msg_dt.ResCityFarmReceiveFood{}
			}
			return
		},
		// 请求当前畜牧场和个人粮仓数据
		msg_dt.CdCityFarmGranaryAndFarmInfo: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityFarmGranaryAndFarmInfo)
			farmData, cargoData, err := f.GetFarmData(ac, req.Cid, req.BuildingId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				errCode = err_dt.ErrQueryFailed
			} else {
				resBody = msg_dt.ResCityFarmGranaryAndFarmInfo{
					FarmData:   farmData,
					GranaryMap: cargoData,
				}
			}
			return
		},
	}
}

func (f *cityFarmService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		msg_dt.CdCityFarmPlanting: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				// 请求畜牧场种植
				req := reqBody.(*msg_dt.ReqCityFarmPlanting)
				cityFarm, err := f.Planting(ac, req.Building, req.PropId)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					errCode = err_dt.ErrCreateFailed
				} else {
					resBody = msg_dt.ResCityFarmPlanting{Farm: cityFarm}
				}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := tryResBody.(*msg_dt.ResCityFarmPlanting)
				iError = f.SaveFarm(ac, req.Farm)
				if iError != nil {
					app.Log().TError(ac.Tid(), iError)
					return
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdCityFarmIrrigation: {
			// 请求灌溉
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqCityFarmIrrigation)
				farmId, upMap, err := f.Irrigation(ac, req.BuildingId, req.BuildingLv)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					errCode = err_dt.ErrUpdateFailed
				} else {
					resBody = msg_dt.ResCityFarmIrrigation{FarmId: farmId, UpMap: upMap}
				}
				return
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := tryResBody.(*msg_dt.ResCityFarmIrrigation)
				iError = f.UpFarmland(req.UpMap, req.FarmId)
				if iError != nil {
					app.Log().TError(ac.Tid(), iError)
					return
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
	}
}
