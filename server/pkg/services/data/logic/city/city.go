package city

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/building_e"
	"hm/pkg/services/data/model/enum/city_e"
	"hm/pkg/services/data/msg_dt"
)

type cityService struct {
	common.IBaseService
	cityLock              intfc.IMTxProducer
	cityBuildingTemplates *tsv.CityBuildingTsvManager
	cityInitTemplates     *tsv.CityInitTsvManager
	cityList              []*model.City
	cityMemberCap         map[int64]int32
}

func NewCityService() *cityService {
	return &cityService{IBaseService: common.NewBaseService(&model.City{}, &model.CityMember{})}
}

func (c *cityService) AfterInit() error {
	c.cityBuildingTemplates = tsv.GetTsvManager(tsv.CityBuilding).(*tsv.CityBuildingTsvManager)
	c.cityInitTemplates = tsv.GetTsvManager(tsv.CityInit).(*tsv.CityInitTsvManager)
	err := c.InitCityAndBuilding()
	if err != nil {
		return err
	}
	c.cityList = make([]*model.City, 0)
	c.cityMemberCap = make(map[int64]int32, 0)
	return nil
}

// InitCityAndBuilding 初始化城邦建筑 init city building
func (c *cityService) InitCityAndBuilding() error {
	cityCap, err := c.GetCityCount(0)
	if err != nil {
		return err
	}
	if cityCap == 0 {
		cityConfig := c.Conf().Design.City
		cityIdList, err := c.createCity(cityConfig.InitPlayerCap)
		if err != nil {
			return err
		}
		addBuildings := make([]*model.CityBuilding, 0)
		defaultCityAuth := make([]*model.CityAuth, 0)
		for _, cityId := range cityIdList {
			for _, buildingTsv := range c.cityBuildingTemplates.TsvSlice {
				addBuildings = append(addBuildings, model.GenDefaultBuilding(cityId, building_e.BuildingType(buildingTsv.BuildType), buildingTsv.BuildLv,buildingTsv.Id))
			}

			//todo 等待策划城邦身份权限数据
			//for _,cityIdentity := range c.cityIdentityTemplates.CsvSlice {
			//	authId := util.NumberListConvert(cityIdentity.DefaultAuth...)
			//	defaultCityAuth = append(defaultCityAuth,model.GenDefaultCityAuth(cityId,authId,cityIdentity.Id))
			//}
		}

		if len(addBuildings) > 0 {
			db := c.Db().Model(&model.CityBuilding{})
			err := db.Create(addBuildings).Error
			if err != nil {
				app.Log().TError(0, utils.NewError(err.Error(), nil))
				return err
			}
		}

		if len(defaultCityAuth) > 0 {
			db := c.Db().Model(&model.CityAuth{})
			err := db.Create(defaultCityAuth).Error
			if err != nil {
				app.Log().TError(0, utils.NewError(err.Error(), utils.M{"defaultCityAuth": defaultCityAuth}))
				return err
			}
		}
	}
	return nil
}

// UpDateCity 通过城邦id和修改参数集合修改城邦数据 update city data by cityId and upMap
func (c *cityService) UpDateCity(id int64, upMap map[string]interface{}) utils.IError {
	db := c.Db().Model(&model.City{})
	err := db.Where("id = ?", id).Updates(&upMap).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"cityId:": id, "upMap": upMap})
	}
	return nil
}

//  createCity 创建城邦 create city
func (c *cityService) createCity(initPlayerCap int32) (cityIdList []int64, iErr utils.IError) {
	addCityList := make([]*model.City, 0)
	for _, tsv := range c.cityInitTemplates.TsvSlice {
		addCityList = append(addCityList, model.GenDefaultCity(tsv.CityName, initPlayerCap, tsv.Id, tsv.LandID))
	}
	db := c.Db().Model(&model.City{})
	err := db.Create(addCityList).Error
	if err != nil {
		return cityIdList, utils.NewError(err.Error(), nil)
	}
	for _, city := range addCityList {
		cityIdList = append(cityIdList, city.Id)
	}
	return
}

// GetCityInfoByIds 通过对应参数查询城邦对应数据 query city part data by city_e.CityInfo list
func (c *cityService) GetCityInfoByIds(selInfos []city_e.CityInfo, cityId int64) (cityMap map[int64]*model.City, iErr utils.IError) {
	if len(selInfos) > 0 {
		var sels []string
		for _, info := range selInfos {
			sels = append(sels, info.String())
		}
		var cityList []*model.City
		db := c.Db().Model(&model.City{})
		err := db.Select(sels).Find(&cityList, cityId).Error
		if err != nil {
			return nil, utils.NewError(err.Error(), nil)
		}
		if len(cityList) > 0 {
			cityMap = make(map[int64]*model.City)
			for _, city := range cityList {
				cityMap[city.Id] = city
			}
		}
	}
	return
}

func (c *cityService) GetCityTemId(temId []int64) (res []*model.City, iErr utils.IError) {
	db := c.Db().Model(model.City{})
	err := db.Select("id,city_temp_id").Where("city_temp_id in ? ", temId).Find(&res).Error
	if err != nil {
		return nil, utils.NewError(err.Error(), utils.M{"temId": temId})
	}
	return res, nil
}

// UpdateCityInfoByIds 通过对应参数修改城邦对应数据 update city part data by city_e.CityInfo list
func (c *cityService) UpdateCityInfoByIds(upMap map[city_e.CityInfo]interface{}, cityId int64) (iErr utils.IError) {
	if len(upMap) > 0 {
		upMaps := make(map[string]interface{})
		for info, data := range upMap {
			upMaps[info.String()] = data
		}
		db := c.Db().Model(&model.City{})
		err := db.Where("id = ?", cityId).Updates(upMaps).Error
		if err != nil {
			return utils.NewError(err.Error(), nil)
		}
	}
	return
}

// GetCityById 通过城邦id查询城邦数据 query city data by cityId
func (c *cityService) GetCityById(cityId int64) (city *model.City, iErr utils.IError) {
	db := c.Db().Model(&model.City{})
	var t model.City
	var err error
	if cityId == 50 {
		err = db.Where("city_temp_id = ?", 1).First(&t).Error
	} else {
		err = db.Where("id = ?", cityId).First(&t).Error
	}

	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), nil)
		}
	}
	if t.Id > 0 {
		city = &t
	}
	return
}

// GetCityList 查询所有城邦 query all city
func (c *cityService) GetCityList() (cityList []*model.City, iErr utils.IError) {
	if len(c.cityList) > 0 {
		cityList = c.cityList
	} else {
		db := c.Db().Model(&model.City{})
		err := db.Find(&cityList).Error
		if err != nil {
			return nil, utils.NewError(err.Error(), nil)
		}
		c.cityList = cityList
	}

	return
}

// GetCityCount 查询所有城邦数量 query all city count
func (c *cityService) GetCityCount(int64) (count int64, iErr utils.IError) {
	db := c.Db().Model(&model.City{})
	err := db.Count(&count).Error
	if err != nil {
		return 0, utils.NewError(err.Error(), utils.M{"err": err})
	}
	return
}

// UpCityBanner 通过城邦id修改城邦旗帜 update city banner by cityId
func (c *cityService) UpCityBanner(ac common.IActionCtx, bannerId int32, cityId int64) utils.IError {
	cityData, iErr := c.GetCityById(cityId)
	if iErr != nil {
		return iErr
	}
	if cityData == nil {
		err := errors.New(TErrCityDoesNotExist)
		return utils.NewError(err.Error(), utils.M{"cityId": cityId})
	}
	cityData.BannerProp = bannerId
	iErr = c.UpDateCity(cityId, map[string]interface{}{"banner_prop": bannerId})
	if iErr != nil {
		return iErr
	}
	return nil
}

// PackageCityData 包装城邦数据 package city data
func (c *cityService) PackageCityData(tid int64, cityList []*model.City) (cityDetails []*msg_dt.CityData) {
	cityDetails = make([]*msg_dt.CityData, 0)
	for _, city := range cityList {
		memberCap, err := c.GetCityMemberCap(city.Id)
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), utils.M{"query city current player cap fail , err is ": err}))
		}
		cityDetails = append(cityDetails, &msg_dt.CityData{
			Id:               city.Id,
			CityName:         city.CityNick,
			BannerProp:       city.BannerProp,
			CityType:         1,
			SantoName:        city.CityNick + "大佬",
			CurrentPlayerCap: int32(memberCap),
			MaxPlayerCap:     city.CityPersonMax,
			CityTemplateId:   city.CityTempId,
		})
	}
	return
}

// PackageCityDetails 包装城邦详情数据 package city data
func (c *cityService) PackageCityDetails(tid int64, city *model.City) *msg_dt.CityDetailsData {
	memberCap, err := c.GetCityMemberCap(city.Id)
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), utils.M{"query city current player cap fail , err is ": err}))
	}
	return &msg_dt.CityDetailsData{
		Id:               city.Id,
		Cid:              city.Cid,
		CityName:         city.CityNick,
		BannerProp:       city.BannerProp,
		Lv:               city.Level,
		CityType:         1,
		CurrentPlayerCap: int32(memberCap),
		CityTemplateId:   city.CityTempId,
		ProsperityNum:    city.ProsperityNum,
		OccupationIdList: city.TerritoryId,
		Announcement:     city.Proclamation,
	}
}
