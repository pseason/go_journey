package city

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/model/enum/city_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
)

type ICityService interface {
	common.IBaseService
}

// 消息处理器
func (c *cityService) Actions() map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
	return map[msg_dt.MsgCode]func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode){
		// 请求所有城邦
		msg_dt.CdCityList: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			cityList, err := c.GetCityList()
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				resErrCode = err_dt.ErrQueryFailed
			} else {
				resBody = msg_dt.ResCityList{CityInfo: c.PackageCityData(ac.Tid(), cityList)}
			}
			return
		},
		// 请求城邦数量
		msg_dt.CdCityGetCount: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			cityCount, err := c.GetCityCount(ac.Tid())
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				resErrCode = err_dt.ErrCreateFailed
			} else {
				resBody = msg_dt.ResCityGetCount{
					Count: cityCount,
				}
			}
			return
		},
		// 获得城邦所有成员
		msg_dt.CdCityMemberList: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityMemberList)
			memberList, total, err := c.GetPageMemberListByCityId(req.CityId, req.Page, req.Size)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				resErrCode = err_dt.ErrCreateFailed
			} else {
				resBody = msg_dt.ResCityMemberList{CityMemberList: memberList, Total: int32(total)}
			}
			return
		},
		// 设置城邦成员身份
		msg_dt.CdCitySetCityMemberIdentity: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCitySetCityMemberIdentity)
			err := c.UpdateCityMemberIdentity(req.CityId, req.Oid, req.Identity)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				resErrCode = err_dt.ErrUpdateFailed
			} else {
				resBody = msg_dt.ResCitySetCityMemberIdentity{}
			}
			return
		},
		// 流放城邦成员
		msg_dt.CdCityExileCityMember: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityExileCityMember)
			err := c.ExileCityMemberByCid(req.Cid, req.Oid)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				resErrCode = err_dt.ErrDelFailed
			} else {
				resBody = msg_dt.ResCityExileCityMember{}
			}
			return
		},
		msg_dt.CdCityLoadCity: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityLoadCity)
			city, err := c.GetCityById(req.CityId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				resErrCode = err_dt.ErrQueryFailed
			} else {
				if city == nil {
					resErrCode = err_dt.ErrQueryFailed
					return
				}
				resBody = msg_dt.ResCityLoadCity{City: c.PackageCityDetails(ac.Tid(), city)}
			}
			return
		},
		msg_dt.CdCityIdByTemId: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityIdByTemId)
			cityTemId, err := c.GetCityTemId(req.CityTempIds)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				resErrCode = err_dt.ErrQueryFailed
				return
			}
			return &msg_dt.ResCityIdByTemId{CityList: cityTemId}, 0
		},
		msg_dt.CdCityTempIdByCityId: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
			req := reqBody.(*msg_dt.ReqCityTempIdByCityId)
			cityCloMap, err := c.GetCityInfoByIds([]city_e.CityInfo{city_e.Id,city_e.CityTempId}, req.CityId)
			if err != nil {
				app.Log().TError(ac.Tid(), err)
				resErrCode = err_dt.ErrQueryFailed
				return
			}
			return &msg_dt.ResCityTempIdByCityId{
				CityTempId: int64(cityCloMap[req.CityId].CityTempId),
			},0
		},
	}
}

func (c *cityService) TxActions() map[msg_dt.MsgCode]*common.TxHandler {
	return map[msg_dt.MsgCode]*common.TxHandler{
		//修改城邦数据
		msg_dt.CdCityUpCityData: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				return &msg_dt.ResCityUpCityData{}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				req := reqBody.(*msg_dt.ReqCityUpCityData)
				iError = c.UpdateCityInfoByIds(req.UpMap, req.CityId)
				if iError != nil {
					app.Log().TError(ac.Tid(), iError)
					return
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},

		// 选择加入城邦
		msg_dt.CdCityJoin: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, resErrCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqCityJoin)
				cityTempId, cityName, member, err := c.JoinCity(req.Cid, req.CityId)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return nil, err_dt.ErrCreateFailed
				}
				return &msg_dt.ResCityJoin{AddMember: member, CityName: cityName, CityTempId: cityTempId}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResCityJoin)
				err := c.CreateCityMember(res.AddMember)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					return err
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
		msg_dt.CdCityTryExitCity: {
			Try: func(ac common.IActionCtx, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				req := reqBody.(*msg_dt.ReqCityTryExitCity)
				cityMemberId, err := c.ExitCity(req.Cid)
				if err != nil {
					app.Log().TError(ac.Tid(), err)
					errCode = err_dt.ErrDelFailed
				}
				return msg_dt.ResCityTryExitCity{CityMemberId: cityMemberId}, 0
			},
			Confirm: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				res := tryResBody.(*msg_dt.ResCityTryExitCity)
				iError = c.DeleteCityMember(res.CityMemberId)
				if iError != nil {
					app.Log().TError(ac.Tid(), iError)
					return
				}
				return nil
			},
			Cancel: func(ac common.IActionCtx, reqBody, tryResBody interface{}) (iError utils.IError) {
				return nil
			},
		},
	}
}
