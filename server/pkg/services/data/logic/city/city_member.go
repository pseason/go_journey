package city

import (
	"95eh.com/eg/utils"
	"errors"
	"gorm.io/gorm"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/city_e"
)

// GetCityMemberByCid 通过玩家id查询城邦成员 query city member by cid
func (c *cityService) GetCityMemberByCid(cid int64) (res *model.CityMember, iErr utils.IError) {
	db := c.Db().Model(&model.CityMember{})
	var t model.CityMember
	err := db.Where("cid = ?", cid).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"cid": cid})
		}
	}
	if t.Id > 0 {

		res = &t
	}
	return res, nil
}

// GetCityMemberByCityIdAndCid 通过城邦id和玩家id查询城邦成员 query city member by cid and cityId
func (c *cityService) GetCityMemberByCityIdAndCid(cityId, cid int64) (res *model.CityMember, iErr utils.IError) {
	db := c.Db().Model(&model.CityMember{})
	var t model.CityMember
	err := db.Where("city_id = ? and cid = ?", cityId, cid).First(&t).Error
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, utils.NewError(err.Error(), utils.M{"cityId": cityId, "cid": cid})
		}
	}
	if t.Id > 0 {
		res = &t
	}
	return res, nil
}

// GetCityMemberCap 通过城邦id查询城邦加入人数 query city join people number by cityId
func (c *cityService) GetCityMemberCap(cityId int64) (cityMemberCap int64, iErr utils.IError) {
	num, ok := c.cityMemberCap[cityId]
	if ok {
		cityMemberCap = int64(num)
		return
	}
	db := c.Db().Model(&model.CityMember{})
	err := db.Where("city_id = ? and apply_type = ?", cityId, city_e.Member).Count(&cityMemberCap).Error
	if err != nil {
		iErr = utils.NewError(err.Error(), nil)
		return
	}
	c.cityMemberCap[cityId] = int32(cityMemberCap)
	return
}

// UpDateCityMember 通过修改参数修改城邦数据 update city member by cityMemberId and updateMap data
func (c *cityService) UpDateCityMember(id int64, upMap map[string]interface{}) utils.IError {
	db := c.Db().Model(&model.CityMember{})
	err := db.Where("id = ?", id).Updates(&upMap).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"cityMemberId:": id, "upMap": upMap})
	}
	return nil
}

// DeleteCityMember 通过城邦成员id删除城邦成员 delete city member by cityMemberId
func (c *cityService) DeleteCityMember(id int64) utils.IError {
	db := c.Db().Model(&model.CityMember{})
	err := db.Unscoped().Where("id = ?", id).Delete(&model.CityMember{}).Error
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"deleteCityMemberId": id})
	}
	return nil
}

// JoinCity 通过城邦id加入城邦 join city by cid and cityId
func (c *cityService) JoinCity(cid, cityId int64) (cityTempId int32, cityName string, addMember *model.CityMember, iErr utils.IError) {
	cityMember, err := c.GetCityMemberByCid(cid)
	if err != nil {
		return cityTempId, cityName, addMember, err
	}
	if cityMember != nil && cityMember.ApplyType == city_e.Member {
		iErr = utils.NewError(TErrCharacterNotJoinCity, utils.M{"cityId": cityId, "cid": cid})
		return
	}
	city, err := c.GetCityById(cityId)
	if err != nil {
		return cityTempId, cityName, addMember, err
	}
	if city == nil {
		utils.NewError(TErrCityDoesNotExist, nil)
		return
	}
	cityTempId = city.CityTempId
	memberCap, err := c.GetCityMemberCap(cityId)
	if err != nil {
		return cityTempId, cityName, addMember, err
	}
	if city.CityPersonMax == int32(memberCap) {
		utils.NewError(TErrCityPlayerCapMax, nil)
		return
	}
	addMember = model.GenDefaultCityMember(cid, cityId)
	return cityTempId, city.CityNick, addMember, nil
}

//CreateCityMember 添加城邦成员 add city member by member data
func (c *cityService) CreateCityMember(cityMember *model.CityMember) utils.IError {
	db := c.Db().Model(&model.CityMember{})
	err := db.Create(&cityMember).Error
	if err != nil {
		return utils.NewError("create city member fail", utils.M{"cityMember": *cityMember})
	}
	num, ok := c.cityMemberCap[cityMember.CityId]
	if ok {
		c.cityMemberCap[cityMember.CityId] = num + 1
	}
	return nil
}

// GetMemberListByCityId 通过城邦id查询所有城邦成员 query all cityMember by cityId
func (c *cityService) GetPageMemberListByCityId(cityId int64, page, size int32) (members []model.CityMember, total int64, iErr utils.IError) {
	db := c.Db().Model(&model.CityMember{})
	total, iErr = c.GetCityMemberCap(cityId)
	if iErr != nil {
		return
	}
	err := db.Where("city_id = ?", cityId).Offset(int(page * size)).Limit(int(size)).Find(&members).Error
	if err != nil {
		iErr = utils.NewError(err.Error(), nil)
	}
	return
}

// ExileCityMemberByCid 通过玩家id流放城邦成员 delete cityMember by cid
func (c *cityService) ExileCityMemberByCid(cid, oid int64) utils.IError {
	member, err := c.GetCityMemberByCid(cid)
	if err != nil {
		return err
	}
	if member != nil {
		return utils.NewError(TErrCharacterNotJoinCity, utils.M{"oid": oid})
	}
	oMember, err := c.GetCityMemberByCityIdAndCid(member.CityId, oid)
	if err != nil {
		return err
	}
	if oMember != nil {
		return utils.NewError(TErrCharacterNotJoinCity, utils.M{"oid": oid})
	}
	err = c.DeleteCityMember(oMember.Id)
	if err != nil {
		return err
	}
	return nil
}

// UpdateCityMemberIdentity 通过城邦id、玩家id、身份类型修改城邦身份 update cityMember identity by cityId and cid and cityIdentity
func (c *cityService) UpdateCityMemberIdentity(cityId, oid int64, identity city_e.Identity) utils.IError {
	cityMember, err := c.GetCityMemberByCityIdAndCid(cityId, oid)
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"up character id": oid, "set city identity": identity})
	}
	if cityMember != nil {
		return utils.NewError(TErrCharacterNotJoinCity, utils.M{"up character id": oid, "set city identity": identity})
	}
	cityMember.Identity = identity
	err = c.UpDateCityMember(cityMember.Id, map[string]interface{}{"position": int32(identity)})
	if err != nil {
		return utils.NewError(err.Error(), utils.M{"up character id": oid, "set city identity": identity})
	}
	return nil
}

//ExitCity 退出城邦
func (c *cityService) ExitCity(cid int64) (cityMemberId int64, iErr utils.IError) {
	cityMember, err := c.GetCityMemberByCid(cid)
	if err != nil {
		return cityMemberId, err
	}
	if cityMember == nil {
		return cityMemberId, utils.NewError("character not join city", utils.M{"cid": cid})
	}
	//err = c.DeleteCityMember(cityMember.Id)
	//if err != nil {
	//	return cityMemberId,err
	//}
	return cityMember.Id, nil
}
