package model

import (
	"database/sql/driver"
	jsoniter "github.com/json-iterator/go"
)

// Model common field

type Int32JsonRecords []int32

func (u Int32JsonRecords) Value() (driver.Value, error) {
	body, err := jsoniter.Marshal(u)
	return string(body), err
}

func (u *Int32JsonRecords) Scan(data interface{}) error {
	return jsoniter.Unmarshal(data.([]byte), u)
}
