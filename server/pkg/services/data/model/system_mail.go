package model

import "hm/pkg/misc"

type SystemMail struct {
	misc.BaseModel
	SystemMailTitle                 string `gorm:"type:varchar(20); not null; default:''; comment:系统邮箱标题"`
	SystemMailContent               string `gorm:"type:varchar(200); not null; default:''; comment:邮件内容"`
	SystemMailEnclosures            string `gorm:"type:varchar(200); not null; default:''; comment:附件"`
	EnclosuresOverdueTime           int    `gorm:"type:int; not null; comment:系统邮件保存天数"`
	SystemMailReincarnationDelState int    `gorm:"type:int; not null; comment:转世之后是否删除"`
}

func SystemBuild() (m SystemMail) {
	return SystemMail{
		EnclosuresOverdueTime: 30,
	}
}

//SetMailReincarnationState /** 设置邮件转世后删除
func (m *SystemMail) SetMailReincarnationState() *SystemMail {
	m.SystemMailReincarnationDelState = 1
	return m
}
