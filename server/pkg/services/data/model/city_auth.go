package model

import (
	"fmt"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/city_e"
	"strconv"
	"strings"
)

type CityAuth struct {
	misc.BaseModel
	CityId       int64           `gorm:"type:bigint; not null; comment:城邦id"`
	PositionType city_e.Identity `gorm:"type:int; not null; default:1; comment:身份类型"`
	AuthId       int64           `gorm:"type:bigint; not null; comment:权限id"`
}

func GenDefaultCityAuth(cityId, authId int64, positionType city_e.Identity) (c *CityAuth) {
	return &CityAuth{
		CityId:       cityId,
		PositionType: positionType,
		AuthId:       authId,
	}
}

func GetAuthList(teamMember string) (teamMembers []int32) {
	strList := strings.Split(strings.Trim(strings.Trim(teamMember, "["), "]"), ",")
	for _, v := range strList {
		authId, _ := strconv.ParseInt(v, 10, 64)
		teamMembers = append(teamMembers, int32(authId))
	}
	return
}

func GetAuthString(teamMemberIds ...int32) (teamMembers string) {
	teamMembers = "["
	for i, memberId := range teamMemberIds {
		if i > 0 {
			teamMembers += ","
		}
		teamMembers += fmt.Sprintf("%d", memberId)
	}
	teamMembers += "]"
	return
}
