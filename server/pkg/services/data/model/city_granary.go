package model

import "hm/pkg/misc"

type CityGranary struct {
	misc.BaseModel
	Cid    int64 `gorm:"type:bigint; not null; comment:玩家ID"`
	PropId int32 `gorm:"type:int; not null; comment:物品ID"`
	Num    int32 `gorm:"type:int; not null; default:0; comment:数量"`
	CityId int64 `gorm:"type:bigint; not null; comment:城邦ID"`
}
