package model

import (
	"hm/pkg/misc"
)

/*
@Time   : 2021-11-24 20:33
@Author : wushu
@DESC   : 角色属性
  数据库中只保存无法推算的属性，比如由消耗道具产生的永久属性；
  可推算的是:装备、职位等产生的属性，这些属性在玩家登录时从各模块取对应的信息去生成对应的属性；
*/

type CharacterAttr struct {
	misc.BaseModel
	Cid int64 `gorm:"type:bigint; not null; comment:角色id"`
	// 灵魂系统产生的力敏智，改变时，由灵魂系统通知过来
	SoulStrength int32 `gorm:"type:int; not null; default:0; comment:灵魂力量"`
	SoulAgile    int32 `gorm:"type:int; not null; default:0; comment:灵魂敏捷"`
	SoulWit      int32 `gorm:"type:int; not null; default:0; comment:灵魂智慧"`

	// TODO 暂不提供通过道具永久增加属性的功能
	// 道具产生的永久力敏智
	//PropStrength int32 `gorm:"type:int; not null; default:0; comment:道具-力量"`
	//PropAgile    int32 `gorm:"type:int; not null; default:0; comment:道具-敏捷"`
	//PropWit      int32 `gorm:"type:int; not null; default:0; comment:道具-智慧"`
	//PropHp       int32 `gorm:"type:int; not null; default:0; comment:道具-生命"`
	//PropMp       int32 `gorm:"type:int; not null; default:0; comment:道具-法力"`

	HpSurplus int32 `gorm:"type:int; not null; default:0; comment:剩余生命值"` // 角色下线时保存
	MpSurplus int32 `gorm:"type:int; not null; default:0; comment:剩余法力值"` // 角色下线时保存
}