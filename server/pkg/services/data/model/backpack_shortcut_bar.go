package model

import (
	"gorm.io/gorm"
	"hm/pkg/misc"
)


type BackpackShortcutBar struct {
	misc.BaseModel
	Cid  int64  `gorm:"type:bigint; not null; comment:玩家ID"`
	Data string `gorm:"type:varchar(100); not null; DEFAULT: ''; comment:物品ID,JSON序列化"`

	Ds []int32 `gorm:"-"`
	Cd []int64 `gorm:"-"`
}

func (s *BackpackShortcutBar) BeforeCreate(tx *gorm.DB) (err error) {
	_ = s.BaseModel.BeforeCreate(tx)
	s.Data = "[0,0,0,0]"
	return nil
}