package model

import "hm/pkg/misc"

type Bill struct {
	misc.BaseModel
	Cid             int64  `gorm:"type:bigint; not null; comment:用户ID"`
	PropId          int64  `gorm:"type:bigint; not null; comment:物品ID"`
	Num             int32  `gorm:"type:int; not null; comment:物品数量"`
	CurrencyType    int32  `gorm:"type:int; not null; default:1; COMMENT '交易币种id'"`
	UnitPrice       int64  `gorm:"type:bigint; not null; comment:单价"`
	TotalPrice      int64  `gorm:"type:bigint; not null; comment:商品总价"`
	TransactionType int32  `gorm:"type:int; not null; default:1; comment:交易类型"`
	TransactionDesc string `gorm:"type:varchar(50); not null; default:''; comment:交易详情"`
	SurplusCurrency int32  `gorm:"type:bigint; not null; comment:用户扣除之后剩货币"`
}
