package model

import "hm/pkg/misc"

type MarketStatus int32

const (
	MarketStatus_UP MarketStatus = iota+1
	MarketStatus_DODOWN
	MarketStatus_EXDOWN
	MarketStatus_SELLED
)

type CityMarket struct {
	misc.BaseModel
	Cid        int64  `gorm:"type:bigint; not null; comment:玩家ID"`
	BuildId    int64  `gorm:"type:bigint; not null; comment:建筑ID"`
	PropId     int32  `gorm:"type:int; not null; comment:物品ID"`
	Name       string `gorm:"type:varchar(30); not null; comment:物品名称"`
	Number     int32  `gorm:"type:int; not null; DEFAULT:0; comment:数量"`
	Price      int32  `gorm:"type:int; not null; DEFAULT:0; comment:价格（单位：分）"`
	Day        int32  `gorm:"type:int; not null; comment:上架天数：7/30"`
	MainType   int32  `gorm:"type:int; not null; comment:主类"`
	SubType    int32  `gorm:"type:int; not null; comment:子类"`
	Expiration int64  `gorm:"type:bigint; not null; comment:物品过期下架时间戳"`
	Status     MarketStatus `gorm:"type:tinyint; not null; comment:物品状态 1-上架，2-手动下架，3-过期下架，4-售出"`
	BuyCid	   int64	`gorm:"type:bigint; not null; DEFAULT:0; comment:购买玩家ID"`
}

type MarketToMail struct {
	Id int64
	Cid int64
	PropId int32
	Num int32
	Name string
	Price int32
}

type MarketRecord struct {
	Id int64
	PropId int32
	Num int32
	Name string
	Expiration int64
	Price int32
}

type ResPageList struct {
	Page int64
	TotalPage,Rows int64
	List []*CityMarket
}

//上架需要传的参数,便于批量上架
type MarketPutParams struct {
	PropId int32
	Num    int32
	Day	   int32
	Price  int32
}