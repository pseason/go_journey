package model

import (
	"hm/pkg/misc"
)

import (
	"hm/pkg/services/data/model/enum/city_e"
)

type City struct {
	misc.BaseModel
	Cid                  int64           `gorm:"type:bigint; not null; comment:拥有者ID"`
	Level                int32           `gorm:"type:int; not null; default:1; comment:城市等级"`
	CityNick             string          `gorm:"type:varchar(50); not null; default:''; comment:城市名称"`
	Proclamation         string          `gorm:"type:varchar(500); not null; default:''; comment:公告内容"`
	HeirsCid             int64           `gorm:"type:bigint; not null; default:0; comment:继承者ID"`
	BannerProp           int32           `gorm:"type:int; not null; default:0; comment:旗帜模板id"`
	ImpeachVotes         int32           `gorm:"type:int; not null; default:0; comment:弹劾票数"`
	ExileNum             int32           `gorm:"type:int; not null; default:10; comment:每日首领流放个数"`
	DemiseNum            int32           `gorm:"type:int; not null; default:1; comment:每周禅让次数"`
	CityPersonMax        int32           `gorm:"type:int; not null; default:150; comment: 城市人数上限"`
	ProsperityNum        int32           `gorm:"type:int; not null; default:0; comment:繁荣度"`
	VoteState            city_e.VoteType `gorm:"type:int; not null; default:0; comment:投票状态"`
	VoteEndTime          int64           `gorm:"type:bigint; not null; default:0; comment:投票结束时间"`
	ExtendsElectionCount int32           `gorm:"type:int; not null; default:0; comment:继承人选举届数"`
	LeaderElectionCount  int32           `gorm:"type:int; not null; default:0; comment:首领选举届数"`
	CityTempId           int32           `gorm:"type:int; not null; default:0; comment:城邦编号"`
	TerritoryId          int32           `gorm:"type:int; not null; default:0; comment:领地id"`
}

func GenDefaultCity(cityNick string, maxPersonNum, cityTempId, territoryId int32) (c *City) {
	return &City{
		CityNick:      cityNick,
		CityPersonMax: maxPersonNum,
		Level:         1,
		CityTempId:    cityTempId,
		TerritoryId:   territoryId,
	}
}

func (c *City) Get(info city_e.CityInfo) (res interface{}) {
	switch info {
	case city_e.Id:
		res = c.Id
	case city_e.Created:
		res = c.Created
	case city_e.Level:
		res = c.Level
	case city_e.CityNick:
		res = c.CityNick
	case city_e.Proclamation:
		res = c.Proclamation
	case city_e.HeirsCid:
		res = c.HeirsCid
	case city_e.BannerProp:
		res = c.BannerProp
	case city_e.ImpeachVotes:
		res = c.ImpeachVotes
	case city_e.ExileNum:
		res = c.ExileNum
	case city_e.DemiseNum:
		res = c.DemiseNum
	case city_e.CityPersonMax:
		res = c.CityPersonMax
	case city_e.ProsperityNum:
		res = c.ProsperityNum
	case city_e.VoteState:
		res = c.VoteState
	case city_e.VoteEndTime:
		res = c.VoteEndTime
	case city_e.ExtendsElectionCount:
		res = c.ExtendsElectionCount
	case city_e.LeaderElectionCount:
		res = c.LeaderElectionCount
	}
	return res
}
