package model

import (
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/city_e"
)

type CityMember struct {
	misc.BaseModel
	Cid                    int64            `gorm:"type:bigint; not null; comment:拥有者ID"`
	CityId                 int64            `gorm:"type:bigint; not null; comment:城市ID"`
	Identity               city_e.Identity  `gorm:"type:int; not null; default:0; comment:身份类型"`
	WeekContributionNum    int32            `gorm:"type:int; not null; default:0; comment:本周贡献值"`
	HistoryContributionNum int32            `gorm:"type:int; not null; default:0; comment:历史贡献值"`
	ApplyType              city_e.ApplyType `gorm:"type:int; not null; default:0; comment:申请类型"`
	GetVote                int32            `gorm:"type:int; not null; default:0; comment:获得票数"`
	Surplus                int32            `gorm:"type:int; not null; default:0; comment:剩余票数"`
	Impeachment            bool             `gorm:"type:tinyint; not null; default:0; comment:是否参与弹劾"`
	VoteList               string           `gorm:"type:varchar(500); not null; default:''; comment:投票id"`
	SignInTime             int64            `gorm:"type:bigint; not null; default:0; comment:签到时间"`
	LegionId               int64            `gorm:"type:bigint; not null; comment:军团ID"`
	PutEnergyTime          int64            `gorm:"type:bigint; not null; default:0; comment:投入精力时间"`
	CityRecallTime         int64            `gorm:"type:bigint; not null; default:0; comment:城邦罢免时间"`
}

func GenDefaultCityMember(cid, cityId int64) (c *CityMember) {
	return &CityMember{
		Cid:       cid,
		CityId:    cityId,
		ApplyType: city_e.Member,
		Identity:  10,
		VoteList:  "[]",
	}
}

func (c *CityMember) SetApplyType(applyType city_e.ApplyType) *CityMember {
	c.ApplyType = applyType
	return c
}
