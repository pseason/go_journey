package model

import (
	"hm/pkg/game/msg_gm"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/character_info_e"
	"strconv"
)

type EnumBaseType interface {
	ToBaseType() interface{} // 转为基本类型，但是用interface接收
}

type FigureType = uint8
type OnlineStateEnum = uint8 // 在线状态
const (
	On_Initializing OnlineStateEnum = iota + 1 // 初始化中(上线)
	On_Switching                               // 切换场景中
	On_Playing                                 // 正常游玩中
)

// @Model(WithRedis)
type CharacterInfo struct {
	misc.BaseModel                        // @Fields(Codes=1001,1002,1003,1004)
	Uid                        int64      `gorm:"type:bigint; not null; unique; comment:玩家账号ID"` // @Field(Code=1006;Unmodifiable)
	Nickname                   string     `gorm:"type:varchar(10); not null; default:''; comment:昵称"` // @Field(Code=1007)
	Gender                     uint8      `gorm:"type:tinyint; not null; default:0; comment:性别 0女 1男"` // @Field(Code=1008)
	Figure                     FigureType `gorm:"type:tinyint; not null; default:0; comment:形象id"` // @Field(Code=1009)
	Birthday                   int32      `gorm:"type:int; not null; default:0; comment:氏族系统-生辰-秒级时间戳"` // @Field(Code=1010)
	Introduction               string     `gorm:"type:varchar(10); not null; default:''; comment:氏族系统-个人简介"` // @Field(Code=1011)
	ClanId                     int64      `gorm:"type:bigint; not null; default:0; comment:氏族id"` // @Field(Code=1012)
	CityId                     int64      `gorm:"type:bigint; not null; default:0; comment:城邦id"` // @Field(Code=1013)
	KingdomId                  int64      `gorm:"type:bigint; not null; default:0; comment:王国id"` // @Field(Code=1014)
	ClanName                   string     `gorm:"type:varchar(10); not null; default:''; comment:氏族名"` // @Field(Code=1015)
	CityName                   string     `gorm:"type:varchar(10); not null; default:''; comment:城邦名"` // @Field(Code=1016)
	KingdomName                string     `gorm:"type:varchar(10); not null; default:''; comment:王国名"` // @Field(Code=1017)
	ClanIdentity               int32      `gorm:"type:int; not null; default:0; comment:氏族身份id"` // @Field(Code=1018)
	CityPosition               int32      `gorm:"type:int; not null; default:0; comment:城邦官职id"` // @Field(Code=1019)
	KingdomPosition            int32      `gorm:"type:int; not null; default:0; comment:王国爵位id"` // @Field(Code=1020)
	CharacterSpaceId           int64      `gorm:"type:bigint; not null; default:0; comment:角色所在空间id"` // @Field(Code=1021)
	TeamId                     int64      `gorm:"type:bigint; not null; default:0; comment:队伍id"` // @Field(Code=1022)
	NewbieFinishTime           int64      `gorm:"type:bigint; not null; default:0; comment:新手结束时间-秒级时间戳"` // @Field(Code=1023)
	AgeAutoRefreshStartTime    int64      `gorm:"type:bigint; not null; default:0; comment:年龄自动刷新计时时间-秒级时间戳"` // @Field(Code=1024)
	OfflineTime                int64      `gorm:"type:bigint; not null; default:0; comment:离线时间-秒级时间戳"` // @Field(Code=1025)
	EnergyAutoRefreshStartTime int64      `gorm:"type:bigint; not null; default:0; comment:精力自动刷新计时时间-秒级时间戳"` // @Field(Code=1026)
	MonthCardExpiresAt         int64      `gorm:"type:bigint; not null; default:0; comment:月卡到期时间-秒级时间戳"` // @Field(Code=1027)
	BestMonthCardExpiresAt     int64      `gorm:"type:bigint; not null; default:0; comment:至尊月卡到期时间-秒级时间戳"` // @Field(Code=1028)
	// 数值类
	Gold                   int32 `gorm:"type:int; not null; default:0; comment:金币"`    // @Field(Code=2001)
	UnrealCoin             int32 `gorm:"type:int; not null; default:0; comment:虚幻币"`   // @Field(Code=2002)
	ClanContributionVal    int32 `gorm:"type:int; not null; default:0; comment:氏族贡献值"` // @Field(Code=2003)
	CityContributionVal    int32 `gorm:"type:int; not null; default:0; comment:城邦贡献值"` // @Field(Code=2004)
	KingdomContributionVal int32 `gorm:"type:int; not null; default:0; comment:王国贡献值"` // @Field(Code=2005)
	Energy                 int32 `gorm:"type:int; not null; default:0; comment:精力"`    // @Field(Code=2006)
	Age                    int32 `gorm:"type:int; not null; default:0; comment:年龄"`    // @Field(Code=2007)
	LifeTime               int32 `gorm:"type:int; not null; default:0; comment:寿命"`    // @Field(Code=2008)
	Merit                  int32 `gorm:"type:int; not null; default:0; comment:功德"`    // @Field(Code=2009)
	Prestige               int32 `gorm:"type:int; not null; default:0; comment:威望"`    // @Field(Code=2010)
	FairyRank              int32 `gorm:"type:int; not null; default:0; comment:仙阶"`    // @Field(Code=2011)
	MagicRank              int32 `gorm:"type:int; not null; default:0; comment:魔阶"`    // @Field(Code=2012)
	BeInfatuatedVal        int32 `gorm:"type:int; not null; default:0; comment:入魔值"`   // @Field(Code=2013)
	BuildLv                int32 `gorm:"type:int; not null; default:0; comment:建造等级"`  // @Field(Code=2014)
	CookLv                 int32 `gorm:"type:int; not null; default:0; comment:烹饪等级"`  // @Field(Code=2015)
	CraftsLv               int32 `gorm:"type:int; not null; default:0; comment:工匠等级"`  // @Field(Code=2016)
	PharmacyLv             int32 `gorm:"type:int; not null; default:0; comment:炼药等级"`  // @Field(Code=2017)
	WineLv                 int32 `gorm:"type:int; not null; default:0; comment:酿酒等级"`  // @Field(Code=2018)
	LifeSkillPoints        int32 `gorm:"type:int; not null; default:0; comment:生活技能点"` // @Field(Code=2019)
	SoulEnergy             int32 `gorm:"type:int; not null; default:0; comment:灵魂能量"`  // @Field(Code=2020)
	BattlePower            int32 `gorm:"type:int; not null; default:0; comment:战力"`    // @Field(Code=2021)
	WareHouseLv            int32 `gorm:"type:int; not null; default:0; comment:仓库等级"`  // @Field(Code=2022)
	// 仅存在于缓存
	//IsOnline bool
}

func (ci *CharacterInfo) MarshalPb() (info *msg_gm.ResCharacterInfo) {
	info = &msg_gm.ResCharacterInfo{}
	if ci.Id == 0 {
		info.LongInfo = map[int32]int64{
			int32(character_info_e.Id): ci.Id,
		}
		return
	}

	info.LongInfo = map[int32]int64{
		int32(character_info_e.Id):                         ci.Id,
		int32(character_info_e.Created):                    ci.Created,
		int32(character_info_e.Uid):                        ci.Uid,
		int32(character_info_e.ClanId):                     ci.ClanId,
		int32(character_info_e.CityId):                     ci.CityId,
		int32(character_info_e.KingdomId):                  ci.KingdomId,
		int32(character_info_e.CharacterSpaceId):           ci.CharacterSpaceId,
		int32(character_info_e.TeamId):                     ci.TeamId,
		int32(character_info_e.NewbieFinishTime):           ci.NewbieFinishTime,
		int32(character_info_e.AgeAutoRefreshStartTime):    ci.AgeAutoRefreshStartTime,
		int32(character_info_e.OfflineTime):                ci.OfflineTime,
		int32(character_info_e.EnergyAutoRefreshStartTime): ci.EnergyAutoRefreshStartTime,
		int32(character_info_e.MonthCardExpiresAt):         ci.MonthCardExpiresAt,
		int32(character_info_e.BestMonthCardExpiresAt):     ci.BestMonthCardExpiresAt,
	}

	info.IntInfo = map[int32]int32{
		int32(character_info_e.Gender):                 int32(ci.Gender),
		int32(character_info_e.Figure):                 int32(ci.Figure),
		int32(character_info_e.Birthday):               ci.Birthday,
		int32(character_info_e.ClanIdentity):           ci.ClanIdentity,
		int32(character_info_e.CityPosition):           ci.CityPosition,
		int32(character_info_e.KingdomPosition):        ci.KingdomPosition,
		int32(character_info_e.Gold):                   ci.Gold,
		int32(character_info_e.UnrealCoin):             ci.UnrealCoin,
		int32(character_info_e.ClanContributionVal):    ci.ClanContributionVal,
		int32(character_info_e.CityContributionVal):    ci.CityContributionVal,
		int32(character_info_e.KingdomContributionVal): ci.KingdomContributionVal,
		int32(character_info_e.Energy):                 ci.Energy,
		int32(character_info_e.Age):                    ci.Age,
		int32(character_info_e.LifeTime):               ci.LifeTime,
		int32(character_info_e.Merit):                  ci.Merit,
		int32(character_info_e.Prestige):               ci.Prestige,
		int32(character_info_e.FairyRank):              ci.FairyRank,
		int32(character_info_e.MagicRank):              ci.MagicRank,
		int32(character_info_e.BeInfatuatedVal):        ci.BeInfatuatedVal,
		int32(character_info_e.BuildLv):                ci.BuildLv,
		int32(character_info_e.CookLv):                 ci.CookLv,
		int32(character_info_e.CraftsLv):               ci.CraftsLv,
		int32(character_info_e.PharmacyLv):             ci.PharmacyLv,
		int32(character_info_e.WineLv):                 ci.WineLv,
		int32(character_info_e.LifeSkillPoints):        ci.LifeSkillPoints,
		int32(character_info_e.SoulEnergy):             ci.SoulEnergy,
		int32(character_info_e.BattlePower):            ci.BattlePower,
		int32(character_info_e.WareHouseLv):            ci.WareHouseLv,
	}

	info.StrInfo = map[int32]string{
		int32(character_info_e.Nickname):     ci.Nickname,
		int32(character_info_e.Introduction): ci.Introduction,
		int32(character_info_e.ClanName):     ci.ClanName,
		int32(character_info_e.CityName):     ci.CityName,
		int32(character_info_e.KingdomName):  ci.KingdomName,
	}
	return
}

func (ci *CharacterInfo) GetByEnum(info int32) (res interface{}) {
	switch info {
	case character_info_e.Id:
		res = ci.Id
	case character_info_e.Created:
		res = ci.Created
	case character_info_e.Updated:
		res = ci.Updated
	case character_info_e.Deleted:
		res = ci.Deleted
	case character_info_e.Uid:
		res = ci.Uid
	case character_info_e.Nickname:
		res = ci.Nickname
	case character_info_e.Gender:
		res = ci.Gender
	case character_info_e.Figure:
		res = ci.Figure
	case character_info_e.Birthday:
		res = ci.Birthday
	case character_info_e.Introduction:
		res = ci.Introduction
	case character_info_e.ClanId:
		res = ci.ClanId
	case character_info_e.CityId:
		res = ci.CityId
	case character_info_e.KingdomId:
		res = ci.KingdomId
	case character_info_e.ClanName:
		res = ci.ClanName
	case character_info_e.CityName:
		res = ci.CityName
	case character_info_e.KingdomName:
		res = ci.KingdomName
	case character_info_e.ClanIdentity:
		res = ci.ClanIdentity
	case character_info_e.CityPosition:
		res = ci.CityPosition
	case character_info_e.KingdomPosition:
		res = ci.KingdomPosition
	case character_info_e.CharacterSpaceId:
		res = ci.CharacterSpaceId
	case character_info_e.TeamId:
		res = ci.TeamId
	case character_info_e.NewbieFinishTime:
		res = ci.NewbieFinishTime
	case character_info_e.AgeAutoRefreshStartTime:
		res = ci.AgeAutoRefreshStartTime
	case character_info_e.OfflineTime:
		res = ci.OfflineTime
	case character_info_e.EnergyAutoRefreshStartTime:
		res = ci.EnergyAutoRefreshStartTime
	case character_info_e.MonthCardExpiresAt:
		res = ci.MonthCardExpiresAt
	case character_info_e.BestMonthCardExpiresAt:
		res = ci.BestMonthCardExpiresAt
	case character_info_e.Gold:
		res = ci.Gold
	case character_info_e.UnrealCoin:
		res = ci.UnrealCoin
	case character_info_e.ClanContributionVal:
		res = ci.ClanContributionVal
	case character_info_e.CityContributionVal:
		res = ci.CityContributionVal
	case character_info_e.KingdomContributionVal:
		res = ci.KingdomContributionVal
	case character_info_e.Energy:
		res = ci.Energy
	case character_info_e.Age:
		res = ci.Age
	case character_info_e.LifeTime:
		res = ci.LifeTime
	case character_info_e.Merit:
		res = ci.Merit
	case character_info_e.Prestige:
		res = ci.Prestige
	case character_info_e.FairyRank:
		res = ci.FairyRank
	case character_info_e.MagicRank:
		res = ci.MagicRank
	case character_info_e.BeInfatuatedVal:
		res = ci.BeInfatuatedVal
	case character_info_e.BuildLv:
		res = ci.BuildLv
	case character_info_e.CookLv:
		res = ci.CookLv
	case character_info_e.CraftsLv:
		res = ci.CraftsLv
	case character_info_e.PharmacyLv:
		res = ci.PharmacyLv
	case character_info_e.WineLv:
		res = ci.WineLv
	case character_info_e.LifeSkillPoints:
		res = ci.LifeSkillPoints
	case character_info_e.SoulEnergy:
		res = ci.SoulEnergy
	case character_info_e.BattlePower:
		res = ci.BattlePower
	case character_info_e.WareHouseLv:
		res = ci.WareHouseLv
	}
	return res
}

// 转为map (用于存到redis) (key:字段的蛇形名)
func (ci *CharacterInfo) ToRedisMap() (data map[string]interface{}) {
	data = make(map[string]interface{})
	for field, snakeName := range character_info_e.AllField() {
		data[snakeName] = ci.GetByEnum(field)
	}
	// todo 暂忽略deleted (deleted字段类型为gorm内置的结构体，不便处理。并且该字段目前没有需要主动查改的需求)
	delete(data, character_info_e.CodeToFieldName(character_info_e.Deleted))
	return
	//return map[string]interface{}{
	//	character_info_e.CodeToFieldName(character_info_e.Id):                         ci.Id,
	//	character_info_e.CodeToFieldName(character_info_e.Created):                    ci.Created,
	//	character_info_e.CodeToFieldName(character_info_e.Uid):                        ci.Uid,
	//	character_info_e.CodeToFieldName(character_info_e.Nickname):                   ci.Nickname,
	//	character_info_e.CodeToFieldName(character_info_e.Gender):                     ci.Gender,
	//	character_info_e.CodeToFieldName(character_info_e.Figure):                     ci.Figure,
	//	character_info_e.CodeToFieldName(character_info_e.CityId):                     ci.CityId,
	//	character_info_e.CodeToFieldName(character_info_e.KingdomId):                  ci.KingdomId,
	//	character_info_e.CodeToFieldName(character_info_e.CityName):                   ci.CityName,
	//	character_info_e.CodeToFieldName(character_info_e.KingdomName):                ci.KingdomName,
	//	character_info_e.CodeToFieldName(character_info_e.CityPosition):               ci.CityPosition,
	//	character_info_e.CodeToFieldName(character_info_e.KingdomPosition):            ci.KingdomPosition,
	//	character_info_e.CodeToFieldName(character_info_e.CharacterSpaceId):           ci.CharacterSpaceId,
	//	character_info_e.CodeToFieldName(character_info_e.TeamId):                     ci.TeamId,
	//	character_info_e.CodeToFieldName(character_info_e.ClanId):                     ci.ClanId,
	//	character_info_e.CodeToFieldName(character_info_e.NewbieFinishTime):           ci.NewbieFinishTime,
	//	character_info_e.CodeToFieldName(character_info_e.AgeAutoRefreshStartTime):    ci.AgeAutoRefreshStartTime,
	//	character_info_e.CodeToFieldName(character_info_e.OfflineTime):                ci.OfflineTime,
	//	character_info_e.CodeToFieldName(character_info_e.EnergyAutoRefreshStartTime): ci.EnergyAutoRefreshStartTime,
	//	character_info_e.CodeToFieldName(character_info_e.MonthCardExpiresAt):         ci.MonthCardExpiresAt,
	//	character_info_e.CodeToFieldName(character_info_e.BestMonthCardExpiresAt):     ci.BestMonthCardExpiresAt,
	//	character_info_e.CodeToFieldName(character_info_e.Gold):                       ci.Gold,
	//	character_info_e.CodeToFieldName(character_info_e.UnrealCoin):                 ci.UnrealCoin,
	//	character_info_e.CodeToFieldName(character_info_e.CityContributionVal):        ci.CityContributionVal,
	//	character_info_e.CodeToFieldName(character_info_e.KingdomContributionVal):     ci.KingdomContributionVal,
	//	character_info_e.CodeToFieldName(character_info_e.Energy):                     ci.Energy,
	//	character_info_e.CodeToFieldName(character_info_e.Age):                        ci.Age,
	//	character_info_e.CodeToFieldName(character_info_e.LifeTime):                   ci.LifeTime,
	//	character_info_e.CodeToFieldName(character_info_e.Merit):                      ci.Merit,
	//	character_info_e.CodeToFieldName(character_info_e.Prestige):                   ci.Prestige,
	//	character_info_e.CodeToFieldName(character_info_e.FairyRank):                  ci.FairyRank,
	//	character_info_e.CodeToFieldName(character_info_e.MagicRank):                  ci.MagicRank,
	//	character_info_e.CodeToFieldName(character_info_e.BeInfatuatedVal):            ci.BeInfatuatedVal,
	//	character_info_e.CodeToFieldName(character_info_e.BuildLv):                    ci.BuildLv,
	//	character_info_e.CodeToFieldName(character_info_e.CookLv):                     ci.CookLv,
	//	character_info_e.CodeToFieldName(character_info_e.CraftsLv):                   ci.CraftsLv,
	//	character_info_e.CodeToFieldName(character_info_e.PharmacyLv):                 ci.PharmacyLv,
	//	character_info_e.CodeToFieldName(character_info_e.WineLv):                     ci.WineLv,
	//	character_info_e.CodeToFieldName(character_info_e.LifeSkillPoints):            ci.LifeSkillPoints,
	//	character_info_e.CodeToFieldName(character_info_e.SoulEnergy):                 ci.SoulEnergy,
	//	character_info_e.CodeToFieldName(character_info_e.BattlePower):                ci.BattlePower,
	//	character_info_e.CodeToFieldName(character_info_e.WareHouseLv):                ci.WareHouseLv,
	//}
}

// map数据转到结构体 (用于把redis取出的map转为结构体) (key:字段的蛇形名)
func (ci *CharacterInfo) FetchRedisMapData(info map[string]string) {
	for k, v := range info {
		switch character_info_e.FieldNameToCode(k) {
		case character_info_e.Id:
			ci.Id, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.Created:
			ci.Created, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.Updated:
			ci.Updated, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.Deleted:
		// todo 暂不处理deleted (deleted字段类型为gorm内置的结构体，不便处理。并且该字段目前没有需要主动查改的需求)
		case character_info_e.Uid:
			ci.Uid, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.Nickname:
			ci.Nickname = v
		case character_info_e.Gender:
			gender, _ := strconv.ParseInt(v, 10, 32)
			ci.Gender = uint8(gender)
		case character_info_e.Figure:
			figure, _ := strconv.ParseInt(v, 10, 32)
			ci.Figure = FigureType(figure)
		case character_info_e.Birthday:
			birthday, _ := strconv.ParseInt(v, 10, 32)
			ci.Birthday = int32(birthday)
		case character_info_e.Introduction:
			ci.Introduction = v
		case character_info_e.ClanId:
			ci.ClanId, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.CityId:
			ci.CityId, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.KingdomId:
			ci.KingdomId, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.ClanName:
			ci.ClanName = v
		case character_info_e.CityName:
			ci.CityName = v
		case character_info_e.KingdomName:
			ci.KingdomName = v
		case character_info_e.ClanIdentity:
			clanIdentity, _ := strconv.ParseInt(v, 10, 32)
			ci.ClanIdentity = int32(clanIdentity)
		case character_info_e.CityPosition:
			cityPosition, _ := strconv.ParseInt(v, 10, 32)
			ci.CityPosition = int32(cityPosition)
		case character_info_e.KingdomPosition:
			kingdomPosition, _ := strconv.ParseInt(v, 10, 32)
			ci.KingdomPosition = int32(kingdomPosition)
		case character_info_e.CharacterSpaceId:
			ci.CharacterSpaceId, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.TeamId:
			ci.TeamId, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.NewbieFinishTime:
			ci.NewbieFinishTime, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.AgeAutoRefreshStartTime:
			ci.AgeAutoRefreshStartTime, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.OfflineTime:
			ci.OfflineTime, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.EnergyAutoRefreshStartTime:
			ci.EnergyAutoRefreshStartTime, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.MonthCardExpiresAt:
			ci.MonthCardExpiresAt, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.BestMonthCardExpiresAt:
			ci.BestMonthCardExpiresAt, _ = strconv.ParseInt(v, 10, 64)
		case character_info_e.Gold:
			gold, _ := strconv.ParseInt(v, 10, 32)
			ci.Gold = int32(gold)
		case character_info_e.UnrealCoin:
			unrealCoin, _ := strconv.ParseInt(v, 10, 32)
			ci.UnrealCoin = int32(unrealCoin)
		case character_info_e.ClanContributionVal:
			clanContributionVal, _ := strconv.ParseInt(v, 10, 32)
			ci.ClanContributionVal = int32(clanContributionVal)
		case character_info_e.CityContributionVal:
			cityContributionVal, _ := strconv.ParseInt(v, 10, 32)
			ci.CityContributionVal = int32(cityContributionVal)
		case character_info_e.KingdomContributionVal:
			kingdomContributionVal, _ := strconv.ParseInt(v, 10, 32)
			ci.KingdomContributionVal = int32(kingdomContributionVal)
		case character_info_e.Energy:
			energy, _ := strconv.ParseInt(v, 10, 32)
			ci.Energy = int32(energy)
		case character_info_e.Age:
			age, _ := strconv.ParseInt(v, 10, 32)
			ci.Age = int32(age)
		case character_info_e.LifeTime:
			lifeTime, _ := strconv.ParseInt(v, 10, 32)
			ci.LifeTime = int32(lifeTime)
		case character_info_e.Merit:
			merit, _ := strconv.ParseInt(v, 10, 32)
			ci.Merit = int32(merit)
		case character_info_e.Prestige:
			prestige, _ := strconv.ParseInt(v, 10, 32)
			ci.Prestige = int32(prestige)
		case character_info_e.FairyRank:
			fairyRank, _ := strconv.ParseInt(v, 10, 32)
			ci.FairyRank = int32(fairyRank)
		case character_info_e.MagicRank:
			magicRank, _ := strconv.ParseInt(v, 10, 32)
			ci.MagicRank = int32(magicRank)
		case character_info_e.BeInfatuatedVal:
			beInfatuatedVal, _ := strconv.ParseInt(v, 10, 32)
			ci.BeInfatuatedVal = int32(beInfatuatedVal)
		case character_info_e.BuildLv:
			buildLv, _ := strconv.ParseInt(v, 10, 32)
			ci.BuildLv = int32(buildLv)
		case character_info_e.CookLv:
			cookLv, _ := strconv.ParseInt(v, 10, 32)
			ci.CookLv = int32(cookLv)
		case character_info_e.CraftsLv:
			craftsLv, _ := strconv.ParseInt(v, 10, 32)
			ci.CraftsLv = int32(craftsLv)
		case character_info_e.PharmacyLv:
			pharmacyLv, _ := strconv.ParseInt(v, 10, 32)
			ci.PharmacyLv = int32(pharmacyLv)
		case character_info_e.WineLv:
			wineLv, _ := strconv.ParseInt(v, 10, 32)
			ci.WineLv = int32(wineLv)
		case character_info_e.LifeSkillPoints:
			lifeSkillPoints, _ := strconv.ParseInt(v, 10, 32)
			ci.LifeSkillPoints = int32(lifeSkillPoints)
		case character_info_e.SoulEnergy:
			soulEnergy, _ := strconv.ParseInt(v, 10, 32)
			ci.SoulEnergy = int32(soulEnergy)
		case character_info_e.BattlePower:
			battlePower, _ := strconv.ParseInt(v, 10, 32)
			ci.BattlePower = int32(battlePower)
		case character_info_e.WareHouseLv:
			wareHouseLv, _ := strconv.ParseInt(v, 10, 32)
			ci.WareHouseLv = int32(wareHouseLv)
		}
	}
}
