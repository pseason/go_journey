package model

import "hm/pkg/misc"

type CityMakeWine struct {
	misc.BaseModel
	Cid     int64 `gorm:"type:bigint; not null; comment:玩家ID"`
	ForgeId int32 `gorm:"type:int; not null; comment:合成ID"`
	BuildId int64 `gorm:"type:bigint; not null; comment:建筑ID"`
	EndTime int64 `gorm:"type:bigint; not null; comment:结束时间"`
	PropId  int32 `gorm:"type:int; not null; comment:合成物品ID"`
	Num     int32 `gorm:"type:int; not null; comment:数量"`
	Slot    int32 `gorm:"type:int; not null; comment:槽位"`
}
