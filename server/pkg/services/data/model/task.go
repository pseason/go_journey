package model

import (
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/misc/utils"
	"hm/pkg/services/data/model/enum/task_e"
)

type TaskRule int32

const (
	TaskRuleAnd  TaskRule = iota + 1 //所有都必须达标
	TaskRuleOr                       //其中一个达标即可
	TaskRuleRand                     //随意，只看所有的总数
)

type TaskNeed struct {
	TemplateId int64
	Num        int32
	NeedNum    int32
}

type Task struct {
	misc.BaseModel
	Cid           int64              `gorm:"type:bigint; not null; comment:玩家id"`
	TaskType      task_e.TaskType    `gorm:"type:int; not null; comment:任务类型"`
	AftType       task_e.TaskAftType `gorm:"type:int; not null; comment:执行类型"`
	Record        Int32JsonRecords   `gorm:"type:text; not null; comment:计数"`
	TemplateId    int32              `gorm:"type:int; not null; comment:模板id"`
	Completed     int64              `gorm:"type:bigint; not null; comment:完成时间戳"`
	Status        task_e.TaskStatus         `gorm:"type:int; not null; comment:状态"`
	LastResetDate int64              `gorm:"type:bigint; not null; comment:上次重置时间戳"`
	InnerId       int64              `gorm:"type:bigint; not null; comment:内部id"`
	Hidden        bool               `gorm:"type:tinyint; not null; default 0; comment:是否隐藏显示"`

	Needs          []*TaskNeed `gorm:"-"`
	OriginValueMd5 string      `gorm:"-"` //用于判断任务是否有改变
}

type RowTask Task

func (t *Task) ToMarshalModel() *RowTask {
	return (*RowTask)(t)
}

func (r *RowTask) MarshalBinary() (data []byte, err error) {
	return utils.EntityMarshal(r)
}

func (r *RowTask) UnmarshalBinary(data []byte) error {
	return utils.EntityUnmarshal(data, r)
}

func GenerateTask(cid int64, template *tsv.TaskTsv) *Task {
	needs := make([]int32, 0, len(template.Needprop))
	for range template.Needprop {
		needs = append(needs, 0)
	}
	return &Task{
		Cid:           cid,
		TaskType:      task_e.TaskType(template.Type),
		AftType:       task_e.TaskAftType(template.Afttype),
		Record:        needs,
		TemplateId:    template.Id,
		Completed:     0,
		Status:        task_e.TaskStatusWait,
		LastResetDate: 0,
	}
}

type TaskRefresh struct {
	misc.BaseModel
	Cid  int64           `gorm:"type:bigint;not null;comment 玩家ID"`
	Type task_e.TaskType `gorm:"type:int;not null;comment 任务类型"`
	Date int64           `gorm:"type:bigint;not null;comment 刷新时间秒级时间戳"`
}

func (t *TaskRefresh) MarshalBinary() (data []byte, err error) {
	return utils.EntityMarshal(t)
}

func (t *TaskRefresh) UnmarshalBinary(data []byte) error {
	return utils.EntityUnmarshal(data, t)
}
