package model

import (
	"hm/pkg/misc"
	"hm/pkg/misc/utils"
)

type UnlockCommonOption int32

const (
	UnlockCommonTaskObtain   UnlockCommonOption = iota + 1 //任务获得时触发
	UnlockCommonTaskComplete                               //任务完成时触发
	UnlockCommonPlotObtain                                 //剧情获得时触发
	UnlockCommonPlotComplete                               //剧情完成时触发
)

const (
	UnlockCommonExitNovice    UnlockCommonOption = iota + 101 //从新手村退出
	UnlockCommonReincarnation                                 //正常死亡转世
)

type Unlock struct {
	misc.BaseModel
	Cid     int64            `gorm:"type:bigint;not null;comment:角色ID"`
	Records Int32JsonRecords `gorm:"type:text;not null;comment:解锁记录"`
}

type RowUnlock Unlock

func (u *Unlock) ToMarshalModel() *RowUnlock {
	return (*RowUnlock)(u)
}

func (r *RowUnlock) MarshalBinary() (data []byte, err error) {
	return utils.EntityMarshal(r)
}

func (r *RowUnlock) UnmarshalBinary(data []byte) error {
	return utils.EntityUnmarshal(data, r)
}
