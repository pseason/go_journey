package model

import "hm/pkg/misc"

type CityFarmland struct {
	misc.BaseModel
	CityId        int64 `gorm:"type:bigint; not null; comment:城市ID"`
	BuildId       int64 `gorm:"type:bigint; not null; comment:建筑ID"`
	CropOmId      int32 `gorm:"type:int; not null; default:0; comment:种植农作物Id"`
	CropHp        int64 `gorm:"type:bigint; not null; default:0; comment:种植农作物生命"`
	CropPabulum   int32 `gorm:"type:int; not null; default:0; comment:种植农作物养料"`
	FarmlandCount int32 `gorm:"type:int; not null; default:1; comment:农田个数"`
}

func GenDefaultFarmland(cityId, buildId int64) *CityFarmland {
	return &CityFarmland{
		CityId:        cityId,
		BuildId:       buildId,
		FarmlandCount: 1,
	}
}
