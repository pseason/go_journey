package model

import (
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/mall_e"
)

type Mall struct {
	misc.BaseModel
	PropId            int32               `gorm:"type:int; not null; comment:模板ID"`
	ContainNum        int32               `gorm:"type:int; not null; comment:商品包含数量"`
	MallPropType      mall_e.MallPropType `gorm:"type:int; not null; default:1; comment:商品类型"`
	LimitTime         int                 `gorm:"type:int; not null; default:0; comment:商品限时时间(小时)"`
	Price             int32               `gorm:"type:int; not null; default:1; comment:商品价格"`
	Discount          int32               `gorm:"type:int; not null; default:1; comment:商品折扣"`
	DiscountTime      int                 `gorm:"type:int; not null; default:1; comment:商品折扣限时时间(小时)"`
	CurrencyType      int32               `gorm:"type:int; not null; default:1; comment:支付货币种类"`
	MallType          mall_e.MallType     `gorm:"type:int; not null; default:1; comment:商城类别"`
	StartTime         int64               `gorm:"type:bigint; not null; default:0; comment:限时商品开始时间"`
	DiscountStartTime int64               `gorm:"type:bigint; not null; default:0; comment:折扣开始时间"`
	PurchaseType      mall_e.PurchaseType `gorm:"type:int; not null; default:0; comment:限购类型"`
	PurchaseLimit     int32               `gorm:"type:int; not null; default:0; comment:限购数量"`
	Position          int32               `gorm:"type:int; not null; default:1; comment:职位类型(王国商城对应职位，竞技场商城对应段位)"`
}
