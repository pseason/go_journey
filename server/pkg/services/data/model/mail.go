package model

import (
	"encoding/json"
	"errors"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/mail_e"
)

type Mail struct {
	misc.BaseModel
	Cid                    int64  `gorm:"type:bigint; not null; comment:玩家ID"`
	MailTitle              string `gorm:"type:varchar(500); not null; default:''; comment: 邮箱标题"`
	MailFrom               string `gorm:"type:varchar(10); not null; default:''; comment: 发件人"`
	MailContent            string `gorm:"type:varchar(500); not null; default:''; comment: 邮件内容"`
	MailEnclosures         string `gorm:"type:varchar(500); default:'0'; comment: 附件"`
	IsMailReceive          bool   `gorm:"type:tinyint; not null; default:0; comment: 邮件是否领取"`
	IsMailRead             bool   `gorm:"type:tinyint; not null; default:0; comment: 邮件是否已读"`
	MailTime               int32  `gorm:"type:int; not null; comment:邮件过期时间"`
	MailReincarnationState bool   `gorm:"type:tinyint; not null; default:0; comment:邮件转世后是否删除"`
	SystemMailId           int64  `gorm:"type:bigint; not null; comment:系统邮件ID"`
}

func NewSystemMail(cid int64, title, content string, propMap map[int64]int32) (m *Mail, err error) {
	m = GenDefaultMail()
	m.MailFrom = string(mail_e.SystemType)
	m.Cid = cid
	m.MailTitle = title
	m.MailContent = content
	var se []byte
	if propMap != nil {
		if len(propMap) > 5 {
			return nil, errors.New("创建邮件附件失败")
		}
		se, err = json.Marshal(propMap)
		if err != nil {
			return nil, err
		}
	}
	m.MailEnclosures = string(se)
	return m, nil
}

func GenDefaultMail() (m *Mail) {
	return &Mail{
		IsMailReceive: false,
		IsMailRead:    false,
		MailTime:      30,
	}
}

//SetMailReincarnationState /** 设置邮件转世后删除
func (m *Mail) SetMailReincarnationState() *Mail {
	m.MailReincarnationState = true
	return m
}
