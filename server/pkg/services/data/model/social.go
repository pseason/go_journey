package model

import (
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/social_e"
)

type Social struct {
	misc.BaseModel
	Cid            int64                       `gorm:"type:bigint; not null; comment:玩家ID"`
	Oid            int64                       `gorm:"type:bigint; not null; comment:关联玩家ID"`
	RelationStatus social_e.FriendRelationEnum `gorm:"type:tinyint; not null; default:0; comment:社交关系"`
	JoinEnemyTime  int32                       `gorm:"type:int; not null; comment:加入仇人时间(秒)"`
}
