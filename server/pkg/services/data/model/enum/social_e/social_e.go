package social_e

type FriendRelationEnum int8

const (
	//申请
	ApplyStatus FriendRelationEnum = iota
	//好友
	BuddyStatus
	//黑名单
	BlacklistStatus
	//仇人
	EnemyStatus
	//黑名单和仇人
	BlacklistAndEnemyStatus
)
