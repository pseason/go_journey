package sample_parts_e

/**
@DESC: 自动生成的model字段枚举
*/

// 别名，便于阅读
// (当某些参数列表要使用本文件的枚举时，可以定义为 sample_parts_e.Enum 类型，能够直观地知道自己需要传该文件中的枚举作为参数)
type Enum = int32

// model是否存在于redis中
func IsWithRedis() bool {
	return false
}

// 字段是否存在于数据库中
func IsInDb(code int32) (ok bool) {
	_, ok = _CodeToFieldName[code]
	return
}

// 枚举转数据库&缓存字段名
func CodeToFieldName(code int32) (fieldName string) {
	return _CodeToFieldName[code]
}

// 数据库&缓存字段名转枚举
func FieldNameToCode(fieldName string) (code int32) {
	return _FieldNameToCode[fieldName]
}

// 获取字段类型
func Type(code int32) (t string) {
	return _FieldType[code]
}

// 获取字段是否可被修改
func IsModifiable(code int32) (ok bool) {
	return _Modifiable[code]
}

// 获取所有字段枚举和字段名
func AllField() map[int32]string {
	return _CodeToFieldName
}

// SampleParts字段枚举
const (
	Parts_Id                         int32 = 1091 // 主键
	Parts_Created                    int32 = 1092 // 创建时间
	Parts_Updated                    int32 = 1093 // 更新时间
	Parts_Deleted                    int32 = 1094 // 删除时间
	Parts_Uid                        int32 = 1000 // 玩家账号ID
	Parts_Nickname                   int32 = 1001 // 昵称
	Parts_Gender                     int32 = 1002 // 性别 0女 1男
	Parts_Figure                     int32 = 1003 // 形象id
	Parts_CityId                     int32 = 1004 // 城邦id
	Parts_KingdomId                  int32 = 1005 // 王国id
	Parts_CityName                   int32 = 1006 // 城邦名
	Parts_KingdomName                int32 = 1007 // 王国名
	Parts_CityPosition               int32 = 1008 // 城邦官职id
	Parts_KingdomPosition            int32 = 1009 // 王国爵位id
	Parts_CharacterSpaceId           int32 = 1010 // 角色所在空间id
	Parts_TeamId                     int32 = 1011 // 队伍id
	Parts_LegionId                   int32 = 1012 // 军团id
	Parts_NewbieFinishTime           int32 = 1013 // 新手结束时间-秒级时间戳
	Parts_AgeAutoRefreshStartTime    int32 = 1014 // 年龄自动刷新计时时间-秒级时间戳
	Parts_OfflineTime                int32 = 1015 // 离线时间-秒级时间戳
	Parts_EnergyAutoRefreshStartTime int32 = 1016 // 精力自动刷新计时时间-秒级时间戳
)

// SampleParts字段枚举 和 数据库&缓存字段名 的映射
var _CodeToFieldName = map[int32]string{
	Parts_Id:                         "id",
	Parts_Created:                    "create",
	Parts_Updated:                    "updated",
	Parts_Deleted:                    "deleted",
	Parts_Uid:                        "uid",
	Parts_Nickname:                   "nickname",
	Parts_Gender:                     "gender",
	Parts_Figure:                     "figure",
	Parts_CityId:                     "city_id",
	Parts_KingdomId:                  "kingdom_id",
	Parts_CityName:                   "city_name",
	Parts_KingdomName:                "kingdom_name",
	Parts_CityPosition:               "city_position",
	Parts_KingdomPosition:            "kingdom_position",
	Parts_CharacterSpaceId:           "character_space_id",
	Parts_TeamId:                     "team_id",
	Parts_LegionId:                   "legion_id",
	Parts_NewbieFinishTime:           "newbie_finish_time",
	Parts_AgeAutoRefreshStartTime:    "age_auto_refresh_start_time",
	Parts_OfflineTime:                "offline_time",
	Parts_EnergyAutoRefreshStartTime: "energy_auto_refresh_start_time",
}

// SampleParts数据库&缓存字段名 和 字段枚举 的映射
var _FieldNameToCode = map[string]int32{
	"id":                             Parts_Id,
	"create":                         Parts_Created,
	"updated":                        Parts_Updated,
	"deleted":                        Parts_Deleted,
	"uid":                            Parts_Uid,
	"nickname":                       Parts_Nickname,
	"gender":                         Parts_Gender,
	"figure":                         Parts_Figure,
	"city_id":                        Parts_CityId,
	"kingdom_id":                     Parts_KingdomId,
	"city_name":                      Parts_CityName,
	"kingdom_name":                   Parts_KingdomName,
	"city_position":                  Parts_CityPosition,
	"kingdom_position":               Parts_KingdomPosition,
	"character_space_id":             Parts_CharacterSpaceId,
	"team_id":                        Parts_TeamId,
	"legion_id":                      Parts_LegionId,
	"newbie_finish_time":             Parts_NewbieFinishTime,
	"age_auto_refresh_start_time":    Parts_AgeAutoRefreshStartTime,
	"offline_time":                   Parts_OfflineTime,
	"energy_auto_refresh_start_time": Parts_EnergyAutoRefreshStartTime,
}

// SampleParts字段枚举 和 字段类型 的映射
var _FieldType = map[int32]string{
	Parts_Id:                         "int64",
	Parts_Created:                    "int64",
	Parts_Updated:                    "int64",
	Parts_Deleted:                    "int64",
	Parts_Uid:                        "int64",
	Parts_Nickname:                   "string",
	Parts_Gender:                     "uint8",
	Parts_Figure:                     "FigureType",
	Parts_CityId:                     "int64",
	Parts_KingdomId:                  "int64",
	Parts_CityName:                   "string",
	Parts_KingdomName:                "string",
	Parts_CityPosition:               "int32",
	Parts_KingdomPosition:            "int32",
	Parts_CharacterSpaceId:           "int64",
	Parts_TeamId:                     "int64",
	Parts_LegionId:                   "int64",
	Parts_NewbieFinishTime:           "int64",
	Parts_AgeAutoRefreshStartTime:    "int64",
	Parts_OfflineTime:                "int64",
	Parts_EnergyAutoRefreshStartTime: "int64",
}

// SampleParts字段枚举 和 是否可被修改 的映射
var _Modifiable = map[int32]bool{
	Parts_Id:                         false,
	Parts_Created:                    false,
	Parts_Updated:                    false,
	Parts_Deleted:                    false,
	Parts_Uid:                        true,
	Parts_Nickname:                   true,
	Parts_Gender:                     true,
	Parts_Figure:                     true,
	Parts_CityId:                     true,
	Parts_KingdomId:                  true,
	Parts_CityName:                   true,
	Parts_KingdomName:                true,
	Parts_CityPosition:               true,
	Parts_KingdomPosition:            true,
	Parts_CharacterSpaceId:           true,
	Parts_TeamId:                     true,
	Parts_LegionId:                   true,
	Parts_NewbieFinishTime:           true,
	Parts_AgeAutoRefreshStartTime:    true,
	Parts_OfflineTime:                true,
	Parts_EnergyAutoRefreshStartTime: true,
}
