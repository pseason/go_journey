package building_e

type BuildingType uint8
type BuildingState uint8
type BuildingInfo int32

const (
	//投入资源
	InvestResource BuildingState = iota
	//投入建设度
	InvestEnergy
	//升级
	Upgrade
)

// 建筑类型
const (
	//仓库
	TypeDepot BuildingType = iota + 1
	//烹饪坊
	TypeCookWorkshop
	//工匠坊
	TypeCraftsmanWorkshop
	//箭塔
	TypeSentryTower
	//城镇中心
	TypeTownCenter
	//炼药房
	TypePharmacy
	//民房
	TypePrivateHouse
	//市场
	TypeMarketConst
	//农场
	TypeFarm
	//畜牧场
	TypeLivestockRanch
	//围墙
	TypeFence
	//水井
	TypeWell
	//校场
	TypeSchool
	//酿酒坊
	TypeWine
	//城门
	TypeCityGate
)

//建筑属性编号
const(
	BuildingInfoType BuildingInfo = iota +1
	BuildingInfoState
	BuildingInfoLv
)