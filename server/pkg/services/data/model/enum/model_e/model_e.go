package model_e

import (
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/model/enum/character_info_e"
	"hm/pkg/services/data/model/enum/sample_e"
	"hm/pkg/services/data/model/enum/sample_parts_e"
)

/**
@DESC: 自动生成的model枚举
*/

type Model = uint8

const (
	CharacterInfo Model = 1
	Sample        Model = 2
	SampleParts   Model = 3
)

func ModelName(e Model) string {
	return _ModelName[e]
}

func NewModelPtr(e Model) (model interface{}) {
	return _ModelPtrFac[e]()
}

func NewModel(e Model) (model interface{}) {
	return _ModelFac[e]()
}

func NewModelPtrSlice(e Model) (model interface{}) {
	return _ModelPtrSliceFac[e]()
}

func NewModelPtrSlicePtr(e Model) (model interface{}) {
	return _ModelPtrSlicePtrFac[e]()
}

func IsWithRedis(e Model) bool {
	return Func_IsWithRedis(e)()
}

func IsInDb(e Model, code int32) bool {
	return Func_IsInDb(e)(code)
}

func IsModifiable(e Model, code int32) bool {
	return Func_IsModifiable(e)(code)
}

func Type(e Model, code int32) string {
	return Func_Type(e)(code)
}

func IsInt32(e Model, code int32) bool {
	return Type(e, code) == "int32"
}

func ToFieldName(e Model, code int32) (fieldName string) {
	return Func_CodeToFieldName(e)(code)
}

func ToCode(e Model, fieldName string) (code int32) {
	return Func_FieldNameToCode(e)(fieldName)
}

func AllField(e Model) map[int32]string {
	return Func_AllField(e)()
}

func Func_IsWithRedis(e Model) func() (ok bool) {
	return _Func_IsWithRedis[e]
}

func Func_IsInDb(e Model) func(code int32) (ok bool) {
	return _Func_IsInDb[e]
}

func Func_IsModifiable(e Model) func(code int32) (ok bool) {
	return _Func_IsModifiable[e]
}

func Func_Type(e Model) func(code int32) (t string) {
	return _Func_Type[e]
}

func Func_CodeToFieldName(e Model) func(code int32) (fieldName string) {
	return _Func_CodeToFieldName[e]
}

func Func_FieldNameToCode(e Model) func(fieldName string) (code int32) {
	return _Func_FieldNameToCode[e]
}

func Func_AllField(e Model) func() map[int32]string {
	return _Func_AllField[e]
}

var _ModelName = map[Model]string{
	CharacterInfo: "CharacterInfo",
	Sample:        "Sample",
	SampleParts:   "SampleParts",
}

var _Func_IsWithRedis = map[Model]func() (ok bool){
	CharacterInfo: character_info_e.IsWithRedis,
	Sample:        sample_e.IsWithRedis,
	SampleParts:   sample_parts_e.IsWithRedis,
}

var _Func_IsInDb = map[Model]func(code int32) (ok bool){
	CharacterInfo: character_info_e.IsInDb,
	Sample:        sample_e.IsInDb,
	SampleParts:   sample_parts_e.IsInDb,
}

var _Func_IsModifiable = map[Model]func(code int32) (ok bool){
	CharacterInfo: character_info_e.IsModifiable,
	Sample:        sample_e.IsModifiable,
	SampleParts:   sample_parts_e.IsModifiable,
}

var _Func_Type = map[Model]func(code int32) (t string){
	CharacterInfo: character_info_e.Type,
	Sample:        sample_e.Type,
	SampleParts:   sample_parts_e.Type,
}

var _Func_CodeToFieldName = map[Model]func(code int32) (fieldName string){
	CharacterInfo: character_info_e.CodeToFieldName,
	Sample:        sample_e.CodeToFieldName,
	SampleParts:   sample_parts_e.CodeToFieldName,
}

var _Func_FieldNameToCode = map[Model]func(fieldName string) (code int32){
	CharacterInfo: character_info_e.FieldNameToCode,
	Sample:        sample_e.FieldNameToCode,
	SampleParts:   sample_parts_e.FieldNameToCode,
}

var _Func_AllField = map[Model]func() map[int32]string{
	CharacterInfo: character_info_e.AllField,
	Sample:        sample_e.AllField,
	SampleParts:   sample_parts_e.AllField,
}

var _ModelFac = map[Model]func() interface{}{
	CharacterInfo: func() interface{} {
		return model.CharacterInfo{}
	},
	Sample: func() interface{} {
		return model.Sample{}
	},
	SampleParts: func() interface{} {
		return model.SampleParts{}
	},
}

var _ModelPtrFac = map[Model]func() interface{}{
	CharacterInfo: func() interface{} {
		return &model.CharacterInfo{}
	},
	Sample: func() interface{} {
		return &model.Sample{}
	},
	SampleParts: func() interface{} {
		return &model.SampleParts{}
	},
}

var _ModelPtrSliceFac = map[Model]func() interface{}{
	CharacterInfo: func() interface{} {
		return []model.CharacterInfo{}
	},
	Sample: func() interface{} {
		return []model.Sample{}
	},
	SampleParts: func() interface{} {
		return []model.SampleParts{}
	},
}

var _ModelPtrSlicePtrFac = map[Model]func() interface{}{
	CharacterInfo: func() interface{} {
		return &[]*model.CharacterInfo{}
	},
	Sample: func() interface{} {
		return &[]*model.Sample{}
	},
	SampleParts: func() interface{} {
		return &[]*model.SampleParts{}
	},
}
