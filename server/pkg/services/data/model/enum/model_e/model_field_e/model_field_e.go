package model_field_e

/*
@Time   : 2022-01-08 17:42
@Author : wushu
@DESC   :
*/

//  当你看到该类型时，应当传入 与你当前业务逻辑相关的model枚举文件中的字段枚举
//  该别名类型表示所有 model/enum/xxx_e/xxx_e.go下的字段枚举
//  如 character_info_e.Enum、city_e.Enum 等
//  该用法只为了 在定义model字段枚举的参数类型时 避免写作int32导致调用者不清楚到底需要传什么
type Enum = int32