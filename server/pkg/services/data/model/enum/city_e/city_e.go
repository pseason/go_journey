package city_e

type CityInfo int32

// 枚举对应的数据库中的字段
var _StringMap map[CityInfo]string

func init() {
	_StringMap = map[CityInfo]string{
		Id:                   "id",
		Created:              "created",
		Level:                "level",
		CityNick:             "city_nick",
		Proclamation:         "proclamation",
		HeirsCid:             "heirs_cid",
		BannerProp:           "banner_prop",
		ImpeachVotes:         "impeach_votes",
		ExileNum:             "exile_num",
		DemiseNum:            "demise_num",
		CityPersonMax:        "city_person_max",
		ProsperityNum:        "prosperity_num",
		VoteState:            "vote_state",
		VoteEndTime:          "vote_end_time",
		ExtendsElectionCount: "extends_election_count",
		LeaderElectionCount:  "leader_election_count",
		CityTempId: "city_temp_id",
	}
}

func (info CityInfo) String() (str string) {
	return _StringMap[info]
}

// 城邦基础信息
const (
	Id                   CityInfo = 101 + iota // id
	Created                                    // 创建时间
	Level                                      //城市等级
	CityNick                                   //城市名称
	Proclamation                               //公告内容
	HeirsCid                                   //继承者ID
	BannerProp                                 //旗帜模板id
	ImpeachVotes                               //弹劾票数
	ExileNum                                   //每日首领流放个数
	DemiseNum                                  //每周禅让次数
	CityPersonMax                              //城市人数上限
	ProsperityNum                              //繁荣度
	VoteState                                  //投票状态
	VoteEndTime                                //投票结束时间
	ExtendsElectionCount                       //继承人选举届数
	LeaderElectionCount                        //首领选举届数
	CityTempId				//城市模板
)

//VoteState /**城邦投票类型
type VoteType int32

const (
	NotState      VoteType = iota //无状态
	Impeach                       //弹劾
	HeirElection                  //继承人选举
	SantoElection                 //城主选举
)

func (v VoteType) ToBaseType() interface{} {
	return int32(v)
}

//Position /**城邦身份类型
type Identity int32

const (
	Default                  Identity = iota //无身份
	Master                                   //城主
	ViceMaster                               //副城主
	UrbanConstructionOfficer                 //城建官
	AgricultureOfficer                       //农业官
	CityDefenseOfficer                       //城防官
	Builder                                  //建造者
	Producer                                 //生产者
	Soldier                                  //士兵
	Collector                                //采集者
	CityMember                               //普通城民
)

func (p Identity) ToBaseType() interface{} {
	return int32(p)
}

//ApplyType /**城邦成员关系类型
type ApplyType int32

const (
	Apply ApplyType = iota //申请

	Member //成员
)

func (a ApplyType) ToBaseType() interface{} {
	return int32(a)
}

//城邦事件
const (
	CityJoinKingdom          = "本城邦加入%s王国"
	CityExitKingdom          = "本城邦退出%s王国"
	CityInitiateExtendsElect = "%s发起继承人选举"
	CityInitiateLeaderElect  = "%s发起城主选举"
	CityInitiateImpeach      = "%s对%s发起弹劾"
	CityInitiateDemise       = "%s通过禅让将城主移交给%s"
)

//排队玩家类型
type QueuePlayerType int8

const (
	Enter QueuePlayerType = iota
	JoinCity
)
