package clan_e

type ClanIdentity int32

const (
	Patriarch         ClanIdentity = iota //族长
	FormalMember                          //正式成员
	PreparationMember                     //预备成员
)
