package mail_e

//MailType /**邮件类型
type MailType string

const (
	SystemType MailType = "系统邮件"
	Personal            //个人
)

//OperationType /**邮件操作类型
type OperationType int32

const (
	DELMAIL        OperationType = iota + 1 //单个删除
	RECEIVEMAIL                             //领取附件
	READMAIL                                //已读邮件
	DELMAILALL                              //一键清理
	RECEIVEMAILALL                          //一键领取附件并设置已读和已领
	Test
)
