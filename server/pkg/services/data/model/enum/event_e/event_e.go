package event_e

type CityEventType int
type KingdomEventType int

const (
	CityEvent       CityEventType = iota + 1 //城邦事件
	CityMemberEvent                          //城邦成员事件 1.成员加入 2.成员退出
	CityBuildEvent                           //城邦建筑事件
	CityBuildRecord                          //城邦建筑记录
)

const (
	KingdomEvent     KingdomEventType = iota + 1 //王国事件
	TerritorialEvent                             //领土事件
)
