package character_info_e

/**
@DESC: 自动生成的model字段枚举
*/

// 别名，便于阅读
// (当某些参数列表要使用本文件的枚举时，可以定义为 character_info_e.Enum 类型，能够直观地知道自己需要传该文件中的枚举作为参数)
type Enum = int32

// model是否存在于redis中
func IsWithRedis() bool {
	return true
}

// 字段是否存在于数据库中
func IsInDb(code int32) (ok bool) {
	_, ok = _CodeToFieldName[code]
	return
}

// 枚举转数据库&缓存字段名
func CodeToFieldName(code int32) (fieldName string) {
	return _CodeToFieldName[code]
}

// 数据库&缓存字段名转枚举
func FieldNameToCode(fieldName string) (code int32) {
	return _FieldNameToCode[fieldName]
}

// 获取字段类型
func Type(code int32) (t string) {
	return _FieldType[code]
}

// 获取字段是否可被修改
func IsModifiable(code int32) (ok bool) {
	return _Modifiable[code]
}

// 获取所有字段枚举和字段名
func AllField() map[int32]string {
	return _CodeToFieldName
}

// CharacterInfo字段枚举
const (
	Id                         int32 = 1001 // 主键
	Created                    int32 = 1002 // 创建时间
	Updated                    int32 = 1003 // 更新时间
	Deleted                    int32 = 1004 // 删除时间
	Uid                        int32 = 1006 // 玩家账号ID
	Nickname                   int32 = 1007 // 昵称
	Gender                     int32 = 1008 // 性别 0女 1男
	Figure                     int32 = 1009 // 形象id
	Birthday                   int32 = 1010 // 氏族系统-生辰-秒级时间戳
	Introduction               int32 = 1011 // 氏族系统-个人简介
	ClanId                     int32 = 1012 // 氏族id
	CityId                     int32 = 1013 // 城邦id
	KingdomId                  int32 = 1014 // 王国id
	ClanName                   int32 = 1015 // 氏族名
	CityName                   int32 = 1016 // 城邦名
	KingdomName                int32 = 1017 // 王国名
	ClanIdentity               int32 = 1018 // 氏族身份id
	CityPosition               int32 = 1019 // 城邦官职id
	KingdomPosition            int32 = 1020 // 王国爵位id
	CharacterSpaceId           int32 = 1021 // 角色所在空间id
	TeamId                     int32 = 1022 // 队伍id
	NewbieFinishTime           int32 = 1023 // 新手结束时间-秒级时间戳
	AgeAutoRefreshStartTime    int32 = 1024 // 年龄自动刷新计时时间-秒级时间戳
	OfflineTime                int32 = 1025 // 离线时间-秒级时间戳
	EnergyAutoRefreshStartTime int32 = 1026 // 精力自动刷新计时时间-秒级时间戳
	MonthCardExpiresAt         int32 = 1027 // 月卡到期时间-秒级时间戳
	BestMonthCardExpiresAt     int32 = 1028 // 至尊月卡到期时间-秒级时间戳
	Gold                       int32 = 2001 // 金币
	UnrealCoin                 int32 = 2002 // 虚幻币
	ClanContributionVal        int32 = 2003 // 氏族贡献值
	CityContributionVal        int32 = 2004 // 城邦贡献值
	KingdomContributionVal     int32 = 2005 // 王国贡献值
	Energy                     int32 = 2006 // 精力
	Age                        int32 = 2007 // 年龄
	LifeTime                   int32 = 2008 // 寿命
	Merit                      int32 = 2009 // 功德
	Prestige                   int32 = 2010 // 威望
	FairyRank                  int32 = 2011 // 仙阶
	MagicRank                  int32 = 2012 // 魔阶
	BeInfatuatedVal            int32 = 2013 // 入魔值
	BuildLv                    int32 = 2014 // 建造等级
	CookLv                     int32 = 2015 // 烹饪等级
	CraftsLv                   int32 = 2016 // 工匠等级
	PharmacyLv                 int32 = 2017 // 炼药等级
	WineLv                     int32 = 2018 // 酿酒等级
	LifeSkillPoints            int32 = 2019 // 生活技能点
	SoulEnergy                 int32 = 2020 // 灵魂能量
	BattlePower                int32 = 2021 // 战力
	WareHouseLv                int32 = 2022 // 仓库等级
)

// CharacterInfo字段枚举 和 数据库&缓存字段名 的映射
var _CodeToFieldName = map[int32]string{
	Id:                         "id",
	Created:                    "create",
	Updated:                    "updated",
	Deleted:                    "deleted",
	Uid:                        "uid",
	Nickname:                   "nickname",
	Gender:                     "gender",
	Figure:                     "figure",
	Birthday:                   "birthday",
	Introduction:               "introduction",
	ClanId:                     "clan_id",
	CityId:                     "city_id",
	KingdomId:                  "kingdom_id",
	ClanName:                   "clan_name",
	CityName:                   "city_name",
	KingdomName:                "kingdom_name",
	ClanIdentity:               "clan_identity",
	CityPosition:               "city_position",
	KingdomPosition:            "kingdom_position",
	CharacterSpaceId:           "character_space_id",
	TeamId:                     "team_id",
	NewbieFinishTime:           "newbie_finish_time",
	AgeAutoRefreshStartTime:    "age_auto_refresh_start_time",
	OfflineTime:                "offline_time",
	EnergyAutoRefreshStartTime: "energy_auto_refresh_start_time",
	MonthCardExpiresAt:         "month_card_expires_at",
	BestMonthCardExpiresAt:     "best_month_card_expires_at",
	Gold:                       "gold",
	UnrealCoin:                 "unreal_coin",
	ClanContributionVal:        "clan_contribution_val",
	CityContributionVal:        "city_contribution_val",
	KingdomContributionVal:     "kingdom_contribution_val",
	Energy:                     "energy",
	Age:                        "age",
	LifeTime:                   "life_time",
	Merit:                      "merit",
	Prestige:                   "prestige",
	FairyRank:                  "fairy_rank",
	MagicRank:                  "magic_rank",
	BeInfatuatedVal:            "be_infatuated_val",
	BuildLv:                    "build_lv",
	CookLv:                     "cook_lv",
	CraftsLv:                   "crafts_lv",
	PharmacyLv:                 "pharmacy_lv",
	WineLv:                     "wine_lv",
	LifeSkillPoints:            "life_skill_points",
	SoulEnergy:                 "soul_energy",
	BattlePower:                "battle_power",
	WareHouseLv:                "ware_house_lv",
}

// CharacterInfo数据库&缓存字段名 和 字段枚举 的映射
var _FieldNameToCode = map[string]int32{
	"id":                             Id,
	"create":                         Created,
	"updated":                        Updated,
	"deleted":                        Deleted,
	"uid":                            Uid,
	"nickname":                       Nickname,
	"gender":                         Gender,
	"figure":                         Figure,
	"birthday":                       Birthday,
	"introduction":                   Introduction,
	"clan_id":                        ClanId,
	"city_id":                        CityId,
	"kingdom_id":                     KingdomId,
	"clan_name":                      ClanName,
	"city_name":                      CityName,
	"kingdom_name":                   KingdomName,
	"clan_identity":                  ClanIdentity,
	"city_position":                  CityPosition,
	"kingdom_position":               KingdomPosition,
	"character_space_id":             CharacterSpaceId,
	"team_id":                        TeamId,
	"newbie_finish_time":             NewbieFinishTime,
	"age_auto_refresh_start_time":    AgeAutoRefreshStartTime,
	"offline_time":                   OfflineTime,
	"energy_auto_refresh_start_time": EnergyAutoRefreshStartTime,
	"month_card_expires_at":          MonthCardExpiresAt,
	"best_month_card_expires_at":     BestMonthCardExpiresAt,
	"gold":                           Gold,
	"unreal_coin":                    UnrealCoin,
	"clan_contribution_val":          ClanContributionVal,
	"city_contribution_val":          CityContributionVal,
	"kingdom_contribution_val":       KingdomContributionVal,
	"energy":                         Energy,
	"age":                            Age,
	"life_time":                      LifeTime,
	"merit":                          Merit,
	"prestige":                       Prestige,
	"fairy_rank":                     FairyRank,
	"magic_rank":                     MagicRank,
	"be_infatuated_val":              BeInfatuatedVal,
	"build_lv":                       BuildLv,
	"cook_lv":                        CookLv,
	"crafts_lv":                      CraftsLv,
	"pharmacy_lv":                    PharmacyLv,
	"wine_lv":                        WineLv,
	"life_skill_points":              LifeSkillPoints,
	"soul_energy":                    SoulEnergy,
	"battle_power":                   BattlePower,
	"ware_house_lv":                  WareHouseLv,
}

// CharacterInfo字段枚举 和 字段类型 的映射
var _FieldType = map[int32]string{
	Id:                         "int64",
	Created:                    "int64",
	Updated:                    "int64",
	Deleted:                    "int64",
	Uid:                        "int64",
	Nickname:                   "string",
	Gender:                     "uint8",
	Figure:                     "FigureType",
	Birthday:                   "int32",
	Introduction:               "string",
	ClanId:                     "int64",
	CityId:                     "int64",
	KingdomId:                  "int64",
	ClanName:                   "string",
	CityName:                   "string",
	KingdomName:                "string",
	ClanIdentity:               "int32",
	CityPosition:               "int32",
	KingdomPosition:            "int32",
	CharacterSpaceId:           "int64",
	TeamId:                     "int64",
	NewbieFinishTime:           "int64",
	AgeAutoRefreshStartTime:    "int64",
	OfflineTime:                "int64",
	EnergyAutoRefreshStartTime: "int64",
	MonthCardExpiresAt:         "int64",
	BestMonthCardExpiresAt:     "int64",
	Gold:                       "int32",
	UnrealCoin:                 "int32",
	ClanContributionVal:        "int32",
	CityContributionVal:        "int32",
	KingdomContributionVal:     "int32",
	Energy:                     "int32",
	Age:                        "int32",
	LifeTime:                   "int32",
	Merit:                      "int32",
	Prestige:                   "int32",
	FairyRank:                  "int32",
	MagicRank:                  "int32",
	BeInfatuatedVal:            "int32",
	BuildLv:                    "int32",
	CookLv:                     "int32",
	CraftsLv:                   "int32",
	PharmacyLv:                 "int32",
	WineLv:                     "int32",
	LifeSkillPoints:            "int32",
	SoulEnergy:                 "int32",
	BattlePower:                "int32",
	WareHouseLv:                "int32",
}

// CharacterInfo字段枚举 和 是否可被修改 的映射
var _Modifiable = map[int32]bool{
	Id:                         false,
	Created:                    false,
	Updated:                    false,
	Deleted:                    false,
	Uid:                        false,
	Nickname:                   true,
	Gender:                     true,
	Figure:                     true,
	Birthday:                   true,
	Introduction:               true,
	ClanId:                     true,
	CityId:                     true,
	KingdomId:                  true,
	ClanName:                   true,
	CityName:                   true,
	KingdomName:                true,
	ClanIdentity:               true,
	CityPosition:               true,
	KingdomPosition:            true,
	CharacterSpaceId:           true,
	TeamId:                     true,
	NewbieFinishTime:           true,
	AgeAutoRefreshStartTime:    true,
	OfflineTime:                true,
	EnergyAutoRefreshStartTime: true,
	MonthCardExpiresAt:         true,
	BestMonthCardExpiresAt:     true,
	Gold:                       true,
	UnrealCoin:                 true,
	ClanContributionVal:        true,
	CityContributionVal:        true,
	KingdomContributionVal:     true,
	Energy:                     true,
	Age:                        true,
	LifeTime:                   true,
	Merit:                      true,
	Prestige:                   true,
	FairyRank:                  true,
	MagicRank:                  true,
	BeInfatuatedVal:            true,
	BuildLv:                    true,
	CookLv:                     true,
	CraftsLv:                   true,
	PharmacyLv:                 true,
	WineLv:                     true,
	LifeSkillPoints:            true,
	SoulEnergy:                 true,
	BattlePower:                true,
	WareHouseLv:                true,
}
