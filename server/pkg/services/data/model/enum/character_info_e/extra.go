package character_info_e

/*
@Time   : 2021-12-09 22:17
@Author : wushu
@DESC   :
*/

// 不需要通知给客户端的
var _NotNotice map[int32]bool

// 需要广播到视野的
var _IsBroadcast map[int32]bool

func init() {
	// 不需要通知给客户端的
	_NotNotice = map[int32]bool{
		AgeAutoRefreshStartTime:    true,
		OfflineTime:                true,
		EnergyAutoRefreshStartTime: true,
	}

	// 需要广播到视野的
	_IsBroadcast = map[int32]bool{
		Nickname: true,
		TeamId:   true,
		ClanId:   true,
	}
}

// 是否需要通知给客户端
func IsNoticeClient(field int32) bool {
	return !_NotNotice[field]
}

// 是否需要广播到视野
func IsBroadcast(field int32) bool {
	return _IsBroadcast[field]
}
