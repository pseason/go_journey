package task_e

import "hm/pkg/misc/utils"

type TaskAftType int32

type RowTaskAftType TaskAftType

func (t *TaskAftType) ToMarshalModel() *RowTaskAftType {
	return (*RowTaskAftType)(t)
}

func (t RowTaskAftType) MarshalBinary() (data []byte, err error) {
	return utils.EntityMarshal(t)
}

func (t RowTaskAftType) UnmarshalBinary(data []byte) error {
	return utils.EntityUnmarshal(data, t)
}

const (
	// TaskAftPick 采集
	// @TemplateId: 采集或得到的物品模板ID, @Num: 获得到的数量
	TaskAftPick TaskAftType = iota + 1
	// TaskAftHunt 打怪
	// @TemplateId: 野怪模板ID, @Num: 数量
	TaskAftHunt
	// TaskAftEquipBuild 装备打造
	// @TemplateId: 装备合成ID, @Num: 数量
	TaskAftEquipBuild
	// TaskAftUseSkills 使用技能
	// @TemplateId: 技能ID, @Num: 次数，可选（默认：1）
	TaskAftUseSkills
	// TaskAftSceneSwitch 场景切换
	// @TemplateId: 场景ID, @Num: 次数，可选（默认：1）
	TaskAftSceneSwitch
	// TaskAftBuildInRes 建筑-投入资源
	// @TemplateId: 资源ID, @Num: 数量
	TaskAftBuildInRes
	// TaskAftBuilding 建筑-建造
	// @TemplateId: 建筑类型, @Num: 等级
	TaskAftBuilding
	// TaskAftUseProp 使用物品
	// @TemplateId: 物品模板ID, @Num: 数量
	TaskAftUseProp
	// TaskAftEquipmentSkills 装备技能
	// @TemplateId: 技能ID, @Num: 次数，可选（默认：1）
	TaskAftEquipmentSkills
	// TaskAftSoulReincarnation 灵魂转世（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftSoulReincarnation
	// TaskAftLogin 登录
	// 无参数
	TaskAftLogin
	// TaskAftDuplicate 副本（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftDuplicate
	// TaskAftBattlefield 战场（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftBattlefield
	// TaskAftArena 竞技场
	// @TemplateId: 场景ID, @Success: 是否胜利
	TaskAftArena
	// TaskAftMoba moba（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftMoba
	// TaskAftKillPlayer 击杀玩家
	// 无参数
	TaskAftKillPlayer
	// TaskAftSpeak 发言（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftSpeak
	// TaskAftGive 赠送（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftGive
	// TaskAftDrawCard 聚灵
	// @TemplateId: 奖池ID, @Num: 数量
	TaskAftDrawCard
	// TaskAftActionSign 签到
	// 无参数
	TaskAftActionSign
	// TaskAftPickMain 采集主体
	// @TemplateId: 采集的目标模板ID, @Num: 数量
	TaskAftPickMain
	// TaskAftFreshmanGfCity 新手攻防据点
	// 无参数
	TaskAftFreshmanGfCity
	// TaskAftWearEquipment 穿戴装备
	// @TemplateId: 装备模板ID
	TaskAftWearEquipment
	// TaskAftSkillUpgrade 技能升级
	// @TemplateId: 技能模板ID
	TaskAftSkillUpgrade
	// TaskAftJoinCity 加入公会
	// 无参数
	TaskAftJoinCity
	// TaskAftSurpriseAttack 奇袭
	// 无参数
	TaskAftSurpriseAttack
	// TaskAftActivityValue 活跃度
	// @Num: 新增活跃值
	TaskAftActivityValue
	// TaskAftJoinKingdom 加入王国（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftJoinKingdom
	// TaskAftGrowOlder 年龄增长（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftGrowOlder
	// TaskAftKingdomDeclaresWar 王国宣战（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftKingdomDeclaresWar
	// TaskAftSiege 攻城战（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftSiege
	// TaskAftArenaMatch 竞技场匹配
	// 无参数
	TaskAftArenaMatch
	// TaskAftActiveSubmit 主动提交
	// 无参数
	TaskAftActiveSubmit
	// TaskAftNoviceDeath 新手村玩家死亡
	// 无参数
	TaskAftNoviceDeath
	// TaskAftSkillStarUpgrade 技能升星
	// @TemplateId: 技能模板ID, @Num: 升星数量
	TaskAftSkillStarUpgrade
	// TaskAftArenaLevel 指定竞技场提升到指定的段位
	// @TemplateId: 竞技场ID, @Num: 段位等级
	TaskAftArenaLevel
	// TaskAftArenaKillCount 在指定的竞技场中，完成指定次数击杀
	// @TemplateId: 竞技场ID, @Num: 击杀次数
	TaskAftArenaKillCount
	// TaskAftCityUpgrade 所在据点达到指定的等级（据点升级时调用）
	// @Num: 当前等级
	TaskAftCityUpgrade
	// TaskAftKingdomUpgrade 所在王国达到指定的等级（王国升级时调用）
	// @Num: 当前等级
	TaskAftKingdomUpgrade
	// TaskAftKingdomCityUpgrade 同一个王国下的多个据点，达到指定等级（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftKingdomCityUpgrade
	// TaskAftCharacterInfoChange 玩家属性改变（暂时无用）
	// @TemplateId: Wait, @Num: Wait
	TaskAftCharacterInfoChange
	// TaskAftCharacterInfoCurrent 玩家当前属性值
	// @TemplateId: 属性ID, @Num: 当前属性值
	TaskAftCharacterInfoCurrent
	// TaskAftNextDayLogin 次日登录
	// 无参数
	TaskAftNextDayLogin
	// TaskAftJoinLegion 加入军团
	// 无参数
	TaskAftJoinLegion
	// TaskAftCreateNextDayLogin 自建号的次日登录
	// 无参数
	TaskAftCreateNextDayLogin
	// TaskAftAutoComplete 获得任务就自动完成
	// 无参数
	TaskAftAutoComplete
)
