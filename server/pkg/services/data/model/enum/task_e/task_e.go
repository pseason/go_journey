package task_e

type ReSetTimeType int8

const(
	Daily ReSetTimeType = iota + 1
	Week
)

type TaskType int32

type TaskStatus int32

const (
	TaskTypeFreshman     TaskType = iota + 1 //新手任务(指引任务)
	TaskTypeMain                             //主线任务
	TaskTypeBranch                           //分支任务
	TaskTypeAdventure                        //奇遇任务
	TaskTypeBuild                            //建造任务
	TaskTypeImmortal                         //仙系任务
	TaskTypeMagic                            //魔系任务
	TaskTypeDaily                            //日常任务
	TaskTypeCityActivity                     //城邦活动任务
	TaskTypeAchievement                      //成就任务
)

const (
	TaskStatusWait      TaskStatus = iota + 1
	TaskStatusCompleted            //已完成
	TaskStatusExpired              //已过期
	TaskStatusReceived             //已领取奖励
	TaskWaitHandel				   //待处理已完成
)