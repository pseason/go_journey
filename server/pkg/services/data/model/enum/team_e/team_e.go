package team_e

type CarryType int
type TeamType int
type RelationshipType int

const (
	OrdinaryTeam TeamType = iota + 1 //普通队伍
	LegionTeam                       //军团队伍
)

const (
	FreeCollect CarryType = iota + 1 //自由拾取
	KillCollect                      //击杀拾取
)

const (
	ApplyTeam  RelationshipType = iota + 1 //申请组队
	InviteTeam                             //邀请组队
)