package mall_e

//商城类型
type MallType int
//商品限购类型
type PurchaseType int

const (
	Defult PurchaseType = iota //默认
	Day  //日
	Week //周
)

const (
	GameMallType MallType = iota + 1 //游戏商城
	VisionalMallType  //虚幻商城
	KingdomMallType   //王国商城
	BuildCityMaLlType //攻城商城
	EncounterMaLlType //遭遇商城
)

//商品类型
type MallPropType int

const (
	bestsellers MallPropType = iota + 1 //热卖
	consume //消耗
	treasure //宝物
	skill //技能
)
