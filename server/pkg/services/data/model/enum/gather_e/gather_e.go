package gather_e

type ResourceInfo int32

const (
	ResourceId ResourceInfo = iota + 100
	ResourceIsEmpty
)