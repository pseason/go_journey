package sample_e

/**
@DESC: 自动生成的model字段枚举
*/

// 别名，便于阅读
// (当某些参数列表要使用本文件的枚举时，可以定义为 sample_e.Enum 类型，能够直观地知道自己需要传该文件中的枚举作为参数)
type Enum = int32

// model是否存在于redis中
func IsWithRedis() bool {
	return false
}

// 字段是否存在于数据库中
func IsInDb(code int32) (ok bool) {
	_, ok = _CodeToFieldName[code]
	return
}

// 枚举转数据库&缓存字段名
func CodeToFieldName(code int32) (fieldName string) {
	return _CodeToFieldName[code]
}

// 数据库&缓存字段名转枚举
func FieldNameToCode(fieldName string) (code int32) {
	return _FieldNameToCode[fieldName]
}

// 获取字段类型
func Type(code int32) (t string) {
	return _FieldType[code]
}

// 获取字段是否可被修改
func IsModifiable(code int32) (ok bool) {
	return _Modifiable[code]
}

// 获取所有字段枚举和字段名
func AllField() map[int32]string {
	return _CodeToFieldName
}

// Sample字段枚举
const (
	Id                         int32 = 1    // 主键
	Created                    int32 = 2    // 创建时间
	Updated                    int32 = 3    // 更新时间
	Deleted                    int32 = 4    // 删除时间
	Uid                        int32 = 5    // 玩家账号ID
	Nickname                   int32 = 6    // 玩家昵称
	Gender                     int32 = 7    // 玩家性别 0女 1男
	Figure                     int32 = 8    // 形象id
	CityId                     int32 = 9    // 城邦id
	KingdomId                  int32 = 10   // 王国id
	CityName                   int32 = 11   // 城邦名
	KingdomName                int32 = 12   // 王国名
	CityPosition               int32 = 13   // 城邦官职id
	KingdomPosition            int32 = 14   // 王国爵位id
	CharacterSpaceId           int32 = 15   // 角色所在空间id
	TeamId                     int32 = 16   // 队伍id
	LegionId                   int32 = 17   // 军团id
	NewbieFinishTime           int32 = 18   // 新手结束时间-秒级时间戳
	AgeAutoRefreshStartTime    int32 = 19   // 年龄自动刷新计时时间-秒级时间戳
	OfflineTime                int32 = 20   // 离线时间-秒级时间戳
	EnergyAutoRefreshStartTime int32 = 21   // 精力自动刷新计时时间-秒级时间戳
	OnlineState                int32 = 1001 // 我是字段说明
)

// Sample字段枚举 和 数据库&缓存字段名 的映射
var _CodeToFieldName = map[int32]string{
	Id:                         "id",
	Created:                    "create",
	Updated:                    "updated",
	Deleted:                    "deleted",
	Uid:                        "uid",
	Nickname:                   "nickname",
	Gender:                     "gender",
	Figure:                     "figure",
	CityId:                     "city_id",
	KingdomId:                  "kingdom_id",
	CityName:                   "city_name",
	KingdomName:                "kingdom_name",
	CityPosition:               "city_position",
	KingdomPosition:            "kingdom_position",
	CharacterSpaceId:           "character_space_id",
	TeamId:                     "team_id",
	LegionId:                   "legion_id",
	NewbieFinishTime:           "newbie_finish_time",
	AgeAutoRefreshStartTime:    "age_auto_refresh_start_time",
	OfflineTime:                "offline_time",
	EnergyAutoRefreshStartTime: "energy_auto_refresh_start_time",
	OnlineState:                "online_state",
}

// Sample数据库&缓存字段名 和 字段枚举 的映射
var _FieldNameToCode = map[string]int32{
	"id":                             Id,
	"create":                         Created,
	"updated":                        Updated,
	"deleted":                        Deleted,
	"uid":                            Uid,
	"nickname":                       Nickname,
	"gender":                         Gender,
	"figure":                         Figure,
	"city_id":                        CityId,
	"kingdom_id":                     KingdomId,
	"city_name":                      CityName,
	"kingdom_name":                   KingdomName,
	"city_position":                  CityPosition,
	"kingdom_position":               KingdomPosition,
	"character_space_id":             CharacterSpaceId,
	"team_id":                        TeamId,
	"legion_id":                      LegionId,
	"newbie_finish_time":             NewbieFinishTime,
	"age_auto_refresh_start_time":    AgeAutoRefreshStartTime,
	"offline_time":                   OfflineTime,
	"energy_auto_refresh_start_time": EnergyAutoRefreshStartTime,
	"online_state":                   OnlineState,
}

// Sample字段枚举 和 字段类型 的映射
var _FieldType = map[int32]string{
	Id:                         "int64",
	Created:                    "int64",
	Updated:                    "int64",
	Deleted:                    "int64",
	Uid:                        "int64",
	Nickname:                   "string",
	Gender:                     "uint8",
	Figure:                     "FigureType",
	CityId:                     "int64",
	KingdomId:                  "int64",
	CityName:                   "string",
	KingdomName:                "string",
	CityPosition:               "int32",
	KingdomPosition:            "int32",
	CharacterSpaceId:           "int64",
	TeamId:                     "int64",
	LegionId:                   "int64",
	NewbieFinishTime:           "int64",
	AgeAutoRefreshStartTime:    "int64",
	OfflineTime:                "int64",
	EnergyAutoRefreshStartTime: "int64",
	OnlineState:                "OnlineStateEnum",
}

// Sample字段枚举 和 是否可被修改 的映射
var _Modifiable = map[int32]bool{
	Id:                         false,
	Created:                    false,
	Updated:                    false,
	Deleted:                    false,
	Uid:                        false,
	Nickname:                   true,
	Gender:                     true,
	Figure:                     true,
	CityId:                     true,
	KingdomId:                  true,
	CityName:                   true,
	KingdomName:                true,
	CityPosition:               true,
	KingdomPosition:            true,
	CharacterSpaceId:           true,
	TeamId:                     true,
	LegionId:                   true,
	NewbieFinishTime:           true,
	AgeAutoRefreshStartTime:    true,
	OfflineTime:                true,
	EnergyAutoRefreshStartTime: true,
	OnlineState:                false,
}
