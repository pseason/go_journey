package model

import (
	"hm/pkg/misc"
	build_e "hm/pkg/services/data/model/enum/building_e"
)

// Author: L
// UpAuthor: byfengfeng
// Created: 2020/12/10
// Updated: 2021/2/3
// Describe: 建筑

type CityBuilding struct {
	misc.BaseModel
	CityId           int64                 `gorm:"type:bigint; not null; comment:城邦ID"`
	BuildingType     build_e.BuildingType  `gorm:"type:int; not null; default:0; comment:建筑类型"`
	Level            int32                 `gorm:"type:int; not null; default:0; comment:等级"`
	BuildingProgress int32                 `gorm:"type:int; not null; default:0; comment:下个等级已积攒建设度"`
	BuildingState    build_e.BuildingState `gorm:"type:int; not null; default:0;  comment:建筑状态"`
	PutResource      string                `gorm:"type:varchar(500); not null; default:'{}'; comment:投入资源"`
	BuildingTempId   int32				   `gorm:"type:int; not null; default:0; comment:等级"`
}

func GenDefaultBuilding(cityId int64, buildType build_e.BuildingType, lv,buildingTempId int32) (b *CityBuilding) {
	return &CityBuilding{
		CityId:       cityId,
		BuildingType: buildType,
		Level:        lv,
		PutResource:  "{}",
		BuildingTempId: buildingTempId,
	}
}
