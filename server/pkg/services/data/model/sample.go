package model

import (
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/sample_e"
	"hm/pkg/services/data/model/enum/sample_parts_e"
)

/*
@Time   : 2021-12-08 21:08
@Author : wushu
@DESC   : 生成枚举的示例
*/

// @Model(EnumCodeIota)
type Sample struct {
	misc.BaseModel
	Uid                        int64           `gorm:"type:bigint; not null; comment:玩家账号ID"`              //@Field(Unmodifiable)
	Nickname                   string          `gorm:"type:varchar(10); not null; default:''; comment:昵称"` // @Field(Comment=玩家昵称)
	Gender                     uint8           `gorm:"type:int; not null; default:0; comment:性别 0女 1男"`    // @Field(Comment=玩家性别 0女 1男)
	Figure                     FigureType      `gorm:"type:int; not null; default:0; comment:形象id"`
	CityId                     int64           `gorm:"type:bigint; not null; default:0; comment:城邦id"`
	KingdomId                  int64           `gorm:"type:bigint; not null; default:0; comment:王国id"`
	CityName                   string          `gorm:"type:varchar(10); not null; default:''; comment:城邦名"`
	KingdomName                string          `gorm:"type:varchar(10); not null; default:''; comment:王国名"`
	CityPosition               int32           `gorm:"type:int; not null; default:0; comment:城邦官职id"`
	KingdomPosition            int32           `gorm:"type:int; not null; default:0; comment:王国爵位id"`
	CharacterSpaceId           int64           `gorm:"type:bigint; not null; default:0; comment:角色所在空间id"`
	TeamId                     int64           `gorm:"type:bigint; not null; default:0; comment:队伍id"`
	LegionId                   int64           `gorm:"type:bigint; not null; default:0; comment:军团id"`
	NewbieFinishTime           int64           `gorm:"type:bigint; not null; default:0; comment:新手结束时间-秒级时间戳"`
	AgeAutoRefreshStartTime    int64           `gorm:"type:bigint; not null; default:0; comment:年龄自动刷新计时时间-秒级时间戳"`
	OfflineTime                int64           `gorm:"type:bigint; not null; default:0; comment:离线时间-秒级时间戳"`
	EnergyAutoRefreshStartTime int64           `gorm:"type:bigint; not null; default:0; comment:精力自动刷新计时时间-秒级时间戳"`
	OnlineState                OnlineStateEnum `gorm:"-"` // @Field(Code=1001;Comment=我是字段说明;Unmodifiable)
}

// @Model(EnumPrefix=Parts)
type SampleParts struct {
	misc.BaseModel                        // @Fields(Codes=1091,1092,1093,1094)
	Uid                        int64      `gorm:"type:bigint; not null; comment:玩家账号ID"`                      // @Field(Code=1000)
	Nickname                   string     `gorm:"type:varchar(10); not null; default:''; comment:昵称"`         // @Field(Code=1001)
	Gender                     uint8      `gorm:"type:int; not null; default:0; comment:性别 0女 1男"`            // @Field(Code=1002)
	Figure                     FigureType `gorm:"type:int; not null; default:0; comment:形象id"`                // @Field(Code=1003)
	CityId                     int64      `gorm:"type:bigint; not null; default:0; comment:城邦id"`             // @Field(Code=1004)
	KingdomId                  int64      `gorm:"type:bigint; not null; default:0; comment:王国id"`             // @Field(Code=1005)
	CityName                   string     `gorm:"type:varchar(10); not null; default:''; comment:城邦名"`        // @Field(Code=1006)
	KingdomName                string     `gorm:"type:varchar(10); not null; default:''; comment:王国名"`        // @Field(Code=1007)
	CityPosition               int32      `gorm:"type:int; not null; default:0; comment:城邦官职id"`              // @Field(Code=1008)
	KingdomPosition            int32      `gorm:"type:int; not null; default:0; comment:王国爵位id"`              // @Field(Code=1009)
	CharacterSpaceId           int64      `gorm:"type:bigint; not null; default:0; comment:角色所在空间id"`         // @Field(Code=1010)
	TeamId                     int64      `gorm:"type:bigint; not null; default:0; comment:队伍id"`             // @Field(Code=1011)
	LegionId                   int64      `gorm:"type:bigint; not null; default:0; comment:军团id"`             // @Field(Code=1012)
	NewbieFinishTime           int64      `gorm:"type:bigint; not null; default:0; comment:新手结束时间-秒级时间戳"`     // @Field(Code=1013)
	AgeAutoRefreshStartTime    int64      `gorm:"type:bigint; not null; default:0; comment:年龄自动刷新计时时间-秒级时间戳"` // @Field(Code=1014)
	OfflineTime                int64      `gorm:"type:bigint; not null; default:0; comment:离线时间-秒级时间戳"`       // @Field(Code=1015)
	EnergyAutoRefreshStartTime int64      `gorm:"type:bigint; not null; default:0; comment:精力自动刷新计时时间-秒级时间戳"` // @Field(Code=1016)
}

func (unnamed *Sample) GetByEnum(info int32) (res interface{}) {
	switch info {
	case sample_e.Id:
		res = unnamed.Id
	case sample_e.Created:
		res = unnamed.Created
	case sample_e.Updated:
		res = unnamed.Updated
	case sample_e.Deleted:
		res = unnamed.Deleted
	case sample_e.Uid:
		res = unnamed.Uid
	case sample_e.Nickname:
		res = unnamed.Nickname
	case sample_e.Gender:
		res = unnamed.Gender
	case sample_e.Figure:
		res = unnamed.Figure
	case sample_e.CityId:
		res = unnamed.CityId
	case sample_e.KingdomId:
		res = unnamed.KingdomId
	case sample_e.CityName:
		res = unnamed.CityName
	case sample_e.KingdomName:
		res = unnamed.KingdomName
	case sample_e.CityPosition:
		res = unnamed.CityPosition
	case sample_e.KingdomPosition:
		res = unnamed.KingdomPosition
	case sample_e.CharacterSpaceId:
		res = unnamed.CharacterSpaceId
	case sample_e.TeamId:
		res = unnamed.TeamId
	case sample_e.LegionId:
		res = unnamed.LegionId
	case sample_e.NewbieFinishTime:
		res = unnamed.NewbieFinishTime
	case sample_e.AgeAutoRefreshStartTime:
		res = unnamed.AgeAutoRefreshStartTime
	case sample_e.OfflineTime:
		res = unnamed.OfflineTime
	case sample_e.EnergyAutoRefreshStartTime:
		res = unnamed.EnergyAutoRefreshStartTime
	case sample_e.OnlineState:
		res = unnamed.OnlineState
	}
	return res
}

func (unnamed *SampleParts) GetByEnum(info int32) (res interface{}) {
	switch info {
	case sample_parts_e.Parts_Id:
		res = unnamed.Id
	case sample_parts_e.Parts_Created:
		res = unnamed.Created
	case sample_parts_e.Parts_Updated:
		res = unnamed.Updated
	case sample_parts_e.Parts_Deleted:
		res = unnamed.Deleted
	case sample_parts_e.Parts_Uid:
		res = unnamed.Uid
	case sample_parts_e.Parts_Nickname:
		res = unnamed.Nickname
	case sample_parts_e.Parts_Gender:
		res = unnamed.Gender
	case sample_parts_e.Parts_Figure:
		res = unnamed.Figure
	case sample_parts_e.Parts_CityId:
		res = unnamed.CityId
	case sample_parts_e.Parts_KingdomId:
		res = unnamed.KingdomId
	case sample_parts_e.Parts_CityName:
		res = unnamed.CityName
	case sample_parts_e.Parts_KingdomName:
		res = unnamed.KingdomName
	case sample_parts_e.Parts_CityPosition:
		res = unnamed.CityPosition
	case sample_parts_e.Parts_KingdomPosition:
		res = unnamed.KingdomPosition
	case sample_parts_e.Parts_CharacterSpaceId:
		res = unnamed.CharacterSpaceId
	case sample_parts_e.Parts_TeamId:
		res = unnamed.TeamId
	case sample_parts_e.Parts_LegionId:
		res = unnamed.LegionId
	case sample_parts_e.Parts_NewbieFinishTime:
		res = unnamed.NewbieFinishTime
	case sample_parts_e.Parts_AgeAutoRefreshStartTime:
		res = unnamed.AgeAutoRefreshStartTime
	case sample_parts_e.Parts_OfflineTime:
		res = unnamed.OfflineTime
	case sample_parts_e.Parts_EnergyAutoRefreshStartTime:
		res = unnamed.EnergyAutoRefreshStartTime
	}
	return res
}
