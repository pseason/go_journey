package model

import (
	"hm/pkg/misc"
)

type BackpackType int32

const (
	BackpackTypeAll BackpackType = iota
	BackpackTypeDefault
	BackpackTypeWarehouse
	BackpackTypeWear
	BackpackTypeWarehouseCombine   //背包和仓库
)

type Backpack struct {
	misc.BaseModel
	Cid       int64        `gorm:"type:bigint; not null; comment:玩家ID"`
	Type      BackpackType `gorm:"type:int; not null; DEFAULT:1; comment:类型 1-背包，2-仓库"`
	PropId    int32        `gorm:"type:int; not null; DEFAULT:0; comment:物品ID"`
	Num       int32        `gorm:"type:int; not null; DEFAULT:0; comment:数量"`
	Position  string       `gorm:"type:varchar(10); not null; DEFAULT:''; comment:装备位置"`
	Sn        int32        `gorm:"type:int; not null; DEFAULT:0; comment:格子序号"`
}

type BackpackNum struct {
	PropId int32
	Num    int32
}
