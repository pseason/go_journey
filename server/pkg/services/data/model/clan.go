package model

import (
	util "95eh.com/eg/utils"
	"hm/pkg/misc"
	"strconv"
	"strings"
)

type Clan struct {
	misc.BaseModel
	ClanName        string `gorm:"type:varchar(4); not null; default:''; comment:氏族名称"`
	ClanSurname     string `gorm:"type:varchar(3); not null; default:''; comment:氏族姓氏"`
	ClanLevel       int32  `gorm:"type:int; not null; default:0; comment:氏族等级"`
	ClanExperience  int32  `gorm:"type:int; not null; default:0; comment:氏族经验"`
	ClanDeclaration string `gorm:"type:varchar(500); not null; default:''; comment:氏族宣言"`
	ClanLeaderId    int64  `gorm:"type:bigint; not null; comment:拥有者ID"`
	ClanMember      string `gorm:"type:varchar(200); not null; default:''; comment:氏族成员id"`
	ClanPeopleNum   int32  `gorm:"type:int; not null; comment:当前氏族人数"`
}

func GenDefaultClan(clanName, clanSurname string, memberList []int64, cid int64) (c *Clan) {
	return &Clan{
		BaseModel:     misc.BaseModel{Id: util.GenSnowflakeGlobalNodeId()},
		ClanName:      clanName,
		ClanSurname:   clanSurname,
		ClanLeaderId:  cid,
		ClanMember:    GetTeamMemberString(memberList...),
		ClanPeopleNum: int32(len(memberList)),
	}
}

func (c *Clan) GetClanMemberList() (clanMembers []int64) {
	strList := strings.Split(strings.Trim(strings.Trim(c.ClanMember, "["), "]"), ",")
	for _, v := range strList {
		cid, _ := strconv.ParseInt(v, 10, 64)
		if cid > 0 {
			clanMembers = append(clanMembers, cid)
		}
	}
	return
}
