package model

import (
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/team_e"
)

type TeamRelationship struct {
	misc.BaseModel
	TeamId    int64                   `gorm:"type:bigint; not null; comment:队伍id"`
	Cid       int64                   `gorm:"type:bigint; not null; comment:玩家id"`
	ApplyType team_e.RelationshipType `gorm:"type:int; not null; default:1; comment:队伍申请关系类型"`
}

func GenRelationshipTeam(teamId, uid int64, applyType team_e.RelationshipType) (t *TeamRelationship) {
	t = GenDefaultRelationshipTeam(teamId, uid)
	t.ApplyType = applyType
	return t
}

func GenDefaultRelationshipTeam(teamId, cid int64,) (t *TeamRelationship) {
	return &TeamRelationship{
		TeamId: teamId,
		Cid:    cid,
	}
}
