package model

import (
	"hm/pkg/misc"
)

type ClanMember struct {
	misc.BaseModel
	Cid                    int64            `gorm:"type:bigint; not null; comment:拥有者ID"`
	ClanId                 int64            `gorm:"type:bigint; not null; comment:氏族ID"`
	SignInTime             int32            `gorm:"type:int; not null; default:0; comment:签到时间"`
}

func GenDefaultClanMember(cid, clanId int64) (c *ClanMember) {
	return &ClanMember{
		Cid:       cid,
		ClanId:    clanId,
	}
}
