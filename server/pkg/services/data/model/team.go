package model

import (
	util "95eh.com/eg/utils"
	"fmt"
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/team_e"
	"strconv"
	"strings"
)

type Team struct {
	misc.BaseModel
	TeamName      string           `gorm:"type:varchar(500); not null; default:''; comment:队伍名称"`
	CurrentNumber int32            `gorm:"type:int; not null; comment:当前队伍人数"`
	TeamType      team_e.TeamType  `gorm:"type:int; not null; comment:队伍类型"`
	TeamMember    string           `gorm:"type:varchar(200); not null; default:''; comment:队伍成员id"`
	CarryType     team_e.CarryType `gorm:"type:int; not null; default:1; comment:拾取类型"`
	MinAgeClaim   int32            `gorm:"type:int; not null; default:0; comment:最小年龄"`
	MaxAgeClaim   int32            `gorm:"type:int; not null; default:60; comment:最大年龄"`
	PowerClaim    int32            `gorm:"type:int; not null; default:0; comment:战力要求"`
	TeamAimsTemId int32            `gorm:"type:int; not null; default:0; comment:队伍目标模板id"`
	AutoAgree     bool             `gorm:"type:tinyint; not null; default:0; comment:是否自动同意加入队伍"`
	LeaderId      int64            `gorm:"type:bigint; not null; comment:拥有者ID"`
	CityId        int64            `gorm:"type:bigint; not null; comment:城邦ID"`
}

var TeamAfterTitle = "的队伍"

func GenDefaultTeam(nick string, cid,cityId int64) (m *Team) {
	return &Team{
		BaseModel:     misc.BaseModel{Id: util.GenSnowflakeGlobalNodeId()},
		TeamName:      nick + TeamAfterTitle,
		CurrentNumber: 1,
		TeamType:      team_e.OrdinaryTeam,
		CarryType:     team_e.FreeCollect,
		TeamMember:    GetTeamMemberString(cid),
		LeaderId:      cid,
		MaxAgeClaim:   60,
		TeamAimsTemId: 0,
		CityId: cityId,
	}
}

func (t *Team) UpTeamType(teamType team_e.TeamType) *Team {
	t.TeamType = teamType
	return t
}

func GetTeamMemberList(teamMember string) (teamMembers []int64) {
	strList := strings.Split(strings.Trim(strings.Trim(teamMember, "["), "]"), ",")
	for _, v := range strList {
		uid, _ := strconv.ParseInt(v, 10, 64)
		if uid > 0 {
			teamMembers = append(teamMembers, uid)
		}
	}
	return
}

func GetTeamMemberString(teamMemberIds ...int64) (teamMembers string) {
	teamMembers = "["
	for i, memberId := range teamMemberIds {
		if i > 0 {
			teamMembers += ","
		}
		teamMembers += fmt.Sprintf("%d", memberId)
	}
	teamMembers += "]"
	return
}
