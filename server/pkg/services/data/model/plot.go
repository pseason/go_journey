package model

import (
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/misc/utils"
)

type PlotType int32

const (
	PlotTypeFreshman PlotType = iota + 1
)

type PlotStatus int32

const (
	PlotStatusUndone PlotStatus = iota + 1
	PlotStatusCompleted
	PlotStatusLink
)

type PlotBtnType int32

const (
	PlotBtnTypeNone PlotBtnType = -1
)

const (
	PlotBtnTypeClose PlotBtnType = iota
	PlotBtnTypeAddTask
	PlotBtnTypeAddProp
	PlotBtnTypeAddSkill
	PlotBtnTypeSkipPlot
	PlotBtnTypeToPlot
)

type Plot struct {
	misc.BaseModel
	Cid        int64      `gorm:"type:bigint; not null; comment:玩家ID"`
	Type       PlotType   `gorm:"type:int; not null; comment:剧情类型"`
	TemplateId int32      `gorm:"type:int; not null; comment:模板ID"`
	InnerId    int64      `gorm:"type:bigint; not null; comment:内部ID"`
	GroupId    int32      `gorm:"type:int; not null; comment:当前剧情集ID"`
	Status     PlotStatus `gorm:"type:int; not null; comment:状态"`
	FightOpt   bool       `gorm:"type:int; not null; comment:是否有战斗选项"`
	SceneOpt   bool       `gorm:"type:int; not null; comment:是否有场景切换选项"`
}

type RowPlot Plot

func (p *Plot) ToMarshalModel() *RowPlot {
	return (*RowPlot)(p)
}

func (r *RowPlot) MarshalBinary() (data []byte, err error) {
	return utils.EntityMarshal(r)
}

func (r *RowPlot) UnmarshalBinary(data []byte) error {
	return utils.EntityUnmarshal(data, r)
}

func GeneratePlot(template *tsv.PlotTsv, cid, innerId int64) *Plot {
	return &Plot{
		Cid:        cid,
		Type:       PlotType(template.Type),
		TemplateId: template.Id,
		InnerId:    innerId,
		GroupId:    0,
		Status:     PlotStatusUndone,
		FightOpt:   false,
		SceneOpt:   false,
	}
}
