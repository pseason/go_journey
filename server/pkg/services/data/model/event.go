package model

import (
	"hm/pkg/misc"
	"hm/pkg/services/data/model/enum/event_e"
)

type Event struct {
	misc.BaseModel
	CityId        int64  `gorm:"type:bigint; not null; comment:据点ID"`
	KingdomId     int64  `gorm:"type:bigint; not null; comment:王国ID"`
	EventType     int32  `gorm:"type:int; not null; default:0; comment:事件类型"`
	OperationType int32  `gorm:"type:int; not null; default:0; comment:事件操作类型"`
	BuildType     int32  `gorm:"type:int; not null; default:0; comment:建筑记录类型"`
	Name          string `gorm:"type:varchar(20); not null; default:''; comment:操作玩家名称"`
	Context       string `gorm:"type:varchar(500); not null; default:''; comment:操作名称"`
}



func GenDefaultKingdomEvent(kingdomId int64, eventType event_e.KingdomEventType, kingdomType int32, context string) (e *Event) {
	return &Event{
		KingdomId:     kingdomId,
		EventType:     int32(eventType),
		OperationType: kingdomType,
		Context:       context,
	}
}

func GenDefaultCityEvent(cityId int64, eventType event_e.CityEventType, cityEvent int32, operationName, context string) (e *Event) {
	return &Event{
		CityId:        cityId,
		EventType:     int32(eventType),
		OperationType: cityEvent,
		Name:          operationName,
		Context:       context,
	}
}

func (e *Event) SetBuildType(buildType int32) *Event {
	e.BuildType = buildType
	return e
}
