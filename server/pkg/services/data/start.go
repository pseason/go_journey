package data

import (
	"95eh.com/eg/app"
	"95eh.com/eg/asset"
	"95eh.com/eg/codec"
	"95eh.com/eg/discovery"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/timer"
	"95eh.com/eg/tx"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	"hm/pkg/misc/tools"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/data/common"
	"hm/pkg/services/data/logic/backpack"
	"hm/pkg/services/data/logic/backpack_shortcut_bar"
	"hm/pkg/services/data/logic/bill"
	"hm/pkg/services/data/logic/character/attr"
	"hm/pkg/services/data/logic/character/info"
	"hm/pkg/services/data/logic/city"
	"hm/pkg/services/data/logic/city_auth"
	"hm/pkg/services/data/logic/city_building"
	"hm/pkg/services/data/logic/city_farm"
	"hm/pkg/services/data/logic/city_market"
	"hm/pkg/services/data/logic/city_mkwine"
	"hm/pkg/services/data/logic/clan"
	"hm/pkg/services/data/logic/event"
	"hm/pkg/services/data/logic/mail"
	"hm/pkg/services/data/logic/plot"
	"hm/pkg/services/data/logic/social"
	"hm/pkg/services/data/logic/task"
	"hm/pkg/services/data/logic/team"
	"hm/pkg/services/data/logic/testTx"
	"hm/pkg/services/data/logic/unlock"
	"hm/pkg/services/data/model/enum/model_e"
	"hm/pkg/services/data/msg_dt"
	"hm/pkg/services/data/msg_dt/err_dt"
	"math/rand"
	"reflect"
	"time"
)

// 定义本服务节点提供的服务和对应的逻辑实例
var _Svcs = map[uint16]common.IBaseService{
	svc.CharacterInfo:       info.NewCharacterInfoService(),
	svc.CharacterAttr:       attr.NewCharacterAttrService(),
	svc.Mail:                mail.NewMailService(),
	svc.City:                city.NewCityService(),
	svc.CityBuilding:        city_building.NewBuildingService(),
	svc.CityAuth:            city_auth.NewCityAuthService(),
	svc.CityFarm:            city_farm.NewFarmService(),
	svc.Backpack:            backpack.NewBackPackService(),
	svc.TestTry:             testTx.NewTxService(),
	svc.BackpackShortcutBar: backpack_shortcut_bar.NewShortcutBarService(),
	svc.CityMarket:          city_market.NewMarketService(),
	svc.CityMakeWine:        city_mkwine.NewMakeWineService(),
	svc.Unlock:              unlock.NewUnlockService(),
	svc.Team:                team.NewTeamService(),
	svc.Social:              social.NewSocialService(),
	svc.Event:               event.NewEventService(),
	svc.Bill:                bill.NewBillService(),
	svc.Clan:                clan.NewClanService(),
	svc.Task:                task.NewTaskService(),
	svc.Plot:                plot.NewPlotService(),
}

func Start() {
	// 设置随机种子
	rand.Seed(time.Now().UnixNano())
	// 加载路径
	utils.ExeDir()

	// 加载配置文件
	conf, err := common.LoadConf("",
		"common", "common_dev",
		"region", "region_dev",
		"region_services_data", "region_services_data_dev",
		"design")
	if err != nil {
		panic(err.Error())
	}
	err = utils.StartConsul(conf.Consul)
	if err != nil {
		panic(err.Error())
	}

	// 加载tsv
	tsv.LoadNodeTsv(misc.NodeServices_Data)

	// 初始化tools
	if err = tools.InitServicesUtils(); err != nil {
		panic(err.Error())
	}

	//serviceCodec := codec.NewJsonCodec()
	// 与服务通信编解码器使用msgpack
	serviceCodec := codec.NewMsgPackCodec()

	if conf.Debug {
		dur := (time.Minute * 30).Milliseconds()
		intfc.ResponseTimeoutDur = dur
		intfc.TcpDeadlineDur = dur
	}

	//启动Redis
	regionRedis := misc.GetRedisConn(conf.Redis)

	// 加载本节点的gorm和redis
	gormDb := common.LoadGorm(conf.MySQL)
	common.LoadRedis(conf.Redis)

	// 加载要注册的服务领域
	var _NodeServices []misc.TNode
	for _, nodeStr := range conf.DataServices {
		nodeSvc, err := misc.NameToTNode(nodeStr)
		if err != nil {
			panic(err)
		}
		_NodeServices = append(_NodeServices, nodeSvc)
	}
	if len(_NodeServices) == 0 {
		panic(errors.New("invalid services node"))
	}

	disConf := conf.Discovery
	app.Start(
		log.NewMLogger(log.Loggers(log.NewConsole(intfc.TLogDebug)), nil),
		asset.NewMAsset(),
		timer.NewMTimer(regionRedis, nil),
		tx.NewMTxConsumer(serviceCodec, regionRedis, nil),
		worker.NewMWorker(),
		discovery.NewMDiscovery(discovery.Nodes(_NodeServices...), disConf.NodeId, disConf.RegionId, serviceCodec, regionRedis,
			intfc.ModuleOptions(
				intfc.BeforeModuleStart(func() {
					// 注入协议号和消息结构体的映射
					msg_dt.InitServiceCodec(app.Discovery())
				}),
				intfc.BeforeModuleStart(func() {
					runningSvcs(gormDb)
				}),
				intfc.AfterModuleStart(func() {
					app.Discovery().WatchNodes(misc.NodeGame)
				}),
			),
			discovery.Addr(disConf.Ip, disConf.Port),
			discovery.Weight(disConf.Weight)),
	)
	utils.BeforeExit("stop services", app.Dispose)
}

var svcCodeToModel = map[svc.Svc][]model_e.Model{}
var modelToSvcCode = map[model_e.Model]svc.Svc{}

func runningSvcs(gormDb *gorm.DB) {
	imDiscovery := app.Discovery()
	txConsumer := app.TxConsumer()

	// SQL自动迁移
	log.Info("begin automatic migration", nil)
	for svcCode, service := range _Svcs {
		for _, model := range service.Models() {
			// ------temp------
			// 如果model是枚举 todo 待做：限制NewBaseService的参数类型为 model_e.Model
			if m, ok := model.(model_e.Model); ok {
				model = model_e.NewModelPtr(m)
				// 建立映射
				svcCodeToModel[svcCode] = append(svcCodeToModel[svcCode], m)
				modelToSvcCode[m] = svcCode
			}
			// ------temp------

			err := gormDb.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(model)
			if err != nil {
				panic(fmt.Sprintf("automatic migration: model:%v \nerror: %v\n", reflect.ValueOf(model).Type(), err))
			}
		}
	}
	log.Info("finish automatic migration", nil)

	// 向baseService注册自己
	for _, service := range _Svcs {
		service.SetIService(service)
	}

	// 绑定逻辑处理器
	for _, service := range _Svcs {
		// 绑定逻辑处理器
		actions := service.Actions()
		for msgCode, v := range actions {
			// subjectId为当前服务的主体id，一般是主体表的主键id。是其它节点发来请求时传入的subjectId参数
			// 框架层用subjectId作为workerId来控制多协程的任务分发，保证同一主体逻辑的同步执行
			// 例如：请求 svc.CharacterInfo、svc.Backpack，则id为角色id；请求 svc.City、svc.CityMarket，则为城邦id
			action := v
			imDiscovery.BindRequestHandler(msgCode, func(service uint16, tid, subjectId int64, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return action(common.NewActionCtx(tid, subjectId), reqBody)
			})
		}
		// 绑定事务逻辑处理器
		for msgCode, v := range service.TxActions() {
			handler := v
			txConsumer.BindTxHandler(msgCode, func(service uint16, tid, workerId int64, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
				return handler.Try(common.NewActionCtx(tid, workerId), reqBody)
			}, func(service uint16, tid, workerId int64, reqBody, tryResBody interface{}) (iError utils.IError) {
				return handler.Confirm(common.NewActionCtx(tid, workerId), reqBody, tryResBody)
			}, func(service uint16, tid, workerId int64, reqBody, tryResBody interface{}) (iError utils.IError) {
				return handler.Cancel(common.NewActionCtx(tid, workerId), reqBody, tryResBody)
			})
		}
	}

	// 绑定通用逻辑处理器
	for c, _ := range common.NewBaseService(nil).CommonActions() {
		msgCode := c
		imDiscovery.BindRequestHandler(msgCode, func(service uint16, tid, workerId int64, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			return _Svcs[service].CommonActions()[msgCode](common.NewActionCtx(tid, workerId), reqBody)
		})
	}

	// 绑定通用事务逻辑处理器
	for c, _ := range common.NewBaseService(nil).CommonTxActions() {
		msgCode := c
		txConsumer.BindTxHandler(msgCode, func(service uint16, tid, workerId int64, reqBody interface{}) (resBody interface{}, errCode err_dt.ErrCode) {
			return _Svcs[service].CommonTxActions()[msgCode].Try(common.NewActionCtx(tid, workerId), reqBody)
		}, func(service uint16, tid, workerId int64, reqBody, tryResBody interface{}) (iError utils.IError) {
			return _Svcs[service].CommonTxActions()[msgCode].Confirm(common.NewActionCtx(tid, workerId), reqBody, tryResBody)
		}, func(service uint16, tid, workerId int64, reqBody, tryResBody interface{}) (iError utils.IError) {
			return _Svcs[service].CommonTxActions()[msgCode].Cancel(common.NewActionCtx(tid, workerId), reqBody, tryResBody)
		})
	}

	// 初始化完成后的回调
	for svcCode, service := range _Svcs {
		err := service.AfterInit()
		if err != nil {
			panic(fmt.Sprintf("%s AfterInit() execution error: %v\n", svc.ServiceName(svcCode), err))
		}
	}
}
