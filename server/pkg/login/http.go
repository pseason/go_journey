package login

import (
	"95eh.com/eg/app"
	"github.com/gin-gonic/gin"
	"hm/pkg/login/common"
	"hm/pkg/login/msg_lg"
	"net/http"
)

func initHttp() {
	ut := app.UserHttp()
	ut.SetIdParser(func(c *gin.Context) (id, role int64, err error) {
		tkn := c.GetHeader("Token")
		if tkn == "" {
			return 0, 0, nil
		}
		_, claims, err := common.ParseJwt(tkn)
		if err != nil {
			return 0, 0, err
		}
		return claims.Uid, claims.Mask, nil
	})
	ut.BindProcessor(http.MethodPost, "user/signIn", msg_lg.CCdUserSignIn)
	ut.BindProcessor(http.MethodPost, "region/list", msg_lg.CCdRegionList)
	ut.BindProcessor(http.MethodPost, "region/gate", msg_lg.CCdRegionValidGameNode)
	ut.BindProcessor(http.MethodPost, "version/info", msg_lg.CCdVersionInfo)
}
