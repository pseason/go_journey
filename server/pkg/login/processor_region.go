package login

import (
	"95eh.com/eg/app"
	"hm/pkg/login/logic"
	"hm/pkg/login/msg_lg"
)

func initRegionProcessor() {
	dis := app.Discovery()
	dis.BindRequestProcessor(msg_lg.RCdAddGameNode, &logic.RAddGameNode{})
	dis.BindRequestProcessor(msg_lg.RCdDelGameNode, &logic.RDelGameNode{})

	dis.BindEventProcessor(msg_lg.RCdUpdateGameNodeCount, &logic.RUpdateGameNodeCount{})
	dis.BindEventProcessor(msg_lg.RCdUpdateGameNodeCap, &logic.TUpdateGameNodeCap{})
}
