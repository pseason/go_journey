package logic

import (
	"95eh.com/eg/app"
	"hm/pkg/login/msg_lg"
	"sync"
)

type Game struct {
	TcpAddr  string
	HttpAddr string
	Count    int
	Cap      int
}

var (
	_RegionGameNodes = make(map[uint16]map[uint16]*Game) // 所有区服的所有game节点 [区服id][game节点id]
	_RegionInfos     = map[uint16]*msg_lg.RegionInfo{}   // 区服信息
	_RegionList      = &msg_lg.ResRegionList{}           // 区服列表
	_RegionDataMtx   sync.RWMutex                        // 区服数据锁
)

// 更新区服列表数据
func updateRegionList(regionId uint16) {
	info, ok := _RegionInfos[regionId]
	if !ok {
		info = &msg_lg.RegionInfo{
			Id:   regionId,
			Name: app.Discovery().GetRegionName(regionId),
		}
		_RegionInfos[regionId] = info
	} else {
		info.Count = 0
		info.Cap = 0
	}
	for _, v := range _RegionGameNodes[regionId] {
		info.Count += v.Count
		info.Cap += v.Cap
	}
	regions := make([]*msg_lg.RegionInfo, 0, len(_RegionInfos))
	for _, info := range _RegionInfos {
		regions = append(regions, info)
	}
	_RegionList.Regions = regions
}

func OnRegionGameNodeRemoved(regionId, nodeId uint16) {
	_RegionDataMtx.Lock()
	gameNodes, ok := _RegionGameNodes[regionId]
	if ok {
		delete(gameNodes, nodeId)
	}
	updateRegionList(regionId)
	_RegionDataMtx.Unlock()
}
