package logic

import (
	"hm/pkg/login/msg_lg"
)

/*
@Time   : 2021-11-25 20:41
@Author : wushu
@DESC   :
*/
type TUpdateGameNodeCap struct {
}

func (u *TUpdateGameNodeCap) Trigger(service uint16, id int64, body interface{}) {
	req := body.(*msg_lg.EveRUpdateGameNodeCap)
	_RegionDataMtx.Lock()
	defer _RegionDataMtx.Unlock()

	gameNodes, ok := _RegionGameNodes[req.RegionId]
	if ok {
		gameNode, ok := gameNodes[req.NodeId]
		if ok {
			gameNode.Cap = req.Cap
		}
	}
}

type RUpdateGameNodeCount struct {
}

func (u *RUpdateGameNodeCount) Trigger(service uint16, id int64, body interface{}) {
	req := body.(*msg_lg.EveRUpdateGameNodeCount)
	_RegionDataMtx.Lock()
	defer _RegionDataMtx.Unlock()

	gameNodes, ok := _RegionGameNodes[req.RegionId]
	if ok {
		gameNode, ok := gameNodes[req.NodeId]
		if ok {
			gameNode.Count = req.Count
		}
	}
}
