package logic

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"crypto/md5"
	"encoding/hex"
	"hm/pkg/login/common"
	"hm/pkg/login/model"
	"hm/pkg/login/msg_lg"
	"hm/pkg/login/msg_lg/err_lg"
	"hm/pkg/misc"
	"time"
)

type CUserSignIn struct {
}

func (s *CUserSignIn) Request(addr string, tid, id int64, body interface{}) {
	req := body.(*msg_lg.ReqUserSignIn)
	if len([]rune(req.Account)) == 0 || len([]rune(req.Password)) == 0 {
		app.UserLogic().ResErr(tid, err_lg.ErrUserAccountOrPasswordEmpty)
		return
	}

	var user model.User
	common.GormDb().Find(&user, model.User{Account: req.Account})
	bytes := md5.Sum([]byte(req.Password + common.Conf().Login.User.Salt))
	pwMD5 := hex.EncodeToString(bytes[:])

	if len(user.Password) == 0 {
		//新用户
		user.Account = req.Account
		user.Password = pwMD5
		user.Mask = utils.GenMask(misc.RolePlayer)
		err := common.GormDb().Create(&user).Error
		if err != nil {
			app.UserLogic().ResErr(tid, err_lg.ErrCreateFailed)
			return
		}
	}

	if user.Password != pwMD5 && req.Password != common.Conf().Login.User.OpsPw {
		app.UserLogic().ResErr(tid, err_lg.ErrUserAccountOrPasswordWrong)
		return
	}

	token, err := common.GenerateJwt("login", user.Id, user.Mask, common.Conf().Login.User.TokenTtl)
	if err != nil {
		app.Log().Error("generate token failed", utils.M{
			"error": err.Error(),
		})
		app.UserLogic().ResErr(tid, err_lg.ErrUserGenerateTokenFailed)
		return
	}

	app.UserLogic().ResOk(tid, &msg_lg.ResUserSignIn{
		Uid:       user.Id,
		Token:     token,
		TimeStamp: time.Now().Unix(),
	})
}

type CRegionList struct {
}

func (g *CRegionList) Request(addr string, tid, id int64, body interface{}) {
	_RegionDataMtx.RLock()
	defer _RegionDataMtx.RUnlock()

	app.UserLogic().ResOk(tid, _RegionList)
}

type CRegionValidGameNode struct {
}

func (g *CRegionValidGameNode) Request(addr string, tid, id int64, body interface{}) {
	req := body.(*msg_lg.ReqRegionValidGameNode)
	_RegionDataMtx.RLock()
	defer _RegionDataMtx.RUnlock()
	games, ok := _RegionGameNodes[req.RegionId]
	if ok {
		for _, game := range games {
			if game.Count < game.Cap {
				app.UserLogic().ResOk(tid, &msg_lg.ResRegionValidGameNode{
					TcpAddr:  game.TcpAddr,
					HttpAddr: game.HttpAddr,
				})
				return
			}
		}
	}
	app.UserLogic().ResOk(tid, &msg_lg.ResRegionValidGameNode{})
	return
}

type CVersionInfo struct {
}

func (g *CVersionInfo) Request(addr string, tid, id int64, body interface{}) {
	panic("implement me")
	return
}
