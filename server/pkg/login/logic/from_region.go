package logic

import (
	"95eh.com/eg/utils"
	"hm/pkg/login/msg_lg"
)

type RAddGameNode struct {
}

func (r *RAddGameNode) Request(service uint16, tid, id int64, body interface{}) (interface{}, utils.TErrCode) {
	req := body.(*msg_lg.ReqRAddGameNode)
	_RegionDataMtx.Lock()
	gameNodes, ok := _RegionGameNodes[req.RegionId]
	if !ok {
		gameNodes = make(map[uint16]*Game)
		_RegionGameNodes[req.RegionId] = gameNodes
	}
	gameNodes[req.NodeId] = &Game{
		TcpAddr:  req.GateTcpAddr,
		Cap:      req.Cap,
		HttpAddr: req.HttpAddr,
	}
	updateRegionList(req.RegionId)
	_RegionDataMtx.Unlock()
	return nil, 0
}

type RDelGameNode struct {
}

func (d *RDelGameNode) Request(service uint16, tid, id int64, body interface{}) (interface{}, utils.TErrCode) {
	req := body.(*msg_lg.ReqRDelGameNode)
	OnRegionGameNodeRemoved(req.RegionId, req.NodeId)
	return nil, 0
}
