package model

import (
	"hm/pkg/misc"
)

type User struct {
	misc.BaseModel
	Account  string `gorm:"type:varchar(12); not null; unique; comment:账号"`
	Password string `gorm:"type:varchar(32); not null; comment:密码"`
	Mask     int64  `gorm:"type:bigint; not null; default:0; comment:角色权限"`
	Status   int    `gorm:"type:tinyint; not null; default:0; comment:状态(0正常 1封停)"`
	RealName string `gorm:"type:varchar(16); not null; default:''; comment:真实姓名"`
	IDNum    string `gorm:"type:varchar(18); not null; default:''; comment:身份证号"`
	PhoneNum string `gorm:"type:varchar(20); not null; default:''; comment:手机号"`
}
