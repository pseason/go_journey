package model

import (
	"hm/pkg/misc"
	"time"
)

/*
@Time   : 2021-11-25 20:43
@Author : wushu
@DESC   :
*/

type LogType int32

const (
	LogTLogin LogType = iota
)

type UserLogin struct {
	misc.BaseModel
	Uid           int64     `gorm:"type:bigint; not null; comment:玩家id"`
	ReadableTime  time.Time `gorm:"autoCreateTime:milli;type:datetime; not null; comment:可读的格式化时间"`
	Type          LogType   `gorm:"type:int; not null; default:0; comment:日志类型"`
	RemoteAddr    string    `gorm:"type:varchar(256); not null; default:''; comment:用户地址"`
	PublicIp      string    `gorm:"type:varchar(256); not null; default:''; comment:公网ip"`
	IntraIp       string    `gorm:"type:varchar(256); not null; default:''; comment:内网ip"`
	RequestHeader string    `gorm:"type:varchar(256); not null; default:''; comment:请求头"`
}
