package svc_lg

/**
定义login对外提供的服务
*/

type Svc = uint16

const scale = 100

// 对客户端提供的
const (
	C_User    Svc = (100 + iota) * scale // 用户信息
	C_Region                             // 区服信息
	C_Version                            // 版本信息
)

// 对服务端的其它节点提供的
const (
	S_Region Svc = (200 + iota) * scale // 区服信息
)

// 其它
const (
	Sys Svc = 650 * scale
)
