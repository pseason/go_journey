package common

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"github.com/go-redis/redis/v8"
	consulApi "github.com/hashicorp/consul/api"
	"hm/pkg/misc"
	"time"
)

var (
	_Conf = &Config{}
)

func Conf() *Config {
	return _Conf
}

func LoadConf(root string, paths ...string) (conf *Config, err error) {
	err = utils.StartLocalConf(root, _Conf, paths...)
	return _Conf, err
}

type Config struct {
	Debug     bool
	Discovery intfc.DiscoveryConf
	Consul    *consulApi.Config
	MySQL     misc.MySQL
	Redis     *redis.Options
	Login     LoginConf
	Http      misc.Http
	WhiteList []string
}

type LoginConf struct {
	User UserConf
}

type UserConf struct {
	Aes      string
	Salt     string
	OpsPw    string
	TokenTtl time.Duration
}
