package common

import (
	"95eh.com/eg/utils"
	"github.com/dgrijalva/jwt-go"
	"time"
)

var (
	_JwtKey []byte
)

type Claims struct {
	Uid  int64 `json:"uid"`
	Sid  int64 `json:"sid"`
	Mask int64 `json:"role"`
	jwt.StandardClaims
}

func SetJwtKey(key string) {
	_JwtKey = []byte(key)
}

func GenerateJwt(issuer string, uid, mask int64, validTime time.Duration) (str string, err error) {
	now := time.Now()
	expireTime := now.Add(validTime)
	claims := &Claims{
		Uid:  uid,
		Sid:  utils.GenSnowflakeGlobalNodeId(),
		Mask: mask,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
			IssuedAt:  now.Unix(),
			Issuer:    issuer,
			Subject:   "user token",
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	str, err = token.SignedString(_JwtKey)
	return
}

func ParseJwt(str string) (*jwt.Token, Claims, error) {
	var claim Claims
	tkn, err := jwt.ParseWithClaims(str, &claim, func(token *jwt.Token) (interface{}, error) {
		return _JwtKey, nil
	})
	return tkn, claim, err
}
