package msg_lg

import (
	"95eh.com/eg/intfc"
	"hm/pkg/login/svc_lg"
)

/**
与区服通信的消息号 RCd = region code
*/

// 消息号 region
const (
	// 添加game节点
	RCdAddGameNode MsgCode = uint32(svc_lg.S_Region)*10000 + iota
	// 移除game节点
	RCdDelGameNode
	// 更新game节点人数
	RCdUpdateGameNodeCount
	// 更新game节点容量
	RCdUpdateGameNodeCap
)

func InitServiceCodec(codec intfc.ICodec) {
	codec.BindFac(RCdAddGameNode,
		func() interface{} {
			return &ReqRAddGameNode{}
		},
		nil)
	codec.BindFac(RCdDelGameNode,
		func() interface{} {
			return &ReqRDelGameNode{}
		},
		nil)
	codec.BindFac(RCdUpdateGameNodeCount,
		nil,
		func() interface{} {
			return &EveRUpdateGameNodeCount{}
		})
	codec.BindFac(RCdUpdateGameNodeCap,
		nil,
		func() interface{} {
			return &EveRUpdateGameNodeCap{}
		})
}
