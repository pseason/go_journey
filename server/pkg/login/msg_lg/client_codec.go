package msg_lg

import (
	"95eh.com/eg/intfc"
	"hm/pkg/login/svc_lg"
)

/**
与客户端通信的消息号 CCd = ClientCode
*/

// 消息号 user
const (
	// 用户登录
	CCdUserSignIn MsgCode = uint32(svc_lg.C_User)*10000 + iota
)

// 消息号 region
const (
	// 区服列表
	CCdRegionList MsgCode = uint32(svc_lg.C_Region)*10000 + iota
	// 可用的区服地址(可用的game节点地址)
	CCdRegionValidGameNode
)

// 消息号 version
const (
	// 服务端的版本信息
	CCdVersionInfo MsgCode = uint32(svc_lg.C_Version)*10000 + iota
)

func InitClientCodec(codec intfc.ICodec) {
	// user
	codec.BindFac(CCdUserSignIn,
		func() interface{} {
			return &ReqUserSignIn{}
		}, func() interface{} {
			return &ResUserSignIn{}
		})
	// region
	codec.BindFac(CCdRegionList,
		nil,
		func() interface{} {
			return &ResRegionList{}
		})
	codec.BindFac(CCdRegionValidGameNode,
		func() interface{} {
			return &ReqRegionValidGameNode{}
		}, func() interface{} {
			return &ResRegionValidGameNode{}
		})
}
