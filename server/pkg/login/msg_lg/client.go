package msg_lg

/*
@Time   : 2021-11-25 20:44
@Author : wushu
@DESC   : 对客户端提供的消息
*/

// 请求登录
type ReqUserSignIn struct {
	Account  string `json:"account"`
	Password string `json:"password"`
}

// 响应登录
type ResUserSignIn struct {
	Uid       int64  `json:"uid"`
	Token     string `json:"token"`
	TimeStamp int64  `json:"timeStamp"`
}

// 区服信息
type RegionInfo struct {
	Id    uint16 `json:"id"`
	Name  string `json:"name"`
	Count int    `json:"count"`
	Cap   int    `json:"cap"`
}

// 返回区服列表
type ResRegionList struct {
	Regions []*RegionInfo `json:"regions"`
}

// 请求获取可用的区服地址(可用的game节点地址)
type ReqRegionValidGameNode struct {
	RegionId uint16
}

// 返回可用的地址(可用的game节点地址)
type ResRegionValidGameNode struct {
	TcpAddr  string `json:"tcp_addr"`
	HttpAddr string `json:"http_addr"`
}
