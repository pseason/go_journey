package msg_lg

/**
对服务端的其它服务提供的消息
*/

// 添加game节点
type ReqRAddGameNode struct {
	RegionId    uint16
	NodeId      uint16
	GateTcpAddr string // 供客户端连接的tcp地址
	HttpAddr    string
	Count       int // 人数
	Cap         int // 容量
}

// 移除game节点
type ReqRDelGameNode struct {
	RegionId uint16
	NodeId   uint16
}

// 更新game节点人数
type EveRUpdateGameNodeCount struct {
	RegionId uint16
	NodeId   uint16
	Count    int
}

// 更新game节点容量
type EveRUpdateGameNodeCap struct {
	RegionId uint16
	NodeId   uint16
	Cap      int
}
