package err_lg

import (
	"95eh.com/eg/utils"
	"hm/pkg/login/svc_lg"
)

/*
@Time   : 2021-11-16 11:43
@Author : wushu
@DESC   : 定义消息响应码(基本都是一些错误码)
	errCode在整个节点中是唯一的，以服务号作为前五位，后四位自增(但要注意iota不要超过10000)
*/

type ErrCode = utils.TErrCode

// 通用
const (
	// 查询失败
	ErrQueryFailed = ErrCode(uint32(svc_lg.Sys)*10000) + iota
	// 创建失败
	ErrCreateFailed
	// 修改失败
	ErrUpdateFailed
	// 删除失败
	ErrDelFailed
)

// client-user
const (
	ErrUserAccountOrPasswordEmpty = ErrCode(uint32(svc_lg.C_User)*10000) + iota
	ErrUserAccountOrPasswordWrong
	ErrUserGenerateTokenFailed
)
