package login

import (
	"95eh.com/eg/app"
	"hm/pkg/login/logic"
	"hm/pkg/login/msg_lg"
	"hm/pkg/misc"
)

func initClientProcessor() {
	userLogic := app.UserLogic()
	userLogic.BindRequestProcessor(misc.RoleGuest, msg_lg.CCdUserSignIn, &logic.CUserSignIn{})

	userLogic.BindRequestProcessor(misc.RolePlayer, msg_lg.CCdRegionList, &logic.CRegionList{})
	userLogic.BindRequestProcessor(misc.RolePlayer, msg_lg.CCdRegionValidGameNode, &logic.CRegionValidGameNode{})

	userLogic.BindRequestProcessor(misc.RolePlayer, msg_lg.CCdVersionInfo, &logic.CVersionInfo{})
}
