package login

import (
	"95eh.com/eg/app"
	"95eh.com/eg/asset"
	"95eh.com/eg/codec"
	"95eh.com/eg/discovery"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/timer"
	"95eh.com/eg/user"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
	"github.com/gin-gonic/gin"
	"hm/pkg/login/common"
	"hm/pkg/login/model"
	"hm/pkg/login/msg_lg"
	"hm/pkg/misc"
	"hm/pkg/misc/tools"
	"hm/pkg/misc/tsv"
	"math/rand"
	"net/http"
	"time"
)

func Start() {
	// 设置随机种子
	rand.Seed(time.Now().UnixNano())
	// 加载路径
	utils.ExeDir()

	// 加载配置文件
	conf, err := common.LoadConf("",
		"common", "common_dev",
		"login", "login_dev")
	if err != nil {
		panic(err.Error())
	}

	err = utils.StartConsul(conf.Consul)
	if err != nil {
		panic(err.Error())
	}

	// 加载tsv
	tsv.LoadNodeTsv(misc.NodeLogin)

	// 初始化tools
	if err = tools.InitLoginUtils(); err != nil {
		panic(err.Error())
	}

	//serviceCodec := codec.NewJsonCodec()
	// 与服务通信编解码器使用msgpack
	serviceCodec := codec.NewMsgPackCodec()

	// 注入协议号和消息结构体的映射
	msg_lg.InitServiceCodec(serviceCodec)

	//与客户端(用户)通信编解码器使用ProtoBuf
	userCodec := codec.NewPbCodec()
	//注入协议号和消息结构体的映射
	msg_lg.InitClientCodec(userCodec)

	//设置服务续约和响应超时时间
	//Debug模式时间为30分钟,否则5秒
	if conf.Debug {
		dur := (time.Minute * 30).Milliseconds()
		intfc.ResponseTimeoutDur = dur
		intfc.TcpDeadlineDur = dur
	}

	// 加载本节点的gorm
	gormDb := common.LoadGorm(conf.MySQL)
	err = gormDb.AutoMigrate(&model.User{})
	if err != nil {
		panic(err.Error())
	}

	redis := misc.GetRedisConn(conf.Redis)

	disConf := conf.Discovery
	//启动模块
	app.Start(
		log.NewMLogger(log.Loggers(log.NewConsole(intfc.TLogDebug)), nil),
		asset.NewMAsset(),
		timer.NewMTimer(redis, nil),
		user.NewMHttp(userCodec,
			func(c *gin.Context, code uint32, any interface{}) {
				c.JSON(http.StatusOK, utils.M{
					"Success": true,
					"Result":  any,
				})
			}, func(c *gin.Context, code uint32, ec utils.TErrCode) {
				c.JSON(http.StatusOK, utils.M{
					"Success": false,
					"Code":    ec, //ErrCode
				})
			}, user.HttpOptions(
				user.HttpListen(conf.Http.Port),
				user.HttpCert(conf.Http.Cert),
				user.HttpKey(conf.Http.Key),
			),
			intfc.BeforeModuleStart(initHttp)),
		user.NewMLogic(userCodec, intfc.BeforeModuleStart(initClientProcessor)),
		worker.NewMWorker(),
		discovery.NewMDiscovery(discovery.Nodes(misc.NodeLogin),
			disConf.NodeId, disConf.RegionId, serviceCodec, redis,
			intfc.ModuleOptions(intfc.BeforeModuleStart(initRegionProcessor)),
			discovery.Addr(disConf.Ip, disConf.Port),
			discovery.Weight(disConf.Weight),
			//discovery.ClientNodeClosed(nodeClosed), //todo,
		),
	)
	//程序退出前处理
	utils.BeforeExit("stop login", app.Dispose)
}
