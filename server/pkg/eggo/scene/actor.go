package scene

import (
	"95eh.com/eg/app"
	"95eh.com/eg/container"
	"95eh.com/eg/event"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"reflect"
)

type actor struct {
	id              int64
	typ             intfc.TActor
	scene           intfc.IScene
	currState       intfc.TActorState
	typeToComponent map[intfc.TActorComponent]intfc.IActorComponent
	tickerId        int64
	tickers         []intfc.ActionInt64Actor
	eventProcessors map[intfc.TActorEvent][]intfc.ActionActorEventToError
	event           intfc.IEventDispatcher
	tags            *container.StrList
}

func newActor(typ intfc.TActor, id int64) *actor {
	return &actor{
		id:              id,
		typ:             typ,
		typeToComponent: make(map[intfc.TActorComponent]intfc.IActorComponent),
		tickers:         make([]intfc.ActionInt64Actor, 0),
		eventProcessors: make(map[intfc.TActorEvent][]intfc.ActionActorEventToError),
		event:           event.NewEventDispatcher(),
		tags:            container.NewStrList(8),
	}
}

func (a *actor) Id() int64 {
	return a.id
}

func (a *actor) Type() intfc.TActor {
	return a.typ
}

func (a *actor) Scene() intfc.IScene {
	return a.scene
}

func (a *actor) SetScene(scene intfc.IScene) {
	a.scene = scene
}

func (a *actor) CurrState() intfc.TActorState {
	return a.currState
}

func (a *actor) SetState(state intfc.TActorState) {
	if a.currState != 0 {
		app.Scene().ActorExitState(a.currState, a)
	}
	a.currState = state
	app.Scene().ActorExitState(state, a)
}

func (a *actor) Event() intfc.IEventDispatcher {
	return a.event
}

func (a *actor) Tags() []string {
	return a.tags.Values()
}

func (a *actor) AddTag(tag string) {
	a.tags.Push(tag)
}

func (a *actor) RemoveTag(tag string) {
	a.tags.Remove(tag)
}

func (a *actor) AddComponent(component intfc.IActorComponent) utils.IError {
	t := component.Type()
	if _, ok := a.typeToComponent[t]; ok {
		return utils.NewError("exist actor component", utils.M{
			"type": t,
		})
	}
	a.typeToComponent[t] = component
	component.SetActor(a)
	return component.Start()
}

func (a *actor) RemoveComponent(t intfc.TActorComponent) utils.IError {
	component, ok := a.typeToComponent[t]
	if !ok {
		return utils.NewError(NotExistActorComponent, utils.M{
			"type": t,
		})
	}
	delete(a.typeToComponent, t)

	err := app.SceneCache().SaveActorComponent(a.id, component)
	if err != nil {
		err.AddParam("actor id", a.id)
		return err
	}
	return component.Dispose()
}

func (a *actor) GetComponent(t intfc.TActorComponent) (component intfc.IActorComponent, ok bool) {
	component, ok = a.typeToComponent[t]
	return
}

func (a *actor) AddTicker(ticker intfc.ActionInt64Actor) {
	if a.tickerId == 0 {
		a.tickerId = a.scene.AddTicker(a.Tick)
	}
	a.tickers = append(a.tickers, ticker)
}

func (a *actor) RemoveTicker(ticker intfc.ActionInt64Actor) {
	v := reflect.ValueOf(ticker)
	for i, t := range a.tickers {
		if reflect.ValueOf(t) == v {
			a.tickers = append(a.tickers[:i], a.tickers[i+1:]...)
			break
		}
	}
	if len(a.tickers) == 0 {
		a.scene.RemoveTicker(a.tickerId)
		a.tickerId = 0
	}
}

func (a *actor) Tick(ms int64) {
	for _, ticker := range a.tickers {
		ticker(ms, a)
	}
}

func (a *actor) BindEventProcessor(events ...intfc.TActorEvent) {
	for _, event := range events {
		ep, err := app.Scene().GetActorEventProcessor(a.scene.Type(), event)
		if err != nil {
			app.Log().Error(err.Error(), err.Params())
			continue
		}
		s, ok := a.eventProcessors[event]
		if ok {
			a.eventProcessors[event] = append(s, ep)
		} else {
			a.eventProcessors[event] = []intfc.ActionActorEventToError{ep}
		}
	}
}

func (a *actor) UnbindEventProcessor(events ...intfc.TActorEvent) {
	for _, event := range events {
		ep, err := app.Scene().GetActorEventProcessor(a.scene.Type(), event)
		if err != nil {
			app.Log().Error(err.Error(), err.Params())
			continue
		}
		s, ok := a.eventProcessors[event]
		if !ok {
			continue
		}
		v := reflect.ValueOf(ep)
		for i, t := range s {
			if reflect.ValueOf(t) == v {
				a.tickers = append(a.tickers[:i], a.tickers[i+1:]...)
				break
			}
		}
	}
}

func (a *actor) ProcessEvent(t intfc.TActorEvent, event interface{}) {
	s, ok := a.eventProcessors[t]
	if !ok {
		return
	}
	for _, p := range s {
		p(a, t, event)
	}
}

func (a *actor) Dispose() {
	if a.currState > 0 {
		app.Scene().ActorExitState(a.currState, a)
	}
	for _, component := range a.typeToComponent {
		component.Dispose()
	}
}
