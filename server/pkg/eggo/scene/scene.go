package scene

import (
	"95eh.com/eg/app"
	"95eh.com/eg/container"
	"95eh.com/eg/data"
	"95eh.com/eg/event"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
	"time"
)

func NewScene(id int64, t intfc.TScene) *scene {
	s := &scene{
		IData:           data.NewData(nil),
		id:              id,
		typ:             t,
		actionCh:        make(chan utils.Action, 1),
		event:           event.NewEventDispatcher(),
		typeToComponent: make(map[intfc.TSceneComponent]intfc.ISceneComponent),
		idToActor:       make(map[int64]intfc.IActor),
		typeToActorId:   make(map[intfc.TActor]*container.IdList),
		tagToActorId:    make(map[string]*container.IdList),
		tickers:         make([]*sceneTicker, 0, 32),
	}
	s.IWorker = worker.StartTickChanWorker(time.Millisecond*50, s.tick, func() {
		app.Log().Info("scene closed", utils.M{
			"scene id":   id,
			"scene type": t,
		})
	})
	return s
}

type scene struct {
	intfc.IWorker
	data.IData
	id              int64
	typ             intfc.TScene
	actionCh        chan utils.Action
	event           intfc.IEventDispatcher
	typeToComponent map[intfc.TSceneComponent]intfc.ISceneComponent
	idToActor       map[int64]intfc.IActor
	typeToActorId   map[intfc.TActor]*container.IdList
	currState       intfc.TSceneState
	tagToActorId    map[string]*container.IdList
	tickers         []*sceneTicker
}

func (s *scene) Id() int64 {
	return s.id
}

func (s *scene) Type() intfc.TScene {
	return s.typ
}

func (s *scene) Event() intfc.IEventDispatcher {
	return s.event
}

func (s *scene) AddComponent(component intfc.ISceneComponent) utils.IError {
	t := component.Type()
	if _, ok := s.typeToComponent[t]; ok {
		return utils.NewError("exist actor component", utils.M{
			"type": t,
		})
	}
	s.typeToComponent[t] = component
	component.SetScene(s)
	component.Start()
	return nil
}

func (s *scene) RemoveComponent(t intfc.TSceneComponent) utils.IError {
	component, ok := s.typeToComponent[t]
	if !ok {
		return utils.NewError(NotExistActorComponent, utils.M{
			"type": t,
		})
	}
	delete(s.typeToComponent, t)
	component.Stop()
	return nil
}

func (s *scene) GetComponent(t intfc.TSceneComponent) (component intfc.ISceneComponent, err utils.IError) {
	component, ok := s.typeToComponent[t]
	if !ok {
		return nil, utils.NewError(NotExistActorComponent, utils.M{
			"type": t,
		})
	}
	return component, nil
}

func (s *scene) AddActor(actorType intfc.TActor, actorId int64) (intfc.IActor, utils.IError) {
	actor := newActor(actorType, actorId)
	_, ok := s.idToActor[actorId]
	if ok {
		return nil, utils.NewError(ExistActor, utils.M{
			"actor id": actorId,
		})
	}
	s.idToActor[actorId] = actor
	list, ok := s.typeToActorId[actorType]
	if !ok {
		list = container.NewIdList(8)
		s.typeToActorId[actorType] = list
	}
	list.Push(actorId)
	actor.SetScene(s)
	app.Scene().ActorEnterScene(s, actor)
	return actor, nil
}

func (s *scene) RemoveActor(actorId int64) utils.IError {
	actor, ok := s.idToActor[actorId]
	if !ok {
		return utils.NewError(NotExistActor, utils.M{
			"actor id": actorId,
		})
	}
	app.Scene().ActorExitScene(s, actor)
	delete(s.idToActor, actorId)
	s.typeToActorId[actor.Type()].Remove(actorId)
	return nil
}

func (s *scene) IsEmpty() bool {
	return len(s.idToActor) == 0
}

func (s *scene) ChangeState(state intfc.TSceneState) {
	if s.currState > 0 {
		app.Scene().SceneExitState(s.currState, s)
	}
	s.currState = state
	app.Scene().SceneEnterState(state, s)
}

func (s *scene) CurrentState() intfc.TSceneState {
	return s.currState
}

func (s *scene) AddActorTags(actorId int64, tags ...string) utils.IError {
	actor, ok := s.idToActor[actorId]
	if !ok {
		return utils.NewError(NotExistActor, utils.M{
			"actor id": actorId,
		})
	}
	for _, tag := range tags {
		actor.AddTag(tag)
		list, ok := s.tagToActorId[tag]
		if !ok {
			list = container.NewIdList(8)
			s.tagToActorId[tag] = list
		}
		list.Push(actorId)
	}
	return nil
}

func (s *scene) RemoveActorTags(actorId int64, tags ...string) utils.IError {
	actor, ok := s.idToActor[actorId]
	if !ok {
		return utils.NewError(NotExistActor, utils.M{
			"actor id": actorId,
		})
	}
	for _, tag := range tags {
		actor.RemoveTag(tag)
		list, ok := s.tagToActorId[tag]
		if !ok {
			continue
		}
		list.Remove(actorId)
	}
	return nil
}

func (s *scene) AddTagActors(tag string, actorIds ...int64) {
	list, ok := s.tagToActorId[tag]
	if !ok {
		list = container.NewIdList(8)
		s.tagToActorId[tag] = list
	}
	for _, actorId := range actorIds {
		list.Push(actorId)
		actor, ok := s.idToActor[actorId]
		if !ok {
			app.Log().Warn(NotExistActor, utils.M{
				"actor id": actorId,
			})
			continue
		}
		actor.AddTag(tag)
	}
}

func (s *scene) RemoveTagActors(tag string, actorIds ...int64) {
	list, ok := s.tagToActorId[tag]
	if !ok {
		app.Log().Warn(NotExistActorTag, utils.M{
			"tag": tag,
		})
		return
	}
	for _, actorId := range actorIds {
		list.Remove(actorId)
		actor, ok := s.idToActor[actorId]
		if !ok {
			app.Log().Warn(NotExistActor, utils.M{
				"actor id": actorId,
			})
			continue
		}
		actor.RemoveTag(tag)
	}
}

func (s *scene) GetTagActors(tag string) ([]int64, utils.IError) {
	list, ok := s.tagToActorId[tag]
	if !ok {
		return nil, utils.NewError(NotExistActorTag, utils.M{
			"tag": tag,
		})
	}
	return list.Values(), nil
}

func (s *scene) ClearActorTags(actorId int64) utils.IError {
	actor, ok := s.idToActor[actorId]
	if !ok {
		return utils.NewError(NotExistActor, utils.M{
			"actor id": actorId,
		})
	}
	for _, tag := range actor.Tags() {
		list, ok := s.tagToActorId[tag]
		if !ok {
			continue
		}
		list.Remove(actorId)
	}
	return nil
}

func (s *scene) GetActor(actorId int64) (intfc.IActor, utils.IError) {
	actor, ok := s.idToActor[actorId]
	if !ok {
		return nil, utils.NewError(NotExistActor, utils.M{
			"actor id": actorId,
		})
	}
	return actor, nil
}

func (s *scene) ActionActor(actorId int64, action intfc.ActionActorToErr) utils.IError {
	actor, ok := s.idToActor[actorId]
	if !ok {
		return utils.NewError(NotExistActor, utils.M{
			"actor id": actorId,
		})
	}
	return action(actor)
}

func (s *scene) ActionActors(actorIds []int64, action intfc.ActionActor) utils.IError {
	var err utils.IError
	for _, actorId := range actorIds {
		actor, ok := s.idToActor[actorId]
		if !ok {
			err.UpdateParam("actor ids", func(any interface{}, ok bool) (interface{}, bool) {
				if any == nil {
					return []int64{actorId}, true
				}
				return append(any.([]int64), actorId), true
			})
		}
		action(actor)
	}
	return err
}

func (s *scene) ActionActorByTags(action intfc.ActionActor, tags ...string) {
	for _, tag := range tags {
		list, ok := s.tagToActorId[tag]
		if !ok {
			continue
		}
		list.Iter(func(actorId int64) {
			action(s.idToActor[actorId])
		})
	}
}

func (s *scene) ActionActorByTypes(action intfc.ActionActor, types ...intfc.TActor) {
	for _, t := range types {
		list, ok := s.typeToActorId[t]
		if !ok {
			continue
		}
		list.Iter(func(actorId int64) {
			action(s.idToActor[actorId])
		})
	}
}

func (s *scene) ActionAllActor(action intfc.ActionActor) {
	for _, actor := range s.idToActor {
		action(actor)
	}
}

func (s *scene) DispatchActorEvent(actorId int64, t intfc.TActorEvent, event interface{}) utils.IError {
	actor, ok := s.idToActor[actorId]
	if !ok {
		return utils.NewError(NotExistActor, utils.M{
			"actor id": actorId,
		})
	}
	actor.ProcessEvent(t, event)
	return nil
}

func (s *scene) DispatchActorsEvent(actorIds []int64, t intfc.TActorEvent, event interface{}) utils.IError {
	var err utils.IError
	for _, actorId := range actorIds {
		actor, ok := s.idToActor[actorId]
		if !ok {
			err.UpdateParam("actor ids", func(any interface{}, ok bool) (interface{}, bool) {
				if any == nil {
					return []int64{actorId}, true
				}
				return append(any.([]int64), actorId), true
			})
		}
		actor.ProcessEvent(t, event)
	}
	return err
}

func (s *scene) DispatchAllActorEvent(t intfc.TActorEvent, event interface{}) {
	for _, actor := range s.idToActor {
		actor.ProcessEvent(t, event)
	}
}

func (s *scene) DispatchActorEventByTags(t intfc.TActorEvent, event interface{}, tags ...string) {
	for _, tag := range tags {
		list, ok := s.tagToActorId[tag]
		if !ok {
			continue
		}
		list.Iter(func(actorId int64) {
			s.idToActor[actorId].ProcessEvent(t, event)
		})
	}
}

func (s *scene) DispatchActorEventByTypes(t intfc.TActorEvent, event interface{}, types ...intfc.TActor) {
	for _, at := range types {
		list, ok := s.typeToActorId[at]
		if !ok {
			continue
		}
		list.Iter(func(actorId int64) {
			s.idToActor[actorId].ProcessEvent(t, event)
		})
	}
}

func (s *scene) AddTicker(ticker utils.ActionInt64) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	s.tickers = append(s.tickers, &sceneTicker{
		id:     id,
		ticker: ticker,
	})
	return id
}

func (s *scene) RemoveTicker(tickId int64) {
	for i, t := range s.tickers {
		if t.id == tickId {
			s.tickers = append(s.tickers[:i], s.tickers[i+1:]...)
			break
		}
	}
}

func (s *scene) Dispose() {
	for _, component := range s.typeToComponent {
		component.Stop()
	}
	s.Stop()
}

func (s *scene) tick(now int64, delta int64) {
	for _, t := range s.tickers {
		t.ticker(delta)
	}
}

type sceneTicker struct {
	id     int64
	ticker utils.ActionInt64
}
