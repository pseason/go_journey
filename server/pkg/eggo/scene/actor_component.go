package scene

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
)

type ActorComponent struct {
	actor intfc.IActor
}

func (a *ActorComponent) Type() intfc.TActorComponent {
	panic("implement me")
}

func (a *ActorComponent) Start() utils.IError {
	return nil
}

func (a *ActorComponent) Dispose() utils.IError {
	return nil
}

func (a *ActorComponent) Actor() intfc.IActor {
	return a.actor
}

func (a *ActorComponent) SetActor(actor intfc.IActor) {
	a.actor = actor
}
