package scene

const (
	NotExistSceneType = "not exist scene type"
	ExistSceneType = "not exist scene type"
	NotExistScene = "not exist scene"
	ExistScene = "exist scene"
	NotExistSceneState = "not exist scene state"
	NotExistActorType = "not exist actor type"
	ExistActor = "exist actor"
	NotExistActor = "not exist actor"
	NotExistActorState = "not exist actor state"
	NotExistActorTag = "not exist actor tag"
	ExistActorComponent = "exist actor component"
	NotExistActorComponent = "not exist actor component"
	NotExistActorEventType = "not exist actor event type"
)