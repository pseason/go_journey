package scene

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"context"
	"github.com/go-redis/redis/v8"
	jsoniter "github.com/json-iterator/go"
	"strconv"
	"strings"
)

func NewSceneCache(redis *redis.Client, moduleOpts []intfc.ModuleOption) *mCache {
	return &mCache{
		IModule: intfc.NewModule(moduleOpts...),
		redis:   redis,
		jsonApi: jsoniter.Config{
			UseNumber:  true,
			EscapeHTML: true,
		}.Froze(),
	}
}

type mCache struct {
	intfc.IModule
	redis   *redis.Client
	jsonApi jsoniter.API
}

func (M *mCache) Type() intfc.TModule {
	return intfc.MSceneCache
}

func (M *mCache) SaveActor(actorId int64, actorType intfc.TActor, tags []string, components ...intfc.IActorComponent) utils.IError {
	kvs := make([]interface{}, 0, 4+len(components)*2)
	kvs = append(kvs, "type", uint16(actorType), "tags", utils.MergeStr(",", tags...))
	for _, component := range components {
		str, err := M.jsonApi.MarshalToString(component)
		if err != nil {
			return utils.NewError(err.Error(), nil)
		}
		kvs = append(kvs, component.Type().Name(), str)
	}
	err := M.redis.HSet(context.Background(), redisActorKey(actorId), kvs...).Err()
	if err != nil {
		return utils.NewError(err.Error(), nil)
	}
	return nil
}

func (M *mCache) LoadActor(actorId int64) (actorType intfc.TActor, tags []string, e utils.IError) {
	r, err := M.redis.HMGet(context.Background(), redisActorKey(actorId), "type", "tags").Result()
	if err != nil {
		e = utils.NewError(err.Error(), nil)
		return
	}
	t, err := strconv.ParseInt(r[0].(string), 10, 64)
	if err != nil {
		e = utils.NewError(err.Error(), nil)
		return
	}
	actorType = intfc.TActor(t)
	if r[1] == "" {
		return
	}
	tags = strings.Split(r[1].(string), ",")
	return
}

func (M *mCache) LoadActorComponent(actorId int64, c intfc.IActorComponent) utils.IError {
	r, err := M.redis.HGet(context.Background(), redisActorKey(actorId), c.Type().Name()).Result()
	if err != nil {
		return utils.NewError(err.Error(), utils.M{
			"actor id": actorId,
		})
	}
	err = M.jsonApi.UnmarshalFromString(r, c)
	if err != nil {
		return utils.NewError(err.Error(), utils.M{
			"actor id": actorId,
		})
	}
	return nil
}

func (M *mCache) SaveActorComponent(actorId int64, c intfc.IActorComponent) utils.IError {
	str, err := M.jsonApi.MarshalToString(c)
	if err != nil {
		return utils.NewError(err.Error(), utils.M{
			"actor id": actorId,
		})
	}
	_, err = M.redis.HSet(context.Background(), redisActorKey(actorId), c.Type().Name(), str).Result()
	if err != nil {
		return utils.NewError(err.Error(), utils.M{
			"actor id": actorId,
		})
	}
	return nil
}

func (M *mCache) DelActor(actorId int64) utils.IError {
	err := M.redis.Del(context.Background(), "actor:"+strconv.FormatInt(actorId, 10)).Err()
	if err != nil {
		return utils.NewError(err.Error(), nil)
	}
	return nil
}

func redisActorKey(actorId int64) string {
	return "actor:" + strconv.FormatInt(actorId, 10)
}
