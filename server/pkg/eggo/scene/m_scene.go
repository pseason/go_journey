package scene

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
)

func NewMScene(moduleOpts []intfc.ModuleOption) intfc.IMScene {
	return &mScene{
		IModule:               intfc.NewModule(moduleOpts...),
		IWorker:               worker.StartChanWorker(nil),
		sceneTypeToFac:        make(map[intfc.TScene]intfc.ActionSceneAnyToErr),
		idToScene:             make(map[intfc.TScene]map[int64]intfc.IScene),
		beforeActorEnterScene: make(map[intfc.TScene]intfc.ActionSceneActor),
		afterActorEnterScene:  make(map[intfc.TScene]intfc.ActionSceneActor),
		beforeActorExitScene:  make(map[intfc.TScene]intfc.ActionSceneActor),
		afterActorExitScene:   make(map[intfc.TScene]intfc.ActionSceneActor),
		sceneEnterState:       make(map[intfc.TSceneState]intfc.ActionSceneStateToErr),
		sceneExitState:        make(map[intfc.TSceneState]intfc.ActionSceneStateToErr),
		actorEnterState:       make(map[intfc.TActorState]intfc.ActionActorStateToErr),
		actorExitState:        make(map[intfc.TActorState]intfc.ActionActorStateToErr),
		actorEventProcessor:   make(map[intfc.TScene]map[intfc.TActorEvent]intfc.ActionActorEventToError),
	}
}

type mScene struct {
	intfc.IModule
	intfc.IWorker
	sceneTypeToFac        map[intfc.TScene]intfc.ActionSceneAnyToErr
	idToScene             map[intfc.TScene]map[int64]intfc.IScene
	beforeActorEnterScene map[intfc.TScene]intfc.ActionSceneActor
	afterActorEnterScene  map[intfc.TScene]intfc.ActionSceneActor
	beforeActorExitScene  map[intfc.TScene]intfc.ActionSceneActor
	afterActorExitScene   map[intfc.TScene]intfc.ActionSceneActor
	sceneEnterState       map[intfc.TSceneState]intfc.ActionSceneStateToErr
	sceneExitState        map[intfc.TSceneState]intfc.ActionSceneStateToErr
	actorEnterState       map[intfc.TActorState]intfc.ActionActorStateToErr
	actorExitState        map[intfc.TActorState]intfc.ActionActorStateToErr
	actorEventProcessor   map[intfc.TScene]map[intfc.TActorEvent]intfc.ActionActorEventToError
}

func (M *mScene) Type() intfc.TModule {
	return intfc.MScene
}

func (M *mScene) BindSceneFac(t intfc.TScene, fac intfc.ActionSceneAnyToErr) {
	M.sceneTypeToFac[t] = fac
}

func (M *mScene) CreateScene(t intfc.TScene, sceneId int64, data interface{}) utils.IError {
	m, ok := M.idToScene[t]
	if !ok {
		m = make(map[int64]intfc.IScene)
		M.idToScene[t] = m
	}
	_, ok = m[sceneId]
	if ok {
		return utils.NewError(ExistScene, utils.M{
			"scene sceneId": sceneId,
		})
	}
	fac, ok := M.sceneTypeToFac[t]
	if !ok {
		return utils.NewError(NotExistSceneType, utils.M{
			"scene type": t,
		})
	}
	scene := NewScene(sceneId, t)
	m[sceneId] = scene
	return fac(scene, data)
}

func (M *mScene) DisposeScene(t intfc.TScene, sceneId int64) utils.IError {
	m, ok := M.idToScene[t]
	if !ok {
		return utils.NewError(NotExistSceneType, utils.M{
			"scene type": t,
		})
	}
	scene, ok := m[sceneId]
	if !ok {
		return utils.NewError(NotExistScene, utils.M{
			"scene id": sceneId,
		})
	}
	delete(m, sceneId)
	scene.DoAction(scene.Dispose)
	return nil
}

func (M *mScene) GetScene(t intfc.TScene, sceneId int64) (intfc.IScene, utils.IError) {
	m, ok := M.idToScene[t]
	if !ok {
		return nil, utils.NewError(NotExistSceneType, utils.M{
			"scene type": t,
		})
	}
	scene, ok := m[sceneId]
	if !ok {
		return nil, utils.NewError(NotExistScene, utils.M{
			"scene id": sceneId,
		})
	}
	return scene, nil
}

func (M *mScene) BindActorEnterScene(sceneType intfc.TScene, before, after intfc.ActionSceneActor) {
	M.beforeActorEnterScene[sceneType] = before
	M.afterActorEnterScene[sceneType] = after
}

func (M *mScene) ActorEnterScene(scene intfc.IScene, actor intfc.IActor) {
	sceneType := scene.Type()
	before, ok := M.beforeActorEnterScene[sceneType]
	if ok {
		before(scene, actor)
	}
	after, ok := M.afterActorEnterScene[sceneType]
	if ok {
		after(scene, actor)
	}
}

func (M *mScene) BindActorExitScene(sceneType intfc.TScene, before, after intfc.ActionSceneActor) {
	M.beforeActorExitScene[sceneType] = before
	M.afterActorExitScene[sceneType] = after
}

func (M *mScene) ActorExitScene(scene intfc.IScene, actor intfc.IActor) {
	sceneType := scene.Type()
	before, ok := M.beforeActorExitScene[sceneType]
	if ok {
		before(scene, actor)
	}
	after, ok := M.afterActorExitScene[sceneType]
	if ok {
		after(scene, actor)
	}
}

func (M *mScene) BindSceneState(state intfc.TSceneState, enter, exit intfc.ActionSceneStateToErr) {
	M.sceneEnterState[state] = enter
	M.sceneExitState[state] = exit
}

func (M *mScene) SceneEnterState(state intfc.TSceneState, scene intfc.IScene) utils.IError {
	enter, ok := M.sceneEnterState[state]
	if !ok {
		return utils.NewError(NotExistSceneState, utils.M{
			"scene state": state,
		})
	}
	return enter(scene, state)
}

func (M *mScene) SceneExitState(state intfc.TSceneState, scene intfc.IScene) utils.IError {
	exit, ok := M.sceneExitState[state]
	if !ok {
		return utils.NewError(NotExistSceneState, utils.M{
			"scene state": state,
		})
	}
	return exit(scene, state)
}

func (M *mScene) BindActorState(state intfc.TActorState, enter, exit intfc.ActionActorStateToErr) {
	M.actorEnterState[state] = enter
	M.actorExitState[state] = exit
}

func (M *mScene) ActorEnterState(state intfc.TActorState, actor intfc.IActor) utils.IError {
	enter, ok := M.actorEnterState[state]
	if !ok {
		return utils.NewError(NotExistActorState, utils.M{
			"actor state": state,
		})
	}
	return enter(actor, state)
}

func (M *mScene) ActorExitState(state intfc.TActorState, actor intfc.IActor) utils.IError {
	exit, ok := M.actorExitState[state]
	if !ok {
		return utils.NewError(NotExistActorState, utils.M{
			"actor state": state,
		})
	}
	return exit(actor, state)
}

func (M *mScene) BindActorEventProcessor(evt intfc.TActorEvent, processor intfc.ActionActorEventToError, sceneTypes ...intfc.TScene) {
	for _, sceneType := range sceneTypes {
		m, ok := M.actorEventProcessor[sceneType]
		if !ok {
			m = make(map[intfc.TActorEvent]intfc.ActionActorEventToError)
			M.actorEventProcessor[sceneType] = m
		}
		m[evt] = processor
	}
}

func (M *mScene) GetActorEventProcessor(sceneType intfc.TScene, t intfc.TActorEvent) (intfc.ActionActorEventToError, utils.IError) {
	m, ok := M.actorEventProcessor[sceneType]
	if !ok {
		return nil, utils.NewError(NotExistSceneType, utils.M{
			"scene type": sceneType,
		})
	}
	p, ok := m[t]
	if !ok {
		return nil, utils.NewError(NotExistActorEventType, utils.M{
			"actor event type": t,
		})
	}
	return p, nil
}
