package scene

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
)

type SceneComponent struct {
	scene intfc.IScene
}

func (a *SceneComponent) Type() intfc.TSceneComponent {
	panic("implement me")
}

func (a *SceneComponent) Start() utils.IError {
	return nil
}

func (a *SceneComponent) Stop() utils.IError {
	return nil
}

func (a *SceneComponent) Scene() intfc.IScene {
	return a.scene
}

func (a *SceneComponent) SetScene(scene intfc.IScene) {
	a.scene = scene
}