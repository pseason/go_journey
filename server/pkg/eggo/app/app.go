package app

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	jsoniter "github.com/json-iterator/go"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/host"
	"runtime"
)

var (
	_Modules    = make(map[intfc.TModule]intfc.IModule)
	_Asset      intfc.IMAsset
	_Discovery  intfc.IMDiscovery
	_Event      intfc.IMEvent
	_Logger     intfc.IMLog
	_TxConsumer intfc.IMTxConsumer
	_TxProducer intfc.IMTxProducer
	_Scene      intfc.IMScene
	_SceneCache intfc.IMSceneCache
	_Timer      intfc.IMTimer
	_UserGate   intfc.IMUserGate
	_UserHttp   intfc.IMUserHttp
	_UserLogic  intfc.IMUserLogic
	_Worker     intfc.IMWorker
)

// RegisterModule 注入模块
func RegisterModule(module intfc.IModule) {
	_Modules[module.Type()] = module
}

// GetModule 获取模块
func GetModule(t intfc.TModule) intfc.IModule {
	return _Modules[t]
}

// Asset 资源模块
func Asset() intfc.IMAsset {
	return _Asset
}

// Discovery 服务发现模块
func Discovery() intfc.IMDiscovery {
	return _Discovery
}

// Event 事件模块
func Event() intfc.IMEvent {
	return _Event
}

// Log 日志模块
func Log() intfc.IMLog {
	return _Logger
}

// TxProducer 区服事务代理模块
func TxProducer() intfc.IMTxProducer {
	return _TxProducer
}

// TxConsumer  区服事务模块
func TxConsumer() intfc.IMTxConsumer {
	return _TxConsumer
}

// Scene 场景模块
func Scene() intfc.IMScene {
	return _Scene
}

// SceneCache 场景缓存
func SceneCache() intfc.IMSceneCache {
	return _SceneCache
}

// Timer 时间轮模块
func Timer() intfc.IMTimer {
	return _Timer
}

// UserGate 用户长连接网关
func UserGate() intfc.IMUserGate {
	return _UserGate
}

// UserHttp 用户Http服务
func UserHttp() intfc.IMUserHttp {
	return _UserHttp
}

// UserLogic 用户逻辑
func UserLogic() intfc.IMUserLogic {
	return _UserLogic
}

// Worker 多协程任务流
func Worker() intfc.IMWorker {
	return _Worker
}

func initModule(t intfc.TModule, action intfc.ActionModule) {
	m, ok := _Modules[t]
	if !ok {
		return
	}
	action(m)
}

func init() {
	cpuInfo, _ := cpu.Info()
	cpuStr, _ := jsoniter.MarshalToString(cpuInfo)
	hostInfo, _ := host.Info()
	log.Info("start easy game", utils.M{
		"email":    "eh95@qq.com",
		"blog":     "95eh.com",
		"go ver":   runtime.Version(),
		"app path": utils.ExeDir() + "/" + utils.ExeName(),
		"cpu":      cpuStr,
		"host":     hostInfo.String(),
	})
}

// Start 添加指定模块并启动框架
// @title
func Start(modules ...intfc.IModule) {
	for _, module := range modules {
		RegisterModule(module)
	}

	initModule(intfc.MAsset, func(module intfc.IModule) {
		_Asset = module.(intfc.IMAsset)
	})
	initModule(intfc.MDiscovery, func(module intfc.IModule) {
		_Discovery = module.(intfc.IMDiscovery)
	})
	initModule(intfc.MEvent, func(module intfc.IModule) {
		_Event = module.(intfc.IMEvent)
	})
	initModule(intfc.MLog, func(module intfc.IModule) {
		_Logger = module.(intfc.IMLog)
	})
	initModule(intfc.MTxConsumer, func(module intfc.IModule) {
		_TxConsumer = module.(intfc.IMTxConsumer)
	})
	initModule(intfc.MTxProducer, func(module intfc.IModule) {
		_TxProducer = module.(intfc.IMTxProducer)
	})
	initModule(intfc.MScene, func(module intfc.IModule) {
		_Scene = module.(intfc.IMScene)
	})
	initModule(intfc.MSceneCache, func(module intfc.IModule) {
		_SceneCache = module.(intfc.IMSceneCache)
	})
	initModule(intfc.MTimer, func(module intfc.IModule) {
		_Timer = module.(intfc.IMTimer)
	})
	initModule(intfc.MUserLogic, func(module intfc.IModule) {
		_UserLogic = module.(intfc.IMUserLogic)
	})
	initModule(intfc.MUserGate, func(module intfc.IModule) {
		_UserGate = module.(intfc.IMUserGate)
	})
	initModule(intfc.MUserHttp, func(module intfc.IModule) {
		_UserHttp = module.(intfc.IMUserHttp)
	})
	initModule(intfc.MWorker, func(module intfc.IModule) {
		_Worker = module.(intfc.IMWorker)
	})

	for _, module := range _Modules {
		module.Init()
	}
	for _, module := range _Modules {
		module.BeforeStart()
	}
	for _, module := range _Modules {
		module.Start()
	}
	for _, module := range _Modules {
		module.AfterStart()
	}
}

// Dispose 释放
func Dispose() {
	for _, module := range _Modules {
		module.BeforeDispose()
	}
	for _, module := range _Modules {
		module.Dispose()
	}
	for _, module := range _Modules {
		module.AfterDispose()
	}
}
