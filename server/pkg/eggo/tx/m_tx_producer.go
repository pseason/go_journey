package tx

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"context"
	"github.com/go-redis/redis/v8"
	"sort"
	"strconv"
)

const (
	_KeyTx      = "tx"
	_KeyTimeout = "tx_timeout"
	_OpConfirm  = "confirm"
	_OpCancel   = "cancel"
)

type (
	Status uint8
)

const (
	StatusNil Status = iota
	StatusTry
	StatusTrySuccess
	StatusTryFailed
	StatusConfirm
	StatusConfirmFailed
	StatusCancel
	StatusCancelFailed
)

type (
	ProducerOption func(option *txProducerOption)
	ActionTxFac    func(pid int64, client *redis.Client, confirm, cancel utils.Action) intfc.ITx
)

type txProducerOption struct {
	maxMsgLen int64
	txFac     ActionTxFac
}

func ProducerMaxMsgLen(l int64) ProducerOption {
	return func(option *txProducerOption) {
		option.maxMsgLen = l
	}
}

func ProducerTxFac(txFac ActionTxFac) ProducerOption {
	return func(option *txProducerOption) {
		option.txFac = txFac
	}
}

func NewMTxProducer(codec intfc.ICodec, redisClient *redis.Client, moduleOpts []intfc.ModuleOption,
	options ...ProducerOption) *mTxProducer {
	option := &txProducerOption{
		maxMsgLen: 1024 << 16,
		//maxMsgLen: 0,
		txFac: NewTx,
	}
	for _, o := range options {
		o(option)
	}
	m := &mTxProducer{
		IModule:     intfc.NewModule(moduleOpts...),
		option:      option,
		codec:       codec,
		redisClient: redisClient,
	}
	return m
}

type mTxProducer struct {
	intfc.IModule
	option      *txProducerOption
	codec       intfc.ICodec
	redisClient *redis.Client
}

func (M *mTxProducer) Type() intfc.TModule {
	return intfc.MTxProducer
}

func (M *mTxProducer) Start() {
	M.IModule.Start()
	M.bindTxTimeout()
}

func (M *mTxProducer) PushMsg(tid int64, code uint32, body interface{}) error {
	str, err := _NumberJsonConf.Marshal(utils.M{
		"tid":  tid,
		"code": code,
		"body": body,
	})
	if err != nil {
		return err
	}
	return M.redisClient.XAdd(context.Background(), &redis.XAddArgs{
		Stream: "msg",
		Values: str,
	}).Err()
}

type txMsg struct {
	//Tid  int64
	Id      int64
	Node    string
	Service uint16
	Code    uint32
	Body    interface{}
}

type (
	opTimeout struct {
		Pid int64
	}
)

func (M *mTxProducer) Tx(tid int64, action intfc.ActionTxToBool) {
	tx := M.option.txFac(tid, M.redisClient, func() {
		M.changeTxState(tid, _OpConfirm)
	}, func() {
		M.changeTxState(tid, _OpCancel)
	})
	if action(tx) {
		tx.Confirm()
	} else {
		tx.Cancel()
	}
}

func (M *mTxProducer) bindTxTimeout() {
	app.Timer().BindPersistentAction(_KeyTimeout, func() interface{} {
		return &opTimeout{}
	}, func(id int64, obj interface{}) {
		to := obj.(*opTimeout)
		M.changeTxState(to.Pid, _OpCancel)
	})
}

//改变事务状态,try->confirm/cancel
func (M *mTxProducer) changeTxState(tid int64, op string) {
	statusKey := getStatusHKey(tid)
	ctx := context.Background()
	r, err := M.redisClient.HGetAll(ctx, statusKey).Result()
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), utils.M{
			"status key": statusKey,
		}))
	}
	l := len(r)
	stidStrToNodeType := make(map[string]*redis.StringCmd, l)
	stids := make([]string, 0, l)
	sort.Strings(stids)
	_, err = M.redisClient.TxPipelined(ctx, func(pl redis.Pipeliner) error {
		for stidStr := range r {
			stids = append(stids, stidStr)
			stidStrToNodeType[stidStr] = pl.HGet(ctx, getRequestKeyByTidStr(stidStr), "Node")
		}
		_, err := pl.Exec(ctx)
		if err != nil {
			return err
		}
		for _, stidStr := range stids {
			stid, err := strToInt64(stidStr)
			if err != nil {
				log.Error("parse id failed", utils.M{
					"stid": stidStr,
				})
				continue
			}
			switch op {
			case _OpConfirm:
				pl.HSet(ctx, statusKey, stid, int(StatusConfirm))
			case _OpCancel:
				pl.HSet(ctx, statusKey, stid, int(StatusCancel))
			}
			nodeTypeStr := stidStrToNodeType[stidStr]
			//添加Confirm/Cancel消息队列
			pl.XAdd(ctx, &redis.XAddArgs{
				Stream: getTxStreamByNodeStr(_KeyTx, nodeTypeStr.Val()),
				Values: map[string]interface{}{
					"op":   op,
					"stid": stid,
					"tid":  tid,
				},
				MaxLen: M.option.maxMsgLen,
			})
		}
		return nil
	})
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), utils.M{
			"op": op,
		}))
		return
	}
	app.Log().TInfo(tid, "change state complete", nil)
}

func getRequestKey(tid int64) (dataKey string) {
	return getRequestKeyByTidStr(int64ToStr(tid))
}

func getRequestKeyByTidStr(tidStr string) (dataKey string) {
	return "tx:request:" + tidStr
}

func getResponseKey(tid int64) (dataKey string) {
	return getResponseKeyByTidStr(int64ToStr(tid))
}

func getResponseKeyByTidStr(tidStr string) (dataKey string) {
	return "tx:response:" + tidStr
}

func getStatusHKey(pid int64) string {
	return "tx:status:" + int64ToStr(pid)
}

func getTidToPidKey(tid int64) string {
	return "tx:tid:" + int64ToStr(tid)
}

func getTxStream(ch string, node intfc.TNode) string {
	return ch + ":" + node.String()
}

func getTxStreamByNodeStr(ch string, nodeStr string) string {
	return ch + ":" + nodeStr
}

func getMsgStream(ch string) string {
	return Msg + ":" + ch
}

func getXId(pid, tid int64) string {
	return int64ToStr(pid) + "-" + int64ToStr(tid)
}

func int64ToStr(v int64) string {
	return strconv.FormatInt(v, 10)
}

func strToInt64(s string) (v int64, err error) {
	return strconv.ParseInt(s, 10, 64)
}

func strToUint32(s string) (uint32, error) {
	v, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		return 0, err
	}
	return uint32(v), nil
}
