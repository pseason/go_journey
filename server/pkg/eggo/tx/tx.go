package tx

import (
	"95eh.com/eg/app"
	"95eh.com/eg/common"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"context"
	"github.com/go-redis/redis/v8"
	jsoniter "github.com/json-iterator/go"
)

func NewTx(tid int64, client *redis.Client, confirm, cancel utils.Action) intfc.ITx {
	return &Tx{
		tid:     tid,
		client:  client,
		confirm: confirm,
		cancel:  cancel,
	}
}

type Tx struct {
	tid    int64
	client *redis.Client
	confirm utils.Action
	cancel  utils.Action
}

func (t *Tx) Tid() int64 {
	return t.tid
}

var (
	_NumberJsonConf = jsoniter.Config{EscapeHTML: true, UseNumber: true}.Froze()
)

func (t *Tx) Try(service uint16, id int64, code uint32, body interface{}) (res interface{}, ec utils.TErrCode) {
	stid := app.Log().TSign(t.tid, utils.M{
		"code": code,
		"body": body,
	})

	bodyStr, err := _NumberJsonConf.MarshalToString(body)
	if err != nil {
		return nil, common.EcEncodeFailed
	}
	node, _ := app.Discovery().GetNodeByService(service)
	if err != nil {
		return nil, common.EcDecodeFailed
	}
	ctx := context.Background()
	_, err = t.client.TxPipelined(ctx, func(pl redis.Pipeliner) error {
		//设置请求的数据
		pl.HSet(ctx, getRequestKey(stid), map[string]interface{}{
			"Id":      id,
			"Node":    node.String(),
			"Service": service,
			"Code":    code,
			"Body":    bodyStr,
		})
		//设置事务状态
		pl.HSet(ctx, getStatusHKey(t.tid), stid, int(StatusTry))
		//设置tid的pid
		pl.Set(ctx, getTidToPidKey(stid), int64ToStr(t.tid), 0)
		return nil
	})
	if err != nil {
		app.Log().TError(stid, utils.NewError(err.Error(), nil))
		return nil, common.EcRedisErr
	}
	_, res, ec = app.Discovery().SyncRequestWithSTid(service, stid, id, code, body)
	if ec == 0 {
		app.Log().TInfo(stid, "try success", nil)
	} else {
		app.Log().TInfo(stid, "try fail", utils.M{
			"error code": ec,
		})
	}
	return
}

func (t *Tx) Confirm() {
	t.confirm()
}

func (t *Tx) Cancel() {
	t.cancel()
}
