package tx

import (
	"context"
	"strconv"
	"sync"
	"time"

	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"github.com/go-redis/redis/v8"
	"github.com/go-redsync/redsync/v4"
	"github.com/go-redsync/redsync/v4/redis/goredis/v8"
	"github.com/mitchellh/mapstructure"
	cmap "github.com/orcaman/concurrent-map"
)

func NewMTxConsumer(codec intfc.ICodec, redisClient *redis.Client, moduleOpts []intfc.ModuleOption) *mTxConsumer {
	return &mTxConsumer{
		IModule:          intfc.NewModule(moduleOpts...),
		codec:            codec,
		redisClient:      redisClient,
		redisLock:        redsync.New(goredis.NewPool(redisClient)),
		nameToRedisMutex: cmap.New(),
		codeToConfirm:    make(map[uint32]intfc.ActionUint16Int642Any2ToErr),
		codeToCancel:     make(map[uint32]intfc.ActionUint16Int642Any2ToErr),
		msgToCancel:      make(map[string]context.CancelFunc),
	}
}

const (
	Group = "tx"
	Msg   = "msg"
)

type mTxConsumer struct {
	intfc.IModule
	codec            intfc.ICodec
	redisClient      *redis.Client
	redisLock        *redsync.Redsync
	nameToRedisMutex cmap.ConcurrentMap
	codeToConfirm    map[uint32]intfc.ActionUint16Int642Any2ToErr
	codeToCancel     map[uint32]intfc.ActionUint16Int642Any2ToErr
	msgWatcherMtx    sync.Mutex
	msgToCancel      map[string]context.CancelFunc
}

func (M *mTxConsumer) Type() intfc.TModule {
	return intfc.MTxConsumer
}

func (M *mTxConsumer) Start() {
	M.IModule.Start()

	for _, nodeType := range app.Discovery().NodeTypes() {
		stream := getTxStream(_KeyTx, nodeType)
		M.watchStream(stream, func(xMsg redis.XMessage) {
			stid, err := strconv.ParseInt(xMsg.Values["stid"].(string), 10, 64)
			if err != nil {
				app.Log().Error("get stid failed", utils.M{
					"values": xMsg.Values,
				})
				return
			}
			tid, err := strconv.ParseInt(xMsg.Values["tid"].(string), 10, 64)
			if err != nil {
				app.Log().TError(stid, utils.NewError("get tid failed", utils.M{
					"values": xMsg.Values,
				}))
				return
			}
			op, ok := xMsg.Values["op"]
			if !ok {
				app.Log().TError(stid, utils.NewError("get op failed", utils.M{
					"values": xMsg.Values,
				}))
				return
			}
			ctx := context.Background()
			msgStr, err := M.redisClient.HGetAll(ctx, getRequestKey(stid)).Result()
			if err != nil {
				app.Log().TError(stid, utils.NewError(err.Error(), nil))
				return
			}
			var msg txMsg
			err = mapstructure.WeakDecode(msgStr, &msg)
			if err != nil {
				app.Log().TError(stid, utils.NewError(err.Error(), utils.M{
					"data": msgStr,
				}))
				return
			}
			reqBody, err := M.codec.SpawnReq(msg.Code)
			if err != nil {
				app.Log().TError(stid, utils.NewError(err.Error(), utils.M{
					"data": msgStr,
				}))
				return
			}
			err = _NumberJsonConf.UnmarshalFromString(msg.Body.(string), reqBody)
			if err != nil {
				app.Log().TError(stid, utils.NewError(err.Error(), utils.M{
					"data": msgStr,
				}))
				return
			}
			resStr, err := M.redisClient.Get(ctx, getResponseKey(stid)).Result()
			if err != nil {
				app.Log().TError(stid, utils.NewError(err.Error(), utils.M{
					"data": msgStr,
				}))
				return
			}
			resBody, err := M.codec.SpawnRes(msg.Code)
			if err != nil {
				app.Log().TError(stid, utils.NewError(err.Error(), utils.M{
					"data": msgStr,
				}))
				return
			}
			err = _NumberJsonConf.UnmarshalFromString(resStr, resBody)
			if err != nil {
				app.Log().TError(stid, utils.NewError(err.Error(), utils.M{
					"data": msgStr,
				}))
				return
			}
			//todo 幂等性
			switch op.(string) {
			case _OpConfirm:
				M.ProcessConfirm(stid, msg.Id, msg.Service, msg.Code, reqBody, resBody, func(ok bool) {
					if !ok {
						err := M.redisClient.HSet(ctx, getStatusHKey(tid), stid, int(StatusConfirmFailed)).Err()
						if err != nil {
							app.Log().TError(stid, utils.NewError(err.Error(), nil))
						}
						return
					}
					M.ackMsg(ctx, stream, tid, stid, xMsg)
				})
			case _OpCancel:
				M.ProcessCancel(stid, msg.Id, msg.Service, msg.Code, reqBody, resBody, func(ok bool) {
					if !ok {
						err := M.redisClient.HSet(ctx, getStatusHKey(tid), stid, int(StatusCancelFailed)).Err()
						if err != nil {
							app.Log().TError(stid, utils.NewError(err.Error(), nil))
						}
						return
					}
					M.ackMsg(ctx, stream, tid, stid, xMsg)
				})
			}
		})
	}
}

func (M *mTxConsumer) WatchMsg(ch string, code uint32, action utils.ActionInt64Uint32Any) {
	stream := getMsgStream(ch)
	cancel := M.watchStream(stream, func(msg redis.XMessage) {
		body, err := M.codec.UnmarshalReq(code, []byte(msg.Values["body"].(string)))
		if err != nil {
			app.Log().Error("unmarshal msg failed", utils.M{
				"error": err.Error(),
			})
			return
		}
		is, _ := utils.SplitInt64("-", msg.ID)
		tid := is[1]
		app.Log().TInfo(tid, "consumer receive message", nil)
		action(tid, code, body)
		err = M.redisClient.XAck(context.Background(), stream, Group, msg.ID).Err()
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
		}
	})
	M.msgWatcherMtx.Lock()
	M.msgToCancel[stream] = cancel
	M.msgWatcherMtx.Unlock()
}

func (M *mTxConsumer) UnwatchMsg(ch string, code uint32) {
	stream := ch + ":" + strconv.FormatInt(int64(code), 10)
	M.msgWatcherMtx.Lock()
	cancel, ok := M.msgToCancel[stream]
	if ok {
		cancel()
		delete(M.msgToCancel, stream)
	}
	M.msgWatcherMtx.Unlock()
}

func (M *mTxConsumer) watchStream(stream string, action func(msg redis.XMessage)) context.CancelFunc {
	bctx := context.Background()
	ctx, cancel := context.WithCancel(bctx)
	consumer := int64ToStr(int64(app.Discovery().NodeId()))

	go func() {
		result, err := M.redisClient.XAdd(bctx, &redis.XAddArgs{
			Stream: stream,
			Values: map[string]string{
				"name": "eg_test",
			},
		}).Result()
		if err == nil {
			M.redisClient.XDel(bctx, stream, result)
		}
		M.redisClient.XGroupCreate(bctx, stream, Group, "0-0")
		M.redisClient.XGroupCreateConsumer(bctx, stream, Group, consumer)

		time.Sleep(time.Second)
		for {
			result, err := M.redisClient.XReadGroup(ctx, &redis.XReadGroupArgs{
				Group:    Group,
				Consumer: consumer,
				Streams:  []string{stream, ">"},
				Block:    1000,
				NoAck:    false,
			}).Result()
			if err == context.Canceled {
				app.Log().Debug("stop watch msg", utils.M{
					"stream": stream,
				})
				break
			}

			for _, r := range result {
				for _, msg := range r.Messages {
					action(msg)
				}
			}
		}
	}()
	return cancel
}

func (M *mTxConsumer) BindTxProcessor(code uint32, processor intfc.ITxProcessor) {
	M.bindTxHandler(code, processor.Try, processor.Confirm, processor.Cancel)
}

func (M *mTxConsumer) BindTxHandler(code uint32, try intfc.ActionUint16Int642AnyToAnyErrCode,
	confirm, cancel intfc.ActionUint16Int642Any2ToErr) {
	M.bindTxHandler(code, try, confirm, cancel)
}

func (M *mTxConsumer) bindTxHandler(code uint32, try intfc.ActionUint16Int642AnyToAnyErrCode, confirm, cancel intfc.ActionUint16Int642Any2ToErr) {
	app.Discovery().BindRequestHandler(code, func(service uint16, tid, id int64, body interface{}) (res interface{}, ec utils.TErrCode) {
		ctx := context.Background()
		pidStr, err := M.redisClient.Get(ctx, getTidToPidKey(tid)).Result()
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
			return
		}
		pid, err := strToInt64(pidStr)
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
			return
		}
		statusKey := getStatusHKey(pid)
		res, ec = try(service, tid, id, body)
		if ec > 0 {
			app.Log().TInfo(tid, "try failed", utils.M{
				"error code": ec,
			})
			err := M.redisClient.HSet(ctx, statusKey, tid, int(StatusTryFailed)).Err()
			if err != nil {
				app.Log().TError(tid, utils.NewError(err.Error(), nil))
			}
			return
		}
		_, err = M.redisClient.TxPipelined(ctx, func(pl redis.Pipeliner) error {
			pl.HSet(ctx, statusKey, tid, int(StatusTrySuccess))
			rs, _ := _NumberJsonConf.MarshalToString(res)
			pl.Set(ctx, getResponseKey(tid), rs, 0)
			return nil
		})
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
			return
		}

		return
	})
	M.codeToConfirm[code] = confirm
	M.codeToCancel[code] = cancel
}

func (M *mTxConsumer) ProcessConfirm(tid, id int64, service uint16, code uint32, reqBody, resBody interface{}, res utils.ActionBool) {
	confirm, ok := M.codeToConfirm[code]
	if !ok {
		app.Log().TError(tid, utils.NewError("not exist code", nil))
		res(false)
		return
	}
	app.Worker().DoAction(id, func() {
		err := confirm(service, tid, id, reqBody, resBody)
		if err != nil {
			app.Log().TError(tid, err)
			res(false)
			return
		}
		app.Log().TInfo(tid, "confirm complete", nil)
		res(true)
	})
}

func (M *mTxConsumer) ProcessCancel(tid, id int64, service uint16, code uint32, reqBody, resBody interface{}, res utils.ActionBool) {
	cancel, ok := M.codeToCancel[code]
	if !ok {
		app.Log().TError(tid, utils.NewError("not exist code", nil))
		res(false)
		return
	}
	app.Worker().DoAction(id, func() {
		err := cancel(service, tid, id, reqBody, resBody)
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
			res(false)
			return
		}
		app.Log().TInfo(tid, "cancel complete", nil)
		res(true)
	})
}

func (M *mTxConsumer) ackMsg(ctx context.Context, serviceStream string, tid, stid int64, xMsg redis.XMessage) {
	_, err := M.redisClient.TxPipelined(ctx, func(pl redis.Pipeliner) error {
		pl.XAck(ctx, serviceStream, Group, xMsg.ID)
		pl.Del(ctx, getRequestKey(stid))
		pl.Del(ctx, getResponseKey(stid))
		pl.HDel(ctx, getStatusHKey(tid), int64ToStr(stid))
		pl.Del(ctx, getTidToPidKey(stid))
		return nil
	})
	if err != nil {
		app.Log().TError(stid, utils.NewError(err.Error(), nil))
		return
	}
	app.Log().TInfo(stid, "ack complete", nil)
}

//todo 服务锁
// 玩家锁
func (M *mTxConsumer) TxLock(pid int64, name string) error {
	//M.redisClient.HSetNX(context.Background(), int64ToStr(tid))
	//mutex := M.redisLock.NewMutex(name)
	//M.nameToRedisMutex.Set(name, mutex)
	//return mutex.Lock()
	panic("not implement")
}

func (M *mTxConsumer) TxUnlock(pid int64, name string) (bool, error) {
	//any, exists := M.nameToRedisMutex.Pop(name)
	//if !exists {
	//	return true, nil
	//}
	//return any.(*redsync.Mutex).Unlock()
	panic("not implement")
}

func (M *mTxConsumer) LockAction(pid int64, name string, action utils.Action) {
	//mutex := M.redisLock.NewMutex(name)
	//mutex.Lock()
	//action()
	//mutex.Unlock()
	panic("not implement")
}
