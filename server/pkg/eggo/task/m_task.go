package task

import (
	"github.com/95eh/eg"
	"github.com/95eh/eg/utils"
	"sync/atomic"
)

func NewMTask(moduleOpts []eg.ModuleOption) eg.IMTask {
	w := &mTask{
		IModule:       eg.NewModule(moduleOpts...),
		typeToTaskTpl: make(map[string]utils.ActionAnyToErr),
	}
	return w
}

type mTask struct {
	eg.IModule
	typeToTaskTpl map[string]utils.ActionAnyToErr
}

func (M *mTask) Type() string {
	return eg.MTask
}

func (M *mTask) BindTaskTpl(t string, action utils.ActionAnyToErr) {
	M.typeToTaskTpl[t] = action
}

func (M *mTask) DoTplTask(typ string, data interface{}) utils.IErr {
	task, ok := M.typeToTaskTpl[typ]
	if !ok {
		return utils.NewErr(utils.ErrNotExistType, utils.M{
			"type": typ,
		})
	}
	return task(data)
}

func (M *mTask) DoTplTasks(typ string, data ...interface{}) utils.IErr {
	task, ok := M.typeToTaskTpl[typ]
	if !ok {
		return utils.NewErr(utils.ErrNotExistType, utils.M{
			"type": typ,
		})
	}
	for _, d := range data {
		err := task(d)
		if err != nil {
			eg.Log().Error(err)
		}
	}
	return nil
}

func (M *mTask) WaitTasks(tasks ...utils.ToErr) {
	count := int32(len(tasks))
	ch := make(chan struct{})
	for _, task := range tasks {
		go func() {
			err := task()
			if err != nil {
				eg.Log().Error(err)
			}
			if atomic.AddInt32(&count, -1) == 0 {
				close(ch)
			}
		}()
	}
	<-ch
}

func (M *mTask) WaitTplTasks(typ string, data ...interface{}) {
	count := int32(len(data))
	ch := make(chan struct{})
	for _, data := range data {
		go func() {
			defer func() {
				if atomic.AddInt32(&count, -1) == 0 {
					close(ch)
				}
			}()
			task, ok := M.typeToTaskTpl[typ]
			if !ok {
				eg.Log().Error(utils.NewErr(utils.ErrNotExistType, utils.M{
					"type": typ,
				}))
				return
			}
			err := task(data)
			if err != nil {
				eg.Log().Error(err)
			}
		}()
	}
	<-ch
}

func (M *mTask) AsyncTasks(complete utils.Action, tasks ...utils.ToErr) {
	count := int32(len(tasks))
	for _, task := range tasks {
		go func() {
			err := task()
			if err != nil {
				eg.Log().Error(err)
			}
			if atomic.AddInt32(&count, -1) == 0 {
				complete.Invoke()
			}
		}()
	}
}

func (M *mTask) AsyncTplTasks(typ string, complete utils.Action, data ...interface{}) {
	count := int32(len(data))
	for _, data := range data {
		go func() {
			defer func() {
				if atomic.AddInt32(&count, -1) == 0 {
					complete.Invoke()
				}
			}()
			task, ok := M.typeToTaskTpl[typ]
			if !ok {
				eg.Log().Error(utils.NewErr(utils.ErrNotExistType, utils.M{
					"type": typ,
				}))
				return
			}
			err := task(data)
			if err != nil {
				eg.Log().Error(err)
			}
		}()
	}
}

func (M *mTask) WaitMultiTplTask(m map[string]interface{}) {
	count := int32(len(m))
	ch := make(chan struct{})
	for typ, data := range m {
		go func() {
			defer func() {
				if atomic.AddInt32(&count, -1) == 0 {
					close(ch)
				}
			}()
			task, ok := M.typeToTaskTpl[typ]
			if !ok {
				eg.Log().Error(utils.NewErr(utils.ErrNotExistType, utils.M{
					"type": typ,
				}))
				return
			}
			err := task(data)
			if err != nil {
				eg.Log().Error(err)
			}
		}()
	}
	<-ch
}

func (M *mTask) AsyncMultiTplTask(m map[string]interface{}, complete utils.Action) {
	count := int32(len(m))
	for typ, data := range m {
		go func() {
			defer func() {
				if atomic.AddInt32(&count, -1) == 0 {
					complete.Invoke()
				}
			}()
			task, ok := M.typeToTaskTpl[typ]
			if !ok {
				eg.Log().Error(utils.NewErr(utils.ErrNotExistType, utils.M{
					"type": typ,
				}))
				return
			}
			err := task(data)
			if err != nil {
				eg.Log().Error(err)
			}
		}()
	}
}

func (M *mTask) WaitMultiTplTasks(m map[string][]interface{}) {
	count := int32(len(m))
	ch := make(chan struct{})
	for typ, data := range m {
		for _, d := range data {
			go func() {
				defer func() {
					if atomic.AddInt32(&count, -1) == 0 {
						close(ch)
					}
				}()
				task, ok := M.typeToTaskTpl[typ]
				if !ok {
					eg.Log().Error(utils.NewErr(utils.ErrNotExistType, utils.M{
						"type": typ,
					}))
					return
				}
				err := task(d)
				if err != nil {
					eg.Log().Error(err)
				}
			}()
		}
	}
	<-ch
}

func (M *mTask) AsyncMultiTplTasks(m map[string][]interface{}, complete utils.Action) {
	count := int32(len(m))
	for typ, data := range m {
		for _, d := range data {
			go func() {
				defer func() {
					if atomic.AddInt32(&count, -1) == 0 {
						complete.Invoke()
					}
				}()
				task, ok := M.typeToTaskTpl[typ]
				if !ok {
					eg.Log().Error(utils.NewErr(utils.ErrNotExistType, utils.M{
						"type": typ,
					}))
					return
				}
				err := task(d)
				if err != nil {
					eg.Log().Error(err)
				}
			}()
		}
	}
}
