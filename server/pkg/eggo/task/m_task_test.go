package task

import (
	"fmt"
	"github.com/95eh/eg"
	"github.com/95eh/eg/utils"
	"math"
	"math/rand"
	"testing"
	"time"
)

type job struct {
	A      float64
	B      float64
	Result float64
}

func f(a, b float64) float64 {
	r := float64(0)
	for i := 0; i < 5; i++ {
		j := float64(i)
		r += math.Sqrt(math.Pow(a*j, 2)+math.Pow(b *j, 2))
	}
	return r
}

func initWorker() eg.IMTask {
	worker := NewMTask(nil)
	s := time.Now()
	r := f(100, 200)
	dur := time.Now().Sub(s).Nanoseconds()
	fmt.Println(dur, r)
	worker.BindTaskTpl("test", func(o interface{}) utils.Action {
		add := o.(*job)
		return func() {
			add.Result = f(add.A, add.B)
		}
	})
	return worker
}

func TestJob(t *testing.T) {
	worker := initWorker()
	worker.DoTplTask("test", &job{
		A: 10,
		B: 20,
	})
}

func BenchmarkJob(b *testing.B) {
	worker := initWorker()
	count := 500000
	jobs := make([]interface{}, count)
	for i := 0; i < count; i++ {
		jobs[i] = &job{
			A: rand.Float64(),
			B: rand.Float64(),
		}
	}
	b.Run("sync", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			worker.DoTplTasks("test", jobs...)
		}
		b.ReportAllocs()
	})
	b.Run("wait", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			worker.WaitTplTasks("test", jobs...)
		}
		b.ReportAllocs()
	})
	b.Run("async", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ch := make(chan struct{})
			worker.AsyncTplTasks("test", func() {
				close(ch)
			}, jobs...)
			<-ch
		}
		b.ReportAllocs()
	})
}
