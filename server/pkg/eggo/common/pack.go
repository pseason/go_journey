package common

import (
	"95eh.com/eg/data"
	"95eh.com/eg/utils"
)

const (
	HdRequest uint8 = iota + 1
	HdResponseOk
	HdResponseErr
	HdEvent
)

func PackRequest(header uint8, service uint16, tid, id int64, code uint32, pkt []byte) []byte {
	buffer := data.NewBytesBufferWithLen(uint32(25 + len(pkt)))
	buffer.WUint8(header)
	buffer.WUint16(service)
	buffer.WInt64(tid)
	buffer.WInt64(id)
	buffer.WUint32(code)
	buffer.Write(pkt)
	return buffer.WAll()
}

func UnpackRequest(bytes []byte) (service uint16, tid, id int64, code uint32, pkt []byte, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	buffer.SetPos(1)
	service, err = buffer.RUint16()
	if err != nil {
		return
	}
	tid, err = buffer.RInt64()
	if err != nil {
		return
	}
	id, err = buffer.RInt64()
	if err != nil {
		return
	}
	code, err = buffer.RUint32()
	if err != nil {
		return
	}
	pkt = buffer.RAll()
	return
}

func PackResponseOk(tid int64, code uint32, pkt []byte) []byte {
	buffer := data.NewBytesBufferWithLen(uint32(13 + len(pkt)))
	buffer.WUint8(HdResponseOk)
	buffer.WInt64(tid)
	buffer.WUint32(code)
	buffer.Write(pkt)
	return buffer.WAll()
}

func UnpackResponseOk(bytes []byte) (tid int64, code uint32, pkt []byte, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	buffer.SetPos(1)
	tid, err = buffer.RInt64()
	if err != nil {
		return
	}
	code, err = buffer.RUint32()
	if err != nil {
		return
	}
	if buffer.Available() > 0 {
		pkt = buffer.RAll()
	}
	return
}

func PackResponseError(tid int64, ec utils.TErrCode) []byte {
	buffer := data.NewBytesBufferWithLen(13)
	buffer.WUint8(HdResponseErr)
	buffer.WInt64(tid)
	buffer.WUint32(uint32(ec))
	return buffer.WAll()
}

func UnpackResponseError(bytes []byte) (tid int64, ec utils.TErrCode, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	buffer.SetPos(1)
	tid, err = buffer.RInt64()
	if err != nil {
		return
	}
	var ecv uint32
	ecv, err = buffer.RUint32()
	if err != nil {
		return
	}
	ec = utils.TErrCode(ecv)
	return
}

func PackEvent(service uint16, tid, id int64, code uint32, pkt []byte) []byte {
	buffer := data.NewBytesBufferWithLen(uint32(19 + len(pkt)))
	buffer.WUint8(HdEvent)
	buffer.WUint16(service)
	buffer.WInt64(tid)
	buffer.WInt64(id)
	buffer.WUint32(code)
	buffer.Write(pkt)
	return buffer.WAll()
}

func UnpackEvent(bytes []byte) (service uint16, tid, id int64, code uint32, pkt []byte, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	buffer.SetPos(1)
	service, err = buffer.RUint16()
	if err != nil {
		return
	}
	tid, err = buffer.RInt64()
	if err != nil {
		return
	}
	id, err = buffer.RInt64()
	if err != nil {
		return
	}
	code, err = buffer.RUint32()
	if err != nil {
		return
	}
	pkt = buffer.RAll()
	return
}
