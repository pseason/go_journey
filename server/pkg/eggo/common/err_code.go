package common

import "95eh.com/eg/utils"

const (
	EcNil utils.TErrCode = iota
	EcNotExistNode
	EcNotImplement
	EcNoAuth
	EcRedisErr
	EcBadPacket
	EcEmptyPacket
	EcBadHead
	EcDecodeFailed
	EcEncodeFailed
	EcUnpackFailed
	EcTimeout
	EcSendPacketFailed
	EcEnterSpaceFailed
	EcExitSpaceFailed
	EcEnterSceneFailed
	EcExitSceneFailed
	EcSceneChangePosFailed
	EcMax
)
