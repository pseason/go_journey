package common

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
)

func NewResponse() intfc.IResponse {
	return &response{}
}

type response struct {
	resOk   utils.ActionAny
	resErr  utils.ActionErrCode
	timerId int64
}

func (r *response) Init(resOk utils.ActionAny, resErr utils.ActionErrCode, timeout utils.Action) {
	r.resOk = resOk
	r.resErr = resErr
	r.timerId = app.Timer().After(intfc.ResponseTimeoutDur, timeout)
}

func (r *response) ResOk(res interface{}) {
	app.Timer().Remove(r.timerId)
	r.resOk(res)
}

func (r *response) ResErr(err utils.TErrCode) {
	app.Timer().Remove(r.timerId)
	r.resErr(err)
}
