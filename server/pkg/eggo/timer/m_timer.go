package timer

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"context"
	"errors"
	tw "github.com/RussellLuo/timingwheel"
	"github.com/go-redis/redis/v8"
	jsoniter "github.com/json-iterator/go"
	"github.com/mitchellh/mapstructure"
	cmap "github.com/orcaman/concurrent-map"
	"github.com/robfig/cron/v3"
	"strconv"
	"time"
)

const (
	_AfterKey = "timer:after"
	_CronKey  = "timer:cron"
)

var (
	_CronParser      = cron.NewParser(cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow | cron.Descriptor)
	_ExistCronTagErr = errors.New("exist cron tag")
)

type Option func(option *timerOption)

type timerOption struct {
	slotCount, slotInterval int64
}

func SlotCount(slotCount int64) Option {
	return func(option *timerOption) {
		option.slotCount = slotCount
	}
}

func SlotInterval(slotInterval int64) Option {
	return func(option *timerOption) {
		option.slotInterval = slotInterval
	}
}

func Options(opts ...Option) []Option {
	return opts
}

func NewMTimer(redisClient *redis.Client, opts []Option, moduleOpts ...intfc.ModuleOption) intfc.IMTimer {
	option := &timerOption{
		slotCount:    10,
		slotInterval: 10,
	}
	for _, opt := range opts {
		opt(option)
	}
	M := &mTimer{
		tw:          tw.NewTimingWheel(time.Millisecond*time.Duration(option.slotInterval), option.slotCount),
		cron:        cron.New(cron.WithParser(_CronParser)),
		idToTimer:   cmap.New(),
		idToCronId:  cmap.New(),
		redisClient: redisClient,
		tagToFac:    make(map[string]utils.ToAny),
		tagToAction: make(map[string]utils.ActionInt64Any),
		cronTagToId: cmap.New(),
	}
	if redisClient != nil {
		moduleOpts = append(moduleOpts, intfc.AfterModuleStart(func() {
			M.getRedisAfter()
			M.getRedisCron()
		}))
	}
	M.IModule = intfc.NewModule(moduleOpts...)
	return M
}

type mTimer struct {
	intfc.IModule
	tw          *tw.TimingWheel
	cron        *cron.Cron
	idToTimer   cmap.ConcurrentMap
	idToCronId  cmap.ConcurrentMap
	redisClient *redis.Client
	tagToFac    map[string]utils.ToAny
	tagToAction map[string]utils.ActionInt64Any
	cronTagToId cmap.ConcurrentMap
}

func (M *mTimer) Type() intfc.TModule {
	return intfc.MTimer
}

func (M *mTimer) Init() {
	M.tw.Start()
	M.cron.Start()
}

func (M *mTimer) Dispose() {
	M.tw.Stop()
	M.cron.Stop()
}

func (M *mTimer) After(ms int64, action utils.Action) (id int64) {
	id = utils.GenSnowflakeRegionNodeId()
	M.after(id, ms, action)
	return id
}

func (M *mTimer) AfterCh(ms int64) (int64, <-chan struct{}) {
	id := utils.GenSnowflakeRegionNodeId()
	ch := make(chan struct{}, 1)
	k := utils.Int64ToStr(id)
	M.idToTimer.Set(k, M.tw.AfterFunc(time.Millisecond*time.Duration(ms), func() {
		_, exist := M.idToTimer.Pop(k)
		if !exist {
			return
		}
		ch <- struct{}{}
	}))
	return id, ch
}

func (M *mTimer) after(id, ms int64, action utils.Action) {
	k := utils.Int64ToStr(id)
	M.idToTimer.Set(k, M.tw.AfterFunc(time.Millisecond*time.Duration(ms), func() {
		_, exist := M.idToTimer.Pop(k)
		if !exist {
			return
		}
		action()
	}))
}

func (M *mTimer) Tick(ms int64, total int32, action utils.ActionInt32) (id int64) {
	id = utils.GenSnowflakeRegionNodeId()
	M.tick(id, ms, 0, total, action)
	return id
}

func (M *mTimer) tick(id, ms int64, count, total int32, action utils.ActionInt32) {
	M.after(id, ms, func() {
		count++
		action(count)
		if total > 0 {
			if count == total {
				return
			}
		}
		M.tick(id, ms, count, total, action)
	})
}

func (M *mTimer) Cron(spec string, action utils.Action) (int64, error) {
	id := utils.GenSnowflakeRegionNodeId()
	return id, M._Cron(id, spec, action)
}

func (M *mTimer) _Cron(id int64, spec string, action utils.Action) error {
	entryID, err := M.cron.AddFunc(spec, action)
	if err != nil {
		return err
	}
	M.idToCronId.Set(utils.Int64ToStr(id), entryID)
	return nil
}

func (M *mTimer) Remove(id int64) bool {
	timer, exist := M.idToTimer.Pop(utils.Int64ToStr(id))
	if !exist {
		return false
	}
	timer.(*tw.Timer).Stop()
	return true
}

func (M *mTimer) RemoveCron(id int64) bool {
	cronId, exist := M.idToCronId.Pop(utils.Int64ToStr(id))
	if !exist {
		return false
	}
	M.cron.Remove(cronId.(cron.EntryID))
	return true
}

func (M *mTimer) Stubborn(ms int64, try utils.ToBool, complete utils.Action) { //倔强
	if try() {
		complete.Invoke()
		return
	}
	M.After(ms, func() {
		M.Stubborn(ms, try, complete)
	})
}

func (M *mTimer) PersistentAfter(tag string, sec int64, body interface{}) (id int64) {
	id = utils.GenSnowflakeRegionNodeId()
	idStr := strconv.FormatInt(id, 10)
	ctx := context.Background()
	M.after(id, sec*1000, func() {
		M.actionAndDelAfter(tag, idStr, id, body)
	})
	t := time.Now().Add(time.Second * time.Duration(sec)).Unix()
	v, _ := jsoniter.MarshalToString(map[string]interface{}{
		"tag":  tag,
		"time": strconv.FormatInt(t, 10),
		"body": body,
	})
	err := M.redisClient.HSet(ctx, "timer:after", idStr, v).Err()
	if err != nil {
		log.Fatal("save redis after failed", utils.M{
			"error": err.Error(),
		})
	}
	return
}

func (M *mTimer) actionAndDelAfter(tag, idStr string, id int64, body interface{}) {
	M.action(tag, id, body)
	err := M.redisClient.HDel(context.Background(), _AfterKey, idStr).Err()
	if err != nil {
		log.Fatal("remove redis after failed", utils.M{
			"error": err.Error(),
		})
	}
}

func (M *mTimer) RemovePersistentAfter(id int64) bool {
	ok := M.Remove(id)
	if !ok {
		return false
	}
	key := strconv.FormatInt(id, 10)
	err := M.redisClient.HDel(context.Background(), "timer:after", key).Err()
	if err != nil {
		log.Fatal("remove redis after failed", utils.M{
			"error": err.Error(),
		})
	}
	return true
}

//todo 同类型多节点会有多节点重复执行的问题,
func (M *mTimer) getRedisAfter() {
	result, err := M.redisClient.HGetAll(context.Background(), _AfterKey).Result()
	if err != nil {
		app.Log().Error(err.Error(), nil)
		return
	}
	for idStr, v := range result {
		id, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			app.Log().Error(err.Error(), utils.M{
				"id":    idStr,
			})
			continue
		}
		var m map[string]interface{}
		err = jsoniter.UnmarshalFromString(v, &m)
		if err != nil {
			app.Log().Error(err.Error(), utils.M{
				"id":    idStr,
			})
			continue
		}
		tag := m["tag"].(string)
		fac, ok := M.tagToFac[tag]
		if !ok {
			app.Log().Error("not exist tag factory", utils.M{
				"id":    idStr,
				"tag":   tag,
			})
			continue
		}
		body := fac()
		err = mapstructure.Decode(m["body"], &body)
		if err != nil {
			app.Log().Error(err.Error(), utils.M{
				"id":    idStr,
				"map":   m,
			})
			continue
		}
		t, err := strconv.ParseInt(m["time"].(string), 10, 64)
		if err != nil {
			app.Log().Error(err.Error(), utils.M{
				"id":    idStr,
				"map":   m,
			})
			continue
		}
		now := time.Now().Unix()
		if now >= t {
			M.actionAndDelAfter(tag, idStr, id, body)
			return
		}
		M.after(id, (t-now)*1000, func() {
			M.actionAndDelAfter(tag, idStr, id, body)
		})
	}
}

func (M *mTimer) PersistentCron(tag, spec string, body interface{}) (id int64, err error) {
	exist := M.cronTagToId.Has(tag)
	if exist {
		err = _ExistCronTagErr
		return
	}
	id = utils.GenSnowflakeRegionNodeId()
	err = M._Cron(id, spec, func() {
		M.action(tag, id, body)
	})
	if err != nil {
		return
	}
	M.cronTagToId.Set(tag, id)
	v, _ := jsoniter.MarshalToString(map[string]interface{}{
		"spec": spec,
		"body": body,
	})
	err = M.redisClient.HSet(context.Background(), _CronKey, tag, v).Err()
	if err != nil {
		log.Fatal("save redis cron failed", utils.M{
			"error": err.Error(),
		})
	}
	return
}

func (M *mTimer) RemovePersistentCron(tag string) bool {
	idAny, exists := M.cronTagToId.Pop(tag)
	if !exists {
		return false
	}
	id := idAny.(int64)
	ok := M.RemoveCron(id)
	if !ok {
		return false
	}
	key := strconv.FormatInt(id, 10)
	err := M.redisClient.HDel(context.Background(), "timer:cron", key).Err()
	if err != nil {
		log.Fatal("remove redis after failed", utils.M{
			"error": err.Error(),
		})
	}
	return true
}

func (M *mTimer) getRedisCron() {
	result, err := M.redisClient.HGetAll(context.Background(), _CronKey).Result()
	if err != nil {
		app.Log().Error(err.Error(), nil)
		return
	}
	for idStr, v := range result {
		id, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			app.Log().Error(err.Error(), utils.M{
				"id":    idStr,
			})
			continue
		}
		var m map[string]interface{}
		err = jsoniter.UnmarshalFromString(v, &m)
		if err != nil {
			app.Log().Error(err.Error(), utils.M{
				"id":    idStr,
			})
			continue
		}
		tag := m["tag"].(string)
		fac, ok := M.tagToFac[tag]
		if !ok {
			app.Log().Error(err.Error(), utils.M{
				"id":    idStr,
				"tag":   tag,
			})
			continue
		}
		body := fac()
		err = mapstructure.Decode(m["body"], &body)
		if err != nil {
			app.Log().Error(err.Error(), utils.M{
				"id":    idStr,
				"map":   m,
			})
			continue
		}
		err = M._Cron(id, m["spec"].(string), func() {
			M.action(tag, id, body)
		})
		if err != nil {
			return
		}
	}
}

func (M *mTimer) BindPersistentAction(tag string, fac utils.ToAny, action utils.ActionInt64Any) {
	M.tagToFac[tag] = fac
	M.tagToAction[tag] = action
}

func (M *mTimer) action(tag string, id int64, body interface{}) {
	action, ok := M.tagToAction[tag]
	if !ok {
		app.Log().Fatal("not exist action", utils.M{
			"tag": tag,
		})
		return
	}
	action(id, body)
}
