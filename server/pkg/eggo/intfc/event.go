package intfc

import (
	"95eh.com/eg/utils"
	"strconv"
)

type IMEvent interface {
	IModule
	IEventDispatcher
}

type TEvent uint16

type IEventDispatcher interface {
	BindEvent(t TEvent, action utils.ActionAny) int64
	UnbindEvent(id int64)
	DispatchEvent(t TEvent, event interface{})
}

var (
	_EventToName = map[TEvent]string{}
)

func BindEventName(m map[TEvent]string) {
	for t, name := range m {
		_EventToName[t] = name
	}
}

func (t TEvent) Name() string {
	name, ok := _EventToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}

func (t TEvent) SetName(name string) {
	_EventToName[t] = name
}
