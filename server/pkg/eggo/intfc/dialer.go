package intfc

import "95eh.com/eg/data"

// IDialer 拨号器接口
type IDialer interface {
	// Name 拨号器名称
	Name() string
	// Addr 远程地址
	Addr() string
	Enable() bool
	// Connect 连接远程服务器
	Connect(m data.Map, options ...ActionAgentOption) error
	// Agent 连接代理
	Agent() IAgent
	// Close 关闭连接
	Close()
}

type (
	ActionDialerOption func(o *DialerOption)
	ActionDialer       func(dialer IDialer)
)

// DialerOption 拨号器选项
type DialerOption struct {
	//连接完成回调
	OnConnected ActionDialer
	//连接关闭回调
	OnClosed ActionDialer
}

// DialerConnected 设置连接完成回调
func DialerConnected(onConnected ActionDialer) ActionDialerOption {
	return func(o *DialerOption) {
		o.OnConnected = onConnected
	}
}

// DialerClosed 设置连接关闭回调
func DialerClosed(onClosed ActionDialer) ActionDialerOption {
	return func(o *DialerOption) {
		o.OnClosed = onClosed
	}
}
