package intfc

import (
	"95eh.com/eg/utils"
)

// IMTxProducer 事务代理模块
type IMTxProducer interface {
	// PushMsg 推送消息
	PushMsg(tid int64, code uint32, body interface{}) error
	// Tx 同步执行事务
	Tx(tid int64, action ActionTxToBool)
}

type (
	ITx interface {
		Tid() int64
		Try(service uint16, id int64, code uint32, body interface{}) (res interface{}, ec utils.TErrCode)
		Confirm()
		Cancel()
	}
	ActionTxToBool func(tx ITx) bool
)
