package intfc

type (
	IDisposable interface {
		Dispose()
	}
	ActionDisposable       func(obj IDisposable)
	ActionActionDisposable func(ActionDisposable)
	ToDisposable           func() IDisposable
)
