package intfc

import (
	"95eh.com/eg/data"
	"95eh.com/eg/utils"
)

type IMUserLogic interface {
	IModule
	// AddUserData 添加用户数据
	AddUserData(uid int64, mask int64, addr string, m data.Map) (oldAddr string, exist bool)
	GetUserUid(addr string) (uid int64, ok bool)
	DelUserData(addr string)
	// SetUidAlias 设置uid别名,例如rpg的角色id
	SetUidAlias(uid, alias int64)
	// GetUidWithAlias 使用别名获取uid
	GetUidWithAlias(alias int64) (uid int64, ok bool)
	// GetAliasWithUid 使用uid获取别名
	GetAliasWithUid(uid int64) (alias int64, ok bool)
	SendByAlias(alias int64, bytes []byte) (err utils.IError)
	SendByUid(uid int64, bytes []byte) (err utils.IError)
	SendByUids(uids []int64, bytes []byte) (ok map[int64]utils.IError)
	SendByAliases(aliases []int64, bytes []byte) (ok map[int64]utils.IError)
	// TestUserAuth 测试用户权限
	TestUserAuth(uid int64, code uint32, addr string) bool
	// AddUserRole 添加用户角色
	AddUserRole(uid int64, role int64)
	// DelUserRole 移除用户角色
	DelUserRole(uid int64, role int64)
	// TestMask 测试权限
	TestMask(mask int64, code uint32) bool
	// ReadUserData 读取用户数据
	ReadUserData(uid int64, action data.ActionBoolData)
	// WriteUserData 写入用户数据
	WriteUserData(uid int64, action data.ActionBoolData)
	// BindCoderFac 绑定编解码工厂函数
	BindCoderFac(code uint32, reqFac, resFac utils.ToAny)
	// BindRequestProcessor 绑定消息处理器
	BindRequestProcessor(role int64, code uint32, processor IUserRequestProcessor)
	// BindRequestHandler 绑定消息处理器
	BindRequestHandler(role int64, code uint32, action utils.ActionStrInt642Any)
	// ProcessRequest 处理请求
	ProcessRequest(addr string, tid, uid int64, code uint32, body interface{}, resOk utils.ActionAny, resErr utils.ActionErrCode)
	// ResOk 响应成功
	ResOk(tid int64, data interface{})
	// ResErr 响应错误
	ResErr(tid int64, ec utils.TErrCode)
}

type IUserRequestProcessor interface {
	// Request 处理用户请求
	Request(addr string, tid, id int64, body interface{})
}
