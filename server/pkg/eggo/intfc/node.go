package intfc

import "strconv"

type TNode uint16

var (
	_NodeToStr    = make(map[TNode]string)
	_NodeToRegion = make(map[TNode]bool)
)

func BindNodeName(app TNode, name string, region bool) {
	_NodeToStr[app] = name
	_NodeToRegion[app] = region
}

// String 应用类型转名称
func (a TNode) String() string {
	str, ok := _NodeToStr[a]
	if ok {
		return str
	}
	return strconv.Itoa(int(a))
}

// Int 应用类型转整型
func (a TNode) Int() int {
	return int(a)
}

// IsRegionApp 是否是区域服
func (a TNode) IsRegion() bool {
	return _NodeToRegion[a]
}

// StrToNode 应用名转应用类型
func StrToNode(str string) TNode {
	for app, s := range _NodeToStr {
		if s == str {
			return app
		}
	}
	v, err := strconv.Atoi(str)
	if err != nil {
		return 0
	}
	return TNode(v)
}
