package intfc

import "95eh.com/eg/utils"

type IMUserGate interface {
	IModule
	IGate
	Codec() ICodec
	StartListen()
	ProcessRequest(addr string, tid, id int64, code uint32, body interface{}, resOk utils.ActionBytes, resErr utils.ActionErrCode)
}
