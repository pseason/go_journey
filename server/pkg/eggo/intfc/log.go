package intfc

import (
	"95eh.com/eg/utils"
	"strconv"
)

type (
	TLogger      int8
	TLoggerLevel int8
	TTrace       uint32
)

const (
	TLoggerNil TLogger = iota
	TLoggerStdout
)

const (
	TLogDebug TLoggerLevel = iota
	TLogInfo
	TLogWarn
	TLogError
	TLogFatal
)

// IMLog 日志模块
type IMLog interface {
	IModule
	// Debug 调试日志
	Debug(str string, params utils.M)
	// Info 普通日志
	Info(str string, params utils.M)
	// Warn 警告日志
	Warn(str string, params utils.M)
	// Error 错误日志
	Error(str string, params utils.M)
	// Fatal 严重错误日志
	Fatal(str string, params utils.M)
	// TSign 标记一个新链路
	TSign(pid int64, params utils.M) int64
	//// TDebugO 链路调试日志
	////Deprecated: 使用TDebug
	//TDebugO(tid int64, t TTrace, params utils.M)
	//// TInfoO 链路普通日志
	////Deprecated: TInfo
	//TInfoO(tid int64, t TTrace, params utils.M)
	//// TWarnO 链路警告日志
	////Deprecated: TWarn
	//TWarnO(tid int64, t TTrace, params utils.M)
	//// TErrorO 链路错误日志
	////Deprecated: TError
	//TErrorO(tid int64, t TTrace, params utils.M)
	//// TFatalO 链路严重错误日志
	////Deprecated: TFatal
	//TFatalO(tid int64, t TTrace, params utils.M)

	// TDebug 链路调试日志
	TDebug(tid int64, msg string, params utils.M)
	// TInfo 链路普通日志
	TInfo(tid int64, msg string, params utils.M)
	// TWarn 链路警告日志
	TWarn(tid int64, msg string, params utils.M)
	// TError 链路错误日志
	TError(tid int64, err utils.IError)
	// TFatal 链路严重错误日志
	TFatal(tid int64, err utils.IError)
}

// ILogger 日志
type ILogger interface {
	// Type 类型
	Type() TLogger
	// Level 日志级别
	Level() TLoggerLevel
	// Log 记录日志
	Log(level TLoggerLevel, timestamp, caller, msg string, params utils.M)
	// Sign 标记链路
	Sign(parent, id int64, timestamp, caller string, params utils.M)
	// Trace 链路日志
	Trace(level TLoggerLevel, id int64, msg, timestamp, caller string, stack []byte, params utils.M)
}

var (
	_TraceToName = map[TTrace]string{}
)

// BindTraceName 绑定链路名
func BindTraceName(m map[TTrace]string) {
	for t, name := range m {
		_TraceToName[t] = name
	}
}

// Name 链路名
func (t TTrace) Name() string {
	name, ok := _TraceToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}