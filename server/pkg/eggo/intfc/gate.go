package intfc

import "95eh.com/eg/utils"

// IGate 网关
type IGate interface {
	// Add 添加连接代理
	Add(agent IAgent)
	// Remove 移除连接代理
	Remove(agent string)
	// Send 发送消息到指定代理
	Send(addr string, data []byte) utils.IError
	// SendAll 发送消息到所有代理
	SendAll(data []byte)
	// SendMulti 发送消息到多个代理
	SendMulti(agents []string, data []byte)
	// SendExclude 发送除指定代理外的其他代理
	SendExclude(agents []string, data []byte)
	// SendByFilter 使用过滤器发送指定代理
	SendByFilter(filter func(IAgent) bool, data []byte)
	// CloseAll 关闭所有代理
	CloseAll()
	// Len 当前代理的数量
	Len() int
}
