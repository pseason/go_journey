package intfc

import "95eh.com/eg/utils"

type (
	TModule      uint16
	ActionModule func(module IModule)
	IModule      interface {
		// Type 模块类型
		Type() TModule
		// Init 模块初始化
		Init()
		// BeforeStart 启动前执行
		BeforeStart()
		// Start 启动模块
		Start()
		// AfterStart 启动后执行
		AfterStart()
		// BeforeDispose 释放执行执行
		BeforeDispose()
		// Dispose 释放模块
		Dispose()
		// AfterDispose 释放之后执行
		AfterDispose()
	}
	ModuleOption func(*Module)
	Module       struct {
		beforeStart   []utils.Action
		afterStart    []utils.Action
		beforeDispose []utils.Action
		afterDispose  []utils.Action
	}
)

func ModuleOptions(opts ...ModuleOption) []ModuleOption {
	return opts
}

func NewModule(opts ...ModuleOption) IModule {
	m := &Module{}
	for _, opt := range opts {
		if opt == nil {
			continue
		}
		opt(m)
	}
	return m
}

// BeforeModuleStart 设置模块启动之前的回调
func BeforeModuleStart(actions ...utils.Action) ModuleOption {
	return func(module *Module) {
		if module.beforeStart == nil {
			module.beforeStart = actions
		} else {
			module.beforeStart = append(module.beforeStart, actions...)
		}
	}
}

// AfterModuleStart 设置模块启动之后的回调
func AfterModuleStart(actions ...utils.Action) ModuleOption {
	return func(module *Module) {
		if module.afterStart == nil {
			module.afterStart = actions
		} else {
			module.afterStart = append(module.afterStart, actions...)
		}
	}
}

// BeforeModuleStart 设置模块启动之前的回调
func BeforeModuleDispose(actions ...utils.Action) ModuleOption {
	return func(module *Module) {
		if module.beforeDispose == nil {
			module.beforeDispose = actions
		} else {
			module.beforeDispose = append(module.beforeDispose, actions...)
		}
	}
}

// AfterModuleStart 设置模块启动之后的回调
func AfterModuleDispose(actions ...utils.Action) ModuleOption {
	return func(module *Module) {
		if module.afterDispose == nil {
			module.afterDispose = actions
		} else {
			module.afterDispose = append(module.afterDispose, actions...)
		}
	}
}

// Type 模块类型
func (M *Module) Type() TModule {
	panic("implement me")
}

// Init 模块初始化
func (M *Module) Init() {

}

// BeforeStart 执行模块启动之前的回调
func (M *Module) BeforeStart() {
	for _, action := range M.beforeStart {
		action()
	}
}

// Start 模块启动
func (M *Module) Start() {

}

// AfterStart 执行模块启动之后的回到
func (M *Module) AfterStart() {
	for _, action := range M.afterStart {
		action()
	}
}

// BeforeDispose 释放模块
func (M *Module) BeforeDispose() {
	for _, action := range M.beforeDispose {
		action()
	}
}

// Dispose 释放模块
func (M *Module) Dispose() {

}

// AfterDispose 释放模块
func (M *Module) AfterDispose() {
	for _, action := range M.afterDispose {
		action()
	}
}

const (
	MNil TModule = iota
	// MAsset 资源模块
	MAsset
	// MDiscovery 服务发现模块
	MDiscovery
	// MEvent 事件模块
	MEvent
	// MLog 日志模块
	MLog
	// MTxConsumer 区服事务模块
	MTxConsumer
	// MTxProducer 区服事务代理模块
	MTxProducer
	// MScene 场景模块
	MScene
	// MSceneCache 场景缓存模块
	MSceneCache
	// MTimer 计时器模块
	MTimer
	// MUserLogic 用户逻辑模块(应用层)
	MUserLogic
	// MUserGate 用户长连接模块
	MUserGate
	// MUserHttp 用户短连接模块
	MUserHttp
	// MWorker 多协程模块
	MWorker
	MMax
)

var (
	_ModuleNames = map[TModule]string{
		MAsset:      "MAsset",
		MDiscovery:  "MDiscovery",
		MEvent:      "MEvent",
		MLog:        "MLog",
		MTxConsumer: "MTxConsumer",
		MTxProducer: "MTxProducer",
		MScene:      "MScene",
		MSceneCache: "MSceneCache",
		MTimer:      "MTimer",
		MUserLogic:  "MUserLogic",
		MUserGate:   "MUserGate",
		MUserHttp:   "MUserHttp",
		MWorker:     "MWorker",
	}
)

// 模块名
func (t TModule) String() string {
	return _ModuleNames[t]
}
