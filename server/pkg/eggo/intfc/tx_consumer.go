package intfc

import (
	"95eh.com/eg/utils"
)

// IMTxConsumer 事务模块
type IMTxConsumer interface {
	IModule
	// WatchMsg 监听消息
	WatchMsg(ch string, code uint32, action utils.ActionInt64Uint32Any)
	// BindTxProcessor 绑定事务处理器
	BindTxProcessor(code uint32, processor ITxProcessor)
	// BindTxHandler 绑定事务处理器
	BindTxHandler(code uint32, try ActionUint16Int642AnyToAnyErrCode,
		confirm, cancel ActionUint16Int642Any2ToErr)
	// ProcessConfirm 处理确认事务
	ProcessConfirm(tid, id int64, service uint16, code uint32, reqBody, resBody interface{}, res utils.ActionBool)
	// ProcessCancel 处理取消事务
	ProcessCancel(tid, id int64, service uint16, code uint32, reqBody, resBody interface{}, res utils.ActionBool)
	// TxLock 加锁
	TxLock(pid int64, name string) error
	// TxUnlock 解锁
	TxUnlock(pid int64, name string) (bool, error)
	// LockAction 加锁执行
	LockAction(pid int64, name string, action utils.Action)
}

// IRequestProcessor 请求处理器
type IRequestProcessor interface {
	// Request 处理请求
	Request(service uint16, v1, v2 int64, body interface{}) (interface{}, utils.TErrCode)
}

// ITxProcessor 事务处理器
type ITxProcessor interface {
	// Try 尝试事务
	Try(service uint16, v1, v2 int64, body interface{}) (interface{}, utils.TErrCode)
	// Confirm 确认事务
	Confirm(service uint16, v1, v2 int64, reqBody, resBody interface{}) utils.IError
	// Cancel 取消事务
	Cancel(service uint16, v1, v2 int64, reqBody, resBody interface{}) utils.IError
}
