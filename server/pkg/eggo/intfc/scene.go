package intfc

import (
	"95eh.com/eg/data"
	"95eh.com/eg/utils"
	"strconv"
)

type (
	TScene          uint16
	TSceneState     uint16
	TSceneComponent uint16
	TActor          uint16
	TActorComponent uint16
	TActorState     uint16
	TActorEvent     uint16
)

type (
	ActionSceneAnyToErr     func(scene IScene, o interface{}) utils.IError
	ActionActorAnyToErr     func(actor IActor, o interface{}) utils.IError
	ActionActor             func(actor IActor)
	ActionInt64Actor        func(v int64, actor IActor)
	ActionActorToErr        func(actor IActor) utils.IError
	ActionSceneActor        func(scene IScene, actor IActor)
	ActionSceneStateToErr   func(scene IScene, state TSceneState) utils.IError
	ActionActorStateToErr   func(actor IActor, state TActorState) utils.IError
	ActionActorEventToError func(actor IActor, t TActorEvent, event interface{}) utils.IError
)

type IMScene interface {
	IModule
	IWorker
	// BindSceneFac 绑定场景工厂
	BindSceneFac(t TScene, fac ActionSceneAnyToErr)
	// CreateScene 创建场景
	CreateScene(t TScene, id int64, data interface{}) utils.IError
	// DisposeScene 释放场景
	DisposeScene(t TScene, sceneId int64) utils.IError
	// GetScene 获取场景
	GetScene(t TScene, sceneId int64) (IScene, utils.IError)
	// BindActorEnterScene 绑定Actor进入场景
	BindActorEnterScene(sceneType TScene, before, after ActionSceneActor)
	// ActorEnterScene Actor进入场景
	ActorEnterScene(scene IScene, actor IActor)
	// BindActorExitScene 绑定Actor退出场景
	BindActorExitScene(sceneType TScene, before, after ActionSceneActor)
	// ActorExitScene Actor退出场景
	ActorExitScene(scene IScene, actor IActor)
	// BindSceneState 绑定场景状态
	BindSceneState(state TSceneState, enter, exit ActionSceneStateToErr)
	// SceneEnterState 场景进入状态
	SceneEnterState(state TSceneState, scene IScene) utils.IError
	// SceneExitState 场景退出状态
	SceneExitState(state TSceneState, scene IScene) utils.IError
	// BindActorState 绑定Actor状态
	BindActorState(state TActorState, enter, exit ActionActorStateToErr)
	// ActorEnterState Actor进入状态
	ActorEnterState(state TActorState, actor IActor) utils.IError
	// ActorExitState Actor退出状态
	ActorExitState(state TActorState, actor IActor) utils.IError
	// BindActorEventProcessor 绑定不同场景的Actor事件处理器
	BindActorEventProcessor(evt TActorEvent, processor ActionActorEventToError, sceneTypes ...TScene)
	// GetActorEventProcessor 获取Actor事件处理器
	GetActorEventProcessor(sceneType TScene, t TActorEvent) (ActionActorEventToError, utils.IError)
}

type IMSceneCache interface {
	IModule
	SaveActor(actorId int64, actorType TActor, tags []string, components ...IActorComponent) utils.IError
	LoadActor(actorId int64) (actorType TActor, tags []string, e utils.IError)
	LoadActorComponent(actorId int64, c IActorComponent) utils.IError
	SaveActorComponent(actorId int64, c IActorComponent) utils.IError
	DelActor(actorId int64) utils.IError
}

type IScene interface {
	IWorker
	data.IData
	// Id 场景唯一id
	Id() int64
	// Type 场景类型
	Type() TScene
	// Event 事件派发器
	Event() IEventDispatcher
	// AddComponent 添加组件
	AddComponent(component ISceneComponent) utils.IError
	// RemoveComponent 移除组件
	RemoveComponent(t TSceneComponent) utils.IError
	// GetComponent 获取组件
	GetComponent(t TSceneComponent) (component ISceneComponent, err utils.IError)
	// AddActor 添加Actor
	AddActor(actorType TActor, actorId int64) (IActor, utils.IError)
	// RemoveActor 移除Actor
	RemoveActor(actorId int64) utils.IError
	// IsEmpty 场景是否没有Actor
	IsEmpty() bool
	// ChangeState 切换状态
	ChangeState(state TSceneState)
	// CurrentState 当前状态
	CurrentState() TSceneState
	// AddActorTags 添加Actor标签
	AddActorTags(actorId int64, tags ...string) utils.IError
	// RemoveActorTags 移除Actor标签
	RemoveActorTags(actorId int64, tags ...string) utils.IError
	// AddTagActors 给指定标签同时添加多个Actor
	AddTagActors(tag string, actorIds ...int64)
	// RemoveTagActors 给指定标签同时移除多个Actor
	RemoveTagActors(tag string, actorIds ...int64)
	// GetTagActors 获取指定标签的宿友Actor
	GetTagActors(tag string) ([]int64, utils.IError)
	// ClearActorTags 清除Actor所有Tag
	ClearActorTags(actorId int64) utils.IError
	// GetActor 获取Actor
	GetActor(actorId int64) (IActor, utils.IError)
	// ActionActor 指定Actor执行行为
	ActionActor(actorId int64, action ActionActorToErr) utils.IError
	// ActionActors 指定多个Actor执行行为
	ActionActors(actorIds []int64, action ActionActor) utils.IError
	// ActionActorByTags 指定标签的所有Actor执行行为
	ActionActorByTags(action ActionActor, tags ...string)
	// ActionActorByTypes 指定类型的所有Actor执行行为
	ActionActorByTypes(action ActionActor, types ...TActor)
	// ActionAllActor 所有Actor执行行为
	ActionAllActor(action ActionActor)
	// DispatchActorEvent 指定Actor派发事件
	DispatchActorEvent(actorId int64, t TActorEvent, event interface{}) utils.IError
	// DispatchActorsEvent 指定多个Actor派发事件
	DispatchActorsEvent(actorIds []int64, t TActorEvent, event interface{}) utils.IError
	// DispatchAllActorEvent 给所有Actor派发事件
	DispatchAllActorEvent(t TActorEvent, event interface{})
	// DispatchActorEventByTags 指定过个标签发发事件
	DispatchActorEventByTags(t TActorEvent, event interface{}, tags ...string)
	// DispatchActorEventByTypes 指定Actor类型派发事件
	DispatchActorEventByTypes(t TActorEvent, event interface{}, types ...TActor)
	// AddTicker 添加需要心跳的Actor
	AddTicker(ticker utils.ActionInt64) int64
	// RemoveTicker 移除不需要心跳的Actor
	RemoveTicker(tickId int64)
	Dispose()
}

type ISceneComponent interface {
	// Type 类型
	Type() TSceneComponent
	// Start 开始
	Start() utils.IError
	// Stop 释放
	Stop() utils.IError
	// Scene 演员对象
	Scene() IScene
	// SetScene 设置演员对象
	SetScene(scene IScene)
}

type IActor interface {
	// Id Actor的唯一id
	Id() int64
	// Type Actor类型
	Type() TActor
	// Scene 所在场景
	Scene() IScene
	// SetScene 设置场景
	SetScene(scene IScene)
	// CurrState 当前状态
	CurrState() TActorState
	// SetState 设置状态
	SetState(state TActorState)
	// Event 事件派发器
	Event() IEventDispatcher
	// Tags 标签
	Tags() []string
	// AddTag 添加标签
	// 使用IScene的AddActorTags调用,外部不要使用
	AddTag(tag string)
	// RemoveTag 移除标签
	// 使用IScene的RemoveActorTags调用,外部不要使用
	RemoveTag(tag string)
	// AddComponent 添加组件
	AddComponent(component IActorComponent) utils.IError
	// RemoveComponent 移除组件
	RemoveComponent(t TActorComponent) utils.IError
	// GetComponent 获取组件
	GetComponent(t TActorComponent) (component IActorComponent, ok bool)
	// AddTicker 添加心跳
	AddTicker(ticker ActionInt64Actor)
	// RemoveTicker 移除心跳
	RemoveTicker(ticker ActionInt64Actor)
	// Tick 执行心跳
	Tick(ms int64)
	// BindEventProcessor 绑定事件处理器
	BindEventProcessor(events ...TActorEvent)
	// UnbindEventProcessor 解绑事件处理器
	UnbindEventProcessor(events ...TActorEvent)
	// ProcessEvent 处理场景事件
	ProcessEvent(t TActorEvent, event interface{})
	// Dispose 释放
	Dispose()
}

type IActorComponent interface {
	// Type 类型
	Type() TActorComponent
	// Start 开始
	Start() utils.IError
	// Dispose 释放
	Dispose() utils.IError
	// Actor 演员对象
	Actor() IActor
	// SetActor 设置演员对象
	SetActor(actor IActor)
}

var (
	_SceneToName          = map[TScene]string{}
	_SceneStateToName     = map[TSceneState]string{}
	_SceneComponentToName = map[TSceneComponent]string{}
	_ActorToName          = map[TActor]string{}
	_ActorEventToName     = map[TActorEvent]string{}
	_ActorStateToName     = map[TActorState]string{}
	_ActorComponentToName = map[TActorComponent]string{}
)

func BindActorName(m map[TActor]string) {
	for t, name := range m {
		_ActorToName[t] = name
	}
}

func (t TActor) String() string {
	name, ok := _ActorToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}

func (t TActor) SetName(name string) {
	_ActorToName[t] = name
}

func BindActorEventName(m map[TActorEvent]string) {
	for t, name := range m {
		_ActorEventToName[t] = name
	}
}

func (t TActorEvent) Name() string {
	name, ok := _ActorEventToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}

func (t TActorEvent) SetName(name string) {
	_ActorEventToName[t] = name
}

func BindActorStateName(m map[TActor]string) {
	for t, name := range m {
		_ActorToName[t] = name
	}
}

func (t TActorState) Name() string {
	name, ok := _ActorStateToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}

func (t TActorState) SetName(name string) {
	_ActorStateToName[t] = name
}

func BindSceneName(m map[TScene]string) {
	for t, name := range m {
		_SceneToName[t] = name
	}
}

func (t TScene) String() string {
	name, ok := _SceneToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}

func BindSceneStateName(m map[TSceneState]string) {
	for t, name := range m {
		_SceneStateToName[t] = name
	}
}

func (t TSceneState) Name() string {
	name, ok := _SceneStateToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}

func BindSceneComponentName(m map[TSceneComponent]string) {
	for t, name := range m {
		_SceneComponentToName[t] = name
	}
}

func (t TSceneComponent) Name() string {
	name, ok := _SceneComponentToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}

func (t TSceneComponent) SetName(name string) {
	_SceneComponentToName[t] = name
}

func BindActorComponentName(m map[TActorComponent]string) {
	for t, name := range m {
		_ActorComponentToName[t] = name
	}
}

func (t TActorComponent) Name() string {
	name, ok := _ActorComponentToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}

func (t TActorComponent) SetName(name string) {
	_ActorComponentToName[t] = name
}
