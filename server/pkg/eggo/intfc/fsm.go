package intfc

import (
	"95eh.com/eg/data"
	"strconv"
)

type (
	TState uint16
)

// IFsm 状态机
type IFsm interface {
	// IData 状态持久数据
	data.IData
	// CurrentState 当前状态
	CurrentState() TState
	// ChangeState 切换状态
	// t 目标状态
	// data 上一状态退出和目标状态进入需要的数据
	ChangeState(t TState, any interface{})
	// Dispose 释放
	// data 当前状态需要的数据
	Dispose(any interface{})
}

// IState 状态
type IState interface {
	// Fsm 状态机
	Fsm() IFsm
	// Type 状态类型
	Type() TState
	// Enter 进入状态
	Enter(any interface{})
	// Exit 退出状态
	Exit(any interface{})
}

var (
	_StateToName = map[TState]string{}
)

// BindStateName 绑定状态名
func BindStateName(m map[TState]string) {
	for t, name := range m {
		_StateToName[t] = name
	}
}

// Name 状态名
func (t TState) Name() string {
	name, ok := _StateToName[t]
	if ok {
		return name
	}
	return strconv.Itoa(int(t))
}
