package intfc

import (
	"95eh.com/eg/utils"
	"github.com/go-redis/redis/v8"
	consulApi "github.com/hashicorp/consul/api"
)

type (
	ActionNodeUint162                           func(node TNode, regionId, nodeId uint16)
	ActionUint16Int64Any                        func(service uint16, v int64, obj interface{})
	ActionUint16Int642AnyActionAnyActionErrCode func(service uint16, v1, v2 int64, body interface{},
		resObj utils.ActionAny, resErr utils.ActionErrCode)
	ActionUint16Int642AnyToAnyErrCode func(service uint16, v1, v2 int64, body interface{}) (interface{}, utils.TErrCode)
	ActionUint16Int642Any2ToErr       func(service uint16, v1, v2 int64, reqBody, resBody interface{}) utils.IError
)

type (
	IMDiscovery interface {
		IModule
		IDisposable
		// Id 节点唯一id
		Id() int64
		// RegionId 区服id
		RegionId() uint16
		// NodeTypes 节点类型
		NodeTypes() []TNode
		// NodeId 节点id
		NodeId() uint16
		// WatchNodes 监听节点
		WatchNodes(nodes ...TNode)
		// GetRegionName 获取区服名
		GetRegionName(regionId uint16) string
		// AddWhiteList 添加节点白名单
		AddWhiteList(list []string)
		// Redis redis客户端
		Redis() *redis.Client
		// SetKV 设置键值对
		SetKV(key string, val []byte, opt *consulApi.WriteOptions) error
		// RemoveKV 移除键值对
		RemoveKV(key string, opt *consulApi.WriteOptions) error
		// GetKV 获取键值对
		GetKV(key string, opt *consulApi.QueryOptions) ([]byte, error)
		// WatchKV 监听键值对
		WatchKV(key string, action utils.ActionStrBytesAny) (int64, error)
		// UnwatchKV 取消键值对监听
		UnwatchKV(id int64)
		// BindNodeServices 绑定节点服务
		BindNodeServices(node TNode, services ...uint16)
		// GetNodeByService 通过服务获取节点
		GetNodeByService(service uint16) (TNode, bool)
		// RequestNode 请求指定节点
		RequestNode(tNode TNode, nodeId, service uint16, pTid, id int64, code uint32, body interface{},
			resOk utils.ActionInt64Any, resErr utils.ActionInt64ErrCode)
		SyncRequestNode(tNode TNode, nodeId, service uint16, pTid, id int64, code uint32,
			body interface{}) (res interface{}, ec utils.TErrCode)
		// Request 请求任意节点
		Request(service uint16, pTid, id int64, code uint32, body interface{},
			resOk utils.ActionUint16Int64Any, resErr utils.ActionUint16Int64ErrCode) (nodeId uint16)
		// SyncRequest 同步请求任意节点
		SyncRequest(service uint16, pTid, id int64, code uint32,
			body interface{}) (nodeId uint16, res interface{}, ec utils.TErrCode)
		// RequestWithSTid 请求任意节点
		RequestWithSTid(service uint16, sTid, id int64, code uint32, body interface{},
			resOk utils.ActionUint16Any, resErr utils.ActionUint16ErrCode) (nodeId uint16)
		// SyncRequestWithSTid 同步请求任意节点
		SyncRequestWithSTid(service uint16, sTid, id int64, code uint32,
			body interface{}) (nodeId uint16, res interface{}, ec utils.TErrCode)
		// DispatchEvent 派发事件给指定类型的节点
		// service和code是派发事件的服务和协议号
		DispatchEvent(tNode TNode, service uint16, pTid, id int64, code uint32, body interface{})
		// DispatchEventToAllNode 派发事件给所有节点
		DispatchEventToAllNode(service uint16, pTid, id int64, code uint32, body interface{})
		// SendEvent 发送事件给任意节点
		SendEvent(tNode TNode, service uint16, pTid, id int64, code uint32, body interface{}) (nodeId uint16, ok bool)
		// SendEventToNode 发送事件给指定节点
		SendEventToNode(tNode TNode, service, nodeId uint16, pTid, id int64, code uint32, body interface{}) (ok bool)
		// BindCoderFac 绑定编解码工厂
		BindCoderFac(code uint32, reqFac, resFac utils.ToAny)
		// BindRequestProcessor 绑定请求处理器
		BindRequestProcessor(code uint32, processor IRequestProcessor)
		// BindRequestAsyncHandler 绑定异步请求处理器
		BindRequestAsyncHandler(code uint32, handler ActionUint16Int642AnyActionAnyActionErrCode)
		// BindRequestHandler 绑定请求处理器
		BindRequestHandler(code uint32, handler ActionUint16Int642AnyToAnyErrCode)
		// BindEventProcessor 绑定事件
		// service为关注的服务
		// processors为关注的code和对象处理器
		BindEventProcessor(code uint32, processor IEventProcessor)
		// BindEventHandler 绑定事件
		BindEventHandler(code uint32, handler ActionUint16Int64Any)
	}
)

// IEventProcessor 事件处理器
type IEventProcessor interface {
	Trigger(service uint16, id int64, obj interface{})
}

// IRegister 节点注册器
type IRegister interface {
	Key() string
	Val() []byte
	Close()
}

// DiscoveryConf 服务发现的配置
type DiscoveryConf struct {
	// 区服id
	RegionId uint16
	// 节点id
	NodeId uint16
	// 负载均衡用的权重
	Weight int
	// 长连接监听的地址
	Ip string
	// 长连接监听的端口
	Port int
}

// INodeSelector 节点拨号器的负载均衡选择器
type INodeSelector interface {
	IDisposable
	// AddNode 添加节点
	AddNode(nodeId uint16, name, addr string, receiver ActionAgentBytesToErrCode, weight int) (IDialer, bool)
	// RemoveNode 移除节点
	RemoveNode(nodeId uint16)
	// EnableNode 启用节点
	EnableNode(nodeId uint16)
	// DisableNode 禁用节点
	DisableNode(nodeId uint16)
	// SendTo 发送消息到指定节点,可能会失败
	SendTo(nodeId uint16, pkt []byte) error
	SendNext(pkt []byte) (nodeId uint16, err error)
	// SendAll 发送给所有节点,可能会失败
	SendAll(pkt []byte)
}
