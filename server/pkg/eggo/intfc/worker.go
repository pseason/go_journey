package intfc

import (
	"95eh.com/eg/utils"
)

type TJob int32

type IMWorker interface {
	IModule
	// BindJob 绑定Job
	BindJob(t TJob, action utils.ActionAnyToAction)
	// DoJob 执行Job
	DoJob(id int64, t TJob, data interface{})
	// DoJobs 按顺序执行多个Job
	DoJobs(id int64, data interface{}, types ...TJob)
	// DoAction 执行Action
	DoAction(id int64, action utils.Action)
	// DoActions 按顺序执行多个Action
	DoActions(id int64, actions ...utils.Action)
}

type IWorker interface {
	// Pause 暂停处理
	Pause() utils.IError
	// Resume 恢复处理
	Resume() utils.IError
	// DoAction 执行Action
	DoAction(action utils.Action) utils.IError
	// DoActions 执行多个Action
	DoActions(actions ...utils.Action) utils.IError
	// Stop 释放
	Stop()
}
