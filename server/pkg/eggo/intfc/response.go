package intfc

import "95eh.com/eg/utils"

var (
	// ResponseTimeoutDur 响应超时时间,单位毫秒
	ResponseTimeoutDur int64 = 5000
)

// IResponse 响应
type IResponse interface {
	// Init 初始化
	Init(resOk utils.ActionAny, resErr utils.ActionErrCode, timeout utils.Action)
	// ResOk 正常响应
	ResOk(res interface{})
	// ResErr 错误响应
	ResErr(err utils.TErrCode)
}
