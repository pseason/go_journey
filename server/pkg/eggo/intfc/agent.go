package intfc

import (
	"95eh.com/eg/data"
	"95eh.com/eg/utils"
	"net"
)

var (
	TcpDeadlineDur int64 = 30000
)

type (
	ActionAgentToBool         func(data IAgent) bool
	ActionAgentBytesToErrCode func(agent IAgent, data []byte) utils.TErrCode
	ActionAgent               func(agent IAgent)
	ActionAgentErr            func(agent IAgent, err error)
	ActionConnToBool          func(conn net.Conn) bool
	ActionAgentOption         func(o *AgentOption)
)

// 连接代理接口
type IAgent interface {
	// Addr 远程地址
	Addr() string
	// Data 数据
	Data() data.IData
	// Conn 连接接口
	Conn() net.Conn
	// Send 发送数据
	Send(data []byte) utils.IError
	// Start 启动读写协程
	Start(conn net.Conn)
	// Stop 停止启动读写协程
	Stop() utils.IError
}

// AgentOption Agent代理选项
type AgentOption struct {
	//时间范围内允许的最大错误次数
	MaxBadPacketLimit int
	//错误计数的间隔
	MaxBadPacketInterval int64
	//连接成功回调
	OnConnected utils.Action
	//连接关闭的回调,Error为空正常关闭
	OnClosed ActionAgentErr
}

// AgentMaxBadPacketLimit 设置时间范围内允许的最大错误次数
func AgentMaxBadPacketLimit(maxBadPacketLimit int) ActionAgentOption {
	return func(o *AgentOption) {
		o.MaxBadPacketLimit = maxBadPacketLimit
	}
}

// AgentMaxBadPacketInterval 设置错误计数的间隔
func AgentMaxBadPacketInterval(maxBadPacketInterval int64) ActionAgentOption {
	return func(o *AgentOption) {
		o.MaxBadPacketInterval = maxBadPacketInterval
	}
}

// AgentConnected 连接成功的回调
func AgentConnected(onConnected utils.Action) ActionAgentOption {
	return func(o *AgentOption) {
		o.OnConnected = onConnected
	}
}

// AgentClosed 连接关闭的回调,Error为空正常关闭
func AgentClosed(onClosed ActionAgentErr) ActionAgentOption {
	return func(o *AgentOption) {
		o.OnClosed = onClosed
	}
}
