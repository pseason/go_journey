package intfc

import (
	"95eh.com/eg/utils"
	"github.com/gin-gonic/gin"
)

type (
	ActionContextToInt642Error   func(c *gin.Context) (uid, role int64, err error)
	ActionContextInt642Uint32Any func(c *gin.Context, tid, uid int64, code uint32, body interface{})
	ActionContextUint32Any       func(c *gin.Context, code uint32, any interface{})
	ActionContextUint32ErrCode   func(c *gin.Context, code uint32, ec utils.TErrCode)
)

type IMUserHttp interface {
	IModule
	// Addr 监听地址
	Addr() string
	// Codec 编解码器
	Codec() ICodec
	// Engine gin引擎
	Engine() *gin.Engine
	// SetIdParser 解析用户id
	SetIdParser(parser ActionContextToInt642Error)
	// BindProcessor 绑定相对路径和IMUserLogic协议号的映射,只支持GET,POST,PUT,DELETE
	BindProcessor(method, path string, code uint32)
	// ResOk 响应成功
	ResOk(c *gin.Context, code uint32, data interface{})
	// ResErr 响应失败
	ResErr(c *gin.Context, code uint32, ec utils.TErrCode)
}
