package intfc

import "95eh.com/eg/utils"

type IMCodec interface {
	IModule
	ICodec
}

// ICodec 消息包编解码器
type ICodec interface {
	// Marshal 编码
	Marshal(pkt interface{}) ([]byte, error)
	// Unmarshal 解码
	Unmarshal(data []byte, pkt interface{}) error
	// UnmarshalReq 解码请求包
	UnmarshalReq(code uint32, data []byte) (interface{}, error)
	// UnmarshalRes 解码响应包
	UnmarshalRes(code uint32, data []byte) (interface{}, error)
	// SpawnReq 生成一个请求包
	SpawnReq(code uint32) (interface{}, error)
	// SpawnRes 生成一个响应包
	SpawnRes(code uint32) (interface{}, error)
	// BindFac 绑定编解码函数
	BindFac(code uint32, reqFac, resFac utils.ToAny)
	// BindReqFac 绑定请求解码函数
	BindReqFac(code uint32, reqFac utils.ToAny)
	// BindResFac 绑定响应解码函数
	BindResFac(code uint32, resFac utils.ToAny)
}

type CodecFac struct {
	Req, Res utils.ToAny
}
