package intfc

import (
	"95eh.com/eg/utils"
)

type IMAsset interface {
	IModule
	// SetRelativePath 设置资源相对路径
	SetRelativePath(relative string) IMAsset
	// BindCoder 绑定编解码器
	BindCoder(path string, marshaller utils.ActionBytesToAnyErr, unmarshaller utils.ActionAnyToBytesErr)
	// Load 加载资源
	Load(paths ...string)
	// WatchAsset 监听资源
	WatchAsset(path string, once bool, action utils.ActionAny)
	// SaveBytes 保存二进制资源
	SaveBytes(path string, bytes []byte) error
	// SaveAsset 保存资源
	SaveAsset(path string, data interface{}) error
}
