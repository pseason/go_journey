package intfc

type (
	TListener uint8
)

const (
	TListenerNil TListener = iota
	TListenerTcp
	TListenerWebsocket
)

// IListener 监听器
type IListener interface {
	// Addr 监听地址
	Addr() string
	// Start 开始监听
	Start() error
	// Close 关闭监听
	Close()
}
