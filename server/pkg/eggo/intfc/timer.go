package intfc

import (
	"95eh.com/eg/utils"
)

type IMTimer interface {
	IModule
	// After 延迟执行回调
	After(ms int64, action utils.Action) (id int64)
	// AfterCh 延迟发送消息到通道
	AfterCh(ms int64) (int64, <-chan struct{})
	// Tick 心跳执行回调
	Tick(ms int64, total int32, action utils.ActionInt32) (id int64)
	// Remove 移除延迟回调或者心跳
	Remove(id int64) bool
	// Cron 定时执行回调
	Cron(spec string, action utils.Action) (int64, error)
	// RemoveCron 移除定时回调
	RemoveCron(id int64) bool
	// Stubborn 执行回调,如果失败延迟继续执行直到成功
	Stubborn(ms int64, action utils.ToBool, complete utils.Action)
	// PersistentAfter 持久化的延迟执行,配合BindSafeAction
	PersistentAfter(tag string, sec int64, body interface{}) (id int64)
	// RemovePersistentAfter 移除延迟执行
	RemovePersistentAfter(id int64) bool
	// PersistentCron 持久化的定时执行,配合BindSafeAction
	PersistentCron(tag, spec string, body interface{}) (id int64, err error)
	// RemovePersistentCron 移除持久化的定时执行
	RemovePersistentCron(tag string) bool
	// BindPersistentAction 绑定延迟/定时行为
	BindPersistentAction(tag string, fac utils.ToAny, action utils.ActionInt64Any)
}
