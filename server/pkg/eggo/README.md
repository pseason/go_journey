

## 介绍

`eg`Easy Game(Go)使用模块化/组件化的设计模式,内置常用模块与功能.便于快速开发游戏服务器.

你可以根据实际需要,使用不同模块组合/架构不同形式的架构.

### 架构示例

![image-20211108103231561](image-sample.png)

> Client->Login:
>
> - 登陆鉴权
> - 获取区服列表
> - 获取区服网关
>
> Client->Game:
>
> - 游戏逻辑
>
> Login->Consul:
>
> - 注册服务
>
> Login->Ops:
>
> - 状态统计数据
>
> Ops->Consul:
>
> - 注册服务
>
> Ops->Login:
>
> - 游戏公告
>
> Ops->Gate:
>
> - 设置运维状态
> - 区服公告
>
> Ops->AI:
>
> - 世界Boss
>
> Game->Consul:
>
> - 注册服务
>
> Game->Service:
>
> - 业务逻辑
>
> Game->Space:
>
> - 玩家行为
>
> Game->AI:
>
> - 生成场景怪物
>
> Game->MongoDB:
>
> - 日志
>
> Game->Redis:
>
> - 运行时高频读写数据
>
> Service->Consul:
>
> - 注册服务
>
> Service->Game:
>
> - 业务事件
>
> Service->GORM:
>
> - 操作区服数据库
>
> Service->MongoDB:
>
> - 日志
>
> Service->Redis:
>
> - 运行时高频读写数据
>
> Space->Consul:
>
> - 注册服务
>
> Space->Game:
>
> - 视野
> - 玩家/怪物事件
>
> Space->AI:
>
> - 视野
>
> - 玩家/怪物事件
>
> Space->MongoDB:
>
> - 日志
>
> Space->Redis:
>
> - 运行时高频读写数据
>
> AI->Consul:
>
> - 注册服务
>
> AI->Ops:
>
> - Boss等状态
>
> AI->Space:
>
> - AI行为
>
> AI->MongoDB:
>
> - 日志
>
> AI-Redis:
>
> - 运行时高频读写数据

### Game启动流程

使用`game.Start`启动框架,该方法执行流程如下:

1. 注入模块
2. 初始化已经注入的常用内置模块
3. 执行所有模块的`Init`方法
4. 执行所有模块的`BeforeStart`方法,该方法内部执行该模块使用`BeforeModuleStart`选项方法设置的回调
5. 执行所有模块的`Start`方法
6. 执行所有模块的`AfterStart`方法,该方法内部执行该模块使用`AfterModuleStart`选项方法设置的回调

### 开始使用

```go
package main

import (
	"95eh.com/eg/asset"
	"95eh.com/eg/codec"
	"95eh.com/eg/discovery"
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/timer"
	"95eh.com/eg/user"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
	"hm/pkg/game/common"
	"hm/pkg/misc"
	"time"
)

func main() {
	//加载配置文件
	conf := common.Conf()
	err := utils.StartLocalConf("", conf,
		"common", "common_dev",
		"region", "region_dev",
		"game", "game_dev")
	if err != nil {
		panic(err.Error())
	}
	//启动Consul
	err = utils.StartConsul(conf.Consul)
	if err != nil {
		panic(err.Error())
	}
	//启动Redis
	regionRedis := redis.NewClient(conf.Redis)
	globalRedis := redis.NewClient(conf.GlobalRedis)

	//服务编解码器使用Json
	serviceCodec := codec.NewJsonCodec()
	//注入协议号和消息结构体的映射
	misc.InitRegionCodec(serviceCodec)

	//用户编解码器使用ProtoBuff
	userCodec := codec.NewPbCodec()
	//注入协议号和消息结构体的映射
	initUserCodec(userCodec)

	//设置响应超时时间
	if conf.Debug {
		intfc.ResponseTimeoutDur = (time.Minute * 30).Milliseconds()
	}

	disConf := conf.Discovery
	app.Start(
		//添加日志记录
		log.NewMLogger(log.Loggers(log.NewConsole(intfc.TLogDebug))),
		//资源模块
		asset.NewMAsset(),
		//区服务事务
		service.NewMRegionTransaction(serviceCodec, regionRedis),
		//全局服务事务
		service.NewMGlobalTransaction(serviceCodec, globalRedis),
		//计时模块
		timer.NewMTimer(nil),
		//短连接模块
		user.NewMHttp(userCodec, user.HttpOptions(
			//设置Http服务监听的地址
			user.SetAddr(conf.Http.Addr)),
			//模块启动前初始化Http请求相对路径和消息协议号的映射
			intfc.BeforeModuleStart(initHttp)),
		//用户逻辑处理服务
		user.NewMService(userCodec,
			//模块启动前初始化协议号和消息处理器的映射
			intfc.BeforeModuleStart(initProcessor)),
		//用户长连接
		user.NewMGate(userCodec, user.GateOptions(
			user.GateAddr(conf.Gate.Addr),
			user.GateCap(conf.Gate.Cap),
			//发送给用户消息的缓存时间
			user.GateCacheDur(conf.Gate.CacheDur),
			//用户连接成功回调
			user.GateConnected(func(str string) {
				updateGateCount()
			}),
			//用户断开连接回调
			user.GateClosed(func(str string) {
				updateGateCount()
			}))),
		//协程模块
		worker.NewMWorker(),
		//服务发现模块
		discovery.NewMDiscovery(
			//节点类型
			misc.NodeGate,
			//节点id
			disConf.NodeId,
			//区服id
			disConf.RegionId,
			//编解码
			serviceCodec,
			//模块选项
			intfc.ModuleOptions(
				//模块启动前,监听登录节点
				intfc.AfterModuleStart(func() {
					app.Discovery().WatchNodes(misc.NodeLogin)
				})),
			//节点连接成功的回调
			discovery.SetOnNodeConnected(onNodeConnected)),
	)

	//进程终止前释放game中注入的模块
	utils.BeforeExit("stop gate", app.Dispose)
	utils.WaitExit()
}

```

### 功能模块

按职责划分的功能模块,可以按实际需求组合成应用节点,对外提供接口.

框架实现了部分常用模块.也可以基于不同的基础设施实现接口.

### 节点

使用需要的模块组合成节点,节点是一个独立的可执行文件.

一般节点都注入服务发现模块,通过该模块注册并监听其他节点.建立节点间的直连.

节点间可以相互发送请求,派发事件.

### 应用

### 服务发现

内置服务发现模块基于`Consule`.

`Start`方法中,开启`TCP`监听,并注册服务,注册信息如下:

- ID:`[区服ID]_[节点类型]_[节点ID]`

- Name:

  - 全局服:`[节点类型名]`
  - 区域服:`[区服名]_[节点类型名]`

- Address:`[监听IP]`

- Port:`[监听端口]`

- Meta:

  `game`:`[游戏名]`

  `regionId`:`[区服Id]`

  `nodeType`:`[节点类型]`

  `nodeId`:`[节点Id]`

  `weight`:`[节点权重]`

- Check:

  `TCP`:`[监听节点连接的地址]`

  `Timeout`:`1s`

  `Interval`:`5s`

  `DeregisterCriticalServiceAfter`: `10s`

### 服务

#### 请求

#### 事务

```mermaid
sequenceDiagram
	participant game as Game
	participant service as Service
	participant redis as Redis(String/Hash)
	participant msg as Redis(Stream)
	Note left of game: Try阶段
	game->>redis: 写入`Try`相关参数/状态/父级链路Id
	game->>service: 发送Try
	service->>game: Try响应
	Note left of game: Confirm/Cancel阶段
	game->>redis: 设置Try状态
	game->>msg: 生成Confirm/Cancel消息
	msg->>service: 消费Confirm/Cancel消息
	service->>msg: Ack确认消息
	service->>redis: 删除Try参数/状态/父级链路Id
```





#### 事件

### 空间

空间(`Space`)管理场景(`Scene`)和Actor(`Actor`).空间可理解为一方世界,场景则是一个地图,Actor是

空间及场景等公共数据必需在空间的`Worker`操作.使用`IMSpace.Worker().DoAction(action utils.Action) bool`接口进入空间`Worker`.然后在`action`中调用`IMSpace`的其他接口.

如果在模组启动时绑定相关的处理器,则不需要进入空间`Worker`(确保该过程中不会有并发导致数据竞争).

空间的职责如下:

- 生成/销毁Actor
- 管理Actor标签
- 管理Actor事件标记
- 派发事件给Actor
- 执行Actor行为
- 注入/处理Actor命令
- 注入/处理Actor状态
- 管理Actor心跳
- 生成/销毁场景
- Actor进入/退出场景
- 场景状态处理
- 执行场景行为

#### 生成/销毁Actor

#### 管理Actor标签

Actor可以有多个标签,一个标签也可以有Actor.

#### 管理Actor事件标记

#### 派发事件给Actor

#### 执行Actor行为

#### 注入/处理Actor命令

#### 注入/处理Actor状态

#### 管理Actor心跳

#### 生成/销毁场景

#### Actor进入/退出场景

#### 场景状态处理

#### 执行场景行为

Actor是空间/场景中的对象,可以按需求添加**Actor组件**组合成不同类型的对象.

每个节点连接的Socket都是一个逻辑协程

空间只有一个`Worker`(协程)

每个Actor都有一个专属`Worker`(协程),用于处理该`Actor`的数据,通过`IMSpace`的相关接口进入该`Worker`.

Actor生命周期如下:

```mermaid
sequenceDiagram
	participant logic as 逻辑
	participant space as 空间
	participant actor as Actor
	
    Note left of logic: 绑定Actor工厂
    logic-->>space: BindActorFac
    
    Note left of logic: 生成Actor
    logic->>space: SpawnActor
    space->>space: 工厂方法初始化Actor的id,类型,属性
    space->>space: 添加Actor
    space->>space: 添加Actor的tag
    space->>actor: Start
    
    Note left of logic: Actor进入场景
    logic->>space: GetScene
    space->>space: IScene.EnterActor
    space->>space: BeforeSceneAddActor
    space->>space: 场景添加Actor
    space->>actor: SetSceneId
    actor->>actor: SetSceneType
    actor->>actor: OnEnterScene
    actor->>actor: IMSpace.AfterActorEnterScene
    actor->>actor: complete方法回调
    
    opt 心跳
        Note left of logic: 添加心跳
        logic->>space: ActionActor
        space->>actor: AddTicker
        actor->>space: MarkActorTicker
        Note left of space: IMSpace.tickerActors
        loop 心跳循环
            space->>actor: Tick
            actor->>actor: IMSpace.TickActor
        end
        Note left of logic: 移除心跳
        logic->>space: ActionActor
        space->>actor: RemoveTicker
        actor->>space: UnmarkActorTicker
    end
    
    Note left of logic: Actor退出场景
    logic->>space: GetScene
    space->>space: IScene.ExitActor
    space->>space: BeforeSceneRemoveActor
    space->>space: 场景移除Actor
    space->>actor: IMSpace.AfterActorExitScene
    actor->>actor: OnExitScene
    actor->>actor: SetSceneId为0
    actor->>actor: SetSceneType为0
    actor->>actor: complete方法回调
    
    Note left of logic: 销毁Actor
    logic->>space: DestroyActor
    space->>space: IActor.Dispose
    space->>actor: worker.Dispose
    actor->>actor: IActorComponent.Dispose
    actor->>space: 移除Actor
    space->>space: 移除Actor的tag
    space->>space: 移除Actor的事件标记
```

### 日志

日志分为5个级别:`Debug`, `Info`,`Warn`,`Error`,`Fatal`.当日志记录器级别大于调用者的级别时,不会记录该日志.

日志分为2个类别:**系统日志**和**链路日志**

#### 系统日志

系统日志用于系统逻辑.与编码相关.

#### 链路日志

链路日志用于用户逻辑链路,与业务相关.便于业务调试/排查.
