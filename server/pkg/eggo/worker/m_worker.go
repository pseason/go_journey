package worker

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"runtime"
)

type (
	Option func(option *option)
	option struct {
		cap int64
	}
)

func SetCap(cap int64) Option {
	return func(option *option) {
		option.cap = cap
	}
}

func NewMWorker(opts ...Option) intfc.IMWorker {
	opt := &option{
		cap: int64(runtime.NumCPU()),
	}
	for _, o := range opts {
		o(opt)
	}

	w := &mWorker{
		option:       opt,
		worker:       StartBufferWorker(nil),
		idToWorker:   make(map[int64]intfc.IWorker, opt.cap),
		typeToJobTpl: make(map[intfc.TJob]utils.ActionAnyToAction),
	}

	for i := int64(0); i < opt.cap; i++ {
		w.idToWorker[i] = StartBufferWorker(nil)
	}
	return w
}

type mWorker struct {
	intfc.Module
	option       *option
	worker       intfc.IWorker
	idToWorker   map[int64]intfc.IWorker
	typeToJobTpl map[intfc.TJob]utils.ActionAnyToAction
}

func (M *mWorker) Type() intfc.TModule {
	return intfc.MWorker
}

func (M *mWorker) BindJob(t intfc.TJob, action utils.ActionAnyToAction) {
	M.typeToJobTpl[t] = action
}

func (M *mWorker) DoJob(id int64, t intfc.TJob, data interface{}) {
	M.DoAction(id, M.typeToJobTpl[t](data))
}

func (M *mWorker) DoJobs(id int64, data interface{}, types ...intfc.TJob) {
	actions := make([]utils.Action, len(types))
	for i, t := range types {
		actions[i] = func() {
			M.typeToJobTpl[t](data)
		}
	}
	M.DoActions(id, actions...)
}

func (M *mWorker) DoAction(id int64, action utils.Action) {
	M.worker.DoAction(func() {
		M.getWorker(id).DoAction(action)
	})
}

func (M *mWorker) DoActions(id int64, actions ...utils.Action) {
	M.worker.DoAction(func() {
		M.getWorker(id).DoActions(actions...)
	})
}

func (M *mWorker) getWorker(id int64) intfc.IWorker {
	return M.idToWorker[id%M.option.cap]
}
