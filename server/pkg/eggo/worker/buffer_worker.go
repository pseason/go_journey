package worker

import (
	"95eh.com/eg/utils"
	"fmt"
	"runtime/debug"
	"sync"
	"time"
)

func newBufferWorker() *bufferWorker {
	return &bufferWorker{
		enable:  utils.NewEnable(),
		closeCh: make(chan struct{}, 1),
		pauseCh: make(chan struct{}, 1),
		jobCh:   make(chan struct{}, 1),
		jobMtx:  &sync.Mutex{},
	}
}

type bufferWorker struct {
	enable        *utils.Enable
	closeCh       chan struct{}
	pauseCh       chan struct{}
	jobCh         chan struct{}
	cacheFirstJob *job
	cacheLastJob  *job
	actionJob     *job
	jobMtx        *sync.Mutex
}

func (w *bufferWorker) Stop() {
	w.enable.Action(func() {
		w.closeCh <- struct{}{}
	})
}

func (w *bufferWorker) DoAction(action utils.Action) utils.IError {
	return w.enable.Action(func() {
		w.jobMtx.Lock()
		w.pushJob(action)
		w.jobMtx.Unlock()
		w.pushSignal()
	})
}

func (w *bufferWorker) pushJob(action utils.Action) {
	j := _JobPool.Get().(*job)
	j.action = action
	j.next = nil
	if w.cacheFirstJob == nil {
		w.cacheFirstJob = j
	} else {
		w.cacheLastJob.next = j
	}
	w.cacheLastJob = j
}

func (w *bufferWorker) DoActions(actions ...utils.Action) utils.IError {
	return w.enable.Action(func() {
		w.jobMtx.Lock()
		for _, action := range actions {
			w.pushJob(action)
		}
		w.jobMtx.Unlock()
		w.pushSignal()
	})
}

func (w *bufferWorker) pushSignal() {
	select {
	case w.jobCh <- struct{}{}:
	default:
	}
}

func (w *bufferWorker) action() {
	w.jobMtx.Lock()
	w.actionJob = w.cacheFirstJob
	w.cacheFirstJob = nil
	w.cacheLastJob = nil
	w.jobMtx.Unlock()

	for w.actionJob != nil {
		j := w.actionJob
		j.action()
		w.actionJob = j.next
		_JobPool.Put(j)
	}
}

func (w *bufferWorker) recover() {
	w.jobMtx.Lock()
	defer w.jobMtx.Unlock()
	j := w.actionJob.next
	if j == nil {
		w.actionJob = nil
		w.cacheFirstJob = nil
		return
	}
	w.actionJob = nil
	fj := w.cacheFirstJob
	w.cacheFirstJob = j
	for j.next != nil {
		j = j.next
	}
	j.next = fj
}

func (w *bufferWorker) Pause() utils.IError {
	return w.DoAction(func() {
		<-w.pauseCh
	})
}

func (w *bufferWorker) Resume() utils.IError {
	return w.enable.Action(func() {
		w.pauseCh <- struct{}{}
	})
}

func (w *bufferWorker) start(over utils.Action) {
	go func() {
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("\033[31m !!!recover!!\n%s\n%s", err, debug.Stack())
				w.recover()
				w.pushSignal()
				w.start(over)
			} else {
				w.enable.Close(func() {
					close(w.closeCh)
					close(w.jobCh)
					close(w.pauseCh)
					over.Invoke()
				})
			}
		}()

		for {
			select {
			case <-w.closeCh:
				return
			case <-w.jobCh:
				w.action()
			}
		}
	}()
}

func StartBufferWorker(over utils.Action) *bufferWorker {
	w := newBufferWorker()
	w.start(over)
	return w
}

type tickBufferWorker struct {
	*bufferWorker
	lastTickTime int64
}

func (w *tickBufferWorker) startTick(duration time.Duration, tick utils.ActionInt642, over utils.Action) {
	go func() {
		tickCh := time.NewTicker(duration)
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("\033[31m !!!recover!!\n%s\n%s", err, debug.Stack())
				w.recover()
				w.startTick(duration, tick, over)
			} else {
				w.enable.Close(func() {
					tickCh.Stop()
					close(w.closeCh)
					close(w.jobCh)
					close(w.pauseCh)
					over.Invoke()
				})
			}
		}()

		w.lastTickTime = time.Now().UnixNano()
		for {
			select {
			case <-w.closeCh:
				return
			case now := <-tickCh.C:
				n := now.UnixNano()
				tick(n, (n-w.lastTickTime)/1e6)
				w.lastTickTime = n
			case <-w.jobCh:
				w.action()
			}
		}
	}()
}

func StartTickBufferWorker(duration time.Duration, tick utils.ActionInt642, complete utils.Action) *tickBufferWorker {
	w := &tickBufferWorker{
		bufferWorker: newBufferWorker(),
	}
	w.startTick(duration, tick, complete)
	return w
}

var (
	_JobPool = &sync.Pool{
		New: func() interface{} {
			return &job{}
		}}
)

type job struct {
	next   *job
	action utils.Action
}
