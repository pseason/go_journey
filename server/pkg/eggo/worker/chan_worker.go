package worker

import (
	"95eh.com/eg/utils"
	"fmt"
	"runtime/debug"
	"time"
)

func newChanWorker() *chanWorker {
	return &chanWorker{
		closeCh: make(chan struct{}, 1),
		pauseCh: make(chan struct{}, 1),
		jobCh:   make(chan utils.Action, 1),
		enable:  utils.NewEnable(),
	}
}

type chanWorker struct {
	enable  *utils.Enable
	closeCh chan struct{}
	pauseCh chan struct{}
	jobCh   chan utils.Action
}

func (w *chanWorker) Stop() {
	w.enable.Action(func() {
		w.closeCh <- struct{}{}
	})
}

func (w *chanWorker) DoAction(action utils.Action) utils.IError {
	return w.enable.Action(func() {
		w.jobCh <- action
	})
}

func (w *chanWorker) DoActions(actions ...utils.Action) utils.IError {
	return w.enable.Action(func() {
		for _, action := range actions {
			w.jobCh <- action
		}
	})
}

func (w *chanWorker) Pause() utils.IError {
	return w.DoAction(func() {
		<-w.pauseCh
	})
}

func (w *chanWorker) Resume() utils.IError {
	return w.enable.Action(func() {
		w.pauseCh <- struct{}{}
	})
}

func (w *chanWorker) start(over utils.Action) {
	go func() {
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("\033[31m !!!recover!!\n%s\n%s", err, debug.Stack())
				w.start(over)
			} else {
				w.enable.Close(func() {
					close(w.closeCh)
					close(w.jobCh)
					close(w.pauseCh)
					over.Invoke()
				})
			}
		}()

		for {
			select {
			case <-w.closeCh:
				return
			case action := <-w.jobCh:
				action()
			}
		}
	}()
}

func StartChanWorker(over utils.Action) *chanWorker {
	w := newChanWorker()
	w.start(over)
	return w
}

type tickChanWorker struct {
	*chanWorker
	lastTickTime int64
}

func (w *tickChanWorker) startTick(duration time.Duration, tick utils.ActionInt642, over utils.Action) {
	go func() {
		tickCh := time.NewTicker(duration)
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("\033[31m !!!recover!!\n%s\n%s", err, debug.Stack())
				w.startTick(duration, tick, over)
			} else {
				w.enable.Close(func() {
					tickCh.Stop()
					close(w.closeCh)
					close(w.jobCh)
					close(w.pauseCh)
					over.Invoke()
				})
			}
		}()

		w.lastTickTime = time.Now().UnixNano()
		for {
			select {
			case <-w.closeCh:
				return
			case now := <-tickCh.C:
				n := now.UnixNano()
				tick(n, (n-w.lastTickTime)/1e6)
				w.lastTickTime = n
			case action := <-w.jobCh:
				action()
			}
		}
	}()
}

func StartTickChanWorker(duration time.Duration, tick utils.ActionInt642, complete utils.Action) *tickChanWorker {
	w := &tickChanWorker{
		chanWorker: newChanWorker(),
	}
	w.startTick(duration, tick, complete)
	return w
}
