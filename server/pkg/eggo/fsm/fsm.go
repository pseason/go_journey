package fsm

import (
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
)

func NewFsm(m data.Map) *fsm {
	return &fsm{
		IData:  data.NewData(m),
		states: make(map[intfc.TState]intfc.IState),
	}
}

type fsm struct {
	data.IData
	currentState intfc.TState
	states       map[intfc.TState]intfc.IState
}

func (f *fsm) CurrentState() (state intfc.TState) {
	state = f.currentState
	return
}

func (f *fsm) AddState(state intfc.IState) {
	f.states[state.Type()] = state
}

func (f *fsm) ChangeState(t intfc.TState, data interface{}) {
	if f.currentState > 0 {
		f.states[f.currentState].Exit(data)
	}
	f.currentState = t
	if t > 0 {
		f.states[t].Enter(data)
	}
}

func (f *fsm) Dispose(data interface{}) {
	if f.currentState > 0 {
		f.states[f.currentState].Exit(data)
		f.currentState = 0
	}
}
