package log

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
)

func Loggers(loggers ...intfc.ILogger) []intfc.ILogger {
	return loggers
}

type (
	Option func(opt *option)
	option struct {
		callerSkip int
	}
)

func SetCallerSkip(callerSkip int) Option {
	return func(opt *option) {
		opt.callerSkip = callerSkip
	}
}

func NewMLogger(loggers []intfc.ILogger, moduleOpts []intfc.ModuleOption, options ...Option) intfc.IMLog {
	opt := &option{
		callerSkip: 3,
	}
	for _, o := range options {
		o(opt)
	}
	return &mLogger{
		IModule: intfc.NewModule(moduleOpts...),
		option:  opt,
		loggers: loggers,
	}
}

type mLogger struct {
	intfc.IModule
	option  *option
	loggers []intfc.ILogger
}

func (M *mLogger) Type() intfc.TModule {
	return intfc.MLog
}

func (M *mLogger) log(level intfc.TLoggerLevel, str string, params utils.M) {
	caller := GetCaller(M.option.callerSkip)
	timestamp := GetTimestamp()
	for _, logger := range M.loggers {
		if logger.Level() > level {
			continue
		}
		logger.Log(level, timestamp, caller, str, params)
	}
}

func (M *mLogger) Debug(str string, params utils.M) {
	M.log(intfc.TLogDebug, str, params)
}

func (M *mLogger) Info(str string, params utils.M) {
	M.log(intfc.TLogInfo, str, params)
}

func (M *mLogger) Warn(str string, params utils.M) {
	M.log(intfc.TLogWarn, str, params)
}

func (M *mLogger) Error(str string, params utils.M) {
	M.log(intfc.TLogError, str, params)
}

func (M *mLogger) Fatal(str string, params utils.M) {
	M.log(intfc.TLogFatal, str, params)
}

func (M *mLogger) TSign(pid int64, params utils.M) int64 {
	caller := GetCaller(2)
	timestamp := GetTimestamp()
	id := utils.GenSnowflakeRegionNodeId()
	for _, logger := range M.loggers {
		logger.Sign(pid, id, timestamp, caller, params)
	}
	return id
}

func (M *mLogger) trace(level intfc.TLoggerLevel, tid int64, msg string, stack []byte, params utils.M) {
	caller := GetCaller(3)
	timestamp := GetTimestamp()
	for _, logger := range M.loggers {
		if logger.Level() > level {
			continue
		}
		logger.Trace(level, tid, msg, timestamp, caller, stack, params)
	}
}

//func (M *mLogger) TDebugO(tid int64, t intfc.TTrace, params utils.M) {
//	M.trace(intfc.TLogDebug, tid, t.Name(), nil, params)
//}
//
//func (M *mLogger) TInfoO(tid int64, t intfc.TTrace, params utils.M) {
//	M.trace(intfc.TLogInfo, tid, t.Name(), nil, params)
//}
//
//func (M *mLogger) TWarnO(tid int64, t intfc.TTrace, params utils.M) {
//	M.trace(intfc.TLogWarn, tid, t.Name(), nil, params)
//}
//
//func (M *mLogger) TErrorO(tid int64, t intfc.TTrace, params utils.M) {
//	M.trace(intfc.TLogError, tid, t.Name(), nil, params)
//}
//
//func (M *mLogger) TFatalO(tid int64, t intfc.TTrace, params utils.M) {
//	M.trace(intfc.TLogFatal, tid, t.Name(), nil, params)
//}

func (M *mLogger) TDebug(tid int64, msg string, params utils.M) {
	M.trace(intfc.TLogDebug, tid, msg, nil, params)
}

func (M *mLogger) TInfo(tid int64, msg string, params utils.M) {
	M.trace(intfc.TLogInfo, tid, msg, nil, params)
}

func (M *mLogger) TWarn(tid int64, msg string, params utils.M) {
	M.trace(intfc.TLogWarn, tid, msg, nil, params)
}

func (M *mLogger) TError(tid int64, err utils.IError) {
	M.trace(intfc.TLogError, tid, err.Error(), err.Stack(), err.Params())
}

func (M *mLogger) TFatal(tid int64, err utils.IError) {
	M.trace(intfc.TLogFatal, tid, err.Error(), err.Stack(), err.Params())
}
