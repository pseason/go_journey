package log

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"fmt"
	"runtime"
	"runtime/debug"
	"strconv"
	"time"
)

const (
	ColorRed      = "\033[31m"
	ColorGreen    = "\033[32m"
	ColorYellow   = "\033[33m"
	ColorBlue     = "\033[34m"
	ColorPurple   = "\033[35m"
	ColorWhite    = "\033[37m"
	ColorHiRed    = "\033[91m"
	ColorHiGreen  = "\033[92m"
	ColorHiYellow = "\033[93m"
	ColorHiBlue   = "\033[94m"
	ColorHiPurple = "\033[95m"
	ColorHiWhite  = "\033[97m"
	ColorReset    = "\033[0m"
)

func Debug(msg string, m utils.M) {
	caller := GetCaller(2)
	timestamp := GetTimestamp()
	fmt.Println(ColorWhite + timestamp + " " + msg + m.Json() + ColorReset + " " + caller)
}

func Info(msg string, m utils.M) {
	caller := GetCaller(2)
	timestamp := GetTimestamp()
	fmt.Println(ColorGreen + timestamp + " " + msg + m.Json() + ColorReset + " " + caller)
}

func Warn(msg string, m utils.M) {
	caller := GetCaller(2)
	timestamp := GetTimestamp()
	fmt.Println(ColorYellow + timestamp + " " + msg + m.Json() + ColorReset + " " + caller + "\n" + string(debug.Stack()))
}

func Error(msg string, m utils.M) {
	caller := GetCaller(2)
	timestamp := GetTimestamp()
	fmt.Println(ColorRed + timestamp + " " + msg + m.Json() + ColorReset + " " + caller + "\n" + string(debug.Stack()))
}

func Fatal(msg string, m utils.M) {
	timestamp := GetTimestamp()
	panic(ColorPurple + timestamp + " " + msg + m.Json() + ColorReset)
}

func GetCaller(skip int) string {
	_, file, line, ok := runtime.Caller(skip)
	if !ok {
		return ""
	}
	return file + ":" + strconv.Itoa(line)
}

func GetTimestamp() string {
	return time.Now().Format("2006-01-02 15:04:05.999")
}

type BaseLogger struct {
	typ   intfc.TLogger
	level intfc.TLoggerLevel
}

func NewBaseLogger(t intfc.TLogger, level intfc.TLoggerLevel) *BaseLogger {
	return &BaseLogger{
		typ:   t,
		level: level,
	}
}

func (b *BaseLogger) Type() intfc.TLogger {
	return b.typ
}

func (b *BaseLogger) Level() intfc.TLoggerLevel {
	return b.level
}

func (b *BaseLogger) Log(level intfc.TLoggerLevel, timestamp, caller, msg string, params utils.M) {
	//TODO implement me
	panic("implement me")
}

func (b *BaseLogger) Sign(parent, id int64, timestamp, caller string, params utils.M) {
	//TODO implement me
	panic("implement me")
}

func (b *BaseLogger) Trace(level intfc.TLoggerLevel, timestamp, caller string, id int64, t intfc.TTrace, params utils.M) {
	//TODO implement me
	panic("implement me")
}
