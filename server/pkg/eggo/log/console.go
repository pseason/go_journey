package log

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"runtime/debug"
)

func NewConsole(level intfc.TLoggerLevel) *console {
	return &console{
		BaseLogger: NewBaseLogger(intfc.TLoggerStdout, level),
	}
}

type console struct {
	*BaseLogger
}

func (s *console) Log(level intfc.TLoggerLevel, timestamp, caller, str string, params utils.M) {
	ps, _ := jsoniter.MarshalToString(params)
	switch level {
	case intfc.TLogDebug:
		fmt.Println(ColorHiWhite + "[D]" + timestamp + " " + str + " " + ps + ColorReset + " " + caller)
	case intfc.TLogInfo:
		fmt.Println(ColorHiGreen + "[I]" + timestamp + " " + str + " " + ps + ColorReset + " " + caller)
	case intfc.TLogWarn:
		fmt.Println(ColorHiYellow + "[W]" + timestamp + " " + str + " " + ps + ColorReset + " " + caller)
	case intfc.TLogError:
		fmt.Println(ColorHiRed + "[E]" + timestamp + " " + str + " " + ps + ColorReset + " " + caller + "\n" + string(debug.Stack()))
	case intfc.TLogFatal:
		fmt.Println(ColorHiPurple + "[F]" + timestamp + " " + str + " " + ps + ColorReset + " " + caller + "\n" + string(debug.Stack()))
	}
}

func (s *console) Sign(parent, id int64, timestamp, caller string, params utils.M) {
	ps, _ := jsoniter.MarshalToString(params)
	str := fmt.Sprintf(ColorHiBlue+"[TS]"+ColorReset+timestamp+" %d<-%d %s "+caller, parent, id, ps)
	fmt.Println(str)
}

func (s *console) Trace(level intfc.TLoggerLevel, id int64, msg, timestamp, caller string, stack []byte, params utils.M) {
	ps, _ := jsoniter.MarshalToString(params)
	switch level {
	case intfc.TLogDebug:
		fmt.Printf(ColorWhite+"[TD]"+ColorReset+timestamp+" tid:%d msg:\"%s\" params:%s "+caller+"\n", id, msg, ps)
	case intfc.TLogInfo:
		fmt.Printf(ColorGreen+"[TI]"+ColorReset+timestamp+" tid:%d msg:\"%s\" params:%s "+caller+"\n", id, msg, ps)
	case intfc.TLogWarn:
		fmt.Printf(ColorYellow+"[TW]"+ColorReset+timestamp+" tid:%d msg:\"%s\" params:%s "+caller+"\n", id, msg, ps)
	case intfc.TLogError:
		fmt.Printf(ColorRed+"[TE]"+ColorReset+timestamp+" tid:%d msg:\"%s\" params:%s "+caller+"\n%s\n", id, msg, ps, stack)
	case intfc.TLogFatal:
		fmt.Printf(ColorPurple+"[TF]"+ColorReset+timestamp+" tid:%d msg:\"%s\" params:%s "+caller+"\n%s\n", id, msg, ps, stack)
	}
}
