package asset

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
	"errors"
	"github.com/fsnotify/fsnotify"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

func NewMAsset(moduleOpts ...intfc.ModuleOption) intfc.IMAsset {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal("watch asset failed", utils.M{
			"error": err.Error(),
		})
		return nil
	}
	m := &mAsset{
		IModule:            intfc.NewModule(moduleOpts...),
		pathToListener:     make(map[string][]utils.ActionAny),
		pathToOnceListener: make(map[string][]utils.ActionAny),
		pathToDecoder:      make(map[string]utils.ActionBytesToAnyErr),
		pathToEncoder:      make(map[string]utils.ActionAnyToBytesErr),
		watcher:            watcher,
	}

	newFiles := make(map[string]struct{})
	closeCh := make(chan struct{})
	m.worker = worker.StartTickChanWorker(time.Second, func(n, d int64) {
		for path := range newFiles {
			err := m.loadFile(path)
			if err != nil {
				continue
			}
			delete(newFiles, path)
		}
	}, func() {
		watcher.Close()
		closeCh <- struct{}{}
	})

	go func() {
		for {
			select {
			case event := <-watcher.Events:
				if event.Op&fsnotify.Write == fsnotify.Write {
					path := m.getRelativelyPath(event.Name)
					m.worker.DoAction(func() {
						newFiles[path] = struct{}{}
					})
				}
			case err := <-watcher.Errors:
				if err != nil {
					log.Info("watch error", utils.M{
						"error": err.Error(),
					})
				}
			case <-closeCh:
				return
			}
		}
	}()

	return m
}

type mAsset struct {
	intfc.IModule
	worker             intfc.IWorker
	pathToListener     map[string][]utils.ActionAny
	pathToOnceListener map[string][]utils.ActionAny
	pathToDecoder      map[string]utils.ActionBytesToAnyErr
	pathToEncoder      map[string]utils.ActionAnyToBytesErr
	root               string
	watcher            *fsnotify.Watcher
}

func (M *mAsset) Type() intfc.TModule {
	return intfc.MAsset
}

func (M *mAsset) Dispose() {
	if M.worker == nil {
		return
	}
	M.worker.Stop()
}

func (M *mAsset) SetRelativePath(root string) intfc.IMAsset {
	M.root = utils.ExeDir() + root
	return M
}

func (M *mAsset) BindCoder(path string, decoder utils.ActionBytesToAnyErr, encoder utils.ActionAnyToBytesErr) {
	M.pathToDecoder[path] = decoder
	M.pathToEncoder[path] = encoder
}

func (M *mAsset) Load(paths ...string) {
	pm := make(map[string]struct{}, len(M.pathToListener)+len(M.pathToOnceListener))
	for path := range M.pathToListener {
		pm[path] = struct{}{}
	}
	for path := range M.pathToOnceListener {
		pm[path] = struct{}{}
	}
	if len(paths) == 0 {
		for path := range pm {
			M.loadFile(path)
		}
		return
	}

	for _, path := range paths {
		if _, ok := pm[path]; !ok {
			app.Log().Warn("not exit listener", utils.M{
				"path": path,
			})
			continue
		}
		M.loadFile(path)
	}
}

func (M *mAsset) WatchAsset(path string, once bool, action utils.ActionAny) {
	M.watcher.Add(M.getFullPath(path))
	if once {
		M.addListener(path, action, M.pathToOnceListener)
	} else {
		M.addListener(path, action, M.pathToListener)
	}
}

func (M *mAsset) addListener(path string, action utils.ActionAny, m map[string][]utils.ActionAny) {
	listeners, ok := m[path]
	if ok {
		m[path] = append(listeners, action)
	} else {
		m[path] = []utils.ActionAny{action}
	}
}

func (M *mAsset) callWatcher(path string, data interface{}) {
	listeners, ok := M.pathToListener[path]
	if ok {
		for _, listener := range listeners {
			listener(data)
		}
	}
	listeners, ok = M.pathToOnceListener[path]
	if ok {
		for _, listener := range listeners {
			listener(data)
		}
		delete(M.pathToOnceListener, path)
	}
}

func (M *mAsset) SaveBytes(path string, bytes []byte) error {
	decoder, ok := M.pathToDecoder[path]
	if ok {
		o, err := decoder(bytes)
		if err != nil {
			return err
		}
		M.callWatcher(path, o)
	} else {
		M.callWatcher(path, bytes)
	}
	os.WriteFile(path, bytes, os.ModePerm)
	return nil
}

func (M *mAsset) SaveAsset(path string, data interface{}) error {
	encoder, ok := M.pathToEncoder[path]
	if !ok {
		return errors.New("not exist encoder")
	}
	bytes, err := encoder(data)
	if err != nil {
		return err
	}
	M.callWatcher(path, data)
	os.WriteFile(path, bytes, os.ModePerm)
	return nil
}

func (M *mAsset) getRelativelyPath(path string) string {
	return filepath.ToSlash(path)[len(M.root):]
}

func (M *mAsset) getFullPath(path string) string {
	return M.root + path
}

func (M *mAsset) loadFile(path string) error {
	bytes, err := ioutil.ReadFile(M.getFullPath(path))
	if err != nil {
		log.Error("load asset failed", utils.M{
			"error": err.Error(),
		})
		return err
	}
	log.Info("load asset complete", utils.M{
		"path": path,
	})
	M.SaveBytes(path, bytes)
	return nil
}
