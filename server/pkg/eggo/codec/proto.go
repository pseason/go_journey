package codec

import (
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
)

func NewPbCodec() intfc.ICodec {
	return NewCodec(data.PbMarshal, data.PbUnmarshal)
}
