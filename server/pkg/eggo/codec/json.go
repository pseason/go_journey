package codec

import (
	"95eh.com/eg/intfc"
	jsoniter "github.com/json-iterator/go"
)

func NewJsonCodec() intfc.ICodec {
	return NewCodec(jsoniter.Marshal, jsoniter.Unmarshal)
}
