package codec

import (
	"95eh.com/eg/intfc"
	"github.com/vmihailenco/msgpack/v5"
)

func NewMsgPackCodec() intfc.ICodec {
	return NewCodec(msgpack.Marshal, msgpack.Unmarshal)
}
