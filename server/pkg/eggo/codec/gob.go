package codec

import (
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
)

func NewGobCodec() intfc.ICodec {
	return NewCodec(data.GobMarshal, data.GobUnmarshal)
}
