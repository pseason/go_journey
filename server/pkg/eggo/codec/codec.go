package codec

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
)

func NewCodec(encode utils.ActionAnyToBytesErr, decode utils.ActionBytesAnyToErr) intfc.ICodec {
	return &codec{
		encode: encode,
		decode: decode,
		reqFac: make(map[uint32]utils.ToAny),
		resFac: make(map[uint32]utils.ToAny),
	}
}

type codec struct {
	encode utils.ActionAnyToBytesErr
	decode utils.ActionBytesAnyToErr
	reqFac map[uint32]utils.ToAny
	resFac map[uint32]utils.ToAny
}

func (c *codec) SpawnReq(code uint32) (interface{}, error) {
	fac, ok := c.reqFac[code]
	if !ok {
		return nil, _NotExistCodeErr
	}
	return fac(), nil
}

func (c *codec) SpawnRes(code uint32) (interface{}, error) {
	fac, ok := c.resFac[code]
	if !ok {
		return nil, _NotExistCodeErr
	}
	return fac(), nil
}

func (c *codec) Marshal(pkt interface{}) ([]byte, error) {
	if pkt == nil {
		return nil, nil
	}
	return c.encode(pkt)
}

func (c *codec) Unmarshal(data []byte, pkt interface{}) error {
	if len(data) == 0 {
		return nil
	}
	return c.decode(data, pkt)
}

func (c *codec) UnmarshalReq(code uint32, data []byte) (interface{}, error) {
	fac, ok := c.reqFac[code]
	if !ok {
		return nil, _NotExistCodeErr
	}
	pkt := fac()
	if len(data) == 0 {
		return pkt, nil
	}
	err := c.decode(data, pkt)
	if err != nil {
		return nil, err
	}
	return pkt, nil
}

func (c *codec) UnmarshalRes(code uint32, data []byte) (interface{}, error) {
	fac, ok := c.resFac[code]
	if !ok {
		return nil, _NotExistCodeErr
	}
	pkt := fac()
	if len(data) == 0 {
		return pkt, nil
	}
	err := c.decode(data, pkt)
	if err != nil {
		return nil, err
	}
	return pkt, nil
}

func (c *codec) BindFac(code uint32, reqFac, resFac utils.ToAny) {
	if reqFac != nil {
		c.BindReqFac(code, reqFac)
	}
	if resFac != nil {
		c.BindResFac(code, resFac)
	}
}

func (c *codec) BindReqFac(code uint32, reqFac utils.ToAny) {
	c.reqFac[code] = reqFac
}

func (c *codec) BindResFac(code uint32, resFac utils.ToAny) {
	c.resFac[code] = resFac
}
