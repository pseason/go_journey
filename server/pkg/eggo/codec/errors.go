package codec

import "errors"

var (
	_NotExistCodeErr = errors.New("not exist code")
)

func NotExistCodeErr() error {
	return _NotExistCodeErr
}
