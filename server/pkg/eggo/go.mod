module 95eh.com/eg

go 1.16

require (
	github.com/RussellLuo/timingwheel v0.0.0-20201029015908-64de9d088c74
	github.com/StackExchange/wmi v1.2.1 // indirect
	github.com/bwmarrin/snowflake v0.3.0
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gin-gonic/gin v1.7.4
	github.com/go-redis/redis/v8 v8.11.4
	github.com/go-redsync/redsync/v4 v4.4.2
	github.com/gogo/protobuf v1.3.2
	github.com/hashicorp/consul/api v1.10.1
	github.com/json-iterator/go v1.1.11
	github.com/mitchellh/mapstructure v1.4.2
	github.com/orcaman/concurrent-map v0.0.0-20210501183033-44dafcb38ecc
	github.com/panjf2000/ants/v2 v2.4.3
	github.com/robfig/cron/v3 v3.0.1
	github.com/shirou/gopsutil v3.21.7+incompatible
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
	github.com/tklauser/go-sysconf v0.3.8 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.4
	golang.org/x/net v0.0.0-20210503060351-7fd8e65b6420
)
