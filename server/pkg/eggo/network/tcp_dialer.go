package network

import (
	"95eh.com/eg/app"
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"net"
)

type tcpDialer struct {
	name     string
	addr     string
	agent    intfc.IAgent
	receiver intfc.ActionAgentBytesToErrCode
	option   *intfc.DialerOption
	enable   *utils.Enable
}

func NewTcpDialer(name, addr string, receiver intfc.ActionAgentBytesToErrCode, options ...intfc.ActionDialerOption) *tcpDialer {
	d := &tcpDialer{
		name:     name,
		addr:     addr,
		receiver: receiver,
		enable:   utils.NewEnable(),
		option:   &intfc.DialerOption{},
	}
	for _, opt := range options {
		opt(d.option)
	}
	return d
}

func (d *tcpDialer) Name() string {
	return d.name
}

func (d *tcpDialer) Addr() string {
	return d.addr
}

func (d *tcpDialer) Enable() bool {
	return d.enable.Enable()
}

func (d *tcpDialer) Connect(m data.Map, options ...intfc.ActionAgentOption) error {
	app.Log().Info("dialer tcp", utils.M{
		"addr": d.addr,
	})
	addr, err := net.ResolveTCPAddr("tcp", d.addr)
	if err != nil {
		return err
	}

	c, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		return err
	}

	d.agent = NewTcpAgent(d.name, d.receiver, m, options...)
	d.agent.Start(c)
	if d.option.OnConnected != nil {
		d.option.OnConnected(d)
	}
	return nil
}

func (d *tcpDialer) Agent() intfc.IAgent {
	return d.agent
}

func (d *tcpDialer) Close() {
	d.enable.Close(func() {
		if d.agent == nil {
			return
		}
		if d.option.OnClosed != nil {
			d.option.OnClosed(d)
		}
		d.agent.Stop()
	})
}
