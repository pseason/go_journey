package network

import (
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"math"
	"net"
	"time"
)

const (
	_MaxShrinkCount  = 1 << 8
	_MaxBufferCount  = 1 << 19
	_MinCap          = 1 << 11
	_MaxPacketLength = math.MaxUint16 - 2
)

func newAgent(addr string, receiver intfc.ActionAgentBytesToErrCode, m data.Map, options []intfc.ActionAgentOption) agent {
	o := &intfc.AgentOption{}
	for _, action := range options {
		action(o)
	}
	cap := uint32(_MinCap)
	return agent{
		data:        data.NewData(m),
		addr:        addr,
		option:      o,
		buffer:      make([]byte, cap),
		cacheBuffer: make([]byte, cap),
		bufferCap:   cap,
		enable:      utils.NewEnable(),
		processor:   receiver,
	}
}

type agent struct {
	option           *intfc.AgentOption
	data             data.IData
	addr             string
	lastErrTime      int64
	errCount         int
	buffer           []byte
	cacheBuffer      []byte
	count            uint32
	headLen          uint32
	bufferCap        uint32
	cacheCount       uint32
	cacheBufferCount uint32
	popIndex         uint32
	pushIndex        uint32
	shrinkCounter    int //缩容计数
	writeChan        chan []byte
	readCloseChan    chan struct{}
	writeCloseChan   chan struct{}
	actionChan       chan utils.Action
	enable           *utils.Enable
	processor        intfc.ActionAgentBytesToErrCode
}

func (a *agent) Data() data.IData {
	return a.data
}

func (a *agent) Conn() net.Conn {
	panic("implement me")
}

func (a *agent) Start(conn net.Conn) {
	panic("implement me")
}

func (a *agent) Stop() utils.IError {
	return a.enable.Action(func() {
		a.readCloseChan <- struct{}{}
		a.writeCloseChan <- struct{}{}
	})
}

func (a *agent) Addr() string {
	return a.addr
}

func (a *agent) markError() bool {
	if a.option.MaxBadPacketLimit == 0 {
		return false
	}
	now := time.Now().Unix()
	if now-a.lastErrTime > a.option.MaxBadPacketInterval {
		a.errCount = 1
		return false
	}
	a.lastErrTime = now
	a.errCount++
	if a.errCount < a.option.MaxBadPacketLimit {
		return false
	}
	return true
}

func (a *agent) resetBuffer(cap uint32) {
	buf := data.SpawnBytesWithLen(cap)
	if a.count > 0 {
		if a.pushIndex > a.popIndex {
			copy(buf, a.buffer[a.popIndex:a.pushIndex])
		} else {
			copy(buf, a.buffer[a.popIndex:a.bufferCap])
			copy(buf[a.bufferCap-a.popIndex:], a.buffer[:a.pushIndex])
		}
	}
	a.pushIndex = a.count
	a.popIndex = 0
	data.RecycleBytes(a.cacheBuffer)
	data.RecycleBytes(a.buffer)
	a.bufferCap = cap
	a.cacheBuffer = data.SpawnBytesWithLen(cap)
	a.buffer = buf
	a.shrinkCounter = 0
}

//粘包/拆包
func (a *agent) receive(bytes []byte, len uint32) {
	total := a.count + len
	if total >= a.bufferCap {
		c := utils.NextPowerOfTwo(total)
		if c >= _MaxBufferCount {
			panic("receive too much")
		}
		a.resetBuffer(c)
	}
	i := a.pushIndex + len
	if i <= a.bufferCap {
		copy(a.buffer[a.pushIndex:], bytes)
		a.pushIndex = i
	} else {
		copy(a.buffer[a.pushIndex:a.bufferCap], bytes)
		j := a.bufferCap - a.pushIndex
		copy(a.buffer, bytes[j:len])
		a.pushIndex = len - j
	}

	a.count = total
	for a.count > 2 {
		if a.headLen == 0 {
			cache := a.copyBytesToCache(2)
			a.headLen = uint32(cache[0])<<8 | uint32(cache[1])
			if a.headLen == 0 {
				log.Fatal("bad head length", nil)
				if a.markError() {
					a.Stop()
					return
				}
			}
		}
		if a.headLen == 2 {
			a.headLen = 0
			continue
		}
		l := a.headLen - 2
		if a.count < l {
			break
		}
		cache := a.copyBytesToCache(l)
		ec := a.processor(a, cache)
		a.headLen = 0
		if ec == 0 {
			continue
		}
		log.Error("process message failed", utils.M{
			"addr":       a.addr,
			"error code": ec,
		})
		if a.markError() {
			a.Stop()
			return
		}
	}

	//缩容
	if _MinCap == a.bufferCap {
		return
	}
	halfCap := a.bufferCap >> 1
	if a.count > halfCap {
		a.shrinkCounter = 0
		return
	}
	a.shrinkCounter++
	if a.shrinkCounter < _MaxShrinkCount {
		return
	}
	a.resetBuffer(halfCap)
}

func (a *agent) copyBytesToCache(l uint32) []byte {
	p := a.popIndex + l
	if p < a.bufferCap {
		copy(a.cacheBuffer, a.buffer[a.popIndex:p])
		a.popIndex = p
	} else {
		p -= a.bufferCap
		copy(a.cacheBuffer, a.buffer[a.popIndex:a.bufferCap])
		copy(a.cacheBuffer[a.bufferCap-a.popIndex:], a.buffer[:p])
		a.popIndex = p
	}
	a.count -= l
	return a.cacheBuffer[:l]
}

func (a *agent) Send(bytes []byte) utils.IError {
	l := len(bytes)
	if l == 0 {
		return utils.NewError("empty packet", nil)
	}
	if l >= _MaxPacketLength {
		return utils.NewError("too long", nil)
	}
	return a.enable.Action(func() {
		a.writeChan <- bytes
	})
}

func (a *agent) pack(bytes []byte) (all []byte, l int) {
	l = len(bytes) + 2
	all = data.SpawnBytesWithLen(uint32(l))
	all[0] = byte(l >> 8)
	all[1] = byte(l)
	copy(all[2:], bytes)
	return
}
