package network

import (
	"95eh.com/eg/app"
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"fmt"
	"net"
	"time"
)

func NewTcpAgent(addr string, receiver intfc.ActionAgentBytesToErrCode, m data.Map, options ...intfc.ActionAgentOption) intfc.IAgent {
	return &tcpAgent{
		agent: newAgent(addr, receiver, m, options),
	}
}

type tcpAgent struct {
	agent
	conn net.Conn
}

func (a *tcpAgent) Conn() net.Conn {
	return a.conn
}

func (a *tcpAgent) Start(conn net.Conn) {
	a.conn = conn
	a.enable.Reset()
	a.writeCloseChan = make(chan struct{}, 1)
	a.readCloseChan = make(chan struct{}, 1)
	a.writeChan = make(chan []byte, 1)
	a.actionChan = make(chan utils.Action)
	go a.write()
	go a.read()
	if a.option.OnConnected != nil {
		a.option.OnConnected()
	}
}

func (a *tcpAgent) close(err error) {
	a.enable.Close(func() {
		close(a.readCloseChan)
		close(a.writeCloseChan)
		close(a.writeChan)
		close(a.actionChan)
		_ = a.conn.Close()
		if a.option.OnClosed != nil {
			a.option.OnClosed(a, err)
		}
	})
}

func (a *tcpAgent) read() {
	var (
		newData [2048]byte
		err     error
		newLen  int
	)
	defer func() {
		r := recover()
		if r != nil {
			app.Log().Fatal("tcp agent recover", utils.M{
				"remote addr": a.conn.RemoteAddr().String(),
				"recover":     fmt.Sprintf("%s", r),
			})
			a.read()
			return
		}
		a.close(err)
	}()

	deadlineDur := time.Millisecond * time.Duration(intfc.TcpDeadlineDur)
	for {
		select {
		case <-a.readCloseChan:
			return
		case action, ok := <-a.actionChan:
			if ok {
				action()
			}
		default:
			if deadlineDur > 0 {
				if err = a.conn.SetReadDeadline(time.Now().Add(deadlineDur)); err != nil {
					return
				}
			}
			newLen, err = a.conn.Read(newData[:])
			if err != nil {
				return
			}
			a.receive(newData[:], uint32(newLen))
		}
	}
}

func (a *tcpAgent) write() {
	var (
		err       error
		timer     = time.NewTicker(time.Millisecond * time.Duration(intfc.TcpDeadlineDur) / 2)
		emptyPack []byte
	)
	defer func() {
		timer.Stop()
		a.close(err)
	}()

	for {
		select {
		case <-a.writeCloseChan:
			return
		case bytes := <-a.writeChan:
			all, l := a.pack(bytes)
			_, err = a.conn.Write(all[:l])
			data.RecycleBytes(bytes)
			data.RecycleBytes(all)
			if err != nil {
				return
			}
		case <-timer.C:
			all, l := a.pack(emptyPack)
			_, err = a.conn.Write(all[:l])
			if err != nil {
				return
			}
		}
	}
}
