package network

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"golang.org/x/net/websocket"
	"net"
	"net/http"
	"strings"
)

func NewWebsocketListener(addr string, onConn func(conn net.Conn)) *websocketListener {
	return &websocketListener{
		addr:   addr,
		onConn: onConn,
	}
}

type websocketListener struct {
	addr   string
	server *http.Server
	onConn func(conn net.Conn)
}

func (l *websocketListener) Addr() string {
	return l.addr
}

type websocketConn struct {
	*websocket.Conn
	closeChan chan struct{}
}

func (my *websocketConn) Close() error {
	close(my.closeChan)
	return nil
}

func (l *websocketListener) Start() error {
	serMux := http.NewServeMux()
	index := strings.Index(l.addr, "/")
	addr := l.addr[:index]
	pattern := l.addr[index:]
	serMux.Handle(pattern, websocket.Handler(func(conn *websocket.Conn) {
		ch := make(chan struct{})
		l.onConn(&websocketConn{Conn: conn, closeChan: ch})
		<-ch
	}))

	l.server = &http.Server{
		Addr:    addr,
		Handler: serMux,
	}

	go func() {
		err := l.server.ListenAndServe()
		if err != nil {
			app.Log().Error("listen websocket failed", utils.M{
				"error": err.Error(),
			})
			return
		}
	}()
	return nil
}
func (l *websocketListener) Close() {
	l.server.Close()
}
