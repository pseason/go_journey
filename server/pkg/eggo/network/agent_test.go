package network

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"testing"
)

type TestMsg struct {
	Name  string
	Age   int
	Email string
	Phone string
	City  string
}

func BenchmarkAgentReceive(b *testing.B) {
	message := &TestMsg{
		Name:  "95eh",
		Age:   36,
		Email: "eh95@qq.com",
		Phone: "17671600008",
		City:  "wuhan",
	}
	agent := newAgent(":8787", func(agent intfc.IAgent, bytes []byte) utils.TErrCode {
		var msg TestMsg
		err := jsoniter.Unmarshal(bytes, &msg)
		//err := data.GobDecode(bytes, &msg)
		if err != nil {
			return 1
		}
		assert.EqualValues(b, &msg, message)
		return 0
	}, nil, nil)
	bytes, err := jsoniter.Marshal(message)
	//bytes, err := data.GobEncode(message)
	if err != nil {
		b.Fatal(err.Error())
		return
	}
	var l int
	bytes, l = agent.pack(bytes)
	bytes = bytes[:l]
	r := 30
	bl := l * r
	bigBytes := make([]byte, bl)
	for i := 0; i < r; i++ {
		copy(bigBytes[i*l:], bytes)
	}
	agent.receive(bigBytes, uint32(bl))
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		//for i := 0; i < 10; i++ {
		agent.receive(bytes, uint32(l))
	}
}
