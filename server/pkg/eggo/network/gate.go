package network

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"sync"
)

func NewGate(cap int) *gate {
	return &gate{
		mtx:         &sync.RWMutex{},
		addrToAgent: make(map[string]intfc.IAgent, cap),
	}
}

type gate struct {
	mtx         *sync.RWMutex
	addrToAgent map[string]intfc.IAgent
}

func (g *gate) Len() (v int) {
	g.mtx.RLock()
	v = len(g.addrToAgent)
	g.mtx.RUnlock()
	return
}

func (g *gate) Add(agent intfc.IAgent) {
	g.mtx.Lock()
	g.addrToAgent[agent.Addr()] = agent
	g.mtx.Unlock()
}

func (g *gate) Remove(addr string) {
	g.mtx.Lock()
	delete(g.addrToAgent, addr)
	g.mtx.Unlock()
}

func (g *gate) Send(addr string, bytes []byte) utils.IError {
	g.mtx.RLock()
	a, ok := g.addrToAgent[addr]
	g.mtx.RUnlock()
	if !ok {
		return utils.NewError("not exist agent", utils.M{
			"addr": addr,
		})
	}
	return a.Send(bytes)
}

func (g *gate) SendAll(bytes []byte) {
	g.mtx.RLock()
	for addr, a := range g.addrToAgent {
		if err := a.Send(bytes); err != nil {
			app.Log().Error("send failed", utils.M{
				"addr": addr,
			})
		}
	}
	g.mtx.RUnlock()
}

func (g *gate) SendMulti(addrs []string, bytes []byte) {
	g.mtx.RLock()
	for _, addr := range addrs {
		a, ok := g.addrToAgent[addr]
		if !ok {
			app.Log().Error("not exist agent", utils.M{
				"addr": addr,
			})
			continue
		}
		if err := a.Send(bytes); err != nil {
			app.Log().Error("send failed", utils.M{
				"addr": addr,
			})
		}
	}
	g.mtx.RUnlock()
}

func (g *gate) SendExclude(addrs []string, bytes []byte) {
	m := make(map[string]bool)
	for _, addr := range addrs {
		m[addr] = true
	}

	g.mtx.RLock()
	for addr, a := range g.addrToAgent {
		if _, o := m[addr]; !o {
			if err := a.Send(bytes); err != nil {
				app.Log().Error("send failed", utils.M{
					"addr": addr,
				})
			}
		}
	}
	g.mtx.RUnlock()
}

func (g *gate) SendByFilter(filter func(intfc.IAgent) bool, bytes []byte) {
	g.mtx.RLock()
	for addr, a := range g.addrToAgent {
		if filter(a) {
			if err := a.Send(bytes); err != nil {
				app.Log().Error("send failed", utils.M{
					"addr": addr,
				})
			}
		}
	}
	g.mtx.RUnlock()
}

func (g *gate) CloseAll() {
	g.mtx.Lock()
	for _, a := range g.addrToAgent {
		a.Stop()
	}
	g.addrToAgent = make(map[string]intfc.IAgent, len(g.addrToAgent))
	g.mtx.Unlock()
}
