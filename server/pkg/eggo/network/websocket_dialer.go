package network

import (
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"golang.org/x/net/websocket"
)

type websocketDialer struct {
	name     string
	url      string
	protocol string
	origin   string
	agent    intfc.IAgent
	receiver intfc.ActionAgentBytesToErrCode
	option   *intfc.DialerOption
	enable   *utils.Enable
}

func NewWebsocketDialer(url, protocol, origin string, receiver intfc.ActionAgentBytesToErrCode, options ...intfc.ActionDialerOption) *websocketDialer {
	d := &websocketDialer{
		url:      url,
		protocol: protocol,
		origin:   origin,
		receiver: receiver,
		enable:   utils.NewEnable(),
		option:   &intfc.DialerOption{},
	}
	for _, opt := range options {
		opt(d.option)
	}
	return d
}

func (d *websocketDialer) Name() string {
	return d.name
}

func (d *websocketDialer) Addr() string {
	return d.url
}

func (d *websocketDialer) Enable() bool {
	return d.enable.Enable()
}

func (d *websocketDialer) Connect(m data.Map, options ...intfc.ActionAgentOption) error {
	conn, err := websocket.Dial(d.url, d.protocol, d.origin)
	if err != nil {
		return err
	}

	d.agent = NewWebsocketAgent(d.name, d.receiver, m, options...)
	d.agent.Start(conn)
	if d.option.OnConnected != nil {
		d.option.OnConnected(d)
	}
	return nil
}

func (d *websocketDialer) Agent() intfc.IAgent {
	return d.agent
}

func (d *websocketDialer) Close() {
	if d.agent != nil {
		d.agent.Stop()
	}
}
