package network

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	"net"
)

func NewTcpListener(addr string, onConn func(conn net.Conn)) *tcpListener {
	return &tcpListener{
		addr:   addr,
		onConn: onConn,
	}
}

type tcpListener struct {
	addr     string
	listener *net.TCPListener
	onConn   func(conn net.Conn)
}

func (l *tcpListener) Addr() string {
	return l.addr
}

func (l *tcpListener) Start() error {
	addr, err := net.ResolveTCPAddr("tcp", l.addr)
	if err != nil {
		return err
	}

	l.listener, err = net.ListenTCP("tcp", addr)
	if err != nil {
		return err
	}

	go func() {
		for {
			conn, err := l.listener.AcceptTCP()
			if err != nil {
				app.Log().Error("accept tcp failed", utils.M{
					"error": err.Error(),
				})
				return
			}

			l.onConn(conn)
		}
	}()
	return nil
}

func (l *tcpListener) Close() {
	l.listener.Close()
}
