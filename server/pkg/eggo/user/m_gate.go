package user

import (
	"95eh.com/eg/app"
	"95eh.com/eg/common"
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/network"
	"95eh.com/eg/utils"
	"net"
)

type (
	GateOption func(option *gateOption)
	gateOption struct {
		typ            intfc.TListener
		addr           string
		cap            int
		cacheDur       int64
		defUserDataFac func() data.Map
		connFilter     intfc.ActionConnToBool
		connected      utils.ActionString
		closed         utils.ActionInt64String
		receiver       intfc.ActionAgentBytesToErrCode
	}
)

func GateType(typ intfc.TListener) GateOption {
	return func(option *gateOption) {
		option.typ = typ
	}
}

func GateListen(addr string) GateOption {
	return func(option *gateOption) {
		option.addr = addr
	}
}

func GateCap(cap int) GateOption {
	return func(option *gateOption) {
		option.cap = cap
	}
}

func GateCacheDur(cacheDur int64) GateOption {
	return func(option *gateOption) {
		option.cacheDur = cacheDur
	}
}

func GateDefaultUserDataFac(defUserDataFac data.ToMap) GateOption {
	return func(option *gateOption) {
		option.defUserDataFac = defUserDataFac
	}
}

func GateBlackFilter(connFilter intfc.ActionConnToBool) GateOption {
	return func(option *gateOption) {
		option.connFilter = connFilter
	}
}

func GateConnected(connected utils.ActionString) GateOption {
	return func(option *gateOption) {
		option.connected = connected
	}
}

func GateClosed(closed utils.ActionInt64String) GateOption {
	return func(option *gateOption) {
		option.closed = closed
	}
}

// GateReceiver 设置消息接收器,默认user/pack.go-> ReceiveGateRequest
func GateReceiver(receiver intfc.ActionAgentBytesToErrCode) GateOption {
	return func(option *gateOption) {
		option.receiver = receiver
	}
}

func GateOptions(opts ...GateOption) []GateOption {
	return opts
}

func NewMGate(codec intfc.ICodec, opts []GateOption, moduleOpts ...intfc.ModuleOption) intfc.IMUserGate {
	option := &gateOption{
		typ:            intfc.TListenerTcp,
		addr:           ":7020",
		cap:            4096,
		cacheDur:       15,
		defUserDataFac: nil,
		connFilter:     nil,
		receiver:       ReceiveGateRequest,
	}
	for _, opt := range opts {
		opt(option)
	}
	g := &mGate{
		IModule: intfc.NewModule(moduleOpts...),
		option:  option,
		codec:   codec,
		IGate:   network.NewGate(option.cap),
	}
	switch g.option.typ {
	case intfc.TListenerTcp:
		g.listener = network.NewTcpListener(g.option.addr, g.onAddConn)
	case intfc.TListenerWebsocket:
		g.listener = network.NewWebsocketListener(g.option.addr, g.onAddConn)
	}
	return g
}

type mGate struct {
	intfc.IModule
	intfc.IGate
	option   *gateOption
	codec    intfc.ICodec
	listener intfc.IListener
}

func (M *mGate) Type() intfc.TModule {
	return intfc.MUserGate
}

func (M *mGate) onAddConn(conn net.Conn) {
	if M.option.connFilter != nil && M.option.connFilter(conn) {
		conn.Close()
		return
	}
	addr := conn.RemoteAddr().String()
	agent := network.NewTcpAgent(addr, M.option.receiver, nil,
		intfc.AgentMaxBadPacketLimit(5),
		intfc.AgentMaxBadPacketInterval(5), intfc.AgentClosed(M.onAgentClosed))
	M.Add(agent)
	agent.Start(conn)
	if M.option.connected != nil {
		M.option.connected(addr)
	}
}

func (M *mGate) onAgentClosed(agent intfc.IAgent, err error) {
	addr := agent.Addr()
	uid, ok := app.UserLogic().GetUserUid(addr)
	if !ok {
		return
	}
	if M.option.closed != nil {
		M.option.closed(uid, addr)
	}
	M.Remove(addr)
	app.UserLogic().DelUserData(addr)
}

func (M *mGate) Codec() intfc.ICodec {
	return M.codec
}

func (M *mGate) StartListen() {
	app.Log().Info("user gate start listening", utils.M{
		"addr": M.option.addr,
	})
	M.listener.Start()
}

func (M *mGate) ProcessRequest(addr string, tid, id int64, code uint32, body interface{}, resOk utils.ActionBytes, resErr utils.ActionErrCode) {
	app.UserLogic().ProcessRequest(addr, tid, id, code, body, func(res interface{}) {
		if res == nil {
			resOk(nil)
			return
		}
		bytes, err := M.codec.Marshal(res)
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
			resErr(common.EcEncodeFailed)
			return
		}
		app.Log().TInfo(tid, "response ok", utils.M{
			"response": res,
		})
		resOk(bytes)
	}, resErr)
}
