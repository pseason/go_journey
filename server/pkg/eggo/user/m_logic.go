package user

import (
	"95eh.com/eg/app"
	"95eh.com/eg/common"
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	cmap "github.com/orcaman/concurrent-map"
	"sync"
)

func NewMLogic(codec intfc.ICodec, moduleOpts ...intfc.ModuleOption) intfc.IMUserLogic {
	return &mLogic{
		IModule:         intfc.NewModule(moduleOpts...),
		codec:           codec,
		codeToRole:      make(map[uint32]int64),
		addrToUid:       make(map[string]int64),
		uidToData:       make(map[int64]*userData),
		aliasToUid:      make(map[int64]int64),
		codeToProcessor: make(map[uint32]utils.ActionStrInt642Any),
		idToResponse:    cmap.New(),
	}
}

type mLogic struct {
	intfc.IModule
	codec           intfc.ICodec
	codeToRole      map[uint32]int64
	codeToProcessor map[uint32]utils.ActionStrInt642Any
	idToResponse    cmap.ConcurrentMap
	userMtx         sync.RWMutex
	addrToUid       map[string]int64
	uidToData       map[int64]*userData
	aliasToUid      map[int64]int64
}

func (M *mLogic) Type() intfc.TModule {
	return intfc.MUserLogic
}

func (M *mLogic) AddUserData(uid int64, mask int64, addr string, m data.Map) (oldAddr string, exist bool) {
	M.userMtx.Lock()
	d, ok := M.uidToData[uid]
	if ok {
		exist = true
		oldAddr = d.Addr()
	}

	M.uidToData[uid] = newUserData(mask, addr, m)
	M.addrToUid[addr] = uid
	M.userMtx.Unlock()
	return
}

func (M *mLogic) GetUserUid(addr string) (uid int64, ok bool) {
	M.userMtx.RLock()
	uid, ok = M.addrToUid[addr]
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) DelUserData(addr string) {
	M.userMtx.Lock()
	uid, ok := M.addrToUid[addr]
	if ok {
		delete(M.addrToUid, addr)
		delete(M.uidToData, uid)
	}
	M.userMtx.Unlock()
}

func (M *mLogic) SetUidAlias(uid, alias int64) {
	M.userMtx.Lock()
	data, ok := M.uidToData[uid]
	if !ok {
		M.userMtx.Unlock()
		app.Log().Error("not exist uid", utils.M{
			"uid": uid,
		})
		return
	}
	data.alias = alias
	M.aliasToUid[alias] = uid
	M.userMtx.Unlock()

}

func (M *mLogic) GetUidWithAlias(alias int64) (uid int64, ok bool) {
	M.userMtx.RLock()
	uid, ok = M.aliasToUid[alias]
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) GetAliasWithUid(uid int64) (alias int64, ok bool) {
	M.userMtx.RLock()
	var data *userData
	data, ok = M.uidToData[uid]
	if !ok {
		M.userMtx.RUnlock()
		return
	}
	alias = data.alias
	if alias == 0 {
		M.userMtx.RUnlock()
		return 0, false
	}
	ok = true
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) sendByUId(uid int64, bytes []byte) utils.IError {
	ud, ok := M.uidToData[uid]
	if !ok {
		return utils.NewError("not exist uid", utils.M{
			"uid": uid,
		})
	}
	return app.UserGate().Send(ud.addr, bytes)
}

func (M *mLogic) sendByAlias(alias int64, bytes []byte) utils.IError {
	uid, ok := M.aliasToUid[alias]
	if !ok {
		return utils.NewError("not exist alias", utils.M{
			"uid": uid,
		})
	}
	return M.sendByUId(uid, bytes)
}

func (M *mLogic) SendByUid(uid int64, bytes []byte) (err utils.IError) {
	M.userMtx.RLock()
	err = M.sendByUId(uid, bytes)
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) SendByAlias(alias int64, bytes []byte) (err utils.IError)  {
	M.userMtx.RLock()
	err = M.sendByAlias(alias, bytes)
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) SendByUids(uids []int64, bytes []byte) (errMap map[int64]utils.IError) {
	errMap = make(map[int64]utils.IError, len(uids))
	M.userMtx.RLock()
	for _, uid := range uids {
		errMap[uid] = M.sendByUId(uid, bytes)
	}
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) SendByAliases(aliases []int64, bytes []byte) (errMap map[int64]utils.IError) {
	errMap = make(map[int64]utils.IError, len(aliases))
	M.userMtx.RLock()
	for _, alias := range aliases {
		errMap[alias] = M.sendByAlias(alias, bytes)
	}
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) AddUserRole(uid int64, role int64) {
	M.userMtx.RLock()
	ud, o := M.uidToData[uid]
	if o {
		ud.AddRole(role)
	}
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) DelUserRole(uid int64, role int64) {
	M.userMtx.RLock()
	ud, o := M.uidToData[uid]
	if o {
		ud.DelRole(role)
	}
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) TestUserAuth(uid int64, code uint32, addr string) (ok bool) {
	M.userMtx.RLock()
	ud, o := M.uidToData[uid]
	if o {
		ok = ud.Addr() == addr && M.TestMask(ud.Mask(), code)
	}
	M.userMtx.RUnlock()
	return
}

func (M *mLogic) TestMask(mask int64, code uint32) bool {
	role, ok := M.codeToRole[code]
	if !ok || role == 0 {
		return true
	}
	return utils.TestMask(role, mask)
}

func (M *mLogic) ReadUserData(uid int64, action data.ActionBoolData) {
	M.userMtx.RLock()
	ud, ok := M.uidToData[uid]
	if !ok {
		M.userMtx.RUnlock()
		action(false, nil)
		return
	}
	action(true, ud)
	M.userMtx.RUnlock()
}

func (M *mLogic) WriteUserData(uid int64, action data.ActionBoolData) {
	M.userMtx.Lock()
	ud, ok := M.uidToData[uid]
	if !ok {
		M.userMtx.Unlock()
		action(false, nil)
		return
	}
	action(true, ud)
	M.userMtx.Unlock()
}

func (M *mLogic) BindCoderFac(code uint32, reqFac, resFac utils.ToAny) {
	M.codec.BindFac(code, reqFac, resFac)
}

func (M *mLogic) BindRequestProcessor(role int64, code uint32, processor intfc.IUserRequestProcessor) {
	M.BindRequestHandler(role, code, processor.Request)
}

func (M *mLogic) BindRequestHandler(role int64, code uint32, action utils.ActionStrInt642Any) {
	M.codeToRole[code] = role
	M.codeToProcessor[code] = action
}

func (M *mLogic) ProcessRequest(addr string, tid, id int64, code uint32, body interface{},
	resOk utils.ActionAny, resErr utils.ActionErrCode) {
	processor, ok := M.codeToProcessor[code]
	if !ok {
		app.Log().TError(tid, utils.NewError("not exist code", nil))
		return
	}
	key := utils.Int64ToStr(tid)
	res := common.NewResponse()
	res.Init(resOk, resErr, func() {
		_, exists := M.idToResponse.Pop(key)
		if !exists {
			return
		}
		app.Log().TError(tid, utils.NewError("response timeout", nil))
		resErr(common.EcTimeout)
	})
	M.idToResponse.Set(key, res)
	processor(addr, tid, id, body)
}

func (M *mLogic) ResOk(tid int64, data interface{}) {
	obj, exists := M.idToResponse.Pop(utils.Int64ToStr(tid))
	if !exists {
		app.Log().TInfo(tid, "not exist response", nil)
		return
	}
	app.Log().TInfo(tid, "response ok", nil)
	res := obj.(intfc.IResponse)
	res.ResOk(data)
}

func (M *mLogic) ResErr(tid int64, ec utils.TErrCode) {
	obj, exists := M.idToResponse.Pop(utils.Int64ToStr(tid))
	if !exists {
		app.Log().TInfo(tid, "not exist response", nil)
		return
	}
	app.Log().TInfo(tid, "response error", nil)
	res := obj.(intfc.IResponse)
	res.ResErr(ec)
}

func newUserData(mask int64, addr string, m data.Map) *userData {
	return &userData{
		IData: data.NewData(m),
		mask:  mask,
		addr:  addr,
	}
}

type userData struct {
	data.IData
	alias int64
	mask  int64
	addr  string
}

func (d *userData) Mask() int64 {
	return d.mask
}

func (d *userData) Addr() string {
	return d.addr
}

func (d *userData) AddRole(role int64) {
	d.mask = utils.MaskAddItem(d.mask, role)
}

func (d *userData) DelRole(role int64) {
	d.mask = utils.MaskDelItem(d.mask, role)
}

func (d *userData) TestAuth(code uint32, addr string) bool {
	return d.addr == addr && app.UserLogic().TestMask(d.mask, code)
}
