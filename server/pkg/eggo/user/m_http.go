package user

import (
	"95eh.com/eg/app"
	"95eh.com/eg/common"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
	"net/http"
)

type (
	HttpOption func(opt *httpOption)
	httpOption struct {
		listen   string
		addr     string
		cert     string
		key      string
		receiver intfc.ActionContextInt642Uint32Any
	}
)

func HttpListen(addr string) HttpOption {
	return func(opt *httpOption) {
		opt.listen = addr
	}
}

func HttpCert(cert string) HttpOption {
	return func(opt *httpOption) {
		opt.cert = cert
	}
}

func HttpKey(key string) HttpOption {
	return func(opt *httpOption) {
		opt.key = key
	}
}

// HttpReceiver 设置消息接收器,默认user/pack.go-> ReceiveHttpRequest
func HttpReceiver(receiver intfc.ActionContextInt642Uint32Any) HttpOption {
	return func(opt *httpOption) {
		opt.receiver = receiver
	}
}

func HttpOptions(ops ...HttpOption) []HttpOption {
	return ops
}

func NewMHttp(codec intfc.ICodec, resOk intfc.ActionContextUint32Any, resErr intfc.ActionContextUint32ErrCode,
	opts []HttpOption, moduleOpts ...intfc.ModuleOption) intfc.IMUserHttp {
	gin.SetMode("release")
	engine := gin.New()
	engine.Use(gin.Recovery())
	opt := &httpOption{
		listen:     ":7030",
		receiver: ReceiveHttpRequest,
	}
	for _, o := range opts {
		o(opt)
	}
	return &mHttp{
		IModule: intfc.NewModule(moduleOpts...),
		option:  opt,
		codec:   codec,
		engine:  engine,
		resOk:   resOk,
		resErr:  resErr,
	}
}

type mHttp struct {
	intfc.IModule
	option   *httpOption
	codec    intfc.ICodec
	engine   *gin.Engine
	server   *http.Server
	idParser intfc.ActionContextToInt642Error
	resOk    intfc.ActionContextUint32Any
	resErr   intfc.ActionContextUint32ErrCode
}

func (M *mHttp) Type() intfc.TModule {
	return intfc.MUserHttp
}

func (M *mHttp) Dispose() {
	M.server.Shutdown(context.Background())
}

func (M *mHttp) Codec() intfc.ICodec {
	return M.codec
}

func (M *mHttp) Addr() string {
	return M.option.addr
}

func (M *mHttp) Engine() *gin.Engine {
	return M.engine
}

func (M *mHttp) SetIdParser(parser intfc.ActionContextToInt642Error) {
	M.idParser = parser
}

func (M *mHttp) BindProcessor(method, path string, code uint32) {
	switch method {
	case http.MethodGet:
		M.bindProcessor(path, code, M.engine.GET)
	case http.MethodPost:
		M.bindProcessor(path, code, M.engine.POST)
	case http.MethodPut:
		M.bindProcessor(path, code, M.engine.PUT)
	case http.MethodDelete:
		M.bindProcessor(path, code, M.engine.DELETE)
	default:
		panic("not implement")
	}
}

func (M *mHttp) bindProcessor(path string, code uint32, method func(relativePath string, handlers ...gin.HandlerFunc) gin.IRoutes) {
	method(path, func(c *gin.Context) {
		body, err := M.codec.SpawnReq(code)
		if err == nil {
			err = c.ShouldBindJSON(body)
			if err != nil {
				M.resErr(c, code, common.EcDecodeFailed)
				return
			}
		}

		addr := c.ClientIP()
		requestPath := c.Request.URL.Path
		tid := app.Log().TSign(0, utils.M{
			"tracer": "http",
			"method": c.Request.Method,
			"path":   requestPath,
			"body":   body,
			"addr":   addr,
		})
		//获取uid
		var uid int64
		if M.idParser != nil {
			var (
				mask int64
				err  error
			)
			uid, mask, err = M.idParser(c)
			if err != nil {
				app.Log().TError(tid, utils.NewError(err.Error(), nil))
				M.resErr(c, code, common.EcNoAuth)
				return
			}
			//测试权限
			ok := app.UserLogic().TestMask(mask, code)
			if !ok {
				app.Log().TError(tid, utils.NewError("unauthorized", utils.M{
					"mask": mask,
					"code": code,
				}))
				//c.JSONP(http.StatusUnauthorized, nil)
				M.resErr(c, code, common.EcNoAuth)
				return
			}
		}
		M.option.receiver(c, tid, uid, code, body)
	})
}

func (M *mHttp) Start() {
	M.server = &http.Server{
		Addr:    M.option.listen,
		Handler: M.engine,
	}
	go func() {
		app.Log().Info("user http listen", utils.M{
			"addr": M.option.addr,
		})
		var err error
		if M.option.cert == "" || M.option.key == "" {
			err = M.server.ListenAndServe()
		} else {
			err = M.server.ListenAndServeTLS(M.option.cert, M.option.key)
		}
		if err != nil && err != http.ErrServerClosed {
			app.Log().Fatal("user http listen failed", utils.M{
				"error": err.Error(),
			})
			return
		}
	}()
}

func (M *mHttp) ResOk(c *gin.Context, code uint32, data interface{}) {
	M.resOk(c, code, data)
}

func (M *mHttp) ResErr(c *gin.Context, code uint32, ec utils.TErrCode) {
	M.resErr(c, code, ec)
}
