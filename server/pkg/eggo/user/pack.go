package user

import (
	"95eh.com/eg/app"
	"95eh.com/eg/common"
	"95eh.com/eg/data"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"github.com/gin-gonic/gin"
)

const (
	UserProtocolResErr uint8 = iota
	UserProtocolResOk
	UserProtocolEvent
)

func PackUserRequest(rid int64, code uint32, pkt []byte) []byte {
	buffer := data.NewBytesBufferWithLen(uint32(12 + len(pkt)))
	buffer.WInt64(rid)
	buffer.WUint32(code)
	buffer.Write(pkt)
	return buffer.WAll()
}

func UnpackUserRequest(bytes []byte) (rid int64, code uint32, pkt []byte, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	rid, err = buffer.RInt64()
	if err != nil {
		return
	}
	code, err = buffer.RUint32()
	if err != nil {
		return
	}
	pkt = buffer.RAll()
	return
}

func PackUserResponseErr(rid int64, errCode utils.TErrCode) []byte {
	buffer := data.NewBytesBufferWithLen(13)
	buffer.WUint8(UserProtocolResErr)
	buffer.WInt64(rid)
	buffer.WUint32(uint32(errCode))
	return buffer.WAll()
}

func UnpackUserResponseErr(bytes []byte) (rid int64, errCode utils.TErrCode, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	buffer.SetPos(1)
	rid, err = buffer.RInt64()
	if err != nil {
		return
	}
	var ec uint32
	ec, err = buffer.RUint32()
	if err != nil {
		return
	}
	errCode = utils.TErrCode(ec)
	return
}

func PackUserResponseOk(rid int64, pkt []byte) []byte {
	buffer := data.NewBytesBufferWithLen(uint32(9 + len(pkt)))
	buffer.WUint8(UserProtocolResOk)
	buffer.WInt64(rid)
	buffer.Write(pkt)
	return buffer.WAll()
}

func UnpackUserResponseOk(bytes []byte) (rid int64, pkt []byte, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	buffer.SetPos(1)
	rid, err = buffer.RInt64()
	if err != nil {
		return
	}
	pkt = buffer.RAll()
	return
}

func PackUserEvent(code uint32, pkt []byte) []byte {
	buffer := data.NewBytesBufferWithLen(uint32(5 + len(pkt)))
	buffer.WUint8(UserProtocolEvent)
	buffer.WUint32(code)
	buffer.Write(pkt)
	return buffer.WAll()
}

func UnpackUserEvent(bytes []byte) (code uint32, pkt []byte, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	buffer.SetPos(1)
	code, err = buffer.RUint32()
	if err != nil {
		return
	}
	pkt = buffer.RAll()
	return
}

func PackUserPacket(code uint32, pkt []byte) []byte {
	buffer := data.NewBytesBufferWithLen(uint32(5 + len(pkt)))
	buffer.WUint32(code)
	buffer.Write(pkt)
	return buffer.WAll()
}

func UnpackUserPacket(bytes []byte) (code uint32, pkt []byte, err error) {
	buffer := data.NewBytesBufferWithData(bytes)
	buffer.SetPos(1)
	code, err = buffer.RUint32()
	if err != nil {
		return
	}
	pkt = buffer.RAll()
	return
}

func ReceiveGateRequest(agent intfc.IAgent, data []byte) utils.TErrCode {
	if len(data) < 12 {
		return common.EcBadPacket
	}
	rid, code, pkt, err := UnpackUserRequest(data)
	if err != nil {
		return common.EcUnpackFailed
	}
	addr := agent.Addr()
	id, _ := app.UserLogic().GetUserUid(addr)
	ok := app.UserLogic().TestUserAuth(id, code, addr)
	if !ok {
		app.Log().Error("no auth", utils.M{
			"addr": addr,
			"code": code,
		})
		return common.EcNoAuth
	}

	body, err := app.UserGate().Codec().UnmarshalReq(code, pkt)
	if err != nil {
		app.Log().Error(err.Error(), utils.M{
			"code":  code,
		})
		return common.EcDecodeFailed
	}
	tid := app.Log().TSign(0, utils.M{
		"tracer":    "game request with id",
		"code":      code,
		"body":      body,
		"requestId": rid,
	})

	app.UserGate().ProcessRequest(addr, tid, id, code, body, func(data []byte) {
		app.Log().TInfo(tid, "response ok", nil)
		app.UserLogic().SendByUid(id, PackUserResponseOk(rid, data))
	}, func(ec utils.TErrCode) {
		app.Log().TInfo(tid, "response error", utils.M{
			"error code": ec,
		})
		app.UserLogic().SendByUid(id, PackUserResponseErr(rid, ec))
	})
	return 0
}

func ReceiveHttpRequest(c *gin.Context, tid, uid int64, code uint32, body interface{}) {
	ch := make(chan struct{}, 1)
	app.UserLogic().ProcessRequest(c.ClientIP(), tid, uid, code, body, func(res interface{}) {
		app.Log().TInfo(tid, "response ok", nil)
		app.UserHttp().ResOk(c, code, res)
		ch <- struct{}{}
	}, func(ec utils.TErrCode) {
		app.Log().TInfo(tid, "response error", utils.M{
			"error": ec.Desc(),
		})
		if !c.IsAborted() {
			app.UserHttp().ResErr(c, code, ec)
		}
		ch <- struct{}{}
	})
	<-ch
}
