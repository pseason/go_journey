package discovery

import (
	"95eh.com/eg/app"
	"95eh.com/eg/common"
	"95eh.com/eg/intfc"
	"95eh.com/eg/network"
	"95eh.com/eg/utils"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	consulApi "github.com/hashicorp/consul/api"
	"github.com/hashicorp/consul/api/watch"
	cmap "github.com/orcaman/concurrent-map"
	"net"
	"strconv"
	"strings"
	"sync"
)

type (
	Option func(opt *option)
	option struct {
		onRemoteNodeClosed utils.ActionString
		onNodeConnected    intfc.ActionNodeUint162
		onNodeClosed       intfc.ActionNodeUint162
		reconnectDur       int64
		ip                 string
		port               int
		regionId           uint16
		nodeId             uint16
		weight             int
		checkSsl           bool
	}
)

const (
	_ReconnectNodeCount = 5
)

func NodeConnected(action intfc.ActionNodeUint162) Option {
	return func(opt *option) {
		opt.onNodeConnected = action
	}
}

func NodeClosed(action intfc.ActionNodeUint162) Option {
	return func(opt *option) {
		opt.onNodeClosed = action
	}
}

func RemoteNodeClosed(action utils.ActionString) Option {
	return func(opt *option) {
		opt.onRemoteNodeClosed = action
	}
}

func ReconnectDur(reconnectDur int64) Option {
	return func(opt *option) {
		opt.reconnectDur = reconnectDur
	}
}

func Addr(ip string, port int) Option {
	return func(opt *option) {
		opt.ip = ip
		opt.port = port
	}
}

func CheckType(ssl bool) Option {
	return func(opt *option) {
		opt.checkSsl = ssl
	}
}

func RegionId(regionId uint16) Option {
	return func(opt *option) {
		opt.regionId = regionId
	}
}

func NodeId(nodeId uint16) Option {
	return func(opt *option) {
		opt.nodeId = nodeId
	}
}

func Weight(weight int) Option {
	return func(opt *option) {
		opt.weight = weight
	}
}

func Nodes(nodeTypes ...intfc.TNode) []intfc.TNode {
	return nodeTypes
}

func NewMDiscovery(nodeTypes []intfc.TNode, nodeId, regionId uint16,
	codec intfc.ICodec, redis *redis.Client,
	moduleOpts []intfc.ModuleOption, opts ...Option) *mDiscovery {
	utils.SetSnowflakeRegionNodeId(int64(nodeId))
	utils.SetSnowflakeGlobalNodeId(int64(regionId))
	opt := &option{
		reconnectDur: 1000,
		ip:           "127.0.0.1",
		port:         7000,
		regionId:     regionId,
		nodeId:       nodeId,
		weight:       1,
	}
	for _, o := range opts {
		o(opt)
	}
	c := &mDiscovery{
		IModule:                intfc.NewModule(moduleOpts...),
		nodeTypes:              nodeTypes,
		regionId:               regionId,
		nodeId:                 nodeId,
		option:                 opt,
		codec:                  codec,
		redis:                  redis,
		nodeMtx:                &sync.RWMutex{},
		idToResponse:           cmap.New(),
		codeToRequestProcessor: make(map[uint32]intfc.ActionUint16Int642AnyActionAnyActionErrCode),
		codeToEventProcessor:   make(map[uint32]intfc.ActionUint16Int64Any),
		idToPlan:               make(map[int64]*watch.Plan),
		nodeTypeToMNode:        make(map[intfc.TNode]*selector),
		serviceToNode:          make(map[uint16]intfc.TNode),
	}
	c.packetHeadToReceiver = map[uint8]intfc.ActionAgentBytesToErrCode{
		common.HdRequest: c.receiveRequest,
		common.HdEvent:   c.receiveEvent,
	}
	c.packetHeadToResponse = map[uint8]intfc.ActionAgentBytesToErrCode{
		common.HdResponseOk:  c.onResponseOk,
		common.HdResponseErr: c.onResponseErr,
	}
	addr := fmt.Sprintf("%s:%d", c.option.ip, c.option.port)
	c.listener = network.NewTcpListener(addr, c.onAddConn)
	return c
}

type mDiscovery struct {
	intfc.IModule
	nodeTypes              []intfc.TNode
	regionId               uint16
	nodeId                 uint16
	option                 *option
	codec                  intfc.ICodec
	redis                  *redis.Client
	nodeMtx                *sync.RWMutex
	idToResponse           cmap.ConcurrentMap
	listener               intfc.IListener
	codeToRequestProcessor map[uint32]intfc.ActionUint16Int642AnyActionAnyActionErrCode
	codeToEventProcessor   map[uint32]intfc.ActionUint16Int64Any
	packetHeadToReceiver   map[uint8]intfc.ActionAgentBytesToErrCode
	packetHeadToResponse   map[uint8]intfc.ActionAgentBytesToErrCode
	planMtx                sync.RWMutex
	idToPlan               map[int64]*watch.Plan
	nodeTypeToMNode        map[intfc.TNode]*selector
	serviceToNode          map[uint16]intfc.TNode
}

func setWriteOptionsToken(opt *consulApi.WriteOptions) *consulApi.WriteOptions {
	tkn := utils.ConsulConfig().Token
	if tkn == "" {
		return opt
	}
	if opt == nil {
		return &consulApi.WriteOptions{
			Token: tkn,
		}
	}
	opt.Token = tkn
	return opt
}

func setQueryOptionsToken(opt *consulApi.QueryOptions) *consulApi.QueryOptions {
	tkn := utils.ConsulConfig().Token
	if tkn == "" {
		return opt
	}
	if opt == nil {
		return &consulApi.QueryOptions{
			Token: tkn,
		}
	}
	opt.Token = tkn
	return opt
}

func (M *mDiscovery) Redis() *redis.Client {
	return M.redis
}

func (M *mDiscovery) SetKV(key string, val []byte, opt *consulApi.WriteOptions) error {
	key = utils.MergeStr("/", key)
	_, err := utils.Consul().KV().Put(&consulApi.KVPair{
		Key:   key,
		Value: val,
	}, setWriteOptionsToken(opt))
	return err
}

func (M *mDiscovery) RemoveKV(key string, opt *consulApi.WriteOptions) error {
	key = utils.MergeStr("/", key)
	_, err := utils.Consul().KV().Delete(key, setWriteOptionsToken(opt))
	return err
}

func (M *mDiscovery) GetKV(key string, opt *consulApi.QueryOptions) ([]byte, error) {
	key = utils.MergeStr("/", key)
	kv, _, err := utils.Consul().KV().Get(key, setQueryOptionsToken(opt))
	if err != nil {
		return nil, err
	}
	if kv == nil {
		return nil, errors.New("not exist key")
	}
	return kv.Value, nil
}

func (M *mDiscovery) WatchKV(key string, action utils.ActionStrBytesAny) (int64, error) {
	key = utils.MergeStr("/", key)
	v, err := M.GetKV(key, nil)
	if err == nil {
		action(key, v)
	}
	params := make(map[string]interface{})
	params["type"] = "key"
	params["key"] = key
	return M.addPlan(params, func(index uint64, any interface{}) {
		if any == nil {
			return
		}
		result := any.(*consulApi.KVPair)
		action(result.Key, result.Value)
	})
}

func (M *mDiscovery) UnwatchKV(id int64) {
	M.delPlan(id)
}

func (M *mDiscovery) AddWhiteList(list []string) {
	for _, ip := range list {
		M.SetKV(utils.MergeStr("/", "whiteList", ip), []byte{1}, nil)
	}
}

func (M *mDiscovery) testWhiteList(ip string) bool {
	_, err := M.GetKV(utils.MergeStr("/", "whiteList", ip), nil)
	return err != nil
}

func (M *mDiscovery) Type() intfc.TModule {
	return intfc.MDiscovery
}

func getRegisterId(nodeType intfc.TNode, regionId, nodeId uint16) string {
	return fmt.Sprintf("%d_%s_%d", regionId, nodeType, nodeId)
}

func (M *mDiscovery) Start() {
	app.Log().Info("discovery listening", utils.M{
		"addr": M.listener.Addr(),
	})
	M.listener.Start()

	for _, nodeType := range M.nodeTypes {
		name := M.getNodeName(nodeType, M.regionId)
		err := utils.Consul().Agent().ServiceRegister(&consulApi.AgentServiceRegistration{
			ID:      getRegisterId(nodeType, M.regionId, M.nodeId),
			Name:    name,
			Address: M.option.ip,
			Port:    M.option.port,
			Meta: map[string]string{
				"regionId": strconv.Itoa(int(M.regionId)),
				"nodeType": nodeType.String(),
				"nodeId":   strconv.Itoa(int(M.nodeId)),
				"weight":   strconv.Itoa(M.option.weight),
			},
			Check: &consulApi.AgentServiceCheck{
				TCP:                            M.listener.Addr(),
				Timeout:                        "1s",
				Interval:                       "5s",
				DeregisterCriticalServiceAfter: "10s",
			},
		})

		if err != nil {
			panic(err)
		}
	}
}

func (M *mDiscovery) WatchNodes(nodes ...intfc.TNode) {
	nm := make(map[intfc.TNode]struct{})
	for _, node := range nodes {
		nm[node] = struct{}{}
		M.nodeTypeToMNode[node] = newSelector()
		M.watchServer(node, M.regionId)
	}

	services, err := utils.Consul().Agent().Services()
	if err == nil {
		for _, svc := range services {
			nts, ok := svc.Meta["nodeTypes"]
			if !ok {
				continue
			}
			nt := intfc.StrToNode(nts)
			_, ok = nm[nt]
			if !ok {
				continue
			}
			M.addNodeByMeta(svc.ID, svc.Address, svc.Port, svc.Meta)
		}
	}
}

func (M *mDiscovery) GetRegionName(regionId uint16) string {
	key := utils.MergeStr("/", "region", strconv.Itoa(int(regionId)), "name")
	v, err := M.GetKV(key, nil)
	if err != nil {
		panic(fmt.Sprintf("get region name failed. region:%s error:%s",
			key, err.Error()))
	}
	return string(v)
}

func (M *mDiscovery) getNodeName(node intfc.TNode, regionId uint16) string {
	if node.IsRegion() {
		regionName := M.GetRegionName(regionId)
		return fmt.Sprintf("%s_%s", regionName, node)
	}
	return node.String()
}

func (M *mDiscovery) addNodeByMeta(serviceId, addr string, port int, meta map[string]string) {
	nodeAddr := fmt.Sprintf("%s:%d", addr, port)
	regionId, _ := strconv.Atoi(meta["regionId"])
	node, _ := meta["nodeType"]
	nodeId, _ := strconv.Atoi(meta["nodeId"])
	weight, _ := strconv.Atoi(meta["weight"])
	M.addNode(intfc.StrToNode(node), serviceId, nodeAddr, uint16(regionId), uint16(nodeId), weight)
}

func (M *mDiscovery) watchServer(node intfc.TNode, regionId uint16) {
	service := M.getNodeName(node, regionId)
	params := make(map[string]interface{})
	params["type"] = "service"
	params["service"] = service
	params["passingonly"] = true
	_, err := M.addPlan(params, func(index uint64, result interface{}) {
		if entries, ok := result.([]*consulApi.ServiceEntry); ok {
			M.nodeMtx.Lock()
			if len(entries) > 0 {
				for _, entry := range entries {
					M.addNodeByMeta(entry.Service.ID, entry.Service.Address, entry.Service.Port, entry.Service.Meta)
				}
			} else {
				M.clearNode(node)
			}
			M.nodeMtx.Unlock()
		}
	})
	if err == nil {
		return
	}
	app.Log().Error("watch server failed", utils.M{
		"error": err.Error(),
	})
}

func (M *mDiscovery) addPlan(params map[string]interface{}, handler func(index uint64, result interface{})) (int64, error) {
	plan, err := watch.Parse(params)
	if err != nil {
		return 0, err
	}
	conf := utils.ConsulConfig()
	plan.Token = conf.Token
	plan.Handler = handler
	id := utils.GenSnowflakeRegionNodeId()
	M.planMtx.Lock()
	M.idToPlan[id] = plan
	M.planMtx.Unlock()
	go func() {
		addr := conf.Scheme + "://" + conf.Address
		plan.Run(addr)
	}()
	return id, nil
}

func (M *mDiscovery) delPlan(id int64) {
	M.planMtx.Lock()
	plan, ok := M.idToPlan[id]
	if ok {
		plan.Stop()
		delete(M.idToPlan, id)
	}
	M.planMtx.Unlock()
}

func (M *mDiscovery) Dispose() {
	M.nodeMtx.Lock()
	for _, n := range M.nodeTypeToMNode {
		n.Dispose()
	}
	M.nodeMtx.Unlock()
	M.planMtx.Lock()
	for _, plan := range M.idToPlan {
		plan.Stop()
	}
	M.planMtx.Unlock()
}

func (M *mDiscovery) Id() int64 {
	return int64(utils.MergeUInt32(M.regionId, M.nodeId))
}

func (M *mDiscovery) RegionId() uint16 {
	return M.regionId
}

func (M *mDiscovery) NodeTypes() []intfc.TNode {
	return M.nodeTypes
}

func (M *mDiscovery) NodeId() uint16 {
	return M.nodeId
}

func (M *mDiscovery) genRequestResponse(sTid int64, resOk utils.ActionAny, resErr utils.ActionErrCode) (intfc.IResponse, string) {
	sTidStr := utils.Int64ToStr(sTid)
	res := common.NewResponse()
	res.Init(resOk, resErr, func() {
		_, exists := M.idToResponse.Pop(sTidStr)
		if !exists {
			return
		}
		res.ResErr(common.EcTimeout)
	})
	return res, sTidStr
}

func (M *mDiscovery) BindNodeServices(node intfc.TNode, services ...uint16) {
	for _, service := range services {
		M.serviceToNode[service] = node
	}
}

func (M *mDiscovery) GetNodeByService(service uint16) (intfc.TNode, bool) {
	node, ok := M.serviceToNode[service]
	return node, ok
}

func (M *mDiscovery) RequestNode(node intfc.TNode, nodeId, service uint16, pTid, id int64, code uint32, body interface{},
	resOk utils.ActionInt64Any, resErr utils.ActionInt64ErrCode) {
	sTid := app.Log().TSign(pTid, utils.M{
		"tracer":  "discovery request node",
		"node":    node.String(),
		"id":      id,
		"node id": nodeId,
		"code":    code,
		"body":    body,
	})
	bytes, err := M.codec.Marshal(body)
	if err != nil {
		app.Log().TError(sTid, utils.NewError("discovery request encode failed", utils.M{
			"error": err.Error(),
		}))
		resErr(sTid, common.EcEncodeFailed)
		return
	}

	res, rid := M.genRequestResponse(sTid, func(obj interface{}) {
		resOk(sTid, obj)
	}, func(ec utils.TErrCode) {
		resErr(sTid, ec)
	})
	M.nodeMtx.RLock()
	ns, o := M.nodeTypeToMNode[node]
	if !o {
		M.nodeMtx.RUnlock()
		app.Log().TError(sTid, utils.NewError("discovery request not exist node", nil))
		res.ResErr(common.EcNotExistNode)
		return
	}
	M.idToResponse.Set(rid, res)
	err = ns.SendTo(nodeId, common.PackRequest(common.HdRequest, service, sTid, id, code, bytes))
	M.nodeMtx.RUnlock()
	if err != nil {
		M.idToResponse.Remove(rid)
		app.Log().TError(sTid, utils.NewError(err.Error(), nil))
		res.ResErr(common.EcSendPacketFailed)
		return
	}

	return
}

func (M *mDiscovery) SyncRequestNode(tNode intfc.TNode, nodeId, service uint16, pTid, id int64, code uint32,
	body interface{}) (res interface{}, ec utils.TErrCode) {
	resCh := make(chan interface{})
	ecCh := make(chan utils.TErrCode)
	M.RequestNode(tNode, nodeId, service, pTid, id, code, body, func(sTid int64, obj interface{}) {
		resCh <- obj
	}, func(sTid int64, ec utils.TErrCode) {
		ecCh <- ec
	})
	select {
	case res = <-resCh:
	case ec = <-ecCh:
	}
	return
}

func (M *mDiscovery) RequestWithSTid(service uint16, sTid, id int64, code uint32, body interface{},
	resOk utils.ActionUint16Any, resErr utils.ActionUint16ErrCode) (nodeId uint16) {
	bytes, err := M.codec.Marshal(body)
	if err != nil {
		app.Log().TError(sTid, utils.NewError("discovery request encode failed", utils.M{
			"error": err.Error(),
		}))
		return
	}

	res, rid := M.genRequestResponse(sTid, func(obj interface{}) {
		resOk(nodeId, obj)
	}, func(ec utils.TErrCode) {
		resErr(nodeId, ec)
	})

	node, ok := M.GetNodeByService(service)
	if !ok {
		res.ResErr(common.EcNotExistNode)
		return
	}
	M.nodeMtx.RLock()
	ns, o := M.nodeTypeToMNode[node]
	if !o {
		app.Log().TError(sTid, utils.NewError("discovery request not exist node", nil))
		res.ResErr(common.EcNotExistNode)
		M.nodeMtx.RUnlock()
		return
	}
	nodeId, err = ns.SendNext(common.PackRequest(common.HdRequest, service, sTid, id, code, bytes))
	M.nodeMtx.RUnlock()
	if err != nil {
		app.Log().TError(sTid, utils.NewError(err.Error(), nil))
		res.ResErr(common.EcSendPacketFailed)
		return
	}

	M.idToResponse.Set(rid, res)
	return
}

func (M *mDiscovery) SyncRequestWithSTid(service uint16, sTid, id int64, code uint32,
	body interface{}) (nodeId uint16, res interface{}, ec utils.TErrCode) {
	resCh := make(chan interface{})
	ecCh := make(chan utils.TErrCode)
	nodeId = M.RequestWithSTid(service, sTid, id, code, body, func(_ uint16, obj interface{}) {
		resCh <- obj
	}, func(nodeId uint16, ec utils.TErrCode) {
		ecCh <- ec
	})
	select {
	case res = <-resCh:
	case ec = <-ecCh:
	}
	return
}

func (M *mDiscovery) Request(service uint16, pTid, id int64, code uint32, body interface{},
	resOk utils.ActionUint16Int64Any, resErr utils.ActionUint16Int64ErrCode) (nodeId uint16) {
	sTid := app.Log().TSign(pTid, utils.M{
		"tracer": "discovery request",
		"id":     id,
		"code":   code,
		"body":   body,
	})
	nodeId = M.RequestWithSTid(service, sTid, id, code, body, func(nodeId uint16, obj interface{}) {
		resOk(nodeId, sTid, obj)
	}, func(nodeId uint16, ec utils.TErrCode) {
		resErr(nodeId, sTid, ec)
	})
	return
}

func (M *mDiscovery) SyncRequest(service uint16, pTid, id int64, code uint32,
	body interface{}) (nodeId uint16, res interface{}, ec utils.TErrCode) {
	resCh := make(chan interface{})
	ecCh := make(chan utils.TErrCode)
	nodeId = M.Request(service, pTid, id, code, body, func(_ uint16, _ int64, obj interface{}) {
		resCh <- obj
	}, func(_ uint16, _ int64, ec utils.TErrCode) {
		ecCh <- ec
	})
	select {
	case res = <-resCh:
	case ec = <-ecCh:
	}
	return
}

func (M *mDiscovery) DispatchEvent(node intfc.TNode, service uint16, pTid, id int64, code uint32, body interface{}) {
	bytes, err := M.codec.Marshal(body)
	if err != nil {
		app.Log().Error("marshal event failed", utils.M{
			"error": err.Error(),
			"code":  code,
		})
		return
	}
	tid := app.Log().TSign(pTid, utils.M{
		"tracer": "discovery dispatch event",
		"node":   node.String(),
		"code":   code,
		"body":   body,
	})
	M.nodeMtx.RLock()
	ns, o := M.nodeTypeToMNode[node]
	if !o {
		M.nodeMtx.RUnlock()
		app.Log().TError(tid, utils.NewError("discovery request not exist node", nil))
		return
	}
	ns.SendAll(common.PackEvent(service, tid, id, code, bytes))
	M.nodeMtx.RUnlock()
}

func (M *mDiscovery) DispatchEventToAllNode(service uint16, pTid, id int64, code uint32, body interface{}) {
	bytes, err := M.codec.Marshal(body)
	if err != nil {
		app.Log().Error("marshal event failed", utils.M{
			"error": err.Error(),
			"code":  code,
		})
		return
	}
	tid := app.Log().TSign(pTid, utils.M{
		"tracer": "discovery dispatch event to all node",
		"code":   code,
		"body":   body,
	})
	M.nodeMtx.RLock()
	for _, ns := range M.nodeTypeToMNode {
		ns.SendAll(common.PackEvent(service, tid, id, code, bytes))
	}
	M.nodeMtx.RUnlock()
}

func (M *mDiscovery) SendEvent(node intfc.TNode, service uint16, pTid, id int64, code uint32, body interface{}) (nodeId uint16, ok bool) {
	tid := app.Log().TSign(pTid, utils.M{
		"tracer": "discovery send event",
		"node":   node.String(),
		"code":   code,
		"body":   body,
	})
	bytes, err := M.codec.Marshal(body)
	if err != nil {
		app.Log().Error("marshal event failed", utils.M{
			"error": err.Error(),
			"code":  code,
		})
		return
	}
	M.nodeMtx.RLock()
	ns, o := M.nodeTypeToMNode[node]
	if !o {
		M.nodeMtx.RUnlock()
		app.Log().TError(tid, utils.NewError("discovery request not exist node", nil))
		return
	}
	nodeId, err = ns.SendNext(common.PackEvent(service, tid, id, code, bytes))
	M.nodeMtx.RUnlock()
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), nil))
	}
	return
}

func (M *mDiscovery) SendEventToNode(node intfc.TNode, service, nodeId uint16, pTid, id int64, code uint32, body interface{}) (ok bool) {
	bytes, err := M.codec.Marshal(body)
	if err != nil {
		app.Log().Error("marshal event failed", utils.M{
			"error": err.Error(),
			"code":  code,
		})
		return
	}
	tid := app.Log().TSign(pTid, utils.M{
		"tracer":  "discovery send event to node",
		"node":    node.String(),
		"node id": nodeId,
		"code":    code,
		"body":    body,
	})
	M.nodeMtx.RLock()
	ns, o := M.nodeTypeToMNode[node]
	if !o {
		M.nodeMtx.RUnlock()
		app.Log().TError(tid, utils.NewError("discovery request not exist node", nil))
		return
	}
	err = ns.SendTo(nodeId, common.PackEvent(service, tid, id, code, bytes))
	M.nodeMtx.RUnlock()
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), nil))
	}
	return
}

func (M *mDiscovery) BindCoderFac(code uint32, reqFac, resFac utils.ToAny) {
	M.codec.BindFac(code, reqFac, resFac)
}

func (M *mDiscovery) BindRequestProcessor(code uint32, processor intfc.IRequestProcessor) {
	M.BindRequestHandler(code, processor.Request)
}

func (M *mDiscovery) BindRequestAsyncHandler(code uint32, handler intfc.ActionUint16Int642AnyActionAnyActionErrCode) {
	M.codeToRequestProcessor[code] = handler
}

func (M *mDiscovery) BindRequestHandler(code uint32, handler intfc.ActionUint16Int642AnyToAnyErrCode) {
	M.codeToRequestProcessor[code] = func(service uint16, tid, cid int64, body interface{}, resObj utils.ActionAny, resErr utils.ActionErrCode) {
		res, ec := handler(service, tid, cid, body)
		if ec > 0 {
			resErr(ec)
			return
		}
		resObj(res)
	}
}

func (M *mDiscovery) BindEventProcessor(code uint32, processor intfc.IEventProcessor) {
	M.BindEventHandler(code, processor.Trigger)
}

func (M *mDiscovery) BindEventHandler(code uint32, handler intfc.ActionUint16Int64Any) {
	M.codeToEventProcessor[code] = handler
}

func (M *mDiscovery) receiveResponse(agent intfc.IAgent, data []byte) utils.TErrCode {
	if len(data) == 0 {
		return common.EcEmptyPacket
	}
	action, ok := M.packetHeadToResponse[data[0]]
	if !ok {
		return common.EcBadHead
	}
	return action(agent, data)
}

func (M *mDiscovery) receivePacket(agent intfc.IAgent, data []byte) utils.TErrCode {
	if len(data) == 0 {
		return common.EcEmptyPacket
	}
	action, ok := M.packetHeadToReceiver[data[0]]
	if !ok {
		return common.EcBadHead
	}
	return action(agent, data)
}

func (M *mDiscovery) keepConnect(dialer intfc.IDialer, node intfc.TNode, serviceId, addr string,
	regionId, nodeId uint16, reconnect int) {
	err := dialer.Connect(nil, intfc.AgentConnected(func() {
		app.Log().Info("node connected", utils.M{
			"node":       M.getNodeName(node, regionId),
			"id":         nodeId,
			"service id": serviceId,
			"addr":       addr,
		})
		M.nodeTypeToMNode[node].EnableNode(nodeId)

		if M.option.onNodeConnected != nil {
			//避免死锁
			utils.Async(func() {
				M.option.onNodeConnected(node, regionId, nodeId)
			})
		}
	}), intfc.AgentClosed(func(agent intfc.IAgent, err error) {
		m := utils.M{
			"node":       M.getNodeName(node, regionId),
			"id":         nodeId,
			"service id": serviceId,
			"addr":       addr,
		}
		if err != nil {
			m["error"] = err.Error()
		}
		app.Log().Info("node closed", m)

		M.nodeTypeToMNode[node].DisableNode(nodeId)

		if M.option.onNodeClosed != nil {
			M.option.onNodeClosed(node, regionId, nodeId)
		}
		if err == nil {
			return
		}
		if M.checkNode(serviceId) {
			M.keepConnect(dialer, node, serviceId, addr, regionId, nodeId, _ReconnectNodeCount)
		} else {
			M.nodeTypeToMNode[node].RemoveNode(nodeId)
		}
	}))
	if err == nil {
		return
	}
	app.Log().Info("node connect failed", utils.M{
		"node":  M.getNodeName(node, regionId),
		"id":    nodeId,
		"addr":  addr,
		"error": err.Error(),
	})
	app.Timer().After(1000, func() {
		if M.checkNode(serviceId) {
			if reconnect == 1 {
				M.nodeTypeToMNode[node].RemoveNode(nodeId)
				return
			}
			M.keepConnect(dialer, node, serviceId, addr, regionId, nodeId, reconnect-1)
		} else {
			M.nodeTypeToMNode[node].RemoveNode(nodeId)
		}
	})
}

func (M *mDiscovery) checkNode(serviceId string) bool {
	status, _, err := utils.Consul().Agent().AgentHealthServiceByID(serviceId)
	if err != nil {
		return false
	}
	app.Log().Info("node status", utils.M{
		"status":     status,
		"service id": serviceId,
	})
	return status == consulApi.HealthPassing
}

func (M *mDiscovery) testIgnoreRegister(typ intfc.TNode, regionId, nodeId uint16) bool {
	for _, nodeType := range M.nodeTypes {
		if typ == nodeType && regionId == M.regionId && nodeId == M.nodeId {
			return true
		}
	}
	return false
}

func (M *mDiscovery) addNode(node intfc.TNode, serviceId, addr string, regionId, nodeId uint16, weight int) {
	if M.testIgnoreRegister(node, regionId, nodeId) {
		return
	}
	dialer, ok := M.nodeTypeToMNode[node].AddNode(nodeId, serviceId, addr, M.receiveResponse, weight)
	if ok {
		M.keepConnect(dialer, node, serviceId, addr, regionId, nodeId, _ReconnectNodeCount)
	}
}

func (M *mDiscovery) clearNode(node intfc.TNode) {
	M.nodeTypeToMNode[node].Dispose()
}

func (M *mDiscovery) onAddConn(conn net.Conn) {
	addr := conn.RemoteAddr().String()
	ip := strings.Split(addr, ":")[0]
	if ip != "127.0.0.1" && !M.testWhiteList(ip) {
		//if !M.testWhite(ip) {
		app.Log().Warn("illegal ip connect", utils.M{
			"addr": addr,
		})
		conn.Close()
		return
	}
	network.NewTcpAgent(addr, M.receivePacket, nil).Start(conn)
}

func (M *mDiscovery) receiveRequest(agent intfc.IAgent, data []byte) utils.TErrCode {
	service, tid, id, code, pkt, err := common.UnpackRequest(data)
	if err != nil {
		return common.EcBadPacket
	}

	app.Log().TInfo(tid, "discovery receive request", nil)
	body, err := M.codec.UnmarshalReq(code, pkt)
	if err != nil {
		app.Log().TError(tid, utils.NewError("discovery request decode failed", nil))
		return common.EcDecodeFailed
	}
	p, ok := M.codeToRequestProcessor[code]
	if !ok {
		app.Log().TError(tid, utils.NewError("discovery receive request code not exist", nil))
		return common.EcBadPacket
	}
	p(service, tid, id, body, func(obj interface{}) {
		if obj == nil {
			app.Log().TInfo(tid, "discovery response ok", nil)
			agent.Send(common.PackResponseOk(tid, code, nil))
			return
		}

		bytes, err := M.codec.Marshal(obj)
		if err != nil {
			app.Log().Error("encode response failed", utils.M{
				"code": code,
			})
			return
		}
		e := agent.Send(common.PackResponseOk(tid, code, bytes))
		if err != nil {
			app.Log().TError(tid, e)
		}
	}, func(ec utils.TErrCode) {
		app.Log().TInfo(tid, "discovery response error", utils.M{
			"error code": ec,
		})
		err := agent.Send(common.PackResponseError(tid, ec))
		if err != nil {
			app.Log().TError(tid, err)
		}
	})
	return 0
}

func (M *mDiscovery) receiveEvent(agent intfc.IAgent, data []byte) utils.TErrCode {
	service, tid, id, code, pkt, err := common.UnpackEvent(data)
	if err != nil {
		return common.EcBadPacket
	}

	p, ok := M.codeToEventProcessor[code]
	if !ok {
		app.Log().TError(tid, utils.NewError("not exist code", nil))
		return 0
	}
	app.Log().TInfo(tid, "discovery receive event", nil)
	body, err := M.codec.UnmarshalRes(code, pkt)
	if err != nil {
		app.Log().TError(tid, utils.NewError("discovery decode event failed", nil))
		return common.EcDecodeFailed
	}
	app.Worker().DoAction(id, func() {
		p(service, tid, body)
	})
	return 0
}

func (M *mDiscovery) onResponseOk(agent intfc.IAgent, data []byte) utils.TErrCode {
	tid, code, pkt, err := common.UnpackResponseOk(data)
	if err != nil {
		return common.EcBadPacket
	}
	var body interface{}
	if pkt != nil {
		body, err = M.codec.UnmarshalRes(code, pkt)
		if err != nil {
			app.Log().TError(tid, utils.NewError(err.Error(), nil))
			return common.EcDecodeFailed
		}
	}
	res, ok := M.idToResponse.Pop(utils.Int64ToStr(tid))
	if ok {
		res.(intfc.IResponse).ResOk(body)
	}
	return 0
}

func (M *mDiscovery) onResponseErr(agent intfc.IAgent, data []byte) utils.TErrCode {
	tid, ec, err := common.UnpackResponseError(data)
	if err != nil {
		app.Log().TError(tid, utils.NewError(err.Error(), nil))
		return common.EcBadPacket
	}
	res, ok := M.idToResponse.Pop(utils.Int64ToStr(tid))
	if ok {
		res.(intfc.IResponse).ResErr(ec)
	}
	return 0
}
