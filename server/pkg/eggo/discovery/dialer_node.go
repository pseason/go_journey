package discovery

import (
	"95eh.com/eg/intfc"
)

type dialerNode struct {
	dialer    intfc.IDialer
	nodeId    uint16
	weight    int
	current   int
	effective int
}

func newDialerNode(nodeId uint16, weight int, dialer intfc.IDialer) *dialerNode {
	return &dialerNode{
		nodeId:    nodeId,
		weight:    weight,
		effective: weight,
		dialer:    dialer,
	}
}

func (d *dialerNode) dispose() {
	d.dialer.Close()
}
