package discovery

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/network"
	"errors"
	"sync"
)

var (
	ErrNotExistDialerNodeId = errors.New("not exist dialer node id")
	ErrSendFailed           = errors.New("send failed")
)

func newSelector() *selector {
	return &selector{
		nodeIdToDialer: make(map[uint16]intfc.IDialer),
		nodeIdToWeight: make(map[uint16]int),
	}
}

type selector struct {
	mtx            sync.RWMutex
	nodeIdToDialer map[uint16]intfc.IDialer
	nodeIdToWeight map[uint16]int
	items          []*dialerNode
	n              int
}

func (s *selector) AddNode(nodeId uint16, name, addr string, receiver intfc.ActionAgentBytesToErrCode, weight int) (intfc.IDialer, bool) {
	s.mtx.Lock()
	dialer, ok := s.nodeIdToDialer[nodeId]
	if ok {
		if dialer.Addr() == addr {
			s.mtx.Unlock()
			return nil, false
		}
		dialer.Close()
		delete(s.nodeIdToDialer, nodeId)
		s.disableNode(nodeId)
	}
	newDialer := network.NewTcpDialer(name, addr, receiver)
	s.nodeIdToDialer[nodeId] = newDialer
	s.nodeIdToWeight[nodeId] = weight
	s.mtx.Unlock()
	return newDialer, true
}

func (s *selector) EnableNode(nodeId uint16) {
	s.mtx.Lock()
	s.enableNode(nodeId)
	s.mtx.Unlock()
}

func (s *selector) DisableNode(nodeId uint16) {
	s.mtx.Lock()
	s.disableNode(nodeId)
	s.mtx.Unlock()
}

func (s *selector) enableNode(nodeId uint16) {
	weight := s.nodeIdToWeight[nodeId]
	dialer := s.nodeIdToDialer[nodeId]
	s.items = append(s.items, newDialerNode(nodeId, weight, dialer))
	s.n++
}

func (s *selector) disableNode(nodeId uint16) {
	for i, item := range s.items {
		if item.nodeId == nodeId {
			s.items = append(s.items[:i], s.items[i+1:]...)
			s.n--
			return
		}
	}
}

func (s *selector) getNextNode() (uint16, bool) {
	if s.n == 0 {
		return 0, false
	}
	if s.n == 1 {
		return s.items[0].nodeId, true
	}

	total := 0
	var node *dialerNode
	for i := 0; i < len(s.items); i++ {
		w := s.items[i]

		w.current += w.effective
		total += w.effective
		if w.effective < w.weight {
			w.effective++
		}

		if node == nil || w.current > node.current {
			node = w
		}
	}

	node.current -= total
	return node.nodeId, true
}

func (s *selector) RemoveNode(nodeId uint16) {
	s.mtx.Lock()
	dialer, ok := s.nodeIdToDialer[nodeId]
	if ok {
		delete(s.nodeIdToDialer, nodeId)
		s.disableNode(nodeId)
		dialer.Close()
	}
	s.mtx.Unlock()
}

func (s *selector) sendTo(nodeId uint16, pkt []byte) error {
	dialer, ok := s.nodeIdToDialer[nodeId]
	if !ok {
		return ErrNotExistDialerNodeId
	}
	err := dialer.Agent().Send(pkt)
	if err != nil {
		return ErrSendFailed
	}
	return nil
}

func (s *selector) SendTo(nodeId uint16, pkt []byte) (err error) {
	s.mtx.RLock()
	err = s.sendTo(nodeId, pkt)
	s.mtx.RUnlock()
	return
}

func (s *selector) SendNext(pkt []byte) (nodeId uint16, err error) {
	s.mtx.RLock()
	var ok bool
	nodeId, ok = s.getNextNode()
	if ok {
		err = s.sendTo(nodeId, pkt)
	} else {
		err = ErrNotExistDialerNodeId
	}
	s.mtx.RUnlock()
	return
}

func (s *selector) SendAll(pkt []byte) {
	s.mtx.RLock()
	for _, dialer := range s.nodeIdToDialer {
		dialer.Agent().Send(pkt)
	}
	s.mtx.RUnlock()
}

func (s *selector) Dispose() {
	s.mtx.RLock()
	for _, dialer := range s.nodeIdToDialer {
		dialer.Close()
	}
	s.mtx.RUnlock()
}
