package data

import "95eh.com/eg/utils"

func NewAny(v interface{}) *Any {
	return &Any{
		v:        v,
		listener: make(map[int64]utils.ActionAny),
	}
}

type Any struct {
	v        interface{}
	listener map[int64]utils.ActionAny
}

func (a *Any) Get() interface{} {
	return a.v
}

func (a *Any) Set(v interface{}, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Any) Bind(action utils.ActionAny) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Any) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Any) ClearListeners() {
	a.listener = make(map[int64]utils.ActionAny)
}
