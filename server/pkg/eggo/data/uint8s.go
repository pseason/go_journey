package data

import "95eh.com/eg/utils"

func NewUint8s(v []uint8) *Uint8s {
	return &Uint8s{
		v:        v,
		listener: make(map[int64]utils.ActionUint8s),
	}
}

type Uint8s struct {
	v        []uint8
	listener map[int64]utils.ActionUint8s
}

func (a *Uint8s) Get() []uint8 {
	return a.v
}

func (a *Uint8s) Set(v []uint8, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Uint8s) Bind(action utils.ActionUint8s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Uint8s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Uint8s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionUint8s)
}
