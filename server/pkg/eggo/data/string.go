package data

import "95eh.com/eg/utils"

func NewString(v string) *String {
	return &String{
		v:        v,
		listener: make(map[int64]utils.ActionString),
	}
}

type String struct {
	v        string
	listener map[int64]utils.ActionString
}

func (a *String) Get() string {
	return a.v
}

func (a *String) Set(v string, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
}

func (a *String) Bind(action utils.ActionString) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *String) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *String) ClearListeners() {
	a.listener = make(map[int64]utils.ActionString)
}
