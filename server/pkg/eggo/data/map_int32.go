package data

import "95eh.com/eg/utils"

func NewMapInt32(v map[TField]int32) *MapInt32 {
	return &MapInt32{
		v:        v,
		listener: make(map[int64]ActionMapInt32),
	}
}

type MapInt32 struct {
	v        map[TField]int32
	listener map[int64]ActionMapInt32
}

func (a *MapInt32) Get() map[TField]int32 {
	return a.v
}

func (a *MapInt32) Set(v map[TField]int32, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *MapInt32) Bind(action ActionMapInt32) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *MapInt32) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *MapInt32) ClearListeners() {
	a.listener = make(map[int64]ActionMapInt32)
}
