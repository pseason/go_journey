package data

import "95eh.com/eg/utils"

func NewUint16(v uint16) *Uint16 {
	return &Uint16{
		v:        v,
		listener: make(map[int64]utils.ActionUint16),
	}
}

type Uint16 struct {
	v        uint16
	listener map[int64]utils.ActionUint16
}

func (a *Uint16) Get() uint16 {
	return a.v
}

func (a *Uint16) Set(v uint16, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Uint16) Bind(action utils.ActionUint16) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Uint16) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Uint16) ClearListeners() {
	a.listener = make(map[int64]utils.ActionUint16)
}
