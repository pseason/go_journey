package data

import "95eh.com/eg/utils"

func NewMapInt64ToInt64s(v map[int64][]int64) *MapInt64ToInt64s {
	return &MapInt64ToInt64s{
		v:        v,
		listener: make(map[int64]utils.ActionMapInt64ToInt64s),
	}
}

type MapInt64ToInt64s struct {
	v        map[int64][]int64
	listener map[int64]utils.ActionMapInt64ToInt64s
}

func (a *MapInt64ToInt64s) Get() map[int64][]int64 {
	return a.v
}

func (a *MapInt64ToInt64s) Set(v map[int64][]int64, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *MapInt64ToInt64s) Bind(action utils.ActionMapInt64ToInt64s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *MapInt64ToInt64s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *MapInt64ToInt64s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionMapInt64ToInt64s)
}
