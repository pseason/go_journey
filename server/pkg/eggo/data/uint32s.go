package data

import "95eh.com/eg/utils"

func NewUint32s(v []uint32) *Uint32s {
	return &Uint32s{
		v:        v,
		listener: make(map[int64]utils.ActionUint32s),
	}
}

type Uint32s struct {
	v        []uint32
	listener map[int64]utils.ActionUint32s
}

func (a *Uint32s) Get() []uint32 {
	return a.v
}

func (a *Uint32s) Set(v []uint32, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Uint32s) Bind(action utils.ActionUint32s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Uint32s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Uint32s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionUint32s)
}
