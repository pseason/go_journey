package data

import "95eh.com/eg/utils"

func NewInt32(v int32) *Int32 {
	return &Int32{
		v:        v,
		listener: make(map[int64]utils.ActionInt32),
	}
}

type Int32 struct {
	v        int32
	listener map[int64]utils.ActionInt32
}

func (a *Int32) Get() int32 {
	return a.v
}

func (a *Int32) Set(v int32, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Int32) Bind(action utils.ActionInt32) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Int32) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Int32) ClearListeners() {
	a.listener = make(map[int64]utils.ActionInt32)
}
