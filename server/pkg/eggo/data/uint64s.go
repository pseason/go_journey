package data

import "95eh.com/eg/utils"

func NewUint64s(v []uint64) *Uint64s {
	return &Uint64s{
		v:        v,
		listener: make(map[int64]utils.ActionUint64s),
	}
}

type Uint64s struct {
	v        []uint64
	listener map[int64]utils.ActionUint64s
}

func (a *Uint64s) Get() []uint64 {
	return a.v
}

func (a *Uint64s) Set(v []uint64, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Uint64s) Bind(action utils.ActionUint64s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Uint64s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Uint64s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionUint64s)
}
