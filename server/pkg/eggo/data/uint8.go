package data

import "95eh.com/eg/utils"

func NewUint8(v uint8) *Uint8 {
	return &Uint8{
		v:        v,
		listener: make(map[int64]utils.ActionUint8),
	}
}

type Uint8 struct {
	v        uint8
	listener map[int64]utils.ActionUint8
}

func (a *Uint8) Get() uint8 {
	return a.v
}

func (a *Uint8) Set(v uint8, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Uint8) Bind(action utils.ActionUint8) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Uint8) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Uint8) ClearListeners() {
	a.listener = make(map[int64]utils.ActionUint8)
}
