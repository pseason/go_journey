package data

import (
	"95eh.com/eg/utils"
)

func NewFields() *fields {
	return &fields{
		string:          make(map[TField]string),
		strings:         make(map[TField][]string),
		bool:            make(map[TField]bool),
		bools:           make(map[TField][]bool),
		int16:           make(map[TField]int16),
		int16s:          make(map[TField][]int16),
		int32:           make(map[TField]int32),
		int32s:          make(map[TField][]int32),
		int64:           make(map[TField]int64),
		int64s:          make(map[TField][]int64),
		float32:         make(map[TField]float32),
		float32s:        make(map[TField][]float32),
		vector2:         make(map[TField]utils.Vec2),
		vector2s:        make(map[TField][]utils.Vec2),
		mapInt32:        make(map[TField]map[string]int32),
		mapInt64:        make(map[TField]map[string]int64),
		mapInt32ToInt32: make(map[TField]map[int32]int32),
		mapInt32ToInt64: make(map[TField]map[int32]int64),
		mapInt64ToInt32: make(map[TField]map[int64]int32),
		mapInt64ToInt64: make(map[TField]map[int64]int64),
		any:             make(map[TField]interface{}),
	}
}

func NewFieldsFromBytes(bytes []byte) (*fields, error) {
	f := NewFields()
	err := f.MergeBytes(bytes)
	if err != nil {
		return nil, err
	}
	return f, nil
}

type fields struct {
	string          map[TField]string
	strings         map[TField][]string
	bool            map[TField]bool
	bools           map[TField][]bool
	int32           map[TField]int32
	int32s          map[TField][]int32
	int16           map[TField]int16
	int16s          map[TField][]int16
	int64           map[TField]int64
	int64s          map[TField][]int64
	float32         map[TField]float32
	float32s        map[TField][]float32
	vector2         map[TField]utils.Vec2
	vector2s        map[TField][]utils.Vec2
	mapInt32        map[TField]map[string]int32
	mapInt64        map[TField]map[string]int64
	mapInt32ToInt32 map[TField]map[int32]int32
	mapInt32ToInt64 map[TField]map[int32]int64
	mapInt64ToInt32 map[TField]map[int64]int32
	mapInt64ToInt64 map[TField]map[int64]int64
	any             map[TField]interface{}
}

func (f *fields) GetString(t TField) (string, bool) {
	v, ok := f.string[t]
	return v, ok
}

func (f *fields) SetString(t TField, v string) {
	f.string[t] = v
}

func (f *fields) GetStrings(t TField) ([]string, bool) {
	v, ok := f.strings[t]
	return v, ok
}

func (f *fields) SetStrings(t TField, v []string) {
	f.strings[t] = v
}

func (f *fields) GetBool(t TField) (bool, bool) {
	v, ok := f.bool[t]
	return v, ok
}

func (f *fields) SetBool(t TField, v bool) {
	f.bool[t] = v
}

func (f *fields) GetBools(t TField) ([]bool, bool) {
	v, ok := f.bools[t]
	return v, ok
}

func (f *fields) SetBools(t TField, v []bool) {
	f.bools[t] = v
}

func (f *fields) GetInt16(t TField) (int16, bool) {
	v, ok := f.int16[t]
	return v, ok
}

func (f *fields) SetInt16(t TField, v int16) {
	f.int16[t] = v
}

func (f *fields) GetInt16s(t TField) ([]int16, bool) {
	v, ok := f.int16s[t]
	return v, ok
}

func (f *fields) SetInt16s(t TField, v []int16) {
	f.int16s[t] = v
}

func (f *fields) GetInt32(t TField) (int32, bool) {
	v, ok := f.int32[t]
	return v, ok
}

func (f *fields) SetInt32(t TField, v int32) {
	f.int32[t] = v
}

func (f *fields) GetInt32s(t TField) ([]int32, bool) {
	v, ok := f.int32s[t]
	return v, ok
}

func (f *fields) SetInt32s(t TField, v []int32) {
	f.int32s[t] = v
}

func (f *fields) GetInt64(t TField) (int64, bool) {
	v, ok := f.int64[t]
	return v, ok
}

func (f *fields) SetInt64(t TField, v int64) {
	f.int64[t] = v
}

func (f *fields) GetInt64s(t TField) ([]int64, bool) {
	v, ok := f.int64s[t]
	return v, ok
}

func (f *fields) SetInt64s(t TField, v []int64) {
	f.int64s[t] = v
}

func (f *fields) GetFloat32(t TField) (float32, bool) {
	v, ok := f.float32[t]
	return v, ok
}

func (f *fields) SetFloat32(t TField, v float32) {
	f.float32[t] = v
}

func (f *fields) GetFloat32s(t TField) ([]float32, bool) {
	v, ok := f.float32s[t]
	return v, ok
}

func (f *fields) SetFloat32s(t TField, v []float32) {
	f.float32s[t] = v
}

func (f *fields) GetVec2(t TField) (utils.Vec2, bool) {
	v, ok := f.vector2[t]
	return v, ok
}

func (f *fields) SetVec2(t TField, v utils.Vec2) {
	f.vector2[t] = v
}

func (f *fields) GetVec2s(t TField) ([]utils.Vec2, bool) {
	v, ok := f.vector2s[t]
	return v, ok
}

func (f *fields) SetVec2s(t TField, v []utils.Vec2) {
	f.vector2s[t] = v
}

func (f *fields) GetMapInt32(t TField) (map[string]int32, bool) {
	v, ok := f.mapInt32[t]
	return v, ok
}

func (f *fields) SetMapInt32(t TField, v map[string]int32) {
	f.mapInt32[t] = v
}

func (f *fields) GetMapInt64(t TField) (map[string]int64, bool) {
	v, ok := f.mapInt64[t]
	return v, ok
}

func (f *fields) SetMapInt64(t TField, v map[string]int64) {
	f.mapInt64[t] = v
}

func (f *fields) GetMapInt32ToInt32(t TField) (map[int32]int32, bool) {
	v, ok := f.mapInt32ToInt32[t]
	return v, ok
}

func (f *fields) SetMapInt32ToInt32(t TField, v map[int32]int32) {
	f.mapInt32ToInt32[t] = v
}

func (f *fields) GetMapInt32ToInt64(t TField) (map[int32]int64, bool) {
	v, ok := f.mapInt32ToInt64[t]
	return v, ok
}

func (f *fields) SetMapInt32ToInt64(t TField, v map[int32]int64) {
	f.mapInt32ToInt64[t] = v
}

func (f *fields) GetMapInt64ToInt32(t TField) (map[int64]int32, bool) {
	v, ok := f.mapInt64ToInt32[t]
	return v, ok
}

func (f *fields) SetMapInt64ToInt32(t TField, v map[int64]int32) {
	f.mapInt64ToInt32[t] = v
}

func (f *fields) GetMapInt64ToInt64(t TField) (map[int64]int64, bool) {
	v, ok := f.mapInt64ToInt64[t]
	return v, ok
}

func (f *fields) SetMapInt64ToInt64(t TField, v map[int64]int64) {
	f.mapInt64ToInt64[t] = v
}

func (f *fields) GetAny(t TField) (interface{}, bool) {
	v, ok := f.any[t]
	return v, ok
}

func (f *fields) SetAny(t TField, v interface{}) {
	f.any[t] = v
}

func (f *fields) ToBytes() ([]byte, error) {
	buffer := NewBytesBuffer()

	buffer.WUint16(uint16(len(f.string)))
	for k, v := range f.string {
		buffer.WInt32(int32(k))
		buffer.WString(v)
	}

	buffer.WUint16(uint16(len(f.strings)))
	for k, v := range f.strings {
		buffer.WInt32(int32(k))
		buffer.WStrings(v)
	}

	buffer.WUint16(uint16(len(f.bool)))
	for k, v := range f.bool {
		buffer.WInt32(int32(k))
		buffer.WBool(v)
	}

	buffer.WUint16(uint16(len(f.bools)))
	for k, v := range f.bools {
		buffer.WInt32(int32(k))
		buffer.WBools(v)
	}

	buffer.WUint16(uint16(len(f.int16)))
	for k, v := range f.int16 {
		buffer.WInt32(int32(k))
		buffer.WInt16(v)
	}

	buffer.WUint16(uint16(len(f.int16s)))
	for k, v := range f.int16s {
		buffer.WInt32(int32(k))
		buffer.WInt16s(v)
	}

	buffer.WUint16(uint16(len(f.int32)))
	for k, v := range f.int32 {
		buffer.WInt32(int32(k))
		buffer.WInt32(v)
	}

	buffer.WUint16(uint16(len(f.int32s)))
	for k, v := range f.int32s {
		buffer.WInt32(int32(k))
		buffer.WInt32s(v)
	}

	buffer.WUint16(uint16(len(f.int64)))
	for k, v := range f.int64 {
		buffer.WInt32(int32(k))
		buffer.WInt64(v)
	}

	buffer.WUint16(uint16(len(f.int64s)))
	for k, v := range f.int64s {
		buffer.WInt32(int32(k))
		buffer.WInt64s(v)
	}

	buffer.WUint16(uint16(len(f.float32)))
	for k, v := range f.float32 {
		buffer.WInt32(int32(k))
		buffer.WFloat32(v)
	}

	buffer.WUint16(uint16(len(f.float32s)))
	for k, v := range f.float32s {
		buffer.WInt32(int32(k))
		buffer.WFloat32s(v)
	}

	buffer.WUint16(uint16(len(f.vector2)))
	for k, v := range f.vector2 {
		buffer.WInt32(int32(k))
		buffer.WVec2(v)
	}

	buffer.WUint16(uint16(len(f.vector2s)))
	for k, v := range f.vector2s {
		buffer.WInt32(int32(k))
		buffer.WVec2s(v)
	}

	buffer.WUint16(uint16(len(f.mapInt32)))
	for k, v := range f.mapInt32 {
		buffer.WInt32(int32(k))
		buffer.WMapInt32(v)
	}

	buffer.WUint16(uint16(len(f.mapInt64)))
	for k, v := range f.mapInt32 {
		buffer.WInt32(int32(k))
		buffer.WMapInt32(v)
	}

	buffer.WUint16(uint16(len(f.mapInt32ToInt64)))
	for k, v := range f.mapInt32ToInt64 {
		buffer.WInt32(int32(k))
		buffer.WMapInt32ToInt64(v)
	}

	buffer.WUint16(uint16(len(f.mapInt64)))
	for k, v := range f.mapInt32 {
		buffer.WInt32(int32(k))
		buffer.WMapInt32(v)
	}

	buffer.WUint16(uint16(len(f.mapInt64ToInt32)))
	for k, v := range f.mapInt64ToInt32 {
		buffer.WInt32(int32(k))
		buffer.WMapInt64ToInt32(v)
	}

	buffer.WUint16(uint16(len(f.mapInt64ToInt64)))
	for k, v := range f.mapInt64ToInt64 {
		buffer.WInt32(int32(k))
		buffer.WMapInt64ToInt64(v)
	}

	l := len(f.any)
	if l > 0 {
		m := make(map[TField]interface{}, l)
		for k, v := range f.any {
			m[k] = v
		}
		bytes, err := GobMarshal(m)
		if err != nil {
			return nil, err
		}
		buffer.Write(bytes)
	}

	return buffer.WAll(), nil
}

func (f *fields) MergeString(m map[TField]string) {
	for k, v := range m {
		f.string[k] = v
	}
}

func (f *fields) MergeStrings(m map[TField][]string) {
	for k, v := range m {
		f.strings[k] = v
	}
}

func (f *fields) MergeBool(m map[TField]bool) {
	for k, v := range m {
		f.bool[k] = v
	}
}

func (f *fields) MergeBools(m map[TField][]bool) {
	for k, v := range m {
		f.bools[k] = v
	}
}

func (f *fields) MergeInt16(m map[TField]int16) {
	for k, v := range m {
		f.int16[k] = v
	}
}

func (f *fields) MergeInt16s(m map[TField][]int16) {
	for k, v := range m {
		f.int16s[k] = v
	}
}

func (f *fields) MergeInt32(m map[TField]int32) {
	for k, v := range m {
		f.int32[k] = v
	}
}

func (f *fields) MergeInt32s(m map[TField][]int32) {
	for k, v := range m {
		f.int32s[k] = v
	}
}

func (f *fields) MergeInt64(m map[TField]int64) {
	for k, v := range m {
		f.int64[k] = v
	}
}

func (f *fields) MergeInt64s(m map[TField][]int64) {
	for k, v := range m {
		f.int64s[k] = v
	}
}

func (f *fields) MergeFloat32(m map[TField]float32) {
	for k, v := range m {
		f.float32[k] = v
	}
}

func (f *fields) MergeFloat32s(m map[TField][]float32) {
	for k, v := range m {
		f.float32s[k] = v
	}
}

func (f *fields) MergeVector2(m map[TField]utils.Vec2) {
	for k, v := range m {
		f.vector2[k] = v
	}
}

func (f *fields) MergeVector2s(m map[TField][]utils.Vec2) {
	for k, v := range m {
		f.vector2s[k] = v
	}
}

func (f *fields) MergeMapInt32(m map[TField]map[string]int32) {
	for k, v := range m {
		f.mapInt32[k] = v
	}
}

func (f *fields) MergeMapInt64(m map[TField]map[string]int64) {
	for k, v := range m {
		f.mapInt64[k] = v
	}
}

func (f *fields) MergeMapInt32ToInt64(m map[TField]map[int32]int64) {
	for k, v := range m {
		f.mapInt32ToInt64[k] = v
	}
}

func (f *fields) MergeMapInt32ToInt32(m map[TField]map[int32]int32) {
	for k, v := range m {
		f.mapInt32ToInt32[k] = v
	}
}

func (f *fields) MergeMapInt64ToInt32(m map[TField]map[int64]int32) {
	for k, v := range m {
		f.mapInt64ToInt32[k] = v
	}
}

func (f *fields) MergeMapInt64ToInt64(m map[TField]map[int64]int64) {
	for k, v := range m {
		f.mapInt64ToInt64[k] = v
	}
}

func (f *fields) MergeAny(m Map) {
	for k, v := range m {
		f.any[k] = v
	}
}

func (f *fields) MergeBytes(data []byte) error {
	buffer := NewBytesBufferWithData(data)

	l, err := buffer.RUint16()
	if err != nil {
		return err
	}
	mString := make(map[TField]string, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RString()
		if err != nil {
			return err
		}
		mString[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mStrings := make(map[TField][]string, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RStrings()
		if err != nil {
			return err
		}
		mStrings[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mBool := make(map[TField]bool, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RBool()
		if err != nil {
			return err
		}
		mBool[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mBools := make(map[TField][]bool, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RBools()
		if err != nil {
			return err
		}
		mBools[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mInt16 := make(map[TField]int16, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RInt16()
		if err != nil {
			return err
		}
		mInt16[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mInt16s := make(map[TField][]int16, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RInt16s()
		if err != nil {
			return err
		}
		mInt16s[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mInt32 := make(map[TField]int32, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RInt32()
		if err != nil {
			return err
		}
		mInt32[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mInt32s := make(map[TField][]int32, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RInt32s()
		if err != nil {
			return err
		}
		mInt32s[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mInt64 := make(map[TField]int64, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RInt64()
		if err != nil {
			return err
		}
		mInt64[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mInt64s := make(map[TField][]int64, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RInt64s()
		if err != nil {
			return err
		}
		mInt64s[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mUint64 := make(map[TField]uint64, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RUint64()
		if err != nil {
			return err
		}
		mUint64[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mUint64s := make(map[TField][]uint64, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RUint64s()
		if err != nil {
			return err
		}
		mUint64s[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mFloat32 := make(map[TField]float32, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RFloat32()
		if err != nil {
			return err
		}
		mFloat32[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mFloat32s := make(map[TField][]float32, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RFloat32s()
		if err != nil {
			return err
		}
		mFloat32s[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mVector2 := make(map[TField]utils.Vec2, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RVec2()
		if err != nil {
			return err
		}
		mVector2[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mVector2s := make(map[TField][]utils.Vec2, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RVec2s()
		if err != nil {
			return err
		}
		mVector2s[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mMapInt32 := make(map[TField]map[string]int32, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RMapInt32()
		if err != nil {
			return err
		}
		mMapInt32[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mMapInt64 := make(map[TField]map[string]int64, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RMapInt64()
		if err != nil {
			return err
		}
		mMapInt64[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mMapInt32ToInt32 := make(map[TField]map[int32]int32, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RMapInt32ToInt32()
		if err != nil {
			return err
		}
		mMapInt32ToInt32[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mMapInt32ToInt64 := make(map[TField]map[int32]int64, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RMapInt32ToInt64()
		if err != nil {
			return err
		}
		mMapInt32ToInt64[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mMapInt64ToInt32 := make(map[TField]map[int64]int32, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RMapInt64ToInt32()
		if err != nil {
			return err
		}
		mMapInt64ToInt32[TField(k)] = v
	}

	l, err = buffer.RUint16()
	if err != nil {
		return err
	}
	mMapInt64ToInt64 := make(map[TField]map[int64]int64, l)
	for i := uint16(0); i < l; i++ {
		k, err := buffer.RUint16()
		if err != nil {
			return err
		}
		v, err := buffer.RMapInt64ToInt64()
		if err != nil {
			return err
		}
		mMapInt64ToInt64[TField(k)] = v
	}

	var mAny Map
	if buffer.Available() > 0 {
		err = GobUnmarshal(buffer.RAll(), &f)
		if err != nil {
			return err
		}
	}

	f.MergeBool(mBool)
	f.MergeBools(mBools)
	f.MergeString(mString)
	f.MergeStrings(mStrings)
	f.MergeInt16(mInt16)
	f.MergeInt16s(mInt16s)
	f.MergeInt32(mInt32)
	f.MergeInt32s(mInt32s)
	f.MergeInt64(mInt64)
	f.MergeInt64s(mInt64s)
	f.MergeFloat32(mFloat32)
	f.MergeFloat32s(mFloat32s)
	f.MergeVector2(mVector2)
	f.MergeVector2s(mVector2s)
	f.MergeMapInt32(mMapInt32)
	f.MergeMapInt64(mMapInt64)
	f.MergeMapInt32ToInt64(mMapInt32ToInt64)
	f.MergeMapInt64ToInt32(mMapInt64ToInt32)
	f.MergeMapInt64ToInt64(mMapInt64ToInt64)
	f.MergeAny(mAny)
	return nil
}
