package data

import (
	"95eh.com/eg/utils"
)

func NewVector3s(v []utils.Vec3) *Vector3s {
	return &Vector3s{
		v:        v,
		listener: make(map[int64]utils.ActionVec3s),
	}
}

type Vector3s struct {
	v        []utils.Vec3
	listener map[int64]utils.ActionVec3s
}

func (a *Vector3s) Get() []utils.Vec3 {
	return a.v
}

func (a *Vector3s) Set(v []utils.Vec3, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Vector3s) Bind(action utils.ActionVec3s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Vector3s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Vector3s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionVec3s)
}
