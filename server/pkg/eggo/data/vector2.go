package data

import (
	"95eh.com/eg/utils"
)

func NewVector2(v utils.Vec2) *Vector2 {
	return &Vector2{
		v:        v,
		listener: make(map[int64]utils.ActionVec2),
	}
}

type Vector2 struct {
	v        utils.Vec2
	listener map[int64]utils.ActionVec2
}

func (a *Vector2) Get() utils.Vec2 {
	return a.v
}

func (a *Vector2) Set(v utils.Vec2, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Vector2) Bind(action utils.ActionVec2) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Vector2) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Vector2) ClearListeners() {
	a.listener = make(map[int64]utils.ActionVec2)
}
