package data

import "95eh.com/eg/utils"

func NewInt64(v int64) *Int64 {
	return &Int64{
		v:        v,
		listener: make(map[int64]utils.ActionInt64),
	}
}

type Int64 struct {
	v        int64
	listener map[int64]utils.ActionInt64
}

func (a *Int64) Get() int64 {
	return a.v
}

func (a *Int64) Set(v int64, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Int64) Bind(action utils.ActionInt64) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Int64) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Int64) ClearListeners() {
	a.listener = make(map[int64]utils.ActionInt64)
}
