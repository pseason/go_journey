package data

import "95eh.com/eg/utils"

func NewInt64s(v []int64) *Int64s {
	return &Int64s{
		v:        v,
		listener: make(map[int64]utils.ActionInt64s),
	}
}

type Int64s struct {
	v        []int64
	listener map[int64]utils.ActionInt64s
}

func (a *Int64s) Get() []int64 {
	return a.v
}

func (a *Int64s) Set(v []int64, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Int64s) Bind(action utils.ActionInt64s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Int64s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Int64s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionInt64s)
}
