package data

import "95eh.com/eg/utils"

func NewFloat32(v float32) *Float32 {
	return &Float32{
		v:        v,
		listener: make(map[int64]utils.ActionFloat32),
	}
}

type Float32 struct {
	v        float32
	listener map[int64]utils.ActionFloat32
}

func (a *Float32) Get() float32 {
	return a.v
}

func (a *Float32) Set(v float32, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Float32) Bind(action utils.ActionFloat32) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Float32) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Float32) ClearListeners() {
	a.listener = make(map[int64]utils.ActionFloat32)
}
