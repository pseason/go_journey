package data

import (
	"95eh.com/eg/utils"
	"time"
)

type data struct {
	data Map
}

func NewData(m Map) IData {
	if m == nil {
		m = make(Map)
	}
	return &data{
		data: m,
	}
}

func (d *data) Pop(key TField) (v interface{}, ok bool) {
	v, ok = d.data[key]
	if ok {
		delete(d.data, key)
	}
	return
}

func (d *data) Remove(key TField) {
	delete(d.data, key)
}

func (d *data) Replace(key TField, val interface{}) (oldVal interface{}, ok bool) {
	oldVal, ok = d.data[key]
	d.data[key] = val
	return
}

func (d *data) Update(key TField, action utils.ActionAnyToAny) (ok bool) {
	oldVal, ok := d.data[key]
	d.data[key] = action(oldVal)
	return
}

func (d *data) Set(key TField, val interface{}) {
	d.data[key] = val
}

func (d *data) Get(key TField) (v interface{}, ok bool) {
	v, ok = d.data[key]
	return
}

func (d *data) Has(key TField) (ok bool) {
	_, ok = d.data[key]
	return
}

func (d *data) Reset(data Map) {
	if data == nil {
		data = make(Map, 16)
	}
	d.data = data
}

func (d *data) Bool(key TField) (bool, bool) {
	o, ok := d.Get(key)
	if !ok {
		return false, false
	}
	v, ok := o.(bool)
	if !ok {
		return false, false
	}
	return v, true
}

func (d *data) MustBool(key TField, def bool) bool {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(bool)
}

func (d *data) Bools(key TField) ([]bool, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]bool)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustBools(key TField, def []bool) []bool {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]bool)
}

func (d *data) Int8(key TField) (int8, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(int8)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustInt8(key TField, def int8) int8 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(int8)
}

func (d *data) Int8s(key TField) ([]int8, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]int8)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustInt8s(key TField, def []int8) []int8 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]int8)
}

func (d *data) Int(key TField) (int, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(int)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustInt(key TField, def int) int {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(int)
}

func (d *data) Ints(key TField) ([]int, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]int)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustInts(key TField, def []int) []int {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]int)
}

func (d *data) Int16(key TField) (int16, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(int16)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustInt16(key TField, def int16) int16 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(int16)
}

func (d *data) Int16s(key TField) ([]int16, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]int16)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustInt16s(key TField, def []int16) []int16 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]int16)
}

func (d *data) Int32(key TField) (int32, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(int32)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustInt32(key TField, def int32) int32 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(int32)
}

func (d *data) Int32s(key TField) ([]int32, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]int32)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustInt32s(key TField, def []int32) []int32 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]int32)
}

func (d *data) Int64(key TField) (int64, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(int64)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustInt64(key TField, def int64) int64 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(int64)
}

func (d *data) Int64s(key TField) ([]int64, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]int64)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustInt64s(key TField, def []int64) []int64 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]int64)
}

func (d *data) Uint(key TField) (uint, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(uint)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustUint(key TField, def uint) uint {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(uint)
}

func (d *data) Uints(key TField) ([]uint, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]uint)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustUints(key TField, def []uint) []uint {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]uint)
}

func (d *data) Uint8(key TField) (uint8, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(uint8)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustUint8(key TField, def uint8) uint8 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(uint8)
}

func (d *data) Uint8s(key TField) ([]uint8, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]uint8)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustUint8s(key TField, def []uint8) []uint8 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]uint8)
}

func (d *data) Uint16(key TField) (uint16, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(uint16)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustUint16(key TField, def uint16) uint16 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(uint16)
}

func (d *data) Uint16s(key TField) ([]uint16, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]uint16)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustUint16s(key TField, def []uint16) []uint16 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]uint16)
}

func (d *data) Uint32(key TField) (uint32, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(uint32)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustUint32(key TField, def uint32) uint32 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(uint32)
}

func (d *data) Uint32s(key TField) ([]uint32, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]uint32)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustUint32s(key TField, def []uint32) []uint32 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]uint32)
}

func (d *data) Uint64(key TField) (uint64, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(uint64)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustUint64(key TField, def uint64) uint64 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(uint64)
}

func (d *data) Uint64s(key TField) ([]uint64, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]uint64)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustUint64s(key TField, def []uint64) []uint64 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]uint64)
}

func (d *data) Float32(key TField) (float32, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(float32)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustFloat32(key TField, def float32) float32 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(float32)
}

func (d *data) Float32s(key TField) ([]float32, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]float32)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustFloat32s(key TField, def []float32) []float32 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]float32)
}

func (d *data) Float64(key TField) (float64, bool) {
	o, ok := d.Get(key)
	if !ok {
		return 0, false
	}
	v, ok := o.(float64)
	if !ok {
		return 0, false
	}
	return v, true
}

func (d *data) MustFloat64(key TField, def float64) float64 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(float64)
}

func (d *data) Float64s(key TField) ([]float64, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]float64)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustFloat64s(key TField, def []float64) []float64 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]float64)
}

func (d *data) String(key TField) (string, bool) {
	o, ok := d.Get(key)
	if !ok {
		return "", false
	}
	v, ok := o.(string)
	if !ok {
		return "", false
	}
	return v, true
}

func (d *data) MustString(key TField, def string) string {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(string)
}

func (d *data) Strings(key TField) ([]string, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]string)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustStrings(key TField, def []string) []string {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]string)
}

func (d *data) Time(key TField) (*time.Time, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.(*time.Time)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustTime(key TField, def *time.Time) *time.Time {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(*time.Time)
}

func (d *data) Times(key TField) ([]*time.Time, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]*time.Time)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustTimes(key TField, def []*time.Time) []*time.Time {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]*time.Time)
}

func (d *data) Vec2(key TField) (utils.Vec2, bool) {
	o, ok := d.Get(key)
	if !ok {
		return utils.Vec2Zero, false
	}
	v, ok := o.(utils.Vec2)
	if !ok {
		return utils.Vec2Zero, false
	}
	return v, true
}

func (d *data) MustVec2(key TField, def utils.Vec2) utils.Vec2 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(utils.Vec2)
}

func (d *data) Vec2s(key TField) ([]utils.Vec2, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]utils.Vec2)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustVec2s(key TField, def []utils.Vec2) []utils.Vec2 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]utils.Vec2)
}

func (d *data) Vec3(key TField) (utils.Vec3, bool) {
	o, ok := d.Get(key)
	if !ok {
		return utils.Vec3Zero, false
	}
	v, ok := o.(utils.Vec3)
	if !ok {
		return utils.Vec3Zero, false
	}
	return v, true
}

func (d *data) MustVec3(key TField, def utils.Vec3) utils.Vec3 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.(utils.Vec3)
}

func (d *data) Vec3s(key TField) ([]utils.Vec3, bool) {
	o, ok := d.Get(key)
	if !ok {
		return nil, false
	}
	v, ok := o.([]utils.Vec3)
	if !ok {
		return nil, false
	}
	return v, true
}

func (d *data) MustVec3s(key TField, def []utils.Vec3) []utils.Vec3 {
	o, ok := d.Get(key)
	if !ok {
		d.Set(key, def)
		return def
	}
	return o.([]utils.Vec3)
}

func (d *data) Map() Map {
	m := make(Map, len(d.data))
	for field, obj := range d.data {
		m[field] = obj
	}
	return m
}
