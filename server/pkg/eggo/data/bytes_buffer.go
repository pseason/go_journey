package data

import (
	"95eh.com/eg/utils"
	"encoding/gob"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/gogo/protobuf/proto"
	"io"
	"math"
	"sync"
)

const (
	_MinBytesCap = 64
)

var (
	_BytesMapMtx     sync.RWMutex
	_BytesMap        = make(map[uint32]*sync.Pool, 8)
	_BytesBufferPool = &sync.Pool{
		New: func() interface{} {
			return &BytesBuffer{}
		}}
)

func SpawnBytesWithLen(l uint32) (bytes []byte) {
	if l < _MinBytesCap {
		l = _MinBytesCap
	} else {
		l = utils.NextPowerOfTwo(l)
	}
	_BytesMapMtx.RLock()
	pool, ok := _BytesMap[l]
	if ok {
		bytes = pool.Get().([]byte)
		_BytesMapMtx.RUnlock()
		return
	}
	_BytesMapMtx.RUnlock()
	f := func() interface{} {
		return make([]byte, l)
	}
	pool = &sync.Pool{New: f}
	bytes = f().([]byte)
	_BytesMapMtx.Lock()
	_BytesMap[l] = pool
	_BytesMapMtx.Unlock()
	return
}

func SpawnBytes() []byte {
	return SpawnBytesWithLen(_MinBytesCap)
}

func RecycleBytes(bytes []byte) {
	l := uint32(len(bytes))
	ll := utils.NextPowerOfTwo(l)
	if l < ll {
		l = ll >> 1
	}
	_BytesMapMtx.RLock()
	pool, ok := _BytesMap[l]
	if ok {
		pool.Put(bytes[:l])
		_BytesMapMtx.RUnlock()
		return
	}
	_BytesMapMtx.RUnlock()

	pool = &sync.Pool{
		New: func() interface{} {
			return make([]byte, l)
		}}
	pool.Put(bytes[:l])
	_BytesMapMtx.Lock()
	_BytesMap[l] = pool
	_BytesMapMtx.Unlock()
}

func CopyBytes(src []byte) (dst []byte, l int) {
	dst = SpawnBytesWithLen(uint32(len(src)))
	l = copy(dst, src)
	return
}

type BytesBuffer struct {
	pos    uint32
	length uint32
	bytes  []byte
}

func NewBytesBuffer() *BytesBuffer {
	return NewBytesBufferWithData(SpawnBytes())
}

func NewBytesBufferWithLen(l uint32) *BytesBuffer {
	return NewBytesBufferWithData(SpawnBytesWithLen(l))
}

func NewBytesBufferWithData(data []byte) *BytesBuffer {
	bb := &BytesBuffer{
		pos:    0,
		length: uint32(len(data)),
		bytes:  data,
	}
	return bb
}

func SpawnBytesBufferWithLen(l uint32) *BytesBuffer {
	bb := _BytesBufferPool.Get().(*BytesBuffer)
	bb.pos = 0
	bb.length = l
	bb.bytes = SpawnBytesWithLen(l)
	return bb
}

func SpawnBytesBuffer() *BytesBuffer {
	return SpawnBytesBufferWithLen(_MinBytesCap)
}

func RecycleBytesBuffer(bb *BytesBuffer) {
	RecycleBytes(bb.bytes)
	_BytesBufferPool.Put(bb)
}

func (b *BytesBuffer) Reset() {
	b.pos = 0
}

func (b *BytesBuffer) SetBytes(bytes []byte) {
	b.pos = 0
	b.length = uint32(len(bytes))
	b.bytes = bytes
}

func (b *BytesBuffer) Pos() uint32 {
	return b.pos
}

func (b *BytesBuffer) SetPos(v uint32) {
	if v < b.length {
		b.pos = v
	} else {
		b.pos = b.length
	}
}

func (b *BytesBuffer) Available() uint32 {
	return b.length - b.pos
}

func (b *BytesBuffer) WAll() []byte {
	return b.bytes[:b.pos]
}

func (b *BytesBuffer) Copy() []byte {
	bytes := make([]byte, len(b.bytes))
	copy(bytes, b.bytes)
	return bytes
}

func (b *BytesBuffer) Length() uint32 {
	return b.length
}

func (b *BytesBuffer) Cap() uint32 {
	return uint32(cap(b.bytes))
}

func (b *BytesBuffer) WBool(v bool) {
	if v {
		b.WUint8(1)
	} else {
		b.WUint8(0)
	}
}

func (b *BytesBuffer) WBools(v []bool) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WBool(d)
	}
}

func (b *BytesBuffer) Write(v []byte) (int, error) {
	l := uint32(len(v))
	if v == nil || l == 0 {
		return 0, nil
	}
	b.length = b.pos + l
	c := b.Cap()
	if c < b.length {
		bytes := SpawnBytesWithLen(utils.NextPowerOfTwo(b.length))
		copy(bytes, b.bytes[:b.length])
		RecycleBytes(b.bytes)
		b.bytes = bytes
	}
	l = b.pos
	b.pos = b.length
	return copy(b.bytes[l:], v), nil
}

func (b *BytesBuffer) WBytesWithLen(v []byte) {
	b.WUint16(uint16(len(v)))
	b.Write(v)
}

func (b *BytesBuffer) WUint8(v uint8) {
	b.Write([]uint8{v})
}

func (b *BytesBuffer) WUint8s(v []uint8) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WUint8(d)
	}
}

func (b *BytesBuffer) WInt8(v int8) {
	b.WUint8(uint8(v))
}

func (b *BytesBuffer) WInt8s(v []int8) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WInt8(d)
	}
}

func (b *BytesBuffer) WUint16(v uint16) {
	b.Write([]byte{byte(v >> 8), byte(v)})
}

func (b *BytesBuffer) WUint16s(v []uint16) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WUint16(d)
	}
}

func (b *BytesBuffer) WInt16(v int16) {
	b.WUint16(uint16(v))
}

func (b *BytesBuffer) WInt16s(v []int16) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WInt16(d)
	}
}

func (b *BytesBuffer) WUint32(v uint32) {
	b.Write([]byte{byte(v >> 24), byte(v >> 16), byte(v >> 8), byte(v)})
}

func (b *BytesBuffer) WUint32s(v []uint32) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WUint32(d)
	}
}

func (b *BytesBuffer) WInt32(v int32) {
	b.WUint32(uint32(v))
}

func (b *BytesBuffer) WInt32s(v []int32) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WInt32(d)
	}
}

func (b *BytesBuffer) WUint64(v uint64) {
	b.Write([]byte{byte(v >> 56), byte(v >> 48), byte(v >> 40), byte(v >> 32), byte(v >> 24), byte(v >> 16), byte(v >> 8), byte(v)})
}

func (b *BytesBuffer) WUint64s(v []uint64) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WUint64(d)
	}
}

func (b *BytesBuffer) WInt64(v int64) {
	b.WUint64(uint64(v))
}

func (b *BytesBuffer) WInt64s(v []int64) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WInt64(d)
	}
}

func (b *BytesBuffer) WFloat32(v float32) {
	b.WUint32(math.Float32bits(v))
}

func (b *BytesBuffer) WFloat32s(v []float32) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WFloat32(d)
	}
}

func (b *BytesBuffer) WFloat64(v float64) {
	b.WUint64(math.Float64bits(v))
}

func (b *BytesBuffer) WFloat64s(v []float64) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WFloat64(d)
	}
}

func (b *BytesBuffer) WVec2(v utils.Vec2) {
	b.WFloat32(v.X)
	b.WFloat32(v.Y)
}

func (b *BytesBuffer) WVec2s(v []utils.Vec2) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WVec2(d)
	}
}

func (b *BytesBuffer) WVec3(v utils.Vec3) {
	b.WFloat32(v.X)
	b.WFloat32(v.Y)
	b.WFloat32(v.Z)
}

func (b *BytesBuffer) WVec3s(v []utils.Vec3) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WVec3(d)
	}
}

func (b *BytesBuffer) WString(v string) {
	_v := []byte(v)
	b.WUint16(uint16(len(_v)))
	b.Write(_v)
}

func (b *BytesBuffer) WStrings(v []string) {
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		b.WString(d)
	}
}

func (b *BytesBuffer) WStringNoLen(v string) {
	b.Write([]byte(v))
	b.pos = b.Length()
}

func (b *BytesBuffer) WAny(any interface{}) error {
	bytes, err := GobMarshal(any)
	if err != nil {
		return err
	}
	b.WBytesWithLen(bytes)
	return nil
}

func (b *BytesBuffer) WAnys(v []interface{}) error {
	p := b.pos
	b.WUint16(uint16(len(v)))
	for _, d := range v {
		err := b.WAny(d)
		if err != nil {
			b.pos = p
			return err
		}
	}
	return nil
}

func (b *BytesBuffer) err(l uint32) error {
	return errors.New(fmt.Sprintf("索引%d超出长度%d", b.pos+l, b.Length()))
}

func (b *BytesBuffer) RBool() (v bool, err error) {
	if b.Available() >= 1 {
		v = b.bytes[b.pos] == byte(1)
		b.pos++
	} else {
		err = b.err(1)
	}
	return
}

func (b *BytesBuffer) RBools() (v []bool, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]bool, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RBool()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RBytes(l uint32) (v []byte, err error) {
	if b.Available() < l {
		err = b.err(l)
		return
	}
	v = SpawnBytesWithLen(l)
	copy(b.bytes[b.pos:], v)
	b.pos += l
	return
}

func (b *BytesBuffer) RBytesWithLen() (v []byte, err error) {
	if l, err := b.RUint16(); err == nil {
		return b.RBytes(uint32(l))
	} else {
		return nil, err
	}
}

// RAll 读取剩余所有字节
func (b *BytesBuffer) RAll() (v []byte) {
	v = b.bytes[b.pos:b.length]
	b.pos = b.length
	return
}

func (b *BytesBuffer) Read(p []byte) (n int, err error) {
	if b.pos == b.length {
		if len(p) == 0 {
			return 0, nil
		}
		return 0, io.EOF
	}
	n = copy(p, b.bytes[b.pos:b.length])
	b.pos = b.length
	return n, nil
}

func (b *BytesBuffer) RUint8() (v uint8, err error) {
	if b.Available() >= 1 {
		v = b.bytes[b.pos]
		b.pos++
	} else {
		err = b.err(1)
	}
	return
}

func (b *BytesBuffer) RUint8s() (v []uint8, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]uint8, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RUint8()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RInt8() (v int8, err error) {
	v1, e := b.RUint8()
	if e == nil {
		v = int8(v1)
	} else {
		err = e
	}
	return
}

func (b *BytesBuffer) RInt8s() (v []int8, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]int8, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RInt8()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RUint16() (v uint16, err error) {
	if b.Available() >= 2 {
		v = uint16(b.bytes[b.pos])<<8 | uint16(b.bytes[b.pos+1])
		b.pos += 2
	} else {
		err = b.err(2)
	}
	return
}

func (b *BytesBuffer) RUint16s() (v []uint16, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]uint16, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RUint16()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RInt16() (v int16, err error) {
	v1, e := b.RUint16()
	if e == nil {
		v = int16(v1)
	} else {
		err = e
	}
	return
}

func (b *BytesBuffer) RInt16s() (v []int16, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]int16, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RInt16()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RUint32() (v uint32, err error) {
	if b.Available() >= 4 {
		v = uint32(b.bytes[b.pos])<<24 | uint32(b.bytes[b.pos+1])<<16 | uint32(b.bytes[b.pos+2])<<8 | uint32(b.bytes[b.pos+3])
		b.pos += 4
	} else {
		err = b.err(4)
	}
	return
}

func (b *BytesBuffer) RUint32s() (v []uint32, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]uint32, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RUint32()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RInt32() (v int32, err error) {
	v1, e := b.RUint32()
	if e == nil {
		v = int32(v1)
	} else {
		err = e
	}
	return
}

func (b *BytesBuffer) RInt32s() (v []int32, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]int32, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RInt32()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RUint64() (v uint64, err error) {
	if b.Available() >= 8 {
		v = uint64(b.bytes[b.pos])<<56 | uint64(b.bytes[b.pos+1])<<48 | uint64(b.bytes[b.pos+2])<<40 | uint64(b.bytes[b.pos+3])<<32 | uint64(b.bytes[b.pos+4])<<24 | uint64(b.bytes[b.pos+5])<<16 | uint64(b.bytes[b.pos+6])<<8 | uint64(b.bytes[b.pos+7])
		b.pos += 8
	} else {
		err = b.err(8)
	}
	return
}

func (b *BytesBuffer) RUint64s() (v []uint64, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]uint64, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RUint64()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RInt64() (v int64, err error) {
	v1, e := b.RUint64()
	if e == nil {
		v = int64(v1)
	} else {
		err = e
	}
	return
}

func (b *BytesBuffer) RInt64s() (v []int64, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]int64, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RInt64()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RFloat32() (v float32, err error) {
	v1, e := b.RUint32()
	if e == nil {
		v = math.Float32frombits(v1)
	} else {
		err = e
	}
	return
}

func (b *BytesBuffer) RFloat32s() (v []float32, err error) {
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		return
	}
	v = make([]float32, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RFloat32()
		if err != nil {
			return
		}
	}
	return
}

func (b *BytesBuffer) RFloat64() (v float64, err error) {
	v1, e := b.RUint64()
	if e == nil {
		v = math.Float64frombits(v1)
	} else {
		err = e
	}
	return
}

func (b *BytesBuffer) RFloat64s() (v []float64, err error) {
	p := b.pos
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		b.pos = p
		return
	}
	v = make([]float64, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RFloat64()
		if err != nil {
			b.pos = p
			return
		}
	}
	return
}

func (b *BytesBuffer) RVec2() (v utils.Vec2, err error) {
	x, err := b.RFloat32()
	if err != nil {
		return utils.Vec2Zero, err
	}
	y, err := b.RFloat32()
	if err != nil {
		return utils.Vec2Zero, err
	}
	return utils.Vec2{X: x, Y: y}, nil
}

func (b *BytesBuffer) RVec2s() (v []utils.Vec2, err error) {
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		return
	}
	v = make([]utils.Vec2, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RVec2()
		if err != nil {
			return
		}
	}
	return
}

func (b *BytesBuffer) RVec3() (v utils.Vec3, err error) {
	x, err := b.RFloat32()
	if err != nil {
		return utils.Vec3Zero, err
	}
	y, err := b.RFloat32()
	if err != nil {
		return utils.Vec3Zero, err
	}
	z, err := b.RFloat32()
	if err != nil {
		return utils.Vec3Zero, err
	}
	return utils.Vec3{X: x, Y: y, Z: z}, nil
}

func (b *BytesBuffer) RVec3s() (v []utils.Vec3, err error) {
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		return
	}
	v = make([]utils.Vec3, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RVec3()
		if err != nil {
			return
		}
	}
	return
}

func (b *BytesBuffer) RString() (v string, err error) {
	var length uint16
	length, err = b.RUint16()
	if err != nil {
		return
	}
	l := uint32(length)
	if b.Available() < l {
		err = b.err(l)
		return
	}
	v = string(b.bytes[b.pos : b.pos+l])
	b.pos += l
	return
}

func (b *BytesBuffer) RStrings() (v []string, err error) {
	var l uint16
	l, err = b.RUint16()
	if err != nil {
		return
	}
	v = make([]string, l)
	for i := uint16(0); i < l; i++ {
		v[i], err = b.RString()
		if err != nil {
			return
		}
	}
	return
}

func (b *BytesBuffer) RStringNoLen() (v string) {
	bs := b.RAll()
	v = string(bs)
	return
}

func (b *BytesBuffer) RAny(v interface{}) (err error) {
	var bytes []byte
	bytes, err = b.RBytesWithLen()
	if err != nil {
		return
	}
	err = GobUnmarshal(bytes, v)
	return
}

func (b *BytesBuffer) ToHex() string {
	str := hex.EncodeToString(b.bytes)
	l := len(str)
	s := ""
	i := 0
	for i < l {
		s += str[i : i+2]
		s += " "
		i += 2
	}
	return s
}

func (b *BytesBuffer) WMapInt32(m map[string]int32) {
	b.WUint8(uint8(len(m)))
	for k, v := range m {
		b.WString(k)
		b.WInt32(v)
	}
}

func (b *BytesBuffer) RMapInt32() (m map[string]int32, err error) {
	var (
		l uint8
		k string
		v int32
	)
	l, err = b.RUint8()
	if err != nil {
		return
	}
	m = make(map[string]int32, l)
	for i := uint8(0); i < l; i++ {
		k, err = b.RString()
		if err != nil {
			return
		}
		v, err = b.RInt32()
		if err != nil {
			return
		}
		m[k] = v
	}
	return
}

func (b *BytesBuffer) WMapInt64(m map[string]int64) {
	b.WUint8(uint8(len(m)))
	for k, v := range m {
		b.WString(k)
		b.WInt64(v)
	}
}

func (b *BytesBuffer) RMapInt64() (m map[string]int64, err error) {
	var (
		l uint8
		k string
		v int64
	)
	l, err = b.RUint8()
	if err != nil {
		return
	}
	m = make(map[string]int64, l)
	for i := uint8(0); i < l; i++ {
		k, err = b.RString()
		if err != nil {
			return
		}
		v, err = b.RInt64()
		if err != nil {
			return
		}
		m[k] = v
	}
	return
}

func (b *BytesBuffer) WMapInt32ToInt32(m map[int32]int32) {
	b.WUint8(uint8(len(m)))
	for k, v := range m {
		b.WInt32(k)
		b.WInt32(v)
	}
}

func (b *BytesBuffer) RMapInt32ToInt32() (m map[int32]int32, err error) {
	var (
		l uint8
		k int32
		v int32
	)
	l, err = b.RUint8()
	if err != nil {
		return
	}
	m = make(map[int32]int32, l)
	for i := uint8(0); i < l; i++ {
		k, err = b.RInt32()
		if err != nil {
			return
		}
		v, err = b.RInt32()
		if err != nil {
			return
		}
		m[k] = v
	}
	return
}

func (b *BytesBuffer) WMapInt32ToInt64(m map[int32]int64) {
	b.WUint8(uint8(len(m)))
	for k, v := range m {
		b.WInt32(k)
		b.WInt64(v)
	}
}

func (b *BytesBuffer) RMapInt32ToInt64() (m map[int32]int64, err error) {
	var (
		l uint8
		k int32
		v int64
	)
	l, err = b.RUint8()
	if err != nil {
		return
	}
	m = make(map[int32]int64, l)
	for i := uint8(0); i < l; i++ {
		k, err = b.RInt32()
		if err != nil {
			return
		}
		v, err = b.RInt64()
		if err != nil {
			return
		}
		m[k] = v
	}
	return
}

func (b *BytesBuffer) WMapInt64ToInt32(m map[int64]int32) {
	b.WUint8(uint8(len(m)))
	for k, v := range m {
		b.WInt64(k)
		b.WInt32(v)
	}
}

func (b *BytesBuffer) RMapInt64ToInt32() (m map[int64]int32, err error) {
	var (
		l uint8
		k int64
		v int32
	)
	l, err = b.RUint8()
	if err != nil {
		return
	}
	m = make(map[int64]int32, l)
	for i := uint8(0); i < l; i++ {
		k, err = b.RInt64()
		if err != nil {
			return
		}
		v, err = b.RInt32()
		if err != nil {
			return
		}
		m[k] = v
	}
	return
}

func (b *BytesBuffer) WMapInt64ToInt64(m map[int64]int64) {
	b.WUint8(uint8(len(m)))
	for k, v := range m {
		b.WInt64(k)
		b.WInt64(v)
	}
}

func (b *BytesBuffer) RMapInt64ToInt64() (m map[int64]int64, err error) {
	var (
		l uint8
		k int64
		v int64
	)
	l, err = b.RUint8()
	if err != nil {
		return
	}
	m = make(map[int64]int64, l)
	for i := uint8(0); i < l; i++ {
		k, err = b.RInt64()
		if err != nil {
			return
		}
		v, err = b.RInt64()
		if err != nil {
			return
		}
		m[k] = v
	}
	return
}

func (b *BytesBuffer) WMapStrStr(m map[string]string) {
	b.WUint8(uint8(len(m)))
	for k, v := range m {
		b.WString(k)
		b.WString(v)
	}
}

func (b *BytesBuffer) RMapStrStr() (m map[string]string, err error) {
	var (
		l uint8
		k string
		v string
	)
	l, err = b.RUint8()
	if err != nil {
		return
	}
	m = make(map[string]string, l)
	for i := uint8(0); i < l; i++ {
		k, err = b.RString()
		if err != nil {
			return
		}
		v, err = b.RString()
		if err != nil {
			return
		}
		m[k] = v
	}
	return
}

func (b *BytesBuffer) Dispose() {
	RecycleBytes(b.bytes)
}

func GobMarshal(v interface{}) ([]byte, error) {
	buffer := NewBytesBuffer()
	err := gob.NewEncoder(buffer).Encode(v)
	if err != nil {
		return nil, err
	}
	return buffer.WAll(), err
}

func GobUnmarshal(data []byte, v interface{}) error {
	return gob.NewDecoder(NewBytesBufferWithData(data)).Decode(v)
}

func init() {
	gob.Register(map[string]interface{}{})
	gob.Register([]interface{}{})
}

func PbMarshal(pkt interface{}) ([]byte, error) {
	return proto.Marshal(pkt.(proto.Message))
}

func PbUnmarshal(data []byte, pkt interface{}) error {
	return proto.Unmarshal(data, pkt.(proto.Message))
}
