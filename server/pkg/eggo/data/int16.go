package data

import "95eh.com/eg/utils"

func NewInt16(v int16) *Int16 {
	return &Int16{
		v:        v,
		listener: make(map[int64]utils.ActionInt16),
	}
}

type Int16 struct {
	v        int16
	listener map[int64]utils.ActionInt16
}

func (a *Int16) Get() int16 {
	return a.v
}

func (a *Int16) Set(v int16, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Int16) Bind(action utils.ActionInt16) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Int16) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Int16) ClearListeners() {
	a.listener = make(map[int64]utils.ActionInt16)
}
