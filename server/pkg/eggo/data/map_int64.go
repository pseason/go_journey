package data

import "95eh.com/eg/utils"

func NewMapInt64(v map[TField]int64) *MapInt64 {
	return &MapInt64{
		v:        v,
		listener: make(map[int64]ActionMapInt64),
	}
}

type MapInt64 struct {
	v        map[TField]int64
	listener map[int64]ActionMapInt64
}

func (a *MapInt64) Get() map[TField]int64 {
	return a.v
}

func (a *MapInt64) Set(v map[TField]int64, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *MapInt64) Bind(action ActionMapInt64) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *MapInt64) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *MapInt64) ClearListeners() {
	a.listener = make(map[int64]ActionMapInt64)
}
