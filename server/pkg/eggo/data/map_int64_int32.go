package data

import "95eh.com/eg/utils"

func NewMapInt64ToInt32(v map[int64]int32) *MapInt64ToInt32 {
	return &MapInt64ToInt32{
		v:        v,
		listener: make(map[int64]utils.ActionMapInt64ToInt32),
	}
}

type MapInt64ToInt32 struct {
	v        map[int64]int32
	listener map[int64]utils.ActionMapInt64ToInt32
}

func (a *MapInt64ToInt32) Get() map[int64]int32 {
	return a.v
}

func (a *MapInt64ToInt32) Set(v map[int64]int32, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *MapInt64ToInt32) Bind(action utils.ActionMapInt64ToInt32) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *MapInt64ToInt32) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *MapInt64ToInt32) ClearListeners() {
	a.listener = make(map[int64]utils.ActionMapInt64ToInt32)
}
