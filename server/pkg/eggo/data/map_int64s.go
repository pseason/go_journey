package data

import "95eh.com/eg/utils"

func NewMapInt64s(v map[TField][]int64) *MapInt64s {
	return &MapInt64s{
		v:        v,
		listener: make(map[int64]ActionMapInt64s),
	}
}

type MapInt64s struct {
	v        map[TField][]int64
	listener map[int64]ActionMapInt64s
}

func (a *MapInt64s) Get() map[TField][]int64 {
	return a.v
}

func (a *MapInt64s) Set(v map[TField][]int64, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *MapInt64s) Bind(action ActionMapInt64s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *MapInt64s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *MapInt64s) ClearListeners() {
	a.listener = make(map[int64]ActionMapInt64s)
}
