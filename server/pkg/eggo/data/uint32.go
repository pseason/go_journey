package data

import "95eh.com/eg/utils"

func NewUint32(v uint32) *Uint32 {
	return &Uint32{
		v:        v,
		listener: make(map[int64]utils.ActionUint32),
	}
}

type Uint32 struct {
	v        uint32
	listener map[int64]utils.ActionUint32
}

func (a *Uint32) Get() uint32 {
	return a.v
}

func (a *Uint32) Set(v uint32, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Uint32) Bind(action utils.ActionUint32) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Uint32) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Uint32) ClearListeners() {
	a.listener = make(map[int64]utils.ActionUint32)
}
