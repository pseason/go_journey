package data

import "95eh.com/eg/utils"

func NewUint16s(v []uint16) *Uint16s {
	return &Uint16s{
		v:        v,
		listener: make(map[int64]utils.ActionUint16s),
	}
}

type Uint16s struct {
	v        []uint16
	listener map[int64]utils.ActionUint16s
}

func (a *Uint16s) Get() []uint16 {
	return a.v
}

func (a *Uint16s) Set(v []uint16, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Uint16s) Bind(action utils.ActionUint16s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Uint16s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Uint16s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionUint16s)
}
