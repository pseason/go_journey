package data

import "95eh.com/eg/utils"

func NewMapInt64ToInt64(v map[int64]int64) *MapInt64ToInt64 {
	return &MapInt64ToInt64{
		v:        v,
		listener: make(map[int64]utils.ActionMapInt64ToInt64),
	}
}

type MapInt64ToInt64 struct {
	v        map[int64]int64
	listener map[int64]utils.ActionMapInt64ToInt64
}

func (a *MapInt64ToInt64) Get() map[int64]int64 {
	return a.v
}

func (a *MapInt64ToInt64) Set(v map[int64]int64, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *MapInt64ToInt64) Bind(action utils.ActionMapInt64ToInt64) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *MapInt64ToInt64) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *MapInt64ToInt64) ClearListeners() {
	a.listener = make(map[int64]utils.ActionMapInt64ToInt64)
}
