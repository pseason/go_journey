package data

import (
	"95eh.com/eg/utils"
	"errors"
	"fmt"
	"strconv"
	"strings"
)

const (
	_ObjectSlicePrefix = '$'
	_ObjectPathSep     = "."
)

var (
	_ErrWrongKey          = errors.New("key must begin with $ and int")
	_NotSupportType       = errors.New("not support type")
	_NotExitKey           = errors.New("not exist key")
	_WatchActionCannotNil = errors.New("watch action can not nil")
)

func testSliceKey(key string) (index int, ok bool) {
	if len(key) < 2 || key[0] != _ObjectSlicePrefix {
		return -1, false
	}
	k, err := strconv.Atoi(key[1:])
	if err != nil {
		return -1, false
	}
	return k, true
}

func pathToNodes(path string) []string {
	return strings.Split(path, _ObjectPathSep)
}

func NewObject(data map[string]interface{}) *object {
	if data == nil {
		data = make(map[string]interface{}, 16)
	}
	return &object{
		data:                 data,
		pathToWatchers:       make(map[string]map[int64]utils.ActionBoolStrAny),
		pathToPrefixWatchers: make(map[string]map[int64]utils.ActionBoolStrAny),
		globalWatchers:       make(map[int64]utils.ActionAny),
		watcherIdToPath:      make(map[int64]string),
	}
}

type object struct {
	data                 map[string]interface{}
	pathToWatchers       map[string]map[int64]utils.ActionBoolStrAny
	pathToPrefixWatchers map[string]map[int64]utils.ActionBoolStrAny
	globalWatchers       map[int64]utils.ActionAny
	watcherIdToPath      map[int64]string
}

func (o *object) Set(path string, v interface{}) error {
	data, err := write(o.data, v, pathToNodes(path))
	if err != nil {
		return err
	}
	o.data = data.(map[string]interface{})
	for p, watchers := range o.pathToWatchers {
		v, err := o.Get(p)
		if err != nil {
			for _, watcher := range watchers {
				watcher(false, p, nil)
			}
			continue
		}
		for _, watcher := range watchers {
			watcher(true, p, v)
		}
	}
	for p, watchers := range o.pathToPrefixWatchers {
		if !strings.HasPrefix(path, p) {
			continue
		}
		v, _ := o.Get(p)
		for _, watcher := range watchers {
			watcher(true, p, v)
		}
	}
	for _, watcher := range o.globalWatchers {
		watcher(o.data)
	}
	return nil
}

func (o *object) Del(path string) {
	var (
		data interface{}
		ok   bool
	)
	data = o.data
	nodes := pathToNodes(path)
	l := len(nodes) - 1
	for _, node := range nodes[:l] {
		switch obj := data.(type) {
		case map[string]interface{}:
			data, ok = obj[node]
			if !ok {
				return
			}
		case []interface{}:
			index, ok := testSliceKey(node)
			if !ok {
				return
			}
			if index < len(obj) {
				data = obj[index]
				continue
			}
			return
		}
	}
	node := nodes[l]
	switch obj := data.(type) {
	case map[string]interface{}:
		delete(obj, node)
		if len(obj) == 0 {
			o.Del(strings.Join(nodes[:l], _ObjectPathSep))
			return
		}
		for p, watchers := range o.pathToWatchers {
			for _, watcher := range watchers {
				watcher(false, p, nil)
			}
		}
		for p, watchers := range o.pathToPrefixWatchers {
			if !strings.HasPrefix(path, p) {
				continue
			}
			v, err := o.Get(p)
			if err == nil {
				for _, watcher := range watchers {
					watcher(true, p, v)
				}
			} else {
				for _, watcher := range watchers {
					watcher(false, p, nil)
				}
			}
		}
		for _, watcher := range o.globalWatchers {
			watcher(o.data)
		}
	case []interface{}:
		index, ok := testSliceKey(node)
		if !ok {
			return
		}
		obj = append(obj[:index], obj[index+1:]...)
		if len(obj) == 0 {
			o.Del(strings.Join(nodes[:l], _ObjectPathSep))
		} else {
			o.Set(strings.Join(nodes[:l], _ObjectPathSep), obj)
		}
	}
}

func (o *object) Merge(data map[string]interface{}) {
	for k, v := range data {
		o.data[k] = v
	}
}

func (o *object) Watch(action utils.ActionBoolStrAny, paths ...string) (map[string]int64, error) {
	return o.watch(o.pathToWatchers, action, paths...)
}

func (o *object) WatchPrefix(action utils.ActionBoolStrAny, paths ...string) (map[string]int64, error) {
	return o.watch(o.pathToPrefixWatchers, action, paths...)
}

func (o *object) watch(watchers map[string]map[int64]utils.ActionBoolStrAny, action utils.ActionBoolStrAny,
	paths ...string) (map[string]int64, error) {
	if action == nil {
		return nil, _WatchActionCannotNil
	}
	m := make(map[string]int64, len(paths))
	for _, p := range paths {
		v, err := o.Get(p)
		if err == nil {
			action(true, p, v)
		} else {
			action(false, p, nil)
		}
		m, ok := watchers[p]
		if !ok {
			m = make(map[int64]utils.ActionBoolStrAny)
			watchers[p] = m
		}
		id := utils.GenSnowflakeGlobalNodeId()
		m[id] = action
		o.watcherIdToPath[id] = p
	}
	return m, nil
}

func (o *object) Unwatch(id int64) {
	o.unwatch(o.pathToWatchers, id)
}

func (o *object) UnwatchPrefix(id int64) {
	o.unwatch(o.pathToPrefixWatchers, id)
}

func (o *object) unwatch(watchers map[string]map[int64]utils.ActionBoolStrAny, id int64) {
	path, ok := o.watcherIdToPath[id]
	if !ok {
		return
	}
	m, ok := watchers[path]
	if !ok {
		return
	}
	delete(m, id)
}

func (o *object) WatchGlobal(action utils.ActionAny) int64 {
	id := utils.GenSnowflakeGlobalNodeId()
	o.globalWatchers[id] = action
	return id
}

func (o *object) UnwatchGlobal(id int64) {
	delete(o.globalWatchers, id)
}

func write(data, v interface{}, nodes []string) (interface{}, error) {
	l := len(nodes)
	if l == 1 {
		key := nodes[0]
		switch data := data.(type) {
		case map[string]interface{}:
			data[key] = v
			return data, nil
		case []interface{}:
			index, ok := testSliceKey(key)
			if !ok {
				return data, _ErrWrongKey
			}
			l := len(data)
			for index < 0 {
				index += l
			}
			if index >= l {
				d := make([]interface{}, index+1)
				copy(d, data)
				data = d
			}
			data[index] = v
			return data, nil
		default:
			return data, fmt.Errorf("not support type %v", data)
		}
	}

	var err error
	key := nodes[0]

	switch data := data.(type) {
	case map[string]interface{}:
		_, dataOk := data[key]
		if dataOk {
			data[key], err = write(data[key], v, nodes[1:])
			return data, err
		}

		key1 := nodes[1]
		index, ok := testSliceKey(key1)
		if ok {
			data[key], err = write(make([]interface{}, index+1), v, nodes[1:])
			return data, err
		} else {
			data[key], err = write(make(map[string]interface{}), v, nodes[1:])
			return data, err
		}
	case []interface{}:
		index, ok := testSliceKey(key)
		if !ok {
			return data, _ErrWrongKey
		}
		l := len(data)
		for index < 0 {
			index += l
		}

		if index >= l {
			d := make([]interface{}, index+1)
			copy(d, data)
			data = d
		}

		if data[index] == nil {
			key1 := nodes[1]
			index1, ok := testSliceKey(key1)
			if ok {
				data[index], err = write(make([]interface{}, index1+1), v, nodes[1:])
			} else {
				data[index], err = write(make(map[string]interface{}), v, nodes[1:])
			}
		} else {
			data[index], err = write(data[index], v, nodes[1:])
		}
		return data, err
	default:
		return data, _NotSupportType
	}
}

func (o *object) Get(path string) (interface{}, error) {
	var (
		data interface{}
		ok   bool
	)
	data = o.data
	nodes := pathToNodes(path)
	for _, node := range nodes {
		switch obj := data.(type) {
		case map[string]interface{}:
			data, ok = obj[node]
			if ok {
				continue
			}
			return nil, _NotExitKey
		case []interface{}:
			index, ok := testSliceKey(node)
			if !ok {
				return nil, _ErrWrongKey
			}
			if index < len(obj) {
				data = obj[index]
				continue
			}
			return nil, fmt.Errorf("out of range %d", index)
		}
	}
	return data, nil
}

func (o *object) Must(path string, val interface{}) interface{} {
	v, err := o.Get(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Bool(path string) (bool, error) {
	obj, err := o.Get(path)
	if err != nil {
		return false, err
	}
	v, ok := obj.(bool)
	if !ok {
		return false, fmt.Errorf("%v not bool", obj)
	}
	return v, err
}

func (o *object) MustBool(path string, val bool) bool {
	v, err := o.Bool(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Bools(path string) ([]bool, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]bool, len(vo))
	for i, s := range vo {
		v[i] = s.(bool)
	}
	return v, err
}

func (o *object) MustBools(path string, val []bool) []bool {
	v, err := o.Bools(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Int16(path string) (int16, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(int16)
	if !ok {
		return 0, fmt.Errorf("%v not int16", obj)
	}
	return v, err
}

func (o *object) MustInt16(path string, val int16) int16 {
	v, err := o.Int16(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Int16s(path string) ([]int16, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]int16, len(vo))
	for i, s := range vo {
		v[i] = s.(int16)
	}
	return v, err
}

func (o *object) MustInt16s(path string, val []int16) []int16 {
	v, err := o.Int16s(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Int(path string) (int, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(int)
	if !ok {
		return 0, fmt.Errorf("%v not int", obj)
	}
	return v, err
}

func (o *object) MustInt(path string, val int) int {
	v, err := o.Int(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Ints(path string) ([]int, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]int, len(vo))
	for i, s := range vo {
		v[i] = s.(int)
	}
	return v, err
}

func (o *object) MustInts(path string, val []int) []int {
	v, err := o.Ints(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Int32(path string) (int32, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(int32)
	if !ok {
		return 0, fmt.Errorf("%v not int32", obj)
	}
	return v, err
}

func (o *object) MustInt32(path string, val int32) int32 {
	v, err := o.Int32(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Int32s(path string) ([]int32, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]int32, len(vo))
	for i, s := range vo {
		v[i] = s.(int32)
	}
	return v, err
}

func (o *object) MustInt32s(path string, val []int32) []int32 {
	v, err := o.Int32s(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Int64(path string) (int64, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(int64)
	if !ok {
		return 0, fmt.Errorf("%v not int64", obj)
	}
	return v, err
}

func (o *object) MustInt64(path string, val int64) int64 {
	v, err := o.Int64(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Int64s(path string) ([]int64, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]int64, len(vo))
	for i, s := range vo {
		v[i] = s.(int64)
	}
	return v, err
}

func (o *object) MustInt64s(path string, val []int64) []int64 {
	v, err := o.Int64s(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uint8(path string) (uint8, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(uint8)
	if !ok {
		return 0, fmt.Errorf("%v not uint8", obj)
	}
	return v, err
}

func (o *object) MustUint8(path string, val uint8) uint8 {
	v, err := o.Uint8(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uint8s(path string) ([]uint8, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]uint8, len(vo))
	for i, s := range vo {
		v[i] = s.(uint8)
	}
	return v, err
}

func (o *object) MustUint8s(path string, val []uint8) []uint8 {
	v, err := o.Uint8s(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uint16(path string) (uint16, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(uint16)
	if !ok {
		return 0, fmt.Errorf("%v not uint16", obj)
	}
	return v, err
}

func (o *object) MustUint16(path string, val uint16) uint16 {
	v, err := o.Uint16(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uint16s(path string) ([]uint16, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]uint16, len(vo))
	for i, s := range vo {
		v[i] = s.(uint16)
	}
	return v, err
}

func (o *object) MustUint16s(path string, val []uint16) []uint16 {
	obj, err := o.Uint16s(path)
	if err != nil {
		return val
	}
	return obj
}

func (o *object) Uint(path string) (uint, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(uint)
	if !ok {
		return 0, fmt.Errorf("%v not uint", obj)
	}
	return v, err
}

func (o *object) MustUint(path string, val uint) uint {
	v, err := o.Uint(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uints(path string) ([]uint, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]uint, len(vo))
	for i, s := range vo {
		v[i] = s.(uint)
	}
	return v, err
}

func (o *object) MustUints(path string, val []uint) []uint {
	v, err := o.Uints(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uint32(path string) (uint32, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(uint32)
	if !ok {
		return 0, fmt.Errorf("%v not uint32", obj)
	}
	return v, err
}

func (o *object) MustUint32(path string, val uint32) uint32 {
	v, err := o.Uint32(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uint32s(path string) ([]uint32, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]uint32, len(vo))
	for i, s := range vo {
		v[i] = s.(uint32)
	}
	return v, err
}

func (o *object) MustUint32s(path string, val []uint32) []uint32 {
	v, err := o.Uint32s(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uint64(path string) (uint64, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(uint64)
	if !ok {
		return 0, fmt.Errorf("%v not uint64", obj)
	}
	return v, err
}

func (o *object) MustUint64(path string, val uint64) uint64 {
	v, err := o.Uint64(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Uint64s(path string) ([]uint64, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]uint64, len(vo))
	for i, s := range vo {
		v[i] = s.(uint64)
	}
	return v, err
}

func (o *object) MustUint64s(path string, val []uint64) []uint64 {
	v, err := o.Uint64s(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Float32(path string) (float32, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(float32)
	if !ok {
		return 0, fmt.Errorf("%v not float32", obj)
	}
	return v, err
}

func (o *object) MustFloat32(path string, val float32) float32 {
	v, err := o.Float32(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Float32s(path string) ([]float32, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]float32, len(vo))
	for i, s := range vo {
		v[i] = s.(float32)
	}
	return v, err
}

func (o *object) MustFloat32s(path string, val []float32) []float32 {
	v, err := o.Float32s(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Float64(path string) (float64, error) {
	obj, err := o.Get(path)
	if err != nil {
		return 0, err
	}
	v, ok := obj.(float64)
	if !ok {
		return 0, fmt.Errorf("%v not float64", obj)
	}
	return v, err
}

func (o *object) MustFloat64(path string, val float64) float64 {
	v, err := o.Float64(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Float64s(path string) ([]float64, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]float64, len(vo))
	for i, s := range vo {
		v[i] = s.(float64)
	}
	return v, err
}

func (o *object) MustFloat64s(path string, val []float64) []float64 {
	v, err := o.Float64s(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) String(path string) (string, error) {
	obj, err := o.Get(path)
	if err != nil {
		return "", err
	}
	v, ok := obj.(string)
	if !ok {
		return "", fmt.Errorf("%v not string", obj)
	}
	return v, err
}

func (o *object) MustString(path string, val string) string {
	v, err := o.String(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Strings(path string) ([]string, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	vo, ok := obj.([]interface{})
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	v := make([]string, len(vo))
	for i, s := range vo {
		v[i] = s.(string)
	}
	return v, err
}

func (o *object) MustStrings(path string, val []string) []string {
	v, err := o.Strings(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Bytes(path string) ([]byte, error) {
	obj, err := o.Get(path)
	if err != nil {
		return nil, err
	}
	v, ok := obj.([]byte)
	if !ok {
		return nil, fmt.Errorf("%v not string", obj)
	}
	return v, err
}

func (o *object) MustBytes(path string, val []byte) []byte {
	v, err := o.Bytes(path)
	if err != nil {
		return val
	}
	return v
}

func (o *object) Data() map[string]interface{} {
	return o.data
}
