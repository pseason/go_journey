package data

import "95eh.com/eg/utils"

func NewUint64(v uint64) *Uint64 {
	return &Uint64{
		v:        v,
		listener: make(map[int64]utils.ActionUint64),
	}
}

type Uint64 struct {
	v        uint64
	listener map[int64]utils.ActionUint64
}

func (a *Uint64) Get() uint64 {
	return a.v
}

func (a *Uint64) Set(v uint64, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Uint64) Bind(action utils.ActionUint64) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Uint64) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Uint64) ClearListeners() {
	a.listener = make(map[int64]utils.ActionUint64)
}
