package data

import "95eh.com/eg/utils"

func NewBool(v bool) *Bool {
	return &Bool{
		v:        v,
		listener: make(map[int64]utils.ActionBool),
	}
}

type Bool struct {
	v        bool
	listener map[int64]utils.ActionBool
}

func (a *Bool) Get() bool {
	return a.v
}

func (a *Bool) Set(v bool, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Bool) Bind(action utils.ActionBool) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Bool) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Bool) ClearListeners() {
	a.listener = make(map[int64]utils.ActionBool)
}
