package data

import "95eh.com/eg/utils"

func NewMapInt32ToInt64(v map[int32]int64) *MapInt32ToInt64 {
	return &MapInt32ToInt64{
		v:        v,
		listener: make(map[int64]utils.ActionMapInt32ToInt64),
	}
}

type MapInt32ToInt64 struct {
	v        map[int32]int64
	listener map[int64]utils.ActionMapInt32ToInt64
}

func (a *MapInt32ToInt64) Get() map[int32]int64 {
	return a.v
}

func (a *MapInt32ToInt64) Set(v map[int32]int64, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *MapInt32ToInt64) Bind(action utils.ActionMapInt32ToInt64) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *MapInt32ToInt64) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *MapInt32ToInt64) ClearListeners() {
	a.listener = make(map[int64]utils.ActionMapInt32ToInt64)
}
