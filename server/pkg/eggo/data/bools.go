package data

import "95eh.com/eg/utils"

func NewBools(v []bool) *Bools {
	return &Bools{
		v:        v,
		listener: make(map[int64]utils.ActionBools),
	}
}

type Bools struct {
	v        []bool
	listener map[int64]utils.ActionBools
}

func (a *Bools) Get() []bool {
	return a.v
}

func (a *Bools) Set(v []bool, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Bools) Bind(action utils.ActionBools) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Bools) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Bools) ClearListeners() {
	a.listener = make(map[int64]utils.ActionBools)
}
