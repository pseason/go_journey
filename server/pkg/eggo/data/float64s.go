package data

import "95eh.com/eg/utils"

func NewFloat64s(v []float64) *Float64s {
	return &Float64s{
		v:        v,
		listener: make(map[int64]utils.ActionFloat64s),
	}
}

type Float64s struct {
	v        []float64
	listener map[int64]utils.ActionFloat64s
}

func (a *Float64s) Get() []float64 {
	return a.v
}

func (a *Float64s) Set(v []float64, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Float64s) Bind(action utils.ActionFloat64s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Float64s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Float64s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionFloat64s)
}
