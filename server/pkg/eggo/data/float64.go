package data

import "95eh.com/eg/utils"

func NewFloat64(v float64) *Float64 {
	return &Float64{
		v:        v,
		listener: make(map[int64]utils.ActionFloat64),
	}
}

type Float64 struct {
	v        float64
	listener map[int64]utils.ActionFloat64
}

func (a *Float64) Get() float64 {
	return a.v
}

func (a *Float64) Set(v float64, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Float64) Bind(action utils.ActionFloat64) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Float64) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Float64) ClearListeners() {
	a.listener = make(map[int64]utils.ActionFloat64)
}
