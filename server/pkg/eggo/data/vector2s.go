package data

import (
	"95eh.com/eg/utils"
)

func NewVector2s(v []utils.Vec2) *Vector2s {
	return &Vector2s{
		v:        v,
		listener: make(map[int64]utils.ActionVec2s),
	}
}

type Vector2s struct {
	v        []utils.Vec2
	listener map[int64]utils.ActionVec2s
}

func (a *Vector2s) Get() []utils.Vec2 {
	return a.v
}

func (a *Vector2s) Set(v []utils.Vec2, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Vector2s) Bind(action utils.ActionVec2s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Vector2s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Vector2s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionVec2s)
}
