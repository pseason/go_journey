package data

import "95eh.com/eg/utils"

func NewInt16s(v []int16) *Int16s {
	return &Int16s{
		v:        v,
		listener: make(map[int64]utils.ActionInt16s),
	}
}

type Int16s struct {
	v        []int16
	listener map[int64]utils.ActionInt16s
}

func (a *Int16s) Get() []int16 {
	return a.v
}

func (a *Int16s) Set(v []int16, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Int16s) Bind(action utils.ActionInt16s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Int16s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Int16s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionInt16s)
}
