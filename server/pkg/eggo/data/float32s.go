package data

import "95eh.com/eg/utils"

func NewFloat32s(v []float32) *Float32s {
	return &Float32s{
		v:        v,
		listener: make(map[int64]utils.ActionFloat32s),
	}
}

type Float32s struct {
	v        []float32
	listener map[int64]utils.ActionFloat32s
}

func (a *Float32s) Get() []float32 {
	return a.v
}

func (a *Float32s) Set(v []float32, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Float32s) Bind(action utils.ActionFloat32s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Float32s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Float32s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionFloat32s)
}
