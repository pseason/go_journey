package data

import "95eh.com/eg/utils"

func NewInt8s(v []int8) *Int8s {
	return &Int8s{
		v:        v,
		listener: make(map[int64]utils.ActionInt8s),
	}
}

type Int8s struct {
	v        []int8
	listener map[int64]utils.ActionInt8s
}

func (a *Int8s) Get() []int8 {
	return a.v
}

func (a *Int8s) Set(v []int8, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Int8s) Bind(action utils.ActionInt8s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Int8s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Int8s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionInt8s)
}
