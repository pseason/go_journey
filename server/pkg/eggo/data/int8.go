package data

import "95eh.com/eg/utils"

func NewInt8(v int8) *Int8 {
	return &Int8{
		v:        v,
		listener: make(map[int64]utils.ActionInt8),
	}
}

type Int8 struct {
	v        int8
	listener map[int64]utils.ActionInt8
}

func (a *Int8) Get() int8 {
	return a.v
}

func (a *Int8) Set(v int8, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *Int8) Bind(action utils.ActionInt8) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Int8) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Int8) ClearListeners() {
	a.listener = make(map[int64]utils.ActionInt8)
}
