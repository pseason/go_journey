package data

import "95eh.com/eg/utils"

func NewInt32s(v []int32) *Int32s {
	return &Int32s{
		v:        v,
		listener: make(map[int64]utils.ActionInt32s),
	}
}

type Int32s struct {
	v        []int32
	listener map[int64]utils.ActionInt32s
}

func (a *Int32s) Get() []int32 {
	return a.v
}

func (a *Int32s) Set(v []int32, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Int32s) Bind(action utils.ActionInt32s) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Int32s) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Int32s) ClearListeners() {
	a.listener = make(map[int64]utils.ActionInt32s)
}
