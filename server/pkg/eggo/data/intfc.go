package data

import (
	"95eh.com/eg/utils"
	"time"
)

type (
	TField          uint16
	ActionMap       func(m Map)
	ActionMapInt32  func(m map[TField]int32)
	ActionMapInt64  func(m map[TField]int64)
	ActionMapInt64s func(m map[TField][]int64)
)

type IFields interface {
	ToBytes() ([]byte, error)
	GetString(t TField) (string, bool)
	SetString(t TField, v string)
	GetStrings(t TField) ([]string, bool)
	SetStrings(t TField, v []string)
	GetBool(t TField) (bool, bool)
	SetBool(t TField, v bool)
	GetBools(t TField) ([]bool, bool)
	SetBools(t TField, v []bool)
	GetInt16(t TField) (int16, bool)
	SetInt16(t TField, v int16)
	GetInt16s(t TField) ([]int16, bool)
	SetInt16s(t TField, v []int16)
	GetInt32(t TField) (int32, bool)
	SetInt32(t TField, v int32)
	GetInt32s(t TField) ([]int32, bool)
	SetInt32s(t TField, v []int32)
	GetInt64(t TField) (int64, bool)
	SetInt64(t TField, v int64)
	GetInt64s(t TField) ([]int64, bool)
	SetInt64s(t TField, v []int64)
	GetFloat32(t TField) (float32, bool)
	SetFloat32(t TField, v float32)
	GetFloat32s(t TField) ([]float32, bool)
	SetFloat32s(t TField, v []float32)
	GetVec2(t TField) (utils.Vec2, bool)
	SetVec2(t TField, v utils.Vec2)
	GetVec2s(t TField) ([]utils.Vec2, bool)
	SetVec2s(t TField, v []utils.Vec2)
	GetMapInt32(t TField) (map[string]int32, bool)
	SetMapInt32(t TField, v map[string]int32)
	GetMapInt64(t TField) (map[string]int64, bool)
	SetMapInt64(t TField, v map[string]int64)
	GetMapInt32ToInt32(t TField) (map[int32]int32, bool)
	SetMapInt32ToInt32(t TField, v map[int32]int32)
	GetMapInt32ToInt64(t TField) (map[int32]int64, bool)
	SetMapInt32ToInt64(t TField, v map[int32]int64)
	GetMapInt64ToInt32(t TField) (map[int64]int32, bool)
	SetMapInt64ToInt32(t TField, v map[int64]int32)
	GetMapInt64ToInt64(t TField) (map[int64]int64, bool)
	SetMapInt64ToInt64(t TField, v map[int64]int64)
	GetAny(t TField) (interface{}, bool)
	SetAny(t TField, v interface{})

	MergeString(m map[TField]string)
	MergeStrings(m map[TField][]string)
	MergeBool(m map[TField]bool)
	MergeBools(m map[TField][]bool)
	MergeInt16(m map[TField]int16)
	MergeInt16s(m map[TField][]int16)
	MergeInt32(m map[TField]int32)
	MergeInt32s(m map[TField][]int32)
	MergeInt64(m map[TField]int64)
	MergeInt64s(m map[TField][]int64)
	MergeFloat32(m map[TField]float32)
	MergeFloat32s(m map[TField][]float32)
	MergeVector2(m map[TField]utils.Vec2)
	MergeVector2s(m map[TField][]utils.Vec2)
	MergeMapInt32(m map[TField]map[string]int32)
	MergeMapInt64(m map[TField]map[string]int64)
	MergeMapInt32ToInt32(m map[TField]map[int32]int32)
	MergeMapInt32ToInt64(m map[TField]map[int32]int64)
	MergeMapInt64ToInt32(m map[TField]map[int64]int32)
	MergeMapInt64ToInt64(m map[TField]map[int64]int64)
	MergeAny(m Map)
	MergeBytes(data []byte) error
}

type (
	ActionDataToBool func(data IData) bool
	ActionData       func(data IData)
	ActionBoolData   func(ok bool, data IData)
	Map              map[TField]interface{}
	ToMap            func() Map
)

type IData interface {
	Set(key TField, val interface{})
	Get(key TField) (interface{}, bool)
	Replace(key TField, val interface{}) (oldVal interface{}, ok bool)
	Update(key TField, action utils.ActionAnyToAny) (ok bool)
	Remove(key TField)
	Pop(key TField) (v interface{}, ok bool)
	Has(key TField) bool
	Bool(key TField) (bool, bool)
	MustBool(key TField, def bool) bool
	Bools(key TField) ([]bool, bool)
	MustBools(key TField, def []bool) []bool
	Int8(key TField) (int8, bool)
	MustInt8(key TField, def int8) int8
	Int8s(key TField) ([]int8, bool)
	MustInt8s(key TField, def []int8) []int8
	Int(key TField) (int, bool)
	MustInt(key TField, def int) int
	Ints(key TField) ([]int, bool)
	MustInts(key TField, def []int) []int
	Int16(key TField) (int16, bool)
	MustInt16(key TField, def int16) int16
	Int16s(key TField) ([]int16, bool)
	MustInt16s(key TField, def []int16) []int16
	Int32(key TField) (int32, bool)
	MustInt32(key TField, def int32) int32
	Int32s(key TField) ([]int32, bool)
	MustInt32s(key TField, def []int32) []int32
	Int64(key TField) (int64, bool)
	MustInt64(key TField, def int64) int64
	Int64s(key TField) ([]int64, bool)
	MustInt64s(key TField, def []int64) []int64
	Uint(key TField) (uint, bool)
	MustUint(key TField, def uint) uint
	Uints(key TField) ([]uint, bool)
	MustUints(key TField, def []uint) []uint
	Uint8(key TField) (uint8, bool)
	MustUint8(key TField, def uint8) uint8
	Uint8s(key TField) ([]uint8, bool)
	MustUint8s(key TField, def []uint8) []uint8
	Uint16(key TField) (uint16, bool)
	MustUint16(key TField, def uint16) uint16
	Uint16s(key TField) ([]uint16, bool)
	MustUint16s(key TField, def []uint16) []uint16
	Uint32(key TField) (uint32, bool)
	MustUint32(key TField, def uint32) uint32
	Uint32s(key TField) ([]uint32, bool)
	MustUint32s(key TField, def []uint32) []uint32
	Uint64(key TField) (uint64, bool)
	MustUint64(key TField, def uint64) uint64
	Uint64s(key TField) ([]uint64, bool)
	MustUint64s(key TField, def []uint64) []uint64
	Float32(key TField) (float32, bool)
	MustFloat32(key TField, def float32) float32
	Float32s(key TField) ([]float32, bool)
	MustFloat32s(key TField, def []float32) []float32
	Float64(key TField) (float64, bool)
	MustFloat64(key TField, def float64) float64
	Float64s(key TField) ([]float64, bool)
	MustFloat64s(key TField, def []float64) []float64
	String(key TField) (string, bool)
	MustString(key TField, def string) string
	Strings(key TField) ([]string, bool)
	MustStrings(key TField, def []string) []string
	Vec2(key TField) (utils.Vec2, bool)
	MustVec2(key TField, def utils.Vec2) utils.Vec2
	Vec2s(key TField) ([]utils.Vec2, bool)
	MustVec2s(key TField, def []utils.Vec2) []utils.Vec2
	Vec3(key TField) (utils.Vec3, bool)
	MustVec3(key TField, def utils.Vec3) utils.Vec3
	Vec3s(key TField) ([]utils.Vec3, bool)
	MustVec3s(key TField, def []utils.Vec3) []utils.Vec3
	Time(key TField) (*time.Time, bool)
	MustTime(key TField, def *time.Time) *time.Time
	Times(key TField) ([]*time.Time, bool)
	MustTimes(key TField, def []*time.Time) []*time.Time
	Reset(m Map)
	Map() Map
}

type IObject interface {
	Get(path string) (interface{}, error)
	Set(path string, v interface{}) error
	Del(path string)
	Merge(data map[string]interface{})
	Watch(action utils.ActionBoolStrAny, paths ...string) (map[string]int64, error)
	WatchPrefix(action utils.ActionBoolStrAny, paths ...string) (map[string]int64, error)
	Unwatch(id int64)
	UnwatchPrefix(id int64)
	WatchGlobal(action utils.ActionAny) int64
	UnwatchGlobal(id int64)
	Must(path string, val interface{}) interface{}
	Bool(path string) (bool, error)
	Bools(path string) ([]bool, error)
	MustBool(path string, val bool) bool
	MustBools(path string, val []bool) []bool
	Int16(path string) (int16, error)
	MustInt16(path string, val int16) int16
	Int16s(path string) ([]int16, error)
	MustInt16s(path string, val []int16) []int16
	Int(path string) (int, error)
	MustInt(path string, val int) int
	Ints(path string) ([]int, error)
	MustInts(path string, val []int) []int
	Int32(path string) (int32, error)
	MustInt32(path string, val int32) int32
	Int32s(path string) ([]int32, error)
	MustInt32s(path string, val []int32) []int32
	Int64(path string) (int64, error)
	MustInt64(path string, val int64) int64
	Int64s(path string) ([]int64, error)
	MustInt64s(path string, val []int64) []int64
	Uint8(path string) (uint8, error)
	MustUint8(path string, val uint8) uint8
	Uint8s(path string) ([]uint8, error)
	MustUint8s(path string, val []uint8) []uint8
	Uint16(path string) (uint16, error)
	MustUint16(path string, val uint16) uint16
	Uint16s(path string) ([]uint16, error)
	MustUint16s(path string, val []uint16) []uint16
	Uint(path string) (uint, error)
	MustUint(path string, val uint) uint
	Uints(path string) ([]uint, error)
	MustUints(path string, val []uint) []uint
	Uint32(path string) (uint32, error)
	MustUint32(path string, val uint32) uint32
	Uint32s(path string) ([]uint32, error)
	MustUint32s(path string, val []uint32) []uint32
	Uint64(path string) (uint64, error)
	MustUint64(path string, val uint64) uint64
	Uint64s(path string) ([]uint64, error)
	MustUint64s(path string, val []uint64) []uint64
	Float32(path string) (float32, error)
	MustFloat32(path string, val float32) float32
	Float32s(path string) ([]float32, error)
	MustFloat32s(path string, val []float32) []float32
	Float64(path string) (float64, error)
	MustFloat64(path string, val float64) float64
	Float64s(path string) ([]float64, error)
	MustFloat64s(path string, val []float64) []float64
	String(path string) (string, error)
	MustString(path string, val string) string
	Strings(path string) ([]string, error)
	MustStrings(path string, val []string) []string
	Bytes(path string) ([]byte, error)
	MustBytes(path string, val []byte) []byte
	Data() map[string]interface{}
}
