package data

import "95eh.com/eg/utils"

func NewStrings(v []string) *Strings {
	return &Strings{
		v:        v,
		listener: make(map[int64]utils.ActionStrings),
	}
}

type Strings struct {
	v        []string
	listener map[int64]utils.ActionStrings
}

func (a *Strings) Get() []string {
	return a.v
}

func (a *Strings) Set(v []string, call bool) {
	if call {
		for _, action := range a.listener {
			action(a.v...)
		}
	}
	a.v = v
}

func (a *Strings) Bind(action utils.ActionStrings) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *Strings) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *Strings) ClearListeners() {
	a.listener = make(map[int64]utils.ActionStrings)
}
