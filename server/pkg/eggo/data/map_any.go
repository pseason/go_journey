package data

import "95eh.com/eg/utils"

func NewMapAny(v Map) *MapAny {
	if v == nil {
		v = make(map[TField]interface{})
	}
	return &MapAny{
		v:        v,
		listener: make(map[int64]ActionMap),
	}
}

type MapAny struct {
	v        Map
	listener map[int64]ActionMap
}

func (a *MapAny) Get() Map {
	return a.v
}

func (a *MapAny) Set(v Map, call bool) {
	if call {
		for _, action := range a.listener {
			action(v)
		}
	}
	a.v = v
}

func (a *MapAny) Bind(action ActionMap) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	a.listener[id] = action
	return id
}

func (a *MapAny) Unbind(id int64) {
	delete(a.listener, id)
}

func (a *MapAny) ClearListeners() {
	a.listener = make(map[int64]ActionMap)
}
