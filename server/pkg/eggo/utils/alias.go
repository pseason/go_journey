package utils

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"github.com/panjf2000/ants/v2"
	"sync"
)

type (
	M map[string]interface{}

	ActionM func(m M)

	ActionBytes func(data []byte)

	ActionAny func(obj interface{})

	ActionBoolAny func(exist bool, value interface{})

	ActionBoolStrAny func(exist bool, path string, value interface{})

	ActionAnys func(objs ...interface{})

	ActionAnyToAny func(obj interface{}) interface{}

	ActionAnyAction func(obj interface{}, action Action)

	ActionAnyErr func(obj interface{}, err TErrCode)

	ActionAnyToErrCode func(obj interface{}) (err TErrCode)

	ActionBool func(v bool)

	ActionBools func(v ...bool)

	ActionInt8 func(v int8)

	ActionInt8s func(v ...int8)

	ActionUint8 func(v uint8)

	ActionUint8s func(v ...uint8)

	ActionInt16 func(v int16)

	ActionInt16s func(v ...int16)

	ActionUint16 func(v uint16)

	ActionUint16s func(v ...uint16)

	ActionInt32 func(v int32)

	ActionInt32s func(v ...int32)

	ActionUint32 func(v uint32)

	ActionUint32s func(v ...uint32)

	ActionInt func(v int)

	ActionInt64  func(v int64)
	ActionInt642 func(v1, v2 int64)

	ActionInt64s func(v ...int64)

	ActionUint64 func(v uint64)

	ActionUint64s func(v ...uint64)

	ActionFloat32 func(v float32)

	ActionFloat32s func(v ...float32)

	ActionFloat64 func(v float64)

	ActionFloat64s func(v ...float64)

	ActionInt32ToInt32 func(v int32) int32

	ActionInt64ToInt64 func(v int64) int64

	ActionFloat32ToFloat32 func(v float32) float32

	ActionErr func(err error)

	ActionError func(err IError)

	ActionMapInt32ToInt64 func(m map[int32]int64)

	ActionMapInt64ToInt32 func(m map[int64]int32)

	ActionMapInt64ToInt64 func(m map[int64]int64)

	ActionMapInt64ToInt64s func(m map[int64][]int64)

	ActionInt32ToBool func(v int32) bool

	ActionInt64ToBool func(v int64) bool

	ActionInt64ToBytes func(v int64) []byte

	ActionInt64Bytes func(v int64, bytes []byte)

	ActionUint16Bytes func(v uint16, data []byte)

	ActionUint16Any func(v uint16, any interface{})

	ActionInt64String func(v int64, str string)

	ActionString func(str string)

	ActionStrings func(strs ...string)

	ActionStringToString func(str string) string

	ActionStringToBool func(str string) bool

	ToAny func() interface{}

	ToInt64 func() int64

	ToUint16 func() uint16

	ToBool func() bool

	ToErrCode func() TErrCode

	ToError func() error

	Action func()

	ActionInt64Any func(v int64, obj interface{})

	ActionUint16Int64Any func(v1 uint16, v2 int64, obj interface{})

	ActionInt64ToAny func(id int64) interface{}

	ActionInt64ToStr func(id int64) string

	ActionAnyErrCode func(any interface{}, ec TErrCode)

	ActionAnyToAction func(o interface{}) Action

	ActionAnyToBool func(any interface{}) bool

	ActionAnyBoolToAnyBool func(any interface{}, ok bool) (interface{}, bool)

	ActionAnyToBytesErr func(any interface{}) ([]byte, error)

	ActionBytesAnyToErr func(data []byte, pkt interface{}) error

	ActionBoolBytesAny func(ok bool, bytes []byte)

	ActionStrBytesAny func(str string, bytes []byte)

	ActionBoolStrBytesAny func(ok bool, str string, bytes []byte)

	ActionVec2 func(v Vec2)

	ActionVec2s func(v ...Vec2)

	ActionVec3 func(v Vec3)

	ActionVec3s func(v ...Vec3)

	ActionVec2ToFloat32 func(v Vec2) float32

	ActionVec3ToFloat32 func(v Vec3) float32

	ActionErrCode func(ec TErrCode)

	ActionUint16ErrCode func(v uint16, ec TErrCode)

	ActionUint16Int64ErrCode func(v1 uint16, v2 int64, ec TErrCode)

	ActionInt64ErrCode func(v int64, ec TErrCode)

	ActionErrCodeToBool func(ec TErrCode) bool

	ActionActionAny func(action ActionAny)

	ActionBytesToErr func(bytes []byte) error

	ActionBytesToAnyErr func(bytes []byte) (interface{}, error)

	ActionInt642Any func(v1, v2 int64, body interface{})

	ActionInt64Uint162Any func(v1 int64, v2, v3 uint16, body interface{})

	ActionInt64Uint32Any func(v1 int64, v2 uint32, body interface{})

	ActionStrInt642Any func(str string, v1, v2 int64, body interface{})

	ActionInt642Any2ToErr func(v1, v2 int64, reqBody, resBody interface{}) error

	ActionInt642AnyToErrCode func(v1, v2 int64, body interface{}) TErrCode

	ActionInt642AnyToAnyErrCode func(v1, v2 int64, body interface{}) (interface{}, TErrCode)
)

func (m M) Json() string {
	if m == nil {
		return ""
	}
	str, err := jsoniter.MarshalToString(m)
	if err == nil {
		return str
	}
	return fmt.Sprintf("%s", m)
}

func (m M) IndentJson() string {
	if m == nil {
		return ""
	}
	bytes, err := jsoniter.MarshalIndent(m, "", " ")
	if err == nil {
		return string(bytes)
	}
	return fmt.Sprintf("%s", m)
}

func (c ActionAny) Invoke(obj interface{}) {
	if c == nil {
		return
	}
	c(obj)
}

func (c ActionAny) Done(obj interface{}) {
	c(obj)
}

func (c ActionAny) IsNil() bool {
	return c == nil
}

func (f ActionBool) Invoke(v bool) {
	if f != nil {
		f(v)
	}
}

func (c ActionErr) IsNil() bool {
	return c == nil
}

func (c ActionErr) Invoke(err error) {
	if c == nil {
		return
	}
	c(err)
}

func (c ActionErr) Done(err error) {
	c(err)
}

func (c ActionErrCode) IsNil() bool {
	return c == nil
}

func (c ActionErrCode) Invoke(ec TErrCode) {
	if c == nil {
		return
	}
	c(ec)
}

func (c ActionErrCode) Done(ec TErrCode) {
	c(ec)
}

func (c Action) Invoke() {
	if c == nil {
		return
	}
	c()
}

func (c Action) Done() {
	c()
}

func (c Action) IsNil() bool {
	return c == nil
}

func (c Action) Wait(wg *sync.WaitGroup) {
	if c == nil {
		return
	}
	Async(func() {
		wg.Wait()
		c()
	})
}

func (c Action) WaitActions(wg *sync.WaitGroup, actions ...Action) {
	Async(func() {
		wg.Wait()
		for _, action := range actions {
			action()
		}
		c.Invoke()
	})
}

func (c Action) Async() {
	if c == nil {
		return
	}
	if ants.Submit(c) == nil {
		return
	}
	go c()
}

func Async(action Action) {
	action.Async()
}

func (c ToBool) Invoke() bool {
	if c == nil {
		return false
	}
	return c()
}
