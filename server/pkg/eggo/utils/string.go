package utils

import (
	"strconv"
	"strings"
)

func MergeStr(sep string, nodes ...string) string {
	l := len(nodes)
	if l == 0 {
		return ""
	}
	var b strings.Builder
	b.WriteString(nodes[0])
	for i := 1; i < l; i++ {
		b.WriteString(sep + nodes[i])
	}
	return b.String()
}

func MergeUint16(sep string, nodes ...uint16) string {
	l := len(nodes)
	if l == 0 {
		return ""
	}
	var b strings.Builder
	b.WriteString(strconv.Itoa(int(nodes[0])))
	for i := 1; i < l; i++ {
		b.WriteString(sep + strconv.Itoa(int(nodes[0])))
	}
	return b.String()
}

func SplitUint16(sep, str string) ([]uint16, error) {
	items := strings.Split(str, sep)
	vals := make([]uint16, len(items))
	for i, item := range items {
		v, err := strconv.Atoi(item)
		if err != nil {
			return nil, err
		}
		vals[i] = uint16(v)
	}
	return vals, nil
}

func SplitInt64(sep, str string) ([]int64, error) {
	items := strings.Split(str, sep)
	vals := make([]int64, len(items))
	for i, item := range items {
		v, err := strconv.ParseInt(item, 10, 64)
		if err != nil {
			return nil, err
		}
		vals[i] = v
	}
	return vals, nil
}
