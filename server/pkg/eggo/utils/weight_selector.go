package utils

type weightData struct {
	any       interface{}
	weight    int
	current   int
	effective int
}

type weightSelector struct {
	items []*weightData
	n     int
}

func NewWeightSelector() *weightSelector {
	return &weightSelector{
		items: make([]*weightData, 0, 2),
	}
}

func (w *weightSelector) Add(any interface{}, weight int) {
	weighted := &weightData{any: any, weight: weight, effective: weight}
	w.items = append(w.items, weighted)
	w.n++
}

func (w *weightSelector) Remove(any interface{}) {
	for i, item := range w.items {
		if any == item.any {
			w.items = append(w.items[i:], w.items[i+1:]...)
			w.n--
			break
		}
	}
}

func (w *weightSelector) RemoveAll() {
	w.items = w.items[:0]
	w.n = 0
}

func (w *weightSelector) Reset() {
	for _, s := range w.items {
		s.effective = s.weight
		s.current = 0
	}
}
func (w *weightSelector) All(action ActionAny) {
	for _, data := range w.items {
		action(data.any)
	}
}

func (w *weightSelector) Next() interface{} {
	i := w.nextWeighted()
	if i == nil {
		return nil
	}
	return i.any
}

func (w *weightSelector) nextWeighted() *weightData {
	if w.n == 0 {
		return nil
	}
	if w.n == 1 {
		return w.items[0]
	}
	return nextSmoothWeighted(w.items)
}

func nextSmoothWeighted(items []*weightData) (best *weightData) {
	total := 0
	for i := 0; i < len(items); i++ {
		w := items[i]

		if w == nil {
			continue
		}

		w.current += w.effective
		total += w.effective
		if w.effective < w.weight {
			w.effective++
		}

		if best == nil || w.current > best.current {
			best = w
		}
	}

	if best == nil {
		return nil
	}

	best.current -= total
	return best
}
