package utils

import (
	"fmt"
	"math"
)

type Vec3 struct {
	X, Y, Z float32
}

var Vec3One = Vec3{1, 1, 1}
var Vec2One = Vec2{1, 1}

var Vec3Zero = Vec3{0, 0, 0}
var Vec2Zero = Vec2{0, 0}

func NewVec3(x, y, z float32) Vec3 {
	var v Vec3
	v.Set(x, y, z)
	return v
}

func (v *Vec3) Set(x, y, z float32) {
	v.X = x
	v.Y = y
	v.Z = z
}

func (v *Vec3) SetData(i int, data float32) {
	switch i {
	case 0:
		v.X = data
	case 1:
		v.Y = data
	case 2:
		v.Z = data
	default:
		panic(fmt.Errorf("invalid vector index:%d", i))
	}
}

func (v *Vec3) GetData(i int) float32 {
	switch i {
	case 0:
		return v.X
	case 1:
		return v.Y
	case 2:
		return v.Z
	default:
		panic(fmt.Errorf("invalid vector index:%d", i))
	}
}

func (v *Vec3) SetZero() {
	v.X = 0
	v.Y = 0
	v.Z = 0
}

func Vec3Sub(v1, v2 Vec3) Vec3 {
	return Vec3{v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z}
}

func Vec3Add(v1, v2 Vec3) Vec3 {
	return Vec3{v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z}
}

func Vec3Mulf(v1 Vec3, val float32) Vec3 {
	return Vec3{v1.X * val, v1.Y * val, v1.Z * val}
}

func Vec3Mulv(v1, data Vec3) Vec3 {
	return Vec3{v1.X * data.X, v1.Y * data.Y, v1.Z * data.Z}
}

func Vec3Div(v1 Vec3, val float32) Vec3 {
	return Vec3{v1.X / val, v1.Y / val, v1.Z / val}
}

func Vec3Neg(v Vec3) Vec3 {
	return Vec3{-v.X, -v.Y, -v.Z}
}

func Vec3Scale(l, r Vec3) Vec3 {
	return Vec3{l.X * r.X, l.Y * r.Y, l.Z * r.Z}
}

func MinVec3(l, r Vec3) Vec3 {
	return Vec3{MinFloat(l.X, r.X), MinFloat(l.Y, r.Y), MinFloat(l.Z, r.Z)}
}

func MaxVec3(l, r Vec3) Vec3 {
	return Vec3{FloatMax(l.X, r.X), FloatMax(l.Y, r.Y), FloatMax(l.Z, r.Z)}
}

func DotVec3(l, r Vec3) float32 {
	return l.X*r.X + l.Y*r.Y + l.Z*r.Z
}

func Vec3Lerp(from, to Vec3, t float32) Vec3 {
	return Vec3Add(Vec3Mulf(to, t), Vec3Mulf(from, 1.0-t))
}

func Vec3Perp2D(u, v Vec3) float32 {
	return u.Z*v.X - u.X*v.Z
}

func Vec3Magnitude(inV Vec3) float32 {
	return float32(math.Sqrt(float64(DotVec3(inV, inV))))
}

func Vec2Magnitude(inV Vec2) float32 {
	return float32(math.Sqrt(float64(DotVec2(inV, inV))))
}

func Vec2SqrMagnitude(inV Vec2) float32 {
	return DotVec2(inV, inV)
}

func Vec3SqrMagnitude(inV Vec3) float32 {
	return DotVec3(inV, inV)
}

func Vec3Distance(a, b Vec3) float32 {
	return Vec3Magnitude(Vec3Sub(a, b))
}

func Vec2Distance(a, b Vec2) float32 {
	return Vec2Magnitude(Vec2Sub(a, b))
}

func Vec3Cross(lhs, rhs Vec3) Vec3 {
	return Vec3{
		lhs.Y*rhs.Z - lhs.Z*rhs.Y,
		lhs.Z*rhs.X - lhs.X*rhs.Z,
		lhs.X*rhs.Y - lhs.Y*rhs.X,
	}
}

func Vec3Abs(v Vec3) Vec3 {
	return NewVec3(AbsFloat(v.X), AbsFloat(v.Y), AbsFloat(v.Z))
}

func Vec3NormalizeSafe(inV, defaultV Vec3) Vec3 {
	mag := Vec3Magnitude(inV)
	if mag > kEpsilon {
		return Vec3Div(inV, mag)
	} else {
		return defaultV
	}
}

func Vec2NormalizeSafe(inV, defaultV Vec2) Vec2 {
	mag := Vec2Magnitude(inV)
	if mag > kEpsilon {
		return Vec2Div(inV, mag)
	} else {
		return defaultV
	}
}

func Vec2TestDis(inV0, inV1 Vec2, inMaxDist float32) bool {
	return Vec2SqrMagnitude(Vec2Sub(inV0, inV1)) <= inMaxDist*inMaxDist
}

func Vec3TestDis(inV0, inV1 Vec3, inMaxDist float32) bool {
	return Vec3SqrMagnitude(Vec3Sub(inV0, inV1)) <= inMaxDist*inMaxDist
}

func Vec3Normalize(inV Vec3) Vec3 {
	return Vec3Div(inV, Vec3Magnitude(inV))
}

func Vec3Angle(a, b Vec3) float32 {
	cv := DotVec3(a, b) / (Vec3Magnitude(a) * Vec3Magnitude(b))
	if cv < -1 && cv > -2 {
		cv = -1
	} else if cv > 1 && cv < 2 {
		cv = 1
	}
	return float32(math.Acos(float64(cv)) * 180 / math.Pi)
}

func Vec2Angle(a, b Vec2) float32 {
	cv := DotVec2(a, b) / (Vec2Magnitude(a) * Vec2Magnitude(b))
	if cv < -1 && cv > -2 {
		cv = -1
	} else if cv > 1 && cv < 2 {
		cv = 1
	}
	return float32(math.Acos(float64(cv)) * 180 / math.Pi)
}

type Vec2 struct {
	X, Y float32
}

func NewVec2(x, y float32) Vec2 {
	return Vec2{x, y}
}
func MinVec2(l, r Vec2) Vec2 {
	return Vec2{MinFloat(l.X, r.X), MinFloat(l.Y, r.Y)}
}
func DotVec2(l, r Vec2) float32 {
	return l.X*r.X + l.Y*r.Y
}

func MaxVec2(l, r Vec2) Vec2 {
	return Vec2{FloatMax(l.X, r.X), FloatMax(l.Y, r.Y)}
}

func Vec2Add(v Vec2, val Vec2) Vec2 {
	return Vec2{v.X + val.X, v.Y + val.Y}
}

func Vec2Sub(v1, v2 Vec2) Vec2 {
	return Vec2{v1.X - v2.X, v1.Y - v2.Y}
}

func Vec2Div(v Vec2, val float32) Vec2 {
	return Vec2{v.X / val, v.Y / val}
}

func Vec2Mul(v Vec2, val float32) Vec2 {
	return Vec2{v.X * val, v.Y * val}
}

type VecInt2 struct {
	X, Y int32
}

func NewVecInt2(x, y int32) VecInt2 {
	return VecInt2{x, y}
}

func VecInt2Sub(v1, v2 VecInt2) VecInt2 {
	return VecInt2{v1.X - v2.X, v1.Y - v2.Y}
}

func VecInt2Div(v VecInt2, val int32) VecInt2 {
	return VecInt2{v.X / val, v.Y / val}
}

func VecInt2Mul(v VecInt2, val int32) VecInt2 {
	return VecInt2{v.X * val, v.Y * val}
}

func VecInt2Add(v1, v2 VecInt2) VecInt2 {
	return VecInt2{v1.X + v2.X, v1.Y + v2.Y}
}

func GetCross(v1, v2, v Vec2) float32 {
	return (v2.X-v1.X)*(v.Y-v1.Y) - (v.X-v1.X)*(v2.Y-v1.Y)
}

func Vec2IsInCircular(target, origin Vec2, radius float32) bool {
	return Vec2TestDis(target, origin, radius)
}

func Vec2IsInSector(target, origin, normal Vec2, radius, angle float32) bool {
	if !Vec2TestDis(target, origin, radius) {
		return false
	}
	p := Vec2Sub(origin, target)
	a := Vec2Angle(p, normal)
	aa := math.Abs(float64(a))
	if float32(aa) > angle/2 {
		return false
	}
	return true
}

func Vec2IsInRect(point, origin, normal Vec2, width, length float32) bool {
	p := Vec2Sub(point, origin)
	m := Vec2Magnitude(p)
	a := Vec2Angle(p, normal)
	m1 := float32(math.Cos(float64(a))) * m
	if m1 > length/2 {
		return false
	}
	p1 := Vec2Mul(normal, m1)
	return Vec2TestDis(p, p1, width/2)
}

func Vec2IsInRay(point, origin, normal Vec2, width, length float32) bool {
	p := Vec2Sub(point, origin)
	a := Vec2Angle(p, normal)
	if a > HalfPi {
		return false
	}
	m := Vec2Magnitude(p)
	m1 := float32(math.Cos(float64(a))) * m
	if m1 > length {
		return false
	}
	p1 := Vec2Mul(normal, m1)
	return Vec2TestDis(p, p1, width/2)
}
