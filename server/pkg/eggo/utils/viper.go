package utils

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/spf13/viper"
)

var (
	_Viper *viper.Viper
)

func setDefault() {
	for k, v := range _Viper.AllSettings() {
		_Viper.SetDefault(k, v)
	}
}

func StartLocalConf(root string, conf interface{}, paths ...string) error {
	if root == "" {
		root = ExeDir()
	}
	return loadConf(conf, root, loadLocalConf, paths...)
}

func loadLocalConf(root, name string, v *viper.Viper) error {
	v.SetConfigName(name)
	v.SetConfigType("yaml")
	v.AddConfigPath(root)
	return v.ReadInConfig()
}

func StartConsulConf(root string, conf interface{}, paths ...string) error {
	return loadConf(conf, root, loadConsulConf, paths...)
}

func loadConsulConf(root, name string, v *viper.Viper) error {
	v.SetConfigType("yaml")
	key := MergeStr("/", root, name)
	res, _, err := Consul().KV().Get(key, nil)
	if err != nil {
		return err
	}
	return v.ReadConfig(bytes.NewBuffer(res.Value))
}

func loadConf(conf interface{}, root string, action func(root, name string, v *viper.Viper) error, paths ...string) error {
	l := len(paths)
	if l == 0 {
		return errors.New("paths length must more than 0")
	}
	_Viper = viper.New()
	for i := 0; i < l; i++ {
		p := paths[i]
		err := action(root, p, _Viper)
		if err == nil {
			if i < l-1 {
				setDefault()
			}
		} else {
			fmt.Printf("\033[93m%s\033[0m\n", err.Error())
		}
	}
	return _Viper.Unmarshal(conf)
}
