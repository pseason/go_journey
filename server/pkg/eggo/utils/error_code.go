package utils

import (
	"errors"
	"strconv"
)

var (
	_errCodeToStr = map[TErrCode]string{
		ErrCodeNil: "nil",
	}
)

type TErrCode uint32

const (
	ErrCodeNil TErrCode = 0
)

func (ec TErrCode) IsNotNil() bool {
	return ErrCodeNil != ec
}

func (ec TErrCode) IsNil() bool {
	return ErrCodeNil == ec
}

// Desc 错误描述
func (ec TErrCode) Desc() string {
	cs := strconv.FormatInt(int64(ec), 10)
	str, ok := _errCodeToStr[ec]
	if !ok {
		return cs
	}
	return "(" + cs + ")" + str
}

func (ec TErrCode) String() string {
	return strconv.FormatInt(int64(ec), 10)
}

func (ec TErrCode) Error() error {
	return errors.New(ec.Desc())
}

func BindErrCode(m map[TErrCode]string) {
	for code, msg := range m {
		_errCodeToStr[code] = msg
	}
}
