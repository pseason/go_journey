package utils

import "runtime/debug"

const (
	ErrClosed = "closed"
)

type IError interface {
	Error() string
	Stack() []byte
	AddParam(k string, v interface{})
	GetParam(k string) (v interface{}, ok bool)
	AddParams(params M)
	UpdateParam(k string, action ActionAnyBoolToAnyBool)
	Params() M
}

func GetStack() []byte {
	return debug.Stack()
}

// NewError
// 相关参数键值对写入params,不要拼接到msg
func NewError(msg string, params M) IError {
	return &err{msg: msg, stack: GetStack(), params: params}
}

type err struct {
	msg    string
	stack  []byte
	params M
}

func (e *err) Params() M {
	return e.params
}

func (e *err) Error() string {
	return e.msg
}

func (e *err) Stack() []byte {
	return e.stack
}

func (e *err) AddParam(k string, v interface{}) {
	if e.params == nil {
		e.params = map[string]interface{}{}
	}
	e.params[k] = v
}

func (e *err) GetParam(k string) (v interface{}, ok bool) {
	v, ok = e.params[k]
	return
}

func (e *err) AddParams(params M) {
	if e.params == nil {
		e.params = map[string]interface{}{}
	}
	for k, v := range params {
		e.params[k] = v
	}
}

func (e *err) UpdateParam(k string, action ActionAnyBoolToAnyBool) {
	ov, ok := e.params[k]
	v, ok := action(ov, ok)
	if ok {
		e.params[k] = v
	} else {
		delete(e.params, k)
	}
	return
}