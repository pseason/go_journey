package utils

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

type beforeExit struct {
	name   string
	action Action
}

var (
	_BeforeActions   = make([]*beforeExit, 0, 4)
	_BeforeWaitGroup = &sync.WaitGroup{}
	_SignalCh        = make(chan os.Signal, 1)
)

func BeforeExit(name string, actions ...func()) {
	for _, action := range actions {
		_BeforeActions = append(_BeforeActions, &beforeExit{
			name:   name,
			action: action,
		})
	}
}

func AddExitWaitGroup() {
	_BeforeWaitGroup.Add(1)
}

func DoneExitWaitGroup() {
	_BeforeWaitGroup.Done()
}

func Exit() {
	_SignalCh <- syscall.SIGQUIT
}

func WaitExit() {
	exitCh := make(chan struct{})
	signal.Notify(_SignalCh,
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGTERM,
	)
	//signal.Notify(signalCh, os.Interrupt, os.Kill)
	go func() {
		s := <-_SignalCh
		fmt.Println("signal", s)
		exitCh <- struct{}{}
	}()
	<-exitCh

	for _, action := range _BeforeActions {
		fmt.Println("!--", action.name, "--!")
		action.action()
	}

	_BeforeWaitGroup.Wait()

	fmt.Println("bye!")
	os.Exit(0)
}

func WaitGroup(wg *sync.WaitGroup, actions ...Action) {
	Async(func() {
		wg.Done()
		for _, action := range actions {
			action.Invoke()
		}
	})
}

func WaitGroupCh(wg *sync.WaitGroup) chan struct{} {
	ch := make(chan struct{})
	Async(func() {
		wg.Wait()
		ch <- struct{}{}
	})
	return ch
}

func WaitAny(action func(chan<- interface{})) (interface{}, error) {
	c := make(chan interface{})
	action(c)
	switch res := (<-c).(type) {
	case error:
		return nil, res
	default:
		return res, nil
	}
}

func WaitAnyAsync(onComplete ActionAnyErr) chan<- interface{} {
	c := make(chan interface{})
	Async(func() {
		switch res := (<-c).(type) {
		case TErrCode:
			onComplete(nil, res)
		default:
			onComplete(res, 0)
		}
	})
	return c
}

func WaitTrue(action func()) chan<- bool {
	c := make(chan bool)
	Async(func() {
		if <-c {
			action()
		}
	})
	return c
}
