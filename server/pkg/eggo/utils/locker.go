package utils

import (
	"sync"
)

type Locker struct {
	mtx *sync.RWMutex
}

func NewLocker() *Locker {
	return &Locker{
		mtx: &sync.RWMutex{},
	}
}

func (l *Locker) Get(action func()) {
	l.mtx.RLock()
	action()
	l.mtx.RUnlock()
}

func (l *Locker) Set(action func()) {
	l.mtx.Lock()
	action()
	l.mtx.Unlock()
}

type Enable struct {
	enable bool
	mtx    sync.RWMutex
}

func NewEnable() *Enable {
	return &Enable{
		enable: true,
	}
}

func (e *Enable) Enable() (ok bool) {
	e.mtx.RLock()
	ok = e.enable
	e.mtx.RUnlock()
	return
}

func (e *Enable) Action(action Action) IError {
	e.mtx.RLock()
	if e.enable {
		action.Invoke()
		e.mtx.RUnlock()
		return nil
	}
	e.mtx.RUnlock()
	return NewError(ErrClosed, nil)
}

func (e *Enable) Close(action Action) bool {
	e.mtx.Lock()
	defer e.mtx.Unlock()
	if e.enable {
		action.Invoke()
		e.enable = false
		return true
	}
	return false
}

func (e *Enable) Reset() {
	e.mtx.Lock()
	e.enable = true
	e.mtx.Unlock()
}
