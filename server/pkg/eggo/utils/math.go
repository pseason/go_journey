package utils

import (
	"math"
)

const (
	Rad2Deg  float32 = 57.29578
	Deg2Rad  float32 = 0.01745329
	HalfPi   float32 = 1.5707963267948966
	kEpsilon float32 = 0.000001
)

func Round(d float32, bit int32) float32 {
	var v = math.Pow(10, float64(bit))
	return float32(math.Round(float64(d)*v) / v)
}

func Ceil(d float32, bit int32) float32 {
	var v = math.Pow(10, float64(bit))
	return float32(math.Ceil(float64(d)*v) / v)
}

func Floor(d float32, bit int32) float32 {
	var v = math.Pow(10, float64(bit))
	return float32(math.Floor(float64(d)*v) / v)
}

func ClampInt32(min, max, v int32) int32 {
	if v < min {
		return min
	} else if v > max {
		return max
	}
	return v
}

func ClampFloat(min, max, v float32) float32 {
	if v < min {
		return min
	} else if v > max {
		return max
	}
	return v
}

func FloorInt32(floor, v int32) (r, o int32) {
	if v < floor {
		r = floor
		o = floor - v
	} else {
		r = v
	}
	return
}

func CeilInt32(ceil, v int32) (r, o int32) {
	if v > ceil {
		r = ceil
		o = v - ceil
	} else {
		r = v
	}
	return
}

func MinInt64(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

func MaxInt64(a, b int64) int64 {
	if a < b {
		return b
	}
	return a
}

func MinInt32(a, b int32) int32 {
	if a < b {
		return a
	}
	return b
}

func MaxInt32(a, b int32) int32 {
	if a < b {
		return b
	}
	return a
}

func MinFloat(l, r float32) float32 {
	if l > r {
		return r
	} else {
		return l
	}
}

func FloatMax(l, r float32) float32 {
	if l > r {
		return l
	} else {
		return r
	}
}

func AbsFloat(d float32) float32 {
	return float32(math.Abs(float64(d)))
}

func Sqr(x float32) float32 {
	return x * x
}

func IsPowerOfTwo(mask int32) bool {
	return (mask & (mask - 1)) == 0
}

func Sqrt(v float32) float32 {
	return float32(math.Sqrt(float64(v)))
}

func NextPowerOfTwo(v uint32) uint32 {
	v -= 1
	v |= v >> 16
	v |= v >> 8
	v |= v >> 4
	v |= v >> 2
	v |= v >> 1
	return v + 1
}

func LerpFloat32(from, to, t float32) float32 {
	return to*t + from*(1.0-t)
}
