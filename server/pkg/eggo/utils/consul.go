package utils

import consulApi "github.com/hashicorp/consul/api"

var (
	_Consul       *consulApi.Client
	_ConsulConfig *consulApi.Config
)

func Consul() *consulApi.Client {
	return _Consul
}

func ConsulConfig() *consulApi.Config {
	return _ConsulConfig
}

func StartConsul(config *consulApi.Config) (err error) {
	_ConsulConfig = config
	_Consul, err = consulApi.NewClient(_ConsulConfig)
	return
}
