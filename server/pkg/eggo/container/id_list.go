package container

import (
	"95eh.com/eg/utils"
)

// IdList int64数组
// 可自动缩容
type IdList struct {
	cap  int
	list []int64
}

func NewIdList(cap int) *IdList {
	return &IdList{
		cap:  cap,
		list: make([]int64, 0, cap),
	}
}

// Has 是否包含元素
func (l *IdList) Has(id int64) bool {
	for _, d := range l.list {
		if d == id {
			return true
		}
	}
	return false
}

// Push 添加元素到末端
func (l *IdList) Push(id int64) {
	l.list = append(l.list, id)
}

// Pop 从末端移除元素
func (l *IdList) Pop() (id int64, ok bool) {
	if len(l.list) == 0 {
		return
	}
	i := len(l.list) - 1
	id = l.list[i]
	ok = true

	ln, c, ok := l.needShrink(1)
	if !ok {
		l.list = l.list[:i]
	} else {
		lt := make([]int64, ln, c)
		copy(lt, l.list[:i])
		l.list = lt
	}
	return
}

// Unshift 添加元素到头部
func (l *IdList) Unshift(id int64) {
	l.list = append([]int64{id}, l.list...)
}

// Shift 从头部移除元素
func (l *IdList) Shift() (id int64, ok bool) {
	if len(l.list) == 0 {
		return
	}
	id = l.list[0]
	ok = true

	ln, c, ok := l.needShrink(1)
	if !ok {
		l.list = l.list[1:]
	} else {
		lt := make([]int64, ln, c)
		copy(lt, l.list[1:])
		l.list = lt
	}
	return
}

func (l *IdList) needShrink(delCount int) (ln, c int, ok bool) {
	if delCount < 1 {
		return
	}
	c = cap(l.list) >> 1
	ln = len(l.list) - delCount
	ok = ln < c && l.cap < c
	return
}

// Remove 移除指定元素
func (l *IdList) Remove(id int64) bool {
	for i, d := range l.list {
		if d == id {
			ln, c, ok := l.needShrink(1)
			if !ok {
				l.list = append(l.list[:i], l.list[i+1:]...)
			} else {
				lt := make([]int64, ln, c)
				copy(lt, l.list[:i])
				copy(lt[i:], l.list[i+1:])
				l.list = lt
			}
			return true
		}
	}
	return false
}

// Splice 从start开始移除delCount个元素,并在该位置插入newItems元素
func (l *IdList) Splice(start int, delCount uint, newItems ...int64) {
	dc := int(delCount)
	nl := len(newItems)
	if nl < dc {
		ln, c, ok := l.needShrink(dc - nl)
		if ok {
			lt := make([]int64, ln, c)
			copy(lt, l.list[:start])
			copy(lt[start:], newItems)
			copy(lt[start+nl:], l.list[start+dc:])
			l.list = lt
			return
		}
	}
	l.list = append(l.list[:start], append(newItems, l.list[start+dc:]...)...)
}

// Iter 迭代元素
func (l *IdList) Iter(action utils.ActionInt64) {
	for _, id := range l.list {
		action(id)
	}
}

// Values 所有元素
func (l *IdList) Values() []int64 {
	return l.list
}

// Copy 复制所有元素
func (l *IdList) Copy() []int64 {
	list := make([]int64, len(l.list))
	copy(list, l.list)
	return list
}

// IsEmpty 是否没有元素
func (l *IdList) IsEmpty() bool {
	return len(l.list) == 0
}

func (l *IdList) Len() int {
	return len(l.list)
}

func (l *IdList) Cap() int {
	return cap(l.list)
}
