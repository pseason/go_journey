package container

import (
	"fmt"
	"testing"
)

func TestNewIdList_Remove(t *testing.T) {
	list := NewIdList(16)
	count := int64(64)
	for i := int64(0); i < count; i++ {
		list.Push(i)
	}
	for i := int64(0); i < count; i++ {
		ok := list.Remove(i)
		if !ok {
			t.Error("remove item failed")
			continue
		}
	}
	ok := list.Remove(0)
	if ok {
		t.Error("removed not exist item")
	}
}

func TestIdList_Splice(t *testing.T) {
	list := NewIdList(64)
	for i := int64(0); i < 32; i++ {
		list.Push(i)
	}
	newItems := make([]int64, 0, 32)
	for i := int64(0); i < 32; i++ {
		newItems = append(newItems, i+32)
	}
	list.Splice(0, 16, newItems...)
	fmt.Println(list.Values())
	list.Splice(0, 32, 100)
	fmt.Println(list.Values())
}
