package event

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
)

type processor struct {
	id     int64
	action utils.ActionAny
}

func NewEventDispatcher() *eventDispatcher {
	return &eventDispatcher{
		typeToProcessor: make(map[intfc.TEvent][]*processor),
		idToType:        make(map[int64]intfc.TEvent),
	}
}

type eventDispatcher struct {
	typeToProcessor map[intfc.TEvent][]*processor
	idToType        map[int64]intfc.TEvent
}

func (e *eventDispatcher) BindEvent(t intfc.TEvent, action utils.ActionAny) int64 {
	id := utils.GenSnowflakeRegionNodeId()
	p := &processor{
		id:     id,
		action: action,
	}
	l, ok := e.typeToProcessor[t]
	if ok {
		e.typeToProcessor[t] = append(l, p)
	} else {
		e.typeToProcessor[t] = []*processor{p}
	}
	e.idToType[id] = t
	return id
}

func (e *eventDispatcher) UnbindEvent(id int64) {
	t, ok := e.idToType[id]
	if !ok {
		return
	}
	list := e.typeToProcessor[t]
	for i, p := range list {
		if p.id == id {
			e.typeToProcessor[t] = append(list[:i], list[i+1:]...)
			break
		}
	}
}

func (e *eventDispatcher) DispatchEvent(t intfc.TEvent, event interface{}) {
	l, ok := e.typeToProcessor[t]
	if !ok {
		return
	}
	for _, p := range l {
		p.action(event)
	}
}
