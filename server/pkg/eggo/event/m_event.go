package event

import "95eh.com/eg/intfc"

func NewMEvent(moduleOpts []intfc.ModuleOption) intfc.IMEvent {
	return &mEvent{
		IModule:          intfc.NewModule(moduleOpts...),
		IEventDispatcher: NewEventDispatcher(),
	}
}

type mEvent struct {
	intfc.IModule
	intfc.IEventDispatcher
}

func (M *mEvent) Type() intfc.TModule {
	return intfc.MEvent
}