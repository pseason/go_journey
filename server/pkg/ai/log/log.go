package log

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"fmt"
	"github.com/gookit/color"
	jsoniter "github.com/json-iterator/go"
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/tool"
	utils2 "hm/pkg/misc/utils"
)

type AiLogType string
type AiLogM map[string]interface{}

var (
	debug = false
)

const (
	AiLogSuccess AiLogType = "SUCCESS"
	AILogError   AiLogType = "ERROR"
	AILogWarn    AiLogType = "WARN"
	AiLogInfo    AiLogType = "INFO"
	AiLogDebug   AiLogType = "DEBUG"
)

const (
	AiActorLogTag = "_ai_actor"
	AiActorLogId  = "_ai_actor_id"
)

type logTrace struct {
	Level         intfc.TLoggerLevel `json:"level"`
	Time          string             `json:"time"`
	Msg           string             `json:"msg"`
	Caller        string             `json:"caller"`
	Params        utils.M            `json:"params"`
	RuntimeCaller string             `json:"runtime_caller"`
	// Ts sort field
	Ts int64 `json:"ts"`
	// Color terminal color
	Color string `json:"color"`
}

func (l *logTrace) ToLine() string {
	prefix, c := l.GetLevelPrefix()
	return c.Sprintf("%s %s %s %s %s %s", prefix, l.Time, l.Msg, l.Caller,
		utils2.If3(len(l.Params) == 0, "", l.Params.Json()).(string), l.RuntimeCaller)
}

func (l *logTrace) ToIndentJson() string {
	_, c := l.GetLevelPrefix()
	l.Color = c.Code()
	bytes, err := jsoniter.MarshalIndent(l, "", "    ")
	if err == nil {
		return string(bytes)
	}
	return string(bytes)
}

func (l *logTrace) GetLevelPrefix() (prefix string, c *color.Theme) {
	switch l.Level {
	case intfc.TLogDebug:
		return "[TD]", color.Debug
	case intfc.TLogInfo:
		return "[TI]", color.Info
	case intfc.TLogWarn:
		return "[TW]", color.Warn
	case intfc.TLogError:
		return "[TE]", color.Error
	case intfc.TLogFatal:
		return "[TF]", color.Danger
	}
	return "[TN]", color.Note
}

type IAiLog interface {
	intfc.ILogger
	// ActorLog 行动日志
	ActorLog(level intfc.TLoggerLevel, timestamp, caller, msg string, params utils.M)
	// SetIndentFormat 设置缩进格式化
	SetIndentFormat(r bool) IAiLog
	// SetIsolation 设置隔离输出
	SetIsolation(r bool) IAiLog
}

func SetLogDebug(r bool) {
	debug = r
}

func AiLogExec(t AiLogType, category inc.BtCategory, tick *cores.Tick, msg string, d ...interface{}) {
	actor := tool.GetTickIActor(tick)
	title := tick.Tree().Title()
	s := fmt.Sprintf("%v [%s] [%s(%d)] | : %s —————— %v", utils2.GetCurrentTimeDefaultFormat(), t, title,
		actor.Id(), msg, d)
	switch category {
	case inc.ACTION:
		color.BgBlue.Println(s)
	case inc.DECORATOR:
		color.BgGreen.Println(s)
	case inc.CONDITION:
		color.BgCyan.Println(s)
	default:
		color.BgDarkGray.Println(s)
	}
}

func AiLog(t AiLogType, tick *cores.Tick, msg string, d ...interface{}) {
	category := tick.Tree().Root().Category()
	AiLogExec(t, category, tick, msg, d)
}

func AiDebugLog(tick *cores.Tick, msg string, d ...interface{}) {
	if debug {
		category := tick.Tree().Root().Category()
		AiLogExec(AiLogDebug, category, tick, msg, d)
	}
}

func initDefaultUtilsM(m utils.M) (res utils.M) {
	if m == nil {
		res = utils.M{
			AiActorLogTag: true,
		}
		return
	}
	m[AiActorLogTag] = true
	return m
}

func DLogError(format string, m utils.M, a ...interface{}) {
	app.Log().Error(fmt.Sprintf(format, a...), initDefaultUtilsM(m))
}

func DLogWarn(format string, m utils.M, a ...interface{}) {
	app.Log().Warn(fmt.Sprintf(format, a...), initDefaultUtilsM(m))
}

func DLogInfo(format string, m utils.M, a ...interface{}) {
	app.Log().Info(fmt.Sprintf(format, a...), initDefaultUtilsM(m))
}

func DLogDebug(format string, m utils.M, a ...interface{}) {
	if debug {
		app.Log().Debug(fmt.Sprintf(format, a...), initDefaultUtilsM(m))
	}
}
