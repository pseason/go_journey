package log

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/utils"
	"fmt"
	"github.com/gookit/color"
	"github.com/olivere/elastic"
	esConfig "github.com/olivere/elastic/config"
	"hm/pkg/ai/inc"
	"hm/pkg/misc"
	utils2 "hm/pkg/misc/utils"
)

type aiLogger struct {
	*log.BaseLogger
	indent    bool
	isolation bool
	esClient  *elastic.Client
}

func NewAiLogger(level intfc.TLoggerLevel, esCfg *esConfig.Config) IAiLog {
	res := &aiLogger{
		BaseLogger: log.NewBaseLogger(intfc.TLoggerStdout, level),
		esClient:   misc.GetElasticSearchConn(esCfg),
	}
	return res
}

func (a *aiLogger) SetIndentFormat(r bool) IAiLog {
	a.indent = r
	return a
}

func (a *aiLogger) SetIsolation(r bool) IAiLog {
	a.isolation = r
	return a
}

func (a *aiLogger) GetParamsIndent(params utils.M) string {
	if len(params) == 0 {
		return ""
	}
	return fmt.Sprintf("\n%s\n", params.IndentJson())
}

func (a *aiLogger) GetParams(params utils.M) string {
	if len(params) == 0 {
		return ""
	}
	return fmt.Sprintf("\n%s\n", params.Json())
}

func (a *aiLogger) Log(level intfc.TLoggerLevel, timestamp, caller, msg string, params utils.M) {
	_, isActorLog := params[AiActorLogTag]
	//var actorId interface{}
	//if isActorLog {
	//	delete(params, AiActorLogTag)
	//	var has bool
	//	actorId, has = params[AiActorLogId]
	//	if !has {
	//		actorId = "system"
	//	} else {
	//		delete(params, AiActorLogId)
	//	}
	//}
	lt := logTrace{
		Time:   timestamp,
		Msg:    msg,
		Caller: caller,
		Params: params,
		Ts:     utils2.GetCurrentMillisecond(),
	}
	logStr := lt.ToLine()
	if isActorLog {
		//_, err := a.esClient.Index().Index(fmt.Sprintf("ai.%v.log", actorId)).Type("_doc").
		//	BodyJson(lt.ToIndentJson()).
		//	Do(context.Background())
		//if err != nil {
		//	a.Log(intfc.TLogError, utils2.GetCurrentTimeDefaultFormatAndMs(), inc.GetRuntimeCaller(1), "log to es failed", utils.M{
		//		"error": err,
		//	})
		//}
		//filename := fmt.Sprintf("%s/%v.log", a.outDir, actorId)
		//file, err := os.OpenFile(fmt.Sprintf("%s/%s.log", a.outDir, actorId), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		//if err != nil {
		//	a.Log(intfc.TLogError, utils2.GetCurrentTimeDefaultFormatAndMs(), inc.GetRuntimeCaller(1), "open log file failed", utils.M{
		//		"fileName": filename,
		//		"err":      err,
		//	})
		//}
		//defer func() {
		//	_ = file.Close()
		//}()
		//_, err = fmt.Fprintln(file, logStr)
		//if err != nil {
		//	a.Log(intfc.TLogError, utils2.GetCurrentTimeDefaultFormatAndMs(), inc.GetRuntimeCaller(1), "write log to file failed", utils.M{
		//		"fileName": filename,
		//		"err":      err,
		//	})
		//}
		//if a.onlyOutFile {
		//	return
		//}
	}
	fmt.Println(logStr)
}

func (a *aiLogger) ActorLog(level intfc.TLoggerLevel, timestamp, caller, msg string, params utils.M) {
	//todo params in actor
	baseFmt := fmt.Sprintf(" %s %s %s %s", timestamp, msg, caller, a.GetParams(params))
	//id, has := params[AiActorLogId]
	//if !has {
	//	id = "system"
	//}
	switch level {
	case intfc.TLogDebug:
		//baseFmt = fmt.Sprintf("[TD] %s", baseFmt)
		baseFmt = color.Red.Sprintf("[TD] %s", baseFmt)
	case intfc.TLogInfo:
		baseFmt = fmt.Sprintf("[TI] %s", baseFmt)
	case intfc.TLogWarn:
		baseFmt = fmt.Sprintf("[TW] %s", baseFmt)
	case intfc.TLogError:
		runtimeCaller := inc.GetRuntimeCaller(3)
		baseFmt = fmt.Sprintf("[TE] %s %s", baseFmt, runtimeCaller)
	case intfc.TLogFatal:
		runtimeCaller := inc.GetRuntimeCaller(3)
		baseFmt = fmt.Sprintf("[TF] %s %s", baseFmt, runtimeCaller)
	}
	//filename := fmt.Sprintf("%s/%s.log", a.outDir, id)
	//file, err := os.OpenFile(fmt.Sprintf("%s/%s.log", a.outDir, id), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	//if err != nil {
	//	a.Log(intfc.TLogError, utils2.GetCurrentTimeDefaultFormatAndMs(), inc.GetRuntimeCaller(1), "open log file failed", utils.M{
	//		"fileName": filename,
	//		"err":      err,
	//	})
	//}
	//defer func() {
	//	_ = file.Close()
	//}()
	//_, err = fmt.Fprintln(file, baseFmt)
	//if err != nil {
	//	a.Log(intfc.TLogError, utils2.GetCurrentTimeDefaultFormatAndMs(), inc.GetRuntimeCaller(1), "write log to file failed", utils.M{
	//		"fileName": filename,
	//		"err":      err,
	//	})
	//}
}

func (a *aiLogger) Sign(parent, id int64, timestamp, caller string, params utils.M) {
	color.Cyanln(fmt.Sprintf("[TS] %s %d<-%d %s %s", timestamp, parent, id, caller, a.GetParams(params)))
}

func (a *aiLogger) Trace(level intfc.TLoggerLevel, id int64, msg, timestamp, caller string, stack []byte, params utils.M) {
	baseFmt := fmt.Sprintf(" %s %d %s %s %s", timestamp, id, msg, caller, a.GetParams(params))
	switch level {
	case intfc.TLogDebug:
		color.Cyanln("[TD]", baseFmt)
	case intfc.TLogInfo:
		color.Infoln("[TI]", baseFmt)
	case intfc.TLogWarn:
		color.Warnln("[TW]", baseFmt)
	case intfc.TLogError:
		runtimeCaller := inc.GetRuntimeCaller(3)
		color.Errorln("[TE]", baseFmt, runtimeCaller, stack)
	case intfc.TLogFatal:
		runtimeCaller := inc.GetRuntimeCaller(3)
		color.BgRed.Println("[TF]", baseFmt, runtimeCaller, stack)
	}
}
