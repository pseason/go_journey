package impl

import (
	"95eh.com/eg/utils"
	"fmt"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"hm/pkg/services/space/common"
	"hm/pkg/services/space/msg_spc"
	"strconv"
)

func (a *ai) registerEvents() map[msg_spc.MsgCode]interf.AiEventAction {
	return map[msg_spc.MsgCode]interf.AiEventAction{
		msg_spc.CdEveActorVisible: func(ctx interf.ActorActionCtx, body interface{}) {
			req := body.(*msg_spc.EveActorVisible)
			if req.TargetType == common.ActorPlayer {
				actor, ok := a.tick.GetActor(req.ActorId)
				if ok {
					wa := &interf.WarnActor{
						Id:       req.TargetId,
						Type:     req.TargetType,
						Position: utils.Vec3{X: req.PosX, Y: req.PosY, Z: req.PosZ},
						Forward:  utils.Vec3{X: req.ForX, Y: req.ForY, Z: req.ForZ},
					}
					actor.WarnActor().Set(strconv.FormatInt(req.TargetId, 10), wa)
					log.DLogDebug("行动者进入视野", utils.M{
						"actorId":   actor.Id(),
						"warnActor": wa,
					})
				}
			}
		},
		msg_spc.CdEveActorInvisible: func(ctx interf.ActorActionCtx, body interface{}) {
			req := body.(*msg_spc.EveActorInvisible)
			actor, ok := a.tick.GetActor(req.ActorId)
			if ok {
				actor.WarnActor().Remove(strconv.FormatInt(req.TargetId, 10))
				log.DLogDebug("行动者离开视野", utils.M{
					"actorId":  actor.Id(),
					"targetId": req.TargetId,
				})
			}
		},
		msg_spc.CdEveActorMoveStart: func(ctx interf.ActorActionCtx, body interface{}) {
			req := body.(*msg_spc.EveActorMoveStart)
			a.updateActorPosition(req.ActorId, req.TargetId, req.Type,
				&utils.Vec3{X: req.PosX, Y: req.PosY, Z: req.PosZ},
				&utils.Vec3{X: req.ForX, Y: req.ForY, Z: req.ForZ},
				"事件-移动开始",
			)
		},
		msg_spc.CdEveActorForwardChange: func(ctx interf.ActorActionCtx, body interface{}) {

		},
		msg_spc.CdEveActorPositionChange: func(ctx interf.ActorActionCtx, body interface{}) {

		},
		msg_spc.CdEveActorMoveStop: func(ctx interf.ActorActionCtx, body interface{}) {
			req := body.(*msg_spc.EveActorMoveStop)
			a.updateActorPosition(req.ActorId, req.TargetId, req.Type,
				&utils.Vec3{X: req.PosX, Y: req.PosY, Z: req.PosZ},
				nil,
				"事件-移动停止",
			)
		},
		msg_spc.CdEveMove: func(ctx interf.ActorActionCtx, body interface{}) {
			req := body.(*msg_spc.EveMove)
			a.updateActorPosition(req.ActorId, req.TargetId, req.Type,
				&utils.Vec3{X: req.PositionX, Y: req.PositionY, Z: req.PositionZ},
				&utils.Vec3{X: req.ForwardX, Y: req.ForwardY, Z: req.ForwardZ},
				"事件-移动改变",
			)
		},
		msg_spc.CdEveActorDeath: func(ctx interf.ActorActionCtx, body interface{}) {
			req := body.(*msg_spc.EveActorDeath)
			//触发累加死亡计数
			if req.IsDeath >0 {
				actor, ok := a.tick.GetActor(req.ActorId)
				if ok {
					actor.DeathCallback()
				}
			}

		},
	}
}

func (a *ai) updateActorPosition(actorId, warnActorId int64, updateType common.TActor, position, forward *utils.Vec3, tag string) {
	actor, ok := a.tick.GetActor(actorId)
	if ok {
		if updateType == common.ActorPlayer {
			wa := &interf.WarnActor{
				Id:       warnActorId,
				Type:     common.ActorPlayer,
				Position: *position,
			}
			if forward != nil {
				wa.Forward = *forward
			} else {
				wa.Forward = *position
			}
			actor.WarnActor().Set(strconv.FormatInt(warnActorId, 10), wa)
			log.DLogDebug(fmt.Sprintf("行动者刷新视野玩家位置(%s) - %d-%d (x:%f,z:%f)", tag, actorId, warnActorId, position.X, position.Z), nil)
		} else {
			actor.SetPosition(position, forward)
			log.DLogDebug(fmt.Sprintf("野怪刷新位置(%s) - %d (x:%f,z:%f)", tag, actorId, position.X, position.Z), nil)
		}
	}
}
