package impl

import (
	"95eh.com/eg/utils"
	"hm/pkg/game/svc"
)

const (
	ErrNodeRequestFailed = utils.TErrCode(uint32(svc.Ai)*10000) + iota
	ErrNodeActorRemoveFailed
)
