package impl

import (
	"95eh.com/eg/utils"
	"errors"
	"fmt"
	cmap "github.com/orcaman/concurrent-map"
	"github.com/panjf2000/ants/v2"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"strconv"
	"sync"
	"time"
)

type aiTick struct {
	base      interf.IAi
	_interval *time.Ticker
	// KEY=ServiceId
	actorMaps     map[uint16]cmap.ConcurrentMap
	actorRelation map[int64]uint16
	actorsMu      sync.RWMutex
	warnPlayers   cmap.ConcurrentMap
	running       bool
}

func NewAiTick(ai interf.IAi) interf.IAiTick {
	return &aiTick{
		base:          ai,
		actorMaps:     make(map[uint16]cmap.ConcurrentMap),
		actorRelation: make(map[int64]uint16),
		warnPlayers:   cmap.New(),
	}
}

func (a *aiTick) Start() {
	if !a.base.Running() || a.running {
		return
	}
	a._interval = time.NewTicker(time.Millisecond * time.Duration(a.base.Conf().TickTime))
	a.running = true
	log.DLogInfo("ai tick started", nil)
	for {
		select {
		case <-a._interval.C:
			a.actorsMu.RLock()
			for _, actors := range a.actorMaps {
				actors.IterCb(func(key string, v interface{}) {
					item := v.(interf.IAiActor)
					if !item.IsDeath() {
						//todo context -> done
						ants.Submit(func() {
							item.Tick()
						})
					}
				})
			}
			a.actorsMu.RUnlock()
		case <-a.base.GetContext().Done():
			a._interval.Stop()
			a.running = false
			log.DLogInfo("tick interval stop", nil)
			return
		}
	}
}

func (a *aiTick) Running() bool {
	return a.running
}

func (a *aiTick) pushBeforeValidate(actors ...interf.IAiActor) (err error) {
	for _, actor := range actors {
		if actor == nil || actor.Meta().Node == 0 || actor.Meta().ServiceId == 0 {
			err = errors.New("actor meta data is empty")
			return
		}
	}
	return
}

func (a *aiTick) getSid(actor interf.IAiActor) uint16 {
	return actor.Meta().ServiceId
}

func (a *aiTick) Push(lock bool, actor interf.IAiActor) (err error) {
	return a.PushAll(lock, actor)
}

func (a *aiTick) PushAll(lock bool, actors ...interf.IAiActor) (err error) {
	err = a.pushBeforeValidate(actors...)
	if err != nil {
		return
	}
	if lock {
		a.actorsMu.Lock()
		defer a.actorsMu.Unlock()
	}
	for _, item := range actors {
		actor := item
		if _, rHas := a.actorRelation[actor.Id()]; rHas {
			continue
		}
		sid := a.getSid(actor)
		actors, has := a.actorMaps[sid]
		if !has {
			actors = cmap.New()
		}
		actors.Set(actor.IdString(), actor)
		a.actorRelation[actor.Id()] = sid
		a.actorMaps[sid] = actors
		if lock {
			a.pushSucceedLog(actor)
		}
	}
	return
}

func (a *aiTick) RemoveAll() {
	a.actorsMu.Lock()
	defer a.actorsMu.Unlock()
	for _, v := range a.actorMaps {
		actors := v.IterBuffered()
		for t := range actors {
			actor := t.Val.(interf.IAiActor)
			_ = actor.RemoteClear()
		}
	}
	a.actorMaps = make(map[uint16]cmap.ConcurrentMap)
	a.actorRelation = make(map[int64]uint16)
	log.DLogInfo("remove all actor succeed", nil)
}

func (a *aiTick) ClearAllBySpaceSid(sid uint16) {
	a.actorsMu.Lock()
	defer a.actorsMu.Unlock()
	actors, has := a.actorMaps[sid]
	if !has {
		log.DLogInfo("not found monster actors by space service id", utils.M{
			"sid": sid,
		})
		return
	}
	if has {
		count := len(actors)
		actors.IterCb(func(key string, v interface{}) {
			actor := v.(interf.IAiActor)
			delete(a.actorRelation, actor.Id())
		})
		delete(a.actorMaps, sid)
		log.DLogInfo("succeed the clear scene tick actor by space service id", utils.M{
			"current-count": len(actors),
			"remove-count":  count,
			"sid":           sid,
		})
	}
}

func (a *aiTick) pushSucceedLog(item interf.IAiActor) {
	born := item.GetBornPoint()
	log.DLogInfo("add actor tick succeed", utils.M{
		"id":           item.Id(),
		"ai-title":     item.Title(),
		"monster-name": item.Template().Name,
		"position":     fmt.Sprintf("x:%f,y:%f,z:%f", born.X, born.Y, born.Z),
	})
}

func (a *aiTick) GetSpaceSidGroupMetas() (metas []interf.ActorMetaInfo) {
	a.actorsMu.Lock()
	defer a.actorsMu.Unlock()
	metas = make([]interf.ActorMetaInfo, 0, len(a.actorMaps))
	for _, v := range a.actorMaps {
		if v.Count() > 0 {
			for item := range v.IterBuffered() {
				actor := item.Val.(interf.IAiActor)
				metas = append(metas, actor.Meta())
				break
			}
		}
	}
	return
}

func (a *aiTick) Remove(actorId int64) bool {
	a.actorsMu.Lock()
	defer a.actorsMu.Unlock()
	sid, has := a.actorRelation[actorId]
	if has {
		if actors, sHas := a.actorMaps[sid]; sHas {
			k := strconv.FormatInt(actorId, 10)
			if actors.Has(k) {
				actors.Remove(k)
				delete(a.actorRelation, actorId)
				log.DLogInfo("remove actor tick: %d", nil, actorId)
				return true
			}
		}
	}
	return false
}

func (a *aiTick) GetActor(actorId int64) (actor interf.IAiActor, ok bool) {
	a.actorsMu.Lock()
	defer a.actorsMu.Unlock()
	sid, has := a.actorRelation[actorId]
	if has {
		if actors, sHas := a.actorMaps[sid]; sHas {
			k := strconv.FormatInt(actorId, 10)
			var val interface{}
			val, ok = actors.Get(k)
			if ok {
				actor = val.(interf.IAiActor)
			}
		}
	}
	return
}

func (a *aiTick) WarnPlayers() cmap.ConcurrentMap {
	return a.warnPlayers
}
