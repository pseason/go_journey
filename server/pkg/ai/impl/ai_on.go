package impl

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"fmt"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"hm/pkg/game/svc"
	"hm/pkg/misc"
	spc_common "hm/pkg/services/space/common"
	"strconv"
	"time"
)

func (a *ai) OnNodeConnected(node intfc.TNode, regionId, nodeId uint16) {
	waitTime := 5
	ticker := time.NewTicker(time.Second)
	log.DLogDebug(fmt.Sprintf("等待%ds向空间服发送野怪", waitTime), nil)
	go func() {
		t := waitTime - 1
		for {
			select {
			case <-ticker.C:
				log.DLogDebug(fmt.Sprintf("等待%ds向空间服发送野怪", t), nil)
				t--
				if t == 0 {
					ticker.Stop()
					return
				}
			}
		}
	}()
	time.Sleep(time.Second * time.Duration(waitTime))
	a.InitAi(node,nodeId,0)
}

func (a *ai) InitAi(node misc.TNode,nodeId uint16,tplId int32) {
	meta := interf.ActorMetaInfo{Node: node, NodeId: nodeId, ServiceId: svc.Space}
	actors, actorIds, err := a.InitSceneMonsters(meta,tplId)
	if err != nil {
		log.DLogError("failed the ai monster init", utils.M{
			"err":  err,
			"meta": meta,
		})
	}
	if len(actors) > 0 {
		log.DLogDebug("预备向空间服发送野怪进入", utils.M{"actorIds": actorIds})
		for _, v := range actors {
			actor := v.(*AiActor)
			tErr := app.SceneCache().SaveActor(actor.Id(), spc_common.ActorMonster, nil, &spc_common.ActorComTransform{
				Position: utils.Vec3{
					X: actor.position.X,
					Y: actor.position.Y,
					Z: actor.position.Z,
				},
				Forward: utils.Vec3{
					X: 1,
				},
				MoveSpeed: 4,
			}, &spc_common.ActorComLife{
				NickName: "monster_" + strconv.FormatInt(actor.Id(), 10),
				Level:    1,
				Gender:   false,
			})
			if tErr != nil {
				continue
			}
			//todo 此处的sceneId为临时的
			a.Space().RequestActorEnterScene(0, actor.Id(), misc.SceneCity, 1, func(v int64, obj interface{}) {
				log.DLogInfo("野怪进入场景成功", utils.M{
					"actorId": actor.Id(),
				})
			}, func(e utils.IError) {
				log.DLogError("野怪进入场景失败", utils.M{
					"actorId": actor.Id(),
				})
			})

		}

	}
}


func (a *ai) OnNodeClosed(node intfc.TNode, regionId, nodeId uint16) {
	fmt.Println("closed node", node, regionId, nodeId)
}
