package impl

import (
	"95eh.com/eg/app"
	"95eh.com/eg/utils"
	cmap "github.com/orcaman/concurrent-map"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/space/common"
	"hm/pkg/services/space/msg_spc"
	"strconv"
	"sync"
	"time"
)

type AiActor struct {
	id           int64
	idString     string
	ai           interf.IAi
	blackboard   *cores.Blackboard
	tree         *cores.BehaviorTree
	death        bool
	typ          interf.TAI
	bornPoint    utils.Vec3
	template     *tsv.MonsterTsv
	scene        misc.TScene
	meta         interf.ActorMetaInfo
	position     utils.Vec3
	forward      utils.Vec3
	mu           sync.RWMutex
	lastMoveTime time.Time
	moveSpeed    float32
	moving       bool
	warnDistance float32
	// EXPORT
	warnActor cmap.ConcurrentMap // value see interf.WarnActor

}

func (a *AiActor) WarnActor() cmap.ConcurrentMap {
	return a.warnActor
}

func (a *AiActor) SetPosition(position *utils.Vec3, forward *utils.Vec3) {
	a.mu.Lock()
	defer a.mu.Unlock()
	if position != nil {
		a.position = utils.Vec3{
			X: position.X,
			Y: position.Y,
			Z: position.Z,
		}
	}
	if forward != nil {
		a.forward = utils.Vec3{
			X: forward.X,
			Y: forward.Y,
			Z: forward.Z,
		}
	}
}

func (a *AiActor) Position() utils.Vec3 {
	a.mu.RLock()
	defer a.mu.RUnlock()
	return a.position
}

func (a *AiActor) Id() int64 {
	return a.id
}

func (a *AiActor) IdString() string {
	if a.idString == "" {
		a.idString = strconv.FormatInt(a.id, 10)
	}
	return a.idString
}

func (a *AiActor) Scene() misc.TScene {
	return a.scene
}

func (a *AiActor) Meta() interf.ActorMetaInfo {
	return a.meta
}

func (a *AiActor) Event(eventId int32, body interface{}) (err error) {
	return
}

func (a *AiActor) Move(speed float32, v *utils.Vec3, currentVec3 *utils.Vec3) (err error) {
	if nil == currentVec3 {
		tp := a.Position()
		currentVec3 = &tp
	}
	if nil == v {
		//todo nav mesh
		v = currentVec3
		//position := aiNavMesh.GetAroundRand(*curVec3, 5)
		//position.Y = curVec3.Y
		//v = &position
	}
	fw := utils.Vec3Normalize(utils.Vec3Sub(utils.Vec3{
		X: v.X + 1,
		Y: v.Y,
		Z: v.Z + 1,
	}, *currentVec3))
	_, err = a.RequestMetaNode(msg_spc.CdActorMoveStart, &msg_spc.ReqActorMoveStart{
		ActorId: a.id,
		PosX:    currentVec3.X,
		PosY:    currentVec3.Y,
		PosZ:    currentVec3.Z,
		ForX:    fw.X,
		ForY:    fw.Y,
		ForZ:    fw.Z,
	})
	if err == nil {
		a.forward = fw
		a.moveSpeed = speed
		a.moving = true
		a.lastMoveTime = time.Now()
		log.DLogDebug("开始移动成功", nil, currentVec3.X, currentVec3.Y, currentVec3.Z)
	}
	return
}

func (a *AiActor) StopMove() (err error) {
	currentVec3 := a.Position()
	_, err = a.RequestMetaNode(msg_spc.CdActorMoveStop, &msg_spc.ReqActorMoveStop{
		PosX: currentVec3.X,
		PosY: currentVec3.Y,
		PosZ: currentVec3.Z,
	})
	if err != nil {
		a.moving = false
	} else {
		log.DLogDebug("停止移动成功", nil, currentVec3.X, currentVec3.Y, currentVec3.Z)
	}
	return
}

func (a *AiActor) TickMove() {
	now := time.Now()
	dur := float32(now.Sub(a.lastMoveTime).Milliseconds()) / 1000
	speed := dur * a.moveSpeed
	ox := a.forward.X * speed
	oy := a.forward.Y * speed
	oz := a.forward.Z * speed
	a.lastMoveTime = now
	a.position = utils.Vec3{
		X: a.position.X + ox,
		Y: a.position.Y + oz,
		Z: a.position.Z + oy,
	}
	log.DLogDebug("野怪位置刷新", utils.M{"position": a.position})
}

func (a *AiActor) GetAttacker() {
	panic("implement me")
}

func (a *AiActor) GetHP() {
	panic("implement me")
}

func (a *AiActor) Type() interf.TAI {
	return a.typ
}

func (a *AiActor) UseSkill() {
	panic("implement me")
}

func (a *AiActor) GetWarnPlayer() {
	panic("implement me")
}

func (a *AiActor) AttrChange() {
	panic("implement me")
}

func (a *AiActor) IsDeath() bool {
	return a.death
}

func (a *AiActor) Tick() {
	//startTime := carbon.Now().ToTimestampWithMicrosecond()
	//if a.moving {
	//	a.TickMove()
	//}//
	a.tree.Tick(a, a.blackboard)
	//log.DLogDebug("%s tick executed, time consumed: %d µs", nil, a.tree.Title(),
	//	carbon.Now().ToTimestampWithMicrosecond()-startTime)
}

func (a *AiActor) ReviveCallback() {
	a.blackboard = cores.NewBlackboard()
	a.death = false
}

func (a *AiActor) DeathCallback() {
	//死亡累计召唤
	a.ai.IncDeathNum(a.typ)
	currentDeathNum := a.ai.GetDeathNum(a.typ)
	deathConf := a.blackboard.GetGlobal(interf.CumulativeCallDeathNum).(int32)
	if currentDeathNum >= deathConf {
		highTplId := a.blackboard.GetGlobal(interf.AddHighAiByLowAiDeath).(int32)
		//todo 生成羊王
		a.ai.InitAi(a.Meta().Node,a.Meta().NodeId,highTplId)
	}
	a.blackboard = nil
	a.death = true

}

func (a *AiActor) Title() string {
	return a.tree.Title()
}

func (a *AiActor) Init() {
	a.blackboard = cores.NewBlackboard()
	a.id = utils.GenSnowflakeGlobalNodeId()
}

func (a *AiActor) GetBornPoint() (p utils.Vec3) {
	return a.bornPoint
}

func (a *AiActor) Template() tsv.MonsterTsv {
	return *a.template
}

func (a *AiActor) GetPatrolSpeed() float32 {
	return a.template.PatrolSpeed
}

func (a *AiActor) GetWarnRadius() int32 {
	return a.template.WarnRadius
}

func (a *AiActor) GetMaxHaredCount() int32 {
	return a.template.MaxHaredCount
}

func (a *AiActor) GetRebirthDuration() int32 {
	return a.template.RebirthDuration
}

func (a *AiActor) Instance() interface{} {
	return a
}

func (a *AiActor) RequestMetaNode(code uint32, body interface{}) (res interface{}, err error) {
	var ec utils.TErrCode
	res, ec = app.Discovery().SyncRequestNode(a.meta.Node, a.meta.NodeId, a.meta.ServiceId, 0, a.id, code, body)
	if ec.IsNotNil() {
		err = ec.Error()
	}
	return
}
func (a *AiActor) GetWarnRecentPlayerDistance() (playerId int64, distance float32, has bool) {
	if a.WarnActor().Count() == 0 {
		return
	}
	distance = -1
	curPosition := a.Position()
	a.WarnActor().IterCb(func(key string, v interface{}) {
		actor := v.(*interf.WarnActor)
		if actor.Type == common.ActorPlayer {
			vec3Distance := utils.Vec3Distance(curPosition, actor.Position)
			if distance == -1 || vec3Distance < distance {
				distance = vec3Distance
				playerId = actor.Id
			}
		}
	})
	has = true
	return
}

func (a *AiActor) GetWarnDistancePlayer(distance float32) (actors []*interf.WarnActor, has bool) {
	if a.WarnActor().Count() == 0 {
		return
	}
	curPosition := a.Position()
	actors = make([]*interf.WarnActor, 0, 3)
	a.WarnActor().IterCb(func(key string, v interface{}) {
		actor := v.(*interf.WarnActor)
		if actor.Type == common.ActorPlayer {
			vec3Distance := utils.Vec3Distance(curPosition, actor.Position)
			if vec3Distance <= distance {
				actors = append(actors, actor)
			}
		}
	})
	has = true
	return
}

func (a *AiActor) GetWarnDistanceHasPlayer(distance float32) (has bool) {
	if a.WarnActor().Count() == 0 {
		return
	}
	curPosition := a.Position()
	for tuple := range a.WarnActor().IterBuffered() {
		actor := tuple.Val.(*interf.WarnActor)
		if actor.Type == common.ActorPlayer {
			vec3Distance := utils.Vec3Distance(curPosition, actor.Position)
			if vec3Distance <= distance {
				has = true
				break
			}
		}
	}
	return
}

func (a *AiActor) RemoteClear() (err error) {
	_, err = a.RequestMetaNode(msg_spc.CdActorExitScene, &msg_spc.ReqActorExitScene{})
	if err != nil {
		return
	}
	_, err = a.RequestMetaNode(msg_spc.CdActorExitSpace, &msg_spc.ReqActorExitSpace{})
	return
}

func (a *AiActor) WarnDistance() float32 {
	return a.warnDistance
}
