package impl

import (
	"95eh.com/eg/app"
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"context"
	"errors"
	"fmt"
	esConfig "github.com/olivere/elastic/config"
	cmap "github.com/orcaman/concurrent-map"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"hm/pkg/ai/tool"
	"hm/pkg/game/common"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"math"
	"sync"
)

type ai struct {
	_onceTick           sync.Once
	load                interf.IAiLoad
	tick                interf.IAiTick
	conf                common.AiConfig
	esConf              esConfig.Config
	monsterManager      *tsv.MonsterTsvManager
	monsterPointManager *tsv.MonsterBornPointTsvManager
	ctx                 context.Context
	ctxCancelFn         func()
	controller          common.IBaseController
	// 死亡累计
	deathNum      map[interf.TAI]int32
}

func (a *ai) GetTick() interf.IAiTick {
	return a.tick
}

func (a *ai) GetContext() context.Context {
	return a.ctx
}

func (a *ai) IncDeathNum(typ interf.TAI) bool {
	if _,ok := a.deathNum[typ];!ok {
		a.deathNum[typ] = 1
	}else{
		a.deathNum[typ] += 1
	}
	return true
}

func (a *ai) GetDeathNum(typ interf.TAI) int32 {
	return a.deathNum[typ]
}

func (a *ai) Running() bool {
	if a.ctx == nil {
		return false
	}
	return a.ctx.Err() == nil
}

func (a *ai) Run() {
	if a.Running() {
		log.DLogWarn("ai run option canceled, because ai is running", nil)
		return
	}
	a.ctx, a.ctxCancelFn = context.WithCancel(context.Background())
	go a.tick.Start()
	log.DLogInfo("ai is running", nil)
	return
}

func (a *ai) Conf() *common.AiConfig {
	return &a.conf
}

func (a *ai) EsConf() *esConfig.Config {
	return &a.esConf
}

func (a *ai) NewAiActorByMonsterId(monsterTemplateId int32, bornPoint *utils.Vec3, scene misc.TScene, meta interf.ActorMetaInfo) (res interf.IAiActor, err error) {
	if bornPoint == nil {
		for _, pointTsv := range a.monsterPointManager.TsvSlice {
			if pointTsv.MonsterType == monsterTemplateId {
				bornPoint = &utils.Vec3{
					X: pointTsv.PosX,
					Y: 100,
					Z: pointTsv.PosY,
				}
				break
			}
		}
		if bornPoint == nil {
			err = errors.New(fmt.Sprintf("not find monster born position: %d", monsterTemplateId))
		}
	}
	monsterTsv, has := a.monsterManager.TsvMap[monsterTemplateId]
	if !has {
		err = errors.New(fmt.Sprintf("monster id not exits: %d", monsterTemplateId))
		return
	}
	actor := &AiActor{
		ai:           a,
		bornPoint:    *bornPoint,
		id:           utils.GenSnowflakeGlobalNodeId(),
		meta:         meta,
		position:     *bornPoint,
		warnActor:    cmap.New(),
		warnDistance: float32(monsterTsv.WarnRadius),
	}
	actor.Init()
	monsterType := interf.TAI(math.Floor(float64(monsterTemplateId)/1000) * 1000)
	treeInterface, exits := a.load.GetGlobalAiBehaviorTrees(monsterType)
	if !exits {
		err = errors.New(fmt.Sprintf("monster id behavior tree not exits: %d", monsterTemplateId))
		return
	}
	trees := treeInterface.(*cores.BehaviorTree)
	actor.scene = scene
	actor.tree = trees
	actor.template = monsterTsv
	actor.typ = monsterType
	res = actor
	return
}

func (a *ai) InitSceneMonsters(meta interf.ActorMetaInfo,tplId int32) (actors []interf.IAiActor, actorIds []int64, err error) {
	sceneType, err := tool.MatchSceneByRequestNodeId(meta.Node)
	if err != nil {
		return
	}
	actors = make([]interf.IAiActor, 0, 10)
	actorIds = make([]int64, 0, 10)
	for _, point := range a.monsterPointManager.TsvSlice {
		if point.Type == int32(sceneType) {
			if (tplId >0 && point.MonsterType == tplId) || tplId <= 0 {
				var actorItem interf.IAiActor
				actorItem, err = a.NewAiActorByMonsterId(point.MonsterType, &utils.Vec3{
					//X: point.PosX,
					//Y: 15,
					//Z: point.PosY,
					X: 101,
					Y: 0,
					Z: 101,
				}, sceneType, meta)
				if err == nil {
					actors = append(actors, actorItem)
					actorIds = append(actorIds, actorItem.Id())
					break //todo 临时一个
				}
			}
		}
	}
	if len(actors) > 0 {
		err = a.GetTick().PushAll(false, actors...)
	}
	return
}

func (a *ai) Stop(clearActors bool) {
	a.ctxCancelFn()
	if clearActors {
		a.tick.RemoveAll()
	}
}

func (a *ai) Reload(path string) {
	//todo send to space service clear all monster
	a.Stop(false)
	//record current all monster space service id
	//out the `ActorMetaInfo` from one of them
	metas := a.tick.GetSpaceSidGroupMetas()
	a.tick.RemoveAll()
	a.load.LoadCfgBehaviorTree(a.Conf(), path)
	//todo 初始化原场景的野怪
	if len(metas) > 0 {
		for _, meta := range metas {
			m := meta
			if _, _, err := a.InitSceneMonsters(m,0); err != nil {
				app.Log().Error("failed the init scene monsters", utils.M{
					"meta": m,
				})
			}
			//todo send
		}
	}
	a.Run()
}

func (a *ai) CodecBind(discovery intfc.IMDiscovery) {
	for c, action := range a.registerCodecs() {
		ac := action
		code := c
		discovery.BindRequestHandler(code, func(service uint16, v1, v2 int64, body interface{}) (interface{}, utils.TErrCode) {
			return ac(interf.NewActorActionCtx(interf.ActorMetaInfo{ServiceId: service}, v1, v2), body)
		})
	}
	for c, action := range a.registerEvents() {
		ac := action
		code := c
		discovery.BindEventHandler(code, func(service uint16, v int64, body interface{}) {
			ac(interf.NewActorActionCtx(interf.ActorMetaInfo{ServiceId: service}, v, 0), body)
		})
	}
}

func (a *ai) Space() common.ISceneProxy {
	return a.controller.Space()
}

func (a *ai) Controller() common.IBaseController {
	return a.controller
}

func NewAi(conf *common.Config, load interf.IAiLoad) interf.IAi {
	instance := &ai{
		conf:                conf.Ai,
		esConf:              *conf.Es,
		load:                load,
		monsterManager:      tsv.GetTsvManager(tsv.Monster).(*tsv.MonsterTsvManager),
		monsterPointManager: tsv.GetTsvManager(tsv.MonsterBornPoint).(*tsv.MonsterBornPointTsvManager),
		controller:          common.NewBaseController(),
	}
	load.LoadCfgBehaviorTree(instance.Conf())
	instance._onceTick.Do(func() {
		instance.tick = NewAiTick(instance)
	})
	return instance
}
