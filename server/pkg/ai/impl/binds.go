package impl

import (
	"95eh.com/eg/utils"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"hm/pkg/ai/tool"
	"hm/pkg/services/space/msg_spc"
)

func (a *ai) registerCodecs() map[msg_spc.MsgCode]interf.AiCodecAction {
	return map[msg_spc.MsgCode]interf.AiCodecAction{
		msg_spc.CdAiMonsterCreate: func(ctx interf.ActorActionCtx, body interface{}) (res interface{}, errCode utils.TErrCode) {
			req := body.(*msg_spc.ReqAiMonsterCreate)
			scene, err := tool.MatchSceneByRequestNodeId(ctx.Meta().Node)
			if err != nil {
				log.DLogError("failed the ai monster create", utils.M{
					"template": req.TemplateId,
					"err":      err,
					"meta":     ctx.Meta(),
				})
				return nil, ErrNodeRequestFailed
			}
			var bornPos *utils.Vec3
			if !tool.IsEmptyPositionVec3(&req.Position) {
				bornPos = &req.Position
			}
			monster, err := a.NewAiActorByMonsterId(req.TemplateId, bornPos, scene, ctx.Meta())
			if err != nil {
				log.DLogError("failed the ai monster create", utils.M{
					"template": req.TemplateId,
					"err":      err,
					"meta":     ctx.Meta(),
				})
				return nil, ErrNodeRequestFailed
			}
			return &msg_spc.ResAiMonsterCreate{MonsterId: monster.Id()}, utils.ErrCodeNil
		},
		msg_spc.CdAiSceneMonsterInit: func(ctx interf.ActorActionCtx, body interface{}) (res interface{}, errCode utils.TErrCode) {
			_, actorIds, err := a.InitSceneMonsters(ctx.Meta(),0)
			if err != nil {
				log.DLogError("failed the ai monster init", utils.M{
					"err":  err,
					"meta": ctx.Meta(),
				})
				return nil, ErrNodeRequestFailed
			}
			return &msg_spc.ResAiSceneMonsterInit{ActorIds: actorIds}, utils.ErrCodeNil
		},
		msg_spc.CdAiSceneMonsterClearAll: func(ctx interf.ActorActionCtx, body interface{}) (res interface{}, errCode utils.TErrCode) {
			a.tick.ClearAllBySpaceSid(ctx.Meta().ServiceId)
			return &msg_spc.ResAiSceneMonsterClearAll{}, utils.ErrCodeNil
		},
		msg_spc.CdAiMonsterRemove: func(ctx interf.ActorActionCtx, body interface{}) (res interface{}, errCode utils.TErrCode) {
			req := body.(*msg_spc.ReqAiMonsterRemove)
			result := a.tick.Remove(req.MonsterId)
			if !result {
				return nil, ErrNodeActorRemoveFailed
			}
			return &msg_spc.ResAiMonsterRemove{}, utils.ErrCodeNil
		},
	}
}
