package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// SetUpSimilarAttackersAround 设置周围同类攻击者
type SetUpSimilarAttackersAround struct {
	cores.Action `default:"" note:"设置周围同类攻击者，同步距离：<distance>m，同步名称：<name>"`
	name         string  `default:"" note:"同步名称"`
	distance     float32 `default:"6" note:"距离范围（m）"`
}

func (s *SetUpSimilarAttackersAround) Init(cfg *inc.BTNodeConfig) {
	s.Action.Init(cfg)
	s.name = cfg.GetString("name")
	s.distance = cfg.GetFloat32("distance")
}

func (s *SetUpSimilarAttackersAround) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
