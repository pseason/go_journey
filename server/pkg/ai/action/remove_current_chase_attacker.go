package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	aiLog "hm/pkg/ai/log"
)

// RemoveCurrentChaseAttacker 移除当前追击玩家
type RemoveCurrentChaseAttacker struct {
	cores.Action `default:"" note:"移除当前追击玩家"`
}

func (r *RemoveCurrentChaseAttacker) Init(cfg *inc.BTNodeConfig) {
	r.Action.Init(cfg)
}

func (r *RemoveCurrentChaseAttacker) OnTick(tick *cores.Tick) inc.BtStatus {
	aiLog.AiDebugLog(tick, "移除当前追击玩家")
	tick.Blackboard().RemoveGlobal(interf.CurrentChasingAttacker)
	return inc.FAILURE
}
