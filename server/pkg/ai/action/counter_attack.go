package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type CounterAttack struct {
	cores.Action `default:"" note:"反击(<skillId>)技能"`
	skillId      int32 `default:"0" note:"技能ID, 为0时按顺序使用技能槽位，普攻为优先级最低"`
}

func (c *CounterAttack) Init(cfg *inc.BTNodeConfig) {
	c.Action.Init(cfg)
	c.skillId = cfg.GetInt32("skillId")
}

func (c *CounterAttack) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
