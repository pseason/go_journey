package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type BloodBack struct {
	cores.Action `note:"回血至<amount>%"`
	amount       float32 `default:"1" note:"血量百分比值"`
}

func (b *BloodBack) Init(cfg *inc.BTNodeConfig) {
	b.Action.Init(cfg)
	b.amount = cfg.GetFloat32("amount") / 100
}

func (b *BloodBack) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
