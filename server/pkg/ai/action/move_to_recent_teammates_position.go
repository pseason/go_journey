package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type MoveToRecentTeammatesPosition struct {
	cores.Action     `default:"" note:"移动至最近的队友身边<distance>m，速度<speed>m/s，最大范围：<maxMd>m，最小范围：<minMd>m"`
	distance         float32 `default:"3" note:"移动到队友身边N米内"`
	stopCleanStates  string  `default:"" note:"结束时清除状态，每个之间用“,“分割"`
	startCleanStates string  `default:"" note:"开始时清除状态，每个之间用“,“分割"`
	speed            float32 `default:"3" note:"速度"`
	minMd            float32 `default:"3" note:"最小范围移动距离"`
	maxMd            float32 `default:"1" note:"最大范围移动距离"`
}

func (m *MoveToRecentTeammatesPosition) Init(cfg *inc.BTNodeConfig) {
	m.Action.Init(cfg)
	m.distance = cfg.GetFloat32("distance")
	m.minMd = cfg.GetFloat32("minMd")
	m.maxMd = cfg.GetFloat32("maxMd")
	m.speed = cfg.GetFloat32("speed")
	m.stopCleanStates = cfg.GetString("stopCleanStates")
	m.startCleanStates = cfg.GetString("startCleanStates")
}

func (m *MoveToRecentTeammatesPosition) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
