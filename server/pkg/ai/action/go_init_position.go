package action

import (
	"95eh.com/eg/utils"
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	aiLog "hm/pkg/ai/log"
	"hm/pkg/ai/tool"
)

type GoInitPosition struct {
	cores.Action    `default:"" note:"回到初始半径(<distance>m)位置，速度<speed>m/s，清除：<stopCleanStates>"`
	distance        float32 `default:"2" note:"距离半径"`
	stopCleanStates string  `default:"" note:"清除状态，每个之间用“,“分割"`
	speed           float32 `default:"4" note:"速度"`
	checkWarn       bool    `default:"false" note:"途中判断是否有玩家，含有则停止"`
}

func (g *GoInitPosition) Init(cfg *inc.BTNodeConfig) {
	g.Action.Init(cfg)
	g.distance = cfg.GetFloat32("distance")
	g.speed = cfg.GetFloat32("speed")
	g.stopCleanStates = cfg.GetString("stopCleanStates")
	g.checkWarn = cfg.GetBool("checkWarn")
}

func (g *GoInitPosition) OnTick(tick *cores.Tick) inc.BtStatus {
	actor := tool.GetTickIActor(tick)
	bornPosition := actor.GetBornPoint()
	curPosition := actor.Position()
	curDistance := utils.Vec3Distance(bornPosition, curPosition)
	if curDistance <= g.distance {
		return g.stop(tick, actor, "已回到初始半径范围内，停止移动")
	} else {
		if g.checkWarn {
			has := actor.GetWarnDistanceHasPlayer(actor.WarnDistance())
			if has {
				return g.stop(tick, actor, "检测到警戒范围内有玩家，停止移动")
			}
		}
		if !tick.Blackboard().GetGlobalBool(interf.ReturnState) {
			err := actor.Move(g.speed, &bornPosition, &curPosition)
			if err != nil {
				return inc.FAILURE
			}
			tick.Blackboard().SetGlobal(interf.ReturnState, true)
		}
		aiLog.AiDebugLog(tick, "回到初始半径范围内移动")
		return inc.RUNNING
	}
}
func (g *GoInitPosition) stop(tick *cores.Tick, actor interf.IAiActor, s string) inc.BtStatus {
	err := actor.StopMove()
	if err != nil {
		return inc.FAILURE
	}
	tick.Blackboard().SetGlobal(interf.ReturnState, false)
	aiLog.AiDebugLog(tick, s)
	tool.ClearGlobalBlackboardState(tick, g.stopCleanStates)
	return inc.SUCCESS
}
