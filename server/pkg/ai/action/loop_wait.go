package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/misc/utils"
)

type LoopWait struct {
	cores.Action `default:"" note:"循环等待<milliseconds>ms，最大次数<count>"`
	milliseconds int64 `default:"2000" note:"等待毫秒"`
	count        int32 `default:"1" note:"等待次数"`
}

func (w *LoopWait) Init(cfg *inc.BTNodeConfig) {
	w.Action.Init(cfg)
	w.milliseconds = cfg.GetInt64("milliseconds", 0)
	w.count = cfg.GetInt32("count", 0)
}

func (w *LoopWait) OnTick(tick *cores.Tick) inc.BtStatus {
	var c int32
	if w.count > 0 {
		c = tick.Blackboard().GetInt32("count", tick.Tree().Id(), w.Id())
		if c >= w.count {
			return inc.FAILURE
		}
	}
	curTime := utils.GetCurrentMillisecond()
	startTime := tick.Blackboard().GetInt64("startTime", tick.Tree().Id(), w.Id())
	if startTime == 0 {
		tick.Blackboard().Set("startTime", curTime, tick.Tree().Id(), w.Id())
	} else {
		if curTime-startTime >= w.milliseconds {
			if w.count > 0 {
				tick.Blackboard().Set("count", c+1, tick.Tree().Id(), w.Id())
			}
			tick.Blackboard().Remove("startTime", tick.Tree().Id(), w.Id())
			return inc.SUCCESS
		}
	}
	return inc.FAILURE
}
