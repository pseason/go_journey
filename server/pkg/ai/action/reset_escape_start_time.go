package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	aiLog "hm/pkg/ai/log"
	"hm/pkg/misc/utils"
)

// ResetEscapeStartTime 重置逃跑开始时间
type ResetEscapeStartTime struct {
	cores.Action `default:"" note:"重置逃跑开始时间"`
}

func (r *ResetEscapeStartTime) Init(cfg *inc.BTNodeConfig) {
	r.Action.Init(cfg)
}

func (r *ResetEscapeStartTime) OnTick(tick *cores.Tick) inc.BtStatus {
	second := utils.GetCurrentSecond()
	aiLog.AiDebugLog(tick, "重置开始逃跑时间", second)
	tick.Blackboard().SetGlobal(interf.EscapeStartTime, second)
	return inc.FAILURE
}
