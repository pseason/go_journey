package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// FindAttacker 寻找攻击玩家
type FindAttacker struct {
	cores.Action `default:"" note:"寻找攻击玩家（匹配最近，反之为仇恨最大：<recent>）"`
	recent       bool `default:"true" note:"如果为true则匹配最近攻击的玩家，false则选择仇恨最大的玩家"`
}

func (f *FindAttacker) Init(cfg *inc.BTNodeConfig) {
	f.Action.Init(cfg)
	f.recent = cfg.GetBool("recent")
}

func (f *FindAttacker) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
