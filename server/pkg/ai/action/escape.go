package action

import (
	utils2 "95eh.com/eg/utils"
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"hm/pkg/ai/tool"
	"hm/pkg/misc/utils"
	"math"
	"strconv"
)

// Escape 逃跑<time>s至<distance>m外(<speed>m/s)
type Escape struct {
	cores.Action `default:"" note:"逃跑<time>s至<distance>m外(<speed>m/s)，设置为运行中：<setRunning>"`
	distance     float32 `default:"6" note:"逃跑距离"`
	speed        float32 `default:"4" note:"速度"`
	time         int     `default:"3" note:"时间，单位秒"`
	setRunning   bool    `default:"false" note:"设置运行中"`
}

func (e *Escape) Init(cfg *inc.BTNodeConfig) {
	e.Action.Init(cfg)
	e.distance = cfg.GetFloat32("distance")
	e.speed = cfg.GetFloat32("speed")
	e.time = cfg.GetInt("time")
	e.setRunning = cfg.GetBool("setRunning")
}

func (e *Escape) OnTick(tick *cores.Tick) inc.BtStatus {
	bb := tick.Blackboard()
	actor := tool.GetTickIActor(tick)
	if e.time > 0 {
		// 检测移动时间
		escapeStartTime := tick.Blackboard().GetGlobalInt64(interf.EscapeStartTime)
		curTime := utils.GetCurrentSecond()
		interval := int(curTime - escapeStartTime)
		if escapeStartTime != 0 && interval >= e.time {
			// 停止移动
			log.AiDebugLog(tick, "逃跑停止移动", log.AiLogM{
				"当前逃跑时间": interval,
				"设定时间":   e.time,
			})
			err := actor.StopMove()
			if err != nil {
				return inc.FAILURE
			}
			e.stopMove(tick, bb)
			return inc.SUCCESS
		}
		speed := float32(e.speed) / 100
		speedAbs := math.Abs(float64(speed) - float64(e.speed))
		if speedAbs >= 0.1 {
			log.AiDebugLog(tick, "逃跑开始移动", log.AiLogM{
				"设定时间": e.time,
			})
			//todo send random direction move
			bb.SetGlobal(interf.EscapeStartTime, curTime)
		}
	} else {
		// 检测移动距离
		escapeStartVec3Interface := tick.Blackboard().GetGlobal(interf.EscapeStartVec3)
		var escapeStartVec3 *utils2.Vec3
		var hasEsv bool
		if nil != escapeStartVec3Interface {
			escapeStartVec3 = escapeStartVec3Interface.(*utils2.Vec3)
			hasEsv = true
		} else {
			// 获取对方的位置，若有攻击者则用，否则用现在的位置
			attackerId := tick.Blackboard().GetGlobalInt64(interf.CurrentChasingAttacker)
			if attackerId > 0 {
				iAttacker, ok := actor.WarnActor().Get(strconv.FormatInt(attackerId, 10))
				if ok {
					attacker := iAttacker.(*interf.WarnActor)
					escapeStartVec3 = &attacker.Position
				} else {
					_ = actor.StopMove()
					e.stopMove(tick, bb)
					return inc.FAILURE
				}
			}
			if nil == escapeStartVec3 {
				tp := actor.Position()
				escapeStartVec3 = &tp
			}
		}
		curVec3 := actor.Position()
		if escapeStartVec3 != nil && hasEsv {
			curDistance := utils2.Vec3Distance(curVec3, *escapeStartVec3)
			if curDistance >= e.distance {
				// 停止移动
				log.AiDebugLog(tick, "逃跑停止移动", log.AiLogM{
					"当前距离": curDistance,
					"设定时间": e.time,
				})
				err := actor.StopMove()
				if err != nil {
					return inc.FAILURE
				}
				e.stopMove(tick, bb)
				return inc.SUCCESS
			}
		}
		//speed := float32(e.speed)
		if e.speed != 0 && !hasEsv {
			log.AiDebugLog(tick, "逃跑开始移动", log.AiLogM{
				"设定距离": e.distance,
			})
			err := actor.Move(e.speed, &curVec3, &curVec3)
			if err != nil {
				log.AiLog(log.AILogError, tick, "逃跑开始移动失败", log.AiLogM{
					"设定距离": e.distance,
				})
				return inc.FAILURE
			}
			tick.Blackboard().SetGlobal(interf.EscapeStartVec3, escapeStartVec3)
		}
	}
	if e.setRunning {
		return inc.RUNNING
	}
	return inc.SUCCESS
}

func (e *Escape) stopMove(tick *cores.Tick, bb *cores.Blackboard) {
	tick.Blackboard().RemoveGlobal(interf.EscapeStartVec3)
	bb.RemoveGlobal(interf.CurrentChasingAttacker)
	tick.Blackboard().RemoveGlobal(interf.EscapeStart)
}
