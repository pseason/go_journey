package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
)

type SummonSkeletons struct {
	cores.Action `note:"召唤<num>只<highId>,是否需要跟随<isFlow>,默认false"`
	num       int32 `default:"1" note:"怪物数量"`
	highId    int32 `default:"2001" note:"怪物类型"`
	isFlow    bool   `default:"false" note:"是否需要跟随"`
}

func (s *SummonSkeletons) Init(cfg *inc.BTNodeConfig) {
	s.Action.Init(cfg)
	s.num = cfg.GetInt32("num")
	s.highId = cfg.GetInt32("highId")
	s.isFlow = cfg.GetBool("isFlow")
}

func (s *SummonSkeletons) OnTick(tick *cores.Tick) inc.BtStatus {
	highId := tick.Blackboard().GetGlobal(interf.AddHighAiByLowAiDeath).(int32)
	if  highId<= 0{
		tick.Blackboard().SetGlobal(interf.AddHighAiByLowAiDeath, s.highId)
	}
	//是否跟随-场景狼王召唤小狼跟随
	if s.isFlow{

	}
	return inc.FAILURE
}