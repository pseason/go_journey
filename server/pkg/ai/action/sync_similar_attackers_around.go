package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type SyncUpSimilarAttackersAround struct {
	cores.Action `default:"" note:"同步设置周围同类攻击者，若存在设置状态布尔：<stateBool>"`
	stateBool    string `default:"" note:"如果存在攻击者，设置状态布尔，每个之间,分隔"`
}

func (s *SyncUpSimilarAttackersAround) Init(cfg *inc.BTNodeConfig) {
	s.Action.Init(cfg)
	s.stateBool = cfg.GetString("stateBool")
}

func (s *SyncUpSimilarAttackersAround) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
