package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	aiLog "hm/pkg/ai/log"
)

// SetStateBool 设置布尔值状态
type SetStateBool struct {
	cores.Action `default:"" note:"设置布尔值状态：<name> = <val>"`
	name         string `default:"" note:"状态名称"`
	val          bool   `default:"false" note:"是否启用"`
}

func (s *SetStateBool) Init(cfg *inc.BTNodeConfig) {
	s.Action.Init(cfg)
	s.name = cfg.GetString("name")
	s.val = cfg.GetBool("val")
}

func (s *SetStateBool) OnTick(tick *cores.Tick) inc.BtStatus {
	aiLog.AiDebugLog(tick, "设置状态布尔", s.name, s.val)
	tick.Blackboard().SetGlobal(s.name, s.val)
	return inc.SUCCESS
}
