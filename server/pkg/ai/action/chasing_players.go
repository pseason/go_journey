package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
)

type ChasingPlayers struct {
	cores.Action      `default:"" note:"追击玩家(<speed>m/s)，使用(<skillId>)技能在<attackRange>m内"`
	speed             float32 `default:"3" note:"速度（m/s），为0时使用内置速度"`
	skillId           int32   `default:"0" note:"技能ID，为0时按顺序使用技能槽位，普攻为优先级最低"`
	attackRange       float32 `default:"2" note:"攻击范围"`
	checkHpStop       bool    `default:"true" note:"是否检测攻击者血量，若已耗尽则停止追击"`
	attackHpOverClean bool    `default:"true" note:"如果攻击者血量耗尽，则清除攻击者"`
}

func (c *ChasingPlayers) Init(cfg *inc.BTNodeConfig) {
	c.Action.Init(cfg)
	c.speed = cfg.GetFloat32("speed")
	c.skillId = cfg.GetInt32("skillId")
	c.attackRange = cfg.GetFloat32("attackRange")
	c.checkHpStop = cfg.GetBool("checkHpStop", true)
	c.attackHpOverClean = cfg.GetBool("attackHpOverClean", true)
}

func (c *ChasingPlayers) OnTick(tick *cores.Tick) inc.BtStatus {
	attackCid := tick.Blackboard().GetGlobalInt64(interf.CurrentChasingAttacker)
	if attackCid == 0 {
		return inc.FAILURE
	}
	//actor := tool.GetTickActor(tick)
	return inc.FAILURE
}
