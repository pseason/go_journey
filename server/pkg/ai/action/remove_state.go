package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/log"
)

// RemoveState 移除状态值
type RemoveState struct {
	cores.Action `default:"" note:"移除状态值：<name>"`
	name         string `default:"" note:"状态值名称"`
}

func (r *RemoveState) Init(cfg *inc.BTNodeConfig) {
	r.Action.Init(cfg)
	r.name = cfg.GetString("name")
}

func (r *RemoveState) OnTick(tick *cores.Tick) inc.BtStatus {
	log.AiDebugLog(tick, "移除状态值", r.name)
	tick.Blackboard().RemoveGlobal(r.name)
	return inc.SUCCESS
}
