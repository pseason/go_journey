package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	aiLog "hm/pkg/ai/log"
)

// PatrolStop 巡逻停止
type PatrolStop struct {
	cores.Action `default:"" note:"巡逻停止"`
}

func (p *PatrolStop) Init(cfg *inc.BTNodeConfig) {
	p.Action.Init(cfg)
}

func (p *PatrolStop) OnTick(tick *cores.Tick) inc.BtStatus {
	aiLog.AiDebugLog(tick, "巡逻停止")
	tick.Blackboard().RemoveGlobal(interf.OnPatrol)
	return inc.SUCCESS
}
