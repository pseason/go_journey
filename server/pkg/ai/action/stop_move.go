package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	aiLog "hm/pkg/ai/log"
	"hm/pkg/ai/tool"
)

// StopMove 停止移动
type StopMove struct {
	cores.Action `default:"" note:"停止移动"`
}

func (s *StopMove) Init(cfg *inc.BTNodeConfig) {
	s.Action.Init(cfg)
}

func (s *StopMove) OnTick(tick *cores.Tick) inc.BtStatus {
	err := tool.GetTickIActor(tick).StopMove()
	if err != nil {
		aiLog.AiDebugLog(tick, "停止移动失败", err)
		return inc.FAILURE
	}
	aiLog.AiDebugLog(tick, "停止移动成功")
	return inc.SUCCESS
}
