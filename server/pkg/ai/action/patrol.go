package action

import (
	"95eh.com/eg/utils"
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	aiLog "hm/pkg/ai/log"
	"hm/pkg/ai/tool"
	utils2 "hm/pkg/misc/utils"
	rand2 "math/rand"
)

// Patrol 巡逻
type Patrol struct {
	cores.Action `default:"" note:"巡逻<speed>m/s，间隔<interval>ms，范围：<scope>m"`
	speed        float32 `default:"3" note:"速度"`
	interval     int64   `default:"2000" note:"多少毫秒巡逻一次"`
	continued    int64   `default:"" note:""`
	scope        float32 `default:"6" note:"巡逻范围"`
}

func (p *Patrol) Init(cfg *inc.BTNodeConfig) {
	p.Action.Init(cfg)
	p.speed = cfg.GetFloat32("speed")
	p.interval = cfg.GetInt64("interval")
	p.continued = cfg.GetInt64("continued")
	p.scope = cfg.GetFloat32("scope", 6)
}

func (p *Patrol) OnTick(tick *cores.Tick) inc.BtStatus {
	lastStartPatrolTime := tick.Blackboard().GetGlobalInt64(interf.LastStartPatrolTime)
	curTime := utils2.GetCurrentMillisecond()
	rand := rand2.Int63n(10) * 1000
	if lastStartPatrolTime == 0 || (curTime-lastStartPatrolTime) >= (p.interval+rand) {
		actor := tool.GetTickIActor(tick)
		if actor.WarnActor().Count() > 0 {
			curVec3 := actor.Position()
			bronVec3 := actor.GetBornPoint()
			//视野内有玩家，进行巡逻
			curDistance := utils.Vec3Distance(bronVec3, actor.Position())
			//检测巡逻距离出生点的位置，如果超过指定位置，则往回方向移动
			var toVec3 *utils.Vec3
			if curDistance >= p.scope {
				toVec3 = &bronVec3
			}
			err := actor.Move(p.speed, toVec3, &curVec3)
			if err != nil {
				aiLog.AiDebugLog(tick, "野怪巡逻错误", err)
				return inc.FAILURE
			}
			tick.Blackboard().SetGlobal(interf.OnPatrol, true)
			tick.Blackboard().SetGlobal(interf.LastStartPatrolTime, curTime)
			aiLog.AiDebugLog(tick, "野怪巡逻开始")
		}
	} else {
		//正在巡逻中且持续时间大于等于设定值
		if (curTime - lastStartPatrolTime) >= p.continued {
			err := tool.GetTickIActor(tick).StopMove()
			if err != nil {
				aiLog.AiDebugLog(tick, "野怪巡逻停止失败", err)
				return inc.FAILURE
			}
			tick.Blackboard().RemoveGlobal(interf.OnPatrol)
			aiLog.AiDebugLog(tick, "野怪巡逻停止")
		}
	}
	return inc.SUCCESS
}
