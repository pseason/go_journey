package action

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/log"
)

// Log 日志
type Log struct {
	cores.Action `default:"" note:"日志：<content>"`
	content      string `default:"" note:"日志内容"`
}

func (l *Log) Init(cfg *inc.BTNodeConfig) {
	l.Action.Init(cfg)
	l.content = cfg.GetString("content")
}

func (l *Log) OnTick(tick *cores.Tick) inc.BtStatus {
	log.AiLog(log.AiLogInfo, tick, l.content)
	return inc.SUCCESS
}
