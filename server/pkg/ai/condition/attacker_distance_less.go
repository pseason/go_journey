package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// AttackerDistanceLess 攻击者距离小于等于(<ratio>%)
type AttackerDistanceLess struct {
	cores.Condition `default:"" note:"攻击者距离小于等于(<ratio>%)，不存在返回成功：<existence>，始终返回成功：<existenceAlways>"`
	distance        float32 `default:"6" note:"距离（米）"`
	existence       bool    `default:"false" note:"若玩家不存在，则返回success，适用于执行清除业务"`
	existenceAlways bool    `default:"false" note:"若不存在，始终返回success"`
}

func (a *AttackerDistanceLess) Init(cfg *inc.BTNodeConfig) {
	a.Condition.Init(cfg)
	a.distance = cfg.GetFloat32("distance")
	a.existence = cfg.GetBool("existence")
	a.existenceAlways = cfg.GetBool("existenceAlways")
}

func (a *AttackerDistanceLess) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
