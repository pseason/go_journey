package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// UnderActiveAttack 受到玩家主动攻击
type UnderActiveAttack struct {
	cores.Condition `default:"" note:"受到玩家主动攻击"`
	reChange        bool `default:"" note:""`
	hasReturnTrue   bool `default:"true" note:"含有攻击者返回true"`
}

func (u *UnderActiveAttack) Init(cfg *inc.BTNodeConfig) {
	u.Condition.Init(cfg)
	u.reChange = cfg.GetBool("reChange")
	u.hasReturnTrue = cfg.GetBool("hasReturnTrue", false)
}

func (u *UnderActiveAttack) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
