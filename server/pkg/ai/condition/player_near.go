package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	aiLog "hm/pkg/ai/log"
	"hm/pkg/ai/tool"
)

// PlayerNear 玩家靠近(<distance>m)
type PlayerNear struct {
	cores.Condition `default:"" note:"玩家靠近(<distance>m)，最小<minDistance>m，有则设为攻击者：<setAttacker>"`
	distance        float32 `default:"6" note:"距离（米）"`
	minDistance     float32 `default:"2" note:"最小距离（米）"`
	setAttacker     bool    `default:"true" note:"是否将检测到的玩家设置为攻击者"`
}

func (p *PlayerNear) Init(cfg *inc.BTNodeConfig) {
	p.Condition.Init(cfg)
	p.distance = cfg.GetFloat32("distance")
	p.minDistance = cfg.GetFloat32("minDistance")
	p.setAttacker = cfg.GetBool("setAttacker")
}

func (p *PlayerNear) OnTick(tick *cores.Tick) inc.BtStatus {
	actor := tool.GetTickIActor(tick)
	playerId, distance, has := actor.GetWarnRecentPlayerDistance()
	if has {
		if distance >= p.minDistance && distance <= p.distance {
			if p.setAttacker {
				tick.Blackboard().SetGlobal(interf.CurrentChasingAttacker, playerId)
			}
			aiLog.AiDebugLog(tick, "玩家靠近", p.distance, p.minDistance, p.setAttacker)
			return inc.SUCCESS
		}
	}
	return inc.FAILURE
}
