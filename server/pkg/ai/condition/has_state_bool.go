package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	aiLog "hm/pkg/ai/log"
)

// HasStateBool 含有状态布尔
type HasStateBool struct {
	cores.Condition `default:"" note:"含有状态布尔：<name>"`
	name            string `default:"" note:"状态布尔名称"`
}

func (h *HasStateBool) Init(cfg *inc.BTNodeConfig) {
	h.Condition.Init(cfg)
	h.name = cfg.GetString("name")
}

func (h *HasStateBool) OnTick(tick *cores.Tick) inc.BtStatus {
	b := tick.Blackboard().GetGlobalBool(h.name)
	if b {
		aiLog.AiDebugLog(tick, "含有状态布尔", h.name)
		return inc.SUCCESS
	}
	return inc.FAILURE
}
