package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
)

// OnPatrol 是否在巡逻中
type OnPatrol struct {
	cores.Condition `default:"" note:"是否在巡逻中"`
}

func (o *OnPatrol) Init(cfg *inc.BTNodeConfig) {
	o.Condition.Init(cfg)
}

func (o *OnPatrol) OnTick(tick *cores.Tick) inc.BtStatus {
	r := tick.Blackboard().GetGlobalBool(interf.OnPatrol)
	if r {
		return inc.SUCCESS
	}
	return inc.FAILURE
}
