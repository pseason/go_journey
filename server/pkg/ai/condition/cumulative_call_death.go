package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
)

type CumulativeCallDeath struct {
	cores.Condition `default:"" note:"累计死亡(<num>只)"`
	num           int32  `default:"50" note:"被击杀数值"`
}

func (c *CumulativeCallDeath) Init(cfg *inc.BTNodeConfig) {
	c.Condition.Init(cfg)
	c.num = cfg.GetInt32("num")
}

func (c *CumulativeCallDeath) OnTick(tick *cores.Tick) inc.BtStatus {
	num := tick.Blackboard().GetGlobal(interf.CumulativeCallDeathNum).(int32)
	if  num<= 0{
		tick.Blackboard().SetGlobal(interf.CumulativeCallDeathNum, c.num)
	}
	return inc.SUCCESS
}