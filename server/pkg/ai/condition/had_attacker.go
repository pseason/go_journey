package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// HadAttacker 是否含有攻击者
type HadAttacker struct {
	cores.Condition `default:"" note:"是否含有攻击者"`
}

func (h *HadAttacker) Init(cfg *inc.BTNodeConfig) {
	h.Condition.Init(cfg)
}

func (h *HadAttacker) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
