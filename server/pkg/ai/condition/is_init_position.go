package condition

import (
	"95eh.com/eg/utils"
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/tool"
)

// IsInitPosition 初始位置半径(<distance>m)内
type IsInitPosition struct {
	cores.Condition `default:"" note:"初始位置半径(<distance>m)内"`
	distance        float32 `default:"" note:"距离（米）"`
}

func (i *IsInitPosition) Init(cfg *inc.BTNodeConfig) {
	i.Condition.Init(cfg)
	i.distance = cfg.GetFloat32("distance")
}

func (i *IsInitPosition) OnTick(tick *cores.Tick) inc.BtStatus {
	actor := tool.GetTickIActor(tick)
	curPosition := actor.Position()
	bornPosition := actor.GetBornPoint()
	if utils.Vec3Distance(curPosition, bornPosition) <= i.distance {
		return inc.SUCCESS
	}
	return inc.FAILURE
}
