package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	aiLog "hm/pkg/ai/log"
	"hm/pkg/ai/tool"
	"hm/pkg/misc/utils"
)

// TimeDistanceNoPlayer (<time>)秒内(<distance>)m内无玩家
type TimeDistanceNoPlayer struct {
	cores.Condition `default:"" note:"(<time>)秒内(<distance>)m内无玩家"`
	time            int     `default:"10" note:"时间"`
	distance        float32 `default:"4" note:"距离（米）"`
}

func (t *TimeDistanceNoPlayer) Init(cfg *inc.BTNodeConfig) {
	t.Condition.Init(cfg)
	t.distance = cfg.GetFloat32("distance")
	t.time = cfg.GetInt("time")
}

func (t *TimeDistanceNoPlayer) OnTick(tick *cores.Tick) inc.BtStatus {
	actor := tool.GetTickIActor(tick)
	has := actor.GetWarnDistanceHasPlayer(t.distance)
	curTime := utils.GetCurrentSecond()
	lastNotHasActorTime := tick.Blackboard().GetGlobalInt64(interf.LastNotHasActorTime)
	if lastNotHasActorTime == 0 && !has {
		lastNotHasActorTime = curTime
		tick.Blackboard().SetGlobal(interf.LastNotHasActorTime, lastNotHasActorTime)
	} else if lastNotHasActorTime != 0 && has {
		tick.Blackboard().RemoveGlobal(interf.LastNotHasActorTime)
		lastNotHasActorTime = curTime
	}
	if lastNotHasActorTime > 0 {
		s := curTime - lastNotHasActorTime
		if s >= int64(t.time) {
			aiLog.AiDebugLog(tick, "周围时间距离范围内无玩家", t.time, t.distance)
			return inc.SUCCESS
		}
	}
	return inc.FAILURE
}
