package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/tool"
)

// PlayerNearDistanceCount (<distance>m)内有(<count>名)玩家
type PlayerNearDistanceCount struct {
	cores.Condition `default:"" note:"(<distance>m)内有(<count>名)玩家，清除状态：<cleanState>"`
	distance        float32 `default:"6" note:"距离（米）"`
	count           int     `default:"1" note:"玩家数量"`
	cleanState      string  `default:"" note:"清除的状态，每个状态之间用','进行分隔"`
}

func (p *PlayerNearDistanceCount) Init(cfg *inc.BTNodeConfig) {
	p.Condition.Init(cfg)
	p.distance = cfg.GetFloat32("distance")
	p.count = cfg.GetInt("count")
	p.cleanState = cfg.GetString("cleanState")
}

func (p *PlayerNearDistanceCount) OnTick(tick *cores.Tick) inc.BtStatus {
	actor := tool.GetTickIActor(tick)
	player, has := actor.GetWarnDistancePlayer(p.distance)
	if has && len(player) >= p.count {
		tool.ClearGlobalBlackboardState(tick, p.cleanState)
		return inc.SUCCESS
	}
	return inc.FAILURE
}
