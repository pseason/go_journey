package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// AttackerHpLess 攻击者血量少于等于(<ratio>%)
type AttackerHpLess struct {
	cores.Condition `default:"" note:"攻击者血量少于等于(<ratio>%)，不存在返回成功：<existence>"`
	ratio           float32 `default:"0" note:"血量百分比数值"`
	existence       bool    `default:"false" note:"若玩家不存在，则返回success，适用于执行清除业务"`
}

func (a *AttackerHpLess) Init(cfg *inc.BTNodeConfig) {
	a.Condition.Init(cfg)
	a.ratio = cfg.GetFloat32("ratio")
	a.existence = cfg.GetBool("existence")
}

func (a *AttackerHpLess) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
