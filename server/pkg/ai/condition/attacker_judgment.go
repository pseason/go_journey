package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// AttackerJudgment 追击者判断
type AttackerJudgment struct {
	cores.Condition `default:"" note:"追击者判断（<name>），时间间隔<time>ms，相等返回失败：<eqReturnFil>"`
	name            string `default:"" note:"判断值名称，具体类型如下：<br><ul><li>maxHunt</li></ul>"`
	time            int64  `default:"1000" note:""`
	set             bool   `default:"false" note:"若当前追击者不是仇恨玩家，则设置为当前的追击对象"`
	eqReturnFil     bool   `default:"false" note:"若相等返回fail"`
}

func (a *AttackerJudgment) Init(cfg *inc.BTNodeConfig) {
	a.Condition.Init(cfg)
	a.name = cfg.GetString("name")
	a.time = cfg.GetInt64("time")
	a.set = cfg.GetBool("set")                 //若当前追击者不是仇恨玩家，则设置为当前的追击对象
	a.eqReturnFil = cfg.GetBool("eqReturnFil") //若相等返回fail
}

func (a *AttackerJudgment) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
