package condition

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// HpLess 血量少于(<ratio>%)
type HpLess struct {
	cores.Condition `default:"" note:"血量少于(<ratio>%)，是否含有状态布尔：<hasBoolList>，含有则返回：<isHas>"`
	ratio           float32  `default:"20" note:"血量百分比数值"`
	hasBoolList     []string `default:"" note:"布尔状态标识"`
	isHas           bool     `default:"true" note:"如果含有填入的布尔状态，是返回true还是false"`
}

func (h *HpLess) Init(cfg *inc.BTNodeConfig) {
	h.Condition.Init(cfg)
	h.ratio = cfg.GetFloat32("ratio")
	h.isHas = cfg.GetBool("isHas")
}

func (h *HpLess) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
