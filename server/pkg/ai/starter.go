package ai

import (
	"95eh.com/eg/app"
	"95eh.com/eg/asset"
	"95eh.com/eg/codec"
	"95eh.com/eg/discovery"
	"95eh.com/eg/intfc"
	"95eh.com/eg/log"
	"95eh.com/eg/scene"
	"95eh.com/eg/timer"
	"95eh.com/eg/utils"
	"95eh.com/eg/worker"
	"fmt"
	"hm/pkg/ai/front"
	"hm/pkg/ai/impl"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/load"
	aiLog "hm/pkg/ai/log"
	"hm/pkg/game/common"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
	"hm/pkg/services/space/msg_spc"
	_ "net/http/pprof"
	"time"
)

func StarterAi() {
	//go func() {
	//	http.ListenAndServe("0.0.0.0:8080", nil)
	//}()
	// 加载配置文件
	conf, err := common.LoadConf("", "common", "common_dev", "region_ai", "region_ai_dev", "region", "region_dev")
	if err != nil {
		panic(err.Error())
	}
	//启动Consul
	err = utils.StartConsul(conf.Consul)
	if err != nil {
		panic(err.Error())
	}
	//启动Redis
	regionRedis := misc.GetRedisConn(conf.Redis)
	//设置响应超时时间
	if conf.Debug {
		dur := (time.Minute * 30).Milliseconds()
		intfc.ResponseTimeoutDur = dur
		intfc.TcpDeadlineDur = dur
	}

	//serviceCodec := codec.NewJsonCodec()
	// 与服务通信编解码器使用msgpack
	serviceCodec := codec.NewMsgPackCodec()

	disConf := conf.Discovery
	aiLogger := aiLog.NewAiLogger(intfc.TLogDebug, conf.Es)
	var ai interf.IAi
	app.Start(
		//添加日志记录
		log.NewMLogger(log.Loggers(aiLogger), nil, log.SetCallerSkip(4)),
		//资源模块
		asset.NewMAsset(),
		//计时模块
		timer.NewMTimer(regionRedis, nil),
		//协程模块
		worker.NewMWorker(),
		//服务发现模块
		discovery.NewMDiscovery(
			//节点类型
			[]intfc.TNode{misc.NodeAi},
			//节点id
			disConf.NodeId,
			//区服id
			disConf.RegionId,
			//编解码
			serviceCodec,
			regionRedis,
			//模块选项
			intfc.ModuleOptions(
				intfc.BeforeModuleStart(func() {
					// 注入协议号和消息结构体的映射
					msg_spc.InitCodecForAi(app.Discovery())
					msg_spc.InitCodecForSpace(app.Discovery())
					tsv.LoadNodeTsv(misc.NodeAi)
				}),
				//模块启动后
				intfc.AfterModuleStart(func() {
					aiLog.SetLogDebug(conf.Ai.Debug)
					aiLoad := load.NewAiLoad()
					ai = impl.NewAi(conf, aiLoad)
					ai.CodecBind(app.Discovery())
					ai.Run()
					front.StartFrontServer(ai)
				}),
				//模块启动后,监听登录节点
				intfc.AfterModuleStart(func() {
					app.Discovery().WatchNodes(misc.NodeServices_CitySpc)
				}),
				intfc.BeforeModuleDispose(func() {
					fmt.Println("bye")
				}),
			),
			//节点连接成功的回调
			discovery.NodeConnected(func(node intfc.TNode, regionId, nodeId uint16) {
				ai.OnNodeConnected(node, regionId, nodeId)
			}),
			discovery.NodeClosed(func(node intfc.TNode, regionId, nodeId uint16) {
				ai.OnNodeClosed(node, regionId, nodeId)
			}),
			discovery.Addr(disConf.Ip, disConf.Port),
			discovery.Weight(disConf.Weight)),
			scene.NewSceneCache(regionRedis,nil),
	)
	utils.BeforeExit("stop ai", app.Dispose)
}
