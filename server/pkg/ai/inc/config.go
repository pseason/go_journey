package inc

import (
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
)

type BTTreeConfig struct {
	ID          string                  `json:"id"`
	Title       string                  `json:"title"`
	Description string                  `json:"description"`
	Root        string                  `json:"root"`
	Properties  map[string]interface{}  `json:"properties"`
	Nodes       map[string]BTNodeConfig `json:"nodes"`
}

type BTNodeConfig struct {
	Id          string                 `json:"id"`
	Name        string                 `json:"name"`
	Title       string                 `json:"title"`
	Description string                 `json:"description"`
	Children    []string               `json:"children"`
	Properties  map[string]interface{} `json:"properties"`
	Child       string                 `json:"child"`
}

type BTProjectConfig struct {
	Trees []BTTreeConfig `json:"trees"`
}

func (b *BTNodeConfig) Get(name string) (float64, bool) {
	v, ok := b.Properties[name]
	if !ok {
		WarnLogNoStack("get property warning, no key: " + name)
		return 0, false
	}
	f64, ok := v.(float64)
	if !ok {
		WarnLogNoStack("get property warning, no key: " + name)
		return 0, false
	}
	return f64, true
}

func (b *BTNodeConfig) GetInt(name string, dv ...int) int {
	v, ok := b.Get(name)
	if ok {
		return int(v)
	}
	if len(dv) > 0 {
		return dv[0]
	}
	return 0
}

func (b *BTNodeConfig) GetInt32(name string, dv ...int32) int32 {
	v, ok := b.Get(name)
	if ok {
		return int32(v)
	}
	if len(dv) > 0 {
		return dv[0]
	}
	return 0
}

func (b *BTNodeConfig) GetInt64(name string, dv ...int64) int64 {
	v, ok := b.Get(name)
	if ok {
		return int64(v)
	}
	if len(dv) > 0 {
		return dv[0]
	}
	return 0
}

func (b *BTNodeConfig) GetFloat32(name string, dv ...float32) float32 {
	v, ok := b.Get(name)
	if ok {
		return float32(v)
	}
	if len(dv) > 0 {
		return dv[0]
	}
	return 0
}

func (b *BTNodeConfig) GetBool(name string, dv ...bool) bool {
	v, ok := b.Properties[name]
	f := func() bool {
		if len(dv) > 0 {
			return dv[0]
		}
		return false
	}
	if !ok {
		WarnLogNoStack("get property warning, no key: " + name)
		return f()
	}
	res, ok := v.(bool)
	if !ok {
		if str, yep := v.(string); yep {
			return str == "true"
		}
		WarnLogNoStack("get property warning, format to bool fail: " + name)
		return f()
	}
	return res
}

func (b *BTNodeConfig) GetString(name string, dv ...string) string {
	v, ok := b.Properties[name]
	f := func() string {
		if len(dv) > 0 {
			return dv[0]
		}
		return ""
	}
	if !ok {
		WarnLogNoStack("get property warning, no key: " + name)
		return f()
	}
	str, ok := v.(string)
	if !ok {
		WarnLogNoStack("get property warning, format to str fail: " + name)
		return f()
	}
	return str
}

func loadBtConfig(path string, r interface{}) (err error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}
	err = jsoniter.Unmarshal(data, r)
	return
}

func LoadTreeConfig(path string) (res *BTTreeConfig, err error) {
	var r BTTreeConfig
	err = loadBtConfig(path, &r)
	if err != nil {
		return
	}
	res = &r
	return
}

func LoadProjectConfig(path string) (res *BTProjectConfig, err error) {
	var r BTProjectConfig
	err = loadBtConfig(path, &r)
	if err != nil {
		return
	}
	res = &r
	return
}
