package actions

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type Failure struct {
	cores.Action
}

func (f *Failure) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.FAILURE
}
