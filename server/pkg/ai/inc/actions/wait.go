package actions

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	utils2 "hm/pkg/misc/utils"
)

type Wait struct {
	cores.Action
	milliseconds int64
}

func (w *Wait) Init(cfg *inc.BTNodeConfig) {
	w.Action.Init(cfg)
	w.milliseconds = cfg.GetInt64("milliseconds", 0)
}

func (w *Wait) OnOpen(tick *cores.Tick) {
	tick.Blackboard().Set("startTime", utils2.GetCurrentMillisecond(), tick.Tree().Id(), w.Id())
}

func (w *Wait) OnTick(tick *cores.Tick) inc.BtStatus {
	curTime := utils2.GetCurrentMillisecond()
	startTime := tick.Blackboard().GetInt64("startTime", tick.Tree().Id(), w.Id())
	if curTime-startTime > w.milliseconds {
		return inc.SUCCESS
	}
	return inc.RUNNING
}
