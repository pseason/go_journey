package actions

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type Success struct {
	cores.Action
}

func (s *Success) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.SUCCESS
}
