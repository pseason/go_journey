package actions

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type Runner struct {
	cores.Action
}

func (r *Runner) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.RUNNING
}
