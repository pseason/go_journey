package actions

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type Error struct {
	cores.Action
}

func (e *Error) OnTick(tick *cores.Tick) inc.BtStatus {
	return inc.ERROR
}
