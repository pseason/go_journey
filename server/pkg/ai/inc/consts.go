package inc

type BtStatus uint8

const (
	SUCCESS BtStatus = iota + 1
	FAILURE
	RUNNING
	ERROR
)

type BtCategory string

const (
	COMPOSITE BtCategory = "composite"
	ACTION    BtCategory = "action"
	CONDITION BtCategory = "condition"
	DECORATOR BtCategory = "decorator"
	EMPTY                = ""
)
