package inc

import (
	"fmt"
	"github.com/gookit/color"
	"runtime"
	"strconv"
	"strings"
)

func WarnLogNoStack(msg string, err ...interface{}) {
	fmt.Println(color.Yellow.Sprintf(msg, err...), GetRuntimeCaller(2))
}

func GetRuntimeCaller(skip int) string {
	_, file, line, ok := runtime.Caller(skip)
	if !ok {
		return ""
	}
	index := strings.Index(file, "/pkg/ai")
	if index != -1 {
		file = file[index:]
	}
	return file + ":" + strconv.Itoa(line)
}
