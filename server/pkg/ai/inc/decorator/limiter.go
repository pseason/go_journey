package decorator

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type Limiter struct {
	cores.Decorator
	maxLoop int
}

func (l *Limiter) Init(cfg *inc.BTNodeConfig) {
	l.Decorator.Init(cfg)
	l.maxLoop = cfg.GetInt("maxLoop", 0)
}

func (l *Limiter) OnTick(tick *cores.Tick) inc.BtStatus {
	child := l.Child()
	if nil == child {
		return inc.ERROR
	}
	count := tick.Blackboard().GetInt("count", tick.Tree().Id(), l.Id())
	if count < l.maxLoop {
		status := child.Execute(tick)
		if status == inc.SUCCESS || status == inc.FAILURE {
			tick.Blackboard().Set("count", count+1, tick.Tree().Id(), l.Id())
		}
		return status
	}
	return inc.FAILURE
}
