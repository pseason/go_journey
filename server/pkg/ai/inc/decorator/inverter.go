package decorator

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type Inverter struct {
	cores.Decorator
}

func (s *Inverter) OnTick(tick *cores.Tick) inc.BtStatus {
	child := s.Child()
	if nil == child {
		return inc.ERROR
	}
	status := child.Execute(tick)
	switch status {
	case inc.FAILURE:
		return inc.SUCCESS
	case inc.SUCCESS:
		return inc.FAILURE
	}
	return inc.SUCCESS
}
