package load

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/actions"
	"hm/pkg/ai/inc/composites"
	"hm/pkg/ai/inc/decorator"
)

func CreateBasicExtStructOptMaps() *inc.BtStructOptMaps {
	maps := inc.NewBtStructOptMaps()

	maps.Register("Error", &actions.Error{}).
		Register("Failer", &actions.Failure{}).
		Register("Runner", &actions.Runner{}).
		Register("Succeeder", &actions.Success{}).
		Register("Wait", &actions.Wait{})

	maps.Register("MemPriority", &composites.MemPriority{}).
		Register("MemSequence", &composites.MemSequence{}).
		Register("Priority", &composites.Priority{}).
		Register("Sequence", &composites.Sequence{})

	maps.Register("Inverter", &decorator.Inverter{}).
		Register("Limiter", &decorator.Limiter{})

	return maps
}
