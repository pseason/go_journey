package cores

import "hm/pkg/ai/inc"

type IDecorator interface {
	Child() IBaseNode
	SetChild(c IBaseNode)
}

type Decorator struct {
	BaseNodeWork
	child IBaseNode
}

func (d *Decorator) Create() {
	d.category = inc.DECORATOR
}

func (d *Decorator) Child() IBaseNode {
	return d.child
}

func (d *Decorator) SetChild(c IBaseNode) {
	d.child = c
}
