package cores

import (
	"95eh.com/eg/utils"
	"hm/pkg/ai/inc"
	"hm/pkg/ai/interf"
	"sync"
)

type BehaviorTree struct {
	id          int64
	title       string
	description string
	properties  map[string]interface{}
	tickPool    *sync.Pool
	root        IBaseNode
}

func NewBehaviorTree(config *inc.BTTreeConfig, basic *inc.BtStructOptMaps, ext *inc.BtStructOptMaps) *BehaviorTree {
	r := &BehaviorTree{}
	r.Init(config, basic, ext)
	return r
}

func (b *BehaviorTree) Id() int64 {
	return b.id
}

func (b *BehaviorTree) Title() string {
	return b.title
}

func (b *BehaviorTree) Description() string {
	return b.description
}

func (b *BehaviorTree) Properties() map[string]interface{} {
	return b.properties
}

func (b *BehaviorTree) Root() IBaseNode {
	return b.root
}

func (b *BehaviorTree) Init(config *inc.BTTreeConfig, basicExt *inc.BtStructOptMaps, customExt *inc.BtStructOptMaps) {
	b.id = utils.GenSnowflakeRegionNodeId()
	b.title = config.Title
	b.description = config.Description
	b.tickPool = &sync.Pool{New: func() interface{} {
		return NewTick()
	}}

	nodes := make(map[string]IBaseNode)

	for k := range config.Nodes {
		nodeCfg := config.Nodes[k]
		var node IBaseNode
		if nil != customExt && customExt.Has(nodeCfg.Name) {
			res, err := customExt.New(nodeCfg.Name)
			if err == nil {
				node = res.(IBaseNode)
			}
		} else {
			res, err := basicExt.New(nodeCfg.Name)
			if err == nil {
				node = res.(IBaseNode)
			}
		}
		if nil == node {
			panic("bt struct does not exist: " + nodeCfg.Name)
		}
		node.Create()
		node.Init(&nodeCfg)
		pub, ok := node.(IBaseNodePublic)
		if !ok {
			panic("bt struct is not IBaseNodePublic: " + nodeCfg.Name)
		}
		node.SetBaseNodePublic(pub)
		nodes[k] = node
	}

	b.root = nodes[config.Root]

	for k := range config.Nodes {
		nodeCfg := config.Nodes[k]
		node := nodes[k]
		if nodeCfg.Children != nil && len(nodeCfg.Children) > 0 && node.Category() == inc.COMPOSITE {
			for _, ck := range nodeCfg.Children {
				composite := node.(IComposite)
				composite.AppendChildren(nodes[ck])
			}
		} else if nodeCfg.Child != inc.EMPTY && node.Category() == inc.DECORATOR {
			d := node.(IDecorator)
			d.SetChild(nodes[nodeCfg.Child])
		}
	}

}

func (b *BehaviorTree) Tick(target interf.IAiActor, blackboard *Blackboard) inc.BtStatus {
	t := b.tickPool.Get().(*Tick)
	t.target = target
	t.blackboard = blackboard
	t.tree = b
	t.actorId = target.Id()

	s := b.root.Execute(t)

	t.Destruct()
	b.tickPool.Put(t)
	return s
}
