package cores

import (
	"github.com/gookit/color"
	"hm/pkg/ai/inc"
)

type IBaseNodeInner interface {
	_execute(tick *Tick) inc.BtStatus
	_enter(tick *Tick)
	_open(tick *Tick)
	_tick(tick *Tick) inc.BtStatus
	_close(tick *Tick)
	_exit(tick *Tick)
}

type IBaseNodePublic interface {
	OnEnter(tick *Tick)
	OnOpen(tick *Tick)
	OnTick(tick *Tick) inc.BtStatus
	OnClose(tick *Tick)
	OnExit(tick *Tick)
}

type IBaseNode interface {
	IBaseNodeInner
	Create()
	Init(cfg *inc.BTNodeConfig)
	Execute(tick *Tick) inc.BtStatus
	Category() inc.BtCategory
	SetBaseNodePublic(pub IBaseNodePublic)
}

type BaseNode struct {
	IBaseNodePublic
	id          string
	name        string
	description string
	properties  map[string]interface{}
	category    inc.BtCategory
}

type BaseNodeWork struct {
	BaseNode
	BaseNodePublic
}

func (b *BaseNode) Id() string {
	return b.id
}

func (b *BaseNode) Name() string {
	return b.name
}

func (b *BaseNode) Description() string {
	return b.description
}

func (b *BaseNode) Category() inc.BtCategory {
	return b.category
}

func (b *BaseNode) SetBaseNodePublic(pub IBaseNodePublic) {
	b.IBaseNodePublic = pub
}

func (b *BaseNode) Set() string {
	return b.id
}

func (b *BaseNode) _execute(tick *Tick) inc.BtStatus {
	b._enter(tick)

	if !tick.blackboard.GetBool("open", tick.tree.id, b.id) {
		b._open(tick)
	}

	status := b._tick(tick)

	if status != inc.RUNNING {
		b._close(tick)
	}

	b._exit(tick)

	return status
}

func (b *BaseNode) _enter(tick *Tick) {
	tick._enter(b)
	b.IBaseNodePublic.OnEnter(tick)
}

func (b *BaseNode) _open(tick *Tick) {
	tick._open(b)
	tick.blackboard.Set("open", true, tick.tree.id, b.id)
	b.OnOpen(tick)
}

func (b *BaseNode) _tick(tick *Tick) inc.BtStatus {
	tick._tick(b)
	return b.OnTick(tick)
}

func (b *BaseNode) _close(tick *Tick) {
	tick._close(b)
	tick.blackboard.Remove("open", tick.tree.id, b.id)
	b.OnClose(tick)
}

func (b *BaseNode) _exit(tick *Tick) {
	tick._exit(b)
	b.OnExit(tick)
}

func (b *BaseNode) Execute(tick *Tick) inc.BtStatus {
	return b._execute(tick)
}

func (b *BaseNode) Create() {

}

func (b *BaseNode) Init(cfg *inc.BTNodeConfig) {
	b.id = cfg.Id
	b.name = cfg.Title
	b.description = cfg.Description
	if cfg.Properties == nil {
		b.properties = make(map[string]interface{})
	} else {
		b.properties = cfg.Properties
	}
}

type BaseNodePublic struct {
	IBaseNodePublic
}

func (b *BaseNodePublic) OnEnter(tick *Tick) {

}

func (b *BaseNodePublic) OnOpen(tick *Tick) {

}

func (b *BaseNodePublic) OnTick(tick *Tick) inc.BtStatus {
	color.BgRed.Sprintf("tick no implement: %s", tick.tree.title)
	return inc.ERROR
}

func (b *BaseNodePublic) OnClose(tick *Tick) {

}

func (b *BaseNodePublic) OnExit(tick *Tick) {

}
