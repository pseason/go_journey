package cores

type ITick interface {
	_enter(b *BaseNode)
	_open(b *BaseNode)
	_tick(b *BaseNode)
	_close(b *BaseNode)
	_exit(b *BaseNode)
}

type Tick struct {
	ITick
	tree       *BehaviorTree
	target     interface{}
	blackboard *Blackboard
	actorId    int64
}

func NewTick() *Tick {
	return &Tick{}
}

func (t *Tick) Destruct() {
	t.tree = nil
	t.target = nil
	t.blackboard = nil
	t.actorId = 0
}

func (t *Tick) Tree() *BehaviorTree {
	return t.tree
}

func (t *Tick) Target() interface{} {
	return t.target
}

func (t *Tick) ActorId() int64 {
	return t.actorId
}

func (t *Tick) Blackboard() *Blackboard {
	return t.blackboard
}

func (t *Tick) _enter(b *BaseNode) {

}

func (t *Tick) _open(b *BaseNode) {

}

func (t *Tick) _tick(b *BaseNode) {

}

func (t *Tick) _close(b *BaseNode) {

}

func (t *Tick) _exit(b *BaseNode) {

}
