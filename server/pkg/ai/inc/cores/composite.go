package cores

import "hm/pkg/ai/inc"

type IComposite interface {
	ChildrenLength() int
	Children(index int) IBaseNode
	AppendChildren(n IBaseNode) []IBaseNode
}

type Composite struct {
	BaseNodeWork
	children []IBaseNode
}

func (c *Composite) Create() {
	c.category = inc.COMPOSITE
	c.children = make([]IBaseNode, 0)
}

func (c *Composite) ChildrenLength() int {
	return len(c.children)
}

func (c *Composite) Children(index int) IBaseNode {
	return c.children[index]
}

func (c *Composite) AppendChildren(n IBaseNode) []IBaseNode {
	c.children = append(c.children, n)
	return c.children
}
