package cores

import (
	"hm/pkg/ai/inc"
	"sync"
)

// Blackboard

type Memory struct {
	sync.RWMutex
	m map[string]interface{}
}

func NewMemory() *Memory {
	return &Memory{m: make(map[string]interface{})}
}

func (m *Memory) Get(key string) interface{} {
	m.RWMutex.RLock()
	defer m.RWMutex.RUnlock()
	return m.m[key]
}
func (m *Memory) Set(key string, val interface{}) {
	m.RWMutex.Lock()
	defer m.RWMutex.Unlock()
	m.m[key] = val
}
func (m *Memory) Remove(key string) {
	m.RWMutex.Lock()
	defer m.RWMutex.Unlock()
	delete(m.m, key)
}

type NodeMemory struct {
	*Memory
	OpenNodes      []IBaseNode
	TraversalDepth int
	TraversalCycle int
}

func NewNodeMemory() *NodeMemory {
	return &NodeMemory{
		Memory:         NewMemory(),
		OpenNodes:      make([]IBaseNode, 0),
		TraversalDepth: 0,
		TraversalCycle: 0,
	}
}

type TreeMemory struct {
	*Memory
	nodes map[string]*NodeMemory
	mu    sync.RWMutex
}

func (t *TreeMemory) GetNodeMemory(node string) *NodeMemory {
	t.mu.Lock()
	defer t.mu.Unlock()
	if memory, has := t.nodes[node]; !has {
		memory = NewNodeMemory()
		t.nodes[node] = memory
		return memory
	} else {
		return memory
	}
}

func NewTreeMemory() *TreeMemory {
	return &TreeMemory{
		Memory: NewMemory(),
		nodes:  make(map[string]*NodeMemory),
	}
}

type Blackboard struct {
	base *Memory
	tree map[int64]*TreeMemory
	mu   sync.RWMutex
}

func NewBlackboard() *Blackboard {
	return &Blackboard{base: NewMemory(), tree: make(map[int64]*TreeMemory)}
}

func (b *Blackboard) getTreeMemory(tree int64) *TreeMemory {
	b.mu.Lock()
	defer b.mu.Unlock()
	memory, has := b.tree[tree]
	if !has {
		memory = NewTreeMemory()
		b.tree[tree] = memory
	}
	return memory
}

func (b *Blackboard) getNodeMemory(tree int64, node string) *NodeMemory {
	t := b.getTreeMemory(tree)
	t.mu.Lock()
	defer t.mu.Unlock()
	memory, has := t.nodes[node]
	if !has {
		memory = NewNodeMemory()
		t.nodes[node] = memory
	}
	return memory
}

func (b *Blackboard) getMemory(tree int64, node string) *Memory {
	memory := b.base
	if tree != 0 {
		tMemory := b.getTreeMemory(tree)
		if node != inc.EMPTY {
			nMemory := tMemory.GetNodeMemory(node)
			memory = nMemory.Memory
		} else {
			memory = tMemory.Memory
		}
	}
	return memory
}

func (b *Blackboard) Set(key string, val interface{}, tree int64, node string) {
	m := b.getMemory(tree, node)
	m.Set(key, val)
}

func (b *Blackboard) SetGlobal(key string, val interface{}) {
	m := b.getMemory(0, inc.EMPTY)
	m.Set(key, val)
}

func (b *Blackboard) SetTree(key string, val interface{}, tree int64) {
	m := b.getMemory(tree, inc.EMPTY)
	m.Set(key, val)
}

func (b *Blackboard) Remove(key string, tree int64, node string) {
	m := b.getMemory(tree, node)
	m.Remove(key)
}

func (b *Blackboard) RemoveGlobal(key string) {
	m := b.getMemory(0, inc.EMPTY)
	m.Remove(key)
}

func (b *Blackboard) RemoveTree(key string, tree int64) {
	m := b.getMemory(tree, inc.EMPTY)
	m.Remove(key)
}

func (b *Blackboard) Get(key string, tree int64, node string) interface{} {
	m := b.getMemory(tree, node)
	return m.Get(key)
}

func (b *Blackboard) GetGlobal(key string) interface{} {
	m := b.getMemory(0, inc.EMPTY)
	return m.Get(key)
}

func (b *Blackboard) GetInt(key string, tree int64, node string) int {
	v := b.getMemory(tree, node).Get(key)
	if nil == v {
		return 0
	}
	return v.(int)
}

func (b *Blackboard) GetInt32(key string, tree int64, node string) int32 {
	v := b.getMemory(tree, node).Get(key)
	if nil == v {
		return 0
	}
	return v.(int32)
}

func (b *Blackboard) GetInt64(key string, tree int64, node string) int64 {
	v := b.getMemory(tree, node).Get(key)
	if nil == v {
		return 0
	}
	return v.(int64)
}

func (b *Blackboard) GetGlobalInt64(key string) int64 {
	v := b.GetGlobal(key)
	if nil == v {
		return 0
	}
	return v.(int64)
}

func (b *Blackboard) GetBool(key string, tree int64, node string) bool {
	v := b.getMemory(tree, node).Get(key)
	if nil == v {
		return false
	}
	return v.(bool)
}

func (b *Blackboard) GetGlobalBool(key string) bool {
	v := b.GetGlobal(key)
	if nil == v {
		return false
	}
	r, ok := v.(bool)
	if !ok {
		return false
	}
	return r
}

func (b *Blackboard) GetString(key string, tree int64, node string) string {
	v := b.getMemory(tree, node).Get(key)
	if nil == v {
		return inc.EMPTY
	}
	return v.(string)
}

func (b *Blackboard) GetFloat32(key string, tree int64, node string) float32 {
	v := b.getMemory(tree, node).Get(key)
	if nil == v {
		return 0
	}
	return v.(float32)
}

func (b *Blackboard) GetGlobalFloat32(key string) float32 {
	v := b.GetGlobal(key)
	if nil == v {
		return 0
	}
	return v.(float32)
}
