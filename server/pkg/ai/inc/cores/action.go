package cores

import "hm/pkg/ai/inc"

type Action struct {
	BaseNodeWork
}

func (a *Action) Create() {
	a.category = inc.ACTION
}
