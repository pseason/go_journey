package cores

import "hm/pkg/ai/inc"

type Condition struct {
	BaseNodeWork
}

func (c *Condition) Create() {
	c.category = inc.CONDITION
}
