package inc

import (
	"errors"
	"reflect"
)

// Author: L
// Created: 03/16/21
// Desc: 行为树核心库

type BtStructOptMaps struct {
	maps map[string]reflect.Type
}

func NewBtStructOptMaps() *BtStructOptMaps {
	return &BtStructOptMaps{maps: make(map[string]reflect.Type)}
}

func (b *BtStructOptMaps) Register(name string, i interface{}) *BtStructOptMaps {
	b.maps[name] = reflect.TypeOf(i).Elem()
	return b
}

func (b *BtStructOptMaps) Has(name string) bool {
	if _, has := b.maps[name]; has {
		return true
	}
	return false
}

func (b *BtStructOptMaps) Type(name string) reflect.Type {
	if v, has := b.maps[name]; has {
		return v
	}
	return nil
}

func (b *BtStructOptMaps) Len() int {
	return len(b.maps)
}

func (b *BtStructOptMaps) Each(call func(key string, t reflect.Type)) {
	for s, r := range b.maps {
		ss := s
		rr := r
		call(ss, rr)
	}
}

func (b *BtStructOptMaps) New(name string) (res interface{}, err error) {
	rt, has := b.maps[name]
	if !has {
		err = errors.New("struct does not exist: " + name)
		return
	}
	res = reflect.New(rt).Interface()
	return
}
