package composites

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type MemSequence struct {
	cores.Composite
}

func (m *MemSequence) OnOpen(tick *cores.Tick) {
	tick.Blackboard().Set("runningChild", 0, tick.Tree().Id(), m.Id())
}

func (m *MemSequence) OnTick(tick *cores.Tick) inc.BtStatus {
	rc := tick.Blackboard().GetInt("runningChild", tick.Tree().Id(), m.Id())
	for i := rc; i < m.ChildrenLength(); i++ {
		status := m.Children(i).Execute(tick)
		if status != inc.SUCCESS {
			if status == inc.RUNNING {
				tick.Blackboard().Set("runningChild", i, tick.Tree().Id(), m.Id())
			}
			return status
		}
	}
	return inc.SUCCESS
}
