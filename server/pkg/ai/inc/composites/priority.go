package composites

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type Priority struct {
	cores.Composite
}

func (p *Priority) OnTick(tick *cores.Tick) inc.BtStatus {
	for i := 0; i < p.ChildrenLength(); i++ {
		status := p.Children(i).Execute(tick)
		if status != inc.FAILURE {
			return status
		}
	}
	return inc.FAILURE
}
