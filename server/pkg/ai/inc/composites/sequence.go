package composites

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

type Sequence struct {
	cores.Composite
}

func (s *Sequence) OnTick(tick *cores.Tick) inc.BtStatus {
	for i := 0; i < s.ChildrenLength(); i++ {
		status := s.Children(i).Execute(tick)
		if status != inc.SUCCESS {
			return status
		}
	}
	return inc.SUCCESS
}
