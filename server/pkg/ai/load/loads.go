package load

import (
	"95eh.com/eg/utils"
	"fmt"
	"hm/pkg/ai/action"
	"hm/pkg/ai/condition"
	"hm/pkg/ai/decorator"
	inc2 "hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/inc/load"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"hm/pkg/game/common"
)

// Author: L
// Created: 2021/1/27
// Describe: 加载行为树

type aiLoad struct {
	globalAiBehaviorTrees map[interf.TAI]*cores.BehaviorTree
	extMonsterMaps        *inc2.BtStructOptMaps
}

func NewAiLoad() interf.IAiLoad {
	return &aiLoad{}
}

func (a *aiLoad) InitBevExtMaps(ext *inc2.BtStructOptMaps) {
	//actions
	ext.Register("Log", &action.Log{})
	ext.Register("Escape", &action.Escape{})
	ext.Register("CounterAttack", &action.CounterAttack{})
	ext.Register("ChasingPlayers", &action.ChasingPlayers{})
	ext.Register("GoInitPosition", &action.GoInitPosition{})
	ext.Register("StopMove", &action.StopMove{})
	ext.Register("BloodBack", &action.BloodBack{})
	ext.Register("FindAttacker", &action.FindAttacker{})
	ext.Register("SetStateBool", &action.SetStateBool{})
	ext.Register("RemoveCurrentChaseAttacker", &action.RemoveCurrentChaseAttacker{})
	ext.Register("ResetEscapeStartTime", &action.ResetEscapeStartTime{})
	ext.Register("RemoveState", &action.RemoveState{})
	ext.Register("SetUpSimilarAttackersAround", &action.SetUpSimilarAttackersAround{})
	ext.Register("SyncUpSimilarAttackersAround", &action.SyncUpSimilarAttackersAround{})
	ext.Register("LoopWait", &action.LoopWait{})
	ext.Register("Patrol", &action.Patrol{})
	ext.Register("PatrolStop", &action.PatrolStop{})
	ext.Register("MoveToRecentTeammatesPosition", &action.MoveToRecentTeammatesPosition{})

	//conditions
	ext.Register("PlayerNear", &condition.PlayerNear{})
	ext.Register("HpLess", &condition.HpLess{})
	ext.Register("TimeDistanceNoPlayer", &condition.TimeDistanceNoPlayer{})
	ext.Register("IsInitPosition", &condition.IsInitPosition{})
	ext.Register("UnderActiveAttack", &condition.UnderActiveAttack{})
	ext.Register("PlayerNearDistanceCount", &condition.PlayerNearDistanceCount{})
	ext.Register("HasStateBool", &condition.HasStateBool{})
	ext.Register("AttackerHpLess", &condition.AttackerHpLess{})
	ext.Register("AttackerJudgment", &condition.AttackerJudgment{})
	ext.Register("HadAttacker", &condition.HadAttacker{})
	ext.Register("OnPatrol", &condition.OnPatrol{})
	ext.Register("AttackerDistanceLess", &condition.AttackerDistanceLess{})

	//decorators
	ext.Register("AlwaysSuccess", &decorator.AlwaysSuccess{})
	ext.Register("AlwaysFailure", &decorator.AlwaysFailure{})
}

// 加载配置的行为树
// path：路径，默认为执行所在目录下的配置文件名
func (a *aiLoad) LoadCfgBehaviorTree(config *common.AiConfig, paths ...string) {
	var path string
	if len(paths) > 0 {
		path = paths[0]
	} else {
		path = utils.ExeDir() + config.BtPath
	}
	a.extMonsterMaps = inc2.NewBtStructOptMaps()
	a.globalAiBehaviorTrees = make(map[interf.TAI]*cores.BehaviorTree)
	a.InitBevExtMaps(a.extMonsterMaps)
	basicMaps := load.CreateBasicExtStructOptMaps()
	projectConfig, err := inc2.LoadProjectConfig(path)
	if err != nil {
		panic("加载Ai行为配置失败")
	}
	//加载野怪
	for i := range projectConfig.Trees {
		tree := projectConfig.Trees[i]
		loadIn, has := tree.Properties["load"]
		if has && loadIn == "false" {
			continue
		}
		pid, has := tree.Properties["id"]
		if !has {
			panic(fmt.Sprintf("野怪行为树[ %s ]无目标[ id ]属性", tree.Title))
		}
		id := uint16(pid.(float64))
		a.globalAiBehaviorTrees[interf.TAI(id)] = cores.NewBehaviorTree(&tree, basicMaps, a.extMonsterMaps)
	}
	log.DLogInfo("AI行为从 %s 加载完成", nil, path)
	log.DLogInfo("加载AI行为配置完成，共计 %d 条", nil, len(a.globalAiBehaviorTrees))
}

func (a *aiLoad) GetGlobalAiBehaviorTrees(typ interf.TAI) (res interface{}, exits bool) {
	res, exits = a.globalAiBehaviorTrees[typ]
	return
}
