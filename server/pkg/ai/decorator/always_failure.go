package decorator

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// AlwaysFailure 始终为failure
type AlwaysFailure struct {
	cores.Decorator `default:"" note:"始终为failure"`
}

func (a *AlwaysFailure) Init(cfg *inc.BTNodeConfig) {
	a.Decorator.Init(cfg)
}

func (a *AlwaysFailure) OnTick(tick *cores.Tick) inc.BtStatus {
	a.Child().Execute(tick)
	return inc.FAILURE
}
