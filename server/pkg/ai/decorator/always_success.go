package decorator

import (
	"hm/pkg/ai/inc"
	"hm/pkg/ai/inc/cores"
)

// AlwaysSuccess 始终为success
type AlwaysSuccess struct {
	cores.Decorator `default:"" note:"始终为success"`
}

func (a *AlwaysSuccess) Init(cfg *inc.BTNodeConfig) {
	a.Decorator.Init(cfg)
}

func (a *AlwaysSuccess) OnTick(tick *cores.Tick) inc.BtStatus {
	a.Child().Execute(tick)
	return inc.SUCCESS
}
