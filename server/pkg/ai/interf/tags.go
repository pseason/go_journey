package interf

const (
	// SE 范围空间空字符
	SE = ""
	// SPEED 速度
	SPEED = "SPEED"
	// UnderActiveAttackLastTime 受到玩家主动攻击时间
	UnderActiveAttackLastTime = "UnderActiveAttackLastTime"
	// CurrentChasingAttacker 当前追击攻击者
	CurrentChasingAttacker = "CurrentChasingAttacker"
	// EscapeStart 逃跑开始标记
	EscapeStart = "EscapeStart"
	// TickInterval 滴答间隔/单位：毫秒
	TickInterval = "TickInterval"
	// EscapeStartTime 逃跑开始时间/单位：秒
	EscapeStartTime = "EscapeStartTime"
	// EscapeStartVec3 逃跑移动开始坐标
	EscapeStartVec3 = "EscapeStartVec3"
	// ReturnState 回归状态
	ReturnState = "ReturnState"
	// LastNotHasActorTime 上次周围无人时间
	LastNotHasActorTime = "LastNotHasActorTime"
	// AttackerHpLessExistenceOver {攻击者血量少于或等于时} 如果攻击者不存在则返回SUCCESS
	AttackerHpLessExistenceOver = "AttackerHpLessExistenceOver"
	// AttackerDistanceLessExistenceOver {攻击者距离小于或等于时} 如果攻击者不存在则返回SUCCESS
	AttackerDistanceLessExistenceOver = "AttackerDistanceLessExistenceOver"
	// LastChasingAttackerVec3 上次追踪攻击者的方位
	LastChasingAttackerVec3 = "LastChasingAttackerVec3"
	// LastAttackerDistanceVec3 上次和攻击者之间调整距离的点位
	LastAttackerDistanceVec3 = "LastAttackerDistanceVec3"
	// OnPatrol 巡逻中
	OnPatrol = "OnPatrol"
	// LastStartPatrolTime 上次巡逻开始时间
	LastStartPatrolTime = "LastStartPatrolTime"
	// LastChasingAttackerTime 上次攻击攻击者的时间
	LastChasingAttackerTime = "LastChasingAttackerTime"
	// SkillCDSurplus 技能剩余CD时间
	SkillCDSurplus = "SkillCDSurplus"
	// SkillReleaseCanMoveTime 技能释放后可移动时间
	SkillReleaseCanMoveTime = "SkillReleaseCanMoveTime"
	// MoveToRTP 移动至最近队友的身边开始状态
	MoveToRTP = "MoveToRTP"
	// GoInitPosition 回到初始位置状态
	GoInitPosition = "GoInitPosition"
	// TooCloseBackCD 距离过近后退CD
	TooCloseBackCD
	// CumulativeCallDeathNum 累计死亡次数
	CumulativeCallDeathNum = "CumulativeCallDeathNum"
	// AddHighAiByLowAiDeath 由于小兵死亡达到上限触发高级AI出生
	AddHighAiByLowAiDeath = "AddHighAiByLowAiDeath"
)
