package interf

import cmap "github.com/orcaman/concurrent-map"

type IAiTick interface {
	// Start 启动（此项一定要在IAi接口中Context未取消状态下进行）
	Start()
	// Running 是否在运行中
	Running() bool
	// Push 添加行动者
	Push(lock bool, actor IAiActor) (err error)
	// PushAll 添加多个行动者
	PushAll(lock bool, actor ...IAiActor) (err error)
	// Remove 删除行动者
	Remove(actorId int64) bool
	// RemoveAll 移除所有的行动者
	RemoveAll()
	// ClearAllBySpaceSid 移除指定空间服务下的行动者
	ClearAllBySpaceSid(sid uint16)
	// GetSpaceSidGroupMetas 获取空间服务ID分组的元信息
	GetSpaceSidGroupMetas() (metas []ActorMetaInfo)
	// GetActor 获取行动者
	GetActor(actorId int64) (actor IAiActor, ok bool)
	WarnPlayers() cmap.ConcurrentMap
}
