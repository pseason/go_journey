package interf

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"hm/pkg/misc"
)

// TAI AI类型
type TAI uint16

// TMonsterScene 野怪场景
type TMonsterScene int32

const (
	// TAISheep 羊
	TAISheep TAI = 1000
	// TAIHare 野兔
	TAIHare TAI = 2000
	// TAIBoar 野猪
	TAIBoar TAI = 3000
	// TAIBear 熊
	TAIBear TAI = 4000
	// TAIWolf 狼
	TAIWolf TAI = 5000
	// TAIOx 牛
	TAIOx TAI = 6000
	// TAIPheasant 野鸡
	TAIPheasant TAI = 7000
	// TAITiger 老虎
	TAITiger TAI = 8000
	// TAIBoss 新手村BOSS
	TAIBoss TAI = 9000
	// TAIRobot 机器人
	TAIRobot TAI = 60000
	// TAIBuildingSkill 建筑（箭塔）
	TAIBuildingSkill TAI = 61100
	// TAITerritoryBoss 领土BOSS
	TAITerritoryBoss TAI = 13000
)

const (
	// TmsWorld 世界
	TmsWorld TMonsterScene = iota + 1
	// TmsNovice 新手村
	TmsNovice
	// TmsBoss BOSS
	TmsBoss
)

type (
	// ActorMetaInfo 元信息
	ActorMetaInfo struct {
		Node      misc.TNode
		ServiceId uint16
		NodeId    uint16
	}
	// AiCodecAction 编码请求操作
	AiCodecAction func(ctx ActorActionCtx, body interface{}) (res interface{}, errCode utils.TErrCode)
	AiEventAction func(ctx ActorActionCtx, body interface{})
	// ActorActionCtx 请求操作上下文
	ActorActionCtx interface {
		Meta() ActorMetaInfo
		Tid() int64
		TargetId() int64
	}
	WarnActor struct {
		Id       int64
		Type     intfc.TActor
		Position utils.Vec3
		Forward  utils.Vec3
	}
	actorActionCtx struct {
		meta     ActorMetaInfo
		tid      int64
		targetId int64
	}
)

func NewActorActionCtx(meta ActorMetaInfo, tid, targetId int64) ActorActionCtx {
	return &actorActionCtx{
		meta:     meta,
		tid:      tid,
		targetId: targetId,
	}
}

func (a *actorActionCtx) Meta() ActorMetaInfo {
	return a.meta
}

func (a *actorActionCtx) Tid() int64 {
	return a.tid
}

func (a *actorActionCtx) TargetId() int64 {
	return a.targetId
}
