package interf

import (
	"95eh.com/eg/utils"
	cmap "github.com/orcaman/concurrent-map"
	"hm/pkg/misc"
	"hm/pkg/misc/tsv"
)

type IAiActor interface {
	// Id 唯一标识
	Id() int64
	// IdString 唯一标识
	IdString() string
	WarnActor() cmap.ConcurrentMap
	Position() utils.Vec3
	SetPosition(position *utils.Vec3, forward *utils.Vec3)
	// Scene 所在场景
	Scene() misc.TScene
	// Meta 元数据
	Meta() ActorMetaInfo
	// Event 事件
	Event(eventId int32, body interface{}) (err error)
	// GetBornPoint 获取出生点
	GetBornPoint() (p utils.Vec3)
	// Move 移动, 当v为nil时则随机点位
	Move(speed float32, v *utils.Vec3, currentVec3 *utils.Vec3) (err error)
	// StopMove 停止移动
	StopMove() (err error)
	// GetAttacker 获取攻击者
	GetAttacker()
	// GetHP 获取血量
	GetHP()
	// Type 类型
	Type() TAI
	// UseSkill 使用技能
	UseSkill()
	// GetWarnPlayer 获取警戒范围内的玩家
	GetWarnPlayer()
	// AttrChange 属性改变
	AttrChange()
	// Tick 帧刷新
	Tick()
	// ReviveCallback 复活回调
	ReviveCallback()
	// DeathCallback 死亡回调
	DeathCallback()
	// IsDeath 是否死亡
	IsDeath() bool
	// Title 名称
	Title() string
	// Init 初始化
	Init()
	// Template Tsv模板
	Template() tsv.MonsterTsv
	// GetPatrolSpeed 获取巡逻速度
	GetPatrolSpeed() float32
	// GetWarnRadius 获取警戒半径
	GetWarnRadius() int32
	// GetMaxHaredCount 获取最大仇恨数量
	GetMaxHaredCount() int32
	// GetRebirthDuration 获取重生时间
	GetRebirthDuration() int32
	// Instance 获取实例本身
	Instance() interface{}
	// RequestMetaNode 请求元信息内的始发节点
	RequestMetaNode(code uint32, body interface{}) (res interface{}, err error)
	// GetWarnRecentPlayerDistance 获取警戒范围内最近的玩家的距离
	GetWarnRecentPlayerDistance() (playerId int64, distance float32, has bool)
	// GetWarnDistancePlayer 获取指定距离内的玩家
	GetWarnDistancePlayer(distance float32) (actors []*WarnActor, has bool)
	// GetWarnDistanceHasPlayer 获取指定距离内是否含有玩家
	GetWarnDistanceHasPlayer(distance float32) (has bool)
	// RemoteClear //远程移除
	RemoteClear() (err error)
	// WarnDistance //警戒距离
	WarnDistance() float32
}
