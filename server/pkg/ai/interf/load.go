package interf

import (
	inc2 "hm/pkg/ai/inc"
	"hm/pkg/game/common"
)

type IAiLoad interface {
	InitBevExtMaps(ext *inc2.BtStructOptMaps)
	LoadCfgBehaviorTree(config *common.AiConfig, paths ...string)
	GetGlobalAiBehaviorTrees(typ TAI) (res interface{}, exits bool)
}
