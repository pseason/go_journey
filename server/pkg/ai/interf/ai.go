package interf

import (
	"95eh.com/eg/intfc"
	"95eh.com/eg/utils"
	"context"
	esConfig "github.com/olivere/elastic/config"
	"hm/pkg/game/common"
	"hm/pkg/misc"
)

type IAi interface {
	// GetTick 获取Tick
	GetTick() IAiTick
	// Run 运行
	Run()
	// Conf 获取配置
	Conf() *common.AiConfig
	// EsConf Es配置
	EsConf() *esConfig.Config
	// NewAiActorByMonsterId 创建一个新的AI目标根据Monster表内的ID
	NewAiActorByMonsterId(monsterTemplateId int32, bornPoint *utils.Vec3, scene misc.TScene, meta ActorMetaInfo) (res IAiActor, err error)
	// InitSceneMonsters 初始化场景的所有野怪
	InitSceneMonsters(meta ActorMetaInfo,tplId int32) (actors []IAiActor, actorIds []int64, err error)
	// Stop 停止
	Stop(clearActors bool)
	// Reload 重载野怪配置并重启
	Reload(path string)
	// GetContext 获取上下文
	GetContext() context.Context
	// Running 是否在运行中
	Running() bool
	// CodecBind 编码器绑定
	CodecBind(discovery intfc.IMDiscovery)
	// Space 空间
	Space() common.ISceneProxy
	// Controller 游戏服控制器
	Controller() common.IBaseController
	OnNodeConnected(node intfc.TNode, regionId, nodeId uint16)
	OnNodeClosed(node intfc.TNode, regionId, nodeId uint16)
	InitAi(node intfc.TNode, nodeId uint16, tplId int32)
	IncDeathNum(typ TAI) bool
	GetDeathNum(typ TAI) int32
}
