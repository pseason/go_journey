package tool

import (
	"95eh.com/eg/utils"
	"errors"
	"fmt"
	"hm/pkg/ai/inc/cores"
	"hm/pkg/ai/interf"
	"hm/pkg/misc"
	"strings"
)

// MatchSceneByRequestNodeId 根据节点ID匹配场景
func MatchSceneByRequestNodeId(nodeId misc.TNode) (scene misc.TScene, err error) {
	switch nodeId {
	case misc.NodeServices_World:
		scene = misc.SceneWorld
	case misc.NodeServices_Novice:
		scene = misc.SceneNovice
	case misc.NodeServices_CitySpc:
		scene = misc.SceneCity
	default:
		err = errors.New(fmt.Sprintf("not match scene: %v", nodeId))
	}
	return
}

// IsEmptyPositionVec3 是否是空位置信息
func IsEmptyPositionVec3(v *utils.Vec3) bool {
	if v == nil {
		return true
	}
	if v.X == 0 && v.Y == 0 && v.Z == 0 {
		return true
	}
	return false
}

func GetTickIActor(tick *cores.Tick) interf.IAiActor {
	return tick.Target().(interf.IAiActor)
}

func ClearGlobalBlackboardState(tick *cores.Tick, s string) {
	if s != "" {
		states := strings.Split(s, ",")
		for _, v := range states {
			name := strings.TrimSpace(v)
			tick.Blackboard().RemoveGlobal(name)
		}
	}
}
