package front

import (
	"95eh.com/eg/utils"
	"context"
	"fmt"
	"github.com/Licoy/stail"
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
	"github.com/olivere/elastic"
	"hm/pkg/ai/interf"
	"hm/pkg/ai/log"
	"hm/pkg/game/common"
	"hm/pkg/misc"
	"net"
	"net/http"
	"path/filepath"
	"sync"
)

type AiFront struct {
	devtoolConfig common.AiDevtoolConfig
	ai            interf.IAi
	router        *gin.Engine
	ws            map[int64]*logFileTail
	wsMu          sync.RWMutex
	stop          chan bool
	ctx           context.Context
	st            stail.STail
	es            *elastic.Client
}

func StartFrontServer(ai interf.IAi) *AiFront {
	st, _ := stail.New(stail.Options{})
	front := &AiFront{
		devtoolConfig: ai.Conf().Devtool,
		ai:            ai,
		ws:            make(map[int64]*logFileTail),
		st:            st,
	}
	go front.runServer()
	if front.devtoolConfig.OpenFileTail {
		go front.logTailWsHeartHealthCheck()
	}
	if front.devtoolConfig.OpenEsTail {
		front.es = misc.GetElasticSearchConn(ai.EsConf())
	}
	return front
}

func (a *AiFront) runServer() {
	a.router = gin.New()
	a.router.HTMLRender = a.loadTemplates(utils.ExeDir() + "/ai/front")
	a.router.StaticFS("/libs",http.Dir(utils.ExeDir() + "/ai/front/libs"))
	listen, err := net.Listen("tcp", fmt.Sprintf(":%d", a.devtoolConfig.Port))
	if err != nil {
		log.DLogError("listen tcp port failed", utils.M{
			"port": a.devtoolConfig.Port,
			"err":  err,
		})
		return
	}
	a.Routers()
	err = a.router.RunListener(listen)
	if err != nil {
		log.DLogError("front server start failed", utils.M{
			"port": a.devtoolConfig.Port,
			"err":  err,
		})
	} else {
		log.DLogInfo("front server start succeed", utils.M{
			"port": a.devtoolConfig.Port,
		})
	}
}

func (a *AiFront) loadTemplates(templatesDir string) multitemplate.Renderer {
	r := multitemplate.NewRenderer()
	layouts, err := filepath.Glob(templatesDir + "/layouts/*.tmpl")
	if err != nil {
		panic(err.Error())
	}

	includes, err := filepath.Glob(templatesDir + "/pages/*.tmpl")
	if err != nil {
		panic(err.Error())
	}

	for _, include := range includes {
		layoutCopy := make([]string, len(layouts))
		copy(layoutCopy, layouts)
		files := append(layoutCopy, include)
		filename := filepath.Base(include)
		log.DLogDebug("加载Go-Template完成:"+filename, nil)
		r.AddFromFiles(filename, files...)
	}
	return r
}

func (a *AiFront) Stop() {
	a.stop <- true
}
