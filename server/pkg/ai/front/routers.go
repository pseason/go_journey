package front

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (a *AiFront) Routers() {
	a.RegisterGroupRouter("/", func(r *gin.RouterGroup) {
		r.Handle(http.MethodGet, "/", func(context *gin.Context) {
			context.HTML(http.StatusOK, "index.tmpl", gin.H{
				"title": "首页",
			})
		})
		r.Handle(http.MethodGet, "/stop", func(context *gin.Context) {
			a.ai.Stop(true)
		})
	})
	a.RegisterGroupRouter("/log/file", func(r *gin.RouterGroup) {
		r.Handle(http.MethodGet, "/", a.logFileTailRouter)
		r.Handle(http.MethodGet, "/tail", a.logFileTailWsRouter)
		r.Handle(http.MethodPost, "/clear", a.logFileTailClearRouter)
	})
	a.RegisterGroupRouter("/log/es", func(r *gin.RouterGroup) {
		r.Handle(http.MethodGet, "/", a.logEsTailIndexRouter)
		r.Handle(http.MethodGet, "/tail", a.logEsTailGetRouter)
		r.Handle(http.MethodPost, "/clear", a.logEsTailClearRouter)
	})
	a.RegisterGroupRouter("/ai", func(r *gin.RouterGroup) {
		r.Handle(http.MethodPost, "/reload", a.aiReloadRouter)
		r.Handle(http.MethodPost, "/pause", a.aiPauseRouter)
		r.Handle(http.MethodPost, "/start", a.aiStartRouter)
		r.Handle(http.MethodPost, "/scene/option", a.aiSceneOption)
	})
}

func (a *AiFront) RegisterGroupRouter(relativeGroupPath string, fn func(r *gin.RouterGroup)) {
	fn(a.router.Group(relativeGroupPath))
}
