package front

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"hm/pkg/misc/utils"
	"net/http"
	"strconv"
)

func (a *AiFront) logEsTailIndexRouter(g *gin.Context) {
	indices, err := a.es.CatIndices().Do(context.Background())
	if err != nil {
		_ = ResponseFail(g, fmt.Sprintf("获取es索引列表失败：%v", err.Error()))
		return
	}
	index := g.Query("index")
	g.HTML(http.StatusOK, "log-es.tmpl", gin.H{
		"title":   "Elastic行为记录",
		"indices": indices,
		"index":   index,
	})
}

func (a *AiFront) logEsTailGetRouter(g *gin.Context) {
	index := g.Query("index")
	offsetStr := g.Query("offset")
	sizeStr := g.Query("size")
	var offset, size int
	if offsetStr != "" {
		offset, _ = strconv.Atoi(offsetStr)
	}
	if sizeStr != "" {
		size, _ = strconv.Atoi(sizeStr)
	}
	if size <= 0 {
		size = 50
	}
	if index == "" {
		_ = ResponseFail(g, "索引名称不能为空")
		return
	}
	if offset == -1 {
		counts, err := a.es.CatCount().Index(index).Do(context.Background())
		if err != nil {
			_ = ResponseFail(g, "获取索引统计数据失败")
			return
		}
		count := counts[0]
		offset = utils.If3(count.Count >= size, count.Count-size, 0).(int)
	}
	data, err := a.es.Search().Index(index).Type("_doc").From(offset).
		Size(size).Sort("ts", true).
		Do(context.Background())
	if err != nil {
		_ = ResponseFail(g, fmt.Sprintf("获取索引内容失败：%v", err.Error()))
		return
	}
	_ = ResponseOk(g, gin.H{
		"count":  data.Hits.TotalHits,
		"offset": offset,
		"size":   size,
		"next":   offset + len(data.Hits.Hits),
		"data":   data.Hits.Hits,
	})
}

func (a *AiFront) logEsTailClearRouter(g *gin.Context) {
	index := g.Query("index")
	_, err := a.es.DeleteIndex(index).Do(context.Background())
	if err != nil {
		_ = ResponseFail(g, fmt.Sprintf("日志清除失败：%v", err.Error()))
		return
	}
	_ = ResponseOk(g, nil)
}
