package front

import (
	"95eh.com/eg/utils"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type logFileTail struct {
	ws            *websocket.Conn
	stop          chan bool
	lastHeartTime time.Time
	closeFn       func()
}

func (a *AiFront) logFileTailRouter(context *gin.Context) {
	dir, err := os.ReadDir(a.devtoolConfig.LogDir)
	files := make([]string, 0, 10)
	if err == nil {
		for _, v := range dir {
			if !v.IsDir() {
				files = append(files, v.Name())
			}
		}
	}
	filename := context.Query("file")
	context.HTML(http.StatusOK, "log-file.tmpl", gin.H{
		"title":    "本地文件行为记录",
		"files":    files,
		"filename": filename,
	})
}

func (a *AiFront) logFileTailWsRouter(context *gin.Context) {
	filename := context.Query("file")
	if filename == "" {
		return
	}
	ws, err := upGrader.Upgrade(context.Writer, context.Request, nil)
	if err != nil {
		return
	}
	if !a.devtoolConfig.OpenFileTail {
		_ = ws.WriteMessage(websocket.TextMessage, []byte("not open file tail, please check your config"))
		_ = ws.Close()
		return
	}
	if a.st == nil {
		_ = ws.WriteMessage(websocket.TextMessage, []byte("stail is nil, watch not open"))
		_ = ws.Close()
		return
	}
	stopChan := make(chan bool, 1)
	is, err := a.st.TailTotal(a.devtoolConfig.LogDir+"/"+filename, func(content string) {
		_ = ws.WriteMessage(websocket.TextMessage, []byte(content))
	})
	if err != nil {
		_ = ws.WriteMessage(websocket.TextMessage, []byte("watch file failed"))
		return
	}
	l := &logFileTail{
		ws:            ws,
		stop:          stopChan,
		lastHeartTime: time.Now(),
		closeFn: func() {
			_ = ws.Close()
			_ = is.Close()
		},
	}
	ws.SetCloseHandler(func(code int, text string) error {
		l.closeFn()
		return nil
	})
	a.pushLogFileTail(l)
	go listenLogFileTailWsRead(l)
	is.Watch()
}

func (a *AiFront) logFileTailClearRouter(context *gin.Context) {
	filename := context.Query("file")
	if filename == "" {
		return
	}
	err := os.Truncate(a.devtoolConfig.LogDir+"/"+filename, 0)
	if err != nil {
		_ = ResponseFail(context, "清空失败")
		return
	}
	_ = ResponseOk(context, nil)
}

func (a *AiFront) logTailWsHeartHealthCheck() {
	tick := time.NewTicker(time.Second * 10)
	defer tick.Stop()
	for {
		select {
		case <-a.stop:
			return
		case <-tick.C:
			a.logFileTailWsHeartCheckExecute()
		}
	}
}

func (a *AiFront) logFileTailWsHeartCheckExecute() {
	a.wsMu.RLock()
	removeIds := make([]int64, 0, 0)
	for k, v := range a.ws {
		id := k
		if v.lastHeartTime.Add(time.Second*30).Unix() < time.Now().Unix() {
			v.closeFn()
			removeIds = append(removeIds, id)
		}
	}
	a.wsMu.RUnlock()
	a.removeLogFileTail(removeIds...)
}

func (a *AiFront) pushLogFileTail(l *logFileTail) int64 {
	a.wsMu.Lock()
	defer a.wsMu.Unlock()
	id := utils.GenSnowflakeGlobalNodeId()
	a.ws[id] = l
	return id
}

func (a *AiFront) removeLogFileTail(id ...int64) {
	a.wsMu.Lock()
	defer a.wsMu.Unlock()
	for _, v := range id {
		delete(a.ws, v)
	}
}

func listenLogFileTailWsRead(l *logFileTail) {
	for {
		_, r, err := l.ws.NextReader()
		if err != nil {
			if l.closeFn != nil {
				l.closeFn()
			}
			break
		}
		data, err := ioutil.ReadAll(r)
		if err != nil {
			continue
		}
		tag := string(data)
		switch tag {
		case "heart":
			l.lastHeartTime = time.Now()
		case "close":
			if l.closeFn != nil {
				l.closeFn()
			}
		}
	}
}
