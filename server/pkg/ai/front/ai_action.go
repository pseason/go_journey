package front

import (
	"95eh.com/eg/utils"
	"fmt"
	"github.com/gin-gonic/gin"
	utils2 "hm/pkg/misc/utils"
	"os"
	"path/filepath"
)

func (a *AiFront) aiReloadRouter(context *gin.Context) {
	defer func() {
		if r := recover(); r != nil {
			_ = ResponseFail(context, "文件处理失败，出现异常恐慌")
		}
	}()
	file, err := context.FormFile("file")
	if err != nil {
		_ = ResponseFail(context, fmt.Sprintf("获取上传文件失败：%v", err))
		return
	}
	path := utils.ExeDir() + "/ai/bts"
	stat, err := os.Stat(path)
	if err != nil || !stat.IsDir() {
		err := os.MkdirAll(path, os.ModePerm)
		if err != nil {
			_ = ResponseFail(context, fmt.Sprintf("创建上传目录失败：%v", err))
			return
		}
	}
	dst := fmt.Sprintf("%s/%d%s", path, utils2.GetCurrentMillisecond(), filepath.Ext(file.Filename))
	err = context.SaveUploadedFile(file, dst)
	if err != nil {
		_ = ResponseFail(context, fmt.Sprintf("上传存储错误：%v", err))
		return
	}
	_, err = os.Stat(dst)
	if err != nil {
		_ = ResponseFail(context, fmt.Sprintf("读取上传后的文件失败：%v", err))
		_ = os.Remove(dst)
		return
	}
	a.ai.Reload(dst)
	_ = os.Remove(dst)
	_ = ResponseOk(context, nil)
}

func (a *AiFront) aiPauseRouter(context *gin.Context) {
	a.ai.Stop(false)
	_ = ResponseOk(context, nil)
}

func (a *AiFront) aiStartRouter(context *gin.Context) {
	a.ai.Run()
	_ = ResponseOk(context, nil)
}

func (a *AiFront) aiSceneOption(context *gin.Context) {
	//sceneId, _ := strconv.Atoi(context.PostForm("sceneId"))
	init := context.PostForm("init") == "true"
	if init {
		_ = ResponseFail(context, "暂未分配空间服务ID，无法进行初始化")
		return
		//err := a.ai.InitSceneMonsters(misc.TScene(sceneId))
		//if err != nil {
		//	_ = ResponseFail(context, err.Error())
		//	return
		//}
	} else {
		_ = ResponseFail(context, "暂未分配空间服务ID，无法进行删除")
		return
		//a.ai.GetTick().ClearFromScene(misc.TScene(sceneId))
	}
	//_ = ResponseOk(context, nil)
}
