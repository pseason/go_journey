package front

import (
	"github.com/gin-gonic/gin"
	jsoniter "github.com/json-iterator/go"
)

type Response struct {
	ErrCode int         `json:"err_code"`
	Msg     string      `json:"msg"`
	Data    interface{} `json:"data"`
}

func ResponseFail(context *gin.Context, msg string) (err error) {
	res, _ := jsoniter.Marshal(&Response{
		ErrCode: 1,
		Msg:     msg,
	})
	_, err = context.Writer.Write(res)
	return
}

func ResponseOk(context *gin.Context, data interface{}) (err error) {
	res, _ := jsoniter.Marshal(&Response{
		ErrCode: 0,
		Msg:     "ok",
		Data:    data,
	})
	_, err = context.Writer.Write(res)
	return
}
