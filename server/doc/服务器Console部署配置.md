## 服务器Console配置

### 1.配置consul ACL安全

#### 1).新增consul\acl.hcl文件

```json
acl = {
  enabled = true
  default_policy = "deny"
  enable_token_persistence = true
  tokens = {
 	master = ".@password@."
  }
}
```

tokens/master密码可以修改master = "123abc"等

#### 2).docker 启动consul

docker启动命令:

docker run --restart always --name game-consul -d -p 8500:8500 -p 8300:8300 -p 8301:8301 -p 8302:8302 -p 8600:8600 -v /etc/consul:/config consul agent -server -bootstrap-expect=1 -ui -bind=0.0.0.0 -client=0.0.0.0 -config-file="/config/acl.hcl"

#### 3).打开网页进入服务器addr:port(默认8500)配置consul权限

##### 1.点击创建roles角色

![image-20220301150409256](md-assets/服务器Console部署配置/image-20220301150409256.png)

##### 2.填写角色名

![image-20220301150627513](md-assets/服务器Console部署配置/image-20220301150627513.png)

##### 3.创建consul读写配置

![image-20220301151020803](md-assets/服务器Console部署配置/image-20220301151020803.png)

配置信息:

```json
node_prefix "" {
   policy = "write"
}

service_prefix "" {
   policy = "write"
}

key_prefix "" {
   policy = "write"
}
```



##### 4.选择配置模板

![image-20220301151103765](md-assets/服务器Console部署配置/image-20220301151103765.png)

##### 5.创建consul角色

![image-20220301151116561](md-assets/服务器Console部署配置/image-20220301151116561.png)

#### 4).查看角色token

##### 1).点击tokens选择已创建的配置查看token，用于客户端连接

![image-20220301151853393](md-assets/服务器Console部署配置/image-20220301151853393.png)

##### 2).查看token

![image-20220301151930019](md-assets/服务器Console部署配置/image-20220301151930019.png)

#### 5).添加服务器节点

![image-20220301152147023](md-assets/服务器Console部署配置/image-20220301152147023.png)

region/服务节点id

