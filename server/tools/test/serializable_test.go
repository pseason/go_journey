package test

import (
	"95eh.com/eg/data"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/vmihailenco/msgpack/v5"
	"hm/pkg/services/data/model"
	"hm/pkg/services/data/msg_dt"
	"testing"
)

/*
@Time   : 2022-01-11 15:15
@Author : wushu
@DESC   :
*/
// json.Unmarshal 反序列化int64精度丢失问题
func TestJson(t *testing.T) {
	m := map[string]interface{}{"id": 1473559737336881171, "age": 11111111, "name": "嘻嘻"} // 反序列化之后，数值类型都变成了float类型
	marshal, err := json.Marshal(m)
	if err != nil {
		fmt.Println(err)
	}
	var n = make(map[string]interface{})
	err = json.Unmarshal(marshal, &n)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n)
	fmt.Printf("id : %f\n", n["id"].(float64))
	fmt.Printf("age: %f\n", n["age"].(float64))
}

func TestMessagePack(t *testing.T) {
	m := map[string]interface{}{"id": 1473559737336881171, "age": 111, "gold": int32(300), "name": "嘻嘻"}

	marshal, err := msgpack.Marshal(m)
	if err != nil {
		fmt.Println(err)
	}
	var n = make(map[string]interface{})
	// int类型会根据符号和大小反序列化为 int8 uint8 int16 uint16 int32 uint32 int64 uint64；并不会反序列化为int
	// 明确的类型会反序列化为原本的类型。比如明确的int32，就会反序列化为int32
	err = msgpack.Unmarshal(marshal, &n)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n)

	var info = &model.CharacterInfo{Nickname: "嘻嘻", Age: 11}
	r := &msg_dt.ResCommonQueryById{Data: info}

	bytes, err := msgpack.Marshal(r)
	var u = msg_dt.ResCommonQueryById{}
	err = msgpack.Unmarshal(bytes, &u)
	fmt.Println(u)

	//err = msgpack.Unmarshal(u.Data.([]byte), &info)
	//fmt.Println(err)

}
func TestGob(t *testing.T) {
	m := map[string]interface{}{"id": 1473559737336881171, "age": 11111111, "name": "嘻嘻"}

	marshal, err := data.GobMarshal(m)
	if err != nil {
		fmt.Println(err)
	}
	var n = make(map[string]interface{})
	err = data.GobUnmarshal(marshal, &n)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n)

	var info = &model.CharacterInfo{Nickname: "嘻嘻", Age: 11}
	r := &msg_dt.ResCommonQueryById{Data: info}
	gob.Register(&model.CharacterInfo{})
	bytes, err := data.GobMarshal(r)
	var u = msg_dt.ResCommonQueryById{}
	err = data.GobUnmarshal(bytes, &u)
	fmt.Println(u)
}

func TestMapStruct(t *testing.T) {
	var m = map[string]string{
		"nickname": "jerry",
		"age":  "10",
	}

	info := model.CharacterInfo{}
	err := mapstructure.WeakDecode(m, &info)
	if err != nil {
		panic(err)
	}
}
