package main

import (
	"bytes"
	"fmt"
	"hm/pkg/misc/utils"
	"io/fs"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
)

/*
@Time   : 2021-12-02 10:35
@Author : wushu
@DESC   : 根据model生成enum文件夹下的文件，每个model都会生成一个以`model名的蛇形_e`命名的文件夹
标记规则：
	struct：
		@Model(EnumPrefix="";EnumCodeIota)所有的属性都是可选的 必须在要生成的model struct的上一行
		EnumPrefix：表示生成枚举名的前缀。~~什么时候用：一个文件中有多个@Model标记时。最好是model-struct的名字~~(原本是为了这种情况准备的，后来想到应该保持一个model文件对应一个model-struct的规范，所以它的用处自己发掘吧)
		EnumCodeIota：如果不关心枚举的编号，请指明这个属性
	field：有 gorm标签 的行会被解析
		@Code：如果没有标记 EnumCodeIota，则需要用该标记指明编号
	fields: 标记baseModel
		@Codes：用于给baseModel中的字段指明编号
*/

func main() {
	//wd, _ := os.Getwd()
	//os.Args = []string{"", wd}
	if len(os.Args) != 2 {
		fmt.Println("参数错误 eg：exe 项目根路径")
		return
	}
	wdDir, err := filepath.Abs(os.Args[1])
	if err != nil {
		panic(err)
	}
	modelDir := fmt.Sprintf("%s/pkg/services/data/model", wdDir)
	outputDir := fmt.Sprintf("%s/pkg/services/data/model/enum", wdDir)

	modelFiles := ReadAll(modelDir)

	// 去除空值
	utils.SliceRemove(&modelFiles, func(i int) bool {
		return modelFiles[i].models == nil
	})

	// 取model
	var models []*modelInfo
	for _, modelFile := range modelFiles {
		// -----------
		// 顺便做个规范检查，不允许一个model文件中定义多个model (虽然本脚本支持这么做)
		//if len(modelFile.models) > 1 && modelFile.fileName != "sample.go" {
		//	panic(fmt.Sprintf("文件 %s 不符合开发规范：不应当在一个model文件中定义多个model", modelFile.fileName))
		//}
		// -----------
		models = append(models, modelFile.models...)
	}

	// 写model字段枚举
	WriteFieldsEnums(models, outputDir)

	// 写model枚举和工厂
	WriteModelsEnums(models, outputDir)

	// 拓展model功能
	ExpandModelFun(modelFiles, modelDir)

}

// flag
const (
	ModelSign                   = "@Model"
	ModelSign_Attr_EnumPrefix   = "EnumPrefix"
	ModelSign_Attr_EnumCodeIota = "EnumCodeIota"
	ModelSign_Attr_WithRedis    = "WithRedis"
	FieldSign                   = "@Field"
	FieldSign_Attr_Code         = "Code"
	FieldSign_Attr_Comment      = "Comment"
	FieldSign_Attr_Unmodifiable = "Unmodifiable"
	FieldSigns                  = "@Fields"
	FieldSign_Attr_Codes        = "Codes"
)

type modelFile struct {
	fileName string
	models   []*modelInfo
}

type modelInfo struct {
	fields     []*fieldInfo
	name       string
	enumPrefix string
	withRedis  bool
}

type fieldInfo struct {
	goFieldName  string
	dbFieldName  string
	comment      string
	code         int32  // 字段代码
	fieldType    string // 字段类型
	isModifiable bool   // 是否是可修改的
}

//  ReadAll
func ReadAll(modelDir string) (modelFiles []*modelFile) {
	files, err := ioutil.ReadDir(modelDir)
	if err != nil {
		panic(fmt.Sprintf("model目录读取错误\n%v", err))
	}
	for _, file := range files {
		fileName := file.Name()
		if !strings.HasSuffix(fileName, ".go") {
			continue
		}
		bs, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", modelDir, fileName))
		if err != nil {
			panic(fmt.Sprintf("%s文件读取错误\n%v", fileName, err))
		}
		lines := strings.Split(string(bs), "\n")
		for i, line := range lines {
			lines[i] = strings.TrimSpace(line)
		}

		modelFile := &modelFile{fileName: fileName}
		modelFiles = append(modelFiles, modelFile)

		for i := 0; i < len(lines); i++ {
			line := lines[i]
			// 解析@ModelSign
			signData := parseSign(line, ModelSign)
			if signData == nil {
				continue
			}
			i++
			line = lines[i]

			var enumCodeIota bool
			var enumPrefix string
			_, ok := signData[ModelSign_Attr_EnumCodeIota]
			if ok {
				enumCodeIota = true
			}
			v, ok := signData[ModelSign_Attr_EnumPrefix]
			if ok {
				enumPrefix = v + "_"
			}
			_, withRedis := signData[ModelSign_Attr_WithRedis]

			modelReg := regexp.MustCompile(`type\s*(\S*)\s*struct\s*{`)
			subMatch := modelReg.FindStringSubmatch(line)
			if subMatch == nil {
				panic(fmt.Sprintf("%s格式不正确，%s的下一行格式必须是：type xxx struct {", fileName, ModelSign))
			}

			modelInfo := &modelInfo{name: subMatch[1], enumPrefix: enumPrefix, withRedis: withRedis}
			modelFile.models = append(modelFile.models, modelInfo)

			var sn int32 = 1 // 字段序号，用于EnumCodeIota
			for {
				i++
				line = lines[i]
				if line == "}" {
					break
				}

				// 添加baseModel字段
				if strings.Contains(line, "misc.BaseModel") {
					fieldInfo_id := &fieldInfo{goFieldName: "Id", dbFieldName: "id", comment: "主键", code: sn, fieldType: "int64", isModifiable: false}
					sn++
					fieldInfo_created := &fieldInfo{goFieldName: "Created", dbFieldName: "create", comment: "创建时间", code: sn, fieldType: "int64", isModifiable: false}
					sn++
					fieldInfo_updated := &fieldInfo{goFieldName: "Updated", dbFieldName: "updated", comment: "更新时间", code: sn, fieldType: "int64", isModifiable: false}
					sn++
					fieldInfo_deleted := &fieldInfo{goFieldName: "Deleted", dbFieldName: "deleted", comment: "删除时间", code: sn, fieldType: "int64", isModifiable: false}
					sn++
					// 重写codes
					if !enumCodeIota {
						signData := parseSign(line, FieldSigns)
						if signData == nil {
							panic(fmt.Sprintf("%s解析失败，文件：%s\t行数：%d\nerr：若没有指定%s，则必须标注code", FieldSigns, fileName, i+1, ModelSign_Attr_EnumCodeIota))
						}
						codes, _ := signData[FieldSign_Attr_Codes]
						split := strings.Split(codes, ",")
						if len(split) != 4 {
							panic(fmt.Sprintf("%s解析失败，文件：%s\t行数：%d\nerr：需标注与BaseModel中的字段相同数量的code", FieldSigns, fileName, i+1))
						}
						fieldInfo_id.code, _ = utils.ParseInt32(split[0])
						fieldInfo_created.code, _ = utils.ParseInt32(split[1])
						fieldInfo_updated.code, _ = utils.ParseInt32(split[2])
						fieldInfo_deleted.code, _ = utils.ParseInt32(split[3])
					}
					modelInfo.fields = append(modelInfo.fields, fieldInfo_id, fieldInfo_created, fieldInfo_updated, fieldInfo_deleted)
					continue
				}

				// 解析字段名
				fieldReg := regexp.MustCompile(`(\S*)\s*(\S*).*gorm:"(.*)"`)
				subMatch := fieldReg.FindStringSubmatch(line)
				if subMatch == nil {
					continue
				}
				goFieldName := subMatch[1]
				fieldType := subMatch[2]
				gormTag := subMatch[3]

				// 解析数据库&缓存字段名和字段说明
				var dbFieldName, comment string
				dbFieldName = utils.SnakeCase(subMatch[1]) // 默认为结构体字段的蛇形命名
				tagData := parseGormTag(gormTag)
				if tagData != nil { // 数据库字段，若有指定则用指定的
					column, ok := tagData["column"]
					if ok {
						dbFieldName = column
					}
					comment = tagData["comment"]
				}

				// 赋值code
				var code int32
				if enumCodeIota {
					code = sn
				}

				modifiable := true
				// 解析@Field (此标记中的comment和code会覆盖先前的)
				signData := parseSign(line, FieldSign)
				if signData != nil {
					v, ok := signData[FieldSign_Attr_Code]
					if ok {
						code, err = utils.ParseInt32(v)
						if err != nil {
							panic(fmt.Sprintf("%s解析失败，文件：%s\t行数：%d\nerr:%v\n", FieldSign, fileName, i+1, err))
						}
					} else if !enumCodeIota {
						panic(fmt.Sprintf("%s解析失败，文件：%s\t行数：%d\nerr:若没有标记%s，则必须指明字段的code", FieldSign, fileName, i+1, ModelSign_Attr_EnumCodeIota))
					}
					v, ok = signData[FieldSign_Attr_Comment]
					if ok {
						comment = v
					}
					_, ok = signData[FieldSign_Attr_Unmodifiable]
					if ok {
						modifiable = false
					}
				} else if !enumCodeIota {
					panic(fmt.Sprintf("%s解析失败，文件：%s\t行数：%d\nerr:若没有标记%s，则必须指明字段的code", FieldSign, fileName, i+1, ModelSign_Attr_EnumCodeIota))
				}

				fieldInfo := &fieldInfo{goFieldName: goFieldName, dbFieldName: dbFieldName, comment: comment, code: code, fieldType: fieldType, isModifiable: modifiable}
				modelInfo.fields = append(modelInfo.fields, fieldInfo)
				sn++
			}
		}
	}
	return modelFiles
}

//  WriteFieldsEnums
func WriteFieldsEnums(models []*modelInfo, outputDir string) {
	for _, model := range models {
		fileName := utils.SnakeCase(model.name)
		buffer := bytes.Buffer{}
		buffer.WriteString(fmt.Sprintf("package %s_e\n\n", fileName))
		buffer.WriteString(fmt.Sprintf("/**\n@DESC: 自动生成的model字段枚举\n*/\n\n"))

		// 定义别名
		buffer.WriteString("// 别名，便于阅读\n")
		buffer.WriteString(fmt.Sprintf("// (当某些参数列表要使用本文件的枚举时，可以定义为 %s_e.Enum 类型，能够直观地知道自己需要传该文件中的枚举作为参数)\n", fileName))
		buffer.WriteString("type Enum = int32\n")

		// 将函数定义在靠上的位置
		// 定义函数-model是否存在于redis中
		buffer.WriteString("// model是否存在于redis中\n")
		buffer.WriteString("func IsWithRedis() bool {\n")
		buffer.WriteString(fmt.Sprintf("\treturn %s\n}\n", utils.If3(model.withRedis, "true", "false")))
		// 定义函数-字段是否存在于数据库中
		buffer.WriteString("// 字段是否存在于数据库中\n")
		buffer.WriteString("func IsInDb(code int32) (ok bool) {\n")
		buffer.WriteString("\t_, ok = _CodeToFieldName[code]\n")
		buffer.WriteString("\treturn\n}\n")
		// 定义函数-枚举转数据库&缓存字段名
		buffer.WriteString("// 枚举转数据库&缓存字段名\n")
		buffer.WriteString("func CodeToFieldName(code int32) (fieldName string) {\n")
		buffer.WriteString("\treturn _CodeToFieldName[code]\n}\n")
		// 定义函数-数据库&缓存字段名转枚举
		buffer.WriteString("// 数据库&缓存字段名转枚举\n")
		buffer.WriteString("func FieldNameToCode(fieldName string) (code int32) {\n")
		buffer.WriteString("\treturn _FieldNameToCode[fieldName]\n}\n")
		// 定义函数-获取字段的类型
		buffer.WriteString("// 获取字段类型\n")
		buffer.WriteString("func Type(code int32) (t string) {\n")
		buffer.WriteString("\treturn _FieldType[code]\n}\n")
		// 定义函数-获取字段是否可以被修改
		buffer.WriteString("// 获取字段是否可被修改\n")
		buffer.WriteString("func IsModifiable(code int32) (ok bool) {\n")
		buffer.WriteString("\treturn _Modifiable[code]\n}\n")
		// 定义函数-获取所有字段枚举和字段名
		buffer.WriteString("// 获取所有字段枚举和字段名\n")
		buffer.WriteString("func AllField() map[int32]string {\n")
		buffer.WriteString("\treturn _CodeToFieldName\n}\n")

		// 定义枚举code
		buffer.WriteString(fmt.Sprintf("// %s字段枚举\n", model.name))
		buffer.WriteString("const (\n")
		for _, v := range model.fields {
			buffer.WriteString(fmt.Sprintf("\t%s\tint32 =%d\t// %s\n", model.enumPrefix+v.goFieldName, v.code, v.comment))
		}
		buffer.WriteString(")\n\n")

		// 定义字段枚举 和 数据库&缓存字段名 的映射
		buffer.WriteString(fmt.Sprintf("// %s字段枚举 和 数据库&缓存字段名 的映射\n", model.name))
		buffer.WriteString("var _CodeToFieldName = map[int32]string{\n")
		for _, v := range model.fields {
			buffer.WriteString(fmt.Sprintf("\t%s: \"%s\",\n", model.enumPrefix+v.goFieldName, v.dbFieldName))
		}
		buffer.WriteString("}\n\n")

		// 定义数据库&缓存字段名 和 字段枚举 的映射
		buffer.WriteString(fmt.Sprintf("// %s数据库&缓存字段名 和 字段枚举 的映射\n", model.name))
		buffer.WriteString("var _FieldNameToCode = map[string]int32{\n")
		for _, v := range model.fields {
			buffer.WriteString(fmt.Sprintf("\t\"%s\": %s,\n", v.dbFieldName, model.enumPrefix+v.goFieldName))
		}
		buffer.WriteString("}\n\n")

		// 定义枚举 和 字段类型 的映射
		buffer.WriteString(fmt.Sprintf("// %s字段枚举 和 字段类型 的映射\n", model.name))
		buffer.WriteString("var _FieldType = map[int32]string{\n")
		for _, v := range model.fields {
			if v.fieldType != "" {
				buffer.WriteString(fmt.Sprintf("\t%s: \"%s\",\n", model.enumPrefix+v.goFieldName, v.fieldType))
			}
		}
		buffer.WriteString("}\n\n")

		// 定义枚举 和 是否可被修改 的映射
		buffer.WriteString(fmt.Sprintf("// %s字段枚举 和 是否可被修改 的映射\n", model.name))
		buffer.WriteString("var _Modifiable = map[int32]bool{\n")
		for _, v := range model.fields {
			buffer.WriteString(fmt.Sprintf("\t%s: %s,\n", model.enumPrefix+v.goFieldName, utils.If3(v.isModifiable, "true", "false")))
		}
		buffer.WriteString("}\n\n")

		dirPath := fmt.Sprintf("%s/%s_e", outputDir, fileName)
		err := utils.MkDirAll(dirPath)
		if err != nil {
			panic(fmt.Sprintf("目录创建错误:%s\n%v", dirPath, err))
		}

		filePath := fmt.Sprintf("%s/%s_e.go", dirPath, fileName)
		err = ioutil.WriteFile(filePath, buffer.Bytes(), fs.ModePerm)
		if err != nil {
			panic(fmt.Sprintf("文件写入错误：%s\n%v", filePath, err))
		}
		err = exec.Command("gofmt", "-w", filePath).Run()
		if err != nil {
			panic(fmt.Sprintf("格式化错误：%s\n%v", filePath, err))
		}
	}
}

//  WriteModelsEnums
func WriteModelsEnums(models []*modelInfo, outputDir string) {
	buffer := bytes.Buffer{}
	fileName := "model_e"
	buffer.WriteString(fmt.Sprintf("package %s\n\n", fileName))
	// import
	buffer.WriteString("import (\n\"hm/pkg/services/data/model\"\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t\"hm/pkg/services/data/model/enum/%s_e\"\n", utils.SnakeCase(modelInfo.name)))
	}
	buffer.WriteString(")\n\n")

	// note
	buffer.WriteString(fmt.Sprintf("/**\n@DESC: 自动生成的model枚举\n*/\n\n"))

	// type
	buffer.WriteString("type Model = uint8\n\n")

	// const
	buffer.WriteString("const(\n")
	for i, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s Model = %d\n", modelInfo.name, i+1))
	}
	buffer.WriteString(")\n\n")

	// func
	buffer.WriteString("func ModelName(e Model) string {\n\treturn _ModelName[e]\n}\n\n")
	// factory func
	buffer.WriteString("func NewModelPtr(e Model) (model interface{}) {\n\treturn _ModelPtrFac[e]()\n}\n\n")
	buffer.WriteString("func NewModel(e Model) (model interface{}) {\n\treturn _ModelFac[e]()\n}\n\n")
	buffer.WriteString("func NewModelPtrSlice(e Model) (model interface{}) {\n\treturn _ModelPtrSliceFac[e]()\n}\n\n")
	buffer.WriteString("func NewModelPtrSlicePtr(e Model) (model interface{}) {\n\treturn _ModelPtrSlicePtrFac[e]()\n}\n\n")
	// enum func
	buffer.WriteString("func IsWithRedis(e Model) bool{\n\treturn Func_IsWithRedis(e)()\n}\n\n")
	buffer.WriteString("func IsInDb(e Model,code int32) bool {\n\treturn Func_IsInDb(e)(code)\n}\n\n")
	buffer.WriteString("func IsModifiable(e Model,code int32) bool {\n\treturn Func_IsModifiable(e)(code)\n}\n\n")
	buffer.WriteString("func Type(e Model,code int32) string {\n\treturn Func_Type(e)(code)\n}\n\n")
	buffer.WriteString("func IsInt32(e Model,code int32) bool {\n\treturn Type(e, code) == \"int32\"\n}\n\n")
	buffer.WriteString("func ToFieldName(e Model,code int32) (fieldName string) {\n\treturn Func_CodeToFieldName(e)(code)\n}\n\n")
	buffer.WriteString("func ToCode(e Model,fieldName string) (code int32) {\n\treturn Func_FieldNameToCode(e)(fieldName)\n}\n\n")
	buffer.WriteString("func AllField(e Model) map[int32]string {\n\treturn Func_AllField(e)()\n}\n\n")
	// origin func
	buffer.WriteString("func Func_IsWithRedis(e Model) func() (ok bool) {\n\treturn _Func_IsWithRedis[e]\n}\n\n")
	buffer.WriteString("func Func_IsInDb(e Model) func(code int32) (ok bool) {\n\treturn _Func_IsInDb[e]\n}\n\n")
	buffer.WriteString("func Func_IsModifiable(e Model) func(code int32) (ok bool) {\n\treturn _Func_IsModifiable[e]\n}\n\n")
	buffer.WriteString("func Func_Type(e Model) func(code int32) (t string) {\n\treturn _Func_Type[e]\n}\n\n")
	buffer.WriteString("func Func_CodeToFieldName(e Model) func(code int32) (fieldName string) {\n\treturn _Func_CodeToFieldName[e]\n}\n\n")
	buffer.WriteString("func Func_FieldNameToCode(e Model) func(fieldName string) (code int32) {\n\treturn _Func_FieldNameToCode[e]\n}\n\n")
	buffer.WriteString("func Func_AllField(e Model) func() map[int32]string {\n\treturn _Func_AllField[e]\n}\n\n")

	// _ModelName
	buffer.WriteString("var _ModelName = map[Model]string{\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s:\"%s\",\n", modelInfo.name, modelInfo.name))
	}
	buffer.WriteString("}\n\n")

	// _Func_IsWithRedis
	buffer.WriteString("var _Func_IsWithRedis = map[Model]func()(ok bool){\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: %s_e.IsWithRedis,\n", modelInfo.name, utils.SnakeCase(modelInfo.name)))
	}
	buffer.WriteString("}\n\n")

	// _Func_IsInDb
	buffer.WriteString("var _Func_IsInDb = map[Model]func(code int32) (ok bool){\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: %s_e.IsInDb,\n", modelInfo.name, utils.SnakeCase(modelInfo.name)))
	}
	buffer.WriteString("}\n\n")

	// _Func_IsModifiable
	buffer.WriteString("var _Func_IsModifiable = map[Model]func(code int32) (ok bool){\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: %s_e.IsModifiable,\n", modelInfo.name, utils.SnakeCase(modelInfo.name)))
	}
	buffer.WriteString("}\n\n")

	// _Func_Type
	buffer.WriteString("var _Func_Type = map[Model]func(code int32) (t string){\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: %s_e.Type,\n", modelInfo.name, utils.SnakeCase(modelInfo.name)))
	}
	buffer.WriteString("}\n\n")

	// _Func_CodeToFieldName
	buffer.WriteString("var _Func_CodeToFieldName = map[Model]func(code int32) (fieldName string){\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: %s_e.CodeToFieldName,\n", modelInfo.name, utils.SnakeCase(modelInfo.name)))
	}
	buffer.WriteString("}\n\n")

	// _Func_FieldNameToCode
	buffer.WriteString("var _Func_FieldNameToCode = map[Model]func(fieldName string) (code int32){\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: %s_e.FieldNameToCode,\n", modelInfo.name, utils.SnakeCase(modelInfo.name)))
	}
	buffer.WriteString("}\n\n")

	// _Func_AllField
	buffer.WriteString("var _Func_AllField = map[Model]func() map[int32]string{\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: %s_e.AllField,\n", modelInfo.name, utils.SnakeCase(modelInfo.name)))
	}
	buffer.WriteString("}\n\n")

	// _ModelFac
	buffer.WriteString("var _ModelFac = map[Model]func() interface{}{\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: func() interface{} {\n", modelInfo.name))
		buffer.WriteString(fmt.Sprintf("\t\treturn model.%s{}\n", modelInfo.name))
		buffer.WriteString(fmt.Sprintf("},\n"))
	}
	buffer.WriteString("}\n\n")

	// _ModelPtrFac
	buffer.WriteString("var _ModelPtrFac = map[Model]func() interface{}{\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: func() interface{} {\n", modelInfo.name))
		buffer.WriteString(fmt.Sprintf("\t\treturn &model.%s{}\n", modelInfo.name))
		buffer.WriteString(fmt.Sprintf("},\n"))
	}
	buffer.WriteString("}\n\n")

	// _ModelPtrSliceFac
	buffer.WriteString("var _ModelPtrSliceFac = map[Model]func() interface{}{\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: func() interface{} {\n", modelInfo.name))
		buffer.WriteString(fmt.Sprintf("\t\treturn []model.%s{}\n", modelInfo.name))
		buffer.WriteString(fmt.Sprintf("},\n"))
	}
	buffer.WriteString("}\n\n")

	// _ModelPtrSlicePtrFac
	buffer.WriteString("var _ModelPtrSlicePtrFac = map[Model]func() interface{}{\n")
	for _, modelInfo := range models {
		buffer.WriteString(fmt.Sprintf("\t%s: func() interface{} {\n", modelInfo.name))
		buffer.WriteString(fmt.Sprintf("\t\treturn &[]*model.%s{}\n", modelInfo.name))
		buffer.WriteString(fmt.Sprintf("},\n"))
	}
	buffer.WriteString("}\n\n")

	dirPath := fmt.Sprintf("%s/%s", outputDir, fileName)
	err := utils.MkDirAll(dirPath)
	if err != nil {
		panic(fmt.Sprintf("目录创建错误:%s\n%v", dirPath, err))
	}
	filePath := fmt.Sprintf("%s/%s.go", dirPath, fileName)
	err = ioutil.WriteFile(filePath, buffer.Bytes(), fs.ModePerm)
	if err != nil {
		panic(fmt.Sprintf("文件写入错误：%s\n%v", filePath, err))
	}
	err = exec.Command("gofmt", "-w", filePath).Run()
	if err != nil {
		panic(fmt.Sprintf("格式化错误：%s\n%v", filePath, err))
	}
}

// parseSign
//  解析@标记
func parseSign(content, sign string) (signData map[string]string) {
	if !strings.Contains(content, sign) {
		return nil
	}
	signData = make(map[string]string)
	subMatch := regexp.MustCompile(fmt.Sprintf(`%s\((.*)\)`, sign)).FindStringSubmatch(content)
	if subMatch != nil {
		for _, v := range strings.Split(subMatch[1], ";") {
			split := strings.Split(v, "=")
			var key, value string
			key = strings.TrimSpace(split[0])
			if len(split) == 2 {
				value = strings.TrimSpace(split[1])
			}
			signData[key] = value
		}
	}
	return
}

// parseGormTag
//  解析gorm标签
func parseGormTag(content string) (tagData map[string]string) {
	if content == "-" {
		return nil
	}
	tagData = make(map[string]string)
	for _, v := range strings.Split(content, ";") {
		split := strings.Split(v, ":")
		var key, value string
		key = strings.TrimSpace(split[0])
		if len(split) == 2 {
			value = strings.TrimSpace(split[1])
		}
		tagData[key] = value
	}
	return
}

const GetByEnum = "GetByEnum"

func ExpandModelFun(modelFiles []*modelFile, modelDir string) {
	for _, modelFile := range modelFiles {
		origContent, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", modelDir, modelFile.fileName))
		if err != nil {
			panic(fmt.Sprintf("model文件读取错误\n%v", err))
		}
		var models = make([]*modelInfo, len(modelFile.models))
		copy(models, modelFile.models)

		newContent := bytes.Buffer{}
		lines := strings.Split(string(origContent), "\n")

		once := &sync.Once{}
	Continue:
		for i := 0; i < len(lines); i++ {
			line := strings.TrimSpace(lines[i])
			// 补充import
			if strings.HasPrefix(line, "import") {
				once.Do(func() {
					if strings.Contains(line, "(") {
						newContent.WriteString(fmt.Sprintf("%s\n", line))
						for _, model := range models {
							modelPkg := fmt.Sprintf("hm/pkg/services/data/model/enum/%s_e", utils.SnakeCase(model.name))
							newContent.WriteString(fmt.Sprintf("\"%s\"\n", modelPkg)) // 直接写，最后通过go -fmt格式化时，重复的包会被清除
						}
						i++
						line = strings.TrimSpace(lines[i])
					}
					// todo 暂时只处理 `import (` 的情况，单行import嫌麻烦不想写了
					//else {
					//	newContent.WriteString("import (")
					//	for _, model := range models {
					//		modelPkg := fmt.Sprintf("hm/pkg/services/data/model/enum/%s_e", utils.SnakeCase(model.name))
					//		newContent.WriteString(fmt.Sprintf("\"%s\"\n", modelPkg))
					//	}
					//	newContent.WriteString("import )")
					//}
				})
			}

			// 替换GetByEnum
			for _, model := range models {
				if regexp.MustCompile(fmt.Sprintf("func.*%s.*%s", model.name, GetByEnum)).MatchString(line) {
					receiverVarName := FindReceiverVarName(lines, model.name)
					// 写新函数
					newContent.WriteString(fmt.Sprintf("func (%s *%s) %s(info int32) (res interface{}) {\n", receiverVarName, model.name, GetByEnum))
					newContent.WriteString("\tswitch info {\n")
					for i := 0; i < len(model.fields); i++ {
						field := model.fields[i]
						newContent.WriteString(fmt.Sprintf("\tcase %s_e.%s:\n", utils.SnakeCase(model.name), model.enumPrefix+field.goFieldName))
						newContent.WriteString(fmt.Sprintf("\t\tres = %s.%s\n", receiverVarName, field.goFieldName))
					}
					newContent.WriteString("}\n\treturn res\n}\n\n")
					// 删除
					utils.SliceRemove(&models, func(k int) bool {
						return models[k].name == model.name
					})
					// 跳过原函数
					for {
						i++
						if strings.HasPrefix(lines[i], "}") && strings.Contains(lines[i-1], "return") {
							continue Continue
						}
					}
				}
			}
			newContent.WriteString(fmt.Sprintf("%s\n", line))
		}

		// 如果始终没有某个 GetByEnum 函数，则新增到最后
		for _, model := range models {
			receiverVarName := FindReceiverVarName(lines, model.name)
			newContent.WriteString(fmt.Sprintf("func (%s *%s) %s(info int32) (res interface{}) {\n", receiverVarName, model.name, GetByEnum))
			newContent.WriteString("\tswitch info {\n")
			for i := 0; i < len(model.fields); i++ {
				field := model.fields[i]
				newContent.WriteString(fmt.Sprintf("\tcase %s_e.%s:\n", utils.SnakeCase(model.name), model.enumPrefix+field.goFieldName))
				newContent.WriteString(fmt.Sprintf("\t\tres = %s.%s\n", receiverVarName, field.goFieldName))
			}
			newContent.WriteString("}\n\treturn res\n}\n\n")
		}

		// 覆盖写原文件
		filePath := fmt.Sprintf("%s/%s", modelDir, modelFile.fileName)
		err = ioutil.WriteFile(filePath, newContent.Bytes(), fs.ModePerm)
		if err != nil {
			panic(fmt.Sprintf("文件写入错误：%s\n%v", filePath, err))
		}
		err = exec.Command("gofmt", "-w", filePath).Run()
		if err != nil {
			panic(fmt.Sprintf("格式化错误：%s\n%v", filePath, err))
		}
	}
}

// 寻找receiver变量名
func FindReceiverVarName(fileContent []string, modelName string) (varName string) {
	for _, line := range fileContent {
		line = strings.TrimSpace(line)
		submatch := regexp.MustCompile(fmt.Sprintf("func\\s*\\((\\S*).*%s", modelName)).FindStringSubmatch(line)
		if submatch != nil {
			varName = submatch[1]
			break
		}
	}
	if varName == "" {
		varName = "unnamed"
	}
	return
}
