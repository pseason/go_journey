package main

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"hm/pkg/ai/inc"
	"os"
	"path/filepath"
	"reflect"
)

type remoteAiNodeResponse struct {
	Version string          `json:"version"`
	Nodes   []*remoteAiNode `json:"nodes"`
}

type remoteAiNode struct {
	Name  string              `json:"name"`
	Note  string              `json:"note"`
	Type  string              `json:"type"`
	Attrs []*remoteAiNodeAttr `json:"attrs"`
}

type remoteAiNodeAttr struct {
	Field   string `json:"field"`
	Type    string `json:"type"`
	Note    string `json:"note"`
	Default string `json:"default"`
}

// makeRemoteNode 生成远程节点，用于AI可视化编辑器远程获取
func makeRemoteNode(ext *inc.BtStructOptMaps, version string) (err error) {
	wd, err := os.Getwd()
	if err != nil {
		return
	}
	rootDir, err := filepath.Abs(wd + "/output/ai")
	if err != nil {
		return
	}
	if ext.Len() > 0 {
		nodes := make([]*remoteAiNode, 0, ext.Len())
		ext.Each(func(key string, t reflect.Type) {
			fmt.Println(t)
			node := &remoteAiNode{
				Attrs: make([]*remoteAiNodeAttr, 0, t.NumField()),
				Type:  t.Field(0).Name,
				Name:  t.Name(),
				Note:  t.Field(0).Tag.Get("note"),
			}
			for i := 1; i < t.NumField(); i++ {
				field := t.Field(i)
				node.Attrs = append(node.Attrs, &remoteAiNodeAttr{
					Field:   field.Name,
					Type:    field.Type.Name(),
					Note:    field.Tag.Get("note"),
					Default: field.Tag.Get("default"),
				})
			}
			nodes = append(nodes, node)
		})
		err = writeNodesToFile(&remoteAiNodeResponse{
			version, nodes,
		}, rootDir, "nodes.json")
	}
	return
}

func writeNodesToFile(data *remoteAiNodeResponse, rootDir, filename string) (err error) {
	marshal, err := jsoniter.MarshalIndent(data, "", "    ")
	if err != nil {
		return
	}
	err = os.WriteFile(filepath.Join(rootDir, filename), marshal, os.ModePerm)
	return
}
