package main

import (
	"fmt"
	"hm/pkg/ai/inc"
	"hm/pkg/ai/load"
	"os"
)

func main() {
	version := "1.0"
	fmt.Println(os.Args)
	if len(os.Args) > 1 {
		version = os.Args[len(os.Args)-1]
	}
	structOptMaps := inc.NewBtStructOptMaps()
	aiLoad := load.NewAiLoad()
	aiLoad.InitBevExtMaps(structOptMaps)
	err := makeRemoteNode(structOptMaps, version)
	if err != nil {
		panic(err)
	}
}
