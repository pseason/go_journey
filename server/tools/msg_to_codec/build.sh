# gameMsg
#go build -o ./game/game_msg_to_codec game/main.go
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o ./game/game_msg_to_codec.exe game/main.go
CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -o ./game/game_msg_to_codec_darwin game/main.go
#CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./game/game_msg_to_codec_linux game/main.go

# servicesMsg
#go build -o ./services_data/services_data_msg_to_codec services_data/main.go
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o ./services_data/services_data_msg_to_codec.exe services_data/main.go
CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -o ./services_data/services_data_msg_to_codec_darwin services_data/main.go
#CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./services_data/services_data_msg_to_codec_linux services_data/main.go

# spaceMsg
