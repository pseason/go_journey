package main

import (
	"bytes"
	"fmt"
	"hm/pkg/misc/utils"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

/**
根据proto文件生成供项目使用的消息码和消息编解码映射
*/

// 排除读取
var excludeFiles = []string{"components.proto"}

func main() {
	//wd, _ := os.Getwd()
	//os.Args = []string{``, wd + `/pkg/game/msg_gm/pb`, wd + `/pkg/game/msg_gm`} // test
	if len(os.Args) != 3 {
		fmt.Println("参数错误 eg：exe 输入路径(proto文件路径) 输出路径")
		return
	}
	inputPath, err := filepath.Abs(os.Args[1]) // filepath.Abs可以接收带有相对路径符号的路径，并返回它的绝对路径；比如d:a/b1/../b2 -> d:a/b2
	if err != nil {
		panic(err)
	}
	outputPath, err := filepath.Abs(os.Args[2])
	if err != nil {
		panic(err)
	}

	files, err := ioutil.ReadDir(inputPath)
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		dir := file.IsDir()
		if !dir && strings.HasSuffix(file.Name(), ".proto") && !IsExclude(file.Name()) {
			generatedCodecByProto(outputPath, file.Name(), inputPath+"/"+file.Name())
		}
	}
	if len(initCodecFuncs) != 0 {
		generatedCodecInitFun(outputPath)
	}
}

var initCodecFuncs []string

const (
	MessageCodeFlag = "@MessageCode"
)

func IsExclude(fileName string) bool {
	for _, v := range excludeFiles {
		if v == fileName {
			return true
		}
	}
	return false
}

type messageInfo struct {
	Code   int
	Name   string
	Req    string
	Res    string
	Notice string
	Desc   string
}

func generatedCodecByProto(outputPath string, fileName string, filePath string) {
	var messageInfos = make(map[int]*messageInfo)
	b, _ := ioutil.ReadFile(filePath)
	text := string(b)
	lines := strings.Split(text, "\n")
	fileName = strings.TrimSuffix(fileName, ".proto")
	//var serviceEnum string

	max := len(lines)
	for i := 0; i < max; i++ {
		line := lines[i]

		//解析消息号
		subMatch := regexp.MustCompile(fmt.Sprintf("^//\\s*%s\\s*=\\s*(\\d+)", MessageCodeFlag)).FindStringSubmatch(line)
		if subMatch == nil {
			continue
		}

		var code int
		if code, _ = strconv.Atoi(subMatch[1]); code != 0 {
			{
				info, exist := messageInfos[code]
				if !exist {
					info = &messageInfo{Code: code}
					messageInfos[code] = info
				}
			}
		} else {
			panic(fmt.Sprintf("%s.proto文件读取错误，error：%s不可为0", fileName, MessageCodeFlag))
		}
		descReg := regexp.MustCompile("[0-9]+(.*)")
		descMatch := descReg.FindStringSubmatch(line)
		desc := strings.TrimSpace(descMatch[1])

		//解析消息名
		var nextLine string
		i++
		if i < max {
			nextLine = strings.TrimSpace(lines[i])
		}
		if strings.HasPrefix(nextLine, "message") {
			messageNameReg := regexp.MustCompile(`message(.*)\{`)
			messageNameMatch := messageNameReg.FindStringSubmatch(nextLine)
			messageName := strings.TrimSpace(messageNameMatch[1])

			info := messageInfos[code]
			if strings.HasPrefix(messageName, "Req") {
				info.Name = messageName[3:]
				info.Req = messageName
				info.Desc = fmt.Sprintf("%sReq：%s；", info.Desc, desc)
			} else if strings.HasPrefix(messageName, "Res") {
				info.Name = messageName[3:]
				info.Res = messageName
				info.Desc = fmt.Sprintf("%sRes：%s；", info.Desc, desc)
			} else if strings.HasPrefix(messageName, "Notice") {
				info.Name = messageName
				info.Notice = messageName
				info.Desc = fmt.Sprintf("%sNotice：%s；", info.Desc, desc)
			}
		}
	}
	// 无有效内容的不生成
	if len(messageInfos) == 0 {
		return
	}

	// 转为slice，方便排序
	infoSlice := make([]*messageInfo, 0, len(messageInfos))
	for _, v := range messageInfos {
		infoSlice = append(infoSlice, v)
	}
	// 排序，按顺序生成
	sort.Slice(infoSlice, func(i, j int) bool {
		return infoSlice[i].Code < infoSlice[j].Code
	})

	// 写文件头
	buffer := &bytes.Buffer{}
	_, pkgName := filepath.Split(outputPath)
	buffer.WriteString(fmt.Sprintf("package %s\n\n", pkgName))
	buffer.WriteString("import (\n\t \"95eh.com/eg/intfc\"\n)\n\n")
	//写消息号
	buffer.WriteString("const (\n")
	for _, v := range infoSlice {
		buffer.WriteString(fmt.Sprintf("\tCd%s MsgCode = %d // %s\n", v.Name, v.Code, v.Desc))
	}
	buffer.WriteString(")\n")
	//写绑定函数
	funcName := fmt.Sprintf("InitCodecFor%s", utils.BigHump(fileName))
	initCodecFuncs = append(initCodecFuncs, funcName)
	buffer.WriteString(fmt.Sprintf("func %s(userLogic intfc.IMUserLogic) {\n", funcName))
	for _, info := range infoSlice {
		var reqStruct, resStruct string
		if info.Notice != "" {
			reqStruct = "nil"
			resStruct = fmt.Sprintf("func() interface{} {\nreturn &%s{}\n}", info.Notice)
		} else {
			if info.Req != "" {
				reqStruct = fmt.Sprintf("func() interface{} {\nreturn &%s{}\n}", info.Req)
			} else {
				reqStruct = "nil"
			}
			if info.Res != "" {
				resStruct = fmt.Sprintf("func() interface{} {\nreturn &%s{}\n}", info.Res)
			} else {
				resStruct = "nil"
			}
		}
		buffer.WriteString(fmt.Sprintf("userLogic.BindCoderFac(Cd%s,\n %s,\n%s)\n", info.Name, reqStruct, resStruct))
	}
	buffer.WriteString("}\n")
	fullPath := fmt.Sprintf("%s/%s_codec.go", outputPath, fileName)
	err := ioutil.WriteFile(fullPath, []byte(buffer.String()), os.ModePerm)
	if err != nil {
		fmt.Printf("写入文件失败：%s : %v\n", fullPath, err)
	}
	exec.Command("gofmt", "-w", fullPath).Run()
	//fmt.Printf("\t- %s_codec.go bingo!\n", fileName)
}

func generatedCodecInitFun(outputPath string) {
	buffer := &bytes.Buffer{}
	_, pkgName := filepath.Split(outputPath)
	buffer.WriteString(fmt.Sprintf("package %s\n\n", pkgName))
	buffer.WriteString("import (\n\t \"95eh.com/eg/intfc\"\n)\n\n")
	buffer.WriteString("// 绑定与客户端通信的消息协议号和消息结构体的映射\n")
	//写绑定方法
	buffer.WriteString(fmt.Sprintf("func InitClientCodec(userLogic intfc.IMUserLogic) {\n"))
	for _, v := range initCodecFuncs {
		buffer.WriteString(fmt.Sprintf("%s(userLogic)\n", v))
	}
	buffer.WriteString("}\n")
	fullPath := fmt.Sprintf("%s/init_codec.go", outputPath)
	err := ioutil.WriteFile(fullPath, []byte(buffer.String()), os.ModePerm)
	if err != nil {
		fmt.Printf("写入文件失败：%s : %v\n", fullPath, err)
	}
	exec.Command("gofmt", "-w", fullPath).Run()
	//fmt.Printf("\t- init_codec.go bingo!\n")
}
