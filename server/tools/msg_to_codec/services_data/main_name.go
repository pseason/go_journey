package main
//
//import (
//	"bytes"
//	"fmt"
//	"io/ioutil"
//	"os"
//	"os/exec"
//	"path/filepath"
//	"regexp"
//	"strings"
//)
//
///**
//根据消息结构体生成供项目使用的消息码和消息编解码映射
//
//todo 不需要指定code号，而是以`ReqXxx`、`ResXxx`、`EveXxx`中的`Xxx`作为消息的唯一标识，用作生成的code枚举名，相同`Xxx`的消息会使用同一个自动生成的code号(一般也就是成对的`ReqXxx`和`ResXxx`，和成单出现的`EveXxx`)
//
//*/
//
//// 排除读取
//var excludeFiles = []string{"generate.bat", "common.go", "alias.go", "README.md"}
//
//func main() {
//	//os.Args = []string{``, `D:/Develop/WorkSpaceForGoLand/hm-server/pkg/services/msg_sv`, `D:/Develop/WorkSpaceForGoLand/hm-server/pkg/services/msg_sv`} // test
//	if len(os.Args) != 3 {
//		fmt.Println("参数错误 eg: exe 输入路径(msg文件路径) 输出路径")
//		return
//	}
//	inputPath, err := filepath.Abs(os.Args[1]) // filepath.Abs可以接收带有相对路径符号的路径，并返回它的绝对路径；比如d:a/b1/../b2 -> d:a/b2
//	if err != nil {
//		panic(err)
//	}
//	outputPath, err := filepath.Abs(os.Args[2])
//	if err != nil {
//		panic(err)
//	}
//
//	fmt.Println(os.Args)
//	fmt.Println(inputPath, outputPath)
//	files, err := ioutil.ReadDir(inputPath)
//	if err != nil {
//		panic(err)
//	}
//	for _, file := range files {
//		dir := file.IsDir()
//		if !dir && !strings.HasSuffix(file.Name(), "_codec.go") && !IsExclude(file.Name()) {
//			generatedCodec(outputPath, file.Name(), inputPath+"/"+file.Name())
//		}
//	}
//	if len(initCodecFuncs) != 0 {
//		generatedCodecInitFun(outputPath)
//	}
//}
//
//var serviceEnums []string
//var initCodecFuncs []string
//
//func IsExclude(fileName string) bool {
//	for _, v := range excludeFiles {
//		if v == fileName {
//			return true
//		}
//	}
//	return false
//}
//
//type messageInfo struct {
//	Name string
//	Req  string
//	Res  string
//	Eve  string
//	Desc string
//}
//
//func generatedCodec(outputPath string, fileName string, filePath string) {
//	var messageInfos = make(map[string]*messageInfo)
//	b, _ := ioutil.ReadFile(filePath)
//	text := string(b)
//	lines := strings.Split(text, "\n")
//	fileName = strings.TrimSuffix(fileName, ".go")
//	var serviceEnum string
//
//	max := len(lines)
//	for i := 0; i < max; i++ {
//		line := lines[i]
//
//		//解析服务枚举名
//		if strings.HasPrefix(strings.TrimSpace(line), "// serviceEnum") || strings.HasPrefix(strings.TrimSpace(line), "//serviceEnum") {
//			svcEnumReg := regexp.MustCompile("serviceEnum.*=(.*)")
//			serviceEnumMatch := svcEnumReg.FindStringSubmatch(line)
//			if len(serviceEnumMatch) == 2 {
//				serviceEnum = strings.TrimSpace(serviceEnumMatch[1])
//			} else {
//				panic(fmt.Sprintf("%s.go解析服务枚举名错误，正确语法：serviceEnum=xxx\n", fileName))
//			}
//			continue
//		}
//
//		//解析消息信息
//		if strings.HasPrefix(line, "type") {
//			// 解析上一行，拿注释
//			var desc string
//			lastLine := strings.TrimSpace(lines[i-1])
//			descReg := regexp.MustCompile("//(.*)")
//			descMatch := descReg.FindStringSubmatch(lastLine)
//			if len(descMatch) == 2 {
//				desc = strings.TrimSpace(descMatch[1])
//			}
//
//			messageNameReg := regexp.MustCompile("type(.*)struct.*\\{") // todo 有波浪线警告，试试``
//			messageNameMatch := messageNameReg.FindStringSubmatch(line)
//			messageName := strings.TrimSpace(messageNameMatch[1])
//
//			var name, req, res, eve string
//			if strings.HasPrefix(messageName, "Req") {
//				name = messageName[3:]
//				req = messageName
//				desc = fmt.Sprintf("Req：%s；", desc)
//			} else if strings.HasPrefix(messageName, "Res") {
//				name = messageName[3:]
//				res = messageName
//				desc = fmt.Sprintf("Res：%s；", desc)
//			} else if strings.HasPrefix(messageName, "Eve") {
//				name = messageName
//				eve = messageName
//				desc = fmt.Sprintf("Eve：%s；", desc)
//			}
//
//			info, ok := messageInfos[name]
//			if !ok {
//				info = &messageInfo{Name: name}
//				messageInfos[name] = info
//			}
//			if req != "" {
//				info.Req = req
//			}
//			if res != "" {
//				info.Res = res
//			}
//			if eve != "" {
//				info.Eve = eve
//			}
//			info.Desc = fmt.Sprintf("%s%s", info.Desc, desc)
//
//		}
//	}
//	// 没有写服务枚举的不生成
//	if serviceEnum == "" {
//		fmt.Printf("%s.go未注明服务枚举\n", fileName)
//		return
//	} else {
//		serviceEnums = append(serviceEnums, serviceEnum)
//	}
//	// 无有效内容的不生成
//	if len(messageInfos) == 0 {
//		return
//	}
//
//	// 写文件头
//	buffer := &bytes.Buffer{}
//	_, pkgName := filepath.Split(outputPath)
//	buffer.WriteString(fmt.Sprintf("package %s\n\n", pkgName))
//	buffer.WriteString("import (\n\t \"95eh.com/eg/intfc\"\n \"hm/pkg/game/svc\"\n\"95eh.com/eg/utils\"\n)\n\n")
//	//写消息号
//	buffer.WriteString("const (\n")
//	for _, v := range messageInfos {
//		buffer.WriteString(fmt.Sprintf("\tCd%s MsgCode = iota // %s\n", v.Name, v.Desc))
//	}
//	buffer.WriteString(")\n")
//	//写绑定函数
//	funcName := fmt.Sprintf("InitCodecFor%s", Capitalize(fileName))
//	initCodecFuncs = append(initCodecFuncs, funcName)
//	buffer.WriteString(fmt.Sprintf("func %s(codec intfc.ICodec) {\n", funcName))
//	for _, info := range messageInfos {
//		var reqStruct, resStruct string
//		if info.Eve != "" {
//			reqStruct = "nil"
//			resStruct = fmt.Sprintf("func() interface{} {\nreturn &%s{}\n}", info.Eve)
//		} else {
//			if info.Req != "" {
//				reqStruct = fmt.Sprintf("func() interface{} {\nreturn &%s{}\n}", info.Req)
//			} else {
//				reqStruct = "nil"
//			}
//			if info.Res != "" {
//				resStruct = fmt.Sprintf("func() interface{} {\nreturn &%s{}\n}", info.Res)
//			} else {
//				resStruct = "nil"
//			}
//		}
//		buffer.WriteString(fmt.Sprintf("codec.BindFac(utils.MergeUInt32(svc.%s, Cd%s),\n %s,\n%s)\n", serviceEnum, info.Name, reqStruct, resStruct))
//	}
//	buffer.WriteString("}\n")
//	writePath := outputPath + "/" + fileName
//	fullPath := fmt.Sprintf("%s_codec.go", writePath)
//	err := ioutil.WriteFile(fullPath, []byte(buffer.String()), os.ModePerm)
//	if err != nil {
//		fmt.Printf("写入文件失败：%s : %v\n", fullPath, err)
//	}
//	exec.Command("gofmt", "-w", fullPath).Run()
//}
//
//func generatedCodecInitFun(outputPath string) {
//	buffer := &bytes.Buffer{}
//	_, pkgName := filepath.Split(outputPath)
//	buffer.WriteString(fmt.Sprintf("package %s\n\n", pkgName))
//	buffer.WriteString("import (\n\t \"95eh.com/eg/intfc\"\n)\n\n")
//	buffer.WriteString("// 绑定与服务通信的消息协议号和消息结构体的映射\n")
//	//写绑定方法
//	buffer.WriteString(fmt.Sprintf("func InitServiceCodec(codec intfc.ICodec) {\n"))
//	for _, v := range initCodecFuncs {
//		buffer.WriteString(fmt.Sprintf("%s(codec)\n", v))
//	}
//	buffer.WriteString("}\n")
//	fullPath := fmt.Sprintf("%s/init_codec.go", outputPath)
//	err := ioutil.WriteFile(fullPath, []byte(buffer.String()), os.ModePerm)
//	if err != nil {
//		fmt.Printf("写入文件失败：%s : %v\n", fullPath, err)
//	}
//	exec.Command("gofmt", "-w", fullPath).Run()
//}
//
//// Capitalize 字符首字母大写
//func Capitalize(str string) string {
//	strRune := []rune(str)
//	if strRune[0] >= 97 && strRune[0] <= 122 {
//		strRune[0] -= 32 // 大小写码表相差32
//	}
//	return string(strRune)
//}
