@echo off
set DIR=%~dp0
set PBDIR=%DIR%../../pkg/game/msg_gm/pb
set OUTDIR=%DIR%../../pkg/game/msg_gm
echo PBDIR: %PBDIR%
echo OUTDIR: %OUTDIR%
echo begin generate pb.go
protoc.exe --proto_path=%PBDIR% --gogofaster_out=%OUTDIR% %PBDIR%/*.proto
protoc-go-inject-tag -input=%OUTDIR%/*.pb.go
echo finished generate pb.go