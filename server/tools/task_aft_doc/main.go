package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	wd, err := os.Getwd()
	if err != nil {
		panic(wd)
	}
	outPath := filepath.Join(wd, "/doc/任务执行参数对照.md")
	content, err := os.ReadFile(filepath.Join(wd, "/pkg/services/data/model/enum/task_e/task_aft.go"))
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(content), "\n")
	start := false
	buff := bytes.Buffer{}
	buff.WriteString("###说明\n- 常量路径：`pkg/services/data/model/enum/task_e/task_aft.go`\n")
	buff.WriteString("- `Cid`: 目标任务执行玩家ID\n")
	buff.WriteString("- `TemplateId`: 模板值或者目标任务所需判断的唯一ID（具体请参考各自Type的协议描述）\n")
	buff.WriteString("- `Num`: 发生或改变的数据（具体请参考各自Type的协议描述）\n")
	buff.WriteString("- `Success`: 本次执行是否成功，多用于竞技成功与否（具体请参考各自Type的协议描述）\n")
	buff.WriteString("> Tips: 在`BaseController`中可以直接使用通用接口`RequestTaskExecute`进行调用触发任务\n\n")
	id := 1
	for i := 0; i < len(lines); i++ {
		line := strings.TrimSpace(lines[i])
		if !start {
			if strings.HasPrefix(line, "const (") {
				start = true
			}
			continue
		}
		if line == ")" {
			break
		}
		if !strings.HasPrefix(line, "//") {
			name := strings.Split(line, " ")[0]
			buff.WriteString(fmt.Sprintf("### (#%d) %s\n", id, name))
			id++
			noteLine := lines[i-2]
			note := noteLine[strings.Index(noteLine, name)+len(name)+1:]
			buff.WriteString(fmt.Sprintf("%s\n", note))
			buff.WriteString("- `Cid` 目标任务执行玩家ID\n")
			paramLine := lines[i-1]
			if !strings.Contains(paramLine, "无参数") {
				paramLine = strings.Replace(paramLine, "//", "", 1)
				params := strings.Split(paramLine, "@")
				if len(params) > 0 {
					for _, v := range params {
						param := strings.TrimSpace(v)
						if strings.HasSuffix(param, ",") {
							param = param[:len(param)-1]
						}
						ps := strings.Split(param, ":")
						if len(ps) != 2 {
							continue
						}
						buff.WriteString(fmt.Sprintf("- `%s` %s\n", ps[0], strings.TrimSpace(ps[1])))
					}
				}
			}
			buff.WriteString("\n")
		}
	}
	err = os.WriteFile(outPath, buff.Bytes(), os.ModePerm)
	if err != nil {
		panic(err)
	}
	fmt.Println("succeed to: ", outPath)
}
