package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

/*
@Time   : 2021-12-02 10:35
@Author : wushu
@DESC   : 生成与客户端的通信错误描述文件
*/

func main() {
	//wd, _ := os.Getwd()
	//os.Args = []string{"", wd}
	if len(os.Args) != 2 {
		fmt.Println("参数错误 eg：exe 项目根路径")
		return
	}
	wdDir, err := filepath.Abs(os.Args[1])
	if err != nil {
		panic(err)
	}
	errContentPath := fmt.Sprintf("%s/pkg/game/msg_gm/err_gm/err_code.go", wdDir)
	servicePath := fmt.Sprintf("%s/pkg/game/svc/service.go", wdDir)
	outputPath := fmt.Sprintf("%s/pkg/game/msg_gm/pb/error_desc.yml", wdDir)
	errContentBytes, err := ioutil.ReadFile(errContentPath)
	if err != nil {
		panic(err)
	}
	serviceBytes, err := ioutil.ReadFile(servicePath)
	if err != nil {
		panic(err)
	}
	errDescs := ReadConst(string(serviceBytes), string(errContentBytes))
	WriteYml(errDescs, outputPath)
}

// 写yml
func WriteYml(errDescs []*errDesc, outputPath string) {
	sort.Slice(errDescs, func(i, j int) bool {
		// 从小到大。通用错误(65000开始的)排在最前
		if strings.EqualFold(errDescs[i].service, "sys") && !strings.EqualFold(errDescs[j].service, "sys") {
			return true
		}
		if !strings.EqualFold(errDescs[i].service, "sys") && strings.EqualFold(errDescs[j].service, "sys") {
			return false
		}
		return errDescs[i].code < errDescs[j].code
	})
	var ymlBuffer = bytes.Buffer{}
	var service = errDescs[0].service
	ymlBuffer.WriteString(fmt.Sprintf("# Service: %s\n", service))
	for _, desc := range errDescs {
		if service != desc.service {
			service = desc.service
			ymlBuffer.WriteString(fmt.Sprintf("\n# Service: %s\n", service))
		}
		ymlBuffer.WriteString(fmt.Sprintf("%d: %s\n", desc.code, desc.note))
	}

	err := ioutil.WriteFile(outputPath, ymlBuffer.Bytes(), os.ModePerm)
	if err != nil {
		panic("错误描述文件写入错误")
	}
}

type constData struct {
	varName string
	expr    string
	note    string // 注释。需要将注释写在常量的上一行或行尾才可解析
	val     int
	okVal   bool // 标记是否成功解析了val
}

type errDesc struct {
	code    int
	note    string
	service string
}

func ReadConst(serviceContent, errContent string) (errDescs []*errDesc) {
	serviceConsts := parseConstExpr(serviceContent)
	errConsts := parseConstExpr(errContent)
	// 解析服务号
	parseConstExprVal(serviceConsts)

	// 解析错误号
	for _, data := range errConsts {
		// 解析表达式。形如 ErrQueryFailed = ErrCode(uint32(svc.Sys)*10000) + 1
		exprReg := regexp.MustCompile(`\.(\S*)\).*\*[^\d]*(\d*)[^\d]*\+[^\d]*(\d*)`)
		subMatch := exprReg.FindStringSubmatch(data.expr)
		a := serviceConsts[subMatch[1]].val
		b, err := strconv.Atoi(subMatch[2])
		if err != nil {
			panic("解析异常")
		}
		c, err := strconv.Atoi(subMatch[3])
		if err != nil {
			panic("解析异常")
		}
		data.val = a*b + c
		data.okVal = true
		errDescs = append(errDescs, &errDesc{code: data.val, note: data.note, service: subMatch[1]})
	}
	return
}

// 解析常量表达式
func parseConstExpr(content string) (constDatas map[string]*constData) {
	constDatas = make(map[string]*constData)
	lines := strings.Split(content, "\n")
	for i, line := range lines {
		lines[i] = strings.TrimSpace(line)
	}
	for i, line := range lines {
		if strings.HasPrefix(line, "//") {
			continue
		}
		// 单行常量 （格式要求：常量的声明和赋值必须写在同一行）
		constReg := regexp.MustCompile(`const\s*(\S*)\s*=(.*)`)
		subMatch := constReg.FindStringSubmatch(line)
		if subMatch != nil {
			constData := constData{varName: subMatch[1]}
			splits := strings.Split(subMatch[2], "//")
			constData.expr = strings.TrimSpace(splits[0])

			var inLineNote string // 行内注释
			if len(splits) == 2 {
				inLineNote = strings.TrimSpace(splits[1])
			}
			constData.note = joinNote(lines[i-1], inLineNote)

			constDatas[subMatch[1]] = &constData
		}

		// 多行常量 （格式要求：const和左括号必须在同一行，右括号必须独占一行）
		constsLeftReg := regexp.MustCompile(`const\s*\(`)
		subMatch = constsLeftReg.FindStringSubmatch(line)
		if subMatch != nil {
			var iotaExpr string
			var iotaVal int
			for {
				i++
				line := lines[i]
				if line == ")" {
					break
				}
				if strings.HasPrefix(line, "//") {
					continue
				}

				constsReg := regexp.MustCompile(`(\S*).*=(.*)`)
				subMatch := constsReg.FindStringSubmatch(line)
				var expr string
				constData := constData{}
				if subMatch != nil { // 有等号
					constData.varName = subMatch[1]
					subMatch[2] = strings.TrimSpace(subMatch[2])
					splits := strings.Split(subMatch[2], "//")
					expr = strings.TrimSpace(splits[0])
					if strings.Contains(expr, "iota") {
						iotaExpr, iotaVal = expr, 0
						expr = strings.ReplaceAll(expr, "iota", strconv.Itoa(iotaVal))
						iotaVal++
					}
					constData.expr = expr
					var inLineNote string // 行内注释
					if len(splits) == 2 {
						inLineNote = strings.TrimSpace(splits[1])
					}
					constData.note = joinNote(lines[i-1], inLineNote)
				} else { // 没有等号，属于iota或者注释
					splits := strings.Split(line, "//")
					constData.varName = splits[0]
					constData.expr = strings.ReplaceAll(iotaExpr, "iota", strconv.Itoa(iotaVal))
					iotaVal++

					var inLineNote string // 行内注释
					if len(splits) == 2 {
						inLineNote = strings.TrimSpace(splits[1])
					}
					constData.note = joinNote(lines[i-1], inLineNote)
				}
				constDatas[constData.varName] = &constData
			}
		}
	}
	return
}

// 拼接常量的注释(单行注释和行内注释)
func joinNote(lastLine, inLineNote string) string {
	var lineNote string
	if strings.HasPrefix(lastLine, "//") { // 有单行注释
		lineNote = strings.TrimSpace(strings.ReplaceAll(lastLine, "//", ""))
	}
	if lineNote == "" {
		return inLineNote
	}
	if inLineNote == "" {
		return lineNote
	}
	return fmt.Sprintf("%s;\t%s", lineNote, inLineNote)
}

// 单集合自循环(递归)解析表达式的值
// (传入的集合内常量应当能够自循环解析出值。如果常量文件中有常量来自外部，可以先将多个常量文件里的所有常量都放到一个集合中)
// 只适用于表达式为：单个数值 或 常量和数值组合的二元四则运算
func parseConstExprVal(constDatas map[string]*constData) {
	for _, c := range constDatas {
		if c.okVal {
			continue
		}
		expr := c.expr
		val, err := strconv.Atoi(expr)
		if err == nil { // 单个数值
			c.val = val
			c.okVal = true
		} else { // 常量&数值的二元四则运算表达式
			operator := regexp.MustCompile(`(\S*)\s([\+\-\*\\])\s(\S*)`)
			subMatch := operator.FindStringSubmatch(expr)
			a := strings.TrimSpace(subMatch[1])
			b := strings.TrimSpace(subMatch[3])
			ai, err := strconv.Atoi(a)
			if err != nil {
				if !constDatas[a].okVal {
					break // 不是数字且未解析，则跳过
				} else {
					ai = constDatas[a].val
				}
			}
			bi, err := strconv.Atoi(b)
			if err != nil {
				if !constDatas[b].okVal {
					break // 不是数字且未解析，则跳过
				} else {
					bi = constDatas[b].val
				}
			}
			switch subMatch[2] {
			case "+":
				c.val = ai + bi
			case "-":
				c.val = ai - bi
			case "*":
				c.val = ai * bi
			case "/":
				c.val = ai / bi
			default:
				panic("不支持的运算符")
			}
			c.okVal = true
		}
	}
	for _, c := range constDatas {
		if !c.okVal {
			parseConstExprVal(constDatas)
			break
		}
	}
}
