package main

import (
	"95eh.com/eg/utils"
	"fmt"
	"github.com/tealeg/xlsx"
	utils2 "hm/pkg/misc/utils"
	"io/fs"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"unicode"
)

func main() {
	wdDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	var conf ToolsConfig
	err = utils.StartLocalConf(wdDir+"/output", &conf, "tools.yml", "tools_dev.yml")
	if err != nil {
		panic(err)
	}
	path := conf.XlsxPath
	if len(path) == 0 {
		fmt.Println("请在tsv_dev.yml 里设置XlsxPath路径")
		return
	}
	files, err := ioutil.ReadDir(fmt.Sprintf("%s/", path))
	if err != nil {
		panic(fmt.Sprintf("excel文件读取错误\n%v", err))
	}
	excels := conf.Include

	for _, file := range files {
		if len(excels) > 0 && !utils2.SliceContain(excels, file.Name()) {
			continue
		}
		ExcelToTsvProcess(file, path, true)
	}
}

var outputTsvPath string
var outputGoPath string

func init() {
	wdDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	outputTsvPath = wdDir + "/output/tsv"
	outputGoPath = wdDir + "/pkg/misc/tsv"

	err = utils2.MkDirAll(outputTsvPath)
	if err != nil {
		panic(err)
	}
	err = utils2.MkDirAll(outputGoPath)
	if err != nil {
		panic(err)
	}
}

const (
	RowSplit   = "\n"
	ColSplit   = "\t"
	ClientRead = "c" // 客户端读取的，服务端不需要读取
)

func ExcelToTsvProcess(file fs.FileInfo, path string, createGoFile bool) {
	if strings.HasSuffix(file.Name(), "xlsx") {
		f, err := xlsx.OpenFile(fmt.Sprintf("%s/%s", path, file.Name()))
		if err != nil {
			panic(err)
		}
		if len(f.Sheets) > 0 {
			fName := strings.Replace(file.Name(), ".xlsx", "", 1)
			parseSheet(fName, f.Sheets[0], createGoFile)
		} else if len(f.Sheets) > 1 {
			for _, sheet := range f.Sheets {
				if len(sheet.Name) > 0 {
					parseSheet(sheet.Name, sheet, createGoFile)
				}
			}
		}
	}
}

func parseSheet(name string, sheet *xlsx.Sheet, createGoFile bool) {
	sb := &strings.Builder{}
	rows := sheet.MaxRow
	var content string
	if rows > 4 {
		cols := sheet.MaxCol
		if len(sheet.Cell(0, 0).Value) == 0 || strings.EqualFold(strings.TrimSpace(sheet.Cell(1, 0).Value), ClientRead) {
			// 未标记索引，或标记为仅客户端读的
			return
		}
		for row := 0; row < rows; row++ {
			subSb := &strings.Builder{}
			if row == 1 {
				//第二行为标记客户端与服务端是否读取的标记
				continue
			}
			for col := 0; col < cols; col++ {
				if len(sheet.Cell(0, col).Value) == 0 {
					continue
				}
				if len(sheet.Cell(row, 0).Value) == 0 {
					continue
				}
				if len(sheet.Cell(1, col).Value) != 0 && (strings.EqualFold(strings.TrimSpace(sheet.Cell(1, col).Value), ClientRead)) {
					continue
				}

				value := sheet.Cell(row, col).Value
				// 去掉值前后的空白字符，替换中间的换行符以及制表符(解析时要通过换行符分隔行，通过制表符分隔列)
				value = strings.ReplaceAll(strings.TrimSpace(value), "\r\n", RowSplit)
				value = strings.ReplaceAll(value, RowSplit, "<br>")
				value = strings.ReplaceAll(value, ColSplit, "<tab>")
				subSb.WriteString(value)
				// 写入列分隔符
				subSb.WriteString(ColSplit)
			}
			// 如果没有内容，表示这是多余行，无实际内容，则读取完毕，停止读取
			subStr := subSb.String()
			if len(subStr) == 0 {
				break
			}
			// 去掉行尾多余的列分隔符（分隔符长度为1）
			if strings.HasSuffix(subStr, ColSplit) {
				subStr = subStr[:len(subStr)-1]
			}
			// 写入内容和换行
			sb.WriteString(subStr + RowSplit)
		}
	}
	content = sb.String()
	// 去掉行尾多余的行分隔符（分隔符长度为1）
	if strings.HasSuffix(content, RowSplit) {
		content = content[:len(content)-1]
	}
	filePath := outputTsvPath + "/" + name + ".tsv"
	err := ioutil.WriteFile(filePath, []byte(content), os.ModePerm)
	if err != nil {
		panic(fmt.Sprintf("文件写入错误%s\n%v", filePath, err))
	}
	if createGoFile {
		createGoScript(name, content)
	}
}

func createGoScript(name, content string) {
	lines := strings.Split(content, RowSplit)
	variableNames := strings.Split(lines[0], ColSplit)
	typeNames := strings.Split(lines[1], ColSplit)
	noteNames := strings.Split(lines[2], ColSplit)
	if len(variableNames) != len(typeNames) {
		fmt.Println(name + " tsv 配置错误，属性长度与类型长度不匹配")
		return
	}
	title := strings.Title(name) + "Tsv"
	fileNameBuilder := strings.Builder{}
	for i, data := range []rune(name) {
		if i == 0 {
			data = unicode.ToLower(data)
		}
		if unicode.IsUpper(data) {
			fileNameBuilder.WriteRune('_')
		}
		fileNameBuilder.WriteRune(unicode.ToLower(data))
	}
	sb := strings.Builder{}
	sb.WriteString("package tsv\n")
	sb.WriteString("type " + title + " struct {\n")
	var setValueSB = strings.Builder{}
	setValueSB.WriteString("\ti:=0\n")
	for i := 0; i < len(variableNames); i++ {
		var vName = strings.Title(variableNames[i])
		var typeName = ""
		var tempName = strings.TrimSpace(typeNames[i])
		switch tempName {
		case "array(int)":
			typeName = "[]int32"
			setValueSB.WriteString("\ttsv." + vName + "=ToIntArray(values[i])\n\ti++\n")
			break
		case "array(string)":
			typeName = "[]string"
			setValueSB.WriteString("\ttsv." + vName + "=ToStringArray(values[i])\n\ti++\n")
			break
		case "array(float)":
			typeName = "[]float32"
			setValueSB.WriteString("\ttsv." + vName + "=ToFloatArray(values[i])\n\ti++\n")
			break
		case "array2(int)":
			typeName = "[][]int32"
			setValueSB.WriteString("\ttsv." + vName + "=ToIntArray2(values[i])\n\ti++\n")
			break
		case "array2(string)":
			typeName = "[][]string"
			setValueSB.WriteString("\ttsv." + vName + "=ToStringArray2(values[i])\n\ti++\n")
			break
		case "array2(float)":
			typeName = "[][]float32"
			setValueSB.WriteString("\ttsv." + vName + "=ToFloatArray2(values[i])\n\ti++\n")
			break
		case "float":
			typeName = "float32"
			setValueSB.WriteString("\ttsv." + vName + "=ToFloat(values[i])\n\ti++\n")
			break
		case "int":
			setValueSB.WriteString("\ttsv." + vName + "=ToInt(values[i])\n\ti++\n")
			typeName = "int32"
			break
		case "string":
			setValueSB.WriteString("\ttsv." + vName + "=values[i]\n\ti++\n")
			typeName = tempName
			break
		case "bool":
			setValueSB.WriteString("\ttsv." + vName + "=ToBool(values[i])\n\ti++\n")
			typeName = tempName
		}
		if i < len(noteNames) {
			sb.WriteString("//" + noteNames[i] + "\n")
		}
		sb.WriteString("\t" + vName + "\t" + typeName + "\n\n")
	}
	sb.WriteString("}\n")
	sb.WriteString("func (tsv *" + title + ") SetValues(values []string){\n")
	sb.WriteString(setValueSB.String())
	sb.WriteString("}\n")
	sb.WriteString("type " + title + "Manager struct {\n")
	sb.WriteString("\tTsvSlice []*" + title + "\n")
	sb.WriteString("\tTsvMap map[int32]*" + title + "\n")
	sb.WriteString("}\n")
	sb.WriteString("func (manager *" + title + "Manager) SetValues(data ITsv) {\n")
	sb.WriteString("\ttsv := data.(*" + title + ")\n")
	sb.WriteString("\tmanager.TsvSlice = append(manager.TsvSlice, tsv)\n")
	sb.WriteString("\tmanager.TsvMap[tsv." + strings.Title(variableNames[0]) + "] = tsv\n")
	sb.WriteString("}\n")
	sb.WriteString("func (manager *" + title + "Manager) ClearValues() {\n")
	sb.WriteString("\tmanager.TsvSlice = make([]*" + title + ", 0)\n")
	sb.WriteString("\tmanager.TsvMap = make(map[int32]*" + title + ", 0)\n")
	sb.WriteString("}\n")
	path := outputGoPath + "/" + fileNameBuilder.String() + "_tsv.go"
	err := ioutil.WriteFile(path, []byte(sb.String()), os.ModePerm)
	if err != nil {
		fmt.Println(fmt.Sprintf("写入GO结构失败：%v", err))
		return
	}
	exec.Command("gofmt", "-w", path).Run()
}
