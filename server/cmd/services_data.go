package main

import (
	"95eh.com/eg/utils"
	"hm/pkg/services/data"
)

func main() {
	data.Start()
	utils.WaitExit()
}
