package main

import (
	"95eh.com/eg/utils"
	"hm/pkg/game"
)

func main() {
	game.Start()
	utils.WaitExit()
}
