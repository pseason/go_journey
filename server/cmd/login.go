package main

import (
	"95eh.com/eg/utils"
	"hm/pkg/login"
)

func main() {
	login.Start()
	utils.WaitExit()
}
