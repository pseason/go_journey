package main

import (
	"95eh.com/eg/utils"
	"hm/pkg/services/space"
)

func main() {
	space.Start()
	utils.WaitExit()
}
