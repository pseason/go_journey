package main

import (
	"95eh.com/eg/utils"
	"hm/pkg/ai"
)

func main() {
	ai.StarterAi()
	utils.WaitExit()
}
