#!/bin/sh
set -v on
CGO_ENABLED=0
GOOS=linux
GOARCH=amd64
go build -gcflags "all=-N -l" -o ../output/login ./login.go
go build -gcflags "all=-N -l" -o ../output/game ./game.go
go build -gcflags "all=-N -l" -o ../output/services_data ./services_data.go
go build -gcflags "all=-N -l" -o ../output/services_space ./services_space.go