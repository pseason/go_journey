package generator

import (
	"com.animal.war/build-template/model"
	_ "embed"
	"fmt"
	"os"
	"text/template"
)

/*
@author pf
@date 2023/4/17 10:10
version 1.0.0
desc:

*/

//go:embed conf.txt
var confTxt string

//go:embed data.txt
var dataTxt string

func DoGen(info *model.GenerateInformation) {
	// 生成目录
	mkGenDir()
	// 读取文件
	file := confTxt
	if info.Ty == model.Data {
		file = dataTxt
	}
	tpl, err := template.New("gen").Parse(file)
	if err != nil {
		panic(err)
	}
	tableEntity := info.TableInfo.TableEntity
	writer, err := os.Create(fmt.Sprintf("gen/%s.java", tableEntity))
	if err != nil {
		panic(err)
	}
	defer func(writer *os.File) {
		err := writer.Close()
		if err != nil {
			fmt.Printf("generate file %s error  %s \n", tableEntity, err)
		}
	}(writer)
	err = tpl.Execute(writer, info)
	if err != nil {
		panic(err)
	}
}

// 生成目录
func mkGenDir() {
	dir := "gen"
	_, err := os.Stat(dir)
	if os.IsNotExist(err) {
		err := os.MkdirAll(dir, 0755)
		if err != nil {
			panic(err)
		}
	}
}
