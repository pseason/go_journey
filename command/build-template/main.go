package main

import (
	"bufio"
	"com.animal.war/build-template/config"
	"com.animal.war/build-template/generator"
	"com.animal.war/build-template/model"
	"fmt"
	"os"
	"strconv"
	"strings"
)

/*
@author pf
@date 2023/4/3 19:50
version 1.0.0
desc:

*/

func main() {
	// 加载配置
	config.LoadConfig()
	// 执行生成
	execute()
}

func execute() {
	for {
		config.AppConf.Table.Name = inputTable()
		config.AppConf.Table.Type = inputTableType()
		if config.AppConf.Table.Type == model.Data {
			config.AppConf.Table.Belongs = inputTableBelongs()
			config.AppConf.Table.Strategy = inputTableStrategy()
			fmt.Printf("数据表名: %s, 数据类型: %s, 数据模块: %s, 数据策略: %s \n", config.AppConf.Table.Name,
				tableTypeString(config.AppConf.Table.Type), tableBelongString(config.AppConf.Table.Belongs),
				tableStrategyString(config.AppConf.Table.Strategy))
		} else {
			fmt.Printf("数据表名: %s, 数据类型: %s \n", config.AppConf.Table.Name, tableTypeString(config.AppConf.Table.Type))
		}
		// 查询表结构
		info := config.Query(config.AppConf.Table.Name, config.AppConf.Table.Type, config.AppConf.Table.Belongs, config.AppConf.Table.Strategy)
		if info != nil {
			// 生成结构
			generator.DoGen(info)
		}
		// 重置参数
		reset()
	}
}

// 重置生成规则
func reset() {
	config.AppConf.Table.Name = ""
	config.AppConf.Table.Type = -1
	config.AppConf.Table.Belongs = -1
	config.AppConf.Table.Strategy = -1
}

// 输入生成table
func inputTable() string {
	msg := "input table name: "
	line := readCommand(msg)
	if line == "" {
		return inputTable()
	}
	return line
}

// table是数据表还是配置表
func inputTableType() int {
	msg := "input table type(1-CONF, 2-DATA): "
	line := readCommand(msg)
	ty, err := strconv.Atoi(line)
	if err != nil || (ty != model.Conf && ty != model.Data) {
		fmt.Printf("%sread error, input [1,2] \n", msg)
		return inputTableType()
	}
	return ty
}

// table所属模块
func inputTableBelongs() int {
	msg := "input table belongs module(1-WORLD, 2-GAME, 3-SHARE): "
	line := readCommand(msg)
	belongs, err := strconv.Atoi(line)
	if err != nil || (belongs != model.World && belongs != model.Game && belongs != model.Share) {
		fmt.Printf("%sread error, input [1,2,3] \n", msg)
		return inputTableBelongs()
	}
	return belongs
}

// table查询策略
func inputTableStrategy() int {
	// select strategy
	msg := "input table select strategy(1-SELECT_ONE, 2-SELECT_MANY): "
	line := readCommand(msg)
	strategy, err := strconv.Atoi(line)
	if err != nil || (strategy != model.One && strategy != model.Many) {
		fmt.Printf("%sread error, input [1,2] \n", msg)
		return inputTableStrategy()
	}
	return strategy
}

func readCommand(msg string) string {
	fmt.Printf("%s", msg)
	inputReader := bufio.NewReader(os.Stdin)
	line, err := inputReader.ReadString('\n')
	if err != nil {
		fmt.Printf("%s read error \n", msg)
		return readCommand(msg)
	}
	return strings.TrimSpace(line)
}

///////////////////////////////////////////////////////////////

func tableTypeString(ty int) string {
	if ty == model.Data {
		return "数据表"
	} else if ty == model.Conf {
		return "配置表"
	}
	return "未知"
}

func tableBelongString(belongs int) string {
	if belongs == model.World {
		return "大厅"
	} else if belongs == model.Game {
		return "游戏"
	} else if belongs == model.Share {
		return "共享"
	}
	return "未知"
}

func tableStrategyString(strategy int) string {
	if strategy == model.One {
		return "一对一"
	} else if strategy == model.Many {
		return "一对多"
	}
	return "未知"
}
