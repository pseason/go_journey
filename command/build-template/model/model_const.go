package model

/*
@author pf
@date 2023/4/6 16:36
version 1.0.0
desc:

*/

const (
	// Conf table type config
	Conf = 1
	// Data table type data
	Data = 2
	// World table belongs module world
	World = 1
	// Game table belongs module game
	Game = 2
	// Share table belongs module share
	Share = 3
	// One table select strategy one
	One = 1
	// Many table select strategy many
	Many = 2
)
