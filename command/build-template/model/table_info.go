package model

/*
@author pf
@date 2023/4/6 17:57
version 1.0.0
desc:

*/

type TableColumnInformation struct {
	ColumnName    string
	DataType      string
	ColumnKey     string
	ColumnComment string
}

type TableInformation struct {
	TableName     string
	TableComment  string
	TableEntity   string
	TableBelongs  string
	TableStrategy string
	PrimaryKey    string
}

type TableColumn struct {
	Column       string
	Primary      bool
	PrimaryOrder int
	Comment      string
	EntityType   string
	EntityField  string
}

type GenerateInformation struct {
	Ty           int
	Date         string
	TableInfo    TableInformation
	TableColumns []TableColumn
}
