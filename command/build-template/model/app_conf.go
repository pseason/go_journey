package model

/*
@author pf
@date 2023/4/6 17:35
version 1.0.0
desc:

*/

type AppConf struct {
	Mysql MysqlConfig
	Table TableConfig
}

type MysqlConfig struct {
	Username string
	Password string
	Url      string
	Database string
	Confbase string
}

type TableConfig struct {
	Name     string
	Type     int
	Belongs  int
	Strategy int
}
