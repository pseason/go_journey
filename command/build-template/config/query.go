package config

import (
	"bytes"
	"com.animal.war/build-template/model"
	"fmt"
	"strings"
	"time"
)

/*
@author pf
@date 2023/4/6 17:35
version 1.0.0
desc:

*/

const (
	querySql     = "SELECT COLUMN_NAME as columnName,DATA_TYPE as dataType,COLUMN_KEY as columnKey,COLUMN_COMMENT as columnComment FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ? ORDER BY ORDINAL_POSITION ASC"
	existSql     = "SELECT TABLE_NAME as tableName, TABLE_COMMENT as tableComment FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ? AND TABLE_NAME= ?"
	configPrefix = "Config"
	underline    = "_"
	keyPrefix    = " + \"_\" + "
)

func Query(tableName string, ty int, belongs int, strategy int) *model.GenerateInformation {
	schema := AppConf.Mysql.Confbase
	if ty == model.Data {
		schema = AppConf.Mysql.Database
	}
	// 查询数据表是否存在
	var info model.TableInformation
	row := MysqlDb.QueryRow(existSql, schema, tableName)
	if err := row.Scan(&info.TableName, &info.TableComment); err != nil {
		fmt.Printf("genetate table: %s, table not exist \n", tableName)
		return nil
	}
	// 查询表结构
	rows, err := MysqlDb.Query(querySql, schema, tableName)
	if err != nil {
		fmt.Printf("genetate table: %s, table not has column \n", tableName)
		return nil
	}
	columns := make([]model.TableColumnInformation, 0)
	for rows.Next() {
		var column model.TableColumnInformation
		if err := rows.Scan(&column.ColumnName, &column.DataType, &column.ColumnKey, &column.ColumnComment); err != nil {
			fmt.Printf("genetate table: %s, table column info error \n", tableName)
			return nil
		}
		columns = append(columns, column)
	}
	if len(columns) == 0 {
		fmt.Printf("genetate table: %s, table column empty error \n", tableName)
		return nil
	}
	fmt.Printf("%v\n", columns)
	// 转换对应的java类
	tableColumns := make([]model.TableColumn, 0)
	primaryKeyOrder := 0
	var buffer bytes.Buffer
	for i := 0; i < len(columns); i++ {
		var tableColumn model.TableColumn
		if toTableColumn(&tableColumn, columns[i]) {
			tableColumn.PrimaryOrder = primaryKeyOrder
			primaryKeyOrder++
			buffer.WriteString(tableColumn.EntityField + keyPrefix)
		}
		tableColumns = append(tableColumns, tableColumn)
	}
	if ty == model.Data {
		if primaryKeyOrder == 0 {
			fmt.Printf("genetate table: %s, table column not has any primary key \n", tableName)
			return nil
		}
		s := buffer.String()
		info.PrimaryKey = s[0 : len(s)-len(keyPrefix)]
		info.TableBelongs = toTableBelongs(ty, belongs)
		info.TableStrategy = toTableStrategy(ty, strategy)
	}
	var generate model.GenerateInformation
	info.TableEntity = toTableName(tableName, ty)
	generate.TableInfo = info
	generate.Ty = ty
	generate.TableColumns = tableColumns
	generate.Date = time.Now().Format(time.DateTime)
	return &generate
}

// 转换成java类名
func toTableName(tableName string, ty int) string {
	split := strings.Split(tableName, underline)
	var buffer bytes.Buffer
	for i := 0; i < len(split); i++ {
		s := split[i]
		if len(s) != 0 {
			buffer.WriteString(strings.ToUpper(string(s[0])))
			buffer.WriteString(s[1:])
		}
	}
	if ty == model.Conf {
		return configPrefix + buffer.String()
	}
	return buffer.String()
}

// 数据库column转换成java属性
func toTableColumn(column *model.TableColumn, info model.TableColumnInformation) bool {
	cm := info.ColumnName
	dt := info.DataType
	ck := info.ColumnKey
	cc := info.ColumnComment
	pk := isPrimaryKey(ck)
	column.Column = cm
	column.Primary = pk
	column.Comment = cc
	column.EntityType = toEntityType(dt)
	column.EntityField = toEntityField(cm)
	return pk
}

func toTableStrategy(ty int, strategy int) string {
	if ty == model.Conf {
		return ""
	}
	if strategy == model.One {
		return "ONE"
	} else if strategy == model.Many {
		return "MANY"
	}
	return ""
}

func toTableBelongs(ty int, belongs int) string {
	if ty == model.Conf {
		return ""
	}
	if belongs == model.Share {
		return "SHARE"
	} else if belongs == model.World {
		return "WORLD"
	} else if belongs == model.Game {
		return "GAME"
	}
	return ""
}

// 判断是否是主键
func isPrimaryKey(columnKey string) bool {
	if columnKey == "" {
		return false
	}
	return strings.Contains(columnKey, "PRI")
}

// 转换成java类字段名
func toEntityField(columnName string) string {
	split := strings.Split(columnName, underline)
	// 第一个小写，后面首字母大写
	var buffer bytes.Buffer
	for i := 0; i < len(split); i++ {
		s := split[i]
		if len(s) != 0 {
			if i == 0 {
				buffer.WriteString(s)
			} else {
				buffer.WriteString(strings.ToUpper(string(s[0])))
				buffer.WriteString(s[1:])
			}
		}
	}
	return buffer.String()
}

// 数据库类型转换成java类类型
func toEntityType(dataType string) string {
	switch dataType {
	case "char", "varchar", "text", "json", "longtext", "tinytext", "mediumtext":
		return "String"
	case "tinyint":
		return "byte"
	case "int", "integer":
		return "int"
	case "float", "double", "decimal":
		return "double"
	case "bigint":
		return "long"
	default:
		return "null"
	}
}
