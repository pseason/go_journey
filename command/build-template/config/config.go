package config

import (
	"com.animal.war/build-template/model"
	"database/sql"
	"fmt"
	"github.com/fsnotify/fsnotify"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"time"
)

/*
@author pf
@date 2023/4/6 17:35
version 1.0.0
desc:

*/

var AppConf model.AppConf

// MysqlDb 定义一个全局对象db
var MysqlDb *sql.DB

const (
	// mysql
	url      = "mysql.url"
	username = "mysql.username"
	password = "mysql.password"
	database = "mysql.database.data"
	confbase = "mysql.database.conf"
)

func LoadConfig() {
	viper.SetConfigName("app")
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic("read config failed:" + err.Error())
	}
	parse(&AppConf)
	// watch
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		//viper配置发生变化了 执行响应的操作
		fmt.Println("Config file changed:", e.Name)
		parse(&AppConf)
	})
}

// 解析
func parse(conf *model.AppConf) {
	conf.Mysql.Url = viper.Get(url).(string)
	conf.Mysql.Username = viper.Get(username).(string)
	conf.Mysql.Password = viper.Get(password).(string)
	conf.Mysql.Database = viper.Get(database).(string)
	conf.Mysql.Confbase = viper.Get(confbase).(string)
	// 连接数据库
	connect(conf)
}

func connect(conf *model.AppConf) {
	var err error
	// 打开连接
	MysqlDb, err = sql.Open(
		"mysql",
		fmt.Sprintf(
			"%s:%s@tcp(%s)/%s?charset=utf8",
			conf.Mysql.Username,
			conf.Mysql.Password,
			conf.Mysql.Url,
			conf.Mysql.Database,
		))
	if err != nil {
		panic("mysql连接配置错误:" + err.Error())
	}
	// 最大连接数
	MysqlDb.SetMaxOpenConns(2)
	// 闲置连接数
	MysqlDb.SetMaxIdleConns(1)
	// 最大连接周期
	MysqlDb.SetConnMaxLifetime(time.Duration(30000))
	if err = MysqlDb.Ping(); err != nil {
		panic("mysql连接异常:" + err.Error())
	}
	fmt.Printf("Mysql Connect Success, uri: %s, username: %s, password: %s, data-base: %s, conf-base: %s \n",
		conf.Mysql.Url, conf.Mysql.Username, conf.Mysql.Password, conf.Mysql.Database, conf.Mysql.Confbase)
}
