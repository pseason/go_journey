package main

import (
	"fmt"
	"os/exec"
)

/*
@author pf
@date 2023/5/12 16:42
version 1.0.0
desc:

*/

const (
	PGREP = "pgrep"
)

// pgrep -alf $name
func pgrep(pattern string) {
	command := execCommand(PGREP, "-alf", pattern)
	fmt.Printf(command)
}

// 执行命令
func execCommand(command string, args ...string) string {
	cmd := exec.Command(command, args...)
	stdout, err := cmd.Output()
	if err != nil {
		panic(err)
	}
	return string(stdout)
}
