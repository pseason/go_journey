package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
@author pf
@date 2023/5/12 15:41
version 1.0.0
desc: input linux command

*/

func main() {
	// 输出规则	exec.Command("ls", "-l")
	printRules()
	// 输入所要匹配的程序名称
	name := inputProgramName()
	// 匹配程序
	pgrep(name)
}

// 输入所要匹配的程序名称
func inputProgramName() string {
	msg := "输入所要匹配的程序名称: "
	command := readCommand(msg)
	if command == "" {
		return inputProgramName()
	}
	return command
}

func readCommand(msg string) string {
	fmt.Printf("%s", msg)
	inputReader := bufio.NewReader(os.Stdin)
	line, err := inputReader.ReadString('\n')
	if err != nil {
		fmt.Printf("%s read error \n", msg)
		return readCommand(msg)
	}
	return strings.TrimSpace(line)
}

func printRules() {
	fmt.Println("------------------------------------------------")
	fmt.Println("|                                              ")
	fmt.Println("| 1.输入要匹配的程序名称,将执行: pgrep -alf $name")
	fmt.Println("| 2.关联程序,选择相关操作执行")
	fmt.Println("|")
	fmt.Println("|")
	fmt.Println("|")
	fmt.Println("-----------------------------------------------")
}
