package lru

/*
@author pengshuo
@date 2021/8/14 16:55
version 1.0.0
desc:

*/
type (
	// Value use Len to count how many bytes it takes
	CValue interface {
		Len() int
	}
	// 回调接口
	OnEvicted interface {
		DoEvicted(key string, value CValue)
	}
	// 回调方法继承接口
	OnEvictedFunc func(key string, value CValue)
)

//  回调方法执行
func (f OnEvictedFunc) DoEvicted(key string, value CValue) {
	f(key, value)
}
