package lru

import (
	"container/list"
)

/*
@author pengshuo
@date 2021/8/14 13:55
version 1.0.0
desc:
	lru cache
*/
// Cache is a LRU cache. It is not safe for concurrent access.
type (
	LruCache struct {
		// maxBytes == 0,不会移除元素，#Add.for
		maxBytes int64
		memBytes int64
		node     *list.List
		cache    map[string]*list.Element
		// optional and executed when an entry is purged.
		evicted OnEvicted
	}

	entry struct {
		key   string
		value CValue
	}
)

// New is the Constructor of Cache
func NewLruCache(maxBytes int64, onEvicted OnEvicted) *LruCache {
	return &LruCache{
		maxBytes: maxBytes,
		memBytes: 0,
		node:     list.New(),
		cache:    make(map[string]*list.Element),
		evicted:  onEvicted,
	}
}

// Add adds a value to the cache.
func (c *LruCache) LruAdd(k string, v CValue) {
	// 存在key，缓存长度补充value的len，替换value，移到末尾
	// 不存在key，添加到末尾，存储，增加缓存长度
	// 判断已用缓存是否超过默认缓存
	if ele, ok := c.cache[k]; ok {
		c.node.MoveToFront(ele)
		kv := ele.Value.(*entry)
		c.memBytes += int64(v.Len()) - int64(kv.value.Len())
		kv.value = v
	} else {
		ele := c.node.PushFront(&entry{key: k, value: v})
		c.cache[k] = ele
		c.memBytes += int64(v.Len()) + int64(len(k))
	}
	for c.maxBytes != 0 && c.maxBytes < c.memBytes {
		c.lruRemoveOldest()
	}
}

// Get look ups a key's value
func (c *LruCache) LruGet(key string) (value CValue, ok bool) {
	// 查找主要有 2 个步骤，第一步是从字典中找到对应的双向链表的节点，第二步，将该节点移动到队尾
	if ele, ok := c.cache[key]; ok {
		c.node.MoveToFront(ele)
		kv := ele.Value.(*entry)
		return kv.value, ok
	}
	return
}

// RemoveOldest removes the oldest item
// 这里的删除，实际上是缓存淘汰。即移除最近最少访问的节点（队首）
func (c *LruCache) lruRemoveOldest() {
	// c.ll.Back() 取到队首节点，从链表中删除。
	// delete(c.cache, kv.key)，从字典中 c.cache 删除该节点的映射关系。
	// 更新当前所用的内存 c.memBytes。
	// 如果回调函数 OnEvicted 不为 nil，则调用回调函数
	ele := c.node.Back()
	if ele != nil {
		c.node.Remove(ele)
		kv := ele.Value.(*entry)
		delete(c.cache, kv.key)
		c.memBytes -= int64(len(kv.key)) + int64(kv.value.Len())
		if c.evicted != nil {
			c.evicted.DoEvicted(kv.key, kv.value)
		}
	}
}

// Len the number of cache entries
func (c *LruCache) Len() int {
	return c.node.Len()
}
