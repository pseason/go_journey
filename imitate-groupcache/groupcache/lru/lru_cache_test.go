package lru

import (
	"testing"
)

/*
@author pengshuo
@date 2021/8/14 14:29
version 1.0.0
desc:
	lur cache test
*/
type String string

func (s String) Len() int {
	return len(s)
}

// test get
func TestCache_LGet(t *testing.T) {
	lru := NewLruCache(int64(0), nil)
	lru.LruAdd("a", String("123"))
	if v, ok := lru.LruGet("a"); !ok || string(v.(String)) != "123" {
		t.Fatal("cache hit a=123 failed")
	} else {
		t.Log("cache hit a=123")
	}
	if _, ok := lru.LruGet("b"); !ok {
		t.Log("cache miss b failed")
	}
}

// test remove
func TestCache_LRemoveOldest(t *testing.T) {
	k1, k2, k3 := "a", "b", "c"
	v1, v2, v3 := String("3457"), String("1222"), String("56")
	// size
	cap := len(k1) + len(k2) + v1.Len() + v2.Len()
	// new
	lru := NewLruCache(int64(cap), nil)
	// add
	lru.LruAdd(k1, v1)
	lru.LruAdd(k2, v2)
	// add if cap is not enough
	lru.LruAdd(k3, v3)

	if 2 == lru.Len() {
		t.Log("test success")
	} else {
		t.Fatal("test fail")
	}
}

func TestCache_LOnEvicted(t *testing.T) {
	k1, k2, k3 := "a", "b", "c"
	v1, v2, v3 := String("3457"), String("1222"), String("56")
	// size
	cap := len(k1) + len(k2) + v1.Len() + v2.Len()
	// new
	lru := NewLruCache(int64(cap), OnEvictedFunc(func(s string, value CValue) {
		t.Log("onEvicted callback:", s, value)
	}))
	// add
	lru.LruAdd(k1, v1)
	lru.LruAdd(k2, v2)
	// add if cap is not enough
	lru.LruAdd(k3, v3)

	if 2 == lru.Len() {
		t.Log("test success")
	} else {
		t.Fatal("test fail")
	}
}
