package groupcache

import (
	"fmt"
	"reflect"
	"testing"
)

/*
@author pengshuo
@date 2021/8/14 16:17
version 1.0.0
desc:

*/
func TestGetterFunc(t *testing.T) {
	var f Getter = GetterFunc(func(key string) ([]byte, error) {
		return []byte(key), nil
	})
	expect := []byte("he")

	if by, err := f.Get("he"); err == nil && reflect.DeepEqual(by, expect) {
		t.Log("success: ", string(by))
	} else {
		t.Fatal("error")
	}
}

//在这个测试用例中，我们主要测试了 2 种情况
//1）在缓存为空的情况下，能够通过回调函数获取到源数据。
//2）在缓存已经存在的情况下，是否直接从缓存中获取，为了实现这一点，使用 loadCounts 统计某个键调用回调函数的次数，
//	 如果次数大于1，则表示调用了多次回调函数，没有缓存
var db = map[string]string{
	"Tom":  "630",
	"Jack": "589",
	"Sam":  "567",
}

func TestGroup_Get(t *testing.T) {
	t.Log(2 << 10)
	loadCounts := make(map[string]int)
	group := NewGroupWithOutEvicted("score", 2<<10, GetterFunc(
		func(key string) ([]byte, error) {
			loadCounts[key] += 1

			if v, ok := db[key]; ok {
				return []byte(v), nil
			}
			return nil, fmt.Errorf("%s not exist", key)
		},
	))
	for k, v := range db {
		for i := 0; i < 2; i++ {
			if bytes, err := group.Get(k); err == nil && reflect.DeepEqual(bytes, []byte(v)) {
				t.Log("fetch success: ", k, v)
			} else {
				t.Log("fetch error: ", k, v)
			}
		}
	}
	for k, v := range loadCounts {
		t.Log("loadCounts: ", k, v)
	}
}
