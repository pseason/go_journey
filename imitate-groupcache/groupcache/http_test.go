package groupcache

import (
	"fmt"
	"log"
	"net/http"
	"testing"
)

/*
@author pengshuo
@date 2021/8/20 10:31
version 1.0.0
desc:
	test http pool
*/

var hdb = map[string]string{
	"Tom":  "630",
	"Jack": "589",
	"Sam":  "567",
}

func TestHttpPool_ServeHttp(t *testing.T) {
	NewGroupWithOutEvicted("scores", 2<<10, GetterFunc(
		func(key string) ([]byte, error) {
			log.Println("[SlowDB] search key", key)
			if v, ok := hdb[key]; ok {
				return []byte(v), nil
			}
			return nil, fmt.Errorf("%s not exist", key)
		}))
	addr := "localhost:9018"
	peers := NewHttpPool(addr)
	log.Println("group-cache is running at", addr)
	log.Fatal(http.ListenAndServe(addr, peers))
}
