package groupcache

import (
	"fmt"
	"log"
	"springmars.com/groupcache/lru"
	"sync"
)

/*
@author pengshuo
@date 2021/8/14 16:06
version 1.0.0
desc:
	group cache

1. 定义接口 Getter 和 回调函数 Get(key string)([]byte, error)，参数是 key，返回值是 []byte。
2. 定义函数类型 GetterFunc，并实现 Getter 接口的 Get 方法。
3. 函数类型实现某一个接口，称之为接口型函数，方便使用者在调用时既能够传入函数作为参数，也能够传入实现了该接口的结构体作为参数

*/
// A Getter loads data for a key.
type Getter interface {
	// 获取缓存数据
	Get(key string) ([]byte, error)
}

// A GetterFunc implements Getter with a function.
type GetterFunc func(key string) ([]byte, error)

// Get implements Getter interface function
func (f GetterFunc) Get(key string) ([]byte, error) {
	return f(key)
}

type Group struct {
	name   string
	getter Getter
	cache  *cache
	peers  PeerPicker
}

var (
	mu     sync.RWMutex
	groups = make(map[string]*Group)
)

// NewGroup create a new instance of Group
func NewGroupWithEvicted(name string, maxCacheBytes int64, getter Getter, evicted lru.OnEvicted) *Group {
	if getter == nil {
		panic("nil Getter")
	}
	mu.Lock()
	defer mu.Unlock()

	g := &Group{
		name:   name,
		getter: getter,
		cache:  newCache(maxCacheBytes, evicted),
	}

	groups[name] = g
	return g
}

// NewGroupWithOutEvicted
func NewGroupWithOutEvicted(name string, maxCacheBytes int64, getter Getter) *Group {
	return NewGroupWithEvicted(name, maxCacheBytes, getter, nil)
}

// GetGroup returns the named group previously created with NewGroup, or
// nil if there's no such group.
func GetGroup(name string) *Group {
	mu.RLock()
	g := groups[name]
	mu.RUnlock()
	return g
}

// Get value for a key from cache
func (g *Group) Get(key string) ([]byte, error) {
	if key == "" {
		return nil, fmt.Errorf("key is required")
	}
	if bytes, ok := g.cache.get(key); ok {
		log.Println("[GroupCache] hit-key: ", key)
		return bytes.ByteSlice(), nil
	}
	return g.load(key)
}

// RegisterPeers registers a PeerPicker for choosing remote peer
func (g *Group) RegisterPeers(peers PeerPicker) {
	if peers == nil {
		panic("RegisterPeerPicker called more than once")
	}
	g.peers = peers
}

// load key from other node server
func (g *Group) load(key string) (bytes []byte, err error) {
	if g.peers != nil {
		if peer, ok := g.peers.PickPeer(key); ok {
			if bytes, err := g.getFromPeer(peer, key); err == nil {
				return bytes, err
			}
			log.Println("[GroupCache] Failed to get from peer", err)
		}
	}
	return g.getLocally(key)
}

func (g Group) getLocally(key string) ([]byte, error) {
	// get value from getter
	bytes, err := g.getter.Get(key)
	// err
	if err != nil {
		return nil, err
	}
	value := ByteView{b: cloneBytes(bytes)}
	// not error populate cache
	g.cache.add(key, value)
	// return
	return value.ByteSlice(), nil
}

func (g *Group) getFromPeer(peer PeerGetter, key string) ([]byte, error) {
	bytes, err := peer.Get(g.name, key)
	if err != nil {
		return nil, err
	}
	return bytes, nil
}
