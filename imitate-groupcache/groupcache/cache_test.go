package groupcache

import (
	"math/rand"
	"reflect"
	"springmars.com/groupcache/lru"
	"strconv"
	"testing"
)

/*
@author pengshuo
@date 2021/8/16 9:47
version 1.0.0
desc:
	cache test
*/

func TestCache_Add_Get(t *testing.T) {

	ca := newCache(int64(128), lru.OnEvictedFunc(func(key string, value lru.CValue) {
		bv := value.(ByteView)
		t.Log("OnEvictedFunc remove", key, string(bv.b))
	}))
	for i := 0; i < 10; i++ {
		go func() {
			a := strconv.Itoa(rand.Intn(100))

			by := ByteView{
				b: []byte(strconv.Itoa(rand.Intn(100) * 10)),
			}
			// add
			ca.add(a, by)
			// get
			if b, ok := ca.get(a); ok && reflect.DeepEqual(by, b) {
				t.Log("get success:", string(b.b))
			} else {
				t.Fatal("get error!!!")
			}
		}()
	}
}
