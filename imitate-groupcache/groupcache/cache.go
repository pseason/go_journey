package groupcache

import (
	"springmars.com/groupcache/lru"
	"sync"
)

/*
@author pengshuo
@date 2021/8/14 15:43
version 1.0.0
desc:

1. cache.go 的实现非常简单，实例化 lru，封装 get 和 add 方法，并添加互斥锁 mu。
2. 在 add 方法中，判断了 c.lru 是否为 nil，如果等于 nil 再创建实例。
	这种方法称之为延迟初始化(Lazy Initialization)，
	一个对象的延迟初始化意味着该对象的创建将会延迟至第一次使用该对象时。主要用于提高性能，并减少程序内存要求
*/
type cache struct {
	mx         sync.Mutex
	lru        *lru.LruCache
	cacheBytes int64
	evicted    lru.OnEvicted
}

func newCache(cacheBytes int64, evicted lru.OnEvicted) *cache {
	return &cache{
		cacheBytes: cacheBytes,
		evicted:    evicted,
	}
}

// add key-value
func (c *cache) add(k string, v ByteView) {
	c.mx.Lock()
	defer c.mx.Unlock()
	// is nil
	if c.lru == nil {
		c.lru = lru.NewLruCache(c.cacheBytes, c.evicted)
	}
	c.lru.LruAdd(k, v)
}

// get value by key
func (c *cache) get(k string) (value ByteView, ok bool) {
	c.mx.Lock()
	defer c.mx.Unlock()
	if c.lru == nil {
		return
	}
	if v, ok := c.lru.LruGet(k); ok {
		return v.(ByteView), ok
	}
	return
}
