package groupcache

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"springmars.com/groupcache/consistenthash"
	"strings"
	"sync"
)

/*
@author pengshuo
@date 2021/8/20 10:02
version 1.0.0
desc:
	http
*/

const (
	defaultBasePath = "/_group-cache/"
	defaultReplicas = 50
)

// struct HttpPool
type (
	HttpPool struct {
		self        string // self url xxx:xxx
		basePath    string
		mu          sync.Mutex // guards peers and httpGetters
		peers       *consistenthash.Map
		httpGetters map[string]*httpGetter // keyed by e.g. "http://10.0.0.2:8008"
	}

	//
	httpGetter struct {
		baseURL string
	}
)

var _ PeerGetter = (*httpGetter)(nil)
var _ PeerPicker = (*HttpPool)(nil)

// new HttpPool
func NewHttpPool(self string) *HttpPool {
	return &HttpPool{
		self:     self,
		basePath: defaultBasePath,
	}
}

// log
func (p *HttpPool) Log(format string, v ...interface{}) {
	log.Printf("[Server %s] %s", p.self, fmt.Sprintf(format, v...))
}

// ServeHttp handler all http request
func (p *HttpPool) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// 判断是否已 basePath 结尾
	if !strings.HasPrefix(r.URL.Path, p.basePath) {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}
	p.Log("%s %s", r.Method, r.URL.Path)
	// /<basepath>/<groupname>/<key> required
	// 如果长度不为规定长度2
	parts := strings.SplitN(r.URL.Path[len(p.basePath):], "/", 2)
	if len(parts) != 2 {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}
	// args
	groupName := parts[0]
	key := parts[1]
	// group
	group := GetGroup(groupName)
	if group == nil {
		http.Error(w, "no such group: "+groupName, http.StatusNotFound)
		return
	}
	// ByteView
	bytes, err := group.Get(key)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Write(bytes)
}

// Set updates the pool's list of peers.
func (p *HttpPool) Set(peers ...string) {
	p.mu.Lock()
	defer p.mu.Unlock()

	p.peers = consistenthash.NewHash(defaultReplicas, nil)
	p.peers.Add(peers...)
	p.httpGetters = make(map[string]*httpGetter, len(peers))
	for _, peer := range peers {
		p.httpGetters[peer] = &httpGetter{baseURL: peer + p.basePath}
	}
}

// 从某个节点获取缓存
func (h *httpGetter) Get(group string, key string) ([]byte, error) {
	u := fmt.Sprintf(
		"%v%v/%v",
		h.baseURL,
		url.QueryEscape(group),
		url.QueryEscape(key),
	)
	resp, err := http.Get(u)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("server returned: %v", resp.Status)
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("reading response body: %v", err)
	}
	return bytes, nil
}

// PickPeer picks a peer according to key
func (p *HttpPool) PickPeer(key string) (PeerGetter, bool) {
	p.mu.Lock()
	defer p.mu.Unlock()
	if peer := p.peers.Get(key); peer != "" && peer != p.self {
		p.Log("Pick peer %s", peer)
		return p.httpGetters[peer], true
	}
	return nil, false
}
