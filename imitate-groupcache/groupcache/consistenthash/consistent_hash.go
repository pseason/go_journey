package consistenthash

import (
	"hash/crc32"
	"sort"
	"strconv"
)

/*
@author pengshuo
@date 2021/8/20 11:00
version 1.0.0
desc:
	consistent hash
*/

// hash maps bate[] to int32
type Hash func(data []byte) uint32

// 存储节点数据关系 maps contains all hashed keys
type Map struct {
	hash     Hash           //计算虚拟节点hash
	replicas int            //每个真实节点分配的虚拟节点个数
	keys     []int          //虚拟节点数组 sorted
	hashMap  map[int]string //节点map，key虚拟节点，value真实节点
}

// 新建为每个节点分配n个虚拟节点的数据集
func NewHash(replicas int, fn Hash) *Map {
	m := &Map{
		hash:     fn,
		replicas: replicas,
		hashMap:  make(map[int]string),
	}
	if m.hash == nil {
		m.hash = crc32.ChecksumIEEE
	}
	return m
}

// 向数据集中添加节点，并分配虚拟节点与真实节点关系
func (m *Map) Add(keys ...string) {
	for _, key := range keys {
		for i := 0; i < m.replicas; i++ {
			hash := int(m.hash([]byte(strconv.Itoa(i) + key)))
			// 分配n个虚拟节点
			m.keys = append(m.keys, hash)
			// 虚拟节点指向真实节点
			m.hashMap[hash] = key
		}
	}
	sort.Ints(m.keys)
}

// 通过key选取真实节点
func (m *Map) Get(key string) string {
	if len(key) == 0 {
		return ""
	}
	// hash
	hash := int(m.hash([]byte(key)))
	// Binary search for appropriate replica.
	idx := sort.Search(len(m.keys), func(i int) bool {
		return m.keys[i] >= hash
	})
	return m.hashMap[m.keys[idx%len(m.keys)]]
}

// remove key
func (m Map) Remove(key string) {
	for i := 0; i < m.replicas; i++ {
		hash := int(m.hash([]byte(strconv.Itoa(i) + key)))
		idx := sort.SearchInts(m.keys, hash)
		m.keys = append(m.keys[:idx], m.keys[idx+1:]...)
		delete(m.hashMap, hash)
	}
}
