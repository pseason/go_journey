package consistenthash

import (
	"fmt"
	"sort"
	"strconv"
	"testing"
)

/*
@author pengshuo
@date 2021/8/20 11:00
version 1.0.0
desc:
	consistent hash
*/
func TestSortSearch(t *testing.T) {
	ids := []int{1, 2, 3, 4, 5, 6}
	search := sort.Search(len(ids), func(i int) bool {
		return ids[i] >= 3
	})
	fmt.Println(search)

	idx := sort.SearchInts(ids, 3)
	fmt.Println(idx)

}

func TestMap_Get(t *testing.T) {
	hash := NewHash(3, func(data []byte) uint32 {
		i, _ := strconv.Atoi(string(data))
		return uint32(i)
	})
	// Given the above hash function, this will give replicas with "hashes":
	// 2, 4, 6, 12, 14, 16, 22, 24, 26
	hash.Add("6", "4", "2")
	fmt.Println(hash.keys, hash.hashMap)

	testCases := map[string]string{
		"2":  "2",
		"11": "2",
		"23": "4",
		"27": "2",
	}

	for k, v := range testCases {
		if hash.Get(k) == v {
			t.Logf("Asking for %s, should have %s", k, v)
		}
	}

	// Adds 8, 18, 28
	hash.Add("8")
	// 27 should now map to 8.
	testCases["27"] = "8"

	fmt.Println(hash.keys, hash.hashMap)
	for k, v := range testCases {
		if hash.Get(k) == v {
			t.Logf("Asking for %s, should have %s", k, v)
		}
	}
}
