module springmars.com/imitate-groupcache

go 1.16


require (
	springmars.com/groupcache v1.0.0
)


replace (
	springmars.com/groupcache => ./groupcache
)
