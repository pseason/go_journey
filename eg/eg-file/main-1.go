package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	_ "springmars.com/file/eg"
	"strconv"
	"strings"
)

func main() {
	readFile()
}

type People struct {
	id    string
	count int
}

func readFile() {

	peoples := make([]People, 0)

	people := make(map[string]int)

	f, err := os.Open("xxx3.txt")
	if err != nil {
		return
	}
	reader := bufio.NewReader(f)
	for true {
		bytes, er := reader.ReadBytes('\n')
		if er != nil {
			if er == io.EOF {
				break
			}
		}
		s := strings.Trim(string(bytes), "\n")
		split := strings.Split(s, "#")
		id := split[0]
		count, _ := strconv.Atoi(split[1])
		people[id] = people[id] + count
	}

	for k, v := range people {
		peoples = append(peoples, People{id: k, count: v})
	}
	bubbling(peoples)
	for _, p := range peoples {
		fmt.Println(p.id, "----", p.count)
	}
}

func bubbling(array []People) {
	for i := 0; i < len(array); i++ {
		for j := i + 1; j < len(array); j++ {
			t := array[i]
			if array[i].count > array[j].count {
				array[i] = array[j]
				array[j] = t
			}
		}
	}
}
