package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"springmars.com/file/base"
	"strconv"
	"strings"
)

/*
@author pengshuo
@date 2021/12/17 11:01
version 1.0.0
desc:

*/

func main() {
	playerCharges := make([]base.PlayerCharge, 0)
	people := make(map[int]int)

	f, err := os.Open("20211217.txt")
	if err != nil {
		return
	}
	reader := bufio.NewReader(f)
	for true {
		bytes, er := reader.ReadBytes('\n')
		if er != nil {
			if er == io.EOF {
				break
			}
		}
		s := strings.Trim(string(bytes), "\n")
		split := strings.Split(s, ": ")
		arr1 := strings.Split(split[1], "[")
		arr2 := strings.Split(arr1[1], "]")
		playerId, _ := strconv.Atoi(arr2[0])
		count, _ := strconv.Atoi(split[2])
		people[playerId] = people[playerId] + count
	}

	for k, v := range people {
		playerCharges = append(playerCharges, base.PlayerCharge{PlayerId: k, Charge: v})
	}
	base.BubblingPlayerCharge(playerCharges)
	for _, p := range playerCharges {
		fmt.Printf("玩家[%d]补偿充值[%d]\n", p.PlayerId, p.Charge)
	}
}
