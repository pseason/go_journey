package base

/*
@author pengshuo
@date 2021/12/17 11:02
version 1.0.0
desc:

*/

type PlayerCharge struct {
	PlayerId int
	Charge   int
}

func BubblingPlayerCharge(array []PlayerCharge) {
	for i := 0; i < len(array); i++ {
		for j := i + 1; j < len(array); j++ {
			t := array[i]
			if array[i].Charge > array[j].Charge {
				array[i] = array[j]
				array[j] = t
			}
		}
	}
}
