package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

/*
@author pengshuo
@date 2021/11/10 16:13
version 1.0.0
desc:

*/

func main() {
	readRefund()
}

type People struct {
	id    string
	count int
}

func readRefund() {

	peoples := make([]People, 0)

	people := make(map[string]int)

	f, err := os.Open("chargerefund.txt")
	if err != nil {
		return
	}
	reader := bufio.NewReader(f)
	for true {
		bytes, er := reader.ReadBytes('\n')
		if er != nil {
			if er == io.EOF {
				break
			}
		}
		s := strings.Trim(string(bytes), "\n")
		split := strings.Split(s, "] : ")

		s2 := split[1]

		s3 := strings.Trim(s2, "\n")

		split2 := strings.Split(s3, "玩家[")

		split3 := strings.Split(split2[1], "]累计")
		id := split3[0]
		// charge
		split1 := strings.Split(s3, "充值额度: ")
		charge, _ := strconv.Atoi(split1[1])
		people[id] = people[id] + charge
	}

	for k, v := range people {
		peoples = append(peoples, People{id: k, count: v})
	}
	bubbling(peoples)
	for _, p := range peoples {
		fmt.Printf("玩家[%s]补偿累充[%d]\n", p.id, p.count)
	}
}

func bubbling(array []People) {
	for i := 0; i < len(array); i++ {
		for j := i + 1; j < len(array); j++ {
			t := array[i]
			if array[i].count > array[j].count {
				array[i] = array[j]
				array[j] = t
			}
		}
	}
}
