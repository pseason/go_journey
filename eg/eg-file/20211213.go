package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

/*
@author pengshuo
@date 2021/12/13 13:50
version 1.0.0
desc:

*/

type PeopleRank struct {
	Rank     int
	PlayerId int
}

func main() {
	f, err := os.Open("child_two_four_point_day_548_20211212.20211212")
	if err != nil {
		return
	}
	reader := bufio.NewReader(f)
	index := 0
	rank := 0
	for true {
		bytes, er := reader.ReadBytes('\n')
		if er != nil {
			if er == io.EOF {
				break
			}
		}
		index++
		if index%2 == 0 {
			continue
		}
		rank++
		s := strings.Trim(string(bytes), "\n")
		split := strings.Split(s, "\"")
		pidstr := split[1]
		fmt.Printf("playerRanks.add(new PlayerRank(%d,%s));\n", rank, pidstr)
		if rank == 100 {
			break
		}
	}
}
