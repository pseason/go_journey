module springmars.com/daily/dtm

go 1.16

//go get -u github.com/dtm-labs/dtm

require (
	github.com/google/gnostic v0.6.6 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
