package main

import (
	"fmt"
	jrl "github.com/juju/ratelimit"
	"go.uber.org/ratelimit"
	"time"
)

/*
@author pengshuo
@date 2021/10/8 14:06
version 1.0.0
desc:

*/

func main() {
	testRateLimit()

	testBucket()
}

func testRateLimit() {
	rate := ratelimit.New(100)

	prev := time.Now()

	for i := 0; i < 120; i++ {
		now := rate.Take()

		fmt.Println(i, prev, now.Sub(prev))

		prev = now
	}

}

func testBucket() {
	bucket := jrl.NewBucket(time.Millisecond*100, 10)
	for i := 0; i < 120; i++ {
		if bucket.WaitMaxDuration(1, time.Millisecond*99) {
			fmt.Println(i)
		}
	}
}
