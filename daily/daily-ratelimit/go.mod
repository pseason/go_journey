module springmars.com/daily/ratelimit

go 1.16

require (
	github.com/juju/ratelimit v1.0.1 // indirect
	go.uber.org/ratelimit v0.2.0 // indirect
)
