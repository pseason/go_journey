package main

import (
	"fmt"
	"github.com/urfave/cli"
	"os"
	"runtime/pprof"
	"time"
)

/*
@author pengshuo
@date 2021/9/2 11:56
version 1.0.0
desc:
	go run main.go --help
*/

func main() {
	app := cli.NewApp()
	app.Name = "daily-cli"
	app.Author = "pseason"
	app.Version = "1.0.0"
	app.Copyright = "springmars.com@2020-2021"
	app.Usage = "go daily study"
	// flags
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "cpuprofile",
			Usage: "enable cpu profile",
		},
	}
	app.Action = server
	app.Run(os.Args)
}

func server(c *cli.Context) error {

	if c.Bool("cpuprofile") {
		filename := fmt.Sprintf("cpuprofile-%d.pprof", time.Now().Unix())
		f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, os.ModePerm)
		if err != nil {
			panic(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	return nil
}
