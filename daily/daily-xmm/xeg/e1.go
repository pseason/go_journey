package xeg

import (
	"fmt"
	"github.com/heiyeluren/xmm"
	"unsafe"
)

/*
@author pengshuo
@date 2022/2/18 14:58
version 1.0.0
desc: XMM 快速使用入门

*/

func E1() {
	// 初始化xmm对象
	factory := &xmm.Factory{}
	// 从操作系统申请一个内存块
	// 如果内存使用达到60%，就进行异步自动扩容，每次异步扩容256MB内存（固定值），0.6这个数值可以自主配置
	memory, err := factory.CreateMemory(0.6)
	if err != nil {
		panic("CreateMemory fail \n" + err.Error())
	}
	//操作int类型，申请内存后赋值
	var tmpNum = 9527
	ptr, err := memory.Alloc(unsafe.Sizeof(tmpNum))
	if err != nil {
		panic("Alloc fail \n" + err.Error())
	}
	id := (*int)(ptr)
	//把设定好的数字赋值写入到XMM内存中
	*id = tmpNum
	//操作字符串类型，XMM提供了From()接口，可以直接获得一个指针，字符串会存储在XMM中
	name, err := memory.From("Jack")
	if err != nil {
		panic("Alloc fail \n" + err.Error())
	}
	//从XMM内存中输出变量和内存地址
	fmt.Println("===== XMM X(eXtensible) Memory Manager example 00 ======")
	fmt.Println("-- Memory data status --")
	fmt.Println(" Id  :", *id, "\t\t( Id ptr addr: ", id, ")")
	fmt.Println(" Name:", name, "\t( Name ptr addr: ", &name, ")")

	fmt.Println("===== Example test success ======")

	//释放Id,Name内存块
	memory.Free(uintptr(ptr))
	memory.FreeString(name)
}
