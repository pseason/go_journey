package xeg

/*
@author pengshuo
@date 2022/2/18 11:12
version 1.0.0
desc: struct

*/

type User struct {
	Id     uint
	Name   string
	Age    uint
	Email  string
	Salary float32
}
