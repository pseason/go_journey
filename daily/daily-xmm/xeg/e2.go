package xeg

import (
	"fmt"
	"github.com/heiyeluren/xmm"
	"unsafe"
)

/*
@author pengshuo
@date 2022/2/18 11:07
version 1.0.0
desc: 如何在结构体中使用 XMM

*/

func E2() {
	// 初始化xmm对象
	factory := &xmm.Factory{}
	// 从操作系统申请一个内存块
	// 如果内存使用达到60%，就进行异步自动扩容，每次异步扩容256MB内存（固定值），0.6这个数值可以自主配置
	memory, err := factory.CreateMemory(0.8)
	if err != nil {
		panic("CreateMemory fail \n" + err.Error())
	}
	//自己从内存块中申请一小段自己想用的内存
	size := unsafe.Sizeof(User{})
	p, err := memory.Alloc(size)
	if err != nil {
		panic("Alloc fail \n" + err.Error())
	}
	//使用该内存块，进行结构体元素赋值
	user := (*User)(p)
	user.Id = 1
	user.Age = 18
	user.Name = "daily"
	user.Email = "daily@example.com"
	//输出变量，打印整个结构体等
	fmt.Println("===== XMM X(eXtensible) Memory Manager example 01 ======")

	fmt.Println("-- Memory data status --")
	fmt.Println("User ptr addr: \t", p)
	fmt.Println("User data: \t", user)
	//释放内存块（实际是做mark标记操作）
	memory.Free(uintptr(p))
	//Free()后再看看变量值，只是针对这个内存块进行mark标记动作，并未彻底从内存中释放（XMM设计机制，降低实际gc回收空闲时间）
	//XMM内部会有触发gc的机制，主要是内存容量，参数TotalGCFactor=0.0004，目前如果要配置，需要自己修改这个常量，一般不用管它，Free()操作中有万分之4的概率会命中触发gc~
	//GC触发策略：待释放内存  > 总内存 * 万分之4 会触发gc动作
	fmt.Println("-- Memory data status after XMM.Free() --")
	fmt.Println("memory ptr addr:\t", p)
	fmt.Println("User data:\t\t", user)
	//结束
	fmt.Println("===== Example test success ======")
}
