package main

import (
	"fmt"
	"springmars.com/daily/xmm/xeg"
)

/*
@author pengshuo
@date 2022/2/18 11:05
version 1.0.0
desc:

*/

func main() {
	xeg.E1()
	fmt.Println("=========================")
	xeg.E2()
}
