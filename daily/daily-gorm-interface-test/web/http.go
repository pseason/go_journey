package web

import (
	"springmars.com/daily/gormtest/ginhttp"
)

/*
@author pengshuo
@date 2021/10/21 17:30
version 1.0.0
desc:

*/

func init() {
	ginhttp.CreateRoute("/teacher/:id", ginhttp.GET, getTeacherByPrimaryKey)
	ginhttp.CreateRoute("/teacher/add", ginhttp.POST, addTeacher)
	ginhttp.CreateRoute("/teacher/update", ginhttp.POST, updateTeacher)
	ginhttp.CreateRoute("/teacher/:id", ginhttp.DELETE, deleteTeacher)
	ginhttp.CreateRoute("/teacher/getAll", ginhttp.GET, getAllTeacher)
	ginhttp.CreateRoute("/teacher/count", ginhttp.GET, getTeacherCount)
	ginhttp.CreateRoute("/teacher/query", ginhttp.POST, queryTeacher)
}
