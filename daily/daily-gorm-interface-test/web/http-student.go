package web

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"springmars.com/daily/gormtest/database"
	"springmars.com/daily/gormtest/ginhttp"
	"strconv"
)

/*
@author pengshuo
@date 2021/10/21 17:28
version 1.0.0
desc:

*/

func getTeacherByPrimaryKey(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	tea := new(database.Teacher)
	tea.Id = id
	err := database.GetRecordByPrimaryKey(tea, database.DB)

	if err == nil {
		c.JSON(http.StatusOK, ginhttp.Success(tea))
	} else {
		c.JSON(http.StatusOK, ginhttp.Error(err))
	}
}

// 绑定JSON的示例 ({"name": "张", "age": 36, "addr": "16", "score": 96 })
func addTeacher(c *gin.Context) {
	var data interface{}
	tea := new(database.Teacher)
	if err := c.BindJSON(tea); err == nil {
		err = database.Add(tea, database.DB)
		if err == nil {
			data = ginhttp.Success(tea)
		} else {
			data = ginhttp.Error(err)
		}
	} else {
		data = ginhttp.Error(err)
	}
	c.JSON(http.StatusOK, data)
}

// 绑定JSON的示例 ({"name": "张", "age": 36, "addr": "16", "score": 96 })
func updateTeacher(c *gin.Context) {
	var data interface{}
	tea := new(database.Teacher)
	if err := c.BindJSON(tea); err == nil {
		err = database.Updates(tea, database.DB)
		if err == nil {
			data = ginhttp.Success(tea)
		} else {
			data = ginhttp.Error(err)
		}
	} else {
		data = ginhttp.Error(err)
	}
	c.JSON(http.StatusOK, data)
}

func deleteTeacher(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	tea := new(database.Teacher)
	tea.Id = id
	err := database.Delete(tea, database.DB)

	if err == nil {
		c.JSON(http.StatusOK, ginhttp.Success(tea))
	} else {
		c.JSON(http.StatusOK, ginhttp.Error(err))
	}
}

func getTeacherCount(c *gin.Context) {
	count, err := database.GetRecordCount(new(database.Teacher), database.DB)
	if err == nil {
		c.JSON(http.StatusOK, ginhttp.Success(count))
	} else {
		c.JSON(http.StatusOK, ginhttp.Error(err))
	}
}

func getAllTeacher(c *gin.Context) {
	ret, err := database.GetRecordAll(new(database.Teacher), database.DB)
	if err == nil {
		c.JSON(http.StatusOK, ginhttp.Success(ret))
	} else {
		c.JSON(http.StatusOK, ginhttp.Error(err))
	}
}

func queryTeacher(c *gin.Context) {
	var data interface{}

	qr := new(database.QueryRecordForm)
	if err := c.BindJSON(qr); err == nil {
		data, err = database.GetRecordList(new(database.Teacher), qr, database.DB)
		if err == nil {
			data = ginhttp.Success(data)
		} else {
			data = ginhttp.Error(err)
		}
	} else {
		data = ginhttp.Error(err)
	}
	c.JSON(http.StatusOK, data)
}
