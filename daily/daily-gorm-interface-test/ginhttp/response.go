package ginhttp

/*
@author pengshuo
@date 2021/8/31 16:56
version 1.0.0
desc:

*/
const (
	successCode int = 200
	errorCode   int = 100
)

type responseData struct {
	Code  int         `json:"code"`
	Data  interface{} `json:"data"`
	Error interface{} `json:"error"`
}

// Success ResponseData
func Success(data interface{}) responseData {
	return responseData{
		Code: successCode,
		Data: data,
	}
}

// Error ResponseData
func Error(data interface{}) responseData {
	switch dt := data.(type) {
	case error:
		return responseData{
			Code:  errorCode,
			Error: dt.Error(),
		}
	default:
		return responseData{
			Code:  errorCode,
			Error: data,
		}
	}
}
