package database

import (
	"errors"
	"gorm.io/gorm"
	"log"
)

/*
@author pengshuo
@date 2021/10/21 16:09
version 1.0.0
desc:
	base repo
*/

var (
	ErrCreate          = errors.New("create model failed")
	ErrDelete          = errors.New("delete model failed")
	ErrUpdate          = errors.New("update model failed")
	ErrGetByPrimaryKey = errors.New("get model by primary key failed")
	ErrGetAll          = errors.New("get all model failed")
	ErrGetRecord       = errors.New("get model list failed")
	ErrCount           = errors.New("get model count failed")
)

// 基础数据模型
type BaseModel interface {
	getPrimaryKey() interface{}
}

// 查询字段
type FieldData struct {
	Column string      `json:"key"`    // 字段名称
	Value  interface{} `json:"value"`  // 字段value
	Symbol string      `json:"symbol"` // 符号
}

// 查询条件
type QueryRecordForm struct {
	Params   []*FieldData `json:"params" form:"params"`
	Order    []string     `json:"order" form:"order"`
	PageNum  int          `json:"pageNum" form:"pageNum"`
	PageSize int          `json:"pageSize" form:"pageSize"`
}

//  add one record
func Add(b BaseModel, db *gorm.DB) (err error) {
	if err = db.Create(b).Error; err != nil {
		log.Println(err.Error())
		err = ErrCreate
	}
	return
}

//  delete record
func Delete(b BaseModel, db *gorm.DB) (err error) {
	if err = db.Delete(b).Error; err != nil {
		log.Println(err.Error())
		err = ErrDelete
	}
	return
}

//  update record
func Updates(b BaseModel, db *gorm.DB) (err error) {
	if err = db.Model(b).Updates(b).Error; err != nil {
		log.Println(err.Error())
		err = ErrUpdate
	}
	return
}

//  get record by primary key
func GetRecordByPrimaryKey(b BaseModel, db *gorm.DB) (err error) {
	if err = db.First(b, b.getPrimaryKey()).Error; err != nil {
		log.Println(err.Error())
		err = ErrGetByPrimaryKey
	}
	return
}

//  get count
func GetRecordCount(b BaseModel, db *gorm.DB) (ret int64, err error) {
	if err = db.Model(b).Count(&ret).Error; err != nil {
		log.Println(err.Error())
		err = ErrCount
	}
	return
}

func GetRecordAll(b BaseModel, db *gorm.DB) (ret []map[string]interface{}, err error) {
	if err = db.Model(b).Find(&ret).Error; err != nil {
		log.Println(err.Error())
		err = ErrGetAll
	}
	return
}

// todo 未实现数据查询接口（go method不支持切片base类型传递）
func _GetRecordAll_(ret []BaseModel, db *gorm.DB) (err error) {
	if err = db.Find(&ret).Error; err != nil {
		log.Println(err.Error())
		err = ErrGetAll
	}
	return
}

//   get record list some field value or some condition
func GetRecordList(b BaseModel, q *QueryRecordForm, db *gorm.DB) (ret []map[string]interface{}, err error) {
	// order
	if len(q.Order) > 0 {
		for _, v := range q.Order {
			db = db.Order(v)
		}
	}
	// pageSize
	if q.PageSize != 0 {
		db = db.Limit(q.PageSize)
	}
	// pageNum
	if q.PageNum != 0 {
		q.PageNum = (q.PageNum - 1) * q.PageSize
		db = db.Offset(q.PageNum)
	}

	// params
	if q.Params != nil {
		for _, param := range q.Params {
			db = db.Where(param.Column+param.Symbol+"?", param.Value)
		}
	}

	if err = db.Model(b).Find(&ret).Error; err != nil {
		log.Println(err.Error())
		err = ErrGetRecord
	}
	return
}
