package database

/*
@author pengshuo
@date 2021/8/30 16:35
version 1.0.0
desc:

*/

// db struct
type Teacher struct {
	Id    int    `json:"id" gorm:"primaryKey"`
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Addr  string `json:"addr"`
	Score int    `json:"score"`
}

func (t *Teacher) getPrimaryKey() interface{} {
	return t.Id
}
