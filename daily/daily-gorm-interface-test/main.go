package main

import (
	"log"
	"os"
	"os/signal"
	"springmars.com/daily/gormtest/conf"
	"springmars.com/daily/gormtest/database"
	"springmars.com/daily/gormtest/ginhttp"
	_ "springmars.com/daily/gormtest/web"
	"syscall"
)

/*
@author pengshuo
@date 2021/10/21 16:25
version 1.0.0
desc:

*/

func main() {
	defer database.Terminate()
	// 加载配置
	conf.LoadAppConfig()
	// start gin web
	ginhttp.StartGin(conf.AppConf.Env, conf.AppConf.Port)

	sg := make(chan os.Signal)
	signal.Notify(sg, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGKILL)
	select {
	case s := <-sg:
		log.Printf("got signal: %s \n", s.String())
	}
}
