package main

import (
	"cxgame.com/luban/api/conf"
	"cxgame.com/luban/api/database"
	"cxgame.com/luban/api/ginhttp"
	_ "cxgame.com/luban/api/web"
)

/*
@author pengshuo
@date 2021/11/03 09:48
version 1.0.0
desc:

*/

func main() {
	defer database.Terminate()
	// 加载配置
	conf.LoadAppConfig()
	// start gin web
	ginhttp.StartGin(conf.AppConf.Env, conf.AppConf.Port)
}
