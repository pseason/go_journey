package ginhttp

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

/*
@author pengshuo
@date 2021/11/03 09:45
version 1.0.0
desc:
	gin 封装
*/

var (
	// 路由
	handlers         = make([]route, 0)
	gin_release_mode = "release"
)

type (
	HttpMethod int
	// 自定义route结构
	route struct {
		url     string
		method  HttpMethod
		handler func(c *gin.Context)
	}
)

// http method
const (
	GET HttpMethod = iota + 1
	POST
	DELETE
	PUT
	PATCH
)

// 创建路由
func CreateRoute(url string, method HttpMethod, handler func(c *gin.Context)) {
	handlers = append(handlers, route{url: url, method: method, handler: handler})
}

// 循环注册路由
func registerRoute(engine *gin.Engine) {
	for _, route := range handlers {
		switch route.method {
		case GET:
			engine.GET(route.url, route.handler)
		case POST:
			engine.POST(route.url, route.handler)
		case DELETE:
			engine.DELETE(route.url, route.handler)
		case PUT:
			engine.PUT(route.url, route.handler)
		case PATCH:
			engine.PATCH(route.url, route.handler)
		default:
			log.Printf("路由注册失败 method: %d ,url: %s \n", route.method, route.url)
		}
	}
}

// 开启Gin
func StartGin(env string, port int) {
	if env == gin_release_mode {
		gin.SetMode(gin.ReleaseMode)
	}
	engine := gin.New()
	// 跨域
	engine.Use(cors())
	//// 日志处理
	engine.Use(logger())
	// 注册路由
	registerRoute(engine)
	uri := fmt.Sprintf(":%d", port)
	log.Printf("gin-web start at: %s \n", uri)
	if err := engine.Run(uri); err != nil {
		log.Panicf("程序启动失败: %s \n", err)
	}
}

// gin cors 跨域 中间件
func cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		origin := c.Request.Header.Get("Origin")
		if origin != "" {
			// 可将将 * 替换为指定的域名
			c.Header("Access-Control-Allow-Origin", "*")
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
			c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
			c.Header("Access-Control-Allow-Credentials", "true")
		}
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}

// gin logger 中间件
func logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()

		c.Next()

		cost := time.Now().Sub(start)
		log.Printf("gin-web: %s?%s, method: %s, ip: %s, cost: %v \n", c.Request.URL.Path, c.Request.URL.RawQuery, c.Request.Method, c.ClientIP(), cost)
	}
}
