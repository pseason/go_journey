package web

import (
	"cxgame.com/luban/api/database"
	"cxgame.com/luban/api/ginhttp"
	"github.com/gin-gonic/gin"
	"net/http"
)

/*
@author pengshuo
@date 2021/11/03 09:42
version 1.0.0
desc:

*/

func init() {
	ginhttp.CreateRoute("/luban-api/:name", ginhttp.GET, func(c *gin.Context) {
		ret, err := database.GetRecordAll(c.Param("name"))
		if err == nil {
			c.JSON(http.StatusOK, ginhttp.Success(ret))
		} else {
			c.JSON(http.StatusOK, ginhttp.Error(err))
		}
	})
}
