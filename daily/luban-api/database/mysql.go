package database

import (
	"database/sql"
	"fmt"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"log"
	"time"
)

/*
@author pengshuo
@date 2021/11/03 09:39
version 1.0.0
desc:
	mysql 连接
*/
const (
	mysqlUrl               = "%s.app.mysql.uri"
	mysqlUserName          = "%s.app.mysql.username"
	mysqlPassword          = "%s.app.mysql.password"
	mysqlDatabase          = "%s.app.mysql.database"
	mysqlMaxOpenConns      = "%s.app.mysql.maxOpenConns"
	mysqlMaxIdleConns      = "%s.app.mysql.maxIdleConns"
	mysqlConnMaxLifetime   = "%s.app.mysql.connMaxLifetime"
	defaultMaxOpenconns    = 10
	defaultMaxIdleConns    = 2
	defaultConnMaxLifetime = 6
)

var (
	// 对外的db类型
	dB    *gorm.DB
	sqlDB *sql.DB
)

func Cfg2Init(env string) {
	url := viper.GetString(fmt.Sprintf(mysqlUrl, env))
	username := viper.GetString(fmt.Sprintf(mysqlUserName, env))
	password := viper.GetString(fmt.Sprintf(mysqlPassword, env))
	database := viper.GetString(fmt.Sprintf(mysqlDatabase, env))
	maxOpenConns := viper.GetInt(fmt.Sprintf(mysqlMaxOpenConns, env))
	maxIdleConns := viper.GetInt(fmt.Sprintf(mysqlMaxIdleConns, env))
	connMaxLifetime := viper.GetInt(fmt.Sprintf(mysqlConnMaxLifetime, env))
	var err error
	dB, err = gorm.Open(
		mysql.Open(fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local", username, password, url, database)),
		&gorm.Config{
			// table naming Strategy (不配置，继承TableName接口)
			NamingStrategy: schema.NamingStrategy{
				// 表名前缀，`User`表为`t_users`
				TablePrefix: "",
				// 使用单数表名，启用该选项后，`User` 表将是`user`
				SingularTable: true,
			},
			Logger: logger.Default.LogMode(logger.Info),
		},
	)
	if err != nil {
		panic("mysql连接配置错误:" + err.Error())
	}
	sqlDB, err = dB.DB()
	if err != nil {
		panic("mysql连接配置错误:" + err.Error())
	}
	// 最大连接数
	if maxOpenConns == 0 {
		maxOpenConns = defaultMaxOpenconns
	}
	sqlDB.SetMaxOpenConns(maxOpenConns)
	// 闲置连接数
	if maxIdleConns == 0 {
		maxIdleConns = defaultMaxIdleConns
	}
	sqlDB.SetMaxIdleConns(maxIdleConns)
	// 最大连接周期
	if connMaxLifetime == 0 {
		connMaxLifetime = defaultConnMaxLifetime
	}
	// 连接可复用的最大时，单位分钟
	sqlDB.SetConnMaxLifetime(time.Minute * time.Duration(connMaxLifetime))

	if err = sqlDB.Ping(); err != nil {
		panic("mysql连接异常:" + err.Error())
	}
	log.Printf("mysql connect at [%s:%s@tcp(%s)/%s] \n", username, password, url, database)
}

func Terminate() {
	sqlDB.Close()
}
