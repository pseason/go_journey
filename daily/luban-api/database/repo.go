package database

import (
	"errors"
	"log"
)

/*
@author pengshuo
@date 2021/11/03 09:40
version 1.0.0
desc:
	base repo
*/

var (
	ErrGetAll = errors.New("get all model failed")
)

type SystemFormData struct {
	Sysformdataid int    `json:"id" gorm:"primaryKey"`
	Fname         string `json:"name"`
	Value         string `json:"value"`
}

func (SystemFormData) TableName() string {
	return "system_form_data"
}

// GetRecordAll get all record
func GetRecordAll(name string) (ret []SystemFormData, err error) {
	if err = dB.Where("fname = ?", name).Find(&ret).Error; err != nil {
		log.Println(err.Error())
		err = ErrGetAll
	}
	return
}
