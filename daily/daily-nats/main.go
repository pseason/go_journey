package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"time"
)

/*
@author pengshuo
@date 2021/10/25 15:14
version 1.0.0
desc:

*/

func main() {
	wildcardBasicUsage()
	// channelBasicUsage()
	// encodeBasicUsage()
	// jetStreamBasicUsage()
	// basicUsage()
}

func wildcardBasicUsage() {
	nc, _ := nats.Connect(nats.DefaultURL)
	defer nc.Close()

	// "*" matches any token, at any level of the subject.
	nc.Subscribe("foo.*.baz", func(m *nats.Msg) {
		log.Printf("Msg received on [%s] : %s\n", m.Subject, string(m.Data))
	})

	nc.Subscribe("foo.bar.*", func(m *nats.Msg) {
		log.Printf("Msg received on [%s] : %s\n", m.Subject, string(m.Data))
	})

	// ">" matches any length of the tail of a subject, and can only be the last token
	// E.g. 'foo.>' will match 'foo.bar', 'foo.bar.baz', 'foo.foo.bar.bax.22'
	nc.Subscribe("foo.>", func(m *nats.Msg) {
		log.Printf("Msg received on [%s] : %s\n", m.Subject, string(m.Data))
	})

	// Matches all of the above
	nc.Publish("foo.bar.baz", []byte("Hello World"))

	time.Sleep(time.Second * 3)
}

func channelBasicUsage() {
	nc, _ := nats.Connect(nats.DefaultURL)
	ec, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	defer ec.Close()

	type person struct {
		Name    string
		Address string
		Age     int
	}

	recvCh := make(chan *person)
	ec.BindRecvChan("hello", recvCh)

	sendCh := make(chan *person)
	ec.BindSendChan("hello", sendCh)

	me := &person{Name: "derek", Age: 22, Address: "140 New Montgomery Street"}

	// Send via Go channels
	sendCh <- me

	// Receive via Go channels
	who := <-recvCh

	log.Printf("recv: %v \n", who)

	time.Sleep(time.Second * 3)
}

func encodeBasicUsage() {
	// Connect to a server
	nc, _ := nats.Connect(nats.DefaultURL)
	ec, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	// defer to close
	defer ec.Close()
	// Subscribe Publish
	foo := "foo"
	ec.Subscribe(foo, func(s string) {
		log.Printf("nats sub: %s, recv: %s \n", foo, s)
	})
	ec.Publish(foo, "Hello World")

	// EncodedConn can Publish any raw Go type using the registered Encoder
	type person struct {
		Name    string
		Address string
		Age     int
	}
	// Go type Subscriber
	ec.Subscribe("person", func(p *person) {
		log.Printf("Received a person: %+v\n", p)
	})
	derek := &person{Name: "person", Age: 22, Address: "140 New Montgomery Street, San Francisco, CA"}
	// Go type Publisher
	ec.Publish("person", derek)

	// Requests
	// Replying
	ec.Subscribe("help", func(subj, reply string, msg string) {
		log.Printf("Request : %s \n", msg)
		ec.Publish(reply, "I can help!")
	})
	var response string
	err := ec.Request("help", "help me", &response, 10*time.Second)
	if err != nil {
		log.Printf("Request failed: %v\n", err)
	} else {
		log.Printf("Request response: %s \n", response)
	}

	time.Sleep(time.Second * 3)
}

// 启动JetStream(nats-server -js)
// jet stream
func jetStreamBasicUsage() {
	// Connect to a server
	nc, _ := nats.Connect(nats.DefaultURL)
	// defer to close
	defer nc.Close()
	// Create JetStream Context
	js, _ := nc.JetStream(nats.PublishAsyncMaxPending(256))
	// Delete Stream
	js.DeleteStream("OBJECT")
	// Add Stream
	if _, err := js.AddStream(&nats.StreamConfig{
		Name:     "OBJECT",
		Subjects: []string{"a"},
		Storage:  nats.MemoryStorage,
	}); err != nil {
		log.Println("AddStream-", err.Error())
		return
	}

	go func() {
		if sub, err := js.Subscribe("a", func(msg *nats.Msg) {
			log.Printf("nats async recv: %s \n", string(msg.Data))
		}, nats.OrderedConsumer()); err == nil {
			defer sub.Unsubscribe()
			time.Sleep(time.Second * 5)
		}
	}()

	go func() {
		// OrderedConsumer does not need HB, it sets it on its own, but for test we override which is ok.
		if sub, err := js.SubscribeSync("a", nats.OrderedConsumer()); err == nil {
			defer sub.Unsubscribe()
			expires := time.Now().Add(5 * time.Second)
			for time.Now().Before(expires) {
				if msg, err := sub.NextMsg(time.Second); err == nil {
					log.Printf("nats sync recv: %s \n", string(msg.Data))
				}
			}
		}
	}()

	// Now send into the stream as chunks.
	for i := 0; i < 20; i++ {
		msg := nats.NewMsg("a")
		msg.Data = []byte(fmt.Sprintf("pma: %d", i))
		msg.Header.Set("data", "true")
		js.PublishMsgAsync(msg)
	}
	// eof
	js.PublishAsync("a", []byte("over"))

	select {
	case <-js.PublishAsyncComplete():
		log.Println("nats stream PublishAsyncComplete")
	case <-time.After(time.Second * 3):
		log.Println("Did not receive completion signal")
	}

	time.Sleep(time.Second * 5)

}

func basicUsage() {
	// Connect to a server
	nc, _ := nats.Connect(nats.DefaultURL)
	// defer to close
	defer nc.Close()

	// Simple Subscriber Publisher
	foo := "foo"
	nc.Subscribe(foo, func(msg *nats.Msg) {
		log.Printf("nats subscribe sub: %s, recv: %s \n", foo, string(msg.Data))
	})
	nc.Publish(foo, []byte("hello world"))
	// Simple Subscribe Response
	request := "request"
	nc.Subscribe(request, func(msg *nats.Msg) {
		log.Printf("nats subscribe sub: %s, recv: %s \n", request, string(msg.Data))
		msg.Respond([]byte("answer is 42"))
	})
	reply, err := nc.Request(request, []byte("你好"), time.Second*2)
	log.Printf("nats Request sub: %s, reply: %s \n", request, string(reply.Data))

	// Simple Sync Subscriber
	if sub, err := nc.SubscribeSync(foo); err == nil {
		go func() {
			for {
				if msg, err := sub.NextMsg(time.Millisecond * 500); err == nil {
					log.Printf("nats sync subscribe sub: %s, rec: %s \n", foo, string(msg.Data))
				}
			}
		}()
	}

	// channel subscriber
	ch := make(chan *nats.Msg, 64)
	if _, err = nc.ChanSubscribe(foo, ch); err == nil {
		go func() {
			for msg := range ch {
				log.Printf("nats chan subscribe sub: %s, rec: %s \n", foo, string(msg.Data))
			}
		}()
	}
	nc.Publish(foo, []byte("xxx-push-xxx"))
	nc.Publish(foo, []byte("xxx-push-yyy"))

	// Requests Replies
	help := "help"
	nc.Subscribe(help, func(msg *nats.Msg) {
		log.Printf("nats subscribe sub: %s, recv: %s \n", help, string(msg.Data))
		nc.Publish(msg.Reply, []byte("I can help!"))
	})
	msg, err := nc.Request(help, []byte("help me"), time.Millisecond*10)
	log.Printf("nats Request sub: %s, reply: %s \n", help, string(msg.Data))

	// Drain connection (Preferred for responders)
	// Close() not needed if this is called.
	nc.Drain()
}
