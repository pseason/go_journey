module springmars.com/daily/nats

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/nats-io/nats-server/v2 v2.6.2 // indirect
	github.com/nats-io/nats.go v1.13.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
