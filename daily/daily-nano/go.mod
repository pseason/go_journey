module springmars.com/daily/nanostd

go 1.16

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/lonng/nano v0.5.0 // indirect
	golang.org/x/net v0.0.0-20210825183410-e898025ed96a // indirect
	golang.org/x/sys v0.0.0-20210831042530-f4d43177bf5e // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20210831024726-fe130286e0e2 // indirect
)
