package model

import (
	"fmt"
	tea "github.com/charmbracelet/bubbletea"
	"os"
	"testing"
)

/*
@author pengshuo
@date 2021/8/11 14:17
version 1.0.0
desc:

*/
func BenchmarkText(b *testing.B) {
	todo := NewTodo([]string{"cleanning", "wash clothes", "write a blog"}, make(map[int]struct{}))
	cmd := tea.NewProgram(todo)
	if err := cmd.Start(); err != nil {
		fmt.Println("start failed:", err)
		os.Exit(1)
	}
}
