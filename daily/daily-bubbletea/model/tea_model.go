package model

import (
	"fmt"
	tea "github.com/charmbracelet/bubbletea"
)

/*
@author pengshuo
@date 2021/8/11 10:56
version 1.0.0
desc:

*/

type todo struct {
	todos    []string
	cursor   int
	selected map[int]struct{}
}

func NewTodo(todos []string, selected map[int]struct{}) todo {
	return todo{
		todos:    todos,
		selected: selected,
	}
}

func (t todo) Init() tea.Cmd {
	return nil
}

func (t todo) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return t, tea.Quit

		case "up", "k":
			if t.cursor > 0 {
				t.cursor--
			}

		case "down", "j":
			if t.cursor < len(t.todos)-1 {
				t.cursor++
			}

		case "enter", " ":
			_, ok := t.selected[t.cursor]
			if ok {
				delete(t.selected, t.cursor)
			} else {
				t.selected[t.cursor] = struct{}{}
			}
		}
	}
	return t, nil
}

func (t todo) View() string {
	s := "todo list:\n\n"

	for i, choice := range t.todos {
		cursor := " "
		if t.cursor == i {
			cursor = ">"
		}

		checked := " "
		if _, ok := t.selected[i]; ok {
			checked = "x"
		}

		s += fmt.Sprintf("%s [%s] %s\n", cursor, checked, choice)
	}

	s += "\nPress q to quit.\n"
	return s
}
