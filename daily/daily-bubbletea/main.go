package main

import (
	"fmt"
	tea "github.com/charmbracelet/bubbletea"
	"os"
	"springmars.com/daily-bubbletea/github"
)

/*
@author pengshuo
@date 2021/8/11 13:42
version 1.0.0
desc:

*/

func main() {
	model := github.NewModel("java", "daily")

	if err := tea.NewProgram(model).Start(); err != nil {
		fmt.Println("start failed:", err)

		os.Exit(1)
	}

}
