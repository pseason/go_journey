package github

import (
	"fmt"
	"github.com/charmbracelet/bubbles/spinner"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"springmars.com/daily-goquery/errmsg"
	"springmars.com/daily-goquery/github"
	"strconv"
)

/*
@author pengshuo
@date 2021/8/11 11:28
version 1.0.0
desc:

*/

var (
	cyan   = lipgloss.NewStyle().Foreground(lipgloss.Color("#00FFFF"))
	green  = lipgloss.NewStyle().Foreground(lipgloss.Color("#32CD32"))
	gray   = lipgloss.NewStyle().Foreground(lipgloss.Color("#696969"))
	gold   = lipgloss.NewStyle().Foreground(lipgloss.Color("#B8860B"))
	purple = lipgloss.NewStyle().Foreground(lipgloss.Color("#800080"))
)

type Model struct {
	repos   []*github.Repo
	err     error
	spinner spinner.Model
	// 额外
	language  string
	dateRange string
}

func NewModel(language, dateRange string) Model {
	spinner := spinner.NewModel()
	spinner.Style = purple

	return Model{
		spinner:   spinner,
		language:  language,
		dateRange: dateRange,
	}
}

func (m Model) Init() tea.Cmd {
	return tea.Batch(
		spinner.Tick,
		func() tea.Msg {
			return fetchRepo(m.language, m.dateRange)
		},
	)
}

func fetchRepo(language, dateRange string) tea.Msg {
	repos, err := github.GetTrending(language, dateRange)
	if err != nil {
		return errmsg.ErrorMsg(err)
	}
	return repos
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "q", "ctrl+c", "esc":
			return m, tea.Quit
		default:
			return m, nil
		}

	case errmsg.ErrMsg:
		m.err = msg
		return m, nil

	case spinner.TickMsg:
		var cmd tea.Cmd
		m.spinner, cmd = m.spinner.Update(msg)
		return m, cmd

	case []*github.Repo:
		m.repos = msg
		return m, nil

	default:
		return m, nil
	}
}

func (m Model) View() string {
	var s string
	if m.err != nil {
		s = gold.Render(fmt.Sprintf("fetch trending failed: %v", m.err))
	} else if len(m.repos) > 0 {
		for _, repo := range m.repos {
			s += repoText(repo)
		}
		s += cyan.Render("--------------------------------------")
	} else {
		// 这里
		s = m.spinner.View() + gold.Render(" Fetching GitHub trending ...")
	}
	s += "\n\n"
	s += purple.Render("Press q or ctrl + c or esc to exit...")
	return s + "\n"
}

func repoText(repo *github.Repo) string {
	s := cyan.Render("--------------------------------------") + "\n"
	s += fmt.Sprintf(`Repo:  %s | Language:  %s | Stars:  %s | Forks:  %s | Stars today:  %s
`, cyan.Render(repo.Name), cyan.Render(repo.Lang), cyan.Render(strconv.Itoa(repo.Stars)),
		cyan.Render(strconv.Itoa(repo.Forks)), cyan.Render(strconv.Itoa(repo.Add)))
	s += fmt.Sprintf("Desc:  %s\n", green.Render(repo.Desc))
	s += fmt.Sprintf("Link:  %s\n", gray.Render(repo.Link))
	return s
}
