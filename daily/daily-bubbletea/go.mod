module springmars.com/daily-bubbletea

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.1 // indirect
	github.com/charmbracelet/bubbles v0.8.0 // indirect
	github.com/charmbracelet/bubbletea v0.14.1 // indirect
	github.com/charmbracelet/lipgloss v0.3.0 // indirect
	github.com/containerd/console v1.0.2 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.9.0 // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	golang.org/x/term v0.0.0-20210615171337-6886f2dfbf5b // indirect
	springmars.com/daily-goquery v1.0.0
)

replace springmars.com/daily-goquery => ../daily-goquery
