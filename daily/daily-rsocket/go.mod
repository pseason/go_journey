module springmars.com/daily/rsocket

go 1.16

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/panjf2000/ants/v2 v2.4.6 // indirect
	github.com/rsocket/rsocket-go v0.8.5 // indirect
	go.uber.org/atomic v1.9.0 // indirect
)
