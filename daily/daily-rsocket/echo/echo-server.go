package main

import (
	"context"
	"github.com/rsocket/rsocket-go"
	"github.com/rsocket/rsocket-go/payload"
	"github.com/rsocket/rsocket-go/rx/mono"
	"log"
)

/*
@author pengshuo
@date 2021/10/18 16:40
version 1.0.0
desc:

*/

func main() {
	err := rsocket.Receive().OnStart(func() {
		log.Println("tcp server start success at :9014")
	}).Acceptor(
		func(ctx context.Context, setup payload.SetupPayload, socket rsocket.CloseableRSocket) (rsocket.RSocket, error) {
			return rsocket.NewAbstractSocket(rsocket.RequestResponse(func(request payload.Payload) (response mono.Mono) {
				utf8, _ := request.MetadataUTF8()
				log.Println("request.payload", request.DataUTF8(), utf8)
				return mono.Just(request)
			})), nil
		},
	).Transport(rsocket.TCPServer().SetAddr(":9014").Build()).Serve(context.Background())
	log.Fatalln(err)
}
