package main

import (
	"context"
	"github.com/rsocket/rsocket-go"
	"github.com/rsocket/rsocket-go/payload"
	"log"
)

/*
@author pengshuo
@date 2021/10/18 16:45
version 1.0.0
desc:

*/

func main() {
	client, err := rsocket.Connect().
		SetupPayload(payload.NewString("hello", "world")).
		Transport(rsocket.TCPClient().SetHostAndPort("127.0.0.1", 9014).Build()).
		Start(context.Background())
	if err != nil {
		panic(err)
	}
	defer client.Close()

	// Send request
	result, err := client.RequestResponse(payload.NewString("你好", "世界")).Block(context.Background())
	if err != nil {
		panic(err)
	}
	utf8, _ := result.MetadataUTF8()
	log.Println("response.payload", result.DataUTF8(), utf8)
}
