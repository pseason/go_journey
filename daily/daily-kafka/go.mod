module springmars.com/daily/kafka

go 1.16

require (
	github.com/Shopify/sarama v1.29.1 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/net v0.0.0-20210825183410-e898025ed96a // indirect
)
