package conf

import (
	"flag"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

/*
@author pengshuo
@date 2021/8/27 14:12
version 1.0.0
desc:
	读取配置文件
*/
// const field
const (
	appName = "app.name"
	appPort = "app.port"
)

type appConf struct {
	Env  string //app env
	App  string //app name
	Port int
}

var (
	// 应用配置
	AppConf *appConf
	// 应用配置读取环境
	env string
)

// 加载应用配置(加载之前要先Append AppCfgInit)
func LoadAppConfig() {
	// 读取指定环境
	flag.String("env", "dev", "--env=dev 程序运行读取配置环境")
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	//解析flag viper绑定pflag
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	// 读取程序运行配置环境
	env = viper.Get("env").(string)
	// 设置配置文件目录、名称、格式
	viper.SetConfigName("app")
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	// 读取配置
	err := viper.ReadInConfig()
	if err != nil {
		panic("read config failed: " + err.Error())
	}
	// 解析参数，初始化配置
	execCfg2Init()
	// watch
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		// 解析参数
		execCfg2Init()
	})
}

// 解析基础并执行加载配置初始化
func execCfg2Init() {
	AppConf = &appConf{
		Env:  env,
		App:  viper.GetString(appName),
		Port: viper.GetInt(appPort),
	}
}
