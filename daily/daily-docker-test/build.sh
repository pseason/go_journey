#!/bin/sh
. /etc/profile
# docker build shell
go mod tidy
go build -o server
docker build -t ginapp:latest .