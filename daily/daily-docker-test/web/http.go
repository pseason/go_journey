package web

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"springmars.com/daily/docker/test/conf"
	"springmars.com/daily/docker/test/ginhttp"
)

/*
@author pengshuo
@date 2021/10/21 17:30
version 1.0.0
desc:

*/

func init() {
	ginhttp.CreateRoute("/hello", ginhttp.GET, func(c *gin.Context) {
		c.JSON(http.StatusOK, ginhttp.Success("hello go web docker test"))
	})

	ginhttp.CreateRoute("/info", ginhttp.GET, func(c *gin.Context) {
		c.JSON(http.StatusOK, ginhttp.Success(
			fmt.Sprintf("server[%s]run with env[%s] listen at 127.0.0.1:%d", conf.AppConf.App, conf.AppConf.Env, conf.AppConf.Port),
		))
	})

}
