package main

import (
	"springmars.com/daily/docker/test/conf"
	"springmars.com/daily/docker/test/ginhttp"
	_ "springmars.com/daily/docker/test/web"
)

/*
@author pengshuo
@date 2021/10/21 16:25
version 1.0.0
desc:

*/

func main() {
	// 加载配置
	conf.LoadAppConfig()
	// start gin web
	ginhttp.StartGin(conf.AppConf.Env, conf.AppConf.Port)

}
