package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/jjeffcaii/reactor-go"
	"github.com/jjeffcaii/reactor-go/mono"
	"time"
)

/*
@author pengshuo
@date 2021/10/12 15:40
version 1.0.0
desc: reactor go
	mono less then one
*/

func main() {
	monoSwitchIfEmpty()

	monoCancel()
	monoDoOnSubscribe()

	monoFlatMap()
	monoDelay()

	monoJust()
	monoCreate()
}

func monoSwitchIfEmpty() {
	mono.Empty().SwitchIfEmpty(mono.Just(42)).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return errors.New("test error")
	}).DoOnError(func(e error) {
		fmt.Println(e)
	}).Subscribe(context.Background())

	v, _ := mono.Empty().SwitchIfEmpty(mono.Just(42)).Block(context.Background())
	fmt.Println(v)
}

func monoCancel() {
	mono.Create(func(ctx context.Context, s mono.Sink) {
		s.Success(1)
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println("do no next", v)
		return nil
	}).DoOnSubscribe(func(ctx context.Context, su reactor.Subscription) {

	}).DoOnCancel(func() {
		fmt.Println("mono cancel")
	}).Subscribe(context.Background())
}

func monoDoOnSubscribe() {
	mono.Create(func(ctx context.Context, s mono.Sink) {
		s.Success(5)
	}).DoOnSubscribe(func(ctx context.Context, su reactor.Subscription) {
		fmt.Println("------subscribed!")
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println("mono do on subscribe", v)

		return nil
	}).Subscribe(context.Background())
}

func monoFlatMap() {
	mono.Just("hello").FlatMap(func(value reactor.Any) mono.Mono {
		str := value.(string) + ", FlatMap"
		return mono.Create(func(ctx context.Context, s mono.Sink) {
			s.Success(str)
		})
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).DoOnComplete(func() {
		fmt.Println("monoFlatMap-DoOnComplete")
	}).DoFinally(func(s reactor.SignalType) {
		fmt.Println("monoFlatMap-DoFinally", s.String())
	}).Subscribe(context.Background())
}

func monoDelay() {
	fmt.Println(time.Now().Unix())
	mono.Delay(time.Second * 2).DoOnNext(func(v reactor.Any) error {
		fmt.Println("Bingo!!!")
		fmt.Println(time.Now().Unix())
		return nil
	}).Block(context.Background())
}

func monoJust() {
	mono.Just("hello").Map(func(any reactor.Any) (reactor.Any, error) {
		return fmt.Sprintf("%s, world", any), nil
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).Subscribe(context.Background())
}

func monoCreate() {
	gen := func(ctx context.Context, sink mono.Sink) {
		sink.Success("World")
	}
	mono.Create(gen).Map(func(input reactor.Any) (output reactor.Any, err error) {
		output = fmt.Sprintf("%s, world", input)
		return
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).Subscribe(context.Background())

	mono.Create(func(ctx context.Context, s mono.Sink) {
		s.Error(errors.New("sink error"))
	}).DoOnComplete(func() {
		fmt.Println("monoCreate-DoOnComplete")
	}).DoFinally(func(s reactor.SignalType) {
		fmt.Println("monoCreate-DoFinally", s.String())
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println("----------", v)
		return nil
	}).DoOnError(func(e error) {
		fmt.Println(e)
	}).Subscribe(context.Background())
}
