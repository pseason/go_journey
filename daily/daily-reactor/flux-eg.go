package main

import (
	"context"
	"fmt"
	"github.com/jjeffcaii/reactor-go"
	"github.com/jjeffcaii/reactor-go/flux"
	"strings"
	"time"
)

/*
@author pengshuo
@date 2021/10/13 17:45
version 1.0.0
desc: reactor go
	flux more then one
*/

func main() {
	fluxSwitchOnFirst()

	fluxDelayElement()

	fluxFilter()
	fluxRange()
	fluxInterval()

	fluxCreate()
	fluxJust()
}

func fluxSwitchOnFirst() {
	flux.Just("golang", "I love golang.", "I love java.", "Awesome golang.", "I love ruby.").
		SwitchOnFirst(func(s flux.Signal, f flux.Flux) flux.Flux {
			first, ok := s.Value()
			if !ok {
				return f
			}
			return f.Filter(func(any reactor.Any) bool {
				return strings.Contains(any.(string), first.(string))
			})
		}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).Subscribe(context.Background())
}

func fluxDelayElement() {
	flux.Range(1, 10).DelayElement(time.Second * 1).Filter(func(any reactor.Any) bool {
		return any.(int) > 4
	}).DoOnDiscard(func(v reactor.Any) {
		fmt.Println("discard element:", time.Now().Unix(), v)
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println("next element:", time.Now().Unix(), v)
		return nil
	}).DoOnComplete(func() {
		fmt.Println("complete")
	}).DoFinally(func(s reactor.SignalType) {
		fmt.Println("finally")
	}).BlockLast(context.Background())
}

func fluxFilter() {
	flux.Range(0, 3).Filter(func(any reactor.Any) bool {
		return any == 2
	}).Map(func(any reactor.Any) (reactor.Any, error) {
		return fmt.Sprintf("filter,%v", any), nil
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).DoOnDiscard(func(v reactor.Any) {
		fmt.Println("--DoOnDiscard--", v)
	}).Subscribe(context.Background())
}

func fluxRange() {
	flux.Range(0, 3).Map(func(any reactor.Any) (reactor.Any, error) {
		return fmt.Sprintf("range,%v", any), nil
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).Subscribe(context.Background())
}

func fluxInterval() {
	flux.Interval(200 * time.Millisecond).Take(4).Map(func(any reactor.Any) (reactor.Any, error) {
		return fmt.Sprintf("interval,%v", any), nil
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).BlockLast(context.Background())
}

func fluxJust() {
	flux.Just("go", "java", "py").Map(func(any reactor.Any) (reactor.Any, error) {
		return fmt.Sprintf("hello,%s", any), nil
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).Subscribe(context.Background())
}

func fluxCreate() {
	flux.Create(func(ctx context.Context, sink flux.Sink) {
		sink.Next("xxx")
		sink.Next("yyy")
		sink.Next("zzz")
		sink.Complete()
	}).Map(func(any reactor.Any) (reactor.Any, error) {
		return fmt.Sprintf("hello,%s", any), nil
	}).DoOnNext(func(v reactor.Any) error {
		fmt.Println(v)
		return nil
	}).Subscribe(context.Background())

}
