module springmars.com/daily/memorycache

go 1.16

require (
	github.com/allegro/bigcache/v3 v3.0.1 // indirect
	github.com/bluele/gcache v0.0.2
)
