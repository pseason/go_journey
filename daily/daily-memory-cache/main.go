package main

import (
	"fmt"
	"github.com/allegro/bigcache/v3"
	"github.com/bluele/gcache"
	"time"
)

/*
@author pengshuo
@date 2021/10/25 14:36
version 1.0.0
desc:

*/

func main() {
	// g cache
	gc := gcache.New(20).LRU().Build()
	gc.Set("key", "ok")
	value, err := gc.Get("key")
	if err != nil {
		panic(err)
	}
	fmt.Println("Get:", value)

	gc.SetWithExpire("key", "ok", time.Second*10)
	value, _ = gc.Get("key")
	fmt.Println("Get:", value)
	// Wait for value to expire
	time.Sleep(time.Second * 10)

	value, err = gc.Get("key")
	if err != nil {
		panic(err)
	}
	fmt.Println("Get:", value)
	// big cache
	cache, _ := bigcache.NewBigCache(bigcache.DefaultConfig(10 * time.Minute))
	cache.Set("my-unique-key", []byte("value"))
	entry, _ := cache.Get("my-unique-key")
	fmt.Println(string(entry))
}
