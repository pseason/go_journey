package main

import (
	"fmt"
	"github.com/emicklei/proto"
	"net/http"
)

/*
@author pengshuo
@date 2021/10/8 17:21
version 1.0.0
desc:

*/

func eg1() {
	definition := fetchAndParse("https://ghproxy.com/https://raw.githubusercontent.com/gogo/protobuf/master/test/theproto3/theproto3.proto")
	count := counter{counts: map[string]int{}}
	proto.Walk(definition,
		proto.WithPackage(func(p *proto.Package) {
			fmt.Println("package:", p.Name)
		}),
		proto.WithService(count.handleService),
		proto.WithRPC(count.handleRPC),
		proto.WithImport(count.handleImport),
		proto.WithMessage(count.handleMessage),
		proto.WithEnum(count.handleEnum),
	)
	fmt.Println("count:", count)
}

type counter struct {
	counts map[string]int
}

func (c counter) handleService(s *proto.Service) {
	c.counts["service"] = c.counts["service"] + 1
}

func (c counter) handleMessage(s *proto.Message) {
	c.counts["message"] = c.counts["message"] + 1
}

func (c counter) handleEnum(s *proto.Enum) {
	c.counts["enum"] = c.counts["enum"] + 1
}

func (c counter) handleRPC(r *proto.RPC) {
	c.counts["rpc"] = c.counts["rpc"] + 1
}

func (c counter) handleImport(r *proto.Import) {
	c.counts["import"] = c.counts["import"] + 1
}

func fetchAndParse(url string) *proto.Proto {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(url, err)
	}
	defer resp.Body.Close()
	parser := proto.NewParser(resp.Body)
	def, err := parser.Parse()
	if err != nil {
		fmt.Println(url, err)
	}
	fmt.Println("elements:", len(def.Elements))
	return def
}
