package main

import (
	"fmt"
	"github.com/emicklei/proto"
	"os"
)

/*
@author pengshuo
@date 2021/10/8 17:22
version 1.0.0
desc:

*/

func eg2() {
	reader, _ := os.Open("hello.proto")
	defer reader.Close()

	parser := proto.NewParser(reader)
	definition, _ := parser.Parse()

	proto.Walk(definition,
		proto.WithService(handleService),
		proto.WithRPC(handleRPC),
		proto.WithMessage(handleMessage))
}

func handleService(s *proto.Service) {
	fmt.Println(s.Name)
}

func handleRPC(r *proto.RPC) {
	fmt.Println(r.Name)
}

func handleMessage(m *proto.Message) {
	fmt.Println(m.Name)
}
