package errmsg

/*
@author pengshuo
@date 2021/8/11 14:27
version 1.0.0
desc:

*/
type ErrMsg struct {
	error
}

func (e ErrMsg) Error() string {
	return e.error.Error()
}

func ErrorMsg(err error) ErrMsg {
	return ErrMsg{err}
}
