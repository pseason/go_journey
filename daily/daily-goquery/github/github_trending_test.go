package github

import (
	"fmt"
	"testing"
)

/*
@author pengshuo
@date 2021/8/11 14:11
version 1.0.0
desc:

*/
func BenchmarkText(b *testing.B) {
	b.StopTimer()
	trending, err := GetTrending("", "daily")
	b.StartTimer()
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, repo := range trending {
		fmt.Println(repo.Author, repo.Link)
	}
}
