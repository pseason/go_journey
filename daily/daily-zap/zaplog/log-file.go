package zaplog

import (
	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
)

/*
@author pengshuo
@date 2021/8/25 14:34
version 1.0.0
desc:

*/

var log *zap.Logger
var slog *zap.SugaredLogger

func init() {
	// new log
	log = zap.New(getZapCore(), zap.AddCaller())
	slog = log.Sugar()
}

func Terminate() {
	log.Sync()
	slog.Sync()
}

// zapcore.NewCore
func getZapCore() zapcore.Core {
	return zapcore.NewCore(
		// 编码器配置
		getJsonEncode(),
		// 打印到控制台和文件
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(getLumberJackLoggerWriter())),
		// 日志级别
		zap.DebugLevel,
	)
}

// json encode json 输出
func getJsonEncode() zapcore.Encoder {
	return zapcore.NewJSONEncoder(getNewProdEncoderConfig())
}

// Production Encoder Config 对日志进行格式化
func getNewProdEncoderConfig() zapcore.EncoderConfig {
	encoderConfig := zap.NewProductionEncoderConfig()
	// 时间格式化
	encoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout("2006-01-02 15:04:05.000")
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return encoderConfig
}

// LumberJack Logger Writer
func getLumberJackLoggerWriter() zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "./info.log",
		MaxSize:    50,   //在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups: 1500, //保留旧文件的最大个数
		MaxAge:     90,   //保留旧文件的最大天数
		Compress:   true, //是否压缩/归档旧文件
		LocalTime:  true,
	}
	return zapcore.AddSync(lumberJackLogger)
}

func SInfof(format string, args ...interface{}) {
	slog.Infof(format, args...)
}

func SInfo(msg string) {
	slog.Info(msg)
}
