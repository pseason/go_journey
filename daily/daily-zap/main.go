package main

import (
	"springmars.com/daily-zap/zaplog"
	_ "springmars.com/daily-zap/zaplog"
	"time"
)

/*
@author pengshuo
@date 2021/8/25 10:35
version 1.0.0
desc:

*/

func main() {
	defer terminate()

	simpleHttpGet("https://www.baidu.com")
}

func terminate() {
	zaplog.Terminate()
}

func simpleHttpGet(url string) {
	for {
		time.Sleep(time.Second * 2)
		zaplog.SInfof("Success fetching url=%s", url)
	}
}
