zap

1. 安装：go get -u go.uber.org/zap

2. Zap提供了两种类型的日志记录器—Sugared Logger和Logger
   
   在性能很好但不是很关键的上下文中，使用SugaredLogger。它比其他结构化日志记录包快4-10倍，并且支持结构化和printf风格的日志记录
   
   在每一微秒和每一次内存分配都很重要的上下文中，使用Logger。它甚至比SugaredLogger更快，内存分配次数也更少，但它只支持强类型的结构化日志记录    

   production logger默认记录调用函数信息、日期和时间等

3. 安装：go get -u github.com/natefinch/lumberjack
   
   Lumberjack Logger采用以下属性作为输入:
      Filename: 日志文件的位置
      MaxSize：在进行切割之前，日志文件的最大大小（以MB为单位）
      MaxBackups：保留旧文件的最大个数
      MaxAges：保留旧文件的最大天数
      Compress：是否压缩/归档旧文件

参考：https://github.com/uber-go/zap