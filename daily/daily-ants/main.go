package main

import (
	"fmt"
	"github.com/panjf2000/ants/v2"
	"springmars.com/daily-ants/pool"
	"time"
)

/*
@author pengshuo
@date 2021/8/10 19:33
version 1.0.0
desc:

*/

func main() {
	pool.InitPool()
	defer pool.ClosePool()

	for i := 0; i < 10; i++ {
		pool.DoTask(printInt, i)
	}

	time.Sleep(time.Second * 3)

	fmt.Printf("running goroutines: %d\n", ants.Running())
}

func printInt(num interface{}) {
	fmt.Println("print:", num)
}
