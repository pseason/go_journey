package pool

import "github.com/panjf2000/ants/v2"

/*
@author pengshuo
@date 2021/8/10 19:38
version 1.0.0
desc:
	pool of ants
*/

const (
	poolSize = 10
)

/* goroutine pool start */
func InitPool() {
	ants.NewPool(poolSize)
}

func DoTask(f func(data interface{}), data interface{}) {
	ants.Submit(func() {
		f(data)
	})
}

/* goroutine pool close */
func ClosePool() {
	ants.Release()
}
