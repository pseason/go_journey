package eg

import (
	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/AsynkronIT/protoactor-go/router"
	"log"
	"strconv"
	"time"
)

/*
@author pengshuo
@date 2022/2/14 15:05
version 1.0.0
desc: actor router demo

*/
type myMessage struct{ i int }

func (m *myMessage) Hash() string {
	return strconv.Itoa(m.i)
}

func E3() {
	system := actor.NewActorSystem()
	rootContext := system.Root
	act := func(context actor.Context) {
		switch msg := context.Message().(type) {
		case *myMessage:
			log.Printf("%v got message %d", context.Self(), msg.i)
		}
	}

	log.Println("default routing:")
	pid, _ := rootContext.SpawnNamed(actor.PropsFromFunc(act), "default")
	for i := 0; i < 10; i++ {
		rootContext.Send(pid, &myMessage{i})
	}

	time.Sleep(1 * time.Second)
	log.Println("Round robin routing:")
	pid, _ = rootContext.SpawnNamed(router.NewRoundRobinPool(5).WithFunc(act), "robin")
	for i := 0; i < 10; i++ {
		rootContext.Send(pid, &myMessage{i})
	}

	time.Sleep(1 * time.Second)
	log.Println("Random routing:")
	pid, _ = rootContext.SpawnNamed(router.NewRandomPool(5).WithFunc(act), "random")
	for i := 0; i < 10; i++ {
		rootContext.Send(pid, &myMessage{i})
	}

	time.Sleep(1 * time.Second)
	log.Println("ConsistentHash routing:")
	pid, _ = rootContext.SpawnNamed(router.NewConsistentHashPool(5).WithFunc(act), "conHash")
	for i := 0; i < 10; i++ {
		rootContext.Send(pid, &myMessage{i})
	}

	time.Sleep(1 * time.Second)
	log.Println("BroadcastPool routing:")
	pid, _ = rootContext.SpawnNamed(router.NewBroadcastPool(5).WithFunc(act), "Broadcast")
	for i := 0; i < 10; i++ {
		rootContext.Send(pid, &myMessage{i})
	}
	//_, _ = console.ReadLine()
}
