package eg

import (
	"fmt"
	"github.com/AsynkronIT/protoactor-go/actor"
	"time"
)

/*
@author pengshuo
@date 2022/2/11 14:13
version 1.0.0
desc: actor hello world

*/

type helloActor struct {
}

func (actor *helloActor) Receive(ctx actor.Context) {
	switch msg := ctx.Message().(type) {
	case Hello:
		fmt.Printf("Hello %s\n", msg.Who)
	case Hello2:
		fmt.Printf("Hello %s\n", msg.Who)
		ctx.Respond(Hello{Who: "Joe"})
	}
}

func E1() {
	// actorSystem props
	actorSystem := actor.NewActorSystem()
	props := actor.PropsFromProducer(func() actor.Actor {
		return &helloActor{}
	})
	// pid send message
	pid := actorSystem.Root.Spawn(props)
	actorSystem.Root.Send(pid, Hello{Who: "Roger"})
	result, _ := actorSystem.Root.RequestFuture(pid, Hello2{Who: "Joe"}, 5*time.Second).Result()
	fmt.Printf("result: %v\n", result)
	// sleep is for send message be processed
	time.Sleep(time.Second)
	// stop pid
	actorSystem.Root.Stop(pid)
	// actually block the main thread not exit
	//_, _ = console.ReadLine()
}
