package eg

import (
	"fmt"
	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/AsynkronIT/protoactor-go/mailbox"
	"time"
)

/*
@author pengshuo
@date 2022/2/14 10:58
version 1.0.0
desc: go actor behavior

*/

type helloBehaviorActor struct {
	behavior actor.Behavior
}

func (actor *helloBehaviorActor) Receive(ctx actor.Context) {
	actor.behavior.Receive(ctx)
}

func (actor *helloBehaviorActor) One(ctx actor.Context) {
	switch msg := ctx.Message().(type) {
	case Hello:
		fmt.Printf("Hello %v\n", msg.Who)
		actor.behavior.Become(actor.Other)
	}
}

func (actor *helloBehaviorActor) Other(ctx actor.Context) {
	switch msg := ctx.Message().(type) {
	case Hello:
		fmt.Printf("%v, ey we are now handling messages in another behavior \n", msg.Who)
	}
}

func newHelloBehaviorActor() actor.Actor {
	act := &helloBehaviorActor{
		behavior: actor.NewBehavior(),
	}
	act.behavior.Become(act.One)
	return act
}

func E2() {
	// actorSystem and props
	actorSystem := actor.NewActorSystem()
	props := actor.PropsFromProducer(newHelloBehaviorActor).WithMailbox(mailbox.Bounded(100))
	// pid
	pid := actorSystem.Root.Spawn(props)
	// send message
	actorSystem.Root.Send(pid, Hello{Who: "Roger"})
	actorSystem.Root.Send(pid, Hello{Who: "Lucy"})
	actorSystem.Root.Send(pid, Hello{Who: "Jack"})
	// sleep
	time.Sleep(time.Second * 1)
	actorSystem.Root.Stop(pid)
	// block
	//_, _ = console.ReadLine()
}
