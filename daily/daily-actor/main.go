package main

import (
	"fmt"
	"springmars.com/daily/actor/eg"
)

/*
@author pengshuo
@date 2022/2/11 14:13
version 1.0.0
desc: actor main enter

*/

func main() {
	// example1
	eg.E1()
	fmt.Println("----------------------")
	// example2
	eg.E2()
	fmt.Println("----------------------")
	// example3
	eg.E3()
	fmt.Println("----------------------")
	// example4
	eg.E4()
}
