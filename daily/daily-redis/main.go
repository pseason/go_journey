package main

import (
	"github.com/gomodule/redigo/redis"
	"log"
	"time"
)

/*
@author pengshuo
@date 2021/10/26 15:15
version 1.0.0
desc:
	10.198.141.232:6388
	dkkaI#27KlmQ-3k2OPj
*/

func main() {
	poolUsage()
	watchUsage()
	pubSubConnUsage()
	pub2subUsage()
	sendDoUsage()
	sendFlushUsage()
	existUsage()
	doUsage()
}

func poolUsage() {
	conn := getRedisPoolConn().Get()
	defer conn.Close()
	if reply, err := conn.Do("set", "s-test", "xxx"); err == nil {
		log.Printf("do reply: %v \n", reply)
	}
	if reply, err := conn.Do("get", "s-test"); err == nil {
		log.Printf("do reply: %v \n", string(reply.([]byte)))
	}
	if reply, err := conn.Do("del", "s-test"); err == nil {
		log.Printf("do reply: %v \n", reply)
	}
	time.Sleep(time.Second)
}

func watchUsage() {
	conn := getRedisConn()
	defer conn.Close()

	key := "foo"
	// set one
	conn.Do("set", key, 1)
	if reply, err := conn.Do("get", key); err == nil {
		log.Printf("do reply: %v \n", string(reply.([]byte)))
	}
	conn.Send("WATCH", key)
	conn.Send("MULTI")
	conn.Send("INCR", key)
	conn.Do("EXEC")
	if reply, err := conn.Do("get", key); err == nil {
		log.Printf("do reply: %v \n", string(reply.([]byte)))
	} else {
		log.Println(err)
	}
	conn.Do("del", key)

	time.Sleep(time.Second)
}

func pubSubConnUsage() {
	psc := redis.PubSubConn{Conn: getRedisConn()}

	psc.Subscribe("example")
	go func() {
		for true {
			recv := psc.Receive()
			log.Printf("pub sub conn recv: %v \n", recv)
		}
	}()

	time.Sleep(time.Second)
}

// Publish and Subscribe
//
// Use the Send, Flush and Receive methods to implement Pub/Sub subscribers.
//
func pub2subUsage() {
	conn := getRedisConn()
	defer conn.Close()
	go func() {
		for true {
			if recv, err := conn.Receive(); err == nil {
				log.Printf("pub2sub recv: %v \n", recv)
			}
		}
	}()
	conn.Send("SUBSCRIBE", "publish-example")
	conn.Flush()

	time.Sleep(time.Second)
}

// multi send do
func sendDoUsage() {
	conn := getRedisConn()
	defer conn.Close()

	conn.Send("MULTI")
	conn.Send("set", "s-test", "xxx")
	conn.Send("get", "s-test")
	conn.Send("del", "s-test")
	if reply, err := conn.Do("EXEC"); err == nil {
		log.Printf("send do recv: %v \n", reply)
	}
	time.Sleep(time.Second)
}

// send flush receive
func sendFlushUsage() {
	conn := getRedisConn()
	defer conn.Close()

	go func() {
		for true {
			if recv, err := conn.Receive(); err == nil {
				log.Printf("send flush recv: %v \n", recv)
			}
		}
	}()

	conn.Send("set", "s-test", "xxx")
	conn.Send("get", "s-test")
	conn.Send("del", "s-test")
	conn.Flush()
	time.Sleep(time.Second)
}

func existUsage() {
	conn := getRedisConn()
	defer conn.Close()

	if boo, err := redis.Bool(conn.Do("EXISTS", "s-test-s")); err == nil {
		log.Printf("do key exist: %v \n", boo)
	}

	time.Sleep(time.Second)
}

func doUsage() {
	conn := getRedisConn()
	defer conn.Close()

	if reply, err := conn.Do("set", "s-test", "xxx"); err == nil {
		log.Printf("do reply: %v \n", reply)
	}
	if reply, err := conn.Do("get", "s-test"); err == nil {
		log.Printf("do reply: %v \n", string(reply.([]byte)))
	}
	if reply, err := conn.Do("del", "s-test"); err == nil {
		log.Printf("do reply: %v \n", reply)
	}
	time.Sleep(time.Second)
}

// get redis conn
func getRedisConn() redis.Conn {
	conn, err := redis.Dial("tcp", "10.198.141.232:6388", redis.DialPassword("dkkaI#27KlmQ-3k2OPj"))
	if err != nil {
		log.Println("redis connect error ", err)
		return nil
	}
	return conn
}

func getRedisPoolConn() *redis.Pool {
	return newPoolWithPwd("10.198.141.232:6388", "dkkaI#27KlmQ-3k2OPj")
}

func newPoolWithPwd(addr, pwd string) *redis.Pool {
	return &redis.Pool{
		MaxActive:       10,
		MaxIdle:         2,
		IdleTimeout:     300 * time.Second,
		MaxConnLifetime: 600 * time.Second,
		// Dial or DialContext must be set. When both are set, DialContext takes precedence over Dial.
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", addr, redis.DialPassword(pwd))
		},
	}
}

func newPool(addr string) *redis.Pool {
	return &redis.Pool{
		MaxActive:       10,
		MaxIdle:         2,
		IdleTimeout:     240 * time.Second,
		MaxConnLifetime: 600 * time.Second,
		// Dial or DialContext must be set. When both are set, DialContext takes precedence over Dial.
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", addr)
		},
	}
}
