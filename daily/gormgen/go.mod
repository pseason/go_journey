module springmars.com/daily/gormgen

go 1.14

require (
	github.com/jinzhu/gorm v1.9.16
	gorm.io/gorm v1.20.2 // indirect
)
