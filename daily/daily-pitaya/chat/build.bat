@echo off

set DIR=%~dp0
set PROTO=%~dp0proto
set OUT=%~dp0base

echo ---------------- start build proto go files ---------------

cd /d %PROTO%

for /f "delims=" %%i in ('dir /b *.proto') do (
	echo ---------------- %%i
	protoc -I . --go_out=%OUT% %%i
)

echo ---------------- finish build go proto files ---------------

pause
