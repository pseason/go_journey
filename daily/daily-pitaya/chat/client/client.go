package main

import (
	"github.com/topfreegames/pitaya/v2/client"
	"github.com/topfreegames/pitaya/v2/conn/message"
	"github.com/topfreegames/pitaya/v2/session"
	"google.golang.org/protobuf/proto"
	"log"
	"springmars.com/daily/pitaya/chat/base/protobuf"
	"springmars.com/daily/pitaya/chat/base/util"
	"time"
)

/*
@author pengshuo
@date 2021/11/17 10:28
version 1.0.0
desc:
	client
*/

var (
	pClient client.PitayaClient

	handshake = &session.HandshakeData{
		Sys: session.HandshakeClientData{
			Platform:    "mac",
			LibVersion:  "0.3.5-release",
			BuildNumber: "20",
			Version:     "1.0.0",
		},
		User: map[string]interface{}{
			"age": 30,
		},
	}

	disconnectedCh = make(chan bool, 1)

	prettyJSON = true

	addr = "127.0.0.1:19001"

	uid = ""
)

func main() {

	pClient = client.New(5)
	pClient.SetClientHandshakeData(handshake)

	err := pClient.ConnectTo(addr)
	if err != nil {
		log.Println(err)
		return
	}
	defer pClient.Disconnect()

	log.Println("------------------ Connected ... ------------------ ")
	// 接收消息
	go readServerMessages(func(msg *message.Message) {
		route := msg.Route
		log.Printf("route %s \n", route)
		resp := &protobuf.ServerResponse{}
		err := proto.Unmarshal(msg.Data, resp)
		if err != nil {
			log.Printf("------------------ unmarshal msg error %s \n", err.Error())
			return
		}
		if resp.GetCode() == protobuf.Code_Success {
			authRes := &protobuf.AuthRes{}
			resp.GetMsg().UnmarshalTo(authRes)
			uid = authRes.Uid

			log.Printf("------------------ auth success uid: %s \n", uid)
			// 发送 加入 消息
			sendMsg(pClient, "Gateway.Join", &protobuf.JoinReq{
				Secret: uid,
			})
		} else {
			log.Printf("------------------ auth error %s \n", resp.GetErr())
		}
	})
	// 发送认证
	sendMsg(pClient, "Gateway.Auth", &protobuf.AuthReq{
		Name: "lucy",
	})

	time.Sleep(time.Second * 120)
}

func sendMsg(pClient client.PitayaClient, route string, msg proto.Message) {
	// 连接状态
	if pClient.ConnectedStatus() {
		bytes := util.Marshal2Bytes(msg)
		pClient.SendRequest(route, bytes)
	}
}

func requestMsg(pClient client.PitayaClient, route, uid, rpcServer, rpcRouter string, msg proto.Message) {
	// 连接状态
	if pClient.ConnectedStatus() {
		req := util.ServerRequest(uid, rpcServer, rpcRouter, msg)
		bytes := util.Marshal2Bytes(req)
		pClient.SendRequest(route, bytes)
	}
}

func notifyMsg(pClient client.PitayaClient, route, uid, rpcServer, rpcRouter string, msg proto.Message) {
	// 连接状态
	if pClient.ConnectedStatus() {
		req := util.ServerRequest(uid, rpcServer, rpcRouter, msg)
		bytes := util.Marshal2Bytes(req)
		pClient.SendNotify(route, bytes)
	}
}

func readServerMessages(callback func(msg *message.Message)) {
	channel := pClient.MsgChannel()
	for {
		select {
		case <-disconnectedCh:
			close(disconnectedCh)
			return
		case m := <-channel:
			callback(m)
		}
	}
}
