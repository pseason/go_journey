module springmars.com/daily/pitaya/chat/client

go 1.16

require (
	github.com/topfreegames/pitaya/v2 v2.0.4
	google.golang.org/protobuf v1.25.1-0.20200805231151-a709e31e5d12
	springmars.com/daily/pitaya/chat/base v1.0.0
)

replace springmars.com/daily/pitaya/chat/base => ../base
