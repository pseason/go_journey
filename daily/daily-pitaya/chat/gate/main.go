package main

import (
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/constants"
	"github.com/topfreegames/pitaya/v2/modules"
	base "springmars.com/daily/pitaya/chat/base"
	"springmars.com/daily/pitaya/chat/base/util"
	"springmars.com/daily/pitaya/chat/gate/component"
	"strconv"
	"time"
)

/*
@author pengshuo
@date 2021/11/16 15:18
version 1.0.0
desc:
	gate server
*/

const (
	gatewayRouter = "Gateway"
	heartbeatTime = time.Second * 15
)

var (
	gate    pitaya.Pitaya
	storage *modules.ETCDBindingStorage

	// args
	serverPort = 19001
	rpcPort    = 19002
)

func main() {
	var meta = map[string]string{
		constants.GRPCHostKey: util.GetLocalIp(base.EtcdUrl),
		constants.GRPCPortKey: strconv.Itoa(rpcPort),
	}
	conf := &base.PitayaAppConf{
		ServerType:     base.GateServer,
		ServerPort:     serverPort,
		RpcServerPort:  rpcPort,
		ServerMetadata: meta,
		ETCDEndpoints:  []string{base.EtcdUrl},
		ETCDPrefix:     base.EtcdPrefix,
	}
	gate, storage = base.CreateApp(*conf)
	// defer app shutdown
	defer gate.Shutdown()
	// register etcd binding storage
	gate.RegisterModule(storage, "bindingStorage")
	// register router
	base.RegisterRoute(gate, component.NewGate(gate), gatewayRouter)
	// set heartbeat time
	gate.SetHeartbeatTime(heartbeatTime)
	// app start
	gate.Start()
}
