package data

import (
	"github.com/topfreegames/pitaya/v2/session"
	"log"
	"springmars.com/daily/pitaya/chat/base"
	"springmars.com/daily/pitaya/chat/base/util"
	"sync"
)

/*
@author pengshuo
@date 2021/11/16 19:18
version 1.0.0
desc:

*/

// GatePlayer player
type GatePlayer struct {
	Name         string `json:"name"`
	UID          string `json:"uid"`
	Session      session.Session
	ChatServerId string `json:"chatServerId"`
}

var (
	onlinePlayers sync.Map // name-player
	players       sync.Map // uid-player
)

func (onlinePlayer *GatePlayer) GetRemoteServerId(serverType string) string {
	if serverType == base.ChatServer {
		return onlinePlayer.ChatServerId
	}
	return base.DefaultServer
}

func PlayerOnline(uid string, session session.Session) *GatePlayer {
	// 从tmp获取player
	player := new(GatePlayer)
	util.CacheGet(uid, player)
	player.Session = session
	// 存入 online player
	onlinePlayers.Store(player.Name, player)
	players.Store(uid, player)
	log.Printf("player [%s] online, uid: [%s] \n", player.Name, uid)
	// 删除 tmp player
	err := util.CacheDelete(uid)
	if err != nil {
		log.Printf("delete tmp player[%s] error\n%v \n", uid, err)
	}
	return player
}

func PlayerOffline(uid string) {
	data, loaded := players.LoadAndDelete(uid)
	if loaded {
		player := data.(*GatePlayer)
		onlinePlayers.Delete(player.Name)
		log.Printf("player [%s] offline, uid: [%s] \n", player.Name, uid)
	}
}

func PutPlayer2Tmp(uid, name string) {
	err := util.CachePut(uid, &GatePlayer{
		UID:  uid,
		Name: name,
	})
	if err != nil {
		log.Printf("put [%s-%s]player to tmp error\n%v \n", uid, name, err)
	}
}

func OnlinePlayerExist(name string) (foo bool) {
	_, foo = onlinePlayers.Load(name)
	return
}

// PlayerIsOnline player is online
func PlayerIsOnline(uid string) (*GatePlayer, bool) {
	data, ok := players.Load(uid)
	if ok {
		return data.(*GatePlayer), ok
	}
	return nil, ok
}

func TmpPlayerExist(uid string) bool {
	return util.CacheContains(uid)
}
