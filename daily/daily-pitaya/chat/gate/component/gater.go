package component

import (
	"context"
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/component"
	"github.com/topfreegames/pitaya/v2/session"
	"github.com/topfreegames/pitaya/v2/timer"
	"log"
	"springmars.com/daily/pitaya/chat/base"
	"springmars.com/daily/pitaya/chat/base/protobuf"
	"springmars.com/daily/pitaya/chat/base/util"
	"springmars.com/daily/pitaya/chat/gate/data"
	"time"
)

/*
@author pengshuo
@date 2021/11/16 16:06
version 1.0.0
desc: login component

*/

// Gate groupName
const groupName = "gateway"

// Gate component
type Gate struct {
	component.Base
	timer *timer.Timer
	app   pitaya.Pitaya
}

// NewGate returns a new gate
func NewGate(app pitaya.Pitaya) *Gate {
	return &Gate{
		app: app,
	}
}

// Init runs on service initialization
func (r *Gate) Init() {
	r.app.GroupCreate(context.Background(), groupName)
}

// AfterInit component lifetime callback
func (r *Gate) AfterInit() {
	r.timer = pitaya.NewTimer(time.Second*30, func() {
		count, err := r.app.GroupCountMembers(context.Background(), groupName)
		if err != nil {
			log.Printf("Gate Online UserCount Stats: error=> %s \n", err.Error())
		} else {
			log.Printf("Gate Online UserCount Stats: Time=> %s ,Count=> %d \n", util.GetDateTimeString(), count)
		}
	})
}

// Auth online auth
func (r *Gate) Auth(ctx context.Context, auth *protobuf.AuthReq) (*protobuf.ServerResponse, error) {
	// 此处直接校验user是否存在
	name := auth.Name
	if data.OnlinePlayerExist(name) {
		return util.ReplyError(protobuf.Code_AuthError, "auth error reason is user exist"), nil
	}
	// binding session uid
	snowflake := util.GenSnowFlakeId()
	s := r.app.GetSessionFromCtx(ctx)
	err := s.Bind(ctx, snowflake)
	if err != nil {
		return util.ReplyError(protobuf.Code_AuthError, "auth error reason is bind session error"), nil
	}
	// put user to tmp
	data.PutPlayer2Tmp(snowflake, name)
	// response
	return util.ReplySuccess(&protobuf.AuthRes{
		Uid: snowflake,
	}), nil
}

// Join online gate
func (r *Gate) Join(ctx context.Context, join *protobuf.JoinReq) (*protobuf.ServerResponse, error) {
	// 只检验存不存缓存即刻
	secret := join.Secret
	if !data.TmpPlayerExist(secret) {
		return util.ReplyError(protobuf.Code_JoinError, "join error reason is not auth"), nil
	}
	// player online
	s := r.app.GetSessionFromCtx(ctx)
	uid := s.UID()
	// 加入Group
	err := r.app.GroupAddMember(ctx, groupName, s.UID())
	if err != nil {
		log.Printf("uid %s join group error %s \n", uid, err.Error())
		return util.ReplyError(protobuf.Code_JoinError, "join error reason is not auth"), nil
	}
	// player online
	r.playerOpt(base.Online, uid, s)
	// 并设置session关闭，移出Group
	s.OnClose(func() {
		r.app.GroupRemoveMember(ctx, groupName, s.UID())
		// player offline
		r.playerOpt(base.Offline, uid, nil)
	})
	// response
	return util.ReplyEmptyResponse(), nil
}

func (r *Gate) playerOpt(opt int, uid string, session session.Session) {
	switch opt {
	case base.Online:
		// player online
		onlinePlayer := data.PlayerOnline(uid, session)
		// 发送请求
		res := &protobuf.PlayerOptRes{}
		err := r.app.RPC(context.Background(), "ChatServer.ChatRoom.PlayerOpt", res, &protobuf.PlayerOptReq{
			Opt: base.Online,
			Uid: uid,
		})
		if err != nil {
			log.Printf("[playerOpt] uid %s online error %s \n", uid, err.Error())
			break
		} else {
			if res.Status != base.StatusSuccess {
				log.Printf("[playerOpt] uid %s online bind error \n", uid)
				break
			}
			// 绑定 ChatServerId
			onlinePlayer.ChatServerId = res.ServerId
			log.Printf("[playerOpt] uid %s online success \n", uid)
		}
	case base.Offline:
		// player offline
		data.PlayerOffline(uid)
		// 发送请求
		res := &protobuf.PlayerOptRes{}
		err := r.app.RPC(context.Background(), "ChatServer.ChatRoom.PlayerOpt", res, &protobuf.PlayerOptReq{
			Opt: base.Offline,
			Uid: uid,
		})
		if err != nil {
			log.Printf("[playerOpt] uid %s offline error %s \n", uid, err.Error())
			break
		}
		log.Printf("[playerOpt] uid %s offline unbind success \n", uid)
	}
}

// Message receive message from client reply response
func (r *Gate) Message(ctx context.Context, msg *protobuf.ServerRequest) (*protobuf.ServerResponse, error) {
	resp := &protobuf.ServerResponse{}
	// serverType and route
	server := msg.GetServer()
	route := msg.GetRoute()
	// check give uid is equals to session uid
	// check player is online
	uid := msg.Gid
	s := r.app.GetSessionFromCtx(ctx)
	sid := s.UID()
	if player, b := data.PlayerIsOnline(uid); b && uid == sid {
		name := player.Name
		formatRoute := base.FormatRoute(server, route)
		log.Printf("[Message] receive message from player[%s], router[%s] \n", name, formatRoute)
		// 获取onlinePlayer绑定的serverType serverId
		remoteServerId := player.GetRemoteServerId(server)
		// 验证通过发送消息
		if len(remoteServerId) > 0 && remoteServerId != base.DefaultServer {
			err := r.app.RPCTo(ctx, remoteServerId, formatRoute, resp, msg)
			if err != nil {
				errStr := err.Error()
				resp.ReplyError(protobuf.Code_NetworkError, errStr)
				// log
				log.Printf("[Message] player[%s] rpc to [%s] error: %s \n", name, formatRoute, errStr)
			}
		} else {
			log.Printf("[Message] player[%s] receive message from client ,server[%s] not found \n", name, server)
			resp.ReplyError(protobuf.Code_ServerNotFound, "server not found")
		}
	} else {
		log.Printf("[Message] receive message from client %s ,player is not online \n", uid)
		resp.ReplyError(protobuf.Code_PlayerNotOnline, "player is not online")
	}
	return resp, nil
}

// Notify receive message from client no response
func (r *Gate) Notify(ctx context.Context, msg *protobuf.ServerRequest) {
	// serverType and route
	server := msg.GetServer()
	route := msg.GetRoute()
	// check give uid is equals to session uid
	// check player is online
	uid := msg.Gid
	s := r.app.GetSessionFromCtx(ctx)
	sid := s.UID()
	if player, b := data.PlayerIsOnline(uid); b && uid == sid {
		name := player.Name
		formatRoute := base.FormatRoute(server, route)
		log.Printf("[Notify] receive message from player[%s], router[%s] \n", name, formatRoute)
		// 获取onlinePlayer绑定的serverType serverId
		remoteServerId := player.GetRemoteServerId(server)
		// 验证通过发送消息
		if len(remoteServerId) > 0 && remoteServerId != base.DefaultServer {
			err := r.app.RPCTo(ctx, remoteServerId, formatRoute, &protobuf.ServerResponse{}, msg)
			if err != nil {
				log.Printf("[Notify] player[%s] rpc to [%s] error: %s \n", name, formatRoute, err.Error())
			}
		} else {
			log.Printf("[Notify] player[%s] receive message from client ,server[%s] not found \n", name, server)
		}
	} else {
		log.Printf("[Notify] receive message from client %s ,player is not online \n", uid)
	}
}
