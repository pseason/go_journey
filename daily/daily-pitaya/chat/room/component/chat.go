package component

import (
	"context"
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/component"
	"log"
	"springmars.com/daily/pitaya/chat/base"
	"springmars.com/daily/pitaya/chat/base/protobuf"
	"springmars.com/daily/pitaya/chat/base/util"
)

/*
@author pengshuo
@date 2021/11/29 10:19
version 1.0.0
desc:

*/

// Chat component
type Chat struct {
	component.Base
	app pitaya.Pitaya
}

// NewChat returns a new chat
func NewChat(app pitaya.Pitaya) *Chat {
	return &Chat{
		app: app,
	}
}

func (r *Chat) PlayerOpt(ctx context.Context, msg *protobuf.PlayerOptReq) (*protobuf.PlayerOptRes, error) {
	uid := msg.Uid

	switch msg.Opt {
	case base.Online:
		log.Printf("Online session success: %v", uid)
		// 返回当前绑定的serverId
		return &protobuf.PlayerOptRes{
			ServerId: r.app.GetServerID(),
			Status:   base.StatusSuccess,
		}, nil
	case base.Offline:
		log.Printf("Offline session success: %v", uid)

		return &protobuf.PlayerOptRes{
			ServerId: r.app.GetServerID(),
			Status:   base.StatusSuccess,
		}, nil
	default:
		return &protobuf.PlayerOptRes{
			Status: base.StatusFail,
		}, nil
	}
}

func (r *Chat) OnMessage(ctx context.Context, msg *protobuf.ServerRequest) (*protobuf.ServerResponse, error) {
	formatRoute := base.FormatRoute(msg.Server, msg.Route)
	log.Printf("OnMessage: %v \n", formatRoute)

	_, err := r.app.SendPushToUsers("OnMessagePush", util.ReplySuccess(&protobuf.JoinRes{
		Status: base.StatusFail,
	}), []string{msg.Gid}, base.GateServer)

	if err != nil {
		// log printf error
		log.Printf("OnMessage SendPushToUsers: %v \n", err)
	}

	return util.ReplySuccess(&protobuf.AuthRes{
		Uid: msg.Gid,
	}), nil
}
