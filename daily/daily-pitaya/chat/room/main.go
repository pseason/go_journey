package main

import (
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/constants"
	"github.com/topfreegames/pitaya/v2/modules"
	"springmars.com/daily/pitaya/chat/base"
	"springmars.com/daily/pitaya/chat/base/util"
	"springmars.com/daily/pitaya/chat/room/component"
	"strconv"
	"time"
)

/*
@author pengshuo
@date 2021/11/29 9:45
version 1.0.0
desc:
	room server
*/

const (
	chatRoomRouter = "ChatRoom"
	heartbeatTime  = time.Second * 15
)

var (
	chat    pitaya.Pitaya
	storage *modules.ETCDBindingStorage

	// args
	serverPort = 19003
	rpcPort    = 19004
)

func main() {
	var meta = map[string]string{
		constants.GRPCHostKey: util.GetLocalIp(base.EtcdUrl),
		constants.GRPCPortKey: strconv.Itoa(rpcPort),
	}
	conf := &base.PitayaAppConf{
		ServerType:     base.ChatServer,
		ServerPort:     serverPort,
		RpcServerPort:  rpcPort,
		ServerMetadata: meta,
		ETCDEndpoints:  []string{base.EtcdUrl},
		ETCDPrefix:     base.EtcdPrefix,
	}
	chat, storage = base.CreateApp(*conf)
	// defer app shutdown
	defer chat.Shutdown()
	// register etcd binding storage
	chat.RegisterModule(storage, "bindingStorage")
	// register remote router
	base.RegisterRemoteRoute(chat, component.NewChat(chat), chatRoomRouter)
	// set heartbeat time
	chat.SetHeartbeatTime(heartbeatTime)
	// app start
	chat.Start()
}
