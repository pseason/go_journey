module springmars.com/daily/pitaya/chat/room

go 1.16

require (
	github.com/topfreegames/pitaya/v2 v2.0.4
	springmars.com/daily/pitaya/chat/base v1.0.0
)

replace springmars.com/daily/pitaya/chat/base => ../base
