### chat room

- 使用 protobuf 作为数据协议
- 使用 pitaya grpc 实现聊天集群服务器组
- 聊天室需join  
- 聊天室服务器组支持多种聊天室类型，包括：
    - 单人聊天室
    - 群聊天室
    - 公共聊天室

### etcd opt
```bash
### get key with prefix
etcdctl get --from-key ''

### get key value
etcdctl get key

### del key
etcdctl del key

### del key with prefix
etcdctl del --from-key ''
```
#### 备注
```
codec.PacketDecoder 
```
```go
// Message receive message from client no response
func (r *Gate) Message(ctx context.Context, msg *protobuf.BaseMessage) (*protobuf.BaseMessage, error) {
	// check give uid is equals to session uid
	// check player is online
	uid := msg.Uid
	session := r.app.GetSessionFromCtx(ctx)
	sid := session.UID()
	if player, b := data.PlayerIsOnline(uid); b && uid == sid {
		name := player.Name
        server := msg.Server
        route := msg.Route
		log.Printf("[NotifyMessage] receive message from player[%s-%s], router[%s.%s] \n", name, uid, server, route)
		// todo chat server 默认会与 uid 绑定, 或者通过其他方式绑定
		_, err := r.app.SendPushToUsers(route, msg.Msg, []string{uid}, server)
		if err != nil {
			log.Printf("[Message] player[%s-%s] push message to router[%s.%s] error: %s \n", name, uid, server, route, err.Error())
		}
	} else {
		log.Printf("[Message] receive message from client %s ,player is not online \n", uid)
	}
	return nil, nil
}
```