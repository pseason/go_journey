package util

import (
	"fmt"
	"github.com/allegro/bigcache/v3"
	"log"
	"time"
)

/*
@author pengshuo
@date 2021/11/16 18:11
version 1.0.0
desc:
	big cache
*/

var cache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(24 * time.Hour))

// CacheGet key
func CacheGet(key string, data interface{}) {
	bytes, err := cache.Get(key)
	if err != nil {
		log.Printf("get %s from cache error %v \n", key, err)
		return
	}
	err = Unmarshal(bytes, data)
	if err != nil {
		log.Printf("get %s from cache Unmarshal error %v \n", key, err)
	}
}

func CacheContains(key string) bool {
	_, err := cache.Get(key)
	if err == nil {
		return true
	}
	return false
}

// CachePut key value
func CachePut(key string, value interface{}) error {
	bytes := Marshal(value)
	if bytes == nil {
		return fmt.Errorf("marshal %s %v error", key, value)
	}
	return cache.Set(key, bytes)
}

// CacheDelete key
func CacheDelete(key string) error {
	return cache.Delete(key)
}
