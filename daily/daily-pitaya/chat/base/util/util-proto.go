package util

import (
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/anypb"
	"log"
	"springmars.com/daily/pitaya/chat/base/protobuf"
)

/*
@author pengshuo
@date 2021/11/26 10:43
version 1.0.0
desc:

*/

// Marshal2Bytes marshal proto message to bytes
func Marshal2Bytes(m proto.Message) []byte {
	bytes, err := proto.Marshal(m)
	if err != nil {
		log.Printf("[Marshal2Bytes] marshal proto message to bytes error: %s \n", err.Error())
		return nil
	}
	return bytes
}

// ReplySuccess is a reply success message
func ReplySuccess(m proto.Message) *protobuf.ServerResponse {
	if m == nil {
		return &protobuf.ServerResponse{
			Code: protobuf.Code_ProtocError,
		}
	} else {
		gotAny := new(anypb.Any)
		err := gotAny.MarshalFrom(m)
		if err != nil {
			log.Printf("[ReplySuccess] marshal proto message to any error: %s \n", err.Error())
			return &protobuf.ServerResponse{
				Code: protobuf.Code_ProtocError,
			}
		}
		return &protobuf.ServerResponse{
			Msg: gotAny,
		}
	}
}

// ReplyEmptyResponse is a reply success message
func ReplyEmptyResponse() *protobuf.ServerResponse {
	return &protobuf.ServerResponse{
		Code: protobuf.Code_Success,
	}
}

// ReplyError is a reply error message
func ReplyError(code protobuf.Code, err string) *protobuf.ServerResponse {
	return &protobuf.ServerResponse{
		Code: code,
		Err:  err,
	}
}

// ServerRequest is a rpc request message
func ServerRequest(uid, rpcServer, rpcRoute string, m proto.Message) *protobuf.ServerRequest {
	if m == nil {
		return &protobuf.ServerRequest{
			Gid:    uid,
			Server: rpcServer,
			Route:  rpcRoute,
		}
	} else {
		gotAny := new(anypb.Any)
		err := gotAny.MarshalFrom(m)
		if err != nil {
			log.Printf("[ServerRequest] marshal proto message to any error: %s \n", err.Error())
			return &protobuf.ServerRequest{
				Gid:    uid,
				Server: rpcServer,
				Route:  rpcRoute,
			}
		}
		return &protobuf.ServerRequest{
			Gid:    uid,
			Server: rpcServer,
			Route:  rpcRoute,
			Msg:    gotAny,
		}
	}
}
