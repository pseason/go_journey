package util

import (
	jsoniter "github.com/json-iterator/go"
	"log"
)

/*
@author pengshuo
@date 2021/11/16 18:27
version 1.0.0
desc:
	json util
*/

var json = jsoniter.ConfigCompatibleWithStandardLibrary

func Marshal(data interface{}) []byte {
	bytes, err := json.Marshal(&data)
	if err != nil {
		log.Printf("%v Marshal Json error:\n%v \n", data, err)
		return nil
	}
	return bytes
}

func Unmarshal(bytes []byte, data interface{}) error {
	return json.Unmarshal(bytes, &data)
}

func Marshal2String(data interface{}) (str string) {
	bytes := Marshal(data)
	if bytes != nil {
		str = string(bytes)
	}
	return
}
