package util

import (
	"log"
	"net"
	"strings"
)

/*
@author pengshuo
@date 2021/11/26 16:06
version 1.0.0
desc:
	ip util
*/

func GetLocalIp(addr string) (ip string) {
	conn, err := net.Dial("tcp", addr)
	defer conn.Close()
	if err == nil {
		ip = strings.Split(conn.LocalAddr().String(), ":")[0]
	} else {
		log.Printf("get local ip error: %s \n", err.Error())
	}
	return
}
