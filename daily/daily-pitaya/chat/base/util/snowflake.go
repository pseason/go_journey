package util

import "github.com/bwmarrin/snowflake"

/*
@author pengshuo
@date 2021/11/16 16:55
version 1.0.0
desc:
	snowflake generate id
*/

var node, _ = snowflake.NewNode(9)

// GenSnowFlakeId generate snowflake id
func GenSnowFlakeId() string {
	return node.Generate().String()
}
