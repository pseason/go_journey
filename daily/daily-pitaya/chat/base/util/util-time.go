package util

import "github.com/golang-module/carbon"

/*
@author pengshuo
@date 2021/11/26 15:29
version 1.0.0
desc:
	time util
*/

// GetDateTimeString DateTime
func GetDateTimeString() string {
	return carbon.Now().ToDateTimeString()
}
