package protobuf

import (
	"google.golang.org/protobuf/proto"
)

/*
@author pengshuo
@date 2021/11/26 10:06
version 1.0.0
desc:

*/

// UnmarshalTo Message
func (x *ServerRequest) UnmarshalTo(m proto.Message) error {
	return x.GetMsg().UnmarshalTo(m)
}

// ReplyError Message
func (x *ServerResponse) ReplyError(code Code, err string) {
	x.Reset()
	x.Code = code
	x.Err = err
}
