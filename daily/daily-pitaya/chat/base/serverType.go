package base

import "fmt"

/*
@author pengshuo
@date 2021/11/27 12:46
version 1.0.0
desc:

*/

const (
	formatServerRoute = "%s.%s"
	DefaultServer     = "default"
	GateServer        = "GateServer"
	ChatServer        = "ChatServer"
	EtcdPrefix        = "pitaya/"
	EtcdUrl           = "localhost:2379"
)

const (
	Online  = 1 //Online
	Offline = 2 //Offline
)

func FormatRoute(server, route string) string {
	return fmt.Sprintf(formatServerRoute, server, route)
}
