package base

import (
	"fmt"
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/acceptor"
	"github.com/topfreegames/pitaya/v2/cluster"
	"github.com/topfreegames/pitaya/v2/component"
	"github.com/topfreegames/pitaya/v2/config"
	"github.com/topfreegames/pitaya/v2/groups"
	"github.com/topfreegames/pitaya/v2/modules"
	"github.com/topfreegames/pitaya/v2/serialize/protobuf"
	"strings"
	"time"
)

/*
@author pengshuo
@date 2021/11/16 14:41
version 1.0.0
desc:
	创建 pitaya app
*/

const (
	isRemote  = true  //远程组件
	notRemote = false //非远程组件
	isLower   = true  //转小写
	notLower  = false //不转小写
)

// PitayaAppConf pitaya app config
type PitayaAppConf struct {
	ServerType     string
	ServerPort     int
	RpcServerPort  int
	ServerMetadata map[string]string
	ETCDEndpoints  []string
	ETCDPrefix     string
}

// CreateApp create pitaya cluster rpc server app
func CreateApp(conf PitayaAppConf) (pitaya.Pitaya, *modules.ETCDBindingStorage) {
	// pitaya builder
	builderConfig := config.NewDefaultBuilderConfig()
	builder := pitaya.NewDefaultBuilder(true, conf.ServerType, pitaya.Cluster, conf.ServerMetadata, *builderConfig)
	// bindingStorage
	bindingConfig := buildETCDBindingConfig(conf.ETCDEndpoints, conf.ETCDPrefix)
	bindingStorage := modules.NewETCDBindingStorage(builder.Server, builder.SessionPool, *bindingConfig)
	// grpcServer
	grpcServerConfig := &config.GRPCServerConfig{
		Port: conf.RpcServerPort,
	}
	grpcServer, _ := cluster.NewGRPCServer(*grpcServerConfig, builder.Server, builder.MetricsReporters)
	// grpcClient
	clientConfig := buildGRPCClientConfig()
	grpcClient, _ := cluster.NewGRPCClient(*clientConfig, builder.Server, builder.MetricsReporters,
		bindingStorage, cluster.NewInfoRetriever(*config.NewDefaultInfoRetrieverConfig()),
	)
	// etcdGroupService
	serviceConfig := buildEtcdGroupServiceConfig(conf.ETCDEndpoints, conf.ETCDPrefix)
	etcdGroupService, err := groups.NewEtcdGroupService(*serviceConfig, nil)
	if err != nil {
		panic(err)
	}
	// builder accept tcp port
	tcpAcceptor := acceptor.NewTCPAcceptor(fmt.Sprintf(":%d", conf.ServerPort))

	builder.Serializer = protobuf.NewSerializer()
	builder.RPCServer = grpcServer
	builder.RPCClient = grpcClient
	builder.Groups = etcdGroupService
	builder.AddAcceptor(tcpAcceptor)

	return builder.Build(), bindingStorage
}

// RegisterRemoteRoute register remote route for rpc client
func RegisterRemoteRoute(app pitaya.Pitaya, cp component.Component, route string) {
	register(app, isRemote, cp, route, notLower)
}

// RegisterRemoteRouteLower register remote route with method lower for rpc client
func RegisterRemoteRouteLower(app pitaya.Pitaya, cp component.Component, route string) {
	register(app, isRemote, cp, route, isLower)
}

// RegisterRoute register local route
func RegisterRoute(app pitaya.Pitaya, cp component.Component, route string) {
	register(app, notRemote, cp, route, notLower)
}

// RegisterRouteLower register local route with method lower
func RegisterRouteLower(app pitaya.Pitaya, cp component.Component, route string) {
	register(app, notRemote, cp, route, isLower)
}

// register add route with args
func register(app pitaya.Pitaya, isRemote bool, cp component.Component, route string, isLower bool) {
	options := make([]component.Option, 0)
	options = append(options, component.WithName(route))
	if isLower {
		options = append(options, component.WithNameFunc(strings.ToLower))
	}
	if isRemote {
		app.RegisterRemote(cp, options...)
	} else {
		app.Register(cp, options...)
	}
}

// buildETCDBindingConfig provides customer configuration for ETCDBindingStorage（NewDefaultETCDBindingConfig）
func buildETCDBindingConfig(endpoints []string, etcdPrefix string) *config.ETCDBindingConfig {
	return &config.ETCDBindingConfig{
		Endpoints:   endpoints,
		Prefix:      etcdPrefix,
		DialTimeout: 3 * time.Second,
		LeaseTTL:    3 * time.Hour,
	}
}

// buildGRPCClientConfig rpc client customer config struct（NewDefaultGRPCClientConfig）
func buildGRPCClientConfig() *config.GRPCClientConfig {
	return &config.GRPCClientConfig{
		DialTimeout:    2 * time.Second,
		RequestTimeout: 2 * time.Second,
		LazyConnection: false,
	}
}

// buildEtcdGroupServiceConfig provides customer ETCD configuration (NewDefaultEtcdGroupServiceConfig)
func buildEtcdGroupServiceConfig(endpoints []string, etcdPrefix string) *config.EtcdGroupServiceConfig {
	return &config.EtcdGroupServiceConfig{
		Endpoints:          endpoints,
		Prefix:             etcdPrefix,
		DialTimeout:        3 * time.Second,
		TransactionTimeout: 3 * time.Second,
	}
}
