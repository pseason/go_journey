package base

/*
@author pengshuo
@date 2021/12/4 16:18
version 1.0.0
desc:

*/
const (
	StatusFail    = 100 // 失败
	StatusSuccess = 200 // 成功
)
