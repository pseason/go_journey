module springmars.com/daily/pitaya/chat/base

go 1.16

require (
	github.com/allegro/bigcache/v3 v3.0.1 // indirect
	github.com/bwmarrin/snowflake v0.3.0 // indirect
	github.com/gobuffalo/envy v1.10.1 // indirect
	github.com/gobuffalo/packd v1.0.1 // indirect
	github.com/golang-module/carbon v1.5.5 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/topfreegames/pitaya/v2 v2.0.4 // indirect
)
