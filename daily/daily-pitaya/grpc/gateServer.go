package main

import (
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/constants"
	"github.com/topfreegames/pitaya/v2/modules"
	"springmars.com/daily/pitaya/grpc/ge"
	"strconv"
)

/*
@author pengshuo
@date 2021/10/28 10:16
version 1.0.0
desc:

*/

const (
	gateServerPort = 11080
	gateRpcPort    = 11081
	gateServerType = "gate"
)

var gate pitaya.Pitaya

func main() {
	var etcd *modules.ETCDBindingStorage
	// init
	var meta = map[string]string{
		constants.GRPCHostKey: "127.0.0.1",
		constants.GRPCPortKey: strconv.Itoa(gateRpcPort),
	}
	gate, etcd = ge.CrateApp(gateServerType, gateServerPort, gateRpcPort, meta)
	// defer app shutdown
	defer gate.Shutdown()
	// register etcd binding storage
	gate.RegisterModule(etcd, "bindingStorage")
	// 注册路由
	ge.ConfigureLocal(gate)
	// app start
	gate.Start()
}
