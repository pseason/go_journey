package services

import (
	"context"
	"github.com/topfreegames/pitaya/v2/component"
	"github.com/topfreegames/pitaya/v2/examples/demo/protos"
	"github.com/topfreegames/pitaya/v2/logger"
)

/*
@author pengshuo
@date 2021/10/28 14:46
version 1.0.0
desc:

*/

type ConnectorRemote struct {
	component.Base
}

// RemoteFunc is a function that will be called remotely
func (c *ConnectorRemote) RemoteFunc(ctx context.Context, msg *protos.RPCMsg) (*protos.RPCRes, error) {
	logger.Log.Infof("--------- request [producer.connectorremote.remotefunc] result ok, recv: %s", msg.GetMsg())

	return &protos.RPCRes{
		Msg: "RemoteFunc Success",
	}, nil
}
