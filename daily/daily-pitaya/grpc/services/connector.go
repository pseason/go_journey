package services

import (
	"context"
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/component"
	"github.com/topfreegames/pitaya/v2/examples/demo/protos"
	"github.com/topfreegames/pitaya/v2/logger"
)

// ConnectorRemote is a remote that will receive rpc's

// Connector struct
type Connector struct {
	component.Base
	app pitaya.Pitaya
}

// NewConnector ctor
func NewConnector(app pitaya.Pitaya) *Connector {
	return &Connector{app: app}
}

// SendPushToUser is a function that will be called remotely
func (c *Connector) SendPushToUser(ctx context.Context, msg *protos.RPCMsg) (*protos.RPCRes, error) {
	logger.Log.Infof("---------request [producer.connector.sendpushtouser] result ok, recv: %s", msg.GetMsg())

	ret := &protos.RPCRes{}
	c.app.RPC(ctx, "gate.room.recvrpc", ret, &protos.RPCMsg{
		Msg: "SendPushToUser request",
	})

	return &protos.RPCRes{
		Msg: "SendPushToUser Success",
	}, nil
}
