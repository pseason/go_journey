package services

import (
	"context"
	"encoding/gob"
	"github.com/topfreegames/pitaya/v2/logger"
	"strconv"
	"time"

	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/component"
	"github.com/topfreegames/pitaya/v2/examples/demo/protos"
	"github.com/topfreegames/pitaya/v2/timer"
)

type (
	// Room represents a component that contains a bundle of room related handler
	// like Join/Message
	Room struct {
		component.Base
		timer *timer.Timer
		app   pitaya.Pitaya
		Stats *Stats
	}

	// UserMessage represents a message that user sent
	UserMessage struct {
		Name    string `json:"name"`
		Content string `json:"content"`
	}

	// Stats exports the room status
	Stats struct {
		outboundBytes int
		inboundBytes  int
	}

	// SendRPCMsg represents a rpc message
	SendRPCMsg struct {
		ServerID string `json:"serverId"`
		Route    string `json:"route"`
		Msg      string `json:"msg"`
	}

	// NewUser message will be received when new user join room
	NewUser struct {
		Content string `json:"content"`
	}

	// AllMembers contains all members uid
	AllMembers struct {
		Members []string `json:"members"`
	}

	// JoinResponse represents the result of joining room
	JoinResponse struct {
		Code   int    `json:"code"`
		Result string `json:"result"`
	}
)

// Outbound gets the outbound status
func (Stats *Stats) Outbound(ctx context.Context, in []byte) ([]byte, error) {
	Stats.outboundBytes += len(in)
	return in, nil
}

// Inbound gets the inbound status
func (Stats *Stats) Inbound(ctx context.Context, in []byte) ([]byte, error) {
	Stats.inboundBytes += len(in)
	return in, nil
}

// NewRoom returns a new room
func NewRoom(app pitaya.Pitaya) *Room {
	return &Room{
		app:   app,
		Stats: &Stats{},
	}
}

// Init runs on service initialization
func (r *Room) Init() {
	r.app.GroupCreate(context.Background(), "room")
	// It is necessary to register all structs that will be used in RPC calls
	// This must be done both in the caller and callee servers
	gob.Register(&UserMessage{})
}

// AfterInit component lifetime callback
func (r *Room) AfterInit() {
	r.timer = pitaya.NewTimer(time.Minute, func() {
		count, err := r.app.GroupCountMembers(context.Background(), "room")
		logger.Log.Infoln("UserCount: Time=>", time.Now().String(), "Count=>", count, "Error=>", err)
		logger.Log.Infoln("OutboundBytes", r.Stats.outboundBytes)
		logger.Log.Infoln("InboundBytes", r.Stats.outboundBytes)
	})
}

// Entry is the entrypoint
func (r *Room) Entry(ctx context.Context) (*JoinResponse, error) {
	s := r.app.GetSessionFromCtx(ctx)
	err := s.Bind(ctx, strconv.Itoa(int(s.ID())))
	if err != nil {
		return nil, pitaya.Error(err, "RH-000", map[string]string{"failed": "bind"})
	}

	logger.Log.Infof("---------[sid: %s] request [gate.room.entry] result ok", strconv.Itoa(int(s.ID())))

	return &JoinResponse{Result: "ok"}, nil
}

// Join room
func (r *Room) Join(ctx context.Context) (*JoinResponse, error) {
	s := r.app.GetSessionFromCtx(ctx)
	err := r.app.GroupAddMember(ctx, "room", s.UID())
	if err != nil {
		return nil, err
	}
	members, err := r.app.GroupMembers(ctx, "room")
	if err != nil {
		return nil, err
	}
	s.Push("onMembers", &AllMembers{Members: members})

	logger.Log.Infof("---------[sid: %s ] request [gate.room.join] result ok", strconv.Itoa(int(s.ID())))

	err = s.OnClose(func() {
		r.app.GroupRemoveMember(ctx, "room", s.UID())
	})
	if err != nil {
		return nil, err
	}
	return &JoinResponse{Result: "success"}, nil
}

// Message sync last message to all members
func (r *Room) Message(ctx context.Context, msg *UserMessage) {
	sid := strconv.Itoa(int(r.app.GetSessionFromCtx(ctx).ID()))

	logger.Log.Infof("---------[sid: %s] request [gate.room.message] result ok", sid)

	err := r.app.GroupBroadcast(ctx, "producer", "connector", "onMessage", msg)
	logger.Log.Infof("---------[sid: %s] request [producer.connector.onMessage] req： %v", sid, msg)
	if err != nil {
		logger.Log.Errorf("---------[sid: %s] request [producer.connector.onMessage] error: %v", sid, err)
	}
}

// SendRPC sends rpc
func (r *Room) SendRPC(ctx context.Context, msg *protos.RPCMsg) (*protos.RPCRes, error) {
	sid := strconv.Itoa(int(r.app.GetSessionFromCtx(ctx).ID()))
	logger.Log.Infof("---------[sid: %s] request [gate.room.sendrpc] result ok", sid)

	ret := &protos.RPCRes{}
	err := r.app.RPC(ctx, "producer.connectorremote.remotefunc", ret, msg)
	err = r.app.RPC(ctx, "producer.connector.sendpushtouser", ret, msg)

	if err != nil {
		logger.Log.Infof("---------[sid: %s] request [producer.connectorremote.remotefunc] error： %v", sid, err)
		return nil, pitaya.Error(err, "RPC-000")
	}
	return ret, nil
}

// RecvRPC recv rpc
func (r *Room) RecvRPC(ctx context.Context, msg *protos.RPCMsg) (*protos.RPCRes, error) {
	logger.Log.Infof("---------request [gate.room.recvrpc] req: %s", msg.Msg)

	return &protos.RPCRes{
		Msg: "Success",
	}, nil
}
