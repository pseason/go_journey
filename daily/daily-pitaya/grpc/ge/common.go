package ge

import (
	"fmt"
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/acceptor"
	"github.com/topfreegames/pitaya/v2/cluster"
	"github.com/topfreegames/pitaya/v2/component"
	"github.com/topfreegames/pitaya/v2/config"
	"github.com/topfreegames/pitaya/v2/groups"
	"github.com/topfreegames/pitaya/v2/modules"
	"springmars.com/daily/pitaya/grpc/services"
	"strings"
)

/*
@author pengshuo
@date 2021/10/28 14:20
version 1.0.0
desc:

*/

// create a new app
func CrateApp(serverType string, serverPort, rpcPort int, serverMetadata map[string]string) (pitaya.Pitaya, *modules.ETCDBindingStorage) {
	// pitaya builder
	builder := pitaya.NewDefaultBuilder(true, serverType, pitaya.Cluster, serverMetadata, *config.NewDefaultBuilderConfig())
	// bindingStorage
	bindingStorage := modules.NewETCDBindingStorage(builder.Server, builder.SessionPool, *config.NewDefaultETCDBindingConfig())
	// grpc server
	grpcServerConfig := &config.GRPCServerConfig{
		Port: rpcPort,
	}
	grpcServer, _ := cluster.NewGRPCServer(*grpcServerConfig, builder.Server, builder.MetricsReporters)
	// cluster new grpc client
	grpcClient, _ := cluster.NewGRPCClient(
		*config.NewDefaultGRPCClientConfig(), builder.Server, builder.MetricsReporters,
		bindingStorage, cluster.NewInfoRetriever(*config.NewDefaultInfoRetrieverConfig()),
	)
	// builder attr
	builder.RPCServer = grpcServer
	builder.RPCClient = grpcClient
	etcdGroupService, err := groups.NewEtcdGroupService(*config.NewDefaultEtcdGroupServiceConfig(), nil)
	if err != nil {
		panic(err)
	}
	builder.Groups = etcdGroupService
	// if is fronted builder accept tcp port
	tcpAcceptor := acceptor.NewTCPAcceptor(fmt.Sprintf(":%d", serverPort))
	builder.AddAcceptor(tcpAcceptor)
	return builder.Build(), bindingStorage
}

func ConfigureLocal(app pitaya.Pitaya) {
	room := services.NewRoom(app)
	app.Register(room,
		component.WithName("room"),
		component.WithNameFunc(strings.ToLower),
	)
	app.RegisterRemote(room,
		component.WithName("room"),
		component.WithNameFunc(strings.ToLower),
	)
}

func ConfigureRemote(app pitaya.Pitaya) {
	app.RegisterRemote(&services.ConnectorRemote{},
		component.WithName("connectorremote"),
		component.WithNameFunc(strings.ToLower),
	)
	app.RegisterRemote(services.NewConnector(app),
		component.WithName("connector"),
		component.WithNameFunc(strings.ToLower),
	)
}
