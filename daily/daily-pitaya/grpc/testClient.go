package main

import (
	"encoding/json"
	"github.com/topfreegames/pitaya/v2/client"
	"github.com/topfreegames/pitaya/v2/conn/message"
	"github.com/topfreegames/pitaya/v2/session"
	"log"
	"time"
)

/*
@author pengshuo
@date 2021/9/3 9:43
version 1.0.0
desc:
	pitaya client 会读取服务器下发的心跳时间，做心跳发送

*/

var (
	pClient client.PitayaClient

	handshake = &session.HandshakeData{
		Sys: session.HandshakeClientData{
			Platform:    "mac",
			LibVersion:  "0.3.5-release",
			BuildNumber: "20",
			Version:     "1.0.0",
		},
		User: map[string]interface{}{
			"age": 30,
		},
	}

	disconnectedCh = make(chan bool, 1)

	prettyJSON = true

	addr = "127.0.0.1:11080"
)

func main() {

	pClient = client.New(5)
	pClient.SetClientHandshakeData(handshake)

	err := pClient.ConnectTo(addr)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("Connected ... ")
	// 接收消息
	go readServerMessages(func(msg *message.Message) {
		var m interface{}
		if prettyJSON {
			_ = json.Unmarshal(msg.Data, &m)
		}
		log.Printf("sv -> type: %v, id: %v, route: %v, msg: %v \n", msg.Type, msg.ID, msg.Route, m)
	})
	// 连接状态
	if pClient.ConnectedStatus() {
		// 发消息
		route := "room.entry"
		data := []byte("")
		pClient.SendRequest(route, data)
		time.Sleep(time.Second)

		route = "room.join"
		data = []byte("")
		pClient.SendRequest(route, data)
		time.Sleep(time.Second)

		route = "room.sendrpc"
		data = []byte("{\"Msg\": \"mock rpc test\"}")
		pClient.SendRequest(route, data)
		time.Sleep(time.Second)

		route = "room.message"
		data = []byte("{\"name\": \"lucy\", \"content\": \"hello\"}")
		pClient.SendNotify(route, data)
		time.Sleep(time.Second)
	}
	time.Sleep(time.Minute * 1)
}

func readServerMessages(callback func(msg *message.Message)) {
	channel := pClient.MsgChannel()
	for {
		select {
		case <-disconnectedCh:
			close(disconnectedCh)
			return
		case m := <-channel:
			callback(m)
		}
	}
}
