package main

import (
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/constants"
	"github.com/topfreegames/pitaya/v2/modules"
	"springmars.com/daily/pitaya/grpc/ge"
	"strconv"
)

/*
@author pengshuo
@date 2021/10/28 10:16
version 1.0.0
desc:

*/

const (
	grpcServerPort = 11082
	gRpcPort       = 11083
	grpcServerType = "producer"
)

var grpc pitaya.Pitaya

func main() {
	var etcd *modules.ETCDBindingStorage
	// init
	var meta = map[string]string{
		constants.GRPCHostKey: "127.0.0.1",
		constants.GRPCPortKey: strconv.Itoa(gRpcPort),
	}
	grpc, etcd = ge.CrateApp(grpcServerType, grpcServerPort, gRpcPort, meta)
	// defer app shutdown
	defer grpc.Shutdown()
	// register etcd binding storage
	grpc.RegisterModule(etcd, "bindingStorage")
	// 注册路由
	ge.ConfigureRemote(grpc)
	// app start
	grpc.Start()
}
