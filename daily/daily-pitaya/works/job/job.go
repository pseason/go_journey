package job

import (
	"github.com/topfreegames/go-workers"
	"log"
)

/*
@author pengshuo
@date 2021/10/26 20:06
version 1.0.0
desc:

*/
func MyJob(message *workers.Msg) {
	// do something with your message
	// message.Jid()
	// message.Args() is a wrapper around go-simplejson (http://godoc.org/github.com/bitly/go-simplejson)
	class, _ := message.Json.Get("class").String()
	log.Printf("job recv: %s  \n", class)
}

type MyMiddleware struct{}

func (r *MyMiddleware) Call(queue string, message *workers.Msg, next func() bool) (acknowledge bool) {
	// do something before each message is processed
	acknowledge = next()
	// do something after each message is processed
	return
}
