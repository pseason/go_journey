package main

import (
	"fmt"
	"github.com/topfreegames/go-workers"
	"time"
)

/*
@author pengshuo
@date 2021/10/26 17:53
version 1.0.0
desc:
	多个服务，一个消息，只在一个服务被消费
*/

func main() {
	workers.Configure(map[string]string{
		"server":   "10.198.141.232:6388",
		"database": "4",
		"pool":     "10",
		"password": "dkkaI#27KlmQ-3k2OPj",
		// unique process id for this instance of workers (for proper recovery of inprogress jobs on crash)
		"process": "3",
	})
	// Add a job to a queue
	for i := 0; i < 10000; i++ {
		workers.Enqueue("myqueue", fmt.Sprintf("Add%d", i), i)
		time.Sleep(time.Millisecond * 200)
	}
	time.Sleep(time.Second * 60)
}
