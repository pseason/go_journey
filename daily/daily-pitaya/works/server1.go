package main

import (
	"github.com/topfreegames/go-workers"
	"springmars.com/daily/pitaya/works/job"
)

/*
@author pengshuo
@date 2021/10/26 17:53
version 1.0.0
desc:
	多个服务，一个消息，只在一个服务被消费
*/
func main() {
	workers.Configure(map[string]string{
		"server":   "10.198.141.232:6388",
		"database": "4",
		"pool":     "10",
		"password": "dkkaI#27KlmQ-3k2OPj",
		// unique process id for this instance of workers (for proper recovery of inprogress jobs on crash)
		"process": "1",
	})
	workers.Middleware.Append(&job.MyMiddleware{})
	// pull messages from "myqueue" with concurrency of 10
	workers.Process("myqueue", job.MyJob, 10)

	// stats will be available at http://localhost:8080/stats
	go workers.StatsServer(8081)

	// Blocks until process is told to exit via unix signal
	workers.Run()
}
