module springmars.com/daily/pitaya/works

go 1.16

require (
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/topfreegames/go-workers v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
)
