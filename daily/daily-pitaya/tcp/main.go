package main

import (
	"context"
	"fmt"
	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/acceptor"
	"github.com/topfreegames/pitaya/v2/component"
	"github.com/topfreegames/pitaya/v2/config"
	logging "github.com/topfreegames/pitaya/v2/logger/interfaces"
	"github.com/topfreegames/pitaya/v2/serialize/json"
	"github.com/topfreegames/pitaya/v2/session"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

/*
@author pengshuo
@date 2021/9/2 17:35
version 1.0.0
desc:
	tcp 实例
*/

// MetaGameServer ...
type MetaGameServer struct {
	component.Base
}

// NewMetaGameServer ...
func NewMetaGameServer() *MetaGameServer {
	return &MetaGameServer{}
}

// CreatePlayerCheatResponse ...
type CreatePlayerCheatResponse struct {
	Msg string `json:"msg"`
}

type CreatePlayerCheatArgs struct {
	Name         string `json:"name"`
	Email        string `json:"email" validate:"email"`
	SoftCurrency int    `json:"softCurrency" validate:"gte=0,lte=1000"`
	HardCurrency int    `json:"hardCurrency" validate:"gte=0,lte=200"`
}

// HandlerNoArgResponse ...
type HandlerNoArgResponse struct {
	Msg string `json:"msg"`
}

// CreatePlayerCheat ...
func (g *MetaGameServer) CreatePlayerCheat(ctx context.Context, args *CreatePlayerCheatArgs) (*CreatePlayerCheatResponse, error) {
	logger := pitaya.GetDefaultLoggerFromCtx(ctx) // The default logger contains a requestId, the route being executed and the sessionId
	logger.Info("CreatePlayerChest called")
	// session直接推送
	session := app.GetSessionFromCtx(ctx)
	session.Push("session.Push", "{\"id\":1}")
	// 推送给多个玩家
	app.SendPushToUsers("appSession", "{\"SendPushToUsers\":2}", []string{session.UID()}, serverType)
	// 广播给全部玩家
	app.GroupBroadcast(ctx, serverType, componentName, "GroupBroadcast", "{\"GroupBroadcast\":board}")
	// Do nothing. This is just an example of how pipelines can be helpful
	return &CreatePlayerCheatResponse{Msg: "ok"}, nil
}

// HandlerNoArg is a simple handler that do not have any arguments
func (g *MetaGameServer) HandlerNoArg(ctx context.Context) (*HandlerNoArgResponse, error) {
	return &HandlerNoArgResponse{Msg: "ok"}, nil
}

// Simple example of a before pipeline that actually asserts the type of the
// in parameter.
// IMPORTANT: that this kind of pipeline will be hard to exist in real code
// as a pipeline function executes for every handler and each of them
// most probably have different parameter types.
func (g *MetaGameServer) simpleBefore(ctx context.Context, in interface{}) (context.Context, interface{}, error) {
	logger := pitaya.GetDefaultLoggerFromCtx(ctx)
	logger.Info("Simple Before exec")
	// 绑定session
	bindSession(ctx, logger)

	if in != nil {
		createPlayerArgs := in.(*CreatePlayerCheatArgs)

		logger.Infof("Name: %s", createPlayerArgs.Name)
		logger.Infof("Email: %s", createPlayerArgs.Email)
		logger.Infof("SoftCurrency: %d", createPlayerArgs.SoftCurrency)
		logger.Infof("HardCurrency: %d", createPlayerArgs.HardCurrency)
	}
	return ctx, in, nil
}

// Simple example of an after pipeline. The 2nd argument is the handler response.
func (g *MetaGameServer) simpleAfter(ctx context.Context, resp interface{}, err error) (interface{}, error) {
	logger := pitaya.GetDefaultLoggerFromCtx(ctx)
	logger.Infof("Simple After exec - response: %v , error: %v", resp, err)
	return resp, err
}

// AfterInit component lifetime callback
func (meta *MetaGameServer) AfterInit() {
	app.GroupCreate(nil, componentName)
	fmt.Println("------- AfterInit --------")
}

var (
	app           pitaya.Pitaya
	serverType    = "chat"
	componentName = "tcpserver"
	sessionMap    = make(map[string]session.Session)
)

func main() {
	// component
	metaGameServer := NewMetaGameServer()

	conf := config.NewDefaultBuilderConfig()
	conf.DefaultPipelines.StructValidation.Enabled = true

	builder := pitaya.NewDefaultBuilder(true, serverType, pitaya.Standalone, map[string]string{}, *conf)
	// 接受消息编码
	builder.Serializer = json.NewSerializer()
	builder.AddAcceptor(acceptor.NewTCPAcceptor(fmt.Sprintf(":%d", 9017)))
	//fixme 消息接受前，接受后，可做验证消息
	builder.HandlerHooks.BeforeHandler.PushBack(metaGameServer.simpleBefore)
	builder.HandlerHooks.AfterHandler.PushBack(metaGameServer.simpleAfter)
	// 自定义 session 管理
	builder.SessionPool = selfSessionPool()

	app = builder.Build()

	app.SetHeartbeatTime(time.Second * 10)

	defer app.Shutdown()

	app.Register(metaGameServer,
		component.WithName(componentName),
		component.WithNameFunc(strings.ToLower),
	)

	app.Start()
}

// session存储、session关闭移除
func selfSessionPool() session.SessionPool {
	ssp := session.NewSessionPool()
	// session bind
	ssp.OnSessionBind(func(ctx context.Context, s session.Session) error {

		sessionMap[s.UID()] = s

		// 添加到group
		app.GroupAddMember(ctx, componentName, s.UID())

		return nil
	})
	// session 关闭
	ssp.OnSessionClose(func(s session.Session) {
		// 移除存的session
		delete(sessionMap, s.UID())
		// 移除group存的session uid
		app.GroupRemoveMember(nil, componentName, s.UID())
		fmt.Printf("session map delete key: %v\n", s.UID())
	})
	return ssp
}

func bindSession(ctx context.Context, log logging.Logger) {
	session := app.GetSessionFromCtx(ctx)
	if session.UID() == "" {
		// 绑定
		uid := "sid:" + strconv.Itoa(rand.Intn(100000))
		session.Bind(ctx, uid)
	}
}
