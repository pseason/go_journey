package main

import (
	"encoding/json"
	"github.com/topfreegames/pitaya/v2/client"
	"github.com/topfreegames/pitaya/v2/conn/message"
	"github.com/topfreegames/pitaya/v2/session"
	"testing"
	"time"
)

/*
@author pengshuo
@date 2021/9/3 9:43
version 1.0.0
desc:

*/

var (
	pClient client.PitayaClient

	handshake = &session.HandshakeData{
		Sys: session.HandshakeClientData{
			Platform:    "mac",
			LibVersion:  "0.3.5-release",
			BuildNumber: "20",
			Version:     "1.0.0",
		},
		User: map[string]interface{}{
			"age": 30,
		},
	}

	disconnectedCh = make(chan bool, 1)

	prettyJSON = true

	addr = "127.0.0.1:9017"
)

/**
pitaya client 会读取服务器下发的心跳时间，做心跳发送
*/
func TestTcp(t *testing.T) {

	pClient = client.New(5)
	pClient.SetClientHandshakeData(handshake)

	err := pClient.ConnectTo(addr)
	if err != nil {
		t.Log(err)
		return
	}
	t.Log("Connected ... ")
	// 接收消息
	go readServerMessages(func(msg *message.Message) {
		var m interface{}
		if prettyJSON {
			_ = json.Unmarshal(msg.Data, &m)
		}
		t.Logf("sv -> type: %v, id: %v, route: %v, msg: %v", msg.Type, msg.ID, msg.Route, m)
	})
	// 连接状态
	if pClient.ConnectedStatus() {
		// 发消息
		for true {
			route := "tcpserver.createplayercheat"
			data := []byte("{\"name\": \"test\",\"email\": \"xxx@163.com\",\"softCurrency\": 1,\"hardCurrency\": 2}")
			pClient.SendRequest(route, data)

			route1 := "tcpserver.handlernoarg"
			data1 := []byte("{\"msg\": \"test\"}")
			pClient.SendRequest(route1, data1)

			time.Sleep(time.Second * 40)
		}
	}
	time.Sleep(time.Hour * 3)
}

func readServerMessages(callback func(msg *message.Message)) {
	channel := pClient.MsgChannel()
	for {
		select {
		case <-disconnectedCh:
			close(disconnectedCh)
			return
		case m := <-channel:
			callback(m)
		}
	}
}
