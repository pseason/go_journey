package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/reactivex/rxgo/v2"
	"time"
)

/*
@author pengshuo
@date 2021/10/12 15:40
version 1.0.0
desc: rx go

*/

func main() {

	rxFromChannel()
	fmt.Println("----------------")
	rxCreate()
	fmt.Println("----------------")
	rxForEach()
	fmt.Println("----------------")
	rxDoOnError()
	fmt.Println("----------------")
	rxDoOnNext()
	fmt.Println("----------------")
	rxJustMapFilter()
	fmt.Println("----------------")
	rxJust()
}

func rxInterval() {
	observable := rxgo.Interval(rxgo.WithDuration(time.Second))
	for item := range observable.Observe() {
		fmt.Println(item.V)
	}
}

// rx create from channel
func rxFromChannel() {
	ch := make(chan rxgo.Item)
	go func() {
		for i := 0; i < 5; i++ {
			ch <- rxgo.Of(i*5 + 2)
		}
		close(ch)
	}()
	observable := rxgo.FromChannel(ch)
	for item := range observable.Observe() {
		fmt.Println("rx create from channel:", item.V)
	}
}

// rx create from create
func rxCreate() {
	observe := rxgo.Create([]rxgo.Producer{func(ctx context.Context, next chan<- rxgo.Item) {
		next <- rxgo.Of(1)
		next <- rxgo.Of(2)
		next <- rxgo.Of(3)
		next <- rxgo.Error(errors.New("rx create error"))
	}}).Observe()
	for item := range observe {
		if item.E != nil {
			fmt.Println("rx create", item.E.Error())
		} else {
			fmt.Println("rx create", item.V)
		}
	}

	observe = rxgo.Create([]rxgo.Producer{func(ctx context.Context, next chan<- rxgo.Item) {
		next <- rxgo.Of(1)
		next <- rxgo.Of(2)
		next <- rxgo.Of(3)
		next <- rxgo.Error(errors.New("unknown"))
	}, func(ctx context.Context, next chan<- rxgo.Item) {
		next <- rxgo.Of(4)
		next <- rxgo.Of(5)
	}}).Observe()
	for item := range observe {
		if item.E != nil {
			fmt.Println("rx create", item.E.Error())
		} else {
			fmt.Println("rx create", item.V)
		}
	}
}

func rxForEach() {
	rxgo.Just("8", 1, 2, 3, 4, "6", errors.New("do on error"))().ForEach(func(i interface{}) {
		fmt.Println("rx foreach item", i)
	}, func(err error) {
		fmt.Println("rx foreach error: ", err.Error())
	}, func() {
		fmt.Println("rx foreach completed")
	})
}

func rxDoOnError() {
	rxgo.Just("8", 1, 2, 3, 4, "6", errors.New("do on error"))().DoOnError(func(err error) {
		fmt.Println(err)
	})
}

// do on next is async
func rxDoOnNext() {
	rxgo.Just("8", 1, 2, 3, 4, "6")().Filter(func(i interface{}) bool {
		switch i.(type) {
		case int:
			return true
		default:
			return false
		}
	}).Map(func(ctx context.Context, i interface{}) (interface{}, error) {
		return i.(int) * 2, nil
	}).DoOnNext(func(i interface{}) {
		fmt.Println(i)
	})
	time.Sleep(time.Second * 3)
}

// filter map
func rxJustMapFilter() {
	observable := rxgo.Just("8", 1, 2, 3, 4, "6")()
	observe := observable.Filter(func(i interface{}) bool {
		switch i.(type) {
		case int:
			return true
		default:
			return false
		}
	}).Map(func(ctx context.Context, i interface{}) (interface{}, error) {
		return i.(int) * 5, nil
	}).Observe()
	for item := range observe {
		fmt.Println(item.V)
	}
}

// rx just
func rxJust() {
	observable := rxgo.Just("101", 102, "103")()
	observe := observable.Observe()

	for item := range observe {
		fmt.Println(item.V)
	}
}
