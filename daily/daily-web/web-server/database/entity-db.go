package database

/*
@author pengshuo
@date 2021/8/30 16:35
version 1.0.0
desc:
	go:generate gormgen -structs Teacher,Student -input . -logName gamelog.Sugar -pkgName database -imports gorm.io/gorm -transformErr true
*/
//go:generate gormgen -structs Teacher,Student -input . -logName gamelog.Sugar -pkgName database -imports springmars.com/daily/web/base/gamelog,gorm.io/gorm -transformErr true

// db struct
type Teacher struct {
	Name  string `json:"name" gorm:"primaryKey"`
	Age   int    `json:"age"`
	Addr  string `json:"addr"`
	Score int    `json:"score"`
}

type Student struct {
	Id   int    `json:"id" gorm:"primaryKey"`
	Name string `json:"name"`
	Age  int    `json:"age"`
	Addr string `json:"addr"`
}
