package database

import (
	"errors"

	"springmars.com/daily/web/base/gamelog"

	"gorm.io/gorm"
)

var (
	ErrCreateStudent = errors.New("create Student failed")
	ErrDeleteStudent = errors.New("delete Student failed")
	ErrGetStudent    = errors.New("get Student failed")
	ErrUpdateStudent = errors.New("update Student failed")
)

// NewStudent new
func NewStudent() *Student {
	return new(Student)
}

// Add add one record
func (t *Student) Add(db *gorm.DB) (err error) {
	if err = db.Create(t).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrCreateStudent
		return
	}
	return
}

// Delete delete record
func (t *Student) Delete(db *gorm.DB) (err error) {
	if err = db.Delete(t).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrDeleteStudent
		return
	}
	return
}

// Updates update record
func (t *Student) Updates(db *gorm.DB, u *Student) (err error) {
	if err = db.Model(t).Updates(u).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrUpdateStudent
		return
	}
	return
}

// GetStudentAll get all record
func GetStudentAll(db *gorm.DB) (ret []*Student, err error) {
	if err = db.Find(&ret).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrGetStudent
		return
	}
	return
}

// GetStudentCount get count
func GetStudentCount(db *gorm.DB) (ret int64) {
	db.Model(&Student{}).Count(&ret)
	return
}

type QueryStudentForm struct {
	Name     *FieldData `json:"name" form:"name"`
	Age      *FieldData `json:"age" form:"age"`
	Addr     *FieldData `json:"addr" form:"addr"`
	Order    []string   `json:"order" form:"order"`
	PageNum  int        `json:"pageNum" form:"pageNum"`
	PageSize int        `json:"pageSize" form:"pageSize"`
}

//  GetStudentList get Student list some field value or some condition
func GetStudentList(q *QueryStudentForm, db *gorm.DB) (ret []*Student, err error) {
	// order
	if len(q.Order) > 0 {
		for _, v := range q.Order {
			db = db.Order(v)
		}
	}
	// pageSize
	if q.PageSize != 0 {
		db = db.Limit(q.PageSize)
	}
	// pageNum
	if q.PageNum != 0 {
		q.PageNum = (q.PageNum - 1) * q.PageSize
		db = db.Offset(q.PageNum)
	}

	// Name
	if q.Name != nil {
		db = db.Where("name"+q.Name.Symbol+"?", q.Name.Value)
	}
	// Age
	if q.Age != nil {
		db = db.Where("age"+q.Age.Symbol+"?", q.Age.Value)
	}
	// Addr
	if q.Addr != nil {
		db = db.Where("addr"+q.Addr.Symbol+"?", q.Addr.Value)
	}
	if err = db.Find(&ret).Error; err != nil {
		return
	}
	return
}

// QueryById query cond by Id
func (t *Student) SetById(id int) *Student {
	t.Id = id
	return t
}

// GetById get one record by Id
func (t *Student) GetById(db *gorm.DB) (err error) {
	if err = db.First(t, "id = ?", t.Id).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrGetStudent
		return
	}
	return
}

// DeleteById delete record by Id
func (t *Student) DeleteById(db *gorm.DB) (err error) {
	if err = db.Delete(t, "id = ?", t.Id).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrDeleteStudent
		return
	}
	return
}
