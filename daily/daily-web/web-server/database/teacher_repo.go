package database

import (
	"errors"

	"springmars.com/daily/web/base/gamelog"

	"gorm.io/gorm"
)

var (
	ErrCreateTeacher = errors.New("create Teacher failed")
	ErrDeleteTeacher = errors.New("delete Teacher failed")
	ErrGetTeacher    = errors.New("get Teacher failed")
	ErrUpdateTeacher = errors.New("update Teacher failed")
)

// NewTeacher new
func NewTeacher() *Teacher {
	return new(Teacher)
}

// Add add one record
func (t *Teacher) Add(db *gorm.DB) (err error) {
	if err = db.Create(t).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrCreateTeacher
		return
	}
	return
}

// Delete delete record
func (t *Teacher) Delete(db *gorm.DB) (err error) {
	if err = db.Delete(t).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrDeleteTeacher
		return
	}
	return
}

// Updates update record
func (t *Teacher) Updates(db *gorm.DB, u *Teacher) (err error) {
	if err = db.Model(t).Updates(u).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrUpdateTeacher
		return
	}
	return
}

// GetTeacherAll get all record
func GetTeacherAll(db *gorm.DB) (ret []*Teacher, err error) {
	if err = db.Find(&ret).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrGetTeacher
		return
	}
	return
}

// GetTeacherCount get count
func GetTeacherCount(db *gorm.DB) (ret int64) {
	db.Model(&Teacher{}).Count(&ret)
	return
}

type QueryTeacherForm struct {
	Age      *FieldData `json:"age" form:"age"`
	Addr     *FieldData `json:"addr" form:"addr"`
	Score    *FieldData `json:"score" form:"score"`
	Order    []string   `json:"order" form:"order"`
	PageNum  int        `json:"pageNum" form:"pageNum"`
	PageSize int        `json:"pageSize" form:"pageSize"`
}

//  GetTeacherList get Teacher list some field value or some condition
func GetTeacherList(q *QueryTeacherForm, db *gorm.DB) (ret []*Teacher, err error) {
	// order
	if len(q.Order) > 0 {
		for _, v := range q.Order {
			db = db.Order(v)
		}
	}
	// pageSize
	if q.PageSize != 0 {
		db = db.Limit(q.PageSize)
	}
	// pageNum
	if q.PageNum != 0 {
		q.PageNum = (q.PageNum - 1) * q.PageSize
		db = db.Offset(q.PageNum)
	}

	// Age
	if q.Age != nil {
		db = db.Where("age"+q.Age.Symbol+"?", q.Age.Value)
	}
	// Addr
	if q.Addr != nil {
		db = db.Where("addr"+q.Addr.Symbol+"?", q.Addr.Value)
	}
	// Score
	if q.Score != nil {
		db = db.Where("score"+q.Score.Symbol+"?", q.Score.Value)
	}
	if err = db.Find(&ret).Error; err != nil {
		return
	}
	return
}

// QueryByName query cond by Name
func (t *Teacher) SetByName(name string) *Teacher {
	t.Name = name
	return t
}

// GetByName get one record by Name
func (t *Teacher) GetByName(db *gorm.DB) (err error) {
	if err = db.First(t, "name = ?", t.Name).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrGetTeacher
		return
	}
	return
}

// DeleteByName delete record by Name
func (t *Teacher) DeleteByName(db *gorm.DB) (err error) {
	if err = db.Delete(t, "name = ?", t.Name).Error; err != nil {
		gamelog.Sugar.Error(err)
		err = ErrDeleteTeacher
		return
	}
	return
}
