package main

import (
	"fmt"
	"os"
	"os/signal"
	"runtime/pprof"
	"springmars.com/daily/web/base/gamecfg"
	"springmars.com/daily/web/base/gamecore"
	"springmars.com/daily/web/base/gamehttp"
	"springmars.com/daily/web/base/gamelog"
	_ "springmars.com/daily/web/base/gamelog"
	_ "springmars.com/daily/web/server/web"
	"syscall"
	"time"
)

/*
@author pengshuo
@date 2021/8/28 13:12
version 1.0.0
desc:

*/

func main() {
	defer gamecore.Terminate()
	// 加载配置
	gamecfg.LoadAppConfig()
	// start gin web
	gamehttp.StartGin(gamecfg.AppConf.Env, gamecfg.AppConf.Port)

	if true {
		filename := fmt.Sprintf("cpuprofile-%d.pprof", time.Now().Unix())
		f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, os.ModePerm)
		if err != nil {
			gamelog.Sugar.Panic(err)
		}
		pprof.StartCPUProfile(f)
		defer func() {
			gamelog.Sugar.Info("scmj server StopCPUProfile...")
			pprof.StopCPUProfile()
		}()
	}

	sg := make(chan os.Signal)
	signal.Notify(sg, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGKILL)
	select {
	case s := <-sg:
		gamelog.Sugar.Infof("got signal: %s", s.String())
	}

}
