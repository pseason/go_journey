module springmars.com/daily/web/server

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	gorm.io/gorm v1.21.14
	springmars.com/daily/web/base v1.0.0
)

replace springmars.com/daily/web/base => ./../web-base
