package web

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"springmars.com/daily/web/base/gamehttp"
	"springmars.com/daily/web/base/gameorm"
	"springmars.com/daily/web/server/database"
	"strconv"
)

/*
@author pengshuo
@date 2021/8/31 16:50
version 1.0.0
desc:

*/

func student2route() {
	gamehttp.CreateRoute("/student/:id", gamehttp.GET, getStudent)
	gamehttp.CreateRoute("/student/insert", gamehttp.POST, insertStudent)
	gamehttp.CreateRoute("/student/update", gamehttp.POST, updateStudent)
	gamehttp.CreateRoute("/student/:id", gamehttp.DELETE, deleteStudent)

}

func getStudent(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	stu := database.NewStudent()
	err := stu.SetById(id).GetById(gameorm.DB)
	if err == nil {
		c.JSON(http.StatusOK, gamehttp.Success(stu))
	} else {
		c.JSON(http.StatusOK, gamehttp.Error(err))
	}
}

// 绑定JSON的示例 ({"name": "lucy", "age": "16"})
func insertStudent(c *gin.Context) {
	stu := database.NewStudent()
	var data interface{}
	if err := c.BindJSON(stu); err == nil {
		err = stu.Add(gameorm.DB)
		if err == nil {
			data = gamehttp.Success(stu)
		} else {
			data = gamehttp.Error(err)
		}
	} else {
		data = gamehttp.Error(err)
	}
	c.JSON(http.StatusOK, data)
}

// 绑定JSON的示例 ({"name": "lucy", "age": "16"})
func updateStudent(c *gin.Context) {
	stu := database.NewStudent()
	var data interface{}
	if err := c.BindJSON(stu); err == nil {
		err = stu.Updates(gameorm.DB, stu)
		if err == nil {
			data = gamehttp.Success(stu)
		} else {
			data = gamehttp.Error(err)
		}
	} else {
		data = gamehttp.Error(err)
	}
	c.JSON(http.StatusOK, data)
}

func deleteStudent(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	err := database.NewStudent().SetById(id).DeleteById(gameorm.DB)
	if err == nil {
		c.JSON(http.StatusOK, gamehttp.Success("ok"))
	} else {
		c.JSON(http.StatusOK, gamehttp.Error(err))
	}
}
