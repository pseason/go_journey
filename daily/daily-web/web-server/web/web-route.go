package web

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"springmars.com/daily/web/base/gamehttp"
)

/*
@author pengshuo
@date 2021/8/31 16:51
version 1.0.0
desc:

*/

func init() {
	// path
	gamehttp.CreateRoute("/", gamehttp.GET, path)

	gamehttp.CreateRoute("/hello", gamehttp.GET, hello)

	student2route()
}

func path(c *gin.Context) {
	c.JSON(http.StatusOK, gamehttp.Success("?"))
}

func hello(c *gin.Context) {
	c.JSON(http.StatusOK, gamehttp.Success("hello game"))
}
