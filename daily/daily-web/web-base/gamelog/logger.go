package gamelog

import (
	"fmt"
	"github.com/natefinch/lumberjack"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"springmars.com/daily/web/base/constant"
	"springmars.com/daily/web/base/gamecore"
	"strings"
)

/*
@author pengshuo
@date 2021/8/27 14:11
version 1.0.0
desc: 日志输出
	zap + lumberjack
*/

// const field
const (
	loggerMaxSize     = "%s.app.logger.maxSize"
	loggerMaxBackups  = "%s.app.logger.maxBackups"
	loggerMaxAge      = "%s.app.logger.maxAge"
	loggerdebug       = "%s.app.logger.debug"
	loggerStdOn       = "%s.app.logger.stdOn"
	serverName        = "app"
	defaultMaxSize    = 50
	defaultMaxBackups = 1500
	defaultMaxAge     = 90
)

// fixme: zap AddCaller无法输出上级打印路劲，导致Log、Sugar没使用小写，即对外公布日志输出接口，也对外公布了Log、Sugar
var (
	// 在每一微秒和每一次内存分配都很重要的上下文中，使用Logger。它甚至比SugaredLogger更快，内存分配次数也更少
	Log *zap.Logger
	// 在性能很好但不是很关键的上下文中，使用SugaredLogger。它比其他结构化日志记录包快4-10倍
	Sugar  *zap.SugaredLogger
	logger *gameLogger = new(gameLogger)
)

// 应用日志
type gameLogger struct{}

// 根据配置初始化日志
func (*gameLogger) Cfg2Init(app, env string) {
	maxSize := viper.GetInt(fmt.Sprintf(loggerMaxSize, env))
	maxBackups := viper.GetInt(fmt.Sprintf(loggerMaxBackups, env))
	maxAge := viper.GetInt(fmt.Sprintf(loggerMaxAge, env))
	debug := viper.GetInt(fmt.Sprintf(loggerdebug, env))
	stdOn := viper.GetInt(fmt.Sprintf(loggerStdOn, env))
	if maxSize == 0 {
		maxSize = defaultMaxSize // file single max size
	}
	if maxBackups == 0 {
		maxBackups = defaultMaxBackups // file store max backups
	}
	if maxAge == 0 {
		maxAge = defaultMaxAge // file store max age
	}
	// 日志writer
	tee := make([]zapcore.Core, 0)
	tee = append(tee, getZapCore(getInfoLoggerWriter(maxSize, maxBackups, maxAge), stdOn, infoLevel))
	tee = append(tee, getZapCore(getErrorLoggerWriter(maxSize, maxBackups, maxAge), stdOn, errorLevel))
	tee = append(tee, getZapCore(getPanicLoggerWriter(maxSize, maxBackups, maxAge), stdOn, panicLevel))
	if constant.IsOn(constant.Status(debug)) {
		tee = append(tee, getZapCore(getDebugLoggerWriter(maxSize, maxBackups, maxAge), stdOn, debugLevel))
	}
	// appName 为空
	if len(strings.Trim(app, " ")) == 0 {
		Log = zap.New(zapcore.NewTee(tee...), zap.AddCaller())
	} else {
		Log = zap.New(zapcore.NewTee(tee...), zap.AddCaller(), zap.Fields(zap.String(serverName, app)))
	}
	Sugar = Log.Sugar()
}

// 加载配置初始化、释放资源
func init() {
	gamecore.AppendAppCfgInit(logger)
	gamecore.AppendAppShutDown(logger)
}

// app 关闭，释放资源
func (*gameLogger) Terminate() {
	Log.Sync()
	Sugar.Sync()
}

// zapcore.NewCore
func getZapCore(writer zapcore.WriteSyncer, status int, level zap.LevelEnablerFunc) zapcore.Core {
	//编码器配置 、 打印到控制台和文件 、 日志级别
	if constant.IsOn(constant.Status(status)) {
		return zapcore.NewCore(getJsonEncode(),
			zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(writer)), level)
	} else {
		return zapcore.NewCore(getJsonEncode(), zapcore.AddSync(writer), level)
	}
}

func panicLevel(level zapcore.Level) bool {
	return level >= zap.DPanicLevel
}

func errorLevel(level zapcore.Level) bool {
	return level == zap.ErrorLevel
}

func infoLevel(level zapcore.Level) bool {
	return level == zap.WarnLevel || level == zap.InfoLevel
}

func debugLevel(level zapcore.Level) bool {
	return level == zap.DebugLevel
}

// json encode
func getJsonEncode() zapcore.Encoder {
	// 时间格式化
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout("2006-01-02 15:04:05.000")
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	// return
	return zapcore.NewJSONEncoder(encoderConfig)
}

// LumberJack Info Logger Writer
func getInfoLoggerWriter(maxSize, maxBackups, maxAge int) zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "./logs/app.log",
		MaxSize:    maxSize,    //在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups: maxBackups, //保留旧文件的最大个数
		MaxAge:     maxAge,     //保留旧文件的最大天数
		Compress:   true,       //是否压缩/归档旧文件
		LocalTime:  true,
	}
	return zapcore.AddSync(lumberJackLogger)
}

// LumberJack Error Logger Writer
func getErrorLoggerWriter(maxSize, maxBackups, maxAge int) zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "./logs/error.log",
		MaxSize:    maxSize,    //在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups: maxBackups, //保留旧文件的最大个数
		MaxAge:     maxAge,     //保留旧文件的最大天数
		Compress:   true,       //是否压缩/归档旧文件
		LocalTime:  true,
	}
	return zapcore.AddSync(lumberJackLogger)
}

// LumberJack Error Logger Writer
func getDebugLoggerWriter(maxSize, maxBackups, maxAge int) zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "./logs/debug.log",
		MaxSize:    maxSize,    //在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups: maxBackups, //保留旧文件的最大个数
		MaxAge:     maxAge,     //保留旧文件的最大天数
		Compress:   true,       //是否压缩/归档旧文件
		LocalTime:  true,
	}
	return zapcore.AddSync(lumberJackLogger)
}

// LumberJack Panic Logger Writer
func getPanicLoggerWriter(maxSize, maxBackups, maxAge int) zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "./logs/panic.log",
		MaxSize:    maxSize,    //在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups: maxBackups, //保留旧文件的最大个数
		MaxAge:     maxAge,     //保留旧文件的最大天数
		Compress:   true,       //是否压缩/归档旧文件
		LocalTime:  true,
	}
	return zapcore.AddSync(lumberJackLogger)
}

// info format msg
func Infof(format string, args ...interface{}) {
	Sugar.Infof(format, args...)
}

// info msg
func Info(msg string) {
	Sugar.Info(msg)
}

// debug format msg
func Debugf(format string, args ...interface{}) {
	Sugar.Debugf(format, args...)
}

// debug msg
func Debug(msg string) {
	Sugar.Debug(msg)
}

// error format msg
func Errorf(format string, args ...interface{}) {
	Sugar.Errorf(format, args...)
}

// error msg
func Error(msg string) {
	Sugar.Error(msg)
}

// panic format msg
func Panicf(format string, args ...interface{}) {
	Sugar.Panicf(format, args...)
}

// panic msg
func Panic(msg string) {
	Sugar.Panic(msg)
}
