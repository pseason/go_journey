package constant

/*
@author pengshuo
@date 2021/8/27 15:58
version 1.0.0
desc:
	常量方法
*/

// 开启
func IsOn(status Status) bool {
	return status == On
}

// 关闭
func IsOff(status Status) bool {
	return status == Off
}
