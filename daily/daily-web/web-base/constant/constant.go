package constant

/*
@author pengshuo
@date 2021/8/27 15:58
version 1.0.0
desc:
	常量定义
*/
type Status int

const (
	On  Status = 1 //开启
	Off Status = 0 //关闭
)
