package gamecore

/*
@author pengshuo
@date 2021/8/27 14:34
version 1.0.0
desc:

*/

// 读取配置执行->Cfg2Init初始应用
var appCfgInit = make([]AppCfgInit, 0)

// 根据配置初始化
type AppCfgInit interface {
	// 根据配置初始化功能
	Cfg2Init(app, env string)
}

// append AppCfgInit
func AppendAppCfgInit(cfgInit ...AppCfgInit) {
	appCfgInit = append(appCfgInit, cfgInit...)
}

// 执行程序关闭前操作
func ReadConf(app, env string) {
	for _, appCfg := range appCfgInit {
		// 执行程序关闭前操作
		appCfg.Cfg2Init(app, env)
	}
}
