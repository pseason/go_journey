package gamecore

/*
@author pengshuo
@date 2021/8/28 15:14
version 1.0.0
desc:

*/
var appShutDown = make([]AppShutDown, 0)

// 服务关闭之后要做的事情
type AppShutDown interface {
	// 执行程序关闭前操作
	Terminate()
}

func AppendAppShutDown(shut ...AppShutDown) {
	appShutDown = append(appShutDown, shut...)
}

// 执行程序关闭前操作
func Terminate() {
	for _, shut := range appShutDown {
		// 执行程序关闭前操作
		shut.Terminate()
	}
}
