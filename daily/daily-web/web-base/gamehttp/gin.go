package gamehttp

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net"
	"net/http"
	"os"
	"runtime/debug"
	"springmars.com/daily/web/base/gamelog"
	"strings"
	"time"
)

/*
@author pengshuo
@date 2021/8/31 16:05
version 1.0.0
desc:
	gin 封装
*/

var (
	// 路由
	handlers         = make([]route, 0)
	gin_release_mode = "release"
)

type (
	HttpMethod int
	// 自定义route结构
	route struct {
		url     string
		method  HttpMethod
		handler func(c *gin.Context)
	}
)

// http method
const (
	GET HttpMethod = iota + 1
	POST
	DELETE
	PUT
	PATCH
)

// 创建路由
func CreateRoute(url string, method HttpMethod, handler func(c *gin.Context)) {
	handlers = append(handlers, route{url: url, method: method, handler: handler})
}

// 循环注册路由
func registerRoute(engine *gin.Engine) {
	for _, route := range handlers {
		switch route.method {
		case GET:
			engine.GET(route.url, route.handler)
		case POST:
			engine.POST(route.url, route.handler)
		case DELETE:
			engine.DELETE(route.url, route.handler)
		case PUT:
			engine.PUT(route.url, route.handler)
		case PATCH:
			engine.PATCH(route.url, route.handler)
		default:
			gamelog.Sugar.Errorf("路由注册失败 method: %d ,url: %s", route.method, route.url)
		}
	}
}

// 开启Gin
func StartGin(env string, port int) {
	if env == gin_release_mode {
		gin.SetMode(gin.ReleaseMode)
	}
	engine := gin.New()
	// 跨域
	engine.Use(cors())
	//// 日志处理
	engine.Use(logger(), recovery())
	// 注册路由
	registerRoute(engine)
	uri := fmt.Sprintf("127.0.0.1:%d", port)
	gamelog.Sugar.Infof("gin-web start at: %s", uri)
	if err := engine.Run(uri); err != nil {
		gamelog.Sugar.Panicf("程序启动失败: %s", err.Error())
	}
}

// gin cors 跨域 中间件
func cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		origin := c.Request.Header.Get("Origin")
		if origin != "" {
			// 可将将 * 替换为指定的域名
			c.Header("Access-Control-Allow-Origin", "*")
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
			c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
			c.Header("Access-Control-Allow-Credentials", "true")
		}
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}

// gin logger 中间件
func logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()

		c.Next()

		cost := time.Now().Sub(start)
		gamelog.Sugar.Debugf("gin-web: %s?%s, method: %s, ip: %s, cost: %v", c.Request.URL.Path, c.Request.URL.RawQuery, c.Request.Method, c.ClientIP(), cost)
	}
}

// 统一异常处理，并返回，recover掉项目可能出现的panic，并使用zap记录相关日志
func recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// Check for a broken connection, as it is not really a
				// condition that warrants a panic stack trace.
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") || strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}
				if brokenPipe {
					gamelog.Sugar.Errorf("[gin-web][broken pipe],path: %s, error: %s", c.Request.URL.Path, err)
				} else {
					gamelog.Sugar.Errorf("[gin-web][recovery from panic],path: %s, stack: %s", c.Request.URL.Path, string(debug.Stack()))
				}
				// 统一错误处理，abort终止接口后续调用
				c.AbortWithStatusJSON(http.StatusOK, Error(err))
			}
		}()
		c.Next()
	}
}
