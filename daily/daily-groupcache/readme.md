go groupcache 实践


1. peersAddrs 为其他 group cache 节点

2. localAddr为本地节点ip

3. 请求流程

    localhost:xxx/get?key=xx

    1）  在本地group cache中查找cache

    2.1）hitCache,直接返回
    2.2）未命中，请求其他节点（一致hash，根据key计算），获取cache
    2.3）其他节点命中，返回cache，本地缓存
    2.4）其他节点未命中，本节点通过默认方式hit

    3）  默认方式hit，存在返回并缓存，不存在返回null