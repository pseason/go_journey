package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/golang/groupcache"
	"log"
	"net/http"
	"strconv"
)

/*
@author pengshuo
@date 2021/8/23 14:30
version 1.0.0
desc:

*/

var (
	peersAddrs = []string{"http://localhost:8001", "http://localhost:8002"}

	db = map[string]string{
		"a": "123",
		"b": "12345",
		"c": "123456",
		"d": "1234567",
		"e": "12345678",
		"f": "123456789",
	}
)

func main() {
	var port int
	flag.IntVar(&port, "port", 9019, "-port=9018")
	flag.Parse()

	localAddr := "localhost:" + strconv.Itoa(port)

	peers := groupcache.NewHTTPPool("http://" + localAddr)
	peers.Set(peersAddrs...)

	group := groupcache.NewGroup("scores", 2<<3,
		groupcache.GetterFunc(func(ctx context.Context, key string, dest groupcache.Sink) error {
			fmt.Printf("get %s of value from db \n", key)
			if v, ok := db[key]; ok {
				dest.SetString(v)
			} else {
				fmt.Errorf("get %s of value from db error", key)
			}
			return nil
		}))

	// 定义返回方式
	http.HandleFunc("/get", func(rw http.ResponseWriter, r *http.Request) {
		var data []byte
		k := r.URL.Query().Get("key")
		fmt.Printf("user get %s of value from groupcache\n", k)

		group.Get(r.Context(), k, groupcache.AllocatingByteSliceSink(&data))

		rw.Write(data)
	})

	log.Fatal(http.ListenAndServe(localAddr, nil))

}
