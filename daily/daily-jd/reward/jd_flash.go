package reward

import (
	"encoding/json"
	"fmt"
	"github.com/nahid/gohttp"
)

/*
@author pengshuo
@date 2021/10/9 17:18
version 1.0.0
desc:
	京东商城-京东闪购
*/
const flashUrl = "https://api.m.jd.com/client.action?functionId=partitionJdSgin"

func jDongFlash(cookie string) {
	request := gohttp.NewRequest()
	response, err := request.Headers(map[string]string{
		"Cookie":     cookie,
		"User-Agent": userAgent,
	}).FormData(map[string]string{
		"body":  "{\"version\":\"v2\"}&client=apple&clientVersion=9.0.8&openudid=1fce88cd05c42fe2b054e846f11bdf33f016d676&sign=6768e2cf625427615dd89649dd367d41&st=1597248593305&sv=121",
		"appid": appId,
	}).Post(flashUrl)
	if err != nil {
		fmt.Println("***【京东商城】京东闪购 请求异常***", err)
	}
	bytes, _ := response.GetBodyAsByte()
	var flash jdFlash
	_ = json.Unmarshal(bytes, &flash)
	if response.GetStatusCode() == 200 {
		if flash.Code == "0" {
			jdBeanCount := flash.Result["jdBeanNum"]
			if jdBeanCount == nil {
				fmt.Println("【京东商城-京东闪购】,签到成功: 0 京豆")
			} else {
				fmt.Printf("【京东商城-京东闪购】,签到成功: %v 京豆 \n", jdBeanCount)
			}
		} else {
			fmt.Println("【京东商城-京东闪购】,签到失败,原因：", flash.Echo)
		}
	} else {
		fmt.Println("【京东商城-京东闪购】,返回异常,response：", string(bytes))
	}
}

type jdFlash struct {
	Code   string                 `json:"code"`
	Echo   string                 `json:"echo"`
	Result map[string]interface{} `json:"result"`
}
