package reward

import (
	"encoding/json"
	"fmt"
	"github.com/nahid/gohttp"
)

/*
@author pengshuo
@date 2021/10/9 16:58
version 1.0.0
desc:
	京东商城-京东转盘
*/
const turnUrl = "https://api.m.jd.com/client.action?functionId=babelGetLottery"

func jDongTurn(cookie string) {
	request := gohttp.NewRequest()
	response, err := request.Headers(map[string]string{
		"Cookie":     cookie,
		"User-Agent": userAgent,
	}).FormData(map[string]string{
		"body":  "{\"enAwardK\":\"95d235f2a09578c6613a1a029b26d12d\",\"riskParam\":{}}&client=wh5",
		"appid": appId,
	}).Post(turnUrl)
	if err != nil {
		fmt.Println("***【京东商城】京东转盘 请求异常***", err)
	}
	bytes, _ := response.GetBodyAsByte()
	var turn jdTurn
	_ = json.Unmarshal(bytes, &turn)
	if response.GetStatusCode() == 200 {
		if turn.Code == "0" {
			fmt.Printf("【京东商城-京东转盘】,%s %s \n", turn.PromptMsg, turn.ReturnMsg)
		} else {
			fmt.Println("【京东商城-京东转盘】,转动失败,原因：", turn.Echo)
		}
	} else {
		fmt.Println("【京东商城-京东转盘】,返回异常,response：", string(bytes))
	}
}

type jdTurn struct {
	Code      string `json:"code"`
	Msg       string `json:"msg"`
	Echo      string `json:"echo"`
	ReturnMsg string `json:"returnMsg"`
	PromptMsg string `json:"promptMsg"`
}
