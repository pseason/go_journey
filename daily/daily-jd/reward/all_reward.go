package reward

/*
@author pengshuo
@date 2021/10/9 15:45
version 1.0.0
desc:

	// 解析
	bytes, _ := response.GetBodyAsByte()
	fmt.Println("【京东商城】xxx 结果", string(bytes))

*/
const userAgent = "JD4iPhone/167169 (iPhone; iOS 13.4.1; Scale/3.00)"
const appId = "ld"

// 领取所有奖励
func GetAllReward(cookie, jrBody string) {
	// 京东商城-京东豆
	jDongBean(cookie)
	// 京东商城-超时签到
	jDongStore(cookie)
	// 京东商城-金融钢镚
	jDongSteel(cookie, jrBody)
	// 京东商城-京东转盘
	jDongTurn(cookie)
	// 京东商城-京东闪购
	jDongFlash(cookie)
}
