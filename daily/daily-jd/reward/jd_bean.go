package reward

import (
	"encoding/json"
	"fmt"
	"github.com/nahid/gohttp"
)

/*
@author pengshuo
@date 2021/10/9 13:36
version 1.0.0
desc:
	京东商城-京东豆
*/
const functionId = "signBeanIndex"
const beanUrl = "https://api.m.jd.com/client.action"

// 领取京东豆
func jDongBean(cookie string) {
	request := gohttp.NewRequest()
	response, err := request.Headers(map[string]string{
		"Cookie":     cookie,
		"User-Agent": userAgent,
	}).FormData(map[string]string{
		"functionId": functionId,
		"appid":      appId,
	}).Post(beanUrl)
	if err != nil {
		fmt.Println("***【京东商城】领取京豆请求异常***", err)
	}
	// 解析
	bytes, _ := response.GetBodyAsByte()
	var bean jdBean
	_ = json.Unmarshal(bytes, &bean)
	if response.GetStatusCode() == 200 {
		if bean.Code == "0" {
			if bean.Data.BeanUserType == 1 {
				// 新人
				fmt.Printf("【京东商城-每日京豆】,%s%s: %s 京豆 \n", bean.Data.DailyAward.Title, bean.Data.DailyAward.SubTitle, bean.Data.DailyAward.BeanAward["beanCount"])
			} else if bean.Data.BeanUserType == 2 {
				fmt.Printf("【京东商城-每日京豆】,%s: %s 京豆 \n", bean.Data.NewUserAward.Title, bean.Data.NewUserAward.AwardList[0]["beanCount"])
			}
		} else if bean.Code == "3" {
			fmt.Println("【京东商城-每日京豆】,领取失败,原因：", bean.ErrorMessage)
		}
	} else {
		fmt.Println("【京东商城-每日京豆】,返回异常,response：", string(bytes))
	}
}

type jdBean struct {
	Code         string     `json:"code"`
	Data         jdBeanData `json:"data"`
	ErrorMessage string     `json:"errorMessage"`
}

type jdBeanData struct {
	Status       string          `json:"status"`
	BeanUserType int             `json:"beanUserType"`
	DailyAward   jdBeanDataAward `json:"dailyAward"`
	NewUserAward jdBeanDataAward `json:"newUserAward"`
}

type jdBeanDataAward struct {
	Title     string              `json:"title"`
	SubTitle  string              `json:"subTitle"`
	BeanAward map[string]string   `json:"beanAward"`
	AwardList []map[string]string `json:"awardList"`
}
