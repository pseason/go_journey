package reward

import (
	"encoding/json"
	"fmt"
	"github.com/nahid/gohttp"
)

/*
@author pengshuo
@date 2021/10/9 15:52
version 1.0.0
desc:
	京东商城-超市签到
*/
const storeUrl = "https://api.m.jd.com/api?appid=jdsupermarket&functionId=smtg_sign&clientVersion=8.0.0&client=m&body=%7B%7D"
const marketOrigin = "https://jdsupermarket.jd.com"

// 京东商城-超市签到
func jDongStore(cookie string) {
	request := gohttp.NewRequest()
	response, err := request.Headers(map[string]string{
		"Cookie":     cookie,
		"User-Agent": userAgent,
		"Origin":     marketOrigin,
	}).Get(storeUrl)
	if err != nil {
		fmt.Println("***【京东商城】超市签到,请求异常***", err)
	}
	// 解析
	bytes, _ := response.GetBodyAsByte()
	var store jdStore
	_ = json.Unmarshal(bytes, &store)
	// check
	if response.GetStatusCode() == 200 {
		if store.Code == 0 {
			if store.Data.BizCode == 0 {
				jdBeanCount := store.Data.Result["jdBeanCount"]
				if jdBeanCount == nil {
					fmt.Println("【京东商城-超市签到】,签到成功: 0 京豆")
				} else {
					fmt.Printf("【京东商城-超市签到】,签到成功: %v 京豆 \n", jdBeanCount)
				}
			} else {
				fmt.Printf("【京东商城-超市签到】,%s \n", store.Data.BizMsg)
			}
		} else {
			fmt.Println("【京东商城-超市签到】,领取失败,原因：", store.Msg)
		}
	} else {
		fmt.Println("【京东商城-超市签到】,返回异常,response：", string(bytes))
	}
}

type jdStore struct {
	Code int         `json:"code"`
	Data jdStoreData `json:"data"`
	Msg  string      `json:"msg"`
}

type jdStoreData struct {
	BizCode int                    `json:"bizCode"`
	BizMsg  string                 `json:"bizMsg"`
	Result  map[string]interface{} `json:"result"`
}
