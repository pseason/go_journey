package reward

import (
	"encoding/json"
	"fmt"
	"github.com/nahid/gohttp"
)

/*
@author pengshuo
@date 2021/10/9 16:26
version 1.0.0
desc:
	京东商城-金融钢镚
*/
const steelUrl = "https://ms.jr.jd.com/gw/generic/hy/h5/m/appSign"

func jDongSteel(cookie, body string) {
	request := gohttp.NewRequest()
	response, err := request.Headers(map[string]string{
		"Cookie":     cookie,
		"User-Agent": userAgent,
	}).FormData(map[string]string{
		"reqData": "xxxxxxxxx",
	}).Post(steelUrl)
	if err != nil {
		fmt.Println("***【京东商城】金融钢镚 请求异常***", err)
	}
	// 解析
	bytes, _ := response.GetBodyAsByte()
	var steel jdSteel
	_ = json.Unmarshal(bytes, &steel)
	if response.GetStatusCode() == 200 {
		if steel.ResultCode == 0 {
			if steel.ResultData.ResBusiCode == 0 {
				fmt.Println("【京东商城-金融钢镚】, 获得钢镚奖励")
			} else {
				fmt.Printf("【京东商城-金融钢镚】,%s \n", steel.ResultData.ResBusiMsg)
			}
		} else {
			fmt.Println("【京东商城-金融钢镚】,领取失败,原因：", steel.ResultMsg)
		}
	} else {
		fmt.Println("【京东商城-金融钢镚】,返回异常,response：", string(bytes))
	}
}

type jdSteel struct {
	ResultCode int         `json:"resultCode"`
	ResultMsg  string      `json:"resultMsg"`
	ResultData jdSteelData `json:"resultData"`
}
type jdSteelData struct {
	ResBusiCode int    `json:"resBusiCode"`
	ResBusiMsg  string `json:"resBusiMsg"`
}
