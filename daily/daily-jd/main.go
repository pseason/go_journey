package main

import (
	"bufio"
	"fmt"
	"github.com/urfave/cli"
	"os"
	"springmars.com/daily/jd/login"
	"springmars.com/daily/jd/reward"
	"strconv"
)

/*
@author pengshuo
@date 2021/10/9 10:39
version 1.0.0
desc:
    京东商城 日常福利
*/

var signPhoneNum int
var signVerifyCode string

func main() {
	// app信息
	showAppInfo()
	fmt.Println("--------------- 开始 ---------------")
	// 输入 手机号码
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("请输入【京东商城】手机号码：")
	for scanner.Scan() {
		input := scanner.Text()
		phone, err := strconv.Atoi(input)
		if err != nil {
			fmt.Println("***【京东商城】手机号码输入有误***")
			os.Exit(-1)
		}
		signPhoneNum = phone
		break
	}
	// 请求登陆京东自动获取 验证码
	err := login.SignByPhoneSenderVerifyCode(signPhoneNum)
	if err != nil {
		fmt.Println("***【京东商城】手机号码登陆异常***")
		os.Exit(-1)
	}
	// 输入 验证码
	scanner = bufio.NewScanner(os.Stdin)
	fmt.Println("请输入【京东商城】手机验证码：")
	for scanner.Scan() {
		signVerifyCode = scanner.Text()
		break
	}
	// todo 完成签到等福利领取
	cookie := "__jdv=76161171|baidu|-|organic|not set|1633692026430; __jdu=1633692026429228576397; areaId=2; ipLoc-djd=2-2813-51976-0; PCSYCityID=CN_310000_310100_310104; shshshfpa=fa5d8330-6637-5d53-5bbd-3108b6c1c7c7-1633692027; TrackID=1H7Hrlqqs8VDAnZjIdjw4zTzePcV9uPLvHPSlC27_JIm4jhtecVfydU_3jmckFMqV92XTI9QQt7GL4qw8f2Nu2aj5zDl-y2_27X66QYL7Wgc; pinId=x_lgwqTOdKf5h16Re1KBs7V9-x-f3wj7; pin=jd_73135902d99ab; unick=pseason; _tp=YNUBnRY/uNWdLPWGyHnKJ7p2yCVqhdBKWnMmjDgMX9Q=; _pst=jd_73135902d99ab; shshshfpb=gGYa9q4 X5 grZ05mi ewJA==; jcap_dvzw_fp=iSN-Gn824t9BcaM-4T8cg6Xx9Ppft5ZtBA2ECMxwRbnUODrY8LEN9uRDpJajz8tIP6V8_A==; __jdc=76161171; __jda=76161171.1633692026429228576397.1633692026.1633751177.1633762305.3; wxa_level=1; cid=9; retina=1; jxsid=16337623257762208390; webp=1; mba_muid=1633692026429228576397; visitkey=18487135637998334; 3AB9D23F7A4B3C9B=VJUWVYANQRDFTIWRVWEATF6OTANLNO64FEYQ23RRK6523YOALLQ5VE527XF3OK6W7HVT5POBHQPN6MLU7WKAXAK6YA; TrackerID=jpF0G1P5ZpheJgp7YOEcsE3_jfHuLr03KE1YWXufAJBcH2TrO9rq7NBMSKZUkweZnEQ6wrkPIMR_HjSwICNYzmHcUtECW5pgusOsJCxizmzGqmXfg7FiIrp4Vb64P6CAKWzHi_cSy9JMZq519vqCQg; pt_key=AAJhYTxYADAH9KTN4H6QJyY3KjFkPCrXZPHil3-dwSKgabocxQ7oTTvXpkW4D4Ju7uuecQW-sEw; pt_pin=jd_73135902d99ab; pt_token=9qy0cbeb; pwdt_id=jd_73135902d99ab; sfstoken=tk01m81041b2ba8sMysyKzF4M3gxE763QP+XAmIpTLO07UwLKC6mZHS3BoCjYy+iEjKU81NfT2OR3jTKkAdlq4saXIVk; whwswswws=; PPRD_P=UUID.1633692026429228576397; sc_width=414; shshshfp=916543ca9a2703ad2aa1e1b9550c6bbb; jxsid_s_u=https://home.m.jd.com/myJd/newhome.action; wqmnx1=MDEyNjM5OGguL2FldTQ5aShDZV9hQWkxLGNpMzFmMXNuMlFEKkg=; __jdb=76161171.10.1633692026429228576397|3.1633762305; mba_sid=16337623258499191737942102076.9; __wga=1633762507738.1633762393439.1633762393439.1633762393439.6.1; jxsid_s_t=1633762507775; shshshsID=d3454018833c874e11e51980b86936b7_8_1633762507873"

	//cookie := "__jda=122270672.16337638555861613594801.1633763855.1633763855.1633763855.1; __jdv=122270672%7Cdirect%7C-%7Cnone%7C-%7C1633763855586; mba_muid=16337638555861613594801; __jdc=122270672; shshshfp=de7919918b31619c3bf4ad24c2e2d300; shshshfpa=a2617647-0b05-7cd4-4c20-458871b1329a-1633763857; shshshsID=c4359325d4d86c397061a4603e1590c7_1_1633763857089; shshshfpb=kCj%2FiRdKtGPCKuwCHRGvC%2Fw%3D%3D; 3AB9D23F7A4B3C9B=37RPI3NSJM236VAJYJVHVBCWZOJTVBSRGRSW52UMPCMMR2FN2R3PXHILVYVXXQUNXANQMNJGYLNAFFTTQS2XJIT6UY; jcap_dvzw_fp=X01UQjdVWj_TIF-shpkFTjVE7sRhofxT8IaZjKVp4b31OMkIuil6PXaVbD8ZfGtlXnz7KA==; TrackerID=A-_72yT7Vj7GD-W24crbimmms0veFFCL11COC3nE5vOneQ_GRsMayzUI7NWtY7HICdwhInniUgf5r970QciEDlCOq83BHlXesg2_lq5smOtQ9q74-PtOmf3kwVClz9UFcOeO1NSlXSVo-cMvw7hnJA; pt_key=AAJhYUJFADBBxxEGbMn2dRKGgYE7aLhQ28BN82W-fVoaCsfYYOh1qT-Dch4LJcY5gfUZrGJ1mSQ; pt_pin=jd_45052a5567a20; pt_token=jruboyef; pwdt_id=jd_45052a5567a20; sfstoken=tk01md5af1c9da8sM3gzeDMrMVcwzY3FjyMf23yKnVMrktI+Td8MkbMuynUKhRkXbtXWAQM/7nW1qWn0HTF7vXdN7L3w; whwswswws=; wxa_level=1; retina=1; cid=9; wqmnx1=MDEyNjM4NTptL3d0by5uaVMgY0FLMVRlIC9NNWYucmQyNE9JSEg%3D; jxsid=16337639112045438987; webp=1; visitkey=60839082294584074; __jdb=122270672.2.16337638555861613594801|1.1633763855; mba_sid=16337638555877995831912926803.2"

	reward.GetAllReward(cookie, "")
}

//app信息
func showAppInfo() {
	app := cli.NewApp()
	app.Name = "【京东商城 日常福利】"
	app.Author = "pseason"
	app.Version = "1.0.0"
	app.Copyright = "springmars.com"
	app.Usage = ""
	app.UsageText = "输入手机号码自动获取【京东商城】验证码，自动完成【京东商城】日常福利领取..."
	app.HideHelp = true
	app.UseShortOptionHandling = false
	app.Run(os.Args)
}
