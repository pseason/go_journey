package login

import "fmt"

/*
@author pengshuo
@date 2021/10/9 11:36
version 1.0.0
desc:

*/

// login by phone sender verify code
func SignByPhoneSenderVerifyCode(signPhoneNum int) error {
	fmt.Println("获取【京东商城】登陆手机验证码 开始")

	fmt.Println("获取【京东商城】登陆手机验证码 成功")
	return nil
}
