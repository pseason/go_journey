module springmars.com/daily/jd

go 1.16

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/nahid/gohttp v0.0.1 // indirect
	github.com/urfave/cli v1.22.5 // indirect
)
