package main

import (
	"springmars.com/daily/gormcache/cacheapi"
	"springmars.com/daily/gormcache/conf"
)

/*
@author pengshuo
@date 2021/10/25 9:53
version 1.0.0
desc:

*/

func main() {
	defer cacheapi.Terminate()
	// 加载配置
	conf.LoadAppConfig()
}
