package database

/*
@author pengshuo
@date 2021/10/25 10:36
version 1.0.0
desc:
	基础数据模型
*/
// 基础数据模型
type BaseModel interface {
	// 获取当前唯一key
	GetPrimaryKey() string
	// 获取 group cache group name
	GetGroupName() string
}

// 查询字段
type FieldData struct {
	Column string      `json:"key"`    // 字段名称
	Value  interface{} `json:"value"`  // 字段value
	Symbol string      `json:"symbol"` // 符号
}

// 查询条件
type QueryRecordForm struct {
	Params   []*FieldData `json:"params" form:"params"`
	Order    []string     `json:"order" form:"order"`
	PageNum  int          `json:"pageNum" form:"pageNum"`
	PageSize int          `json:"pageSize" form:"pageSize"`
}
