package cacheapi

import (
	"context"
	"github.com/golang/groupcache"
	"log"
	"testing"
)

/*
@author pengshuo
@date 2021/10/25 10:06
version 1.0.0
desc:

*/

func TestGroupCache(t *testing.T) {
	group := groupcache.NewGroup("tea", maxCacheBytes, getFromDB)

	key := "a"

	var data []byte
	err := group.Get(context.Background(), key, groupcache.AllocatingByteSliceSink(&data))
	if err == nil {
		log.Printf("get value from cache, key: %s, result: %s \n", key, string(data))
	}

	err = group.Get(context.Background(), key, groupcache.AllocatingByteSliceSink(&data))
	if err == nil {
		log.Printf("get value from cache, key: %s, result: %s \n", key, string(data))
	}
}
