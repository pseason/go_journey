package cacheapi

import (
	"context"
	"github.com/golang/groupcache"
	"log"
)

/*
@author pengshuo
@date 2021/10/22 17:41
version 1.0.0
desc:

*/

// 100m
const (
	maxCacheBytes = 1024 * 1024 * 100
)

var (
	getFromDB = groupcache.GetterFunc(func(ctx context.Context, key string, dest groupcache.Sink) error {
		log.Printf("get value from db key: [%s]\n", key)

		// todo

		log.Printf("get value from db error key: [%s]\n", key)
		return nil
	})
)

func BuildCacheGroup(name string) {
	BuildCacheGroupWithBytes(name, maxCacheBytes)
}

func BuildCacheGroupWithBytes(name string, cacheBytes int64) {
	groupcache.NewGroup(name, cacheBytes, getFromDB)
}
