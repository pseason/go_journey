module springmars.com/daily/gormcache

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da
	github.com/kr/pretty v0.3.0 // indirect
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.9.0
	golang.org/x/sys v0.0.0-20211023085530-d6a326fbbf70 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.16
)
