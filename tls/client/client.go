package main

import (
	"crypto/tls"
	"crypto/x509"
	"log"
	"os"
)

/*
@author pf
@date 2025/1/9 10:14
version 1.0.0
desc:

*/

func main() {
	cert, err := tls.LoadX509KeyPair("../secret/client.crt", "../secret/client.key")
	if err != nil {
		log.Println(err)
		return
	}

	certBytes, err := os.ReadFile("../secret/ca.crt")
	if err != nil {
		panic("Unable to read cert.pem")
	}
	clientCertPool := x509.NewCertPool()
	ok := clientCertPool.AppendCertsFromPEM(certBytes)
	if !ok {
		panic("failed to parse root certificate")
	}
	conf := &tls.Config{
		RootCAs:      clientCertPool,
		Certificates: []tls.Certificate{cert},
		//InsecureSkipVerify: true,
	}
	conn, err := tls.Dial("tcp", "127.0.0.1:443", conf)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	n, err := conn.Write([]byte("hello\n"))
	if err != nil {
		log.Println(n, err)
		return
	}
	buf := make([]byte, 100)
	n, err = conn.Read(buf)
	if err != nil {
		log.Println(n, err)
		return
	}
	println(string(buf[:n]))
}
