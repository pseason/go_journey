生成一组 TLS 服务器和客户端证书通常涉及使用 OpenSSL 工具。以下是一个完整的步骤指南，帮助您生成自签名的 TLS 证书，包括服务器证书和客户端证书。

### 步骤 1：安装 OpenSSL

如果您还没有安装 OpenSSL，可以在大多数操作系统上通过包管理器进行安装。

- **Ubuntu / Debian**:
  ```bash
  sudo apt-get install openssl
  ```

- **CentOS / RHEL**:
  ```bash
  sudo yum install openssl
  ```

- **MacOS**:
  ```bash
  brew install openssl
  ```

### 步骤 2：创建 CA（证书颁发机构）

首先，您需要创建一个根证书（CA），以便为服务器和客户端证书签名。

1. **创建私钥**:
   ```bash
   openssl genrsa -out ca.key 2048
   ```

2. **生成根证书**:
   ```bash
   openssl req -x509 -new -nodes -key ca.key -sha256 -days 1024 -out ca.crt -subj "/C=US/ST=California/L=San Francisco/O=My Organization/CN=My CA"
   ```

### 步骤 3：生成服务器证书

1. **创建私钥**:
   ```bash
   openssl genrsa -out server.key 2048
   ```

2. **生成证书请求（CSR）**:
   ```bash
   openssl req -new -key server.key -out server.csr -subj "/C=US/ST=California/L=San Francisco/O=My Organization/CN=localhost"
   ```

3. **创建一个配置文件**（例如`server.cnf`），内容如下：

   ```ini
   [req]
   distinguished_name = req_distinguished_name
   req_extensions = v3_req
   prompt = no

   [req_distinguished_name]
   C = US
   ST = California
   L = San Francisco
   O = My Organization
   CN = localhost

   [v3_req]
   keyUsage = critical, digitalSignature, keyEncipherment
   extendedKeyUsage = serverAuth
   subjectAltName = @alt_names

   [alt_names]
   DNS.1 = localhost
   IP.1 = 127.0.0.1
   ```

4. **使用 CA 签署服务器证书**:
   ```bash
   openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 1024 -sha256 -extfile server.cnf -extensions v3_req
   ```

### 步骤 4：生成客户端证书

1. **创建私钥**:
   ```bash
   openssl genrsa -out client.key 2048
   ```

2. **生成证书请求（CSR）**:
   ```bash
   openssl req -new -key client.key -out client.csr -subj "/C=US/ST=California/L=San Francisco/O=My Organization/CN=client"
   ```

3. **使用 CA 签署客户端证书**:
   ```bash
   openssl x509 -req -in client.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out client.crt -days 1024 -sha256 -extfile <(printf "[v3_req]\nkeyUsage=digitalSignature\nextendedKeyUsage=clientAuth")
   ```

### 步骤 5：验证证书

可以使用以下命令验证生成的证书：

- 验证服务器证书：
  ```bash
  openssl x509 -in server.crt -text -noout
  ```

- 验证客户端证书：
  ```bash
  openssl x509 -in client.crt -text -noout
  ```

### 总结

现在，您已经生成了一组包含自签名 CA、服务器证书和客户端证书的 TLS 证书。可以在开发和测试环境中使用这些证书来进行 TLS 通信。在生产环境中，建议使用受信任的公共证书颁发机构（CA）签发的证书。