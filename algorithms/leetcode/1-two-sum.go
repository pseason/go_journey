package leetcode

/*
@author pengshuo
@date 2022/2/18 16:20
version 1.0.0
desc: https://leetcode-cn.com/problems/two-sum/
1.两数字和
	给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标
思路：
	哈希表：遍历数组，将数字和下标存入哈希表，遍历哈希表，如果哈希表中存在目标值-当前数字，则返回下标
*/

func twoSum(nums []int, target int) []int {
	if len(nums) < 2 {
		panic("array length must >= 2")
	}
	tmpMap := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		cur := nums[i]
		next := target - cur
		if j, ok := tmpMap[next]; ok {
			return []int{j, i}
		}
		tmpMap[cur] = i
	}
	panic("not found")
}
