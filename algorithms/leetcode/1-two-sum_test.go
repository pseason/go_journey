package leetcode

import (
	"reflect"
	"testing"
)

/*
@author pengshuo
@date 2022/2/18 16:43
version 1.0.0
desc:

*/

func Test_twoSum(t *testing.T) {
	type args struct {
		nums   []int
		target int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{{
		name: "1",
		args: args{nums: []int{1, 2, 3, 4, 5, 6}, target: 5},
		want: []int{1, 2},
	},
		{
			name: "2",
			args: args{nums: []int{1, 2, 7, 4, 5, 9}, target: 16},
			want: []int{2, 5},
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := twoSum(tt.args.nums, tt.args.target); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("twoSum() = %v, want %v", got, tt.want)
			}
		})
	}
}
