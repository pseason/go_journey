### Go Journey

Go Study Journey
##### 赞助感谢
[Jetbrains](https://www.jetbrains.com/)

![alt https://www.jetbrains.com/](https://www.jetbrains.com/apple-touch-icon.png)
##### 目录
- [模仿 go groupcache](https://gitee.com/pseason/go_journey/tree/master/imitate-groupcache)
- [daily module](https://gitee.com/pseason/go_journey/tree/master/daily)
##### 其他
```
curl -H "Content-Type: application/json" -X POST -d '{"name":"lili"}' "http://192.168.50.188:19001/login"
```
##### 参考
[Github Go Daily Trending](https://github.com/trending/go?since=daily)
