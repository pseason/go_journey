package 行为模式

import (
	"fmt"
	"time"
)

/*
@author pengshuo
@date 2021/8/11 20:03
version 1.0.0
desc:
	观察员
*/

type (
	event struct {
		data int64
	}

	observer interface {
		onNotify(event)
	}

	notify interface {
		register(observer)
		unregister(observer)
		notify(event)
	}
)

type (
	eventObserver struct {
		id int
	}

	eventNotify struct {
		observers map[observer]struct{}
	}
)

func (o *eventObserver) onNotify(e event) {
	fmt.Printf("*** Observer %d received: %d\n", o.id, e.data)
}

func (o *eventNotify) register(e observer) {
	o.observers[e] = struct{}{}
}

func (o *eventNotify) unregister(e observer) {
	delete(o.observers, e)
}

func (o *eventNotify) notify(e event) {
	for ob := range o.observers {
		ob.onNotify(e)
	}
}

// 使用观察者模式
func observerUsage() {
	en := eventNotify{
		observers: map[observer]struct{}{},
	}
	en.register(&eventObserver{id: 1})
	en.register(&eventObserver{id: 1})

	// A simple loop publishing the current Unix timestamp to observers.
	stop := time.NewTimer(10 * time.Second).C
	tick := time.NewTicker(time.Second).C
	for true {
		select {
		case <-stop:
			return
		case t := <-tick:
			en.notify(event{data: t.UnixNano()})
		}
	}
}
