package 结构模式

import "log"

/*
@author pengshuo
@date 2021/8/11 19:56
version 1.0.0
desc:
	装饰模式
*/

type obj func(int) int

func logDec(fn obj) obj {
	return func(i int) int {
		log.Println("Starting the execution with the integer", i)

		res := fn(i)

		log.Println("Execution is completed with the result", res)
		return res
	}
}

// 使用装饰模式
func usageObj() {
	double := func(i int) int {
		return i * i
	}

	dec := logDec(double)

	dec(10)
}
