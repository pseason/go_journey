package build

/*
@author pengshuo
@date 2021/8/11 17:31
version 1.0.0
desc:
	创造模式
*/
type Speed float64

const (
	MPH Speed = 1
	KPH       = 1.60934
)

type Color string

const (
	BlueColor  Color = "blue"
	GreenColor       = "green"
	RedColor         = "red"
)

type Wheels string

const (
	SportsWheels Wheels = "sports"
	SteelWheels         = "steel"
)

type Builder interface {
	Color(Color) Builder
	Wheels(Wheels) Builder
	TopSpeed(Speed) Builder
	Build() Interface
}

type Interface interface {
	Drive() error
	Stop() error
}

type Car struct {
	speed Speed
	color Color
	wheel Wheels
}

func NewCar() Car {
	return Car{}
}

func (c Car) Color(color Color) Builder {
	c.color = color
	return c
}

func (c Car) Wheels(w Wheels) Builder {
	c.wheel = w
	return c
}

func (c Car) TopSpeed(s Speed) Builder {
	c.speed = s
	return c
}

func (c Car) Build() Interface {
	return c
}

func (c Car) Drive() error {
	return nil
}

func (c Car) Stop() error {

	return nil
}

func usage() {

	assembly := NewCar().Color(BlueColor)

	familyCar := assembly.Wheels(SportsWheels).TopSpeed(50 * MPH).Build()
	familyCar.Drive()

	sportsCar := assembly.Wheels(SteelWheels).TopSpeed(150 * MPH).Build()
	sportsCar.Stop()
}
