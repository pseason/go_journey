package build

import (
	"fmt"
	"math/rand"
)

/*
@author pengshuo
@date 2021/8/11 19:36
version 1.0.0
desc:
	对象池
*/

type oInterface interface {
	add(int) int
}

type oModel struct {
}

func (o oModel) add(i int) int {
	return i + rand.Int()
}

// 通道
var oPool chan *oModel

// 创建对象池
func createObjPool(size int) {
	oPool = make(chan *oModel, size)

	for i := 0; i < size; i++ {
		oPool <- new(oModel)
	}
}

// 执行完放回
func exec(i int) (s int) {
	select {
	case o := <-oPool:
		s = o.add(i)

		oPool <- o
	default:

	}
	return
}

// 使用对象池
func oPoolUsage() {
	createObjPool(10)
	for i := 0; i < 15; i++ {
		go func(i int) {
			fmt.Println(exec(i))
		}(i)
	}
}
