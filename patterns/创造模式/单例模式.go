package build

import (
	"fmt"
	"sync"
)

/*
@author pengshuo
@date 2021/8/11 19:49
version 1.0.0
desc:
	单例模式
*/

type singleton map[string]string

var (
	once     sync.Once
	instance singleton
)

func newInstance() singleton {
	once.Do(func() {
		instance = make(singleton)
	})
	return instance
}

// 使用单例
func usageSingleton() {
	s := newInstance()

	s["this"] = "that"

	s2 := newInstance()

	fmt.Println("This is ", s2["this"])
}
