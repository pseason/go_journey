package build

/*
@author pengshuo
@date 2021/8/11 18:24
version 1.0.0
desc:
	工厂模式
*/

type store interface {
	addStore(int) int
}

type storageType int

const (
	AStorage storageType = 1 << iota
	BStorage
	CStorage
)

/////////////a///////////////
type aStorage struct {
}

func (a aStorage) addStore(i int) int {
	return i + 10
}

/////////////b///////////////
type bStorage struct {
}

func (a bStorage) addStore(i int) int {
	return i + 12
}

/////////////c///////////////
type cStorage struct {
}

func (a cStorage) addStore(i int) int {
	return i + 11
}

// 工厂模式
func factoryNewStorage(t storageType) store {
	switch t {
	case AStorage:
		return aStorage{}
	case BStorage:
		return bStorage{}
	case CStorage:
		return cStorage{}
	default:
		return nil
	}
}

func factoryUsage() {

}
